//
//  CustomeCell_PassWord.h
//  MyReferences
//
//  Created by Neel  Shah on 02/01/12.
//  Copyright (c) 2012 Sunshine Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"
@interface CustomeCell_PassWord : UITableViewCell
{
    UITextField *txt;
    UILabel *lbl;
    PropertyLogBookAppDelegate*appDelegate;
}

@property(nonatomic,retain)UITextField *txt;
@property(nonatomic,retain)UILabel *lbl;

@end
