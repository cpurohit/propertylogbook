//
//  AboutViewController.m
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 11/1/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "AboutViewController.h"

@implementation AboutViewController
@synthesize webview;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


-(IBAction)feedBackButton_pressed:(id)sender
{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            [self displayComposerSheet];
        }
        else
        {
            [self launchMailAppOnDevice];
        }
    }
    else
    {
        [self launchMailAppOnDevice];
    }
}

-(IBAction)iapplab_clicked:(id)sender
{
    
    
    //[[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.iapplab.com/"]];
}


-(IBAction)bonvoyage_clicked:(id)sender
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/bon-voyage-travel-budget-expenses/id589792460?mt=8"]];
    
}

-(IBAction)myreference_clicked:(id)sender
{
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/my-references/id531447634?mt=8"]];
}


// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
    //	[[picker navigationBar] setTintColor:[UIColor blackColor]];
	//[[picker navigationBar] setTintColor:[UIColor colorWithRed:0.36 green:0.09 blue:0.39 alpha:1.00]];
    
	[picker setSubject:@"Contact: Property Log Book"];
	
	
	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:@"contactus@iapplab.com"]; 
    
	
	[picker setToRecipients:toRecipients];
    
    NSString *deviceModel = [UIDevice currentDevice].model;
    nslog(@"Device Model------%@",[UIDevice currentDevice].model);
    NSString *deviceOS = [UIDevice currentDevice].systemVersion;
    nslog(@"Device Current os-----%@",[UIDevice currentDevice].systemVersion);
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    nslog(@"App Version-----%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]);
    
    NSLocale *currentUsersLocale = [NSLocale currentLocale];
    NSString *region = [currentUsersLocale localeIdentifier];
    nslog(@"Current Locale: %@", [currentUsersLocale localeIdentifier]);
	
	[picker setToRecipients:toRecipients];
    
    NSString *emailBody = [NSString stringWithFormat:@"\n \n \n Device : %@ \n OS Version: %@ \n App Version: %@ \n Region Code: %@",deviceModel, deviceOS, version, region];
    
   	[picker setMessageBody:emailBody isHTML:NO];
    
	
	[self presentModalViewController:picker animated:YES];
    [picker release];
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
    [self dismissModalViewControllerAnimated:YES];
	
}

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	NSString *recipients =[NSString stringWithFormat:@"mailto:%@&subject=",@"contactus@iapplab.com"] ;
	NSString *body = @"&body=";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}


- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(dismissAboutControllerIfNeeded:)
                                                 name: @"dismissAboutControllerIfNeeded"
                                               object: nil];

    
    appDelegate = (PropertyLogBookAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    
 
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.title= @"ABOUT";
    
    
    

    
    if(appDelegate.isIphone5)
    {
        iconImageView.image = [UIImage imageNamed:@"aboutus_iphone5.png"];
        iconImageView.frame = CGRectMake(0, 0,320,509);
    }
    
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    [imgView release];

    
    /*
     Start:
     Sun:0004
     Setting custom image in navigation bar..
     */
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0)
    {
       [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
    }
    
    /*
     Ends:
     Sun:0004
     Setting custom image in navigation bar..
     */

    

    if(appDelegate.isIPad)
    {
        //[self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
        /*
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            self.view.frame= CGRectMake(0,0,768,1024);
            iconImageView.frame= CGRectMake(9,5,114,114);
            versionLabel.frame = CGRectMake(self.view.frame.size.width-105,39,86,21);
            urlButton.frame= CGRectMake(self.view.frame.size.width-165,5,160,37);
            versionLabel.font  = [UIFont systemFontOfSize:20.0];
            webview.frame = CGRectMake(0,160,768,600);
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            self.view.frame= CGRectMake(0,0,1024,768);
            iconImageView.frame= CGRectMake(9,5,114,114);
            versionLabel.frame = CGRectMake(self.view.frame.size.width-105,39,86,21);
            urlButton.frame= CGRectMake(self.view.frame.size.width-165,5,160,37);
            versionLabel.font  = [UIFont systemFontOfSize:20.0];
            webview.frame = CGRectMake(0,160,768,600);

        }
         */
        
       
        
    }
    else
    {
       webview.frame = CGRectMake(0,160, 320,290);     
    }
  

    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];

        
    }

    
    
    
    /* CP :  */
    
    if(appDelegate.isIPad)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    }
    
    
    
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    

    
    
    [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:1];
    [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)back_clicked:(id)sender
{
    //[self.navigationController popViewControllerAnimated:TRUE];
    is_dismiss_properly = TRUE;
    [self.navigationController dismissModalViewControllerAnimated:YES];
    //[self.view removeFromSuperview];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    //[self.navigationController.navigationBar.layer removeAllAnimations];

    if(appDelegate.isIPad)
    {
       
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
    textLabel.textColor = [UIColor colorWithRed:112.0f/255.0f green:112.0f/255.0f blue:112.0f/255.0f alpha:1.0];
    textLabel.text = @"Property Log book has been developed by iAppLab Solutions Pty Ltd as a tool for property owners & investors. At iAppLab we believe in continuous improvement & innovation to effectively meet customer needs. \n \nWe welcome any feedback or suggestions from our customers. Feel free to drop us a line by clicking the button below.";
    
    if(appDelegate.isIphone5)
    {
        
        iapplab_site_button.frame = CGRectMake(iapplab_site_button.frame.origin.x, iapplab_site_button.frame.origin.y+88, iapplab_site_button.frame.size.width, iapplab_site_button.frame.size.height);
        
        my_reference_button.frame = CGRectMake(my_reference_button.frame.origin.x, my_reference_button.frame.origin.y+75, my_reference_button.frame.size.width, my_reference_button.frame.size.height);
        
        
        travel_logbook_button.frame = CGRectMake(travel_logbook_button.frame.origin.x, travel_logbook_button.frame.origin.y+70, travel_logbook_button.frame.size.width, travel_logbook_button.frame.size.height);
        
    }
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    appDelegate.currentOrientation = 0;
    
    
    /* CP :  */
    
    
    if(appDelegate.isIPad)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    }
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    

    
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(appDelegate.isIPad)
    {
               
    }
    

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        NSString *reqSysVer = @"6.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        nslog(@"\n currSysVer = %@",currSysVer);
        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        {

            
            if(appDelegate.currentOrientation == 3)
            {
                return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
            }
            else if(appDelegate.currentOrientation == 4)
            {
                return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
            }
            else
            {
                return YES;
            }

        
            
        }
        else
        {
           return YES;
        }
        
                
       
    }
    return NO;
}


-(void)dismissAboutControllerIfNeeded:(id)sender
{
    if(!is_dismiss_properly)
    {
        
        
         [self.navigationController dismissModalViewControllerAnimated:YES];
    }
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            bgImageView.frame = CGRectMake(0, 0,768,920);
            bgImageView.image = [UIImage imageNamed:@"about_bg_ipad.png"];
            travel_logbook_button.frame = CGRectMake(36,335,692,230);
            myReferenceLinkButton.frame = CGRectMake(38,576,692,230);
            
            siteLinkButton.frame = CGRectMake(257,819,256,37);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            bgImageView.frame = CGRectMake(0, 0,1024,655);
            
            travel_logbook_button.frame = CGRectMake(34,238,955,165);
            
            myReferenceLinkButton.frame = CGRectMake(34,416,955,165);
            
            
            bgImageView.image = [UIImage imageNamed:@"aboutus_ipad_landscape.png"];
            siteLinkButton.frame = CGRectMake(363,588,256,37);
        }
    }
    
    
}

#pragma mark -
@end
