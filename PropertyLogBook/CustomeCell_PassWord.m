//
//  CustomeCell_PassWord.m
//  MyReferences
//
//  Created by Neel  Shah on 02/01/12.
//  Copyright (c) 2012 Sunshine Infotech. All rights reserved.
//

#import "CustomeCell_PassWord.h"

@implementation CustomeCell_PassWord

@synthesize txt,lbl;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        
        appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
        
        lbl=[[UILabel alloc] init];
        lbl.backgroundColor=[UIColor clearColor];
        lbl.textAlignment=UITextAlignmentLeft;
        lbl.textColor=[UIColor blackColor];
        lbl.font=[UIFont systemFontOfSize:15.0];
        [self.contentView addSubview:lbl];
        [lbl release];
        
        txt=[[UITextField alloc] init];
        txt.backgroundColor=[UIColor clearColor];
        txt.textColor=[UIColor blackColor];
        txt.textAlignment=UITextAlignmentRight;
        txt.font=[UIFont systemFontOfSize:14.0];
        [self.contentView addSubview:txt];
        [txt release];
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    lbl.frame=CGRectMake(40, 12,130, 21);
            appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        if(appDelegate.isIOS7)
        {
            
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                txt.frame=CGRectMake(490, 12,250, 20);
            }
            else
            {
                txt.frame=CGRectMake(730, 12,250, 20);
            }

            
        }
        else
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                txt.frame=CGRectMake(400, 12,250, 20);
            }
            else
            {
                txt.frame=CGRectMake(640, 12,250, 20);
            }

        }
        
        

        //txt.backgroundColor = [UIColor redColor];
    }
    else 
    {
        
        txt.frame=CGRectMake(162, 12, 125, 20);
        if(appDelegate.isIOS7)
        {
            txt.frame=CGRectMake(182, 12, 125, 20);
        }
        
        
        
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
