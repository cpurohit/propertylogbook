//
//  DashBoardDetailViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 9/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"



@class PropertyLogBookAppDelegate;
@interface DashBoardDetailViewController : UIViewController<CPTPieChartDataSource,UIScrollViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate,CPTPlotDataSource>
{
    
    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UIView *incomeGraphView, *incomeBarView, *expenseGraphView, *expenseBarView;
    IBOutlet UIImageView *propertyImage;
    IBOutlet UIImageView *dashboard_proeprtydetail_bg;
    CPTXYGraph *pieChart, *pieChart1;
    
    IBOutlet UIImageView *incomePercentageImg, *expensePercentageImg;

    IBOutlet UILabel *noIncomePie, *noIncomeBar, *noExpensePie, *noExpenseBar;
   
    IBOutlet UIScrollView *pieChartScrollView, *barChartScrollView;
    CPTXYGraph *barChart;
    CPTXYGraph *barChart1;
    
    IBOutlet UILabel *propertyName, *period;
    IBOutlet UISegmentedControl *segment;
    int arrayindex;
    double totalIncomeAmount, totalExpenseAmount;
    
    IBOutlet UITableView *tblView;
    NSDateFormatter *df;
    
    NSMutableArray *dashboardArray;
    
    NSString *periodString;
    
    NSMutableArray *incomeSummeryArray, *expenseSummeryArray, *expenseDataTypeArray;
    
    NSMutableDictionary *incomeSummeryDictionary, *expenseSummeryDictionary;
    
    NSMutableArray *incomeDataforChart, *expenseDataForChart;
    
    IBOutlet UIView *pieChartView, *barChartView, *tableViewChart;
    NSMutableArray *tempIncomeArray, *tempExpenseArray;
    NSMutableArray *incomeDataTypeArray;
    
    IBOutlet UIScrollView *incomeBarScrollView;
    IBOutlet UIScrollView *expenseBarScrollView;
    // pie chart custom method
    
    UIScrollView *infoScrollView;
    int yCalForColor;
    
    UIScrollView *infoScrollView1;
    int yCalForColor1;
    
    /*Starts:For Bar Chart..*/
    //IBOutlet UIScrollView *barChart1scrollView;
    /*Starts:For Bar Chart..*/
    
    IBOutlet UIImageView*incomeBarAxis;
    IBOutlet UIImageView*expenseBarAxis;
    
    NSMutableArray *customTickLocations;
    
    
    CPTBarPlot *barPlot;
    CPTBarPlot *barPlotIncome;
    /**/
    IBOutlet UIImageView*bgImageView;
    IBOutlet UIImageView*barImageView;
    IBOutlet UILabel*lblLine;
    
    
    IBOutlet UILabel*lblExpenseSummary;
    IBOutlet UILabel*lblIncomeSummary;    
   
    IBOutlet UILabel*lblBarExpenseSummary;
    IBOutlet UILabel*lblBarIncomeSummary;
    
    
    /*
        Sun:0004
     Removed from IBOutlet,as need to hide this.
     */
    
    IBOutlet UIImageView*imgIncomeSummary;    
    IBOutlet UIImageView*imgExpenseSummary;    
    
    IBOutlet UIImageView*imgBarIncomeSummary;    
    IBOutlet UIImageView*imgBarExpenseSummary;    
    
    
    
    
    
    /*Starts:Income Expense Tab Bar*/
    IBOutlet UISegmentedControl *incomeExpenseFilterSegmentControl;
    IBOutlet UIView*segementControlView;
    IBOutlet UIImageView*barBackground;
    
    BOOL isIncome;
    
    IBOutlet UIButton*imgPieChartButton;
    IBOutlet UIButton*imgBarChartButton;
    IBOutlet UIButton*imgTableViewButton;
    int selectedBarTypeSegment;
    IBOutlet UIView*chartTypeView;
    
    
    /*For income and expense button*/
    
    IBOutlet UIButton*imgIncomeButton;
    IBOutlet UIButton*imgExpenseButton;
    int isIncomeOrExpenseSelected;//0 = income 1 = expense
    
    
    
    CPTPieChart *piePlot;
    CPTPieChart * piePlotIncome;
    
    CPTGraphHostingView *hostingView;
    
    
    /*
        Sun:0004
        Phase : 3
        From and to date
     */
    
    NSString*from_date;
    NSString*to_date;
    
    /*
     Sun:0004
     Phase : 3
     From and to date
     */

    
    /*
        Sun:0004
        income
     */
    
    IBOutlet UIImageView*piechart_shadow_imageView;
    IBOutlet UIImageView*piechart_hover_imageView;

    /*
     Sun:0004
     expense
     */
    IBOutlet UIImageView*expense_piechart_shadow_imageView;
    IBOutlet UIImageView*expense_piechart_hover_imageView;

    
    IBOutlet UIImageView*pie_chart_label_bg_imageView;
    
    BOOL is_expense_pie_generate;
    
}



-(void)generateBarChart;
-(void)generateExpenseBarChart;
-(void)generateExpensePieChart;

-(IBAction)segment_change;

-(void)segmentButtonPie_Pressed:(id)sender;
-(void)segmentButtonBar_Pressed:(id)sender;
-(void)segmentButtonTable_Pressed:(id)sender;
-(void)segment_change_through_click;

@property (nonatomic, retain)IBOutlet UIImageView *propertyImage;
@property (nonatomic, readwrite)int arrayindex;
@property (nonatomic, retain)NSMutableArray *dashboardArray;
@property (nonatomic, retain)NSString *periodString;
@property (nonatomic, retain)IBOutlet UIImageView*incomeBarAxis;
@property (nonatomic, retain)IBOutlet UIImageView*expenseBarAxis;
@property (nonatomic,retain)NSString*from_date;
@property (nonatomic,retain)NSString*to_date;


-(IBAction)incomeExpenseFilterSegmentControl_Changed:(id)sender;

//-(void)income_expense_segment_change_through_click;

-(IBAction)income_expense_segment_change_through_click:(id)sender;

-(IBAction)bar_type_button_pressed:(id)sender;

-(void)generateIncomeExpenseReport;

/*Sun:0004
 Phase:3
 Generating pdf,excel from here...
 */

-(IBAction)choose_report_type_button_pressed:(id)sender;


@end
