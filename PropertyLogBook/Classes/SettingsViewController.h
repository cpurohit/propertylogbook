//
//  SettingsViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

/*
 Tags
 
 Starting from 1000:Section header button;
 */


/*Methods
 
 
 showHideSection : method for showing and hiding sections based on selection.
 
 */


#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Base64.h"
#import "Note.h"
@interface SettingsViewController : UIViewController<UITableViewDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIAlertViewDelegate,UIAlertViewDelegate> 
{
	
	IBOutlet UITableView *tblView;
	
    NSMutableArray *propertyArray,*customizeArray,*preferences_array,*backupresoreArray,*help_feedback_array;
    
    
    
    PropertyLogBookAppDelegate *appDelegate;
    NSMutableArray *detailArray;
    NSMutableDictionary*detailDictionary;
    IBOutlet UIView *footerView;
    IBOutlet UIButton *btnDelete;
    NSString*emailRecepient;
    
    NSData*backupNoteData;
    
    /*Activity Indicator.*/
    UIView *ActivityView;
    UIActivityIndicatorView *mainActivityIndicator;
    
    /*Checking if ios 5*/
    
    BOOL isIOS5;
    int successLoopCount;
    BOOL isPasswordSet;
    
    
    /*
        Sun:0004
        Phase:3
     */

    IBOutlet UIView*headerView_for_section;
    IBOutlet UIButton*header_button;
    
    int pressed_button_tag;
    NSMutableArray*minimised_section_array;
    
    
    NSString*date_filter_start_date;
    NSString*date_filter_end_date;
    
    
    NSMetadataQuery *query_for_backup;
    
    NSString*icloud_backupdate;
    
    UISwitch *milage_on_off_switch;
    UIButton*km_button;
    UIButton*mile_button;
    
    NSMutableDictionary*data_dictionary;
    
    BOOL is_view_will_appear_called;
}

/*Starts:icloud*/

@property (strong) Note * doc;
@property (strong) NSMutableArray * notes;
@property (strong) NSMetadataQuery *query;
@property (nonatomic,readwrite) int successLoopCount;
-(void)addData;
-(void)loadNotes;
/*Ends:icloud*/

-(IBAction)restore_clicked;

-(IBAction)btnDelete_Clicked:(id)sender;
-(IBAction)sendSms;
-(IBAction)showEmail;
-(void)displayComposerSheet ;
-(void)launchMailAppOnDevice;
-(void)displayComposerSheetforsupport;
-(void)displayComposerSheetForFeedback;
-(int)getPassCodeStatus;

/*
 Sun:0004
 Phase:3
 */

-(IBAction)header_button_pressed:(id)sender;


@end
