//
//  MasterCheckPassViewController.h
//  MyReferences
//
//  Created by Neel  Shah on 02/01/12.
//  Copyright (c) 2012 Sunshine Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSON.h"
@class PropertyLogBookAppDelegate;
@interface MasterCheckPassViewController : UIViewController<UITextFieldDelegate>
{
    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UITextField *txt_Pass;
    IBOutlet UITableView*tblView;
    IBOutlet UILabel *messageLabel;
    
    NSString *StringPassword;
    
    NSUserDefaults* prefs;
    
    /*Starts:WebService*/
    NSArray *requestObjects;
	NSArray *requestkeys;
	NSDictionary *requestJSONDict;
	NSMutableDictionary *finalJSONDictionary;
	NSString *jsonRequest;
	NSString *requestString;
	NSData *requestData;
	NSString *urlString;
	NSMutableURLRequest *request;
	NSData *returnData;
	NSError *error;
	SBJSON *json;
	NSDictionary *responseDataDictionary;
    /*Ends:WebService*/ 

    UITextField *txtPass;
    
    /*Activity Indicator.*/
    UIView *ActivityView;
    UIActivityIndicatorView *mainActivityIndicator;
    
    
    IBOutlet UIImageView*masterPasswordBG;
    IBOutlet UIImageView*masterPasswordLockButton;
    IBOutlet UIButton*forgotPasswordButton;
    
    IBOutlet UIImageView*bg_imgeView;
    IBOutlet UIImageView*lock_imageView;
    
    IBOutlet UIButton*forgot_password_button;

    
    
    
    
}
@property(nonatomic,strong)NSDictionary *responseDataDictionary;
-(IBAction)DoneToolBarKey;
-(IBAction)ForgotPassword;

@end
