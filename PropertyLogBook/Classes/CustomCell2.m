//
//  CustomCell2.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomCell2.h"


@implementation CustomCell2
@synthesize financialLabel, financialSwitch, financialTextField, repaymentButton, leaseSwitch, leaseDateButton, detailLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (self) 
    {
        
        financialTextField = [[UITextField alloc]initWithFrame:CGRectMake(150, 7, 150, 30)];
        financialTextField.borderStyle = UITextBorderStyleNone;
        financialTextField.textAlignment = UITextAlignmentRight;
        [financialTextField setFont:[UIFont systemFontOfSize:15.0]];
        [financialTextField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
        [self.contentView addSubview:financialTextField];
       // [financialTextField release];
        
        
        financialSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(206, 8, 94, 27)];
        financialSwitch.onTintColor = [UIColor colorWithRed:10.0f/255.0f green:142.0f/255.0f blue:184.0f/255.0f alpha:1.0];

        
        
        [self.contentView addSubview:financialSwitch];
        
        leaseSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(206, 8, 94, 27)];
        
        leaseSwitch.onTintColor = [UIColor colorWithRed:10.0f/255.0f green:142.0f/255.0f blue:184.0f/255.0f alpha:1.0];
        
        [self.contentView addSubview:leaseSwitch];
        
        
        financialLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 162, 21)];
        //lbl.text = @"Select Mesurement:";
        financialLabel.numberOfLines = 0;
        financialLabel.lineBreakMode = UILineBreakModeWordWrap;
        [financialLabel setBackgroundColor:[UIColor clearColor]];
        [financialLabel setFont:[UIFont systemFontOfSize:15.0]];
        
        [self.contentView addSubview:financialLabel];
        [financialLabel release];
        

        repaymentButton = [[UIButton buttonWithType:UIButtonTypeCustom]retain];
		repaymentButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        //[repaymentButton setBackgroundImage:[UIImage imageNamed:@"dropDownBlue.png"] forState:UIControlStateNormal];

		[repaymentButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
		[self.contentView addSubview:repaymentButton];

        
        leaseDateButton = [[UIButton buttonWithType:UIButtonTypeCustom]retain];
        leaseDateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
		//[leaseDateButton setBackgroundImage:[UIImage imageNamed:@"dropDownBlue.png"] forState:UIControlStateNormal];
		
        

        [leaseDateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
		[self.contentView addSubview:leaseDateButton];
        
        
        
        detailLabel = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        detailLabel.textAlignment = UITextAlignmentRight;
        detailLabel.numberOfLines = 0;
        detailLabel.lineBreakMode = UILineBreakModeTailTruncation;
        [detailLabel setFont:[UIFont systemFontOfSize:15.0]];
        [detailLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:detailLabel];
        [detailLabel release];
        
       // [self.contentView setBackgroundColor:[UIColor whiteColor]];
        
        
    }
    return self;
}

-(void)layoutSubviews
{
    
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            financialTextField.frame = CGRectMake(150, 12, 500, 30);
            
            //financialTextField.backgroundColor = [UIColor redColor];
            
            detailLabel.frame = CGRectMake(520, 7, 140, 30);
            
            financialLabel.frame = CGRectMake(10, 7, 152, 30);
            repaymentButton.frame = CGRectMake(510, 7, 140, 30);
            leaseDateButton.frame = CGRectMake(510, 7, 140, 30);
            
            NSString *reqSysVer = @"5.0";
            NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
            nslog(@"\n currSysVer = %@",currSysVer);
            if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
            {
                isIOS5 = YES;
                
                financialSwitch.frame = CGRectMake(580, 8, 94, 27);
                leaseSwitch.frame = CGRectMake(580, 8, 94, 27);
                
                
                

            }
            else
            {
                isIOS5 = NO;
                financialSwitch.frame = CGRectMake(550, 8, 94, 27);
                leaseSwitch.frame = CGRectMake(550, 8, 94, 27);

            }
            
            if(appDelegate.isIOS7)
            {
                
                financialTextField.frame = CGRectMake(financialTextField.frame.origin.x+90,financialTextField.frame.origin.y,financialTextField.frame.size.width, financialTextField.frame.size.height);
                
                financialSwitch.frame= CGRectMake(financialSwitch.frame.origin.x+90,financialSwitch.frame.origin.y,financialSwitch.frame.size.width, financialSwitch.frame.size.height);
                
                leaseSwitch.frame= CGRectMake(leaseSwitch.frame.origin.x+90,leaseSwitch.frame.origin.y,leaseSwitch.frame.size.width, leaseSwitch.frame.size.height);
                
                
                
                repaymentButton.frame = CGRectMake(repaymentButton.frame.origin.x+90,repaymentButton.frame.origin.y,repaymentButton.frame.size.width, repaymentButton.frame.size.height);
                leaseDateButton.frame = CGRectMake(leaseDateButton.frame.origin.x+90,leaseDateButton.frame.origin.y,leaseDateButton.frame.size.width, leaseDateButton.frame.size.height);
                
                
            }
            
            
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
             //financialTextField.backgroundColor = [UIColor redColor];
            
            financialTextField.frame = CGRectMake(150, 12,750, 30);
            detailLabel.frame = CGRectMake(520, 7, 140, 30);
            financialLabel.frame = CGRectMake(10, 7, 152, 30);
            repaymentButton.frame = CGRectMake(768, 7, 140, 30);
            leaseDateButton.frame = CGRectMake(768, 7, 140, 30);
            
            
            NSString *reqSysVer = @"5.0";
            NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
            nslog(@"\n currSysVer = %@",currSysVer);
            if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
            {
                isIOS5 = YES;
                financialSwitch.frame = CGRectMake(830, 8, 94, 27);
                leaseSwitch.frame = CGRectMake(830, 8, 94, 27);
            }
            else
            {
                isIOS5 = NO;
                financialSwitch.frame = CGRectMake(810, 8, 94, 27);
                leaseSwitch.frame = CGRectMake(810, 8, 94, 27);
            }

            
            
            if(appDelegate.isIOS7)
            {
                
                financialTextField.frame = CGRectMake(financialTextField.frame.origin.x+90,financialTextField.frame.origin.y,financialTextField.frame.size.width, financialTextField.frame.size.height);
                
                financialSwitch.frame= CGRectMake(financialSwitch.frame.origin.x+90,financialSwitch.frame.origin.y,financialSwitch.frame.size.width, financialSwitch.frame.size.height);
                
                leaseSwitch.frame= CGRectMake(leaseSwitch.frame.origin.x+90,leaseSwitch.frame.origin.y,leaseSwitch.frame.size.width, leaseSwitch.frame.size.height);
                
                
                
                repaymentButton.frame = CGRectMake(repaymentButton.frame.origin.x+90,repaymentButton.frame.origin.y,repaymentButton.frame.size.width, repaymentButton.frame.size.height);
                leaseDateButton.frame = CGRectMake(leaseDateButton.frame.origin.x+90,leaseDateButton.frame.origin.y,leaseDateButton.frame.size.width, leaseDateButton.frame.size.height);
                
                
            }

            
            
            
        }
        

        
    }
    else
    {
        
        financialTextField.frame = CGRectMake(150, 12, 140, 30);
        
        
        
        NSString *reqSysVer = @"5.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        nslog(@"\n currSysVer = %@",currSysVer);
        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        {
            isIOS5 = YES;
            financialSwitch.frame = CGRectMake(211, 8, 94, 27);
            leaseSwitch.frame = CGRectMake(211, 8, 94, 27);
            
            if(appDelegate.isIOS7)
            {
                
                financialSwitch.frame = CGRectMake(238, 8, 94, 27);
                leaseSwitch.frame = CGRectMake(238, 8, 94, 27);
                
            }

            
        }
        else
        {
            isIOS5 = NO;
            financialSwitch.frame = CGRectMake(196, 8, 94, 27);
            leaseSwitch.frame = CGRectMake(196, 8, 94, 27);
        }
        
        financialLabel.frame = CGRectMake(10, 7, 152, 30);
        financialLabel.font = [UIFont systemFontOfSize:15.0f];
        
        repaymentButton.frame = CGRectMake(150,7, 120, 30);
        leaseDateButton.frame = CGRectMake(150, 7, 120, 30);
        detailLabel.frame = CGRectMake(150, 7, 140, 30);
    }
    

    
	[super layoutSubviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
