//
//  ReminderViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

/*
 btn_reminder_type_pending = 8001
 btn_reminder_type_completed = 8002
 */

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

@class AddReminderViewController;

@interface ReminderViewController : UIViewController<UITableViewDelegate,UIAlertViewDelegate>
{

	IBOutlet UITableView *tblView;
	AddReminderViewController *addReminder;
	NSMutableArray *reminderArray;
    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UILabel *lblNoData, *lblPlus;
    NSString *reminderType;
    NSString *property;
    NSString *date ;
    NSString *time;
    NSString *repeat;
    NSString *notes;
    IBOutlet UIImageView *reminderImage;
    int numberSection;
    NSMutableDictionary *propertyDictionary;
    
    NSMutableArray *groupBtnArray;

  //  UIButton *btn;
    
    int pressed_button_tag;
    NSMutableArray*minimised_section_array;

    
    IBOutlet UIButton*btn_reminder_type_pending;
    IBOutlet UIButton*btn_reminder_type_completed;
    
    NSMutableDictionary *replaceDictionary;//earlier local,now need to put here to manage
    
    
    NSMutableArray *tempArray_reminder_checklist;//earlier local,now need to put here to manage
    
    int selectedSection;
    int selectedRow;
    int reminder;
    UIButton*temp_button_for_reminder_switch;
    
    IBOutlet UIView*view_reminder_lagend_bg;
    BOOL is_any_reminder_overDue;
    
    NSCalendar *gregorian;
    NSDateFormatter*date_formatter;
    
    NSDateComponents *test_components;
    NSDateComponents *components;
    
}
- (IBAction) scheduleAlarm;
-(IBAction)btn_reminder_type_clicked:(id)sender;
-(void)load_reminder_table;
-(void) cancelAlarm;
@end
