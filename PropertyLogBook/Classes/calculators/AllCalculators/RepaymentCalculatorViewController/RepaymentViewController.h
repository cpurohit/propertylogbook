//
//  RepaymentViewController.h
//  Loansactually
//
//  
//  
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "PropertyLogBookAppDelegate.h"

@class CustomToolbar;
@interface RepaymentViewController : UIViewController <UITextFieldDelegate,MFMailComposeViewControllerDelegate>
{
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UIButton *btnBack;
    IBOutlet UIButton *btnEmail;
    IBOutlet UIButton *btnCalculator;
    IBOutlet UIButton *btnInfo;
    IBOutlet UITableView *tableView;
    NSArray *arrText;
    NSInteger indexOfPaymentType;
    NSInteger indexOfRepayment;
    IBOutlet CustomToolbar *toolbar;
    NSString *repaymentType;
    NSString *repayment;
    NSString *strRepay;
    
    NSString *stramountTemp,*strrateTemp,*strtermTemp;
    
    int current_tag;
    
    PropertyLogBookAppDelegate*appDelegate;
    IBOutlet UIView*footer_view;
    
    IBOutlet UIButton*reset_bottom_button;
    IBOutlet UILabel*important_note_label;
    
    NSNumberFormatter *numberFormatter;
    NSNumberFormatter *_currencyFormatter;
    
    
    NSNumberFormatter *currencyFormatter;
    
    IBOutlet UIBarButtonItem *cancelBtn;
    
    
}

@property(nonatomic,retain) NSString *amount;
@property(nonatomic,retain) NSString *rate;
@property(nonatomic,retain) NSString *term;
@property(nonatomic,retain) NSString *monthlyRepayment;
@property(nonatomic,retain) NSString *totalCostOfLoan;
@property(nonatomic,retain) NSString *totalInterestPayable;


-(IBAction)btnBack_Clicked:(id)sender;
-(IBAction)btnEmail_Clicked:(id)sender;
-(IBAction)btnCalculator_Clicked:(id)sender;
-(IBAction)btnInfo_Clicekd:(id)sender;
-(IBAction)btnDoneClicked:(id)sender;
-(IBAction)reset_button_pressed:(id)sender;

-(IBAction)cancel_button_pressed:(id)sender;


@end
