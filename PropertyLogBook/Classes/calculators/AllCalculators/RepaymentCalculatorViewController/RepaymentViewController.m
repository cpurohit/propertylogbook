//
//  RepaymentViewController.m
//  Loansactually
//
//  
//  
//

#import "RepaymentViewController.h"
#import "RepaymentCell.h"
#import "Formatter.h"
#import "CustomToolbar.h"
#import <QuartzCore/QuartzCore.h>
@interface RepaymentViewController ()

@end

@implementation RepaymentViewController

@synthesize amount = _amount;
@synthesize rate = _rate;
@synthesize term = _term;
@synthesize monthlyRepayment = _monthlyRepayment;
@synthesize totalCostOfLoan = _totalCostOfLoan;
@synthesize totalInterestPayable = _totalInterestPayable;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"LOAN REPAYMENTS";
    }
    return self;
}
-(void)dealloc
{
    [arrText release];
    [_currencyFormatter release];
    [currencyFormatter release];
    [numberFormatter release];
    [footer_view release];
    
    /*Property*/

    [_amount release];
    [_rate release];
    [_term release];
    
    [stramountTemp release];
    [strrateTemp release];
    [strtermTemp release];
    
    
    [_monthlyRepayment release];
    [_totalCostOfLoan release];
    [_totalInterestPayable release];
    
    /*Property*/
    
    if(!appDelegate.isIOS7)
    {
         [super dealloc];
    }
   
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y,tableView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y-25.0f,tableView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y,tableView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y-25.0f,tableView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y,tableView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
else
{
    if(appDelegate.isIOS7)
    {
        tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y-25.0f,tableView.frame.size.width,self.view.frame.size.height+25.0f);
    }
}
    
    
    
    
    //lblTitle.font = [UIFont fontWithName:@"Bookman Old Style" size:15.0];
    lblTitle.font = [UIFont systemFontOfSize:15.0f];
    
    arrText = [[NSArray alloc] initWithObjects:@"Loan Amount",@"Interest Rate (%)",@"Loan Term",@"Frequency",@"Repayment Type", nil];
    indexOfRepayment = 1;
    indexOfPaymentType = 1;
    
     _currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    currencyFormatter = [[NSNumberFormatter alloc] init];
    
    numberFormatter = [[NSNumberFormatter alloc] init] ;
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    
    stramountTemp= [[NSString alloc] init];
    strrateTemp = [[NSString alloc] init];
    strtermTemp = [[NSString alloc] init];
    
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [_currencyFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [_currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [_currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [_currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"NAD"])
        {
            [_currencyFormatter setCurrencyCode:@"NAD"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        
    }

    
    
    _amount = @"0.00";
    _amount = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_amount floatValue]]];
    [_amount retain];
    
    
    _rate = @"0.00";
    _term = @"0.00";
    stramountTemp = @"0.00";
    strrateTemp = @"0.00";
    strtermTemp = @"1.00";
    
    _monthlyRepayment = @"0.00";
    
   
    _monthlyRepayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_monthlyRepayment intValue]]];
    [_monthlyRepayment retain];
    
    
    _totalCostOfLoan = @"0.00";
    
    
    _totalCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_totalCostOfLoan intValue]]];
    [_totalCostOfLoan retain];
    
    
    _totalInterestPayable = @"0.00";
    
    _totalInterestPayable = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_totalInterestPayable intValue]]];
    [_totalInterestPayable retain];
    
    [toolbar setHidden:YES];
    
    repaymentType = @"Prin. & inter.";
    repayment = @"Monthly";
    
    strRepay = @"Monthly repayment";
    [tableView setBackgroundColor:[UIColor clearColor]];
    
    // Do any additional setup after loading the view from its nib.
    
    
    tableView.backgroundColor = [UIColor clearColor];
    [tableView setBackgroundView:nil];
    [tableView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    [imgView release];
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }
    

    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    if(appDelegate.isIPad)
    {
        
        return TRUE;
    }
    return FALSE;
    
    /*
    BOOL isPortraite = FALSE;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft||interfaceOrientation == UIInterfaceOrientationLandscapeRight)
        {
            isPortraite = FALSE;
        }
        else
        {
            isPortraite = TRUE;
        }
    }
    else {
        return YES;
    }
    return isPortraite;
     */
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //[tableView reloadData];
    
    if(appDelegate.isIPad)
    {
        [tableView reloadData];
        [appDelegate manageViewControllerHeight];
    }
    
    
}


- (void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    //[self.view addSubview:appDelegate.bottomView];
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */


    tableView.backgroundColor = [UIColor clearColor];
    [tableView setBackgroundView:nil];
    [tableView setBackgroundView:[[[UIView alloc] init] autorelease]];

    if(appDelegate.isIPad)
    {
        
		[self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    	[self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
	}
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

-(IBAction)btnBack_Clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)btnEmail_Clicked:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        nslog(@"%@ %@",_totalInterestPayable,_totalCostOfLoan);
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        
        [picker setSubject:@"Home Loan Repayment"];
        
        
        NSMutableString *EmailBody1=[[[NSMutableString alloc] initWithString:@"<html><body>"] retain];
        
        [EmailBody1 appendString:@"Input:"];
       
        
        [EmailBody1 appendString:[NSString stringWithFormat:@"<table width='150' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>Repayment Type</th><td> %@</td></tr><tr><th>Loan Amount</th><td> %@</td></tr><tr><th>Interest Rate</th><td> %@&#37</td></tr><tr><th>Loan Term</th><td> %@</td></tr><tr><th>Repayment</th><td> %@</td></tr>",repaymentType,_amount,_rate,_term,repayment]];
        
        
        
        [EmailBody1 appendString:@"</table>"];
        
        
        [EmailBody1 appendString:@"Output:"];
        
        [EmailBody1 appendString:[NSString stringWithFormat:@"<table width='150' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>%@</th><td> %@</td></tr><tr><th>Total cost of loan</th><td> %@</td></tr><tr><th>Total interest payable</th><td> %@</td></tr>",strRepay,_monthlyRepayment,_totalCostOfLoan,_totalInterestPayable]];
        
        
        //        [EmailBody1 appendString:@"<table width='310' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>Loan Amount</th><th>Total cost of loan</th><th>Total interest payable</th></tr>"];
        //
        //        [EmailBody1 appendString:[NSString stringWithFormat:@"<tr><td>  %@ </td><td>  %@ </td><td>      %@ </td></tr>",_amount,_rate,_term]];
        [EmailBody1 appendString:@"</table>"];
        
        
        
        [EmailBody1 appendString:@"</body></html>"];
        
        [picker setMessageBody:EmailBody1 isHTML:YES];
        
        [self presentModalViewController:picker animated:YES];
        
        [picker release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            nslog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            nslog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            nslog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            nslog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            nslog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)btnCalculator_Clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(IBAction)btnInfo_Clicekd:(id)sender
{
    
}


-(IBAction)cancel_button_pressed:(id)sender
{
    [toolbar setHidden:YES];
    [self.view endEditing:TRUE];
    
}

-(IBAction)btnDoneClicked:(id)sender
{
    [toolbar setHidden:YES];
    
    
   

    
    
    UITextField *txtAmount = (UITextField *)[self.view viewWithTag:101];
    UITextField *txtRate = (UITextField *)[self.view viewWithTag:102];
    
    
    
    
    UITextField *txtTerms = (UITextField *)[self.view viewWithTag:103];
    
    [txtAmount resignFirstResponder];
    [txtRate resignFirstResponder];
    [txtTerms resignFirstResponder];
    
    UITextField *txtMonthlyRepayment = (UITextField *)[self.view viewWithTag:5000];
    UITextField *txtTotalCostOfLoan = (UITextField *)[self.view viewWithTag:5001];
    UITextField *txtInterestPayble= (UITextField *)[self.view viewWithTag:5002];
    
    
    
    nslog(@"stramountTemp == %@",stramountTemp);
    nslog(@"strrateTemp == %@",strrateTemp);
    nslog(@"strtermTemp == %@",strtermTemp);
    
    NSString *strAmount = stramountTemp;
    NSString *strRate = strrateTemp;
    NSString *strTerms = strtermTemp;
    
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"," withString:@""];
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"" withString:@""];
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@" " withString:@""];
    strRate =  [strRate stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    
    
    double ans = 0.0;
    double amounts = [strAmount doubleValue];
    double rates =  [strRate floatValue];
    double terms = [strTerms floatValue];
    
    
    
    
    //
    
    
    NSDecimalNumber *someAmount = [NSDecimalNumber decimalNumberWithString:@"5.00"];
    
    
    
    nslog(@"%@", [currencyFormatter stringFromNumber:someAmount]);
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [currencyFormatter setCurrencyCode:appDelegate.curCode];
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [currencyFormatter setLocale:locale];
            
        }
        else if([[currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        else if([[currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
    }
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    
    int currencyScale = [currencyFormatter maximumFractionDigits];

    nslog(@"\n scale = %d",currencyScale);
    
  
    
    
      
    
    
    if (indexOfPaymentType == 2 && indexOfRepayment == 1)
    {
        ans = (amounts * rates)/1200;
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*12);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        
        
        
        
        
        
        
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = strtermTemp;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 2 && indexOfRepayment == 2)
    {
        ans = (amounts * rates)/(2600);
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*26);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = strtermTemp;
        [_term retain];
        
        
    }
    else if (indexOfPaymentType == 2 && indexOfRepayment == 3)
    {
        ans = (amounts * rates)/(5200);
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*52);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = strtermTemp;
        [_term retain];
        
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 1)
    {
        double r = rates/1200;
        double month = terms *12;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*12);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = strtermTemp;
        [_term retain];
        
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 2)
    {
        
        double r = rates/2600;
        double month = terms *26;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*26);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = strtermTemp;
        [_term retain];
        
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 3)
    {
        double r = rates/5200;
        double month = terms *52;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*52);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = strtermTemp;
        [_term retain];
        
        
        
        
    }
    
    
    
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    nslog(@"\n montly = %@",_monthlyRepayment);
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [_currencyFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [_currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [_currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [_currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"NAD"])
        {
            [_currencyFormatter setCurrencyCode:@"NAD"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        
    }
    
    
    
    if (amounts == 0 || rates == 0)
    {
        _monthlyRepayment = @"0.00";
        _totalCostOfLoan = @"0.00";
        _totalInterestPayable = @"0.00";
       
        
    }
    
    
     float temp = (float)[_totalCostOfLoan floatValue];
    _totalCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_totalCostOfLoan retain];
    
    temp = (float)[_totalInterestPayable floatValue];
    _totalInterestPayable = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_totalInterestPayable retain];
    
    temp = (float)[_monthlyRepayment floatValue];
    _monthlyRepayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_monthlyRepayment retain];
   

    [tableView reloadData];
    [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(selectKeyboard) userInfo:nil repeats:NO];
    
    
}
-(void)selectKeyboard
{
    UITextField*tempTextField = (UITextField*)[self.view viewWithTag:current_tag];
     [tempTextField resignFirstResponder];
    
    tempTextField = (UITextField*)[self.view viewWithTag:current_tag+1];
    nslog(@"\n text = %@",tempTextField.text);
    if(tempTextField!=nil)
    {
        [tempTextField becomeFirstResponder];
    }

}


#pragma mark - reset_button_pressed
#pragma mark Resetting result...

-(IBAction)reset_button_pressed:(id)sender
{
    
    _monthlyRepayment = @"0.00";
    _totalCostOfLoan = @"0.00";
    _totalInterestPayable = @"0.00";
    
    _amount = @"0.00";
    _rate = @"0.00";
    _term = @"0.00";
    
    stramountTemp= @"0.00";
    strrateTemp= @"0.00";
    
    
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [_currencyFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [_currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [_currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [_currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"NAD"])
        {
            [_currencyFormatter setCurrencyCode:@"NAD"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        
    }

    
    _amount = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_amount floatValue]]];
    [_amount retain];
    
    
    _monthlyRepayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_monthlyRepayment floatValue]]];
    [_monthlyRepayment retain];
    
    _totalInterestPayable = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_totalInterestPayable floatValue]]];
    [_totalInterestPayable retain];
    
    
    _totalCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_totalCostOfLoan floatValue]]];
    [_totalCostOfLoan retain];

    
    
    repaymentType = @"Prin. & inter.";
      
    strRepay = @"Monthly repayment";
    repayment = @"Monthly";
    indexOfRepayment = 1;
    
    indexOfPaymentType = 1;
    
    [tableView reloadData];
}

#pragma mark TableView Methods



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == 1)
    {
       return 210;
    }
    return 0.0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger cnt = 0;
    if (section == 0)
    {
        cnt = 5;
    }
    if (section == 1)
    {
        cnt = 3;
    }
    
    return cnt;
}


- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if(section == 1)
    {
        return @" Important Note: The results from all calculators are approximate guide only. These results are not a quote, a pre-qualification for a loan, a loan offer, or investment advice. The calculations are not intended to be a substitute for professional financial advice.Before taking out a loan you should consult your mortgage broker. ";
    }
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section == 0)
    {
        
        if(indexPath.row==3)
        {
            if(!appDelegate.isIPad)
            {
                return 40;
            }
            
            return 40;
        }
        
        
    }

    
    return 40.0;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    
    
    if(section==1)
    {
       
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                
                footer_view.frame = CGRectMake(0, 0,680, 254);
                reset_bottom_button.center = CGPointMake(384,40);
                
                
                important_note_label.frame = CGRectMake(0, 0,680,132);
                important_note_label.center= CGPointMake(384,120);
                /*
                 reset_bottom_button.frame = CGRectMake(1000,reset_bottom_button.frame.origin.y,reset_bottom_button.frame.size.width, reset_bottom_button.frame.size.height);
                 */
                
            }
            else
            {
                footer_view.frame = CGRectMake(0, 0,936, 254);
                reset_bottom_button.center = CGPointMake(512,40);
                important_note_label.frame = CGRectMake(0, 0,936,132);
                important_note_label.center= CGPointMake(512,120);
            }
            
        }

        
        
        return footer_view;
    }
    
    
    return nil;
}

-(UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    
    
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setNegativeFormat:@"-¤#,##0.00"];
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [numberFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [numberFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [numberFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [numberFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        
        
        
        
    }
    
    
    
    
    RepaymentCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil)
    {
        cell = [[[RepaymentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    else
    {
        //cell is not nil....
    }
    
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            
            
            
            cell.cellImageView.frame = CGRectMake(0, 0, 680, 40);
            
            if(appDelegate.isIOS7)
            {
                cell.segment31.frame = CGRectMake(470,7, 90, 25);
                cell.segment32.frame = CGRectMake(560, 7,90, 25);
                cell.segment33.frame = CGRectMake(650, 7,90, 25);
                cell.txtValues.frame = CGRectMake(600, 10, 140, 20);
                cell.segment21.frame = CGRectMake(609, 7, 65, 25);
                cell.segment22.frame = CGRectMake(674,7, 65, 25);
            }
            else
            {
                cell.segment31.frame = CGRectMake(370,7, 90, 25);
                cell.segment32.frame = CGRectMake(460, 7,90, 25);
                cell.segment33.frame = CGRectMake(550, 7,90, 25);
                cell.txtValues.frame = CGRectMake(500, 10, 140, 20);
                cell.segment21.frame = CGRectMake(509, 7, 65, 25);
                cell.segment22.frame = CGRectMake(574,7, 65, 25);
            }
            
            
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            cell.cellImageView.frame = CGRectMake(0, 0, 936, 40);
            
            if(appDelegate.isIOS7)
            {
                cell.segment31.frame = CGRectMake(725,7, 90, 25);
                cell.segment32.frame = CGRectMake(815, 7,90, 25);
                cell.segment33.frame = CGRectMake(905, 7,90, 25);
                cell.txtValues.frame = CGRectMake(856, 10, 140, 20);
                cell.segment21.frame = CGRectMake(864, 7, 65, 25);
                cell.segment22.frame = CGRectMake(929,7, 65, 25);
            }
            else
            {
                cell.segment31.frame = CGRectMake(625,7, 90, 25);
                cell.segment32.frame = CGRectMake(715, 7,90, 25);
                cell.segment33.frame = CGRectMake(805, 7,90, 25);
                cell.txtValues.frame = CGRectMake(756, 10, 140, 20);
                cell.segment21.frame = CGRectMake(764, 7, 65, 25);
                cell.segment22.frame = CGRectMake(829,7, 65, 25);
            }
            
        }
    }
    


    
    
    
    
    /*
    if(appDelegate.isIPad)
    {
        cell = [[[RepaymentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    }
    else
    {
        if(cell==nil)
        {
            cell = [[[RepaymentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        
    }
     */
    
    
    
    
    cell.txtValues.hidden = TRUE;
    cell.segment21.hidden = TRUE;
    cell.segment22.hidden = TRUE;
    cell.segment31.hidden = TRUE;
    cell.segment32.hidden = TRUE;
    cell.segment33.hidden = TRUE;
    cell.slider.hidden = TRUE;
    cell.lblPersent.hidden = TRUE;
    
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if (indexPath.section == 0)
    {
        cell.txtValues.delegate = self;
        
        
        cell.lblTitle.text = [arrText objectAtIndex:indexPath.row];
        cell.lblTitle.font = [UIFont systemFontOfSize:15.0f];
        
               
        
        if (indexPath.row == 4)
        {
            [cell.txtValues setHidden:YES];
            
            
            
            
            UIImageView*repaymenttype_imageView = [[UIImageView alloc] init];
            repaymenttype_imageView.frame = CGRectMake(0, 0,40,40);
            repaymenttype_imageView.image = [UIImage imageNamed:@"repayment_type.png"];
            [cell.contentView addSubview:repaymenttype_imageView];
            [repaymenttype_imageView release];

            
            
            
            [cell.segment21 setHidden:NO];
            cell.segment21.tag = 1001;
            
            [cell.segment21 addTarget:self action:@selector(btnSegment2_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.segment22 setHidden:NO];
            
            
            cell.segment22.tag = 1002;
            [cell.segment22 addTarget:self action:@selector(btnSegment2_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
            if ([repaymentType isEqualToString:@"Prin. & inter."])
            {
                indexOfPaymentType = 1;
                [cell.segment21 setImage:[UIImage imageNamed:@"prin_inter_btn_on.png"] forState:UIControlStateNormal];
                [cell.segment22 setImage:[UIImage imageNamed:@"interest_btn_off.png"] forState:UIControlStateNormal];
            }
            else if([repaymentType isEqualToString:@"Interest"])
            {
                repaymentType = @"Interest";
                indexOfPaymentType = 2;
                [cell.segment21 setImage:[UIImage imageNamed:@"prin_inter_btn_off.png"] forState:UIControlStateNormal];
                [cell.segment22 setImage:[UIImage imageNamed:@"interest_btn_on.png"] forState:UIControlStateNormal];
            }

            
            
            
        }
        else if (indexPath.row == 0)
        {
            [cell.txtValues setHidden:NO];
            
            //cell.imageView.image = [UIImage imageNamed:@"loan_amount.png"];
            
            
            UIImageView*loanamount_imageView = [[UIImageView alloc] init];
            loanamount_imageView.frame = CGRectMake(0, 0,40,40);
            loanamount_imageView.image = [UIImage imageNamed:@"loan_amount.png"];
            [cell.contentView addSubview:loanamount_imageView];
            [loanamount_imageView release];
            
            
            nslog(@"\n _amount = %@",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[_amount floatValue]]]);
            
            //cell.txtValues.text = _amount;
            cell.txtValues.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[_amount floatValue]]];
            cell.txtValues.tag = 101+indexPath.row;
        }
        
        else if (indexPath.row == 1)
        {
          //  cell.lblPersent.frame = CGRectMake(280, 10, 20, 20);
          //  cell.txtValues.frame = CGRectMake(150, 10, 120, 20);
        //[cell.lblPersent setHidden:NO];
            
           // cell.imageView.image = [UIImage imageNamed:@"interest_rate.png"];
            
            
            UIImageView*interestrate_imageView = [[UIImageView alloc] init];
            interestrate_imageView.frame = CGRectMake(0, 0,40,40);
            interestrate_imageView.image = [UIImage imageNamed:@"interest_rate.png"];
            [cell.contentView addSubview:interestrate_imageView];
            [interestrate_imageView release];
            
            
            [cell.txtValues setHidden:NO];
            cell.txtValues.text = _rate;
            cell.txtValues.tag = 101+indexPath.row;
            
        }
        else if (indexPath.row == 2)
        {
            
           
            
            
            UIImageView*loanterm_imageView = [[UIImageView alloc] init];
            loanterm_imageView.frame = CGRectMake(0, 0,40,40);
            loanterm_imageView.image = [UIImage imageNamed:@"loan_term.png"];
            [cell.contentView addSubview:loanterm_imageView];
            [loanterm_imageView release];

            
            
            
            cell.txtValues.text = _term;
            cell.slider.value = [_term floatValue]*2;
            [cell.txtValues setEnabled:YES];
            [cell.txtValues setHidden:NO];
            
            cell.txtValues.tag = 101+indexPath.row;
            //[cell.txtValues setFrame:CGRectMake(150, 10, 140, 20)];
            [cell.slider setHidden:YES];
            [cell.slider addTarget:self action:@selector(changeValue1:) forControlEvents:UIControlEventValueChanged];
        }
        else if (indexPath.row == 3)
        {
            
            //cell.imageView.image = [UIImage imageNamed:@"frequency.png"];
            
            
            UIImageView*frequency_imageView = [[UIImageView alloc] init];
            frequency_imageView.frame = CGRectMake(0, 0,40,40);
            frequency_imageView.image = [UIImage imageNamed:@"frequency.png"];
            [cell.contentView addSubview:frequency_imageView];
            [frequency_imageView release];
            
            
            
            if (indexOfRepayment == 1)
            {
                
                if(appDelegate.isIPad)
                {
                    [cell.segment31 setImage:[UIImage imageNamed:@"monthly_btn_on.png"] forState:UIControlStateNormal];
                    [cell.segment32 setImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                    [cell.segment33 setImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
                    
                }
                else
                {
                    [cell.segment31 setImage:[UIImage imageNamed:@"monthly_btn_on_iphone.png"] forState:UIControlStateNormal];
                    [cell.segment32 setImage:[UIImage imageNamed:@"fotnightly_btn_off_iphone.png"] forState:UIControlStateNormal];
                    [cell.segment33 setImage:[UIImage imageNamed:@"weekly_btn_off_iphone.png"] forState:UIControlStateNormal];
                    
                }
                
            }
            else if(indexOfRepayment == 2)
            {
               
                if(appDelegate.isIPad)
                {
                    [cell.segment31 setImage:[UIImage imageNamed:@"monthly_btn_off.png"] forState:UIControlStateNormal];
                    [cell.segment32 setImage:[UIImage imageNamed:@"fotnightly_btn_on.png"] forState:UIControlStateNormal];
                    [cell.segment33 setImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
                    
                }
                else
                {
                    [cell.segment31 setImage:[UIImage imageNamed:@"monthly_btn_off_iphone.png"] forState:UIControlStateNormal];
                    [cell.segment32 setImage:[UIImage imageNamed:@"fotnightly_btn_on_iphone.png"] forState:UIControlStateNormal];
                    [cell.segment33 setImage:[UIImage imageNamed:@"weekly_btn_off_iphone.png"] forState:UIControlStateNormal];
                    
                }
                
                
            }
            else if (indexOfRepayment == 3)
            {
               
                if(appDelegate.isIPad)
                {
                    [cell.segment31 setImage:[UIImage imageNamed:@"monthly_btn_off.png"] forState:UIControlStateNormal];
                    [cell.segment32 setImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                    [cell.segment33 setImage:[UIImage imageNamed:@"weekly_btn_on.png"] forState:UIControlStateNormal];
                    
                }
                else
                {
                    [cell.segment31 setImage:[UIImage imageNamed:@"monthly_btn_off_iphone.png"] forState:UIControlStateNormal];
                    [cell.segment32 setImage:[UIImage imageNamed:@"fotnightly_btn_off_iphone.png"] forState:UIControlStateNormal];
                    [cell.segment33 setImage:[UIImage imageNamed:@"weekly_btn_on_iphone.png"] forState:UIControlStateNormal];
                    
                }
               
            }

            
            
            
            [cell.txtValues setHidden:YES];
            [cell.segment31 setHidden:NO];
            cell.segment31.tag = 3001;
            [cell.segment31 addTarget:self action:@selector(btnSegment3_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.segment32 setHidden:NO];
            cell.segment32.tag = 3002;
            [cell.segment32 addTarget:self action:@selector(btnSegment3_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.segment33 setHidden:NO];
            cell.segment33.tag = 3003;
            [cell.segment33 addTarget:self action:@selector(btnSegment3_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            
            
            
        }
        
    }
    else if (indexPath.section == 1)
    {
        
        /*
        cell.backgroundView = nil;
        CALayer * layer = [cell layer];
        [layer setMasksToBounds:YES];
        [layer setCornerRadius:100.0]; //note that when radius is 0, the border is a rectangle
        [layer setBorderWidth:0.0];
        [layer setBorderColor:[[UIColor whiteColor] CGColor]];
        */
        
        cell.txtValues.textColor = [UIColor whiteColor];
        [cell.txtValues setEnabled:NO];
        [cell.txtValues setHidden:NO];
        [cell.segment21 setHidden:YES];
        [cell.segment22 setHidden:YES];
        [cell.segment31 setHidden:YES];
        [cell.segment32 setHidden:YES];
        [cell.segment33 setHidden:YES];
        [cell.slider setHidden:YES];
        
        cell.lblTitle.frame = CGRectMake(10, 10, 200, 20);

        
        cell.lblTitle.font = [UIFont systemFontOfSize:15.0f];
        cell.txtValues.font = [UIFont boldSystemFontOfSize:15.0f];
        
        cell.lblTitle.textColor =[UIColor whiteColor];
        cell.txtValues.textColor =[UIColor whiteColor];
        
        if (indexPath.row == 0)
        {
            
            cell.lblTitle.tag = 1111;
            cell.txtValues.tag = 5000+indexPath.row;
            cell.lblTitle.text = strRepay;
            
            
           // int temp = (int)ceil([_monthlyRepayment floatValue]);
           // cell.txtValues.text = [NSString stringWithFormat:@"%d",temp];
            
            nslog(@"\n _monthlyRepayment = %@",_monthlyRepayment);
            cell.txtValues.text = _monthlyRepayment;
           
            
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    
                    
                    cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ipad_port_calculation_summary_top_cell_bg.png"]] autorelease];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    
                    cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ipad_land_calculation_summary_top_cell_bg.png"]] autorelease];
                }
            }
            else
            {
                
                cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calculation_summary_top_cell_bg.png"]] autorelease];
            }

            
            
            
        }
        else if (indexPath.row == 1)
        {
            
            cell.txtValues.tag = 5000+indexPath.row;
            cell.lblTitle.text = @"Total loan cost";

            
            /*
            int temp = (int)ceil([_totalCostOfLoan floatValue]);
            cell.txtValues.text = [NSString stringWithFormat:@"%d",temp];
            */
            cell.txtValues.text = _totalCostOfLoan;
           
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    
                    
                    cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ipad_port_calculation_summary_middle_cell_bg.png"]] autorelease];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    
                    cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ipad_land_calculation_summary_middle_cell_bg.png"]] autorelease];
                }
            }
            else
            {
                
                cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calculation_summary_middle_cell_bg.png"]] autorelease];

            }

            
            
        }
        else if (indexPath.row == 2)
        {
            
            cell.txtValues.text = _totalInterestPayable;
            
            /*
            int temp = (int)ceil([_totalInterestPayable floatValue]);
            cell.txtValues.text = [NSString stringWithFormat:@"%d",temp];
            */
            
            cell.txtValues.tag = 5000+indexPath.row;
            cell.lblTitle.text = @"Total interest payable";
            
            
            
            
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    
                    
                    cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ipad_port_calculation_summary_bottom_cell_bg.png"]] autorelease];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    
                    cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ipad_land_calculation_summary_bottom_cell_bg.png"]] autorelease];
                }
            }
            else
            {
                
                cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calculation_summary_bottom_cell_bg.png"]] autorelease];
                
            }

            
            
        }
        
    }
    return cell;
}
-(void)changeValue1:(id)sender
{
    
    UITextField *txtTerms = (UITextField *)[self.view viewWithTag:103];
    UISlider *slider = (UISlider*) sender;
    [slider setThumbImage:[UIImage imageNamed:@"slider_top.png"] forState:UIControlStateHighlighted];
    nslog(@"%@",txtTerms.text);
    double value = roundf(slider.value);
    value = value/2;
    
    
    [toolbar setHidden:YES];
    UITextField *txtAmount = (UITextField *)[self.view viewWithTag:101];
    UITextField *txtRate = (UITextField *)[self.view viewWithTag:102];
    
    
    [txtAmount resignFirstResponder];
    [txtRate resignFirstResponder];
    
    
    txtTerms.text = [NSString stringWithFormat:@"%.1f", value];
    UITextField *txtMonthlyRepayment = (UITextField *)[self.view viewWithTag:5000];
    UITextField *txtTotalCostOfLoan = (UITextField *)[self.view viewWithTag:5001];
    UITextField *txtInterestPayble= (UITextField *)[self.view viewWithTag:5002];
    
    NSString *strAmount = txtAmount.text;
    NSString *strRate = txtRate.text;
    NSString *strTerms = txtTerms.text;
    
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"," withString:@""];
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"" withString:@""];
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@" " withString:@""];
    strRate =  [strRate stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    
    
    double ans = 0.0;
    double amounts = [strAmount doubleValue];
    double rates =  [strRate floatValue];
    double terms = [strTerms floatValue];
    
    if (indexOfPaymentType == 2 && indexOfRepayment == 1)
    {
        ans = (amounts * rates)/1200;
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*12);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        _amount = txtAmount.text;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
    }
    else if (indexOfPaymentType == 2 && indexOfRepayment == 2)
    {
        ans = (amounts * rates)/(2600);
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*26);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        _amount = txtAmount.text;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 2 && indexOfRepayment == 3)
    {
        ans = (amounts * rates)/(5200);
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*52);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        _amount = txtAmount.text;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 1)
    {
        double r = rates/1200;
        double month = terms *12;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*12);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        _amount = txtAmount.text;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 2)
    {
        
        double r = rates/2600;
        double month = terms *26;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*26);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        _amount = txtAmount.text;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 3)
    {
        double r = rates/5200;
        double month = terms *52;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*52);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        _amount = txtAmount.text;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    
    if (amounts == 0 || rates == 0)
    {
        txtMonthlyRepayment.text  = @"0.00";
        txtInterestPayble.text  = @"0.00";
        txtTotalCostOfLoan.text = @"0.00";
        _monthlyRepayment = @"0.00";
        _totalCostOfLoan = @"0.00";
        _totalInterestPayable = @"0.00";
        //        [tableView reloadData];
    }
    [tableView reloadData];
}
-(void)btnSegment3_Clicked:(id)sender
{
    
    UIButton *btn1 = (UIButton*) [self.view viewWithTag:3001];
    UIButton *btn2 = (UIButton*) [self.view viewWithTag:3002];
    UIButton *btn3 = (UIButton*) [self.view viewWithTag:3003];
    
    UILabel *lbl = (UILabel*) [self.view viewWithTag:1111];
    if ([sender tag] == 3001)
    {
        strRepay = @"Monthly repayment";
        repayment = @"Monthly";
        indexOfRepayment = 1;
        
        if(appDelegate.isIPad)
        {
            [btn1 setImage:[UIImage imageNamed:@"monthly_btn_on.png"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            [btn1 setImage:[UIImage imageNamed:@"monthly_btn_on_iphone.png"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"fotnightly_btn_off_iphone.png"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"weekly_btn_off_iphone.png"] forState:UIControlStateNormal];
            
        }
        
        lbl.text = @"Monthly repayment";
    }
    else if([sender tag] == 3002)
    {
        strRepay = @"Fortnightly repayment";
        repayment = @"Fortnightly";
        indexOfRepayment = 2;
        
        if(appDelegate.isIPad)
        {
            [btn1 setImage:[UIImage imageNamed:@"monthly_btn_off.png"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"fotnightly_btn_on.png"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            [btn1 setImage:[UIImage imageNamed:@"monthly_btn_off_iphone.png"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"fotnightly_btn_on_iphone.png"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"weekly_btn_off_iphone.png"] forState:UIControlStateNormal];
            
        }
        
        
        lbl.text = @"Fortnightly repayment";
    }
    else if ([sender tag] == 3003)
    {
        strRepay = @"Weekly repayment";
        repayment = @"Weekly";
        indexOfRepayment = 3;
        
        if(appDelegate.isIPad)
        {
            [btn1 setImage:[UIImage imageNamed:@"monthly_btn_off.png"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"weekly_btn_on.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            [btn1 setImage:[UIImage imageNamed:@"monthly_btn_off_iphone.png"] forState:UIControlStateNormal];
            [btn2 setImage:[UIImage imageNamed:@"fotnightly_btn_off_iphone.png"] forState:UIControlStateNormal];
            [btn3 setImage:[UIImage imageNamed:@"weekly_btn_on_iphone.png"] forState:UIControlStateNormal];
            
        }
        
        
        
        lbl.text = @"Weekly repayment";
    }
    
    [toolbar setHidden:YES];
    UITextField *txtAmount = (UITextField *)[self.view viewWithTag:101];
    UITextField *txtRate = (UITextField *)[self.view viewWithTag:102];
    UITextField *txtTerms = (UITextField *)[self.view viewWithTag:103];
    nslog(@"\n txtTerms == %@ \n",txtTerms);
    
    [txtAmount resignFirstResponder];
    [txtRate resignFirstResponder];
    
    
    UITextField *txtMonthlyRepayment = (UITextField *)[self.view viewWithTag:5000];
    UITextField *txtTotalCostOfLoan = (UITextField *)[self.view viewWithTag:5001];
    UITextField *txtInterestPayble= (UITextField *)[self.view viewWithTag:5002];
    
    NSString *strAmount = txtAmount.text;
    NSString *strRate = txtRate.text;
    NSString *strTerms = txtTerms.text;
    
    nslog(@"strAmount == %@",strAmount);
    nslog(@"strRate == %@",strRate);
    nslog(@"stramountTemp == %@",stramountTemp);
    nslog(@"strrateTemp == %@",strrateTemp);
    
    
   
    
    
    strAmount = [stramountTemp stringByReplacingOccurrencesOfString:@"," withString:@""];
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"" withString:@""];
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@" " withString:@""];
    strRate =  [strrateTemp stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    nslog(@"strAmount === %@",strAmount);
    nslog(@"strRate === %@",strRate);
    
    double ans = 0.0;
    double amounts = [strAmount doubleValue];
    double rates =  [strRate floatValue];
    double terms = [strTerms floatValue];
    
    nslog(@"amounts == %f",amounts);
    nslog(@"rates == %f",rates);
    
    if (indexOfPaymentType == 2 && indexOfRepayment == 1)
    {
        ans = (amounts * rates)/1200;
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*12);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 2 && indexOfRepayment == 2)
    {
        ans = (amounts * rates)/(2600);
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*26);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 2 && indexOfRepayment == 3)
    {
        ans = (amounts * rates)/(5200);
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*52);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 1)
    {
        double r = rates/1200;
        double month = terms *12;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*12);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        
        
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 2)
    {
        
        double r = rates/2600;
        double month = terms *26;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*26);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 3)
    {
        double r = rates/5200;
        double month = terms *52;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*52);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
               
        _amount = stramountTemp;
        [_amount retain];
        _rate = strrateTemp;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    
    if (amounts == 0 || rates == 0)
    {
        _monthlyRepayment = @"0.00";
        _totalCostOfLoan = @"0.00";
        _totalInterestPayable = @"0.00";
        
    }
    
    
   
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
   
    
    
    
    
    /*
    int temp = (int)ceil([_totalCostOfLoan floatValue]);
    _totalCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:temp]];
    [_totalCostOfLoan retain];
    */

    
    float temp = (float)[_totalCostOfLoan floatValue];
    _totalCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_totalCostOfLoan retain];

    
    temp = (float)[_totalInterestPayable floatValue];
    _totalInterestPayable = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_totalInterestPayable retain];
    
    temp = (float)[_monthlyRepayment floatValue];
    _monthlyRepayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_monthlyRepayment retain];

    
    
    
        
    
    [tableView reloadData];
}


-(void)btnSegment2_Clicked:(id)sender
{
    UIButton *btn1 = (UIButton*) [self.view viewWithTag:1001];
    UIButton *btn2 = (UIButton*) [self.view viewWithTag:1002];
    if ([sender tag] == 1001)
    {
        repaymentType = @"Prin. & inter.";
        
        indexOfPaymentType = 1;
        [btn1 setImage:[UIImage imageNamed:@"prin_inter_btn_on.png"] forState:UIControlStateNormal];
        [btn2 setImage:[UIImage imageNamed:@"interest_btn_off.png"] forState:UIControlStateNormal];
    }
    else
    {
        repaymentType = @"Interest";
        indexOfPaymentType = 2;
        [btn1 setImage:[UIImage imageNamed:@"prin_inter_btn_off.png"] forState:UIControlStateNormal];
        [btn2 setImage:[UIImage imageNamed:@"interest_btn_on.png"] forState:UIControlStateNormal];
    }
    
    [toolbar setHidden:YES];
    UITextField *txtAmount = (UITextField *)[self.view viewWithTag:101];
    UITextField *txtRate = (UITextField *)[self.view viewWithTag:102];
    UITextField *txtTerms = (UITextField *)[self.view viewWithTag:103];
    
    [txtAmount resignFirstResponder];
    [txtRate resignFirstResponder];
    UITextField *txtMonthlyRepayment = (UITextField *)[self.view viewWithTag:5000];
    UITextField *txtTotalCostOfLoan = (UITextField *)[self.view viewWithTag:5001];
    UITextField *txtInterestPayble= (UITextField *)[self.view viewWithTag:5002];
    
    NSString *strAmount = txtAmount.text;
    NSString *strRate = txtRate.text;
    NSString *strTerms = txtTerms.text;
    
    strAmount = [stramountTemp stringByReplacingOccurrencesOfString:@"," withString:@""];
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"" withString:@""];
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@" " withString:@""];
    strRate =  [strrateTemp stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    
    
    
    double ans = 0.0;
    double amounts = [strAmount doubleValue];
    double rates =  [strRate floatValue];
    double terms = [strTerms floatValue];
    
    nslog(@"amounts in segemtn_2 == %f",amounts);
    nslog(@"rates in segemtn_2 == %f",rates);
    
    if (indexOfPaymentType == 2 && indexOfRepayment == 1)
    {
        ans = (amounts * rates)/1200;
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*12);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
       
        _amount = stramountTemp;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
    }
    else if (indexOfPaymentType == 2 && indexOfRepayment == 2)
    {
        ans = (amounts * rates)/(2600);
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*26);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        _amount = stramountTemp;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 2 && indexOfRepayment == 3)
    {
        ans = (amounts * rates)/(5200);
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans *(terms*52);
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans +amounts;
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 1)
    {
        double r = rates/1200;
        double month = terms *12;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*12);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 2)
    {
        
        double r = rates/2600;
        double month = terms *26;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*26);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    else if (indexOfPaymentType == 1 && indexOfRepayment == 3)
    {
        double r = rates/5200;
        double month = terms *52;
        double x = 1+r;
        double b = pow(x, month) -1;
        ans = (r +(r/b)) *amounts;
        
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"%.2f",ans];
        ans = ans *(terms*52);
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"%.2f",ans];
        
        ans = ans - amounts;
        txtInterestPayble.text = [NSString stringWithFormat:@"%.2f",ans];
        
        _totalCostOfLoan = txtTotalCostOfLoan.text;
        [_totalCostOfLoan retain];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        _totalInterestPayable = txtInterestPayble.text;
        [_totalInterestPayable retain];
        
        _amount = stramountTemp;
        [_amount retain];
        _rate = txtRate.text;
        [_rate retain];
        _term = txtTerms.text;
        [_term retain];
        
    }
    
    if (amounts == 0 || rates == 0)
    {
        _monthlyRepayment = @"0.00";
        _totalCostOfLoan = @"0.00";
        _totalInterestPayable = @"0.00";
        
    }
    
    
    
    
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    nslog(@"\n montly = %@",_monthlyRepayment);
    
    
    
    float temp = (float)[_totalCostOfLoan floatValue];
    _totalCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_totalCostOfLoan retain];
    
    temp = (float)[_totalInterestPayable floatValue];
    _totalInterestPayable = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_totalInterestPayable retain];
    
    temp = (float)[_monthlyRepayment floatValue];
    _monthlyRepayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_monthlyRepayment retain];
 
    
    
    [tableView reloadData];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL istag = FALSE;
    if (textField.tag == 101 || textField.tag == 102 || textField.tag == 103)
    {
        [textField setKeyboardType:UIKeyboardTypeNumberPad];
        istag = TRUE;
    }
    
    if(textField.tag == 103)
    {
        NSIndexPath *index = [NSIndexPath indexPathForRow:2 inSection:0];
        [tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:TRUE];
    }
    current_tag = textField.tag;
    return istag;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [toolbar setHidden:NO];
    
    if(appDelegate.isIPad)
    {
        
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                toolbar.frame = CGRectMake(0,672,768,44);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                toolbar.frame = CGRectMake(0,328,1024,44);
            }
        
        
    }

    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag == 101)
    {
        
        NSString *cleanCentString = [[textField.text
                                      componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                     componentsJoinedByString:@""];
        
        
        
        // Parse final integer value
        
        nslog(@"\n cleanCentString = %@",cleanCentString);
        

        
        
        
        
        
        
        
         stramountTemp = [NSString stringWithFormat:@"%f",[cleanCentString floatValue]/100];
        
        nslog(@"\n stramountTemp = %@",stramountTemp);
        
        [stramountTemp retain];
    }
    if(textField.tag == 102)
    {
        
        strrateTemp = textField.text;
        nslog(@"\n strrateTemp = %@",strrateTemp);
        [strrateTemp retain];//ios 7 crashign if not retain..
        
    }
    if(textField.tag == 103)
    {
        strtermTemp = textField.text;
    }
    [strtermTemp retain];
    nslog(@"stramountTemp at end editing == %@",stramountTemp);
    nslog(@"strrateTemp at end editing == %@",strrateTemp);
    nslog(@"strtermTemp at end editing == %@",strtermTemp);
    
    
    [toolbar setHidden:YES];
    [textField resignFirstResponder];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    nslog(@"\n tag = %d",textField.tag);
    
    if(textField.tag ==101)
    {
        NSDecimalNumber *someAmount = [NSDecimalNumber decimalNumberWithString:@"5.00"];
        
        
        
        nslog(@"%@", [currencyFormatter stringFromNumber:someAmount]);
        
        if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
        {
            [currencyFormatter setCurrencyCode:appDelegate.curCode];
            
            if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
            {
                [currencyFormatter setCurrencyCode:@"USD"];
                nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                [currencyFormatter setLocale:locale];
                
            }
            else if([[currencyFormatter currencyCode] isEqualToString:@"CNY"])
            {
                [currencyFormatter setCurrencyCode:@"CNY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                [currencyFormatter setLocale:locale];
                [locale release];
                
            }
            
            else if([[currencyFormatter currencyCode] isEqualToString:@"JPY"])
            {
                [currencyFormatter setCurrencyCode:@"JPY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                [currencyFormatter setLocale:locale];
                [locale release];
                
            }
            
        }
        [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        
        
        int currencyScale = [currencyFormatter maximumFractionDigits];
        
        nslog(@"\n scale = %d",currencyScale);
        
        if(currencyScale == 0)
        {
            
            NSString *cleanCentString = [[textField.text
                                          componentsSeparatedByCharactersInSet:
                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                         componentsJoinedByString:@""];
            
            
            
            
            double centAmount = [cleanCentString doubleValue];
            // Check the user input
            
            if (string.length > 0)
            {
                // Digit added
                centAmount = centAmount * 10 + [string doubleValue];
            }
            else
            {
                // Digit deleted
                centAmount = centAmount / 10;
            }
            // Update call amount value
            NSNumber *_amount;
            //_amount = [[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f];
            _amount = [[NSNumber alloc]initWithInt:(int)centAmount];
            
            
            
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
            {
                [_currencyFormatter setCurrencyCode:appDelegate.curCode];
                
                
                if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
                {
                    [_currencyFormatter setCurrencyCode:@"USD"];
                    nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                    [_currencyFormatter setLocale:locale];
                    
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
                {
                    [_currencyFormatter setCurrencyCode:@"CNY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
                {
                    [_currencyFormatter setCurrencyCode:@"JPY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                
                
            }
            else
            {
                [_currencyFormatter setCurrencyCode:[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
            }
            [_currencyFormatter setNegativeFormat:@"-¤#,##0.00"];
            
            textField.text = [_currencyFormatter stringFromNumber:_amount];
            
        }
        else
        {
            NSString *cleanCentString = [[textField.text
                                          componentsSeparatedByCharactersInSet:
                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                         componentsJoinedByString:@""];
            // Parse final integer value
            
            double centAmount = [cleanCentString doubleValue];
            // Check the user input
            if (string.length > 0)
            {
                // Digit added
                centAmount = centAmount * 10 + [string doubleValue];
            }
            else
            {
                // Digit deleted
                centAmount = centAmount / 10;
            }
            // Update call amount value
            NSNumber *_amount;
            _amount = [[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f];
            //    _amount = [[NSNumber alloc] initWithFloat:(float)centAmount / 100.0f];
            // Write amount with currency symbols to the textfield
            
            
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
            {
                [_currencyFormatter setCurrencyCode:appDelegate.curCode];
                
                
                if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
                {
                    [_currencyFormatter setCurrencyCode:@"USD"];
                    nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                    [_currencyFormatter setLocale:locale];
                    
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
                {
                    [_currencyFormatter setCurrencyCode:@"CNY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                
                else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
                {
                    [_currencyFormatter setCurrencyCode:@"JPY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                
                
            }
            textField.text = [_currencyFormatter stringFromNumber:_amount];
            
        }

    }
    
    
    else
    {
        
        
        NSString* clean_string = [[textField.text
                                   componentsSeparatedByCharactersInSet:
                                   [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                  componentsJoinedByString:@""];
        
        nslog(@"\n clean_string = %@",clean_string);
        
        double centAmount = [clean_string doubleValue];
        // Check the user input
        if (string.length > 0)
        {
            // Digit added
            centAmount = centAmount * 10 + [string doubleValue];
        }
        else
        {
            // Digit deleted
            centAmount = centAmount / 10;
        }
        // Update call amount value
        NSNumber *_amount_local;
        _amount_local = [[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f];
        
        
        nslog(@"\n _amount_local = %@",_amount_local);
        

        
        [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
        {
            [_currencyFormatter setCurrencyCode:appDelegate.curCode];
            
            
            if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
            {
                [_currencyFormatter setCurrencyCode:@"USD"];
                nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                [_currencyFormatter setLocale:locale];
                [locale release];
                
            }
            else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
            {
                [_currencyFormatter setCurrencyCode:@"CNY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                [_currencyFormatter setLocale:locale];
                [locale release];
                
            }
            else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
            {
                [_currencyFormatter setCurrencyCode:@"JPY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                [_currencyFormatter setLocale:locale];
                [locale release];
                
            }
            else if([[_currencyFormatter currencyCode] isEqualToString:@"NAD"])
            {
                [_currencyFormatter setCurrencyCode:@"NAD"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
                [_currencyFormatter setLocale:locale];
                [locale release];
                
            }
            
            
        }
        textField.text = [_currencyFormatter stringFromNumber:_amount_local];

        

        
        
        
        textField.text = [[textField.text componentsSeparatedByCharactersInSet:[NSCharacterSet  symbolCharacterSet]] componentsJoinedByString:@""];
        
        textField.text = [[textField.text componentsSeparatedByCharactersInSet:[NSCharacterSet  letterCharacterSet]] componentsJoinedByString:@""];
        
        
        
        
        
        // [NSString stringWithFormat:@"%@",_amount_local];
    }
    
    /*
    else
    {
        
        
        
        
        
        NSMutableCharacterSet *numberSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
        [numberSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
        NSCharacterSet *nonNumberSet = [numberSet invertedSet];
        
        BOOL result = NO;
        
        if([string length] == 0)
        {
            result = YES;
        }
        else
        {
            if([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0){
                result = YES;
            }
        }
        
        if(result){
            NSMutableString* mstring = [[textField text] mutableCopy];
            
            if([string length] > 0){
                [mstring insertString:string atIndex:range.location];
            }
            else
            {
                [mstring deleteCharactersInRange:range];
            }
            
            NSLocale* locale = [NSLocale currentLocale];
            NSString *localCurrencySymbol = [locale objectForKey:NSLocaleCurrencySymbol];
            NSString *localGroupingSeparator = [locale objectForKey:NSLocaleGroupingSeparator];
            
            NSString* clean_string = [[mstring stringByReplacingOccurrencesOfString:localGroupingSeparator
                                                                         withString:@""]
                                      stringByReplacingOccurrencesOfString:localCurrencySymbol
                                      withString:@""];
            
            if([[Formatters currencyFormatter] maximumFractionDigits] > 0){
                NSMutableString *mutableCleanString = [clean_string mutableCopy];
                
                if([string length] > 0){
                    NSRange theRange = [mutableCleanString rangeOfString:@"."];
                    [mutableCleanString deleteCharactersInRange:theRange];
                    [mutableCleanString insertString:@"." atIndex:(theRange.location + 1)];
                    clean_string = mutableCleanString;
                }
                else {
                    [mutableCleanString insertString:@"0" atIndex:0];
                    NSRange theRange = [mutableCleanString rangeOfString:@"."];
                    [mutableCleanString deleteCharactersInRange:theRange];
                    [mutableCleanString insertString:@"." atIndex:(theRange.location - 1)];
                    clean_string = mutableCleanString;
                }
            }
            
            NSNumber* number = [[Formatters basicFormatter] numberFromString: clean_string];
            NSMutableString *numberString = [[[Formatters currencyFormatter] stringFromNumber:number] mutableCopy];
            [numberString deleteCharactersInRange:NSMakeRange(0, 1)];
            
            [textField setText:numberString];
            if (textField.tag == 101)
            {
                textField.text = [@"" stringByAppendingString:textField.text];
            }
            
            
        }
        
        
        
        
        

    }
      */  

    
    
    
    
    /*
    
    
    NSMutableCharacterSet *numberSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
    [numberSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
    NSCharacterSet *nonNumberSet = [numberSet invertedSet];
    
    BOOL result = NO;
    
    if([string length] == 0)
    {
        result = YES;
    }
    else
    {
        if([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0){
            result = YES;
        }
    }
    
    if(result){
        NSMutableString* mstring = [[textField text] mutableCopy];
        
        if([string length] > 0){
            [mstring insertString:string atIndex:range.location];
        }
        else {
            [mstring deleteCharactersInRange:range];
        }
        
        NSLocale* locale = [NSLocale currentLocale];
        NSString *localCurrencySymbol = [locale objectForKey:NSLocaleCurrencySymbol];
        NSString *localGroupingSeparator = [locale objectForKey:NSLocaleGroupingSeparator];
        
        NSString* clean_string = [[mstring stringByReplacingOccurrencesOfString:localGroupingSeparator
                                                                     withString:@""]
                                  stringByReplacingOccurrencesOfString:localCurrencySymbol
                                  withString:@""];
        
        if([[Formatters currencyFormatter] maximumFractionDigits] > 0){
            NSMutableString *mutableCleanString = [clean_string mutableCopy];
            
            if([string length] > 0){
                NSRange theRange = [mutableCleanString rangeOfString:@"."];
                [mutableCleanString deleteCharactersInRange:theRange];
                [mutableCleanString insertString:@"." atIndex:(theRange.location + 1)];
                clean_string = mutableCleanString;
            }
            else {
                [mutableCleanString insertString:@"0" atIndex:0];
                NSRange theRange = [mutableCleanString rangeOfString:@"."];
                [mutableCleanString deleteCharactersInRange:theRange];
                [mutableCleanString insertString:@"." atIndex:(theRange.location - 1)];
                clean_string = mutableCleanString;
            }
        }
        
        NSNumber* number = [[Formatters basicFormatter] numberFromString: clean_string];
        NSMutableString *numberString = [[[Formatters currencyFormatter] stringFromNumber:number] mutableCopy];
        [numberString deleteCharactersInRange:NSMakeRange(0, 1)];
        
        [textField setText:numberString];
        if (textField.tag == 101)
        {
            textField.text = [@"" stringByAppendingString:textField.text];
        }
        
        
    }
    */
    
    return NO; // we return NO because we have manually edited the textField contents.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UITextField *txtAmount = (UITextField *)[self.view viewWithTag:101];
    
    if([[Formatters currencyFormatter] maximumFractionDigits] > 0)
    {
        [txtAmount setText:_amount];
    } else
    {
        [txtAmount setText:_amount];
    }
    UITextField *txtRate = (UITextField *)[self.view viewWithTag:102];
    
    if([[Formatters currencyFormatter] maximumFractionDigits] > 0)
    {
        [txtRate setText:_rate];
    }
    else
    {
        
        [txtRate setText:_rate];
    }
}

#pragma mark - back_clicked
#pragma mark

-(void)back_clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}




@end
