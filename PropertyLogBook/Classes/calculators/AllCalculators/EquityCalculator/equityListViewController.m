//
//  equityListViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "equityListViewController.h"
#import "PropertyLogBookAppDelegate.h"
#import "EquityViewController.h"
#import "UIImage+Resize.h"

@implementation equityListViewController



- (void)dealloc
{
    MemoryLog(@"\n dealloc in = %@ \n",NSStringFromClass([self class]));
    [super dealloc];
    [tblView release];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        [tblView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
        
    }

    
    
    //tableview height in textfieldend editing etc…
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        
    }


    
    
    
    self.navigationItem.title = @"EQUITY";
    
    dtFormatterTo =[[NSDateFormatter alloc]init];
    [dtFormatterTo setDateFormat:@"dd MMM YYYY"];
    
    df =[[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd"];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }
    
    
    
    
    tblView.backgroundColor = [UIColor clearColor];
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)cancel_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    
    //[self.view addSubview:appDelegate.bottomView];
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    if([appDelegate.propertyDetailArray count]==0)
    {
        [appDelegate selectPropertyDetail];
    }
    
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
        [appDelegate selectCurrencyIndex];
    }
    
    if(appDelegate.isIPad)
    {
    
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
    
    [tblView reloadData];
    
    
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
        [tblView reloadData];
    }
    
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section >0)
    {
        if ([appDelegate.propertyDetailArray count]>0)
        {
            return @"Update Equity of your property by clicking on the Property";
        }
        else
        {
            return @"";
        }
    }
    return @"";
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if(appDelegate.isIPad)
        {
            return 90;
        }
        return 60;
    }
	return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section..
    noLabel.hidden = TRUE;
    noDataImage.hidden = TRUE;
    if (section == 1)
    {
        if ([appDelegate.propertyDetailArray count]>0)
        {
            return [appDelegate.propertyDetailArray count];
        }
    }
    else if (section == 0)
    {
        if ([appDelegate.propertyDetailArray count]>0)
        {
            return 1;
        }
    }
    
    if(appDelegate.isIPad)
    {
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
            {
                noLabel.center = CGPointMake(384,self.view.frame.size.height/2);
                noDataImage.center = CGPointMake(384,(self.view.frame.size.height/2-noDataImage.frame.size.height));
            }
            else
            {
                noLabel.center = CGPointMake(512,self.view.frame.size.height/2);
                noDataImage.center = CGPointMake(512,(self.view.frame.size.height/2-noDataImage.frame.size.height));
            }
            
        }
        
    }
    noLabel.hidden = FALSE;
    noDataImage.hidden = FALSE;
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *label, *detailLabel, *dateLabel;
    UIImageView *cellImg;
    NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setGeneratesDecimalNumbers:NO];
    //[numberFormatter setAllowsFloats:NO];
    [numberFormatter setRoundingMode:NSNumberFormatterRoundUp];
    [numberFormatter setNegativeFormat:@"-¤#,##0.00"];
    
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
    
        [appDelegate selectCurrencyIndex];
    }
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [numberFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [numberFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [numberFormatter setLocale:locale];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [numberFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [numberFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        
        
        
        
    }
    if (indexPath.section == 1)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        
        //if(1)
        if(cell==nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
            if(appDelegate.isIOS7)
            {
                [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
            }

            
            cellImg = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
            cellImg.tag = 6000;
            [cell.contentView addSubview:cellImg];
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    label = [[UILabel alloc]initWithFrame:CGRectMake(50, 5, 320, 30)];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    label = [[UILabel alloc]initWithFrame:CGRectMake(50, 5, 320, 30)];
                }
                
            }
            else
            {
                label = [[UILabel alloc]initWithFrame:CGRectMake(50, 5, 120, 30)];
            }
            label.tag = 400;
            label.numberOfLines = 0;
            label.lineBreakMode = UILineBreakModeWordWrap;
            [label setFont:[UIFont systemFontOfSize:15.0]];
            [label setBackgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:label];
            
            dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 25, 200, 30)];
            dateLabel.tag = 600;
            [dateLabel setFont:[UIFont systemFontOfSize:13.0]];
            
            [dateLabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];
            dateLabel.numberOfLines = 0;
            dateLabel.lineBreakMode = UILineBreakModeWordWrap;
            [dateLabel setBackgroundColor:[UIColor clearColor]];
            
            [cell.contentView addSubview:dateLabel];
            
            [dateLabel release];
            
             detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(172, 10, 105, 30)];
            
            if(appDelegate.isIPad)
            {
                if(appDelegate.isIOS7)
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                    {
                        detailLabel.frame = CGRectMake(440, 10, 285, 30);
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                    {
                        detailLabel.frame = CGRectMake(440, 10, 530, 30);
                    }
                }
                else
                {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    detailLabel.frame = CGRectMake(440, 10, 205, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    detailLabel.frame = CGRectMake(440, 10, 430, 30);
                }
                }
                
                
            }
            
            
            detailLabel.tag = 500;
            //lbl.text = @"Select Mesurement:";
            detailLabel.numberOfLines = 0;
            [detailLabel setTextAlignment:UITextAlignmentRight];
            [detailLabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];
            detailLabel.lineBreakMode = UILineBreakModeTailTruncation;
            [detailLabel setFont:[UIFont systemFontOfSize:15.0]];
            [detailLabel setBackgroundColor:[UIColor clearColor]];
            
            [cell.contentView addSubview:detailLabel];
            
            [detailLabel release];
            
        }
        else
        {
            label = (UILabel *)[cell.contentView viewWithTag:400];
            detailLabel = (UILabel *)[cell.contentView viewWithTag:500];
            dateLabel = (UILabel *)[cell.contentView viewWithTag:600];
            cellImg = (UIImageView *)[cell.contentView viewWithTag:6000];
            
            
        }
        
        
        if(appDelegate.isIPad)
        {
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    detailLabel.frame = CGRectMake(440, 10, 285, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    detailLabel.frame = CGRectMake(440, 10, 530, 30);
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    detailLabel.frame = CGRectMake(440, 10, 205, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    detailLabel.frame = CGRectMake(440, 10, 430, 30);
                }
            }
            
            
        }
        

        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        // Configure the cell...
        
        label.text = [[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"0"];
        
        
        if ([[[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"imageCount"]intValue]==1)
        {
            cellImg.image = [[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"image"];
        }
        else
        {
            cellImg.image = [UIImage imageNamed:@"new-cell-property.png"];
            
        }
        
        
        
        
        if(appDelegate.decimalScale >0)
        {
            
            @try
            {
                
                NSString *numberString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"zEquity"]doubleValue]]];
                numberString = [numberString substringToIndex:[numberString length]-3];
                detailLabel.text = numberString;
                
            }
            @catch (NSException *exception)
            {
                
            }
            @finally
            {
                
            }
        }
        
        
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"])
        {
            NSString *numberString =  [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"zEquity"]doubleValue]]];
            detailLabel.text = numberString;
            
        }
        
        /*
         NSDate *tempDate = [df dateFromString:[[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"zDate"]];
         
         */
        
        nslog(@"\n propertyDetailArray in equityList.. = %@",appDelegate.propertyDetailArray);
        nslog(@"\n date1 in appDelegate = %@",[[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"zDate"]);
        NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
        [dtFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDate *date = [dtFormatter dateFromString:[[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"zDate"]];
        nslog(@"\n date using dateFormatter = %@",date);
        NSString* dateString = [appDelegate.regionDateFormatter stringFromDate:date];
        nslog(@"\n date1 = %@",dateString);
        
        
        dateLabel.text = dateString;
        
        
        float equ = [[[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"zEquity"]floatValue];
        if (equ >= 0 )
        {
            detailLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
        else
        {
            detailLabel.textColor = [UIColor redColor];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 0)
    {
        static NSString *CellIdentifier1 = @"Cell1";
        UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        
        //if (cell1 == nil)
        if (1)
        {
            cell1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier1] autorelease];
            cellImg = [[UIImageView alloc]initWithFrame:CGRectMake(10, 25, 30, 30)];
            cellImg.tag = 7000;
            [cell1.contentView addSubview:cellImg];
            cell1.backgroundColor = [UIColor clearColor];
            
            
            if(appDelegate.isIOS7)
            {
                [cell1 setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
            }

            
            label = [[UILabel alloc]initWithFrame:CGRectMake(50, 25, 120, 30)];
            
            if(!appDelegate.isIPad)
            {
                label.frame = CGRectMake(50,18, 120, 30);
            }
            
            label.tag = 4000;
            //lbl.text = @"Select Mesurement:";
            label.numberOfLines = 0;
            label.lineBreakMode = UILineBreakModeWordWrap;
            [label setFont:[UIFont boldSystemFontOfSize:15.0]];
            [label setBackgroundColor:[UIColor clearColor]];
            
            [cell1.contentView addSubview:label];
            
            
            
            
            
            if(appDelegate.isIPad)
            {
                
                if(appDelegate.isIOS7)
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                    {
                        detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(440,46, 285, 30)];
                        label.frame = CGRectMake(50,46, 120, 30);
                        
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                    {
                        detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(440, 46, 530, 30)];
                        label.frame  = CGRectMake(50, 46, 120, 30);
                    }
                }
                else
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                    {
                        detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(440,46, 205, 30)];
                        label.frame = CGRectMake(50,46, 120, 30);

                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                    {
                        detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(440, 46, 430, 30)];
                        label.frame  = CGRectMake(50, 46, 120, 30);
                    }
                }
                
                
            }
            else
            {
                detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(172, 18, 105, 30)];
            }
            
            
            detailLabel.tag = 5000;
            //lbl.text = @"Select Mesurement:";
            detailLabel.numberOfLines = 0;
            [detailLabel setTextAlignment:UITextAlignmentRight];
            [detailLabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];
            detailLabel.lineBreakMode = UILineBreakModeTailTruncation;
            [detailLabel setFont:[UIFont boldSystemFontOfSize:15.0]];
            [detailLabel setBackgroundColor:[UIColor clearColor]];
            
            [cell1.contentView addSubview:detailLabel];
            
            [detailLabel release];
        }
        else
        {
            label = (UILabel *)[cell1.contentView viewWithTag:4000];
            detailLabel = (UILabel *)[cell1.contentView viewWithTag:5000];
            
            cellImg = (UIImageView *)[cell1.contentView viewWithTag:7000];
        }
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation ==1 || [UIApplication sharedApplication].statusBarOrientation ==2 )
            {
                
                cell1.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipad_port_bar_white.png"]];
            }
            else if([UIApplication sharedApplication].statusBarOrientation ==3 || [UIApplication sharedApplication].statusBarOrientation ==4 )
            {
                cell1.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipad_land_bar_white.png"]];
            }
        }
        else
        {
            cell1.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bar_white.png"]];
        }
        
        
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        label.text = @"Net Equity:";
        // cellImg.image = [UIImage imageNamed:@"net_equity_30.png"];
        // cellImg.image = [UIImage imageNamed:@"equity__calculator_icon.png"];
        
        
        UIImageView*frequency_imageView = [[UIImageView alloc] init];
        
        if(appDelegate.isIPad)
        {
            frequency_imageView.frame = CGRectMake(2,36,50,50);
        }
        else
        {
            frequency_imageView.frame = CGRectMake(2,7,50,50);
        }
        
        
        frequency_imageView.image = [UIImage imageNamed:@"equity__calculator_icon.png"];
        [cell1.contentView addSubview:frequency_imageView];
        [frequency_imageView release];
        
        
        /*
         double currency = [Amount1.text doubleValue] + [Amount2.text doubleValue];
         NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
         [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
         NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithInt:currency]];
         SumCurrency.text = [NSString stringWithFormat:@"Converted:%@",numberAsString];
         */
        
        float netEquity=0;
        for (int i = 0; i<[appDelegate.propertyDetailArray count];i++)
        {
            netEquity = netEquity+[[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"zEquity"]intValue];
            
        }
        NSString *numberString = [numberFormatter stringFromNumber:[NSNumber numberWithInt:netEquity]];
        numberString = [numberString substringToIndex:[numberString length]-3];
        detailLabel.text = numberString;
        
        if (netEquity >= 0)
        {
            detailLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
        else
        {
            detailLabel.textColor = [UIColor redColor];
        }
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell1;
    }
    return nil;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        EquityViewController *equityView = [[EquityViewController alloc]initWithNibName:@"EquityViewController" bundle:nil];
        
        equityView.isIndex = indexPath.row;
        
        
        [self.navigationController pushViewController:equityView animated:YES];
        
        [equityView release];
    }
    
}



@end
