//
//  equityListViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PropertyLogBookAppDelegate;
@interface equityListViewController : UIViewController {
    
    IBOutlet UITableView *tblView;
    PropertyLogBookAppDelegate *appDelegate;
    
    IBOutlet UIImageView*noDataImage;
    IBOutlet UILabel *noLabel;
    NSDateFormatter *dtFormatterTo;
    NSDateFormatter *df;
}

@end
