//
//  EquityViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EquityViewController.h"
#import "PropertyLogBookAppDelegate.h"
#import "propertyViewController.h"
@implementation EquityViewController
@synthesize isIndex;
@synthesize loanValue,estimatedValue,equityValue;
#pragma mark -
#pragma mark View lifecycle

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationItem.title = @"EQUITY";
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    tblView.backgroundColor = [UIColor clearColor];
    
    
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        [tblView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
        
    }

    
    //tableview height in textfieldend editing etc…
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        
    }

    
    if(!appDelegate.isIPad)
    {
        if(appDelegate.isIphone5)
        {
            toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,244, 320, 44)];
        }
        else
        {
            toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 156, 320, 44)];
        }
        
    }
    else
    {
        toolBar = [[UIToolbar alloc]init];
    }
    
    
    
    
    currencyFormatter = [[NSNumberFormatter alloc] init];/*If we keep autorelease,or release in dealloc app got crashed.*/
    
    _currencyFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter = [[NSNumberFormatter alloc] init];
    
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        UIButton * update_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [update_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        update_button.bounds = CGRectMake(0, 0,58.0,30.0);
        [update_button setBackgroundImage:[UIImage imageNamed:@"equity_update_button.png"] forState:UIControlStateNormal];
        [update_button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:update_button] autorelease];
        
        UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator1.width = -12;
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        
        UIButton * update_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [update_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        update_button.bounds = CGRectMake(0, 0,58.0,30.0);
        [update_button setBackgroundImage:[UIImage imageNamed:@"equity_update_button.png"] forState:UIControlStateNormal];
        [update_button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:update_button] autorelease];
        
    }
    
    
    
    
    /*
     self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithTitle:@"Update" style:UIBarButtonItemStyleDone target:self action:@selector(cancel_clicked)] autorelease];
     */
    
    
	
	equityMainArray = [[NSMutableArray alloc]initWithObjects:@"Property",@"Purchased Price", @"Market Value",@"Loan Outstanding",@"Equity",nil];
    appDelegate.equityIndex = 0;
    
    if([appDelegate.propertyDetailArray count]==0)
    {
        [appDelegate selectPropertyDetail];
    }
    
    
    
    if ([appDelegate.propertyDetailArray count]>0)
    {
        appDelegate.incomeProperty = [[appDelegate.propertyDetailArray objectAtIndex:appDelegate.equityIndex]valueForKey:@"0"];
    }
    
	
    tblView.backgroundColor = [UIColor clearColor];
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
	
}

-(void)back_clicked
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)nav_done
{
    
    
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    
    nslog(@"\n\n\n nav_done => [numberFormatter currencyCode] = %@",[numberFormatter currencyCode]);
    nslog(@"\n\n\n nav_done => [numberFormatter locale].localeIdentifier = %@",[numberFormatter locale].localeIdentifier);
    nslog(@"\n\n\n nav_done => curCode = %@",appDelegate.curCode);
    
    for (int i=2000;i<2005;i++)
    {
        UITextField *textfield = (UITextField *)[self.view viewWithTag:i];
        if (textfield.tag == 2001)
        {
            
            
            
            
            
            textfield.text = [textfield.text stringByReplacingOccurrencesOfString:@"%20" withString:@""];
            
            
            //NSString *amountStr = textField.text;
            
            NSString *textFieldStr = [NSString stringWithFormat:@"%@",textfield.text];
            
            purchaseValue = [NSMutableString stringWithString:textFieldStr];
            
            //NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            
            
            [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            
            
            
            
            [purchaseValue replaceOccurrencesOfString:numberFormatter.currencySymbol
                                       withString:@""
                                          options:NSLiteralSearch
                                            range:NSMakeRange(0, [purchaseValue length])];
            
            [purchaseValue replaceOccurrencesOfString:numberFormatter.groupingSeparator
                                       withString:@""
                                          options:NSLiteralSearch
                                            range:NSMakeRange(0, [purchaseValue length])];
            
            [purchaseValue replaceOccurrencesOfString:numberFormatter.decimalSeparator
                                       withString:@"."
                                          options:NSLiteralSearch
                                            range:NSMakeRange(0, [purchaseValue length])];
            
            
            
            
            NSLog(@"\n purchaseValue = %@",purchaseValue);
            
            
            
            /*
            
            purchaseValue = [textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet symbolCharacterSet]];
            purchaseValue = [purchaseValue stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
            
            
            
            purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"[^A-Z]" withString:@""];
            purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"[^a-z]" withString:@""];
            purchaseValue = [purchaseValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@" " withString:@""];
            purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            nslog(@"\n purchaseValue = %@",purchaseValue);
            
           
            
            
            
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"])
            {
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"])
            {
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"])
            {
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"])
            {
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
            
            NSMutableString *reversedStr;
            int len = [purchaseValue length];
            reversedStr = [NSMutableString stringWithCapacity:len];
            
            while (len > 0)
            {
                [reversedStr appendString:[NSString stringWithFormat:@"%C", [purchaseValue characterAtIndex:--len]]];
            }
            
            NSRange d = [reversedStr rangeOfString:@","];//for example 3,50(normal 3.50)
           
            
            
            nslog(@"\n d = %d",d.location);
            
            
            if(appDelegate.isIOS7)
            {
                
                
                if ([purchaseValue rangeOfString:@" "].location != NSNotFound || d.location == 3)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
                {
                    purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@" " withString:@""];
                    purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
                }
                
                
            }

            
            
            if ([purchaseValue rangeOfString:@" "].location != NSNotFound || d.location == 2)
            {
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@" " withString:@""];
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
            if ([purchaseValue rangeOfString:@"'"].location != NSNotFound)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
            {
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@" " withString:@""];
                purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"'" withString:@""];
            }
            
            
            
            if ([appDelegate.curCode isEqualToString:@"ZAR"])
            {
                if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
                {
                    purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
                }
                else
                {
                    purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                }
                
            }
            
           
            
            else if([[numberFormatter locale].localeIdentifier isEqualToString:@"en_BE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_BE"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_AW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_CW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_NL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_SX"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"id_ID"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_AO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_BR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_GW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_PT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_ST"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_AT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_DE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_LU"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_CY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_GR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"da_DK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"it_IT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_MD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_RO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_AR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_BO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_EC"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_GQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_PY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_UY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_VE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"seh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sg_CF"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rn_BI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mua_CM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ms_BN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mgh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mk_MK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"lu_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rw_RW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kl_GL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kea_CV"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"is_IS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ka_GE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"gl_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"fo_FO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"swc_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ca_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"bs_BA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"eu_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az-Cyrl_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sq_AL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MA"])
            {
                nslog(@"Str_budget -- %@",purchaseValue);
                
                                NSString *stringToFilter =[NSString stringWithFormat:@"%@",purchaseValue];
                NSMutableString *targetString = [NSMutableString string];
                //set of characters which are required in the string......
                NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"."];
                nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
                
                for(int i = 0; i < [stringToFilter length]; i++)
                {
                    unichar currentChar = [stringToFilter characterAtIndex:i];
                    nslog(@"currentChar -- %C",currentChar);
                    
                    if(i == [stringToFilter length]-3)
                    {
                        nslog(@"currentChar -- %C",currentChar);
                        [targetString appendFormat:@"%C", currentChar];
                    }
                    else
                    {
                        if([okCharacterSet characterIsMember:currentChar])
                        {
                            [targetString appendFormat:@""];
                        }
                        else
                        {
                            [targetString appendFormat:@"%C", currentChar];
                        }
                    }
                }
                nslog(@"targetString -- %@",targetString);
                purchaseValue = [NSString stringWithFormat:@"%@",targetString];
                nslog(@"Str_budget -- %@",purchaseValue);
            }
            
            else if([[numberFormatter locale].localeIdentifier isEqualToString:@"gsw_CH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rm_CH"])
            {
                nslog(@"Str_budget -- %@",purchaseValue);
                
                                NSString *stringToFilter =[NSString stringWithFormat:@"%@",purchaseValue];
                NSMutableString *targetString = [NSMutableString string];
                //set of characters which are required in the string......
                NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
                nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
                
                for(int i = 0; i < [stringToFilter length]; i++)
                {
                    unichar currentChar = [stringToFilter characterAtIndex:i];
                    nslog(@"currentChar -- %C",currentChar);
                    
                    if(i == [stringToFilter length]-3)
                    {
                        nslog(@"currentChar -- %C",currentChar);
                        [targetString appendFormat:@"%C", currentChar];
                    }
                    else
                    {
                        if([okCharacterSet characterIsMember:currentChar])
                        {
                            [targetString appendFormat:@"%C", currentChar];
                        }
                        else
                        {
                            [targetString appendFormat:@""];
                        }
                    }
                }
                nslog(@"targetString -- %@",targetString);
                purchaseValue = [NSString stringWithFormat:@"%@",targetString];
                nslog(@"Str_budget -- %@",purchaseValue);
            }
            else if([[numberFormatter locale].localeIdentifier isEqualToString:@"ar_IQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_BH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_TD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DJ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_ER"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_JO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LB"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_OM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_PS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_QA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_AE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_001"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_YE"])
            {
                nslog(@"Str_budget -- %@",purchaseValue);
                NSString *stringToFilter =[NSString stringWithFormat:@"%@",purchaseValue];
                NSMutableString *targetString = [NSMutableString string];
                //set of characters which are required in the string......
                // NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@","];
                nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
                
                for(int i = 0; i < [stringToFilter length]; i++)
                {
                    unichar currentChar = [stringToFilter characterAtIndex:i];
                    nslog(@"currentChar -- %C",currentChar);
                    
                    if(i == [stringToFilter length]-3)
                    {
                        nslog(@"currentChar -- %C",currentChar);
                        [targetString appendFormat:@"."];
                    }
                    else
                    {
                        [targetString appendFormat:@"%C", currentChar];
                    }
                }
                nslog(@"targetString -- %@",targetString);
                purchaseValue = [NSString stringWithFormat:@"%@",targetString];
                nslog(@"Str_budget -- %@",purchaseValue);
            }
            
            
            
            
            
            else
            {
                if([appDelegate.curCode isEqualToString:@"Default Region Currency"])
                {
                    if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
                    {
                        purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
                    }
                    else
                    {
                        purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                    }
                }
                else
                {
                    purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                }
                
                
            }
            
            
            
            
            
            purchaseValue = [purchaseValue stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            */
            
            if ([purchaseValue length]<=0)
            {
                purchaseValue =[NSMutableString stringWithString: @""];
                
                
            }
            
            [purchaseValue retain];
        }
        if (textfield.tag == 2002)
        {
            
            
            textfield.text = [textfield.text stringByReplacingOccurrencesOfString:@"%20" withString:@""];
            
            
            //NSString *amountStr = textField.text;
            
            NSString *textFieldStr = [NSString stringWithFormat:@"%@",textfield.text];
            
            estimatedValue = [NSMutableString stringWithString:textFieldStr];
            
            //NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            
            
            [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            
            
            
            
            [estimatedValue replaceOccurrencesOfString:numberFormatter.currencySymbol
                                           withString:@""
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [estimatedValue length])];
            
            [estimatedValue replaceOccurrencesOfString:numberFormatter.groupingSeparator
                                           withString:@""
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [estimatedValue length])];
            
            [estimatedValue replaceOccurrencesOfString:numberFormatter.decimalSeparator
                                           withString:@"."
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [estimatedValue length])];
            
            
            
            
            NSLog(@"\n estimatedValue = %@",estimatedValue);

            
            
            
            
            /*
            
            estimatedValue = [textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet symbolCharacterSet]];
            estimatedValue = [estimatedValue stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
            
            estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"[^A-Z]" withString:@""];
            estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"[^a-z]" withString:@""];
            
            estimatedValue = [estimatedValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
           
             estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            
            
            
            
            
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"])
            {
                estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"])
            {
                estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"])
            {
                estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"])
            {
                estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
            
            
            
            
            NSMutableString *reversedStr;
            int len = [estimatedValue length];
            reversedStr = [NSMutableString stringWithCapacity:len];
            
            while (len > 0)
            {
                [reversedStr appendString:[NSString stringWithFormat:@"%C", [estimatedValue characterAtIndex:--len]]];
            }
            
            NSRange d = [reversedStr rangeOfString:@","];//for example 3,50(normal 3.50)
            nslog(@"\n d = %d",d.location);
            
            
            if(appDelegate.isIOS7)
            {
                
                
                
                
                if ([estimatedValue rangeOfString:@" "].location != NSNotFound || d.location == 3)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
                {
                    estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@" " withString:@""];
                    estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
                }
                
                
            }

            
            
            
            
            if ([estimatedValue rangeOfString:@"'"].location != NSNotFound || d.location == 2)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
            {
                estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@" " withString:@""];
                estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
            //estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            
            if ([appDelegate.curCode isEqualToString:@"ZAR"])
            {
                if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
                {
                    estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
                }
                else
                {
                    
                    
                    estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                }
                
            }
            
           
            
            else if([[numberFormatter locale].localeIdentifier isEqualToString:@"en_BE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_BE"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_AW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_CW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_NL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_SX"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"id_ID"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_AO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_BR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_GW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_PT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_ST"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_AT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_DE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_LU"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_CY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_GR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"da_DK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"it_IT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_MD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_RO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_AR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_BO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_EC"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_GQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_PY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_UY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_VE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"seh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sg_CF"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rn_BI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mua_CM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ms_BN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mgh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mk_MK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"lu_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rw_RW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kl_GL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kea_CV"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"is_IS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ka_GE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"gl_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"fo_FO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"swc_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ca_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"bs_BA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"eu_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az-Cyrl_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sq_AL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MA"])
            {
                nslog(@"Str_budget -- %@",estimatedValue);
                
                               NSString *stringToFilter =[NSString stringWithFormat:@"%@",estimatedValue];
                NSMutableString *targetString = [NSMutableString string];
                //set of characters which are required in the string......
                NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"."];
                nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
                
                for(int i = 0; i < [stringToFilter length]; i++)
                {
                    unichar currentChar = [stringToFilter characterAtIndex:i];
                    nslog(@"currentChar -- %C",currentChar);
                    
                    if(i == [stringToFilter length]-3)
                    {
                        nslog(@"currentChar -- %C",currentChar);
                        [targetString appendFormat:@"%C", currentChar];
                    }
                    else
                    {
                        if([okCharacterSet characterIsMember:currentChar])
                        {
                            [targetString appendFormat:@""];
                        }
                        else
                        {
                            [targetString appendFormat:@"%C", currentChar];
                        }
                    }
                }
                nslog(@"targetString -- %@",targetString);
                estimatedValue = [NSString stringWithFormat:@"%@",targetString];
                nslog(@"Str_budget -- %@",estimatedValue);
            }
            
            else if([[numberFormatter locale].localeIdentifier isEqualToString:@"gsw_CH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rm_CH"])
            {
                nslog(@"Str_budget -- %@",estimatedValue);
                
                               NSString *stringToFilter =[NSString stringWithFormat:@"%@",estimatedValue];
                NSMutableString *targetString = [NSMutableString string];
                //set of characters which are required in the string......
                NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
                nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
                
                for(int i = 0; i < [stringToFilter length]; i++)
                {
                    unichar currentChar = [stringToFilter characterAtIndex:i];
                    nslog(@"currentChar -- %C",currentChar);
                    
                    if(i == [stringToFilter length]-3)
                    {
                        nslog(@"currentChar -- %C",currentChar);
                        [targetString appendFormat:@"%C", currentChar];
                    }
                    else
                    {
                        if([okCharacterSet characterIsMember:currentChar])
                        {
                            [targetString appendFormat:@"%C", currentChar];
                        }
                        else
                        {
                            [targetString appendFormat:@""];
                        }
                    }
                }
                nslog(@"targetString -- %@",targetString);
                estimatedValue = [NSString stringWithFormat:@"%@",targetString];
                nslog(@"Str_budget -- %@",estimatedValue);
            }
            else if([[numberFormatter locale].localeIdentifier isEqualToString:@"ar_IQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_BH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_TD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DJ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_ER"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_JO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LB"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_OM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_PS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_QA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_AE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_001"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_YE"])
            {
                nslog(@"Str_budget -- %@",estimatedValue);
                NSString *stringToFilter =[NSString stringWithFormat:@"%@",estimatedValue];
                NSMutableString *targetString = [NSMutableString string];
                //set of characters which are required in the string......
                // NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@","];
                nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
                
                for(int i = 0; i < [stringToFilter length]; i++)
                {
                    unichar currentChar = [stringToFilter characterAtIndex:i];
                    nslog(@"currentChar -- %C",currentChar);
                    
                    if(i == [stringToFilter length]-3)
                    {
                        nslog(@"currentChar -- %C",currentChar);
                        [targetString appendFormat:@"."];
                    }
                    else
                    {
                        [targetString appendFormat:@"%C", currentChar];
                    }
                }
                nslog(@"targetString -- %@",targetString);
                estimatedValue = [NSString stringWithFormat:@"%@",targetString];
                nslog(@"Str_budget -- %@",estimatedValue);
            }
            
            
            
            
            else
            {
                if ([appDelegate.curCode isEqualToString:@"Default Region Currency"])
                {
                    
                    
                    
                    if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
                    {
                        estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
                    }
                    else
                    {
                        estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                    }
                    
                    
                    
                    
                    
                    
                    
                }
                else
                {
                    estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                }
                
                
            }
            
            
            
            
            estimatedValue = [estimatedValue stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            
            */
            
            
            if ([estimatedValue length]<=0)
            {
                estimatedValue = [NSMutableString stringWithString:@"%@"];
            }
            [estimatedValue retain];
        }
        else if (textfield.tag == 2003)
        {
            
            
            
            
            textfield.text = [textfield.text stringByReplacingOccurrencesOfString:@"%20" withString:@""];
            
            
            //NSString *amountStr = textField.text;
            
            NSString *textFieldStr = [NSString stringWithFormat:@"%@",textfield.text];
            
            loanValue = [NSMutableString stringWithString:textFieldStr];
            
            //NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            
            
            [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            
            
            
            
            [loanValue replaceOccurrencesOfString:numberFormatter.currencySymbol
                                           withString:@""
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [loanValue length])];
            
            [loanValue replaceOccurrencesOfString:numberFormatter.groupingSeparator
                                           withString:@""
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [loanValue length])];
            
            [loanValue replaceOccurrencesOfString:numberFormatter.decimalSeparator
                                           withString:@"."
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [loanValue length])];
            
            
            
            
            NSLog(@"\n purchaseValue = %@",loanValue);
            
            
            
            /*
            loanValue = [textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet symbolCharacterSet]];
            
            
            
            
            loanValue = [loanValue stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
            
            loanValue = [loanValue stringByReplacingOccurrencesOfString:@"[^A-Z]" withString:@""];
            loanValue = [loanValue stringByReplacingOccurrencesOfString:@"[^a-z]" withString:@""];
            
            loanValue = [loanValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            
            
            loanValue = [loanValue stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            
            
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"])
            {
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"])
            {
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"])
            {
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"])
            {
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@"." withString:@""];
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
            nslog(@"\n loanValue = %@",loanValue);
            
           
            NSMutableString *reversedStr;
            int len = [loanValue length];
            reversedStr = [NSMutableString stringWithCapacity:len];
            
            while (len > 0)
            {
                [reversedStr appendString:[NSString stringWithFormat:@"%C", [loanValue characterAtIndex:--len]]];
            }
            
            NSRange d = [reversedStr rangeOfString:@","];//for example 3,50(normal 3.50)
             
            // NSRange r = [loanValue rangeOfString:@","];
            
            
            nslog(@"\n d = %d",d.location);
            
            
            if(appDelegate.isIOS7)
            {
                
                
                if ([loanValue rangeOfString:@" "].location != NSNotFound || d.location == 3)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
                {
                    loanValue = [loanValue stringByReplacingOccurrencesOfString:@" " withString:@""];
                    loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
                }
                
                
            }
            

            
            
            
            if ([loanValue rangeOfString:@" "].location != NSNotFound || d.location==2)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
            {
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@" " withString:@""];
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
            if ([loanValue rangeOfString:@"'"].location != NSNotFound)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
            {
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@" " withString:@""];
                loanValue = [loanValue stringByReplacingOccurrencesOfString:@"'" withString:@""];
            }
            
            
            //loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            if ([appDelegate.curCode isEqualToString:@"ZAR"])
            {
                if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
                {
                    loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
                }
                else
                {
                    loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                }
                
            }
            
            
            
            
            
            
            else if([[numberFormatter locale].localeIdentifier isEqualToString:@"en_BE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_BE"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_AW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_CW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_NL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_SX"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"id_ID"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_AO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_BR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_GW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_PT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_ST"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_AT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_DE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_LU"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_CY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_GR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"da_DK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"it_IT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_MD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_RO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_AR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_BO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_EC"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_GQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_PY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_UY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_VE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"seh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sg_CF"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rn_BI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mua_CM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ms_BN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mgh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mk_MK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"lu_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rw_RW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kl_GL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kea_CV"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"is_IS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ka_GE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"gl_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"fo_FO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"swc_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ca_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"bs_BA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"eu_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az-Cyrl_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sq_AL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MA"])
            {
                nslog(@"Str_budget -- %@",loanValue);
                
             
                NSString *stringToFilter =[NSString stringWithFormat:@"%@",loanValue];
                NSMutableString *targetString = [NSMutableString string];
                //set of characters which are required in the string......
                NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"."];
                nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
                
                for(int i = 0; i < [stringToFilter length]; i++)
                {
                    unichar currentChar = [stringToFilter characterAtIndex:i];
                    nslog(@"currentChar -- %C",currentChar);
                    
                    if(i == [stringToFilter length]-3)
                    {
                        nslog(@"currentChar -- %C",currentChar);
                        [targetString appendFormat:@"%C", currentChar];
                    }
                    else
                    {
                        if([okCharacterSet characterIsMember:currentChar])
                        {
                            [targetString appendFormat:@""];
                        }
                        else
                        {
                            [targetString appendFormat:@"%C", currentChar];
                        }
                    }
                }
                nslog(@"targetString -- %@",targetString);
                loanValue = [NSString stringWithFormat:@"%@",targetString];
                nslog(@"Str_budget -- %@",loanValue);
            }
            
            else if([[numberFormatter locale].localeIdentifier isEqualToString:@"gsw_CH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rm_CH"])
            {
                nslog(@"Str_budget -- %@",loanValue);
                
             
                NSString *stringToFilter =[NSString stringWithFormat:@"%@",loanValue];
                NSMutableString *targetString = [NSMutableString string];
                //set of characters which are required in the string......
                NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
                nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
                
                for(int i = 0; i < [stringToFilter length]; i++)
                {
                    unichar currentChar = [stringToFilter characterAtIndex:i];
                    nslog(@"currentChar -- %C",currentChar);
                    
                    if(i == [stringToFilter length]-3)
                    {
                        nslog(@"currentChar -- %C",currentChar);
                        [targetString appendFormat:@"%C", currentChar];
                    }
                    else
                    {
                        if([okCharacterSet characterIsMember:currentChar])
                        {
                            [targetString appendFormat:@"%C", currentChar];
                        }
                        else
                        {
                            [targetString appendFormat:@""];
                        }
                    }
                }
                nslog(@"targetString -- %@",targetString);
                loanValue = [NSString stringWithFormat:@"%@",targetString];
                nslog(@"Str_budget -- %@",loanValue);
            }
            else if([[numberFormatter locale].localeIdentifier isEqualToString:@"ar_IQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_BH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_TD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DJ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_ER"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_JO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LB"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_OM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_PS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_QA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_AE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_001"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_YE"])
            {
                nslog(@"Str_budget -- %@",loanValue);
                NSString *stringToFilter =[NSString stringWithFormat:@"%@",loanValue];
                NSMutableString *targetString = [NSMutableString string];
                //set of characters which are required in the string......
                // NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@","];
                nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
                
                for(int i = 0; i < [stringToFilter length]; i++)
                {
                    unichar currentChar = [stringToFilter characterAtIndex:i];
                    nslog(@"currentChar -- %C",currentChar);
                    
                    if(i == [stringToFilter length]-3)
                    {
                        nslog(@"currentChar -- %C",currentChar);
                        [targetString appendFormat:@"."];
                    }
                    else
                    {
                        [targetString appendFormat:@"%C", currentChar];
                    }
                }
                nslog(@"targetString -- %@",targetString);
                loanValue = [NSString stringWithFormat:@"%@",targetString];
                nslog(@"Str_budget -- %@",loanValue);
            }
            
            
            
            
            
            else
            {
                
                if ([appDelegate.curCode isEqualToString:@"Default Region Currency"])
                {
                    
                    
                    if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
                    {
                        loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
                    }
                    else
                    {
                        loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                    }
                    
                    
                }
                else
                {
                    loanValue = [loanValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                }
                
                
            }
            
            
            
            
            loanValue = [loanValue stringByReplacingOccurrencesOfString:@" " withString:@""];
             
             */
             
            if ([loanValue length]<=0)
            {
                loanValue = [NSMutableString stringWithString:@""];
            }
            [loanValue retain];
            
        }
        
        [textfield resignFirstResponder];
    }
    
    
    
   
    
    
    
    float loan;
    float estimate;
    if ([estimatedValue length] > 0)
    {
        estimate = [estimatedValue floatValue];
    }
    else
    {
        estimate = 0;
    }
    if (loanValue)
    {
        if ([loanValue length]>0)
        {
            loan = [loanValue floatValue];
        }
        else
        {
            loan = 0;
            loanValue = [NSMutableString stringWithString:@""];
        }
    }
    
    
    
    
    
    
    if(([[numberFormatter currencyCode] isEqualToString:@"ZAR"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"ZAR"]))
    {
        
       
    }
    
    nslog(@"\n estimate = %f",estimate);
    nslog(@"\n loan = %f",loan);
    
    float equity = estimate - loan;
    
    nslog(@"\n equity = %f",equity);
    
    equityValue = [NSMutableString stringWithFormat:@"%0.2f",equity];
    
    
    nslog(@"\n equityValue = %@",equityValue);
    
    
    [numberFormatter setNegativeFormat:@"-¤#,##0.00"];
    
    
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
        [appDelegate selectCurrencyIndex];
    }
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [numberFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [numberFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [numberFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [numberFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        
    }
    UITableViewCell *cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    nslog(@"cell subviews %@",[cell.contentView subviews]);
    UITextField *textfield = (UITextField *)[[cell.contentView subviews]objectAtIndex:3];
    nslog(@"textfield text is %@",textfield.text);
    
    
    // UITextField *textfield = (UITextField *)[self.view viewWithTag:30000];
    
    textfield.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:equity]];
    
    if (equity >=0)
    {
        textfield.textColor = [UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0];
    }
    else
    {
        textfield.textColor = [UIColor redColor];
    }
    //    textfield.text = equityValue;
    
    [tblView setFrame:CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height)];
    // [tblView reloadData];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString* currentTime = [dateFormatter stringFromDate:[NSDate date]];
    //NSString* currentTime = [appDelegate.regionDateFormatter stringFromDate:[NSDate date]];
    
    
    
    [dateFormatter release];
    
    [appDelegate updatePropertyEquity:purchaseValue maketValue:estimatedValue Loan:loanValue equity:equityValue date:currentTime rowid:[[[appDelegate.propertyDetailArray objectAtIndex:isIndex]valueForKey:@"rowid"]intValue]];
    [appDelegate selectPropertyDetail];
    
    
    //[tblView reloadData];
    
}

-(void)cancel_clicked
{
    [self nav_done];
	[self.navigationController popViewControllerAnimated:YES];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    //nslog(@"index====%d",appDelegate.equityIndex);
    
    
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
        [appDelegate selectCurrencyIndex];
    }
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    //[self.view addSubview:appDelegate.bottomView];
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
    
        [appDelegate selectCurrencyIndex];
    
    }
    
    if(appDelegate.isIPad)
    {
    
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
    [tblView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations.
    if(appDelegate.isIPad)
    {
        return  YES;
    }
    return FALSE;
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    if(appDelegate.isIPad)
    {
        
        [appDelegate manageViewControllerHeight];
        [tblView reloadData];
    }
    
    
    
    
}
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    if(appDelegate.isIPad)
    {
        toolBar.hidden = TRUE;
        
       
       
            if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
            {
                toolBar.frame = CGRectMake(0, 672,768, 44);
            }
            else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
            {
                toolBar.frame = CGRectMake(0,328,1024, 44);
                
            }
        
        
        
    }
    
    
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if(appDelegate.isIPad)
        {
            return 90;
        }
        
        return 60;
    }
	return 44;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 1)
    {
        return [equityMainArray count]-1;
    }
    else
    {
        return 1;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if(section == 1)
    {
        return @"Update Market Value & Loan Outstanding to re-calculate Equity";
    }
    return @"";
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setNegativeFormat:@"-¤#,##0.00"];
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [numberFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [numberFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [numberFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [numberFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        
        
        
        
    }
    
    static NSString *CellIdentifier = @"Cell";
    UITextField *textField, *eqTextField;
    UILabel *label, *detailLabel;
    UIImageView*frequency_imageView;
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //cell.backgroundColor = [UIColor clearColor];

    if (cell == nil)
    //if (1)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        
        
        if(appDelegate.isIOS7)
        {
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        }

        
        label = [[UILabel alloc]initWithFrame:CGRectMake(40, 7, 140, 30)];
        label.tag = 400;
        label.numberOfLines = 0;
        label.lineBreakMode = UILineBreakModeWordWrap;
        [label setFont:[UIFont systemFontOfSize:15.0]];
        [label setBackgroundColor:[UIColor clearColor]];
        
        [cell.contentView addSubview:label];
        
        
        detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(150, 7, 140, 30)];

        
        
        
        detailLabel.tag = 500;
        
        detailLabel.textAlignment = UITextAlignmentRight;
        
        detailLabel.numberOfLines = 0;
        detailLabel.lineBreakMode = UILineBreakModeTailTruncation;
        [detailLabel setFont:[UIFont systemFontOfSize:15.0]];
        [detailLabel setBackgroundColor:[UIColor clearColor]];
        
        [cell.contentView addSubview:detailLabel];
        
        textField = [[UITextField alloc]initWithFrame:CGRectMake(150, 10, 140, 30)];
		textField.delegate = self;
		textField.tag = 2000+indexPath.row;
        [textField setTextAlignment:UITextAlignmentRight];
        
        [textField setFont:[UIFont systemFontOfSize:15.0]];
		textField.keyboardType = UIKeyboardTypeNumberPad;
		[textField setBorderStyle:UITextBorderStyleNone];
		[cell.contentView addSubview:textField];
        
        eqTextField = [[UITextField alloc]initWithFrame:CGRectMake(150, 10, 140, 30)];
        
               
        
        
		eqTextField.delegate = self;
		eqTextField.tag = 30000+indexPath.row;
        
        
        [eqTextField setTextAlignment:UITextAlignmentRight];
        
        [eqTextField setFont:[UIFont systemFontOfSize:15.0]];
		eqTextField.keyboardType = UIKeyboardTypeNumberPad;
		[eqTextField setBorderStyle:UITextBorderStyleNone];
		[cell.contentView addSubview:eqTextField];
        
        
        frequency_imageView = [[UIImageView alloc] init];
        frequency_imageView.tag = 600;
        [cell.contentView addSubview:frequency_imageView];
        
		
    }
	else
    {
        label = (UILabel *)[cell.contentView viewWithTag:400];
        detailLabel = (UILabel *)[cell.contentView viewWithTag:500];
        textField = (UITextField *)[cell.contentView viewWithTag:2000+indexPath.row];
        eqTextField = (UITextField *)[cell.contentView viewWithTag:30000+indexPath.row];
        frequency_imageView = (UIImageView *)[cell.contentView viewWithTag:600];
        
        
        [label retain];
        [detailLabel retain];
        [textField retain];
        [eqTextField retain];
        [frequency_imageView retain];
        
    }
    
    // textField
    
    if(appDelegate.isIPad)
    {
        
        if(appDelegate.isIOS7)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                textField.frame = CGRectMake(340, 10, 385, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                textField.frame = CGRectMake(340, 10, 650, 30);
            }
        }
        else
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                textField.frame = CGRectMake(340, 10, 305, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                textField.frame = CGRectMake(340, 10, 550, 30);
            }
        }
    }
    else
    {
         textField.frame = CGRectMake(150, 10, 140, 30);
        if(appDelegate.isIOS7)
        {
             textField.frame = CGRectMake(150,7, 140, 30);
        }
        
        
       
    }

    //detailLabel
    
    if(appDelegate.isIPad)
    {
        
        if(appDelegate.isIOS7)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                detailLabel.frame = CGRectMake(340, 10, 385, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                detailLabel.frame = CGRectMake(340, 10, 650, 30);
            }
        }
        else
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                detailLabel.frame = CGRectMake(340, 10, 305, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                detailLabel.frame = CGRectMake(340, 10, 550, 30);
            }
        }
    }
    else
    {
        detailLabel.frame = CGRectMake(150, 7, 140, 30);
    }
    
    
    
    
    detailLabel.hidden = TRUE;
    cell.accessoryType = UITableViewCellAccessoryNone;
    eqTextField.hidden = FALSE;
    
    
    //eqTextField
    
    
    if(appDelegate.isIPad)
    {
        
        if(appDelegate.isIOS7)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                eqTextField.frame = CGRectMake(340, 10, 385, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                eqTextField.frame = CGRectMake(340, 10, 650, 30);
            }
        }
        else
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                eqTextField.frame = CGRectMake(340, 10, 305, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                eqTextField.frame = CGRectMake(340, 10, 550, 30);
            }
        }
        
    }
    else
    {
        eqTextField.frame = CGRectMake(150, 10, 140, 30);
    }

    
    
    
    
    
   
    
    
    
    frequency_imageView.hidden = TRUE;

    
    if (indexPath.section == 0)
    {
        frequency_imageView.hidden = FALSE;
        
        
        if(appDelegate.isIPad)
        {
            
            frequency_imageView.frame = CGRectMake(12,40,40,40);
            
            if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
            {
                cell.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipad_port_bar_white.png"]] autorelease];
            }
            else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
            {
                cell.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipad_land_bar_white.png"]] autorelease];
            }
        }
        
        else
        {
            
            frequency_imageView.frame = CGRectMake(3,12,40,40);
            cell.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bar_white.png"]] autorelease];
        }
        
        
        //cell.imageView.image = [UIImage imageNamed:@"equity__calculator_icon.png"];
        
        
                
        frequency_imageView.image = [UIImage imageNamed:@"equity__calculator_icon.png"];
        
        
        
        
        
        
        
        
    }
    
    
    if (indexPath.section == 1)
    {
        eqTextField.hidden = TRUE;
        if (indexPath.row == 0)
        {
            textField.hidden = TRUE;
            detailLabel.hidden = FALSE;
            detailLabel.text = [[appDelegate.propertyDetailArray objectAtIndex:isIndex]valueForKey:@"0"];
            
            //cell.imageView.image = [UIImage imageNamed:@"property_icon.png"];
            
            
            UIImageView*property_imageView = [[UIImageView alloc] init];
            property_imageView.frame = CGRectMake(0, 0,40,40);
            property_imageView.image = [UIImage imageNamed:@"property_icon.png"];
            [cell.contentView addSubview:property_imageView];
            [property_imageView release];
            
            
        }
        
        else if (indexPath.row == 1)
        {
            textField.placeholder = @"Purchase Price";
            NSString *str = [[appDelegate.propertyDetailArray objectAtIndex:isIndex]valueForKey:@"8"];
            if ([str length]>0)
            {
                textField.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[[appDelegate.propertyDetailArray objectAtIndex:isIndex]valueForKey:@"8"]floatValue]]];
            }
            
            //cell.imageView.image = [UIImage imageNamed:@"purchase_price.png"];
           
            UIImageView*purchase_price_imageView = [[UIImageView alloc] init];
            purchase_price_imageView.frame = CGRectMake(0, 0,40,40);
            purchase_price_imageView.image = [UIImage imageNamed:@"purchase_price.png"];
            [cell.contentView addSubview:purchase_price_imageView];
            [purchase_price_imageView release];
            
            
        }
        else if (indexPath.row == 2)
        {
            
            textField.placeholder = @"Market Value";
            NSString *str = [[appDelegate.propertyDetailArray objectAtIndex:isIndex]valueForKey:@"9"];
            if ([str length]>0)
            {
                textField.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[[appDelegate.propertyDetailArray objectAtIndex:isIndex]valueForKey:@"9"]floatValue]]];
                estimatedValue = textField.text;
            }
            else
            {
                estimatedValue = @"0";
            }
            
            
            UIImageView*marketvalue_imageView = [[UIImageView alloc] init];
            marketvalue_imageView.frame = CGRectMake(0, 0,40,40);
            marketvalue_imageView.image = [UIImage imageNamed:@"market_value.png"];
            [cell.contentView addSubview:marketvalue_imageView];
            [marketvalue_imageView release];
            
           // cell.imageView.image = [UIImage imageNamed:@"market_value.png"];
            
            
        }
        
        else if (indexPath.row == 3)
        {
            //   textField.text = @"Loan Outstanding";
            
            
            estimatedValue = [[appDelegate.propertyDetailArray objectAtIndex:isIndex]valueForKey:@"13"];
            
            if ([estimatedValue floatValue]>0)
            {
                textField.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[[appDelegate.propertyDetailArray objectAtIndex:isIndex]valueForKey:@"13"]floatValue]]];
            }
            else
            {
                textField.placeholder = @"Loan Outstanding";
            }
            //cell.imageView.image = [UIImage imageNamed:@"loan_outstanding.png"];
            
            
            UIImageView*loan_outstanding_imageView = [[UIImageView alloc] init];
            loan_outstanding_imageView.frame = CGRectMake(0, 0,40,40);
            loan_outstanding_imageView.image = [UIImage imageNamed:@"loan_outstanding.png"];
            [cell.contentView addSubview:loan_outstanding_imageView];
            [loan_outstanding_imageView release];

            
            
            
        }
        label.text = [equityMainArray objectAtIndex:indexPath.row];
    }
    else if (indexPath.section == 0)
    {
        cell.backgroundColor = [UIColor clearColor];

        eqTextField.hidden = FALSE;
        textField.hidden = TRUE;
  	    label.text = @"Equity";
        label.font = [UIFont boldSystemFontOfSize:14.0];
        
        
        eqTextField.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[[appDelegate.propertyDetailArray objectAtIndex:isIndex]valueForKey:@"zEquity"]floatValue]]];
        
        float equity = [[[appDelegate.propertyDetailArray objectAtIndex:isIndex]valueForKey:@"zEquity"]floatValue];
        
        if (equity >=0)
        {
            eqTextField.textColor = [UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
        else
        {
            eqTextField.textColor = [UIColor redColor];
        }
        
        //        textField.text = equityValue;
        eqTextField.userInteractionEnabled = FALSE;
        
        
        /*
         SUN:0004
         iPad Pending.
         */
        
        if(appDelegate.isIPad)
        {
            
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    
                    eqTextField.frame = CGRectMake(340, 54, 385, 30);
                }
                else
                {
                    eqTextField.frame = CGRectMake(340, 54, 650, 30);
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    
                    eqTextField.frame = CGRectMake(340, 54, 305, 30);
                }
                else
                {
                    eqTextField.frame = CGRectMake(340, 54, 550, 30);
                }
            }
            

            
            label.frame = CGRectMake(60,46, 140, 30);
            
        }
        else
        {
            
             eqTextField.frame = CGRectMake(150,25, 140, 30);
            if(appDelegate.isIOS7)
            {
                 eqTextField.frame = CGRectMake(150,20, 140, 30);
            }
           
            
            
            label.frame = CGRectMake(40,18, 140, 30);
            
        }
        
        
    }

	
    
	[frequency_imageView release];
	[label release];
    [detailLabel release];
    [textField release];
    [eqTextField release];
   
    
    // Configure the cell...
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark -
#pragma mark text field delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textFieldIndex = textField.tag;
    [toolBar removeFromSuperview];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelToolBar_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    
    /*
     UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done_clicked)];
     */
    
    
    if(appDelegate.isIPad)
    {
        
        
        
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                toolBar.frame = CGRectMake(0,672,768,44);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                toolBar.frame = CGRectMake(0,328,1024,44);
            }
        
        
    }
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,doneButton, nil]];
    
    //    toolBar.items = array;
    toolBar.barStyle = UIBarStyleBlack;
    [self.view addSubview:toolBar];
    
    [doneButton release];
    [flexibleSpaceLeft release];
    [cancelBtn release];
    
    toolBar.tag = textField.tag;
    nslog(@"\n textField.tag = %d",textField.tag);
    
    if(!appDelegate.isIPad)
    {
        if (textField.tag == 2003 )
        {
            [tblView setFrame:CGRectMake(0, -100,tblView.frame.size.width,tblView.frame.size.height)];
        }
        else if (textField.tag == 2002)
        {
            [tblView setFrame:CGRectMake(0, -80,tblView.frame.size.width,tblView.frame.size.height)];
        }
        else if(textField.tag ==2001)
        {
            [tblView setFrame:CGRectMake(0, -40,tblView.frame.size.width,tblView.frame.size.height)];
        }
        
    }
    
    return YES;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    //[tblView reloadData];
    
    if(appDelegate.isIPad)
    {
        toolBar.hidden = TRUE;
        [textField resignFirstResponder];
    }
    
    
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    toolBar.hidden = FALSE;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    nslog(@"\n textField = %@",textField.text);
    
    if(appDelegate.isIPad)
    {
        if([string length]!=0)
        {
            NSMutableString *strippedString = [NSMutableString
                                               stringWithCapacity:string.length];
            NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            NSScanner *scanner = [NSScanner scannerWithString:string];
            
            
            while ([scanner isAtEnd] == NO)
            {
                NSString *buffer;
                if ([scanner scanCharactersFromSet:numbers intoString:&buffer])
                {
                    [strippedString appendString:buffer];
                    
                } else
                {
                    [scanner setScanLocation:([scanner scanLocation] + 1)];
                }
            }
            
            nslog(@"strippedString  = %@", strippedString); // "123123123"
            if([strippedString length]==0)
            {
                return NO;
            }
            string = strippedString;
            
        }
        
    }
    
    
    
    /*
    NSDecimalNumber *someAmount = [NSDecimalNumber decimalNumberWithString:@"5.00"];
    */
    
    
    
    
    
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [currencyFormatter setCurrencyCode:appDelegate.curCode];
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        else if([[currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
    }
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    
    int currencyScale = [currencyFormatter maximumFractionDigits];
    nslog(@"\n scale = %d",currencyScale);
    
    if(currencyScale == 0)
    {
        
        NSString *cleanCentString = [[textField.text
                                      componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                     componentsJoinedByString:@""];
        
        
        
        
        double centAmount = [cleanCentString doubleValue];
        // Check the user input
        
        if (string.length > 0)
        {
            // Digit added
            centAmount = centAmount * 10 + [string doubleValue];
        }
        else
        {
            // Digit deleted
            centAmount = centAmount / 10;
        }
        // Update call amount value
        NSNumber *_amount;
        //_amount = [[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f];
        _amount = [[[NSNumber alloc]initWithInt:(int)centAmount] autorelease];
        
        
        
        
        
        [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        
        if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
        {
            [_currencyFormatter setCurrencyCode:appDelegate.curCode];
            
            
            if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
            {
				[_currencyFormatter setCurrencyCode:@"USD"];
                nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                [_currencyFormatter setLocale:locale];
                [locale release];
                
            }
            else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
            {
                [_currencyFormatter setCurrencyCode:@"CNY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                [_currencyFormatter setLocale:locale];
                [locale release];
                
            }
            else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
            {
                [_currencyFormatter setCurrencyCode:@"JPY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                [_currencyFormatter setLocale:locale];
                [locale release];
                
            }
            
            
        }
        else
        {
            [_currencyFormatter setCurrencyCode:[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
        }
        [_currencyFormatter setNegativeFormat:@"-¤#,##0.00"];
        
        textField.text = [_currencyFormatter stringFromNumber:_amount];
        
    }
    else
    {
        NSString *cleanCentString = [[textField.text
                                      componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                     componentsJoinedByString:@""];
        // Parse final integer value
        
        nslog(@"\n cleanCentString = %@",cleanCentString);
        
        double centAmount = [cleanCentString doubleValue];
        // Check the user input
        if (string.length > 0)
        {
            // Digit added
            centAmount = centAmount * 10 + [string doubleValue];
        }
        else
        {
            // Digit deleted
            centAmount = centAmount / 10;
        }
        // Update call amount value
        NSNumber *_amount;
        _amount = [[[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f] autorelease];
        
        
        [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
        {
            [_currencyFormatter setCurrencyCode:appDelegate.curCode];
            
            
            if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
            {
                [_currencyFormatter setCurrencyCode:@"USD"];
                nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                [_currencyFormatter setLocale:locale];
                [locale release];
                
            }
            else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
            {
                [_currencyFormatter setCurrencyCode:@"CNY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                [_currencyFormatter setLocale:locale];
                [locale release];
                
            }
            
            else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
            {
                [_currencyFormatter setCurrencyCode:@"JPY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                [_currencyFormatter setLocale:locale];
                [locale release];
                
            }
            
            
        }
        textField.text = [_currencyFormatter stringFromNumber:_amount];
        
    }
    
    
    
    return NO;
}

-(void)cancelToolBar_clicked
{
    
    UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldIndex];
    [textField resignFirstResponder];
    [toolBar removeFromSuperview];
    //[self nav_done];
    
    
    if(appDelegate.isIPad)
    {
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            [tblView setFrame:CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height)];
        }

    }
    else
    {
         [tblView setFrame:CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height)];
    }
    
    
    
   
}

-(void)done_clicked
{
    /*
     [self nav_done];
     */
    
    [toolBar removeFromSuperview];
    nslog(@"toolbar tag === %d",toolBar.tag);
    
    
    if(appDelegate.isIPad)
    {
        
        if(appDelegate.isIOS7)
        {
            
            
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            
        }
        else
        {
             [tblView setFrame:CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height)];
        }
        
    }
    else
    {
        [tblView setFrame:CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height)];
    }
    
    
    
    
    if (toolBar.tag == 2003)
    {
        UITextField *textfield = (UITextField *)[self.view viewWithTag:2003];
        [textfield resignFirstResponder];
        
    }
    else
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:toolBar.tag+1];
        [textField becomeFirstResponder];
    }
    
    
    
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source.
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
 }
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //appDelegate.isFromIncomeExpenseView = -1;
    
    //    if (indexPath.row == 0)
    //    {
    //        propertyViewController *propertyView = [[propertyViewController alloc]initWithNibName:@"propertyViewController" bundle:nil];
    //        [self.navigationController pushViewController:propertyView animated:YES];
    //        [propertyView release];
    //    }
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload
{
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc
{
    
     MemoryLog(@"\n dealloc in = %@ \n",NSStringFromClass([self class]));
    [super dealloc];
    
    
    [_currencyFormatter release];
    [currencyFormatter release];
    //[numberFormatter release];
    [toolBar release];
    
    

}


@end

