//
//  EquityViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

@interface EquityViewController : UIViewController<UITextFieldDelegate> {

	IBOutlet UITableView *tblView;
	
	NSMutableArray *equityMainArray;
	NSMutableArray *equityDetailArray;
    UIToolbar *toolBar;
    UISegmentedControl *segment;
    PropertyLogBookAppDelegate *appDelegate;
    
    NSMutableString *estimatedValue;
    
    
    NSMutableString *loanValue;
    NSMutableString *equityValue;
    NSMutableString *purchaseValue;
    
    
    
    int isIndex;
    int textFieldIndex;
    NSNumberFormatter *currencyFormatter;
    NSNumberFormatter *_currencyFormatter;
    NSNumberFormatter *numberFormatter;
    
}
@property (nonatomic, readwrite)int isIndex;
@property(nonatomic,retain) NSString *loanValue,*estimatedValue,*equityValue;
@end
