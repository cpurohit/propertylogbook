//
//  LumsumViewCell.m
//  Loansactually
//
//  
//  
//

#import "LumsumViewCell.h"

@implementation LumsumViewCell
//@synthesize lblTitle,segment21,segment22,segment31,segment32,segment33,slider,txtValues,lblPersent;
@synthesize lblTitle = _lblTitle;
@synthesize segment21 = _segment21;
@synthesize segment22 = _segment22;
@synthesize segment31 = _segment31;
@synthesize segment32 = _segment32;
@synthesize segment33 = _segment33;
@synthesize slider = _slider;
@synthesize txtValues = _txtValues;
@synthesize lblPersent = _lblPersent;
@synthesize cellImageView = _cellImageView;
-(void)dealloc
{
    _lblTitle = nil;
    _segment21 = nil;
    _segment22 = nil;
    _segment31 = nil;
    _segment32 = nil;
    _segment33 = nil;
    _slider = nil;

    _lblPersent = nil;
    _cellImageView = nil;
    [super dealloc];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    app_delegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    
    _segment21 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _segment22 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    _segment31 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _segment32 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _segment33 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    _cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 40)];
    _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, 120, 20)];
    _lblPersent = [[UILabel alloc] init];
    
     _txtValues = [[UITextField alloc] initWithFrame:CGRectMake(150, 10, 140, 20)];
    
    if(app_delegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
             _cellImageView.frame = CGRectMake(0, 0, 680, 40);
            
            _segment31.frame = CGRectMake(370,15, 90, 25);
            _segment32.frame = CGRectMake(460, 15,90, 25);
            _segment33.frame = CGRectMake(550, 15,90, 25);

            _txtValues.frame = CGRectMake(500, 10, 140, 20);

            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
             _cellImageView.frame = CGRectMake(0, 0, 936, 40);
            
            
            _segment31.frame = CGRectMake(625,15, 90, 25);
            _segment32.frame = CGRectMake(715, 15,90, 25);
            _segment33.frame = CGRectMake(805, 15,90, 25);

            
            _txtValues.frame = CGRectMake(756, 10, 140, 20);
            
        }

    }
    else
    {
        
        
        
        
         _segment31.frame = CGRectMake(15,37, 90, 25);
        _segment32.frame = CGRectMake(105, 37,90, 25);
        _segment33.frame = CGRectMake(195, 37,90, 25);
        
       
    }
    
    
    
    
    
    [self.contentView addSubview:_cellImageView];
    
    
    _lblTitle.font = [UIFont fontWithName:@"Bookman Old Style" size:13.0];
    [_lblTitle setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:_lblTitle];
    
    
    _lblPersent.font = [UIFont fontWithName:@"Bookman Old Style" size:13.0];
    [_lblPersent setBackgroundColor:[UIColor clearColor]];
    _lblPersent.text = @"%";
    [self.contentView addSubview:_lblPersent];
    
    
    _segment21.frame = CGRectMake(128, 7, 85, 25);
   
    [_segment21 setTitle:@"Prin.& inter." forState:UIControlStateNormal];
    [_segment21 setBackgroundImage:[UIImage imageNamed:@"prin_inter_btn_on.png"] forState:UIControlStateNormal];
    [self.contentView addSubview:_segment21];
    
    
    
    _segment22.frame = CGRectMake(207,25, 85, 25);
    
    
    [_segment22 setBackgroundImage:[UIImage imageNamed:@"interest_btn_off.png"] forState:UIControlStateNormal];
    [self.contentView addSubview:_segment22];
    
    
    
    
   
    [_segment31 setBackgroundImage:[UIImage imageNamed:@"monthly_btn_on.png"] forState:UIControlStateNormal];
    [self.contentView addSubview:_segment31];
    
    
   
    
    [_segment32 setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
    [self.contentView addSubview:_segment32];
    
    
    
    
    [_segment33 setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
    [self.contentView addSubview:_segment33];
    
    CGRect frame = CGRectMake(25.0, 15.0, 236.0, 7.0);
    _slider = [[UISlider alloc] initWithFrame:frame];
    [_slider setBackgroundColor:[UIColor clearColor]];
    _slider.minimumValue = 0;
    _slider.maximumValue = 100;
    _slider.continuous = YES;
    
    
    
    UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"slider_on.png"];
    UIImage *sliderRightTrackImage = [UIImage imageNamed: @"slider_off.png"];
    [_slider setMinimumTrackImage: sliderLeftTrackImage forState: UIControlStateNormal];
    [_slider setMaximumTrackImage: sliderRightTrackImage forState: UIControlStateNormal];
    [_slider setThumbImage:[UIImage imageNamed:@"slider_top.png"] forState:UIControlStateNormal];
    [self.contentView addSubview:_slider];
    
    
    [_txtValues setTextAlignment:UITextAlignmentRight];
    [_txtValues setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    _txtValues.font = [UIFont fontWithName:@"Bookman Old Style" size:15.0];

    _txtValues.backgroundColor = [UIColor clearColor];
    
    [self.contentView addSubview:_txtValues];
    
    
    
    if (self)
    {
        
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
