//
//  LumsumViewController.m
//  Loansactually
//
//  
//  
//

#import "LumsumViewController.h"
#import "LumsumViewCell.h"
#import "Formatter.h"
#import "CustomToolbar.h"


@interface LumsumViewController ()

@end

@implementation LumsumViewController
@synthesize amount = _amount;
@synthesize rate = _rate;
@synthesize term = _term;
@synthesize monthlyRepayment = _monthlyRepayment;
@synthesize totalCostOfLoan = _totalCostOfLoan;
@synthesize totalInterestPayable = _totalInterestPayable;
@synthesize strExtraPayment = _strExtraPayment;
@synthesize strLumpsum  =_strLumpsum;
@synthesize strInterestSaved = _strInterestSaved;
@synthesize strTimeSaved = _strTimeSaved;
@synthesize strUpdatedLoanTerm = _strUpdatedLoanTerm;
@synthesize strUpdatedCostOfLoan = _strUpdatedCostOfLoan;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"EXTRA REPAYMENT CALCULATOR";
    }
    return self;
}
-(void)dealloc
{
    [arrText release];
    [super dealloc];
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    totalInterest = 0.0;
    monthCount = 0;
    tg = 0;
    
    //lblTitle.font = [UIFont fontWithName:@"Bookman Old Style" size:15.0];
    lblTitle.font = [UIFont systemFontOfSize:15.0f];
    arrText = [[NSArray alloc] initWithObjects:@"Loan Amount",@"Interest Rate",@"Loan Term",@"Frequency",@"", nil];
    indexOfRepayment = 1;
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];

    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [_currencyFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [_currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [_currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [_currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"NAD"])
        {
            [_currencyFormatter setCurrencyCode:@"NAD"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        
    }
    
    
    
    _amount = @"0.00";
    _amount = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_amount floatValue]]];
    [_amount retain];
    
    
    _rate = @"0.00";
    _term = @"0.00";
    
    _monthlyRepayment = @"0.00";
    
    _monthlyRepayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_monthlyRepayment floatValue]]];
    [_monthlyRepayment retain];

    
    _totalCostOfLoan = @"0.00";
    
    _totalCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_totalCostOfLoan floatValue]]];
    [_totalCostOfLoan retain];
    
    _totalInterestPayable = @"0.00";

    
    _totalInterestPayable = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_totalInterestPayable floatValue]]];
    [_totalInterestPayable retain];
    
    
    
    
    _strLumpsum = @"0.00";
    
    _strLumpsum = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_strLumpsum floatValue]]];
    [_strLumpsum retain];
    
    _strExtraPayment = @"0.00";
    
    _strInterestSaved = @"0.00";
    
    _strInterestSaved = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_strInterestSaved floatValue]]];
    [_strInterestSaved retain];
    
    
    _strTimeSaved = @"0";
    _strUpdatedLoanTerm =@"0";
    _strUpdatedCostOfLoan = @"0.00";
    
    _strUpdatedCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_strUpdatedCostOfLoan floatValue]]];
    [_strUpdatedCostOfLoan retain];

    
    increased_monthly_repayment = @"0.00";
    
    increased_monthly_repayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[increased_monthly_repayment floatValue]]];
    [increased_monthly_repayment retain];

    
    
    [toolbar setHidden:YES];
    
   
    repayment = @"Monthly";
    
    strRepay = @"Min. monthly repayment";
    [tableView setBackgroundColor:[UIColor clearColor]];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    button.bounds = CGRectMake(0, 0,49.0,29.0);
    [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];

    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    
    //[self.view addSubview:appDelegate.bottomView];
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];

    tableView.backgroundColor = [UIColor clearColor];
    [tableView setBackgroundView:nil];
    [tableView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
    
    
    if(appDelegate.isIPad)
    {
        [self willAnimateRotationToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    }

    
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //[tblView reloadData];
    
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
    }
    [tableView reloadData];
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    if(appDelegate.isIPad)
    {
        return TRUE;
    }
    return FALSE;
    
    /*
    BOOL isPortraite = FALSE;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft||interfaceOrientation == UIInterfaceOrientationLandscapeRight)
        {
            isPortraite = FALSE;
        }
        else
        {
            isPortraite = TRUE;
        }
    } 
    else {
        return YES;
    }
    return isPortraite;
     */
    
}
- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:TRUE];
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    //[self.view addSubview:appDelegate.bottomView];
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(!appDelegate.isIPad)
    {
        if(appDelegate.isIphone5)
        {
            tableView.frame = CGRectMake(0,7,320,438);
        }
        else
        {
            tableView.frame = CGRectMake(0,7,320,370);
        }

    }
    
    
        
    
    /*Sun:0004
     Managing bottom toolbar
     */
    

    if(appDelegate.isIPad)
    {
        
		[self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    	[self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
	}

    
    
    [tableView reloadData];
}
-(IBAction)btnBack_Clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - reset_button_pressed
#pragma mark Resetting result...

-(IBAction)reset_button_pressed:(id)sender
{
    
    
    totalInterest = 0.0;
    monthCount = 0;
    tg = 0;
    
    indexOfRepayment = 1;
    
    _amount = @"0.00";
    
    
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [_currencyFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [_currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [_currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [_currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"NAD"])
        {
            [_currencyFormatter setCurrencyCode:@"NAD"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        
    }
    

    _amount = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_amount floatValue]]];
    [_amount retain];

    
    
    
    _rate = @"0.00";
    _term = @"0.00";
    
    
    
    _monthlyRepayment = @"0.00";
    _monthlyRepayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_monthlyRepayment floatValue]]];
    [_monthlyRepayment retain];
    
    
    
    
    _totalCostOfLoan = @"0.00";
    
    _totalCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_totalCostOfLoan floatValue]]];
    [_totalCostOfLoan retain];
    
    
    _totalInterestPayable = @"0.00";
    
    _totalInterestPayable = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_totalInterestPayable floatValue]]];
    [_totalInterestPayable retain];
    
    
    
    _strLumpsum = @"0.00";
    
    
    _strLumpsum = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_strLumpsum floatValue]]];
    [_strLumpsum retain];
    
    
    _strExtraPayment = @"0.00";
    
    _strExtraPayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_strExtraPayment floatValue]]];
    [_strExtraPayment retain];
    
    
    _strInterestSaved = @"0.00";
    
    _strInterestSaved = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_strInterestSaved floatValue]]];
    [_strInterestSaved retain];
    
    
    _strTimeSaved = @"0";
    _strUpdatedLoanTerm =@"0";
    
    _strUpdatedCostOfLoan = @"0.00";
    _strUpdatedCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[_strUpdatedCostOfLoan floatValue]]];
    [_strUpdatedCostOfLoan retain];

    
    repayment = @"Monthly";
    
    strRepay = @"Min. monthly repayment";
    
    increased_monthly_repayment = @"0.00";
    increased_monthly_repayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithInt:[increased_monthly_repayment floatValue]]];
    [increased_monthly_repayment retain];

    
    
    
    [tableView reloadData];
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

-(IBAction)btnEmail_Clicked:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        nslog(@"%@ %@",_totalInterestPayable,_totalCostOfLoan);
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        
        [picker setSubject:@"Lump sum payment"];
        
        
        NSMutableString *EmailBody1=[[[NSMutableString alloc] initWithString:@"<html><body>"] retain];
        
        [EmailBody1 appendString:@"Input:"];

//        
        [EmailBody1 appendString:[NSString stringWithFormat:@"<table width='150' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>Loan Amount</th><td> %@</td></tr><tr><th>Interest Rate</th><td> %@</td></tr><tr><th>Loan Term</th><td> %@</td></tr><tr><th>Repayment</th><td> %@</td></tr><tr><th>Lump sum repayment</th><td> %@</td></tr><tr><th>Make extra payment at year</th><td> %@</td></tr>",_amount,_rate,_term,repayment,_strLumpsum,_strExtraPayment]];
//        
        
        
        [EmailBody1 appendString:@"</table>"];
        
        
        [EmailBody1 appendString:@"Output1:"];
        
        [EmailBody1 appendString:[NSString stringWithFormat:@"<table width='150' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>%@</th><td> %@</td></tr><tr><th>Total cost of loan</th><td> %@</td></tr><tr><th>Total interest payable</th><td> %@</td></tr>",strRepay,_monthlyRepayment,_totalCostOfLoan,_totalInterestPayable]];
        
        
       
        [EmailBody1 appendString:@"</table>"];
        
        
        [EmailBody1 appendString:@"Output2:"];
        
        [EmailBody1 appendString:[NSString stringWithFormat:@"<table width='150' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>Interest Saved</th><td> %@</td></tr><tr><th>Time Saved</th><td> %@</td></tr><tr><th>Updated Loan Term</th><td> %@</td></tr><tr><th>Updated Cost of Loan</th><td> %@</td></tr>",_strInterestSaved,_strTimeSaved,_strUpdatedLoanTerm,_strUpdatedCostOfLoan]];
        
        
        
        [EmailBody1 appendString:@"</table>"];
        
        
        
        [EmailBody1 appendString:@"</body></html>"];
        
        [picker setMessageBody:EmailBody1 isHTML:YES];
        
        [self presentModalViewController:picker animated:YES];
        [picker release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            nslog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            nslog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            nslog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            nslog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            nslog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)btnCalculator_Clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:TRUE];
}


-(IBAction)btnInfo_Clicekd:(id)sender
{

}

-(IBAction)cancel_button_pressed:(id)sender
{
    [toolbar setHidden:YES];
    [self.view endEditing:TRUE];
    
}

-(IBAction)btnDoneClicked:(id)sender
{
    [toolbar setHidden:YES];
    
    UITextField *textField = (UITextField*)[self.view viewWithTag:tg];
    [textField resignFirstResponder];
    
    UITextField *txtMonthlyRepayment = (UITextField *)[self.view viewWithTag:300];
    UITextField *txtTotalCostOfLoan = (UITextField *)[self.view viewWithTag:301];
    
    
    
    
    _amount = [[_amount componentsSeparatedByCharactersInSet:
                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                         componentsJoinedByString:@""];

    
    _amount = [NSString stringWithFormat:@"%f",[_amount floatValue]/100];
    [_amount retain];
    
    NSString *strAmount = _amount;
    NSString *strRate = _rate;
    NSString *strTerms = _term;
    
    
    /*
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"," withString:@""];
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"$" withString:@""];
   */
    
    
    strRate =  [strRate stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    nslog(@"_rate == %@",_rate);
    
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [_currencyFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [_currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [_currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [_currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"NAD"])
        {
            [_currencyFormatter setCurrencyCode:@"NAD"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        
    }

    
    
    _amount = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:[_amount floatValue]]];
    [_amount retain];
    
    
    double amounts = [strAmount doubleValue];
    
    nslog(@"\n amounts = %f",amounts);
    
    
    double originalAmt = amounts;
    double rates =  [strRate floatValue];
    double terms = [strTerms floatValue];
    
    
    _strLumpsum = [[_strLumpsum componentsSeparatedByCharactersInSet:
                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                   componentsJoinedByString:@""];
    
    
    _strLumpsum = [NSString stringWithFormat:@"%f",[_strLumpsum floatValue]/100];
    [_strLumpsum retain];
    
    
    float temp = [_strLumpsum floatValue];
    _strLumpsum = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_strLumpsum retain];
    

    
    
    if (indexOfRepayment == 1)
    {
        double r = rates/1200;
        int totalMonth = terms *12;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
        
        //txtMonthlyRepayment.text = [NSString stringWithFormat:@"$%.2f",monthlyRepayment];
        
        double temp = monthlyRepayment;
        txtMonthlyRepayment.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        [txtMonthlyRepayment.text retain];
        
        
        
        
        double totalCostOfLoan = monthlyRepayment *(terms*12);
        
        //txtTotalCostOfLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        
        temp = totalCostOfLoan;
        txtTotalCostOfLoan.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
      
        
        
        _totalCostOfLoan =  [NSString stringWithFormat:@"%@",txtTotalCostOfLoan.text];
        [_totalCostOfLoan retain];
        
        double totalInterestPayable = totalCostOfLoan - amounts;
        
        temp = totalInterestPayable;
        NSString*str_totalInterestPayable = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        [str_totalInterestPayable retain];
        
        
        _totalInterestPayable = [NSString stringWithFormat:@"%@",str_totalInterestPayable];
        _monthlyRepayment = txtMonthlyRepayment.text;
        
        
        
        [_monthlyRepayment retain];
        
        [_totalInterestPayable retain];
        
        
        int extraPaymentMonth = [_strExtraPayment intValue]*12;
        
        for (int i =0; i<extraPaymentMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
        }
        
        
               
        /*
        NSString *strlumpsum = [_strLumpsum stringByReplacingOccurrencesOfString:@"$" withString:@""];
        strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
        */
        
        NSString *strlumpsum = [[_strLumpsum componentsSeparatedByCharactersInSet:[NSCharacterSet  symbolCharacterSet]] componentsJoinedByString:@""];
        
        
        
        
        double lumpsumValue = [strlumpsum doubleValue];
        
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            if (amounts<0) {
                break;
            }
        }
        
        
        
        double updatedCostOfloan = originalAmt + totalInterest;
        
        temp = updatedCostOfloan;
        _strUpdatedCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
        [_strUpdatedCostOfLoan retain];
        
        /*Sun:0004 Original
        _strUpdatedCostOfLoan = [NSString stringWithFormat:@"$%.2f",updatedCostOfloan];
        [_strUpdatedCostOfLoan retain];
        
         */
        
         nslog(@"\n totalCostOfLoan = %f",totalCostOfLoan);
         nslog(@"\n originalAmt = %f",originalAmt);
         nslog(@"\n totalInterest = %f",totalInterest);
        
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;
        
        
        nslog(@"\n interestsaved = %f",interestsaved);
        
        temp = interestsaved;
        _strInterestSaved = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
        [_strInterestSaved retain];
        
        
        /*
        _strInterestSaved = [NSString stringWithFormat:@"$%.2f",interestsaved];
        [_strInterestSaved retain];
        */
        
         
         
        int UpdatedTermInMonth = monthCount % 12;
        int UpdatedTermInYear = monthCount / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        int remainMonth = totalMonth - monthCount;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
    }
    else if (indexOfRepayment == 2)
    {
        
        double r = rates/2600;
        int totalMonth = terms *26;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
        
        
        
        double temp = monthlyRepayment;
        txtMonthlyRepayment.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        
        [txtMonthlyRepayment.text retain];

        
        
        
        
        
        double totalCostOfLoan = monthlyRepayment *(terms*26);
        
        //txtTotalCostOfLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
       
        temp = totalCostOfLoan;
        txtTotalCostOfLoan.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        
        
        _totalCostOfLoan =  [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        [_totalCostOfLoan retain];
        
        double totalInterestPayable = totalCostOfLoan - amounts;
        
        temp = totalInterestPayable;
        NSString*str_totalInterestPayable = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        [str_totalInterestPayable retain];
        
        _totalInterestPayable = [NSString stringWithFormat:@"$%@",str_totalInterestPayable];
        
        
        
        
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        
        [_totalInterestPayable retain];
        
        int extraPaymentMonth = [_strExtraPayment intValue]*26;
        
        
        
        
        
        
        for (int i =0; i<extraPaymentMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            
        }
        
        
        
        
        NSString *strlumpsum = [[_strLumpsum componentsSeparatedByCharactersInSet:[NSCharacterSet  symbolCharacterSet]] componentsJoinedByString:@""];
        
        
        strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
        double lumpsumValue = [strlumpsum doubleValue];
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            
            if (amounts<0)
            {
                break;
            }
        }
        int fortnight = roundf((monthCount*12.0)/26.0);
        
        double updatedCostOfloan = originalAmt + totalInterest;
        
        temp = updatedCostOfloan;
        _strUpdatedCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
        [_strUpdatedCostOfLoan retain];

        
        
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;

        
        temp = interestsaved;
        _strInterestSaved = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
        [_strInterestSaved retain];
        
        
        int UpdatedTermInMonth = fortnight % 12;
        int UpdatedTermInYear = fortnight / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        totalMonth= roundf((totalMonth*12.0)/26.0);
        int remainMonth = totalMonth - fortnight;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
    }
    else if (indexOfRepayment == 3)
    {
        double r = rates/5200;
        int totalMonth = terms *52;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
        
        
        double temp = monthlyRepayment;
        txtMonthlyRepayment.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        [txtMonthlyRepayment.text retain];

        
        
        
        
        
        
        double totalCostOfLoan = monthlyRepayment *(terms*52);
        
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        _totalCostOfLoan =  [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        [_totalCostOfLoan retain];
        double totalInterestPayable = totalCostOfLoan - amounts;
        _totalInterestPayable = [NSString stringWithFormat:@"$%.2f",totalInterestPayable];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        [_totalInterestPayable retain];
        
        int extraPaymentMonth = [_strExtraPayment intValue]*52;
        
        for (int i =0; i<extraPaymentMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            
        }
        NSString *strlumpsum = [_strLumpsum stringByReplacingOccurrencesOfString:@"$" withString:@""];
        strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
        double lumpsumValue = [strlumpsum doubleValue];
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            if (amounts<0) {
                break;
            }
        }
        
        int fortnight = roundf((monthCount*12.0)/52.0);        
        double updatedCostOfloan = originalAmt + totalInterest;
        _strUpdatedCostOfLoan = [NSString stringWithFormat:@"$%.2f",updatedCostOfloan];
        [_strUpdatedCostOfLoan retain];
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;
        
        
        
        _strInterestSaved = [NSString stringWithFormat:@"$%.2f",interestsaved];
        [_strInterestSaved retain];
        
        
        
        
        int UpdatedTermInMonth = fortnight % 12;
        int UpdatedTermInYear = fortnight / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        totalMonth= roundf((totalMonth*12.0)/52.0);
        int remainMonth = totalMonth - fortnight;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
        
        
    }
    
    
    
    increased_monthly_repayment =   _monthlyRepayment;
    
  
    
    
    NSString *clean_monthly_repayment = [[_monthlyRepayment
                                  componentsSeparatedByCharactersInSet:
                                  [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                 componentsJoinedByString:@""];
    
    
    NSString *clean_str_lumpsum = [[_strLumpsum
                                          componentsSeparatedByCharactersInSet:
                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                         componentsJoinedByString:@""];
    

        
    temp = [clean_monthly_repayment floatValue]/100+ [clean_str_lumpsum floatValue]/100;
    increased_monthly_repayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [increased_monthly_repayment retain];
    
    
    
    
    
    
    
    monthCount = 0;
    totalInterest = 0.0;
    
    [tableView reloadData];
    
   
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(selectKeyboard) userInfo:nil repeats:NO];
 

    

}

-(void)selectKeyboard
{
    UITextField*tempTextField = (UITextField*)[self.view viewWithTag:current_tag+1];
    nslog(@"\n text = %@",tempTextField.text);
    if(tempTextField!=nil)
    {
        [tempTextField becomeFirstResponder];
    }
    
}


#pragma mark TableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == 1)
    {
        return 260;
    }
    return 0.0;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(section == 1)
    {
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                
                footer_view.frame = CGRectMake(0, 0,680, 254);
                reset_bottom_button.center = CGPointMake(384,40);
                
                
                important_note_label.frame = CGRectMake(0, 0,500,132);
                important_note_label.center= CGPointMake(384,120);
                /*
                reset_bottom_button.frame = CGRectMake(1000,reset_bottom_button.frame.origin.y,reset_bottom_button.frame.size.width, reset_bottom_button.frame.size.height);
                 */
                
            }
            else
            {
                footer_view.frame = CGRectMake(0, 0,936, 254);
                reset_bottom_button.center = CGPointMake(512,40);
                important_note_label.frame = CGRectMake(0, 0,500,132);
                important_note_label.center= CGPointMake(512,120);
            }
            
        }
        
        return footer_view;
    }
    return nil;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger cnt = 0;
    if (section == 0) 
    {
        cnt = 5;
    }
    else if (section == 1)
    {
        cnt = 4;
    }
       
    return cnt;
}


- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if(section == 3)
    {
        return @" Important Note: The results from all calculators are approximate guide only. These results are not a quote, a pre-qualification for a loan, a loan offer, or investment advice. The calculations are not intended to be a substitute for professional financial advice.Before taking out a loan you should consult your mortgage broker. ";
    }
    return @"";
}







- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    /*
    if(indexPath.section==1)
    {
        
        if(indexPath.row==1)
        {
            return 0.0f;
        }
        
    }

    
    
       
   else if(indexPath.section==3)
    {
        
        if(indexPath.row==2 || indexPath.row==3)
        {
            return 0.0f;
        }
        
    }

    */
    
    
    

    
    
    if(indexPath.section == 0)
    {
        
        if(indexPath.row==3)
        {
           
            if(!appDelegate.isIPad)
            {
                return 67;
            }

            
            return 55;
        }
           
        
    }
    
    return 40.0f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    LumsumViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[[LumsumViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    
    cell.txtValues.hidden = TRUE;
    cell.segment21.hidden = TRUE;
    cell.segment22.hidden = TRUE;
    cell.segment31.hidden = TRUE;
    cell.segment32.hidden = TRUE;
    cell.segment33.hidden = TRUE;
    cell.slider.hidden = TRUE;
    cell.lblPersent.hidden = TRUE;
    cell.cellImageView.hidden = TRUE;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    cell.txtValues.tag = 100*(indexPath.section+1) + indexPath.row;

    
    
    
    cell.lblTitle.font = [UIFont systemFontOfSize:15.0f];
    cell.txtValues.font = [UIFont systemFontOfSize:15.0f];
    
    
    
    /*
    cell.lblTitle.textColor =[UIColor colorWithRed:56.0/255.0f green:49.0f/255.0f blue:49.0f/255.0f alpha:1.0f];
    cell.txtValues.textColor = [UIColor colorWithRed:3.0f/255.0f green:80.0/255.0 blue:103.0f/255.0 alpha:1.0];
    cell.lblPersent.textColor = [UIColor colorWithRed:3.0f/255.0f green:80.0/255.0 blue:103.0f/255.0 alpha:1.0];
*/
    
    
    if (indexPath.section == 0)
    {   
        cell.txtValues.delegate = self;
        cell.lblTitle.text = [arrText objectAtIndex:indexPath.row];

        
        
         if (indexPath.row == 0) 
        {
            
            cell.imageView.image = [UIImage imageNamed:@"loan_amount.png"];
            
            //cell.txtValues.frame = CGRectMake(150, 10, 140, 20);
            
            
            [cell.txtValues setHidden:NO];
            [cell.txtValues setEnabled:YES];
            
            nslog(@"\n _amount in cell for = %@",_amount);
            
            cell.txtValues.text = _amount;
           
        }
        else if (indexPath.row == 1) 
        {
            
            cell.imageView.image = [UIImage imageNamed:@"interest_rate.png"];
            cell.lblPersent.frame = CGRectMake(280, 10, 20, 20);
            //cell.txtValues.frame = CGRectMake(150, 10, 120, 20);
            //[cell.lblPersent setHidden:NO];
            [cell.txtValues setEnabled:YES];
            [cell.txtValues setHidden:NO];
            cell.txtValues.text = _rate;
            
            //            cell.txtValues.delegate = self;
        }
        else if (indexPath.row == 2) 
        {
            
            cell.imageView.image = [UIImage imageNamed:@"loan_term.png"];
            
           
            
            
            cell.txtValues.text = _term;
            [cell.txtValues setEnabled:YES];
            [cell.txtValues setHidden:NO];
            
           // [cell.txtValues setFrame:CGRectMake(230, 10, 60, 20)];
            [cell.slider setHidden:YES];
        }
        else if (indexPath.row == 3) 
        {
            [cell.txtValues setHidden:YES];
            
           // cell.imageView.image = [UIImage imageNamed:@"frequency.png"];
            
            UIImageView*frequency_imageView = [[UIImageView alloc] init];
            frequency_imageView.frame = CGRectMake(0, 0,40,40);
            frequency_imageView.image = [UIImage imageNamed:@"frequency.png"];
            [cell.contentView addSubview:frequency_imageView];
            [frequency_imageView release];
            
            
            [cell.segment31 setHidden:NO];
            cell.segment31.tag = 3001;
            [cell.segment31 addTarget:self action:@selector(btnSegment3_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.segment32 setHidden:NO];
            cell.segment32.tag = 3002;
            [cell.segment32 addTarget:self action:@selector(btnSegment3_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.segment33 setHidden:NO];
            cell.segment33.tag = 3003;
            [cell.segment33 addTarget:self action:@selector(btnSegment3_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            
            if(indexOfRepayment == 1)
            {
                [cell.segment31 setBackgroundImage:[UIImage imageNamed:@"monthly_btn_on.png"] forState:UIControlStateNormal];
                [cell.segment32 setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.segment33 setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            if(indexOfRepayment == 2)
            {
                [cell.segment31 setBackgroundImage:[UIImage imageNamed:@"monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.segment32 setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_on.png"] forState:UIControlStateNormal];
                [cell.segment33 setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            if(indexOfRepayment == 3)
            {
                [cell.segment31 setBackgroundImage:[UIImage imageNamed:@"monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.segment32 setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.segment33 setBackgroundImage:[UIImage imageNamed:@"weekly_btn_on.png"] forState:UIControlStateNormal];
            }
        }
        else if(indexPath.row==4)
        {
            cell.txtValues.tag = 200;
           // cell.txtValues.frame = CGRectMake(150, 10, 140, 20);
            cell.imageView.image = [UIImage imageNamed:@"extra_repayment.png"];
            
            
            cell.txtValues.text = _strLumpsum;
            
            [cell.txtValues setHidden:NO];
            [cell.txtValues setEnabled:YES];
            
            //cell.lblTitle.text = @"Lump sum repayment";
            cell.lblTitle.text = @"Extra Repayment";

            
        }
        
    }
    else if (indexPath.section == 1)
    {
         cell.txtValues.tag = 100*(indexPath.section+2) + indexPath.row;
        

        [cell.txtValues setEnabled:NO];
        [cell.txtValues setHidden:NO];
        [cell.segment21 setHidden:YES];
        [cell.segment22 setHidden:YES];
        [cell.segment31 setHidden:YES];
        [cell.segment32 setHidden:YES];
        [cell.segment33 setHidden:YES];
        [cell.slider setHidden:YES];
        
        cell.lblTitle.frame = CGRectMake(10, 10, 200, 20);
        
        
        cell.txtValues.textColor =[UIColor whiteColor];
        cell.lblTitle.textColor = [UIColor whiteColor];
        
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                 cell.txtValues.frame = CGRectMake(500, 10, 140, 20);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                 cell.txtValues.frame = CGRectMake(756, 10, 140, 20);
            }
        }
        else
        {
            cell.txtValues.frame = CGRectMake(150, 10, 140, 20);
        }
        
        
        
        
        if (indexPath.row == 0)
        {
            
            cell.lblTitle.tag = 1111;
           
            cell.lblTitle.text = strRepay;
            
            nslog(@"\n _monthlyRepayment = %@",_monthlyRepayment);
            
            cell.txtValues.text = _monthlyRepayment;
            [cell.cellImageView setHidden:NO];
            [cell.lblTitle bringSubviewToFront:self.view];
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                     cell.cellImageView.image = [UIImage imageNamed:@"ipad_port_calculation_summary_top_cell_bg.png"];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                     cell.cellImageView.image = [UIImage imageNamed:@"ipad_land_calculation_summary_top_cell_bg.png"];
                }
            }
            else
            {
                cell.cellImageView.image = [UIImage imageNamed:@"calculation_summary_top_cell_bg.png"];
            }
            
            
        }
        else if (indexPath.row == 1)
        {
            /*Sun:0004
             Changed
             */
            
            //cell.hidden = TRUE;
            cell.lblTitle.text = @"Increased month repayment";
            
            nslog(@"\n increased_monthly_repayment = %@",increased_monthly_repayment);
            
            cell.txtValues.text = increased_monthly_repayment;
            [cell.cellImageView setHidden:NO];
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    cell.cellImageView.image = [UIImage imageNamed:@"ipad_port_calculation_summary_middle_cell_bg.png"];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    cell.cellImageView.image = [UIImage imageNamed:@"ipad_land_calculation_summary_middle_cell_bg.png"];
                }
            }
            else
            {
                cell.cellImageView.image = [UIImage imageNamed:@"calculation_summary_middle_cell_bg.png"];
            }
            
            
        }
        else if (indexPath.row == 2)
        {
            cell.lblTitle.tag = 1111;
            
            cell.lblTitle.text = @"Interest Saved";
            cell.txtValues.text = _strInterestSaved;
            [cell.cellImageView setHidden:NO];
            
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    cell.cellImageView.image = [UIImage imageNamed:@"ipad_land_calculation_summary_middle_cell_bg.png"];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    cell.cellImageView.image = [UIImage imageNamed:@"ipad_port_calculation_summary_middle_cell_bg.png"];
                }
            }
            else
            {
                cell.cellImageView.image = [UIImage imageNamed:@"calculation_summary_middle_cell_bg.png"];
            }
            
            

        }
        else if(indexPath.row == 3)
        {
            cell.lblTitle.text = @"Time Saved";
            cell.txtValues.text = _strTimeSaved;
            [cell.cellImageView setHidden:NO];
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    cell.cellImageView.image = [UIImage imageNamed:@"ipad_port_calculation_summary_bottom_cell_bg.png"];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                   cell.cellImageView.image = [UIImage imageNamed:@"ipad_land_calculation_summary_bottom_cell_bg.png"];
                }
            }
            else
            {
                cell.cellImageView.image = [UIImage imageNamed:@"calculation_summary_bottom_cell_bg.png"];
            }
            
            
            
        }
        
    }

    return cell;
}
-(void)changeValue:(id)sender
{
    UITextField *txt1 = (UITextField *) [self.view viewWithTag:201];
    UISlider *slider1 =(UISlider*) [self.view viewWithTag:3333];
    double v = [txt1.text doubleValue];
    UISlider *slider = (UISlider*) sender;
    [slider setThumbImage:[UIImage imageNamed:@"slider_top.png"] forState:UIControlStateHighlighted];
    
    //    txt1.text = [NSString stringWithFormat:@"%.f", roundf(slider.value)];
    
    double value = roundf(slider.value);
    value = value/2;
    
    if (v>value) {
        v= value;
    }
    slider1.value = v*2;
    txt1.text = [NSString stringWithFormat:@"%.1f", v];
    [toolbar setHidden:YES];
    UITextField *txtAmount = (UITextField *)[self.view viewWithTag:100];
    UITextField *txtRate = (UITextField *)[self.view viewWithTag:101];
    UITextField *txtTerms = (UITextField *)[self.view viewWithTag:102];
    
    [txtAmount resignFirstResponder];
    [txtRate resignFirstResponder];
    txtTerms.text = [NSString stringWithFormat:@"%.1f", value];
    _term = txtTerms.text;
    [_term retain];
    
//    UITextField *txtMonthlyRepayment = (UITextField *)[self.view viewWithTag:300];
//    UITextField *txtTotalCostOfLoan = (UITextField *)[self.view viewWithTag:301];
    
    UITextField *txtMonthlyRepayment = (UITextField *)[self.view viewWithTag:300];
    UITextField *txtTotalCostofLoan = (UITextField *)[self.view viewWithTag:301];
    UITextField *txtTotalInterestPayable = (UITextField *)[self.view viewWithTag:302];
    
    UITextField *txtInterestSaved = (UITextField *)[self.view viewWithTag:400];
    UITextField *txtTimeSaved = (UITextField *)[self.view viewWithTag:401];
    UITextField *txtUpatedLoanTerm = (UITextField *)[self.view viewWithTag:402];
    UITextField *txtUpdatedCostOfLoan = (UITextField *)[self.view viewWithTag:403];
    
    
    
    _amount = [[_amount componentsSeparatedByCharactersInSet:
                [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
               componentsJoinedByString:@""];
    
    
    _amount = [NSString stringWithFormat:@"%f",[_amount floatValue]/100];
    [_amount retain];

    
    NSString *strAmount = _amount;
    NSString *strRate = _rate;
    NSString *strTerms = _term;
    
   // strAmount = [strAmount stringByReplacingOccurrencesOfString:@"," withString:@""];
   // strAmount = [strAmount stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    strRate =  [strRate stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [_currencyFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [_currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [_currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [_currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"NAD"])
        {
            [_currencyFormatter setCurrencyCode:@"NAD"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        
    }

    
    
    _amount = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:[_amount floatValue]]];
    [_amount retain];

    
    
    //    double ans = 0.0;
    double amounts = [strAmount doubleValue];
    double originalAmt = amounts;
    double rates =  [strRate floatValue];
    double terms = [strTerms floatValue];
    
    
    
    _strLumpsum = [[_strLumpsum componentsSeparatedByCharactersInSet:
                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                   componentsJoinedByString:@""];
    
    
    _strLumpsum = [NSString stringWithFormat:@"%f",[_strLumpsum floatValue]/100];
    [_strLumpsum retain];
    
    
    float temp = [_strLumpsum floatValue];
    _strLumpsum = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_strLumpsum retain];

    
    
    if (indexOfRepayment == 1)
    {
        double r = rates/1200;
        int totalMonth = terms *12;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
       
        if (isnan(monthlyRepayment)) {
            monthlyRepayment = 0.00;
        }
      
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"$%.2f",monthlyRepayment];
        double totalCostOfLoan = monthlyRepayment *(terms*12);
      
        if (isnan(totalCostOfLoan)) {
            totalCostOfLoan = 0.00;
        }
        
        txtTotalCostofLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        _totalCostOfLoan =  [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        [_totalCostOfLoan retain];
        
        double totalInterestPayable = totalCostOfLoan - amounts;
        
        if (isnan(totalInterestPayable)) {
            totalInterestPayable = 0.00;
        }
        _totalInterestPayable = [NSString stringWithFormat:@"$%.2f",totalInterestPayable];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        [_totalInterestPayable retain];
      
        int extraPaymentMonth = [_strExtraPayment intValue]*12;
        
        for (int i =0; i<extraPaymentMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
           
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
        }
        
        
                

        
        
        NSString *strlumpsum = [_strLumpsum stringByReplacingOccurrencesOfString:@"$" withString:@""];
        strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
        double lumpsumValue = [strlumpsum doubleValue];
       
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
           
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
           
            if (amounts<0) {
                break;
            }
        }
               
        double updatedCostOfloan = originalAmt + totalInterest;
        _strUpdatedCostOfLoan = [NSString stringWithFormat:@"$%.2f",updatedCostOfloan];
        [_strUpdatedCostOfLoan retain];
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;
        
        
        
        
        _strInterestSaved = [NSString stringWithFormat:@"$%.2f",interestsaved];
        [_strInterestSaved retain];
        
        int UpdatedTermInMonth = monthCount % 12;
        int UpdatedTermInYear = monthCount / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        int remainMonth = totalMonth - monthCount;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
    }
    else if (indexOfRepayment == 2)
    {
        
        double r = rates/2600;
        int totalMonth = terms *26;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
        if (isnan(monthlyRepayment)) {
            monthlyRepayment = 0.00;
        }
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"$%.2f",monthlyRepayment];
        double totalCostOfLoan = monthlyRepayment *(terms*26);
        if (isnan(totalCostOfLoan)) {
            totalCostOfLoan = 0.00;
        }
        
        txtTotalCostofLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        _totalCostOfLoan =  [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        [_totalCostOfLoan retain];
       
        double totalInterestPayable = totalCostOfLoan - amounts;
        if (isnan(totalInterestPayable)) {
            totalInterestPayable = 0.00;
        }
        _totalInterestPayable = [NSString stringWithFormat:@"$%.2f",totalInterestPayable];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        [_totalInterestPayable retain];
        int extraPaymentMonth = [_strExtraPayment intValue]*26;
        for (int i =0; i<extraPaymentMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
        }
        
               

        
        
        
        NSString *strlumpsum = [_strLumpsum stringByReplacingOccurrencesOfString:@"$" withString:@""];
        strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
        double lumpsumValue = [strlumpsum doubleValue];
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
         
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            if (amounts<0) {
                break;
            }
        }
        int fortnight = roundf((monthCount*12.0)/26.0);
       
        
        double updatedCostOfloan = originalAmt + totalInterest;
        _strUpdatedCostOfLoan = [NSString stringWithFormat:@"$%.2f",updatedCostOfloan];
        [_strUpdatedCostOfLoan retain];
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;
        _strInterestSaved = [NSString stringWithFormat:@"$%.2f",interestsaved];
        [_strInterestSaved retain];
        
        int UpdatedTermInMonth = fortnight % 12;
        int UpdatedTermInYear = fortnight / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        totalMonth= roundf((totalMonth*12.0)/26.0);
        int remainMonth = totalMonth - fortnight;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
    }
    else if (indexOfRepayment == 3)
    {
        double r = rates/5200;
        int totalMonth = terms *52;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
       
        if (isnan(monthlyRepayment))
        {
            monthlyRepayment = 0.00;
        }
        
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"$%.2f",monthlyRepayment];
        double totalCostOfLoan = monthlyRepayment *(terms*52);
        if (isnan(totalCostOfLoan))
        {
            totalCostOfLoan = 0.00;
        }
        
        txtTotalCostofLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        _totalCostOfLoan =  [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        [_totalCostOfLoan retain];
       
        double totalInterestPayable = totalCostOfLoan - amounts;
        if (isnan(totalInterestPayable))
        {
            totalInterestPayable = 0.00;
        }
        
        _totalInterestPayable = [NSString stringWithFormat:@"$%.2f",totalInterestPayable];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        [_totalInterestPayable retain];
        int extraPaymentMonth = [_strExtraPayment intValue]*52;
        for (int i =0; i<extraPaymentMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
          
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
        }
        NSString *strlumpsum = [_strLumpsum stringByReplacingOccurrencesOfString:@"$" withString:@""];
        strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
        double lumpsumValue = [strlumpsum doubleValue];
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            if (amounts<0) {
                break;
            }
        }
        
        int fortnight = roundf((monthCount*12.0)/52.0);
        double updatedCostOfloan = originalAmt + totalInterest;
        _strUpdatedCostOfLoan = [NSString stringWithFormat:@"$%.2f",updatedCostOfloan];
        [_strUpdatedCostOfLoan retain];
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;
        _strInterestSaved = [NSString stringWithFormat:@"$%.2f",interestsaved];
        [_strInterestSaved retain];
        
        int UpdatedTermInMonth = fortnight % 12;
        int UpdatedTermInYear = fortnight / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        totalMonth= roundf((totalMonth*12.0)/52.0);
        int remainMonth = totalMonth - fortnight;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
        
        
    }      
    
    
    increased_monthly_repayment =   _monthlyRepayment;
    
    
    
    
    NSString *clean_monthly_repayment = [[_monthlyRepayment
                                          componentsSeparatedByCharactersInSet:
                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                         componentsJoinedByString:@""];
    
    
    NSString *clean_str_lumpsum = [[_strLumpsum
                                    componentsSeparatedByCharactersInSet:
                                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                   componentsJoinedByString:@""];
    
    
    
    increased_monthly_repayment = [NSString stringWithFormat:@"$%.2f",[clean_monthly_repayment floatValue]/100+ [clean_str_lumpsum floatValue]/100];
    
    [increased_monthly_repayment retain];

    
    
    
    txtMonthlyRepayment.text = _monthlyRepayment;
    txtTotalCostofLoan.text = _totalCostOfLoan;
    txtTotalInterestPayable.text = _totalInterestPayable;
    
    txtInterestSaved.text = _strInterestSaved;
    txtTimeSaved.text = _strTimeSaved;
    txtUpatedLoanTerm.text = _strUpdatedLoanTerm;
    txtUpdatedCostOfLoan.text = _strUpdatedCostOfLoan;
    
    if ([_monthlyRepayment isEqualToString:@"$nan"])
    {
        _monthlyRepayment = @"$0.00";
    }
    
    if ([_totalCostOfLoan isEqualToString:@"$nan"])
    {
        _totalCostOfLoan = @"$0.00";
    }
    if ([_totalInterestPayable isEqualToString:@"$nan"])
    {
        _totalInterestPayable = @"$0.00";
    }
    
    if ([_strInterestSaved isEqualToString:@"$nan"])
    {
        _strInterestSaved = @"$0.00";
    }
    if ([_strUpdatedCostOfLoan isEqualToString:@"$nan"])
    {
        _strUpdatedCostOfLoan = @"$0.00";
    }
    
    monthCount = 0;
    totalInterest = 0.0;

}
-(void)changeValue1:(id)sender
{
    
    UITextField *textField = (UITextField*)[self.view viewWithTag:tg];
    [textField resignFirstResponder];
    UISlider *slider = (UISlider*) sender;
    [slider setThumbImage:[UIImage imageNamed:@"slider_top.png"] forState:UIControlStateHighlighted];
    double v =[_term doubleValue]*2;
    
    double value = roundf(slider.value);
   
    if (value>v) 
    {
        value = v;
    }
     value = value/2;
    [toolbar setHidden:YES];
    UITextField *txtAmount = (UITextField *)[self.view viewWithTag:100];
    UITextField *txtRate = (UITextField *)[self.view viewWithTag:101];
    UITextField *txtTerms = (UITextField *)[self.view viewWithTag:201];
    
    [txtAmount resignFirstResponder];
    [txtRate resignFirstResponder];
    txtTerms.text = [NSString stringWithFormat:@"%.1f", value];
    slider.value = value*2;
    _strExtraPayment = txtTerms.text;
    [_strExtraPayment retain];
   
    
    UITextField *txtMonthlyRepayment = (UITextField *)[self.view viewWithTag:300];
    UITextField *txtTotalCostofLoan = (UITextField *)[self.view viewWithTag:301];
    UITextField *txtTotalInterestPayable = (UITextField *)[self.view viewWithTag:302];
    
    UITextField *txtInterestSaved = (UITextField *)[self.view viewWithTag:400];
    UITextField *txtTimeSaved = (UITextField *)[self.view viewWithTag:401];
    UITextField *txtUpatedLoanTerm = (UITextField *)[self.view viewWithTag:402];
    UITextField *txtUpdatedCostOfLoan = (UITextField *)[self.view viewWithTag:403];
    
    
    _amount = [[_amount componentsSeparatedByCharactersInSet:
                [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
               componentsJoinedByString:@""];
    
    
    _amount = [NSString stringWithFormat:@"%f",[_amount floatValue]/100];
    [_amount retain];
    
    
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
   
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [_currencyFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [_currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [_currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [_currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"NAD"])
        {
            [_currencyFormatter setCurrencyCode:@"NAD"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        
    }


    
    
    NSString *strAmount = _amount;
    NSString *strRate = _rate;
    NSString *strTerms = _term;
    
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"," withString:@""];
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"$" withString:@""];
    strRate =  [strRate stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    
    _amount = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:[_amount floatValue]]];
    [_amount retain];
    
    
    double amounts = [strAmount doubleValue];
    double originalAmt = amounts;
    double rates =  [strRate floatValue];
    double terms = [strTerms floatValue];
    
    
    _strLumpsum = [[_strLumpsum componentsSeparatedByCharactersInSet:
                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                   componentsJoinedByString:@""];
    
    
    _strLumpsum = [NSString stringWithFormat:@"%f",[_strLumpsum floatValue]/100];
    [_strLumpsum retain];
    
    
    float temp = [_strLumpsum floatValue];
    _strLumpsum = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_strLumpsum retain];
    

    
    if (indexOfRepayment == 1)
    {
        double r = rates/1200;
        int totalMonth = terms *12;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
        if (isnan(monthlyRepayment)) {
            monthlyRepayment = 0.00;
        }
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"$%.2f",monthlyRepayment];
        double totalCostOfLoan = monthlyRepayment *(terms*12);
        if (isnan(totalCostOfLoan)) {
            totalCostOfLoan = 0.00;
        }

        txtTotalCostofLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        _totalCostOfLoan =  [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        [_totalCostOfLoan retain];
        
        double totalInterestPayable = totalCostOfLoan - amounts;
        if (isnan(totalInterestPayable)) {
            totalInterestPayable = 0.00;
        }

        
        _totalInterestPayable = [NSString stringWithFormat:@"$%.2f",totalInterestPayable];
       
        
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        [_totalInterestPayable retain];
        int extraPaymentMonth = [_strExtraPayment intValue]*12;
        
       
        for (int i =0; i<extraPaymentMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
        }
        NSString *strlumpsum = [_strLumpsum stringByReplacingOccurrencesOfString:@"$" withString:@""];
        strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
        double lumpsumValue = [strlumpsum doubleValue];
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            if (amounts<0) {
                break;
            }
        }
        
        double updatedCostOfloan = originalAmt + totalInterest;
        _strUpdatedCostOfLoan = [NSString stringWithFormat:@"$%.2f",updatedCostOfloan];
        [_strUpdatedCostOfLoan retain];
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;
        _strInterestSaved = [NSString stringWithFormat:@"$%.2f",interestsaved];
        [_strInterestSaved retain];
        
        int UpdatedTermInMonth = monthCount % 12;
        int UpdatedTermInYear = monthCount / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        int remainMonth = totalMonth - monthCount;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
    }
    else if (indexOfRepayment == 2)
    {
        
        double r = rates/2600;
        int totalMonth = terms *26;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
        if (isnan(monthlyRepayment)) {
            monthlyRepayment = 0.00;
        }
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"$%.2f",monthlyRepayment];
        double totalCostOfLoan = monthlyRepayment *(terms*26);
        if (isnan(totalCostOfLoan)) {
            totalCostOfLoan = 0.00;
        }
        txtTotalCostofLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        _totalCostOfLoan =  [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        [_totalCostOfLoan retain];
        
        double totalInterestPayable = totalCostOfLoan - amounts;
        if (isnan(totalInterestPayable)) {
            totalInterestPayable = 0.00;
        }
        _totalInterestPayable = [NSString stringWithFormat:@"$%.2f",totalInterestPayable];
      
        
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        [_totalInterestPayable retain];
        
       
        int extraPaymentMonth = [_strExtraPayment intValue]*26;
        for (int i =0; i<extraPaymentMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
        }
        NSString *strlumpsum = [_strLumpsum stringByReplacingOccurrencesOfString:@"$" withString:@""];
        strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
        double lumpsumValue = [strlumpsum doubleValue];
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            if (amounts<0) {
                break;
            }
        }
        int fortnight = roundf((monthCount*12.0)/26.0);
        
        
        double updatedCostOfloan = originalAmt + totalInterest;
        _strUpdatedCostOfLoan = [NSString stringWithFormat:@"$%.2f",updatedCostOfloan];
        [_strUpdatedCostOfLoan retain];
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;
        _strInterestSaved = [NSString stringWithFormat:@"$%.2f",interestsaved];
        [_strInterestSaved retain];
        
        int UpdatedTermInMonth = fortnight % 12;
        int UpdatedTermInYear = fortnight / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        totalMonth= roundf((totalMonth*12.0)/26.0);
        int remainMonth = totalMonth - fortnight;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
    }
    else if (indexOfRepayment == 3)
    {
        double r = rates/5200;
        int totalMonth = terms *52;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
        if (isnan(monthlyRepayment)) {
            monthlyRepayment = 0.00;
        }
        txtMonthlyRepayment.text = [NSString stringWithFormat:@"$%.2f",monthlyRepayment];
        double totalCostOfLoan = monthlyRepayment *(terms*52);
        //        double t = ans;
        if (isnan(totalCostOfLoan)) {
            totalCostOfLoan = 0.00;
        }
        txtTotalCostofLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        _totalCostOfLoan =  [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        [_totalCostOfLoan retain];
        
        double totalInterestPayable = totalCostOfLoan - amounts;
        if (isnan(totalInterestPayable)) {
            totalInterestPayable = 0.00;
        }
        _totalInterestPayable = [NSString stringWithFormat:@"$%.2f",totalInterestPayable];
            _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
       
        [_totalInterestPayable retain];
        
       
        int extraPaymentMonth = [_strExtraPayment intValue]*52;
        
        for (int i =0; i<extraPaymentMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
        }
        NSString *strlumpsum = [_strLumpsum stringByReplacingOccurrencesOfString:@"$" withString:@""];
        strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
        double lumpsumValue = [strlumpsum doubleValue];
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++) 
        {
            monthCount++;
            double mv = amounts * r;
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            if (amounts<0) {
                break;
            }
        }
        
        int fortnight = roundf((monthCount*12.0)/52.0);
        double updatedCostOfloan = originalAmt + totalInterest;
        _strUpdatedCostOfLoan = [NSString stringWithFormat:@"$%.2f",updatedCostOfloan];
        [_strUpdatedCostOfLoan retain];
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;
        _strInterestSaved = [NSString stringWithFormat:@"$%.2f",interestsaved];
        [_strInterestSaved retain];
        
        int UpdatedTermInMonth = fortnight % 12;
        int UpdatedTermInYear = fortnight / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        totalMonth= roundf((totalMonth*12.0)/52.0);
        int remainMonth = totalMonth - fortnight;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
        
        
    }
    
    
    
    increased_monthly_repayment =   _monthlyRepayment;
    
    
    
    
    NSString *clean_monthly_repayment = [[_monthlyRepayment
                                          componentsSeparatedByCharactersInSet:
                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                         componentsJoinedByString:@""];
    
    
    NSString *clean_str_lumpsum = [[_strLumpsum
                                    componentsSeparatedByCharactersInSet:
                                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                   componentsJoinedByString:@""];
    
    
    
    increased_monthly_repayment = [NSString stringWithFormat:@"$%.2f",[clean_monthly_repayment floatValue]/100+ [clean_str_lumpsum floatValue]/100];
    
    [increased_monthly_repayment retain];

    
    
    txtMonthlyRepayment.text = _monthlyRepayment;
    txtTotalCostofLoan.text = _totalCostOfLoan;
    txtTotalInterestPayable.text = _totalInterestPayable;
    
    txtInterestSaved.text = _strInterestSaved;
    txtTimeSaved.text = _strTimeSaved;
    txtUpatedLoanTerm.text = _strUpdatedLoanTerm;
    txtUpdatedCostOfLoan.text = _strUpdatedCostOfLoan;
    
    if ([_monthlyRepayment isEqualToString:@"$nan"])
    {
        _monthlyRepayment = @"$0.00";
    }
    
    if ([_totalCostOfLoan isEqualToString:@"$nan"])
    {
        _totalCostOfLoan = @"$0.00";
    }
    if ([_totalInterestPayable isEqualToString:@"$nan"])
    {
        _totalInterestPayable = @"$0.00";
    }
    
    if ([_strInterestSaved isEqualToString:@"$nan"])
    {
        _strInterestSaved = @"$0.00";
    }
    if ([_strUpdatedCostOfLoan isEqualToString:@"$nan"])
    {
        _strUpdatedCostOfLoan = @"$0.00";
    }

    
    monthCount = 0;
    totalInterest = 0.0;
    
}

-(void)btnSegment3_Clicked:(id)sender
{
    UILabel *lbl = (UILabel*) [self.view viewWithTag:1111];
    if ([sender tag] == 3001) 
    {
       // strRepay = @"Monthly repayment";
        strRepay = @"Min. monthly repayment";
        repayment = @"Monthly";
        indexOfRepayment = 1;
        lbl.text = @"Min. Monthly Repayment";
    }
    else if([sender tag] == 3002) 
    {
        strRepay = @"Fortnightly repayment";
        repayment = @"Fortnightly";
        indexOfRepayment = 2;
        lbl.text = @"Fortnightly repayment";
    }
    else if ([sender tag] == 3003) 
    {
        strRepay = @"Weekly repayment";
        repayment = @"Weekly";
        indexOfRepayment = 3;
        lbl.text = @"Weekly repayment";
    }
    
  
    UITextField *txtMonthlyRepayment = (UITextField *)[self.view viewWithTag:300];
    UITextField *txtTotalCostOfLoan = (UITextField *)[self.view viewWithTag:301];
    
    
    _amount = [[_amount componentsSeparatedByCharactersInSet:
                [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
               componentsJoinedByString:@""];
    
    
    _amount = [NSString stringWithFormat:@"%f",[_amount floatValue]/100];
    [_amount retain];
    

    
    
    
    
    NSString *strAmount = _amount;
    NSString *strRate = _rate;
    NSString *strTerms = _term;
    
    
    
    
    /*
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"," withString:@""];
    strAmount = [strAmount stringByReplacingOccurrencesOfString:@"$" withString:@""];
    */
    
    strRate =  [strRate stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [_currencyFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [_currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [_currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [_currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[_currencyFormatter currencyCode] isEqualToString:@"NAD"])
        {
            [_currencyFormatter setCurrencyCode:@"NAD"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
            [_currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        
    }

    
    _amount = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:[_amount floatValue]]];
    [_amount retain];
    
    
    double amounts = [strAmount doubleValue];
    double originalAmt = amounts;
    double rates =  [strRate floatValue];
    double terms = [strTerms floatValue];
    
    
    _strLumpsum = [[_strLumpsum componentsSeparatedByCharactersInSet:
                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                   componentsJoinedByString:@""];
    
    
    _strLumpsum = [NSString stringWithFormat:@"%f",[_strLumpsum floatValue]/100];
    [_strLumpsum retain];
    
    
    float temp = [_strLumpsum floatValue];
    _strLumpsum = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [_strLumpsum retain];
    

    
    if (indexOfRepayment == 1)
    {
        double r = rates/1200;
        int totalMonth = terms *12;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
        
        //txtMonthlyRepayment.text = [NSString stringWithFormat:@"$%.2f",monthlyRepayment];
        
        double temp = monthlyRepayment;
        txtMonthlyRepayment.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        [txtMonthlyRepayment.text retain];
        
        
        
        
        double totalCostOfLoan = monthlyRepayment *(terms*12);
        
        //txtTotalCostOfLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        
        temp = totalCostOfLoan;
        txtTotalCostOfLoan.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        
        
        
        _totalCostOfLoan =  [NSString stringWithFormat:@"%@",txtTotalCostOfLoan.text];
        [_totalCostOfLoan retain];
        
        double totalInterestPayable = totalCostOfLoan - amounts;
        
        temp = totalInterestPayable;
        NSString*str_totalInterestPayable = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        [str_totalInterestPayable retain];
        
        
        _totalInterestPayable = [NSString stringWithFormat:@"%@",str_totalInterestPayable];
        _monthlyRepayment = txtMonthlyRepayment.text;
        
        
        
        [_monthlyRepayment retain];
        
        [_totalInterestPayable retain];
        
        
        int extraPaymentMonth = [_strExtraPayment intValue]*12;
        
        for (int i =0; i<extraPaymentMonth; i++)
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
        }
        
        
        
        /*
         NSString *strlumpsum = [_strLumpsum stringByReplacingOccurrencesOfString:@"$" withString:@""];
         strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
         */
        
        NSString *strlumpsum = [[_strLumpsum componentsSeparatedByCharactersInSet:[NSCharacterSet  symbolCharacterSet]] componentsJoinedByString:@""];
        
        
        
        
        double lumpsumValue = [strlumpsum doubleValue];
        
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++)
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            if (amounts<0) {
                break;
            }
        }
        
        
        
        double updatedCostOfloan = originalAmt + totalInterest;
        
        temp = updatedCostOfloan;
        _strUpdatedCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
        [_strUpdatedCostOfLoan retain];
        
        /*Sun:0004 Original
         _strUpdatedCostOfLoan = [NSString stringWithFormat:@"$%.2f",updatedCostOfloan];
         [_strUpdatedCostOfLoan retain];
         
         */
        
        nslog(@"\n totalCostOfLoan = %f",totalCostOfLoan);
        nslog(@"\n originalAmt = %f",originalAmt);
        nslog(@"\n totalInterest = %f",totalInterest);
        
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;
        
        
        nslog(@"\n interestsaved = %f",interestsaved);
        
        temp = interestsaved;
        _strInterestSaved = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
        [_strInterestSaved retain];
        
        
        /*
         _strInterestSaved = [NSString stringWithFormat:@"$%.2f",interestsaved];
         [_strInterestSaved retain];
         */
        
        
        
        int UpdatedTermInMonth = monthCount % 12;
        int UpdatedTermInYear = monthCount / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        int remainMonth = totalMonth - monthCount;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
    }
    else if (indexOfRepayment == 2)
    {
        
        double r = rates/2600;
        int totalMonth = terms *26;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
        
        
        
        double temp = monthlyRepayment;
        txtMonthlyRepayment.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        
        [txtMonthlyRepayment.text retain];
        
        
        
        
        
        
        double totalCostOfLoan = monthlyRepayment *(terms*26);
        
        //txtTotalCostOfLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        
        temp = totalCostOfLoan;
        txtTotalCostOfLoan.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        
        
        _totalCostOfLoan =  [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        [_totalCostOfLoan retain];
        
        double totalInterestPayable = totalCostOfLoan - amounts;
        
        temp = totalInterestPayable;
        NSString*str_totalInterestPayable = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        [str_totalInterestPayable retain];
        
        _totalInterestPayable = [NSString stringWithFormat:@"$%@",str_totalInterestPayable];
        
        
        
        
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        
        [_totalInterestPayable retain];
        
        int extraPaymentMonth = [_strExtraPayment intValue]*26;
        
        
        
        
        
        
        for (int i =0; i<extraPaymentMonth; i++)
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            
        }
        
        
        
        
        NSString *strlumpsum = [[_strLumpsum componentsSeparatedByCharactersInSet:[NSCharacterSet  symbolCharacterSet]] componentsJoinedByString:@""];
        
        
        strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
        double lumpsumValue = [strlumpsum doubleValue];
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++)
        {
            monthCount++;
            double mv = amounts * r;
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            
            if (amounts<0)
            {
                break;
            }
        }
        int fortnight = roundf((monthCount*12.0)/26.0);
        
        double updatedCostOfloan = originalAmt + totalInterest;
        
        temp = updatedCostOfloan;
        _strUpdatedCostOfLoan = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
        [_strUpdatedCostOfLoan retain];
        
        
        
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;
        
        
        temp = interestsaved;
        _strInterestSaved = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
        [_strInterestSaved retain];
        
        
        int UpdatedTermInMonth = fortnight % 12;
        int UpdatedTermInYear = fortnight / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        totalMonth= roundf((totalMonth*12.0)/26.0);
        int remainMonth = totalMonth - fortnight;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
    }
    else if (indexOfRepayment == 3)
    {
        double r = rates/5200;
        int totalMonth = terms *52;
        double x = 1+r;
        double b = pow(x, totalMonth) -1;
        double monthlyRepayment = (r +(r/b)) *amounts;
        
        
        double temp = monthlyRepayment;
        txtMonthlyRepayment.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithDouble:temp]];
        [txtMonthlyRepayment.text retain];
        
        
        
        
        
        
        
        double totalCostOfLoan = monthlyRepayment *(terms*52);
        
        txtTotalCostOfLoan.text = [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        _totalCostOfLoan =  [NSString stringWithFormat:@"$%.2f",totalCostOfLoan];
        [_totalCostOfLoan retain];
        double totalInterestPayable = totalCostOfLoan - amounts;
        _totalInterestPayable = [NSString stringWithFormat:@"$%.2f",totalInterestPayable];
        _monthlyRepayment = txtMonthlyRepayment.text;
        [_monthlyRepayment retain];
        [_totalInterestPayable retain];
        
        int extraPaymentMonth = [_strExtraPayment intValue]*52;
        
        for (int i =0; i<extraPaymentMonth; i++)
        {
            monthCount++;
            double mv = amounts * r;
            
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            
        }
        NSString *strlumpsum = [_strLumpsum stringByReplacingOccurrencesOfString:@"$" withString:@""];
        strlumpsum = [strlumpsum stringByReplacingOccurrencesOfString:@"," withString:@""];
        double lumpsumValue = [strlumpsum doubleValue];
        amounts = amounts - lumpsumValue;
        for (int i = extraPaymentMonth; i<totalMonth; i++)
        {
            monthCount++;
            double mv = amounts * r;
            totalInterest = totalInterest + mv;
            double mp = monthlyRepayment - mv;
            amounts = amounts - mp;
            if (amounts<0) {
                break;
            }
        }
        
        int fortnight = roundf((monthCount*12.0)/52.0);
        double updatedCostOfloan = originalAmt + totalInterest;
        _strUpdatedCostOfLoan = [NSString stringWithFormat:@"$%.2f",updatedCostOfloan];
        [_strUpdatedCostOfLoan retain];
        
        double interestsaved = totalCostOfLoan - originalAmt -totalInterest;
        
        
        
        _strInterestSaved = [NSString stringWithFormat:@"$%.2f",interestsaved];
        [_strInterestSaved retain];
        
        
        
        
        int UpdatedTermInMonth = fortnight % 12;
        int UpdatedTermInYear = fortnight / 12;
        
        _strUpdatedLoanTerm = [NSString stringWithFormat:@"%d years %d months",UpdatedTermInYear,UpdatedTermInMonth];
        [_strUpdatedLoanTerm retain];
        totalMonth= roundf((totalMonth*12.0)/52.0);
        int remainMonth = totalMonth - fortnight;
        
        int timeSavedInMonth = remainMonth % 12;
        int timeSavedInYear = remainMonth/12;
        
        _strTimeSaved = [NSString stringWithFormat:@"%d years %d months",timeSavedInYear,timeSavedInMonth];
        [_strTimeSaved retain];
        
        
    }
    
    
    
    increased_monthly_repayment =   _monthlyRepayment;
    
    
    
    
    NSString *clean_monthly_repayment = [[_monthlyRepayment
                                          componentsSeparatedByCharactersInSet:
                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                         componentsJoinedByString:@""];
    
    
    NSString *clean_str_lumpsum = [[_strLumpsum
                                    componentsSeparatedByCharactersInSet:
                                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                   componentsJoinedByString:@""];
    
    
    
    temp = [clean_monthly_repayment floatValue]/100+ [clean_str_lumpsum floatValue]/100;
    increased_monthly_repayment = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:temp]];
    [increased_monthly_repayment retain];
    
    
    
    
    nslog(@"\n clean_monthly_repayment = %f",[clean_monthly_repayment floatValue]/100);
    nslog(@"\n clean_str_lumpsum = %f",[clean_str_lumpsum floatValue]/100);
    
/*

    @try
    {
        [_monthlyRepayment floatValue];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        _monthlyRepayment = @"0.00";
        
    }
    
    
    if ([_monthlyRepayment isEqualToString:@"$nan"])
    {
        _monthlyRepayment = @"0.00";
    }
    
    if ([_totalCostOfLoan isEqualToString:@"$nan"])
    {
        _totalCostOfLoan = @"$0.00";
    }
    if ([_totalInterestPayable isEqualToString:@"$nan"])
    {
        _totalInterestPayable = @"$0.00";
    }
    
    if ([_strInterestSaved isEqualToString:@"$nan"])
    {
        _strInterestSaved = @"$0.00";
    }
    if ([_strUpdatedCostOfLoan isEqualToString:@"$nan"])
    {
        _strUpdatedCostOfLoan = @"$0.00";
    }
    */
    
    

    monthCount = 0;
    totalInterest = 0.0;
    [tableView reloadData];
        
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/*
 -(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
 
 NSMutableCharacterSet *numberSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
 [numberSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
 NSCharacterSet *nonNumberSet = [numberSet invertedSet];
 
 BOOL result = NO;
 
 if([string length] == 0){ 
 result = YES;
 }
 else{
 if([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0){
 result = YES;
 }
 }
 
 if(result){
 NSMutableString* mstring = [[textField text] mutableCopy];
 
 if([string length] > 0){
 [mstring insertString:string atIndex:range.location];
 }
 else {
 [mstring deleteCharactersInRange:range];
 }
 
 NSLocale* locale = [NSLocale currentLocale];
 NSString *localCurrencySymbol = [locale objectForKey:NSLocaleCurrencySymbol];
 NSString *localGroupingSeparator = [locale objectForKey:NSLocaleGroupingSeparator];
 
 NSString* clean_string = [[mstring stringByReplacingOccurrencesOfString:localGroupingSeparator 
 withString:@""]
 stringByReplacingOccurrencesOfString:localCurrencySymbol 
 withString:@""];
 
 if([[Formatters currencyFormatter] maximumFractionDigits] > 0){
 NSMutableString *mutableCleanString = [clean_string mutableCopy];
 
 if([string length] > 0){
 NSRange theRange = [mutableCleanString rangeOfString:@"."];
 [mutableCleanString deleteCharactersInRange:theRange];
 [mutableCleanString insertString:@"." atIndex:(theRange.location + 1)];
 clean_string = mutableCleanString;
 }
 else {
 [mutableCleanString insertString:@"0" atIndex:0];
 NSRange theRange = [mutableCleanString rangeOfString:@"."];
 [mutableCleanString deleteCharactersInRange:theRange];
 [mutableCleanString insertString:@"." atIndex:(theRange.location - 1)];
 clean_string = mutableCleanString;
 }
 }
 
 NSNumber* number = [[Formatters basicFormatter] numberFromString: clean_string];
 NSMutableString *numberString = [[[Formatters currencyFormatter] stringFromNumber:number] mutableCopy];
 [numberString deleteCharactersInRange:NSMakeRange(0, 1)];
 [textField setText:numberString];
 }
 return NO;
 }
 
 */
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{ 
    BOOL istag = FALSE;
    
    nslog(@"\n textfield.tag = %d",textField.tag);
    
    
    if (textField.tag == 100 || textField.tag == 101 || textField.tag == 200 || textField.tag==102 || textField.tag==201) 
    {
        if (textField.tag == 200) 
        {
            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        [textField setKeyboardType:UIKeyboardTypeNumberPad];
        istag = TRUE;  
    }
    current_tag = textField.tag;
    return istag;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    nslog(@"\n txtTextField.tag in begin = %d",textField.tag);
    tg = textField.tag;
    CGPoint pnt = [tableView convertPoint:textField.bounds.origin fromView:textField];
    NSIndexPath* path = [tableView indexPathForRowAtPoint:pnt];
    [tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    if(appDelegate.isIPad)
    {
        
        
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            toolbar.frame = CGRectMake(0,672,768,44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            toolbar.frame = CGRectMake(0,328,1024,44);
        }
        
    }

    
    [toolbar setHidden:NO];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    UITextField *txtTextField = (UITextField*)[self.view viewWithTag:tg];
    nslog(@"\n txtTextField.tag end edit = %d",txtTextField.tag);
    
    if (tg == 100) 
    {
        _amount = txtTextField.text;
        [_amount retain];
        /*
        NSString *cleanCentString = [[textField.text
                                      componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                     componentsJoinedByString:@""];
        
        
        
        // Parse final integer value
        
        nslog(@"\n cleanCentString = %@",cleanCentString);
        
        
        
        
        //  stramountTemp = textField.text;
        
        _amount = [NSString stringWithFormat:@"%f",[cleanCentString floatValue]/100];
        [_amount retain];

        */
        
        
    }
    else if (tg == 101)
    {
        _rate = txtTextField.text;
        [_rate retain];
    }
    else if (tg == 102)
    {
        _term = txtTextField.text;
        [_term retain];
    }
    else if (tg == 200)
    {
        
        _strLumpsum = txtTextField.text;
        [_strLumpsum retain];
        
        /*
        
        NSString *cleanCentString = [[textField.text
                                      componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                     componentsJoinedByString:@""];
        
        
        
        // Parse final integer value
        
        nslog(@"\n cleanCentString = %@",cleanCentString);
        
        
        
        
       
        
        _strLumpsum = [NSString stringWithFormat:@"%f",[cleanCentString floatValue]/100];
        [_strLumpsum retain];
        
        */

        
       
    }
    else if (tg == 201)
    {
        _strExtraPayment = txtTextField.text;
    }
    [toolbar setHidden:YES];
    [txtTextField resignFirstResponder];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSMutableCharacterSet *numberSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
    [numberSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
    NSCharacterSet *nonNumberSet = [numberSet invertedSet];
    
    BOOL result = NO;
    
    if([string length] == 0)
    {
        result = YES;
    }
    else
    {
        if([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0)
        {
            result = YES;
        }
    }
    
    if(result)
    {
        

        
        
        if (textField.tag == 100 || textField.tag == 200) 
        {
            
            
            NSDecimalNumber *someAmount = [NSDecimalNumber decimalNumberWithString:@"5.00"];
            
            NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
            
            nslog(@"%@", [currencyFormatter stringFromNumber:someAmount]);
            
            if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
            {
                [currencyFormatter setCurrencyCode:appDelegate.curCode];
                
                if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
                {
                    [currencyFormatter setCurrencyCode:@"USD"];
                    nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                    [currencyFormatter setLocale:locale];
                    
                }
                else if([[currencyFormatter currencyCode] isEqualToString:@"CNY"])
                {
                    [currencyFormatter setCurrencyCode:@"CNY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                    [currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                
                else if([[currencyFormatter currencyCode] isEqualToString:@"JPY"])
                {
                    [currencyFormatter setCurrencyCode:@"JPY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                    [currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                
            }
            [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            
            
            int currencyScale = [currencyFormatter maximumFractionDigits];
            [currencyFormatter release];
            nslog(@"\n scale = %d",currencyScale);
            
            if(currencyScale == 0)
            {
                
                NSString *cleanCentString = [[textField.text
                                              componentsSeparatedByCharactersInSet:
                                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                             componentsJoinedByString:@""];
                
                
                
                
                double centAmount = [cleanCentString doubleValue];
                // Check the user input
                
                if (string.length > 0)
                {
                    // Digit added
                    centAmount = centAmount * 10 + [string doubleValue];
                }
                else
                {
                    // Digit deleted
                    centAmount = centAmount / 10;
                }
                // Update call amount value
                NSNumber *_amount_local;
                //_amount = [[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f];
                _amount_local = [[NSNumber alloc]initWithInt:(int)centAmount];
                
                
                NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
                [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
                if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
                {
                    [_currencyFormatter setCurrencyCode:appDelegate.curCode];
                    
                    
                    if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
                    {
                        [_currencyFormatter setCurrencyCode:@"USD"];
                        nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                        [_currencyFormatter setLocale:locale];
                        
                    }
                    else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
                    {
                        [_currencyFormatter setCurrencyCode:@"CNY"];
                        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                        [_currencyFormatter setLocale:locale];
                        [locale release];
                        
                    }
                    else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
                    {
                        [_currencyFormatter setCurrencyCode:@"JPY"];
                        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                        [_currencyFormatter setLocale:locale];
                        [locale release];
                        
                    }
                    
                    
                }
                else
                {
                    [_currencyFormatter setCurrencyCode:[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
                }
                [_currencyFormatter setNegativeFormat:@"-¤#,##0.00"];
                
                textField.text = [_currencyFormatter stringFromNumber:_amount_local];
                [_currencyFormatter release];
            }
            else
            {
                NSString *cleanCentString = [[textField.text
                                              componentsSeparatedByCharactersInSet:
                                              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                             componentsJoinedByString:@""];
                // Parse final integer value
                
                double centAmount = [cleanCentString doubleValue];
                // Check the user input
                if (string.length > 0)
                {
                    // Digit added
                    centAmount = centAmount * 10 + [string doubleValue];
                }
                else
                {
                    // Digit deleted
                    centAmount = centAmount / 10;
                }
                // Update call amount value
                NSNumber *_amount_local;
                _amount_local = [[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f];
                //    _amount = [[NSNumber alloc] initWithFloat:(float)centAmount / 100.0f];
                // Write amount with currency symbols to the textfield
                NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
                [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
                if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
                {
                    [_currencyFormatter setCurrencyCode:appDelegate.curCode];
                    
                    
                    if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
                    {
                        [_currencyFormatter setCurrencyCode:@"USD"];
                        nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                        [_currencyFormatter setLocale:locale];
                        
                    }
                    else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
                    {
                        [_currencyFormatter setCurrencyCode:@"CNY"];
                        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                        [_currencyFormatter setLocale:locale];
                        [locale release];
                        
                    }
                    
                    else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
                    {
                        [_currencyFormatter setCurrencyCode:@"JPY"];
                        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                        [_currencyFormatter setLocale:locale];
                        [locale release];
                        
                    }
                    
                    
                }
                textField.text = [_currencyFormatter stringFromNumber:_amount_local];
                
            }

            
            
            
            
            
            
            
            
           // textField.text = [@"$" stringByAppendingString:textField.text];
        }
        else
        {
            
            
            
            
           
                          
            
           NSString* clean_string = [[textField.text
              componentsSeparatedByCharactersInSet:
              [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
             componentsJoinedByString:@""];
            
            nslog(@"\n clean_string = %@",clean_string);
            
            double centAmount = [clean_string doubleValue];
            // Check the user input
            if (string.length > 0)
            {
                // Digit added
                centAmount = centAmount * 10 + [string doubleValue];
            }
            else
            {
                // Digit deleted
                centAmount = centAmount / 10;
            }
            // Update call amount value
            NSNumber *_amount_local;
            _amount_local = [[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f];
            
            
            nslog(@"\n _amount_local = %@",_amount_local);
            
                        
            NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            
        
            if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
            {
                [_currencyFormatter setCurrencyCode:appDelegate.curCode];
                
                
                if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
                {
                    [_currencyFormatter setCurrencyCode:@"USD"];
                    nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
                {
                    [_currencyFormatter setCurrencyCode:@"CNY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
                {
                    [_currencyFormatter setCurrencyCode:@"JPY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"NAD"])
                {
                    [_currencyFormatter setCurrencyCode:@"NAD"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                
                
            }


            
            
        textField.text = [_currencyFormatter stringFromNumber:_amount_local];
            
          textField.text = [[textField.text componentsSeparatedByCharactersInSet:[NSCharacterSet  symbolCharacterSet]] componentsJoinedByString:@""];
            
            
            
           
            
            
           // [NSString stringWithFormat:@"%@",_amount_local];
        }
        
    }
    

    return NO; // we return NO because we have manually edited the textField contents.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UITextField *txtAmount = (UITextField *)[self.view viewWithTag:100];
    
    if([[Formatters currencyFormatter] maximumFractionDigits] > 0)
    {
        [txtAmount setText:_amount];
    } else
    {

        [txtAmount setText:_amount];
    }
    UITextField *txtRate = (UITextField *)[self.view viewWithTag:101];
    
    if([[Formatters currencyFormatter] maximumFractionDigits] > 0)
    {

        [txtRate setText:_rate];
    } else
    {

        [txtRate setText:_rate];
    }
    
}

#pragma mark - back_clicked
#pragma mark

-(void)back_clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



@end
