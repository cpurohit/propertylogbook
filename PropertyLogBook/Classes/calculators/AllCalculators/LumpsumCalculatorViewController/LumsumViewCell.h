//
//  LumsumViewCell.h
//  Loansactually
//
//  
//  
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"
@interface LumsumViewCell : UITableViewCell
{
    PropertyLogBookAppDelegate*app_delegate;
}

@property(nonatomic,retain) UILabel *lblTitle;
@property(nonatomic,retain) UIButton *segment21;
@property(nonatomic,retain) UIButton *segment22;
@property(nonatomic,retain) UISlider *slider;
@property(nonatomic,retain) UIButton *segment31;
@property(nonatomic,retain) UIButton *segment32;
@property(nonatomic,retain) UIButton *segment33;
@property(nonatomic,retain) UITextField *txtValues;
@property(nonatomic,retain) UILabel *lblPersent;
@property(nonatomic,retain)UIImageView *cellImageView;
@end
