//
//  LumsumViewController.h
//  Loansactually
//
//  
//  
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "PropertyLogBookAppDelegate.h"
@class CustomToolbar;
@interface LumsumViewController : UIViewController<UITextFieldDelegate,MFMailComposeViewControllerDelegate>
{
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UIButton *btnBack;
    IBOutlet UIButton *btnEmail;
    IBOutlet UIButton *btnCalculator;
    IBOutlet UIButton *btnInfo;
    IBOutlet UITableView *tableView;
    NSArray *arrText;
    NSInteger indexOfRepayment;
    IBOutlet CustomToolbar *toolbar;
   
    NSString *repayment;
    NSString *strRepay;
    NSInteger tg;
    NSInteger monthCount;
    double totalInterest;
    
    IBOutlet UIView *viewforfooter;
    
    PropertyLogBookAppDelegate*appDelegate;
    
    int current_tag;
    
    IBOutlet UIView*footer_view;
    
    NSString*increased_monthly_repayment;
    
    IBOutlet UIButton*reset_bottom_button;
    IBOutlet UILabel*important_note_label;
    
}
@property(nonatomic,retain) NSString *amount;
@property(nonatomic,retain) NSString *rate;
@property(nonatomic,retain) NSString *term;
@property(nonatomic,retain) NSString *monthlyRepayment;
@property(nonatomic,retain) NSString *totalCostOfLoan;
@property(nonatomic,retain) NSString *totalInterestPayable;
@property(nonatomic,retain) NSString *strLumpsum;
@property(nonatomic,retain) NSString *strExtraPayment;
@property(nonatomic,retain) NSString *strInterestSaved;
@property(nonatomic,retain) NSString *strTimeSaved;
@property(nonatomic,retain) NSString *strUpdatedLoanTerm;
@property(nonatomic,retain) NSString *strUpdatedCostOfLoan;
-(IBAction)btnBack_Clicked:(id)sender;
-(IBAction)btnEmail_Clicked:(id)sender;
-(IBAction)btnCalculator_Clicked:(id)sender;
-(IBAction)btnInfo_Clicekd:(id)sender;
-(IBAction)btnDoneClicked:(id)sender;
-(IBAction)reset_button_pressed:(id)sender;


@end
