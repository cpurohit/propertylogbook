//
//  ListsViewController.m
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 11/03/13.
//
//

#import "ListsViewController.h"

#import "equityListViewController.h"
#import "RepaymentViewController.h"
#import "LumsumViewController.h"
#import "BorrowViewController.h"


@interface ListsViewController ()

@end

@implementation ListsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    
    //[self.view addSubview:appDelegate.bottomView];
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    self.title = @"CALCULATOR";
    
    
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
    
    //tableview height in textfieldend editing etc…
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                calculator_list_tbl_view.frame = CGRectMake(calculator_list_tbl_view.frame.origin.x,calculator_list_tbl_view.frame.origin.y-25.0f,calculator_list_tbl_view.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                calculator_list_tbl_view.frame = CGRectMake(calculator_list_tbl_view.frame.origin.x,calculator_list_tbl_view.frame.origin.y,calculator_list_tbl_view.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                calculator_list_tbl_view.frame = CGRectMake(calculator_list_tbl_view.frame.origin.x,calculator_list_tbl_view.frame.origin.y-25.0f,calculator_list_tbl_view.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                calculator_list_tbl_view.frame = CGRectMake(calculator_list_tbl_view.frame.origin.x,calculator_list_tbl_view.frame.origin.y,calculator_list_tbl_view.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
else
{
    if(appDelegate.isIOS7)
    {
        calculator_list_tbl_view.frame = CGRectMake(calculator_list_tbl_view.frame.origin.x,calculator_list_tbl_view.frame.origin.y-25.0f,calculator_list_tbl_view.frame.size.width,self.view.frame.size.height+25.0f);
    }

}
    
    
    
    if(!appDelegate.isIPad)
    {
        
        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
            UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator.width = -12;
            
            [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
            
            
        }
        else
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
        }
    }
    
    
    
    
    
    calculator_list_tbl_view.backgroundColor = [UIColor clearColor];
    [calculator_list_tbl_view setBackgroundView:nil];
    [calculator_list_tbl_view setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    /*
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0, 1024, 1024)];
    */
    
    
    /*
     Start:
     Sun:0004
     Setting custom image in navigation bar..
     */
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor],UITextAttributeTextColor,
                                                                     [UIColor whiteColor],UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
                                                                     UITextAttributeTextShadowOffset ,
                                                                     [UIFont boldSystemFontOfSize:19.0f],UITextAttributeFont  , nil]];
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0)
    {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
        /*
         [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor],UITextAttributeTextColor,[UIColor blackColor],UITextAttributeTextShadowColor,[NSValue valueWithUIOffset:UIOffsetMake(1, 0)], UITextAttributeTextShadowOffset ,[UIFont fontWithName:@"System" size:25.0],UITextAttributeFont  , nil]];
         */
        
    }
    
    /*
     Ends:
     Sun:0004
     Setting custom image in navigation bar..
     */

    
    
   

    
    calculator_list_tbl_view.backgroundColor = [UIColor clearColor];
    [calculator_list_tbl_view setBackgroundView:nil];
    [calculator_list_tbl_view setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    [imgView release];

    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 
#pragma mark tableview method


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
/*
    return 4;
*/
    return 2;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*SUN:004
    if(indexPath.row ==2)
    {
        return 0;
    }
     */
    
    
    return 44.0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //cell.textLabel.textColor = [UIColor colorWithRed:56.0f/255.0f green:49.0f/255.0f blue:49.0f/255.0f alpha:1.0];
    cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
    switch (indexPath.row)
    {
        case 0:
        {
            
            cell.imageView.image = [UIImage imageNamed:@"equity__calculator_icon.png"];
            cell.textLabel.text = @"Equity";
            break;
            
        }
        case 1:
        {
            cell.imageView.image = [UIImage imageNamed:@"loan__calculator_icon.png"];
            cell.textLabel.text = @"Loan Repayments";
            break;
            
        }
        case 2:
        {
            cell.imageView.image = [UIImage imageNamed:@"borrowing_calculator_icon.png"];
            cell.textLabel.text = @"Borrowing Power";
            
            //cell.hidden = TRUE;
            break;
            
        }
        case 3:
        {
            cell.imageView.image = [UIImage imageNamed:@"lumpsum_calculator_icon.png"];
            cell.textLabel.text = @"Extra Repayment Calculator";
            break;
        }
        default:
        {
            
            break;
        }
    
    }
    
    
    
   
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
        {
            
            
            equityListViewController*viewController = [[equityListViewController alloc]initWithNibName:@"equityListViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
            [viewController release];
            
            break;
            
        }
        case 1:
        {
            RepaymentViewController*viewController = [[RepaymentViewController alloc]initWithNibName:@"RepaymentViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
            [viewController release];
             break;
        }
        case 2:
        {
            BorrowViewController*viewController = [[BorrowViewController alloc]initWithNibName:@"BorrowViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
            [viewController release];
             break;
            
        }
        case 3:
        {
            LumsumViewController*viewController = [[LumsumViewController alloc]initWithNibName:@"LumsumViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
            [viewController release];
             break;
        }
        default:
        {
            break;
        }
    
    }
}



#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //[tblView reloadData];
    
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
    }
    
    
}

#pragma mark - shouldAutorotateToInterfaceOrientation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations.
    if(appDelegate.isIPad)
    {
        return  YES;
    }
    return FALSE;
}


#pragma mark - back_clicked
#pragma mark 

-(void)back_clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(void)dealloc
{
    [calculator_list_tbl_view release];
    [super dealloc];
}

@end
