//
//  ListsViewController.h
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 11/03/13.
//
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

@interface ListsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView*calculator_list_tbl_view;
    PropertyLogBookAppDelegate*appDelegate;
}

@end
