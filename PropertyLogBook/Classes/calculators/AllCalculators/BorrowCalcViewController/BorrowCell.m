//
//  SecondViewCell.m
//  Loansactually
//
//  
//  
//

#import "BorrowCell.h"

@implementation BorrowCell

@synthesize btnSingle = _btnSingle;
@synthesize btnJoint= _btnJoint;
@synthesize txtValues = _txtValues;
@synthesize btnAnnual = _btnAnnual;
@synthesize btnMonthly = _btnMonthly;
@synthesize btnFortnightly = _btnFortnightly;
@synthesize btnWeekly = _btnWeekly;
@synthesize slider = _slider;
@synthesize btnMonthlyBig = _btnMonthlyBig;
@synthesize btnFortnightlyBig = _btnFortnightlyBig;
@synthesize btnWeeklyBig = _btnWeeklyBig;
@synthesize cellImageView = _cellImageView;
@synthesize lblTemp;
@synthesize lblPersent = _lblPersent;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    
    if (self)
    {
        
        _cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 40)];
        [self.contentView addSubview:_cellImageView];
        
        
        _lblPersent = [[UILabel alloc] init];
        _lblPersent.font = [UIFont fontWithName:@"Bookman Old Style" size:13.0];
        [_lblPersent setBackgroundColor:[UIColor clearColor]];
        _lblPersent.text = @"%";
        [self.contentView addSubview:_lblPersent];
        
        
        lblTemp = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 205, 20)];
        lblTemp.font = [UIFont fontWithName:@"Bookman Old Style" size:13.0];
        [lblTemp setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:lblTemp];
        
        
        CGRect frame = CGRectMake(25.0, 30.0, 236.0, 7.0);
        _slider = [[UISlider alloc] initWithFrame:frame];
        [_slider setBackgroundColor:[UIColor clearColor]];
        _slider.minimumValue = 0;
        _slider.maximumValue = 100;
        _slider.continuous = YES;
        
        UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"slider_on.png"];
        UIImage *sliderRightTrackImage = [UIImage imageNamed: @"slider_off.png"];
        [_slider setMinimumTrackImage: sliderLeftTrackImage forState: UIControlStateNormal];
        [_slider setMaximumTrackImage: sliderRightTrackImage forState: UIControlStateNormal];
        [_slider setThumbImage:[UIImage imageNamed:@"slider_top.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_slider];
        
        _btnAnnual = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _btnAnnual.font = [UIFont systemFontOfSize:12.0f];
        
        _btnAnnual.frame = CGRectMake(10, 40, 70, 25);
        
        [self.contentView addSubview:_btnAnnual];
        
        _btnMonthly = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _btnMonthly.font = [UIFont systemFontOfSize:12.0f];
        
        _btnMonthly.frame = CGRectMake(80, 40, 70, 25);
        
        [self.contentView addSubview:_btnMonthly];
        
        _btnFortnightly = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        

        _btnFortnightly.frame = CGRectMake(150, 40, 70, 25);
        
        [self.contentView addSubview:_btnFortnightly];
        
        _btnWeekly = [UIButton buttonWithType:UIButtonTypeRoundedRect];

       // [_btnWeekly setTitle:@"Weekly" forState:UIControlStateNormal];
        _btnWeekly.frame = CGRectMake(220, 40, 70, 25);
        
        [self.contentView addSubview:_btnWeekly];
        
        
        _btnSingle = [UIButton buttonWithType:UIButtonTypeRoundedRect];

        _btnSingle.frame = CGRectMake(194, 7, 48, 25);
        //[_btnSingle setTitle:@"Single" forState:UIControlStateNormal];
        [_btnSingle setBackgroundImage:[UIImage imageNamed:@"single_on.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_btnSingle];
        
        _btnJoint = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _btnJoint.frame = CGRectMake(242, 7, 48, 25);

       // [_btnJoint setTitle:@"Joint" forState:UIControlStateNormal];
        [_btnJoint setBackgroundImage:[UIImage imageNamed:@"joint_off.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_btnJoint];
        
        
        _btnMonthlyBig = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _btnMonthlyBig.frame = CGRectMake(60, 40, 94, 25);

        
        
        //[_btnMonthlyBig setTitle:@"Monthly" forState:UIControlStateNormal];
        
        
        [_btnMonthlyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_annually_off@2x.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_btnMonthlyBig];
        
        
        _btnFortnightlyBig = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _btnFortnightlyBig.frame = CGRectMake(150, 40, 94, 25);

        //[_btnFortnightlyBig setTitle:@"Fortnightly" forState:UIControlStateNormal];
        
        [_btnFortnightlyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_annually_off@2x.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_btnFortnightlyBig];
        
        
        _btnWeeklyBig = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _btnWeeklyBig.frame = CGRectMake(198, 40, 94, 25);

        //[_btnWeeklyBig setTitle:@"Weekly" forState:UIControlStateNormal];
        
        [_btnWeeklyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_weekly_off@2x.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_btnWeeklyBig];
        
        _txtValues = [[UITextField alloc] initWithFrame:CGRectMake(170, 10, 120, 20)];
        [_txtValues setTextAlignment:UITextAlignmentRight];
        
        [_txtValues setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        
        
        
       // _txtValues.font = [UIFont fontWithName:@"Bookman Old Style" size:15.0];
         _txtValues.font = [UIFont systemFontOfSize:15.0];
        
        [self.contentView addSubview:_txtValues];
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
