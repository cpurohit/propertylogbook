//
//  SecondViewCell.h
//  Loansactually
//
//  
//  
//

#import <UIKit/UIKit.h>

@interface BorrowCell : UITableViewCell
{
   
}

@property (nonatomic,retain)UILabel *lblTemp;
@property(nonatomic,retain)UILabel *lblPersent;
@property(nonatomic,retain)UIButton *btnSingle;
@property(nonatomic,retain)UIButton *btnJoint;
@property(nonatomic,retain)UITextField *txtValues;
@property(nonatomic,retain)UIButton *btnAnnual;
@property(nonatomic,retain)UIButton *btnMonthly;
@property(nonatomic,retain)UIButton *btnFortnightly;
@property(nonatomic,retain)UIButton *btnWeekly;
@property(nonatomic,retain)UISlider *slider;
@property(nonatomic,retain)UIButton *btnMonthlyBig;
@property(nonatomic,retain)UIButton *btnFortnightlyBig;
@property(nonatomic,retain)UIButton *btnWeeklyBig;
@property(nonatomic,retain)UIImageView *cellImageView;
@end
