//
//  BorrowViewController.m
//  Loansactually
//
//  
//  
//

#import "BorrowViewController.h"
#import "BorrowCell.h"
#import "Formatter.h"
#import "print.h"
#import "CustomToolbar.h"


@interface BorrowViewController ()

@end

@implementation BorrowViewController
static int tg = 0;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"BORROWING POWER";
    }
    return self;
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    [toolbar setHidden:YES];
    
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    totalExp = 0.0;
    totalIncome = 0.0;
    totalAnnualAllowns = 0.0;
    totalMonthlyAllowns = 0.0;
    youCanBorrow = 0.0;
    lblApplicationType = @"Application type";
    lblGrossINcome1 = @"Gross Income 1";
    lblGrossINcome2 = @"Gross Income 2";
    lblUntaxedAlloeableIncome = @"Untaxed Allowable Income";
    lblRentalIncome = @"Rental income";
    lblOtherLoanRepayment = @"Other loan repayments";
    lblCarLoanRepaymentPerMonth = @"Car loan repayments per month";
    lblTotalCreditcardLimit = @"Total credit card limits";
    lblNumberOfDepartment = @"Number of Dependants";
    lblTermOfLoan = @"Term of loan";
    lblInterestRate = @"Interest Rate";
    lblYouMayBorrow = @"You may borrow";
    lblMonthlyRepayment = @"Monthly repayments";
    lblTotalInterestPayable = @"Total interest payable";
    
    lblTitle1.font = [UIFont fontWithName:@"Bookman Old Style" size:14.0];
    indexofApplicationType = 1;
    indexofGrossIncome1 = 1;
    indexofGrossIncome2 = 1;
    indexofRentalIncome = 1;
    indexofUntaxedIncome = 1;
    indexofOtherLoanRepayment = 1;
    
    strGrossINcome1 = @"$0.00";
    strGrossINcome2 = @"$0.00";
    strUntaxedAlloeableIncome = @"$0.00";
    strRentalIncome = @"$0.00";
    strOtherLoanRepayment = @"$0.00";
    strCarLoanRepaymentPerMonth = @"$0.00";
    strTotalCreditcardLimit = @"$0.00";
    strNumberOfDepartment = @"0";
    strTermOfLoan = @"0.00";
    strInterestRate = @"0.0";
    strYoumayBorrow = @"$0.00";
    strMonthlyRepayment = @"$0.00";;
    strTotalInterestPayable = @"$0.00";
    [tableView setBackgroundColor:[UIColor clearColor]];
    // Do any additional setup after loading the view from its nib.
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    button.bounds = CGRectMake(0, 0,49.0,29.0);
    [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];

    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    BOOL isPortraite = FALSE;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft||interfaceOrientation == UIInterfaceOrientationLandscapeRight)
        {
            isPortraite = FALSE;
        }
        else
        {
            isPortraite = TRUE;
        }
    }
    else {
        return YES;
    }
    return isPortraite;
    
}

-(IBAction)btnBack_Clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)btnEmail_Clicked:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        
        //last added
        NSString *str = [strYoumayBorrow substringFromIndex:1];
        nslog(@"str == %@",str);
        NSString *str1 = [strTotalInterestPayable substringFromIndex:1];
        nslog(@"str1 == %@",str1);
        //NSString *str = [NSString stringWithFormat:@"%f",[str floatValue]-[str1 floatValue]];
        float f = [str1 floatValue]-[str floatValue];
        NSString *Str2 = [NSString stringWithFormat:@"%.2f",f];
        NSString *Str3 = [NSString stringWithFormat:@"$%.2f",Str2];
        nslog(@"Str3 == %@",Str3);
        
        NSString *_strAppType;
        if (indexofApplicationType == 1)
        {
            _strAppType =@"Single";
        }
        else if (indexofApplicationType == 2)
        {
            _strAppType = @"Joint";
        }
        
        NSString *_strGross1;
        if (indexofGrossIncome1  == 1)
        {
            _strGross1 = @"Annual";
        }
        else if (indexofGrossIncome1 == 2)
        {
            _strGross1 = @"Monthly";
        }
        else if (indexofGrossIncome1 == 3)
        {
            _strGross1 = @"Fortnightly";
        }
        else if (indexofGrossIncome1 == 4)
        {
            _strGross1 = @"Weekly";
        }
        
        
        NSString *_strGross2;
        if (indexofGrossIncome2  == 1)
        {
            _strGross2 = @"Annual";
        }
        else if (indexofGrossIncome2 == 2)
        {
            _strGross2 = @"Monthly";
        }
        else if (indexofGrossIncome2 == 3)
        {
            _strGross2 = @"Fortnightly";
        }
        else if (indexofGrossIncome2 == 4)
        {
            _strGross2 = @"Weekly";
        }
        
        NSString *_strUntax;
        if (indexofUntaxedIncome  == 1)
        {
            _strUntax = @"Annual";
        }
        else if (indexofUntaxedIncome == 2)
        {
            _strUntax = @"Monthly";
        }
        else if (indexofUntaxedIncome == 3)
        {
            _strUntax = @"Fortnightly";
        }
        else if (indexofUntaxedIncome == 4)
        {
            _strUntax = @"Weekly";
        }
        
        NSString *_strRental;
        if (indexofRentalIncome  == 1)
        {
            _strRental = @"Annual";
        }
        else if (indexofRentalIncome == 2)
        {
            _strRental = @"Monthly";
        }
        else if (indexofRentalIncome == 3)
        {
            _strRental = @"Fortnightly";
        }
        else if (indexofRentalIncome == 4)
        {
            _strRental = @"Weekly";
        }
        
        NSString *_strOtherLoan;
        if (indexofOtherLoanRepayment  == 1)
        {
            _strOtherLoan = @"Annually";
        }
        else if (indexofOtherLoanRepayment == 2)
        {
            _strOtherLoan = @"Monthly";
        }
        else if (indexofOtherLoanRepayment == 3)
        {
            _strOtherLoan = @"Weekly";
        }
        
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        
        [picker setSubject:@"How much can I Borrow"];
        
        
        NSMutableString *EmailBody1=[[[NSMutableString alloc] initWithString:@"<html><body>"] retain];
        
        [EmailBody1 appendString:@"Input1:"];
        //        [EmailBody1 appendString:@"<table width='310' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>Repayment Type</th><th>Loan Amount</th><th>Interest Rate</th><th>Loan Term</th><th>Repayment</th></tr>"];
        
        [EmailBody1 appendString:[NSString stringWithFormat:@"<table width='150' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>Application Type</th><td> %@</td></tr><tr><th>Gross Income 1</th><td> %@</td><td> %@</td></tr><tr><th>Gross Income 2</th><td> %@</td><td> %@</td></tr><tr><th>Untaxed Allowable Income</th><td> %@</td><td> %@</td></tr><tr><th>Rental Income</th><td> %@</td><td> %@</td></tr>",_strAppType,_strGross1,strGrossINcome1,_strGross2,strGrossINcome2,_strUntax,strUntaxedAlloeableIncome,_strRental,strRentalIncome]];
        
        //@&#37
        
        [EmailBody1 appendString:@"</table>"];
        
        
        [EmailBody1 appendString:@"Input2:"];
        
        [EmailBody1 appendString:[NSString stringWithFormat:@"<table width='150' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>Other loan repayment</th><td> %@</td><td> %@</td></tr><tr><th>Car loan repayment per month</th><td> %@</td></tr><tr><th>Total credit card limits</th><td> %@</td></tr><tr><th>Number of Dependants</th><td> %@</td></tr>",_strOtherLoan,strOtherLoanRepayment,strCarLoanRepaymentPerMonth,strTotalCreditcardLimit,strNumberOfDepartment]];
        
        
        
        [EmailBody1 appendString:@"</table>"];
        
        [EmailBody1 appendString:@"Input3:"];
        [EmailBody1 appendString:[NSString stringWithFormat:@"<table width='150' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>Term of loan</th><td> %@ years</td></tr><tr><th>Interest Rate</th><td> %@&#37</td></tr>",strTermOfLoan,strInterestRate]];
        
        
        
        [EmailBody1 appendString:@"</table>"];
        
        
        [EmailBody1 appendString:@"Output:"];
        
        
        [EmailBody1 appendString:[NSString stringWithFormat:@"<table width='150' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>You may borrow</th><td> %@</td></tr><tr><th>Monthly repayments</th><td> %@</td></tr><tr><th>Total interest payable</th><td> %@</td></tr>",strYoumayBorrow,strMonthlyRepayment,Str3]];
        
        
        //        [EmailBody1 appendString:@"<table width='310' border='1' cellpadding='0' cellspacing='0' bordercolor='#999999'><tr><th>Loan Amount</th><th>Total cost of loan</th><th>Total interest payable</th></tr>"];
        //
        //        [EmailBody1 appendString:[NSString stringWithFormat:@"<tr><td>  %@ </td><td>  %@ </td><td>      %@ </td></tr>",_amount,_rate,_term]];
        [EmailBody1 appendString:@"</table>"];
        
        
        
        [EmailBody1 appendString:@"</body></html>"];
        
        [picker setMessageBody:EmailBody1 isHTML:YES];
        
        [self presentModalViewController:picker animated:YES];
        
        [picker release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            nslog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            nslog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            nslog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            nslog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            nslog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)btnCalculator_Clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:TRUE];
}


/*
 -(IBAction)btnCalculator_Clicked:(id)sender
 {
 
 double rate = [strInterestRate doubleValue] + 1.5;
 rate = rate/1200;
 int month = [strTermOfLoan doubleValue] * 12;
 
 double R_plus_1 = rate + 1;
 double R_plus_1_res_month = pow(R_plus_1, month);
 double R_plus_1_res_month_min_1 = R_plus_1_res_month - 1;
 double R_into_R_plus_1_res_month = rate * R_plus_1_res_month;
 double finalInterest = R_plus_1_res_month_min_1/R_into_R_plus_1_res_month;
 
 [print printDescription:[NSString stringWithFormat:@"%.2f",finalInterest] message:@"FinalInterest"];
 int numberOfDependants = [strNumberOfDepartment intValue];
 
 
 NSString *strGross1 = strGrossINcome1;
 
 strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"," withString:@""];
 strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"$" withString:@""];
 
 NSString *strGross2 = strGrossINcome2;
 
 strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"," withString:@""];
 strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"$" withString:@""];
 
 NSString *strUntaxedIncome = strUntaxedAlloeableIncome;
 
 strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"," withString:@""];
 strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"$" withString:@""];
 
 
 NSString *strRental = strRentalIncome;
 
 strRental = [strRental stringByReplacingOccurrencesOfString:@"," withString:@""];
 strRental = [strRental stringByReplacingOccurrencesOfString:@"$" withString:@""];
 
 
 NSString *strOtherLoan = strOtherLoanRepayment;
 
 strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"," withString:@""];
 strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"$" withString:@""];
 
 NSString *strCarLoanPerMonth = strCarLoanRepaymentPerMonth;
 
 strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"," withString:@""];
 strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"$" withString:@""];
 
 NSString *strCreditCard = strTotalCreditcardLimit;
 
 strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"," withString:@""];
 strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"$" withString:@""];
 
 double creditCardInterest = [strCreditCard doubleValue] * 0.03 * 12;
 //    [print printDescription:[NSString stringWithFormat:@"%.2f",creditCardInterest] message:@"You Can Borrow"];
 
 double gross1,gross2,untaxedIncomde,rentalIncome,otherLoan;
 gross1 = [strGross1 doubleValue];
 gross2 = [strGross2 doubleValue];
 untaxedIncomde = [strUntaxedIncome doubleValue];
 rentalIncome = [strRental doubleValue];
 otherLoan = [strOtherLoan doubleValue];
 double ExpJointDependants = 0.0;
 if (indexofApplicationType == 1)
 {
 
 if (numberOfDependants == 0)
 {
 ExpJointDependants = 16460.0;
 }
 else if(numberOfDependants == 1)
 {
 ExpJointDependants = 22607.0;
 }
 else if (numberOfDependants == 2)
 {
 ExpJointDependants = 28423.0;
 }
 else if(numberOfDependants == 3)
 {
 ExpJointDependants = 34239.0;
 }
 else if (numberOfDependants == 4)
 {
 ExpJointDependants = 40056.0;
 }
 
 if (indexofGrossIncome1 == 1)
 {
 gross1 = gross1;
 }
 else if (indexofGrossIncome1 == 2)
 {
 gross1 = gross1*12;
 }
 else if (indexofGrossIncome1 == 3)
 {
 gross1 = gross1*26;
 }
 else if (indexofGrossIncome1 == 4)
 {
 gross1 = gross1*52;
 }
 
 
 
 if (indexofGrossIncome2 == 1)
 {
 gross2 = gross2;
 }
 else if (indexofGrossIncome2 == 2)
 {
 gross2 = gross2*12;
 }
 else if (indexofGrossIncome2 == 3)
 {
 gross2 = gross2*26;
 }
 else if (indexofGrossIncome2 == 4)
 {
 gross2 = gross2*52;
 }
 
 
 
 
 
 if (indexofUntaxedIncome ==1)
 {
 untaxedIncomde = untaxedIncomde;
 }
 else if (indexofUntaxedIncome ==2)
 {
 untaxedIncomde = untaxedIncomde*12;
 }
 else if (indexofUntaxedIncome ==3)
 {
 untaxedIncomde = untaxedIncomde*26;
 }
 else if (indexofUntaxedIncome ==4)
 {
 untaxedIncomde = untaxedIncomde*52;
 }
 
 
 
 if (indexofRentalIncome == 1)
 {
 rentalIncome = rentalIncome;
 }
 else if (indexofRentalIncome == 2)
 {
 rentalIncome = rentalIncome*12;
 }
 else if (indexofRentalIncome == 3)
 {
 rentalIncome = rentalIncome*26;
 }
 else if (indexofRentalIncome == 4)
 {
 rentalIncome = rentalIncome*52;
 }
 
 totalIncome = gross1 + untaxedIncomde + rentalIncome;
 
 }
 else if (indexofApplicationType == 2)
 {
 
 if (numberOfDependants == 0)
 {
 ExpJointDependants = 23930.0;
 }
 else if(numberOfDependants == 1)
 {
 ExpJointDependants = 29746.0;
 }
 else if (numberOfDependants == 2)
 {
 ExpJointDependants = 35562.0;
 }
 else if(numberOfDependants == 3)
 {
 ExpJointDependants = 41379.0;
 }
 else if (numberOfDependants == 4)
 {
 ExpJointDependants = 47130.0;
 }
 
 if (indexofGrossIncome1 == 1)
 {
 gross1 = gross1;
 }
 else if (indexofGrossIncome1 == 2)
 {
 gross1 = gross1*12;
 }
 else if (indexofGrossIncome1 == 3)
 {
 gross1 = gross1*26;
 }
 else if (indexofGrossIncome1 == 4)
 {
 gross1 = gross1*52;
 }
 
 
 
 if (indexofGrossIncome2 == 1)
 {
 gross2 = gross2;
 }
 else if (indexofGrossIncome2 == 2)
 {
 gross2 = gross2*12;
 }
 else if (indexofGrossIncome2 == 3)
 {
 gross2 = gross2*26;
 }
 else if (indexofGrossIncome2 == 4)
 {
 gross2 = gross2*52;
 }
 
 
 
 
 
 if (indexofUntaxedIncome ==1)
 {
 untaxedIncomde = untaxedIncomde;
 }
 else if (indexofUntaxedIncome ==2)
 {
 untaxedIncomde = untaxedIncomde*12;
 }
 else if (indexofUntaxedIncome ==3)
 {
 untaxedIncomde = untaxedIncomde*26;
 }
 else if (indexofUntaxedIncome ==4)
 {
 untaxedIncomde = untaxedIncomde*52;
 }
 
 
 
 if (indexofRentalIncome == 1)
 {
 rentalIncome = rentalIncome;
 }
 else if (indexofRentalIncome == 2)
 {
 rentalIncome = rentalIncome*12;
 }
 else if (indexofRentalIncome == 3)
 {
 rentalIncome = rentalIncome*26;
 }
 else if (indexofRentalIncome == 4)
 {
 rentalIncome = rentalIncome*52;
 }
 
 totalIncome = gross1 + gross2 + untaxedIncomde + rentalIncome;
 }
 
 if (indexofOtherLoanRepayment == 1)
 {
 otherLoan = otherLoan * 1;
 }
 else if (indexofOtherLoanRepayment == 2)
 {
 otherLoan = otherLoan * 12;
 }
 if (indexofOtherLoanRepayment == 3)
 {
 otherLoan = otherLoan * 52;
 }
 totalExp = otherLoan + ([strCarLoanPerMonth doubleValue]*12) +creditCardInterest + ExpJointDependants;
 
 totalAnnualAllowns = totalIncome - totalExp;
 totalMonthlyAllowns = totalAnnualAllowns/12;
 youCanBorrow = totalMonthlyAllowns * finalInterest;
 
 if (youCanBorrow<1)
 {
 youCanBorrow = 0.00;
 }
 
 NSString *strAns = [NSString stringWithFormat:@"%.f",youCanBorrow];
 
 NSInteger length = strAns.length;
 if (length >4)
 {
 NSInteger start = length - 3;
 strAns = [strAns stringByReplacingCharactersInRange:NSMakeRange(start, 3) withString:@"000"];
 strAns = [NSString stringWithFormat:@"$%@.00",strAns];
 }
 else
 {
 strAns = [NSString stringWithFormat:@"$%.2f",youCanBorrow];
 }
 [print printDescription:strAns message:@"You Can Borrow"];
 
 strYoumayBorrow = strAns;
 [strYoumayBorrow retain];
 
 double r = [strInterestRate doubleValue];
 r = r/1200;
 
 
 strAns = [strAns stringByReplacingOccurrencesOfString:@"$" withString:@""];
 
 double x = 1+r;
 double b = pow(x, month) -1;
 double ans1 = (r +(r/b)) *[strAns doubleValue];
 if (ans1 < 0 || isnan(ans1)) {
 ans1 = 0.00;
 }
 strMonthlyRepayment = [NSString stringWithFormat:@"$%.2f",ans1];
 [strMonthlyRepayment retain];
 ans1 = ans1 *month;
 if (ans1 < 0  || isnan(ans1)) {
 ans1 = 0.00;
 }
 strTotalInterestPayable = [NSString stringWithFormat:@"$%.2f",ans1];
 nslog(@"strTotalInterestPayable == %@",strTotalInterestPayable);
 [strTotalInterestPayable retain];
 
 
 
 
 
 totalIncome = 0.0;
 totalExp = 0.0;
 totalAnnualAllowns = 0.0;
 totalMonthlyAllowns = 0.0;
 youCanBorrow = 0.0;
 [tableView reloadData];
 }*/
-(IBAction)btnInfo_Clicekd:(id)sender
{
    //some logic
    
}
-(IBAction)btnDoneClicked:(id)sender
{
    UITextField *textField = (UITextField*)[self.view viewWithTag:tg];
    [textField resignFirstResponder];
    [toolbar setHidden:YES];
    
    double rate = [strInterestRate doubleValue] + 1.5;
    rate = rate/1200;
    int month = [strTermOfLoan doubleValue] * 12;
    
    double R_plus_1 = rate + 1;
    double R_plus_1_res_month = pow(R_plus_1, month);
    double R_plus_1_res_month_min_1 = R_plus_1_res_month - 1;
    double R_into_R_plus_1_res_month = rate * R_plus_1_res_month;
    double finalInterest = R_plus_1_res_month_min_1/R_into_R_plus_1_res_month;
    
    [print printDescription:[NSString stringWithFormat:@"%.2f",finalInterest] message:@"FinalInterest"];
    int numberOfDependants = [strNumberOfDepartment intValue];
    nslog(@"numberOfDependants == %d",numberOfDependants);
    if(numberOfDependants > 4)
    {
        UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Number of dependants must be less than 5." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Alert show];
        [Alert release];
        return;
    }
    NSString *strGross1 = strGrossINcome1;
    
    strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"," withString:@""];
    strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strGross2 = strGrossINcome2;
    
    strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"," withString:@""];
    strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strUntaxedIncome = strUntaxedAlloeableIncome;
    
    strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"," withString:@""];
    strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    NSString *strRental = strRentalIncome;
    
    strRental = [strRental stringByReplacingOccurrencesOfString:@"," withString:@""];
    strRental = [strRental stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    NSString *strOtherLoan = strOtherLoanRepayment;
    
    strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"," withString:@""];
    strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strCarLoanPerMonth = strCarLoanRepaymentPerMonth;
    
    strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"," withString:@""];
    strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strCreditCard = strTotalCreditcardLimit;
    
    strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"," withString:@""];
    strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    double creditCardInterest = [strCreditCard doubleValue] * 0.03 * 12;
    //    [print printDescription:[NSString stringWithFormat:@"%.2f",creditCardInterest] message:@"You Can Borrow"];
    
    double gross1,gross2,untaxedIncomde,rentalIncome,otherLoan;
    gross1 = [strGross1 doubleValue];
    gross2 = [strGross2 doubleValue];
    untaxedIncomde = [strUntaxedIncome doubleValue];
    rentalIncome = [strRental doubleValue];
    otherLoan = [strOtherLoan doubleValue];
    double ExpJointDependants = 0.0;
    if (indexofApplicationType == 1)
    {
        
        if (numberOfDependants == 0)
        {
            ExpJointDependants = 16460.0;
        }
        else if(numberOfDependants == 1)
        {
            ExpJointDependants = 22607.0;
        }
        else if (numberOfDependants == 2)
        {
            ExpJointDependants = 28423.0;
        }
        else if(numberOfDependants == 3)
        {
            ExpJointDependants = 34239.0;
        }
        else if (numberOfDependants == 4)
        {
            ExpJointDependants = 40056.0;
        }
        
        if (indexofGrossIncome1 == 1)
        {
            gross1 = gross1;
        }
        else if (indexofGrossIncome1 == 2)
        {
            gross1 = gross1*12;
        }
        else if (indexofGrossIncome1 == 3)
        {
            gross1 = gross1*26;
        }
        else if (indexofGrossIncome1 == 4)
        {
            gross1 = gross1*52;
        }
        
        
        
        if (indexofGrossIncome2 == 1)
        {
            gross2 = gross2;
        }
        else if (indexofGrossIncome2 == 2)
        {
            gross2 = gross2*12;
        }
        else if (indexofGrossIncome2 == 3)
        {
            gross2 = gross2*26;
        }
        else if (indexofGrossIncome2 == 4)
        {
            gross2 = gross2*52;
        }
        
        
        
        
        
        if (indexofUntaxedIncome ==1)
        {
            untaxedIncomde = untaxedIncomde;
        }
        else if (indexofUntaxedIncome ==2)
        {
            untaxedIncomde = untaxedIncomde*12;
        }
        else if (indexofUntaxedIncome ==3)
        {
            untaxedIncomde = untaxedIncomde*26;
        }
        else if (indexofUntaxedIncome ==4)
        {
            untaxedIncomde = untaxedIncomde*52;
        }
        
        
        
        if (indexofRentalIncome == 1)
        {
            rentalIncome = rentalIncome;
        }
        else if (indexofRentalIncome == 2)
        {
            rentalIncome = rentalIncome*12;
        }
        else if (indexofRentalIncome == 3)
        {
            rentalIncome = rentalIncome*26;
        }
        else if (indexofRentalIncome == 4)
        {
            rentalIncome = rentalIncome*52;
        }
        
        totalIncome = gross1 + untaxedIncomde + rentalIncome;
        
    }
    else if (indexofApplicationType == 2)
    {
        
        if (numberOfDependants == 0)
        {
            ExpJointDependants = 23930.0;
        }
        else if(numberOfDependants == 1)
        {
            ExpJointDependants = 29746.0;
        }
        else if (numberOfDependants == 2)
        {
            ExpJointDependants = 35562.0;
        }
        else if(numberOfDependants == 3)
        {
            ExpJointDependants = 41379.0;
        }
        else if (numberOfDependants == 4)
        {
            ExpJointDependants = 47130.0;
        }
        
        if (indexofGrossIncome1 == 1)
        {
            gross1 = gross1;
        }
        else if (indexofGrossIncome1 == 2)
        {
            gross1 = gross1*12;
        }
        else if (indexofGrossIncome1 == 3)
        {
            gross1 = gross1*26;
        }
        else if (indexofGrossIncome1 == 4)
        {
            gross1 = gross1*52;
        }
        
        
        
        if (indexofGrossIncome2 == 1)
        {
            gross2 = gross2;
        }
        else if (indexofGrossIncome2 == 2)
        {
            gross2 = gross2*12;
        }
        else if (indexofGrossIncome2 == 3)
        {
            gross2 = gross2*26;
        }
        else if (indexofGrossIncome2 == 4)
        {
            gross2 = gross2*52;
        }
        
        
        
        
        
        if (indexofUntaxedIncome ==1)
        {
            untaxedIncomde = untaxedIncomde;
        }
        else if (indexofUntaxedIncome ==2)
        {
            untaxedIncomde = untaxedIncomde*12;
        }
        else if (indexofUntaxedIncome ==3)
        {
            untaxedIncomde = untaxedIncomde*26;
        }
        else if (indexofUntaxedIncome ==4)
        {
            untaxedIncomde = untaxedIncomde*52;
        }
        
        
        
        if (indexofRentalIncome == 1)
        {
            rentalIncome = rentalIncome;
        }
        else if (indexofRentalIncome == 2)
        {
            rentalIncome = rentalIncome*12;
        }
        else if (indexofRentalIncome == 3)
        {
            rentalIncome = rentalIncome*26;
        }
        else if (indexofRentalIncome == 4)
        {
            rentalIncome = rentalIncome*52;
        }
        
        totalIncome = gross1 + gross2 + untaxedIncomde + rentalIncome;
    }
    
    if (indexofOtherLoanRepayment == 1)
    {
        otherLoan = otherLoan * 1;
    }
    else if (indexofOtherLoanRepayment == 2)
    {
        otherLoan = otherLoan * 12;
    }
    if (indexofOtherLoanRepayment == 3)
    {
        otherLoan = otherLoan * 52;
    }
    totalExp = otherLoan + ([strCarLoanPerMonth doubleValue]*12) +creditCardInterest + ExpJointDependants;
    
    totalAnnualAllowns = totalIncome - totalExp;
    totalMonthlyAllowns = totalAnnualAllowns/12;
    youCanBorrow = totalMonthlyAllowns * finalInterest;
    
    if (youCanBorrow<1)
    {
        youCanBorrow = 0.00;
    }
    
    NSString *strAns = [NSString stringWithFormat:@"%.f",youCanBorrow];
    
    NSInteger length = strAns.length;
    if (length >4)
    {
        NSInteger start = length - 3;
        strAns = [strAns stringByReplacingCharactersInRange:NSMakeRange(start, 3) withString:@"000"];
        strAns = [NSString stringWithFormat:@"$%@.00",strAns];
    }
    else
    {
        strAns = [NSString stringWithFormat:@"$%.2f",youCanBorrow];
    }
    [print printDescription:strAns message:@"You Can Borrow"];
    
    strYoumayBorrow = strAns;
    [strYoumayBorrow retain];
    
    double r = [strInterestRate doubleValue];
    r = r/1200;
    
    
    strAns = [strAns stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    double x = 1+r;
    double b = pow(x, month) -1;
    double ans1 = (r +(r/b)) *[strAns doubleValue];
    if (ans1 < 0 || isnan(ans1)) {
        ans1 = 0.00;
    }
    strMonthlyRepayment = [NSString stringWithFormat:@"$%.2f",ans1];
    [strMonthlyRepayment retain];
    ans1 = ans1 *month;
    if (ans1 < 0  || isnan(ans1)) {
        ans1 = 0.00;
    }
    strTotalInterestPayable = [NSString stringWithFormat:@"$%.2f",ans1];
    nslog(@"strTotalInterestPayable == %@",strTotalInterestPayable);
    
    [strTotalInterestPayable retain];
    
    
    
    
    
    totalIncome = 0.0;
    totalExp = 0.0;
    totalAnnualAllowns = 0.0;
    totalMonthlyAllowns = 0.0;
    youCanBorrow = 0.0;
    [tableView reloadData];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(selectKeyboard) userInfo:nil repeats:NO];
    
}

-(void)selectKeyboard
{
    UITextField*tempTextField = (UITextField*)[self.view viewWithTag:current_tag+1];
    nslog(@"\n text = %@",tempTextField.text);
    if(tempTextField!=nil)
    {
        [tempTextField becomeFirstResponder];
    }
    
}

#pragma mark TableView Methods



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == 3)
    {
        return 60;
    }
    return 0.0;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(section == 3)
    {
        return viewforfooter;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger cnt = 0;
    if (section == 0)
    {
        cnt = 5;
    }
    else if (section == 1)
    {
        cnt = 4;
    }
    else if (section == 2)
    {
        cnt = 2;
    }
    else if (section == 3)
    {
        cnt = 3;
    }
    
    return cnt;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float ht = 40;
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            ht = 40.0;
        }
        else if (indexPath.row == 1)
        {
            ht = 80.0;
        }
        else if (indexPath.row == 2)
        {
            ht = 80.0;
        }
        else if (indexPath.row == 3)
        {
            ht = 80.0;
        }
        else if (indexPath.row == 4)
        {
            ht = 80.0;
        }
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            ht = 80.0;
        }
        else if (indexPath.row == 1)
        {
            ht = 40.0;
        }
        else if (indexPath.row == 2)
        {
            ht = 40.0;
        }
        else if (indexPath.row == 3)
        {
            ht = 40.0;
        }
        
    }
    else if (indexPath.section == 2)
    {
        ht = 40.0;
        if (indexPath.row == 0)
        {
            
        }
        else if (indexPath.row == 1)
        {
            
        }
    }
    else if (indexPath.section == 3)
    {
        ht = 40.0;
        if (indexPath.row == 0)
        {
            
        }
        else if (indexPath.row == 1)
        {
            
        }
        else if (indexPath.row == 2)
        {
            
        }
    }
    
    return ht;
}
-(UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    BorrowCell *cell = (BorrowCell *)[tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    cell = [[[BorrowCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    
    cell.txtValues.delegate = self;
    cell.slider.hidden = TRUE;
    cell.btnAnnual.hidden = TRUE;
    cell.btnMonthly.hidden = TRUE;
    cell.btnFortnightly.hidden = TRUE;
    cell.btnWeekly.hidden = TRUE;
    cell.btnJoint.hidden = TRUE;
    cell.btnSingle.hidden = TRUE;
    cell.cellImageView.hidden = TRUE;
    cell.btnMonthlyBig.hidden = TRUE;
    cell.btnFortnightlyBig.hidden = TRUE;
    cell.btnWeeklyBig.hidden = TRUE;
    cell.lblTemp.frame = CGRectMake(10, 10, 205, 20);
    cell.lblTemp.hidden = FALSE;
    
    cell.btnAnnual.tag = 1000 + indexPath.row;
    cell.btnMonthly.tag = 2000 + indexPath.row;
    cell.btnFortnightly.tag = 3000 + indexPath.row;
    cell.btnWeekly.tag = 4000 + indexPath.row;
    
    [cell.btnAnnual addTarget:self action:@selector(segment4_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnMonthly addTarget:self action:@selector(segment4_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnFortnightly addTarget:self action:@selector(segment4_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnWeekly addTarget:self action:@selector(segment4_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    cell.btnMonthlyBig.tag = 22220;
    cell.btnFortnightlyBig.tag = 22221;
    cell.btnWeeklyBig.tag = 22222;
    
    [cell.btnMonthlyBig addTarget:self action:@selector(selectOtherLoanRepayment:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnFortnightlyBig addTarget:self action:@selector(selectOtherLoanRepayment:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnWeeklyBig addTarget:self action:@selector(selectOtherLoanRepayment:) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*
   
    cell.lblTemp.font = [UIFont systemFontOfSize:12.0f];
    */
    
     cell.txtValues.tag = 100*(indexPath.section+1) + indexPath.row;
    cell.lblTemp.textColor =[UIColor colorWithRed:56.0/255.0f green:49.0f/255.0f blue:49.0f/255.0f alpha:1.0f];
    cell.txtValues.textColor = [UIColor colorWithRed:3.0f/255.0f green:80.0/255.0 blue:103.0f/255.0 alpha:1.0];
    cell.lblPersent.textColor = [UIColor colorWithRed:3.0f/255.0f green:80.0/255.0 blue:103.0f/255.0 alpha:1.0];

    
    cell.lblTemp.font = [UIFont systemFontOfSize:12.0f];
    cell.txtValues.font = [UIFont boldSystemFontOfSize:13.0f];

    
    if (indexPath.section == 0)
    {
        
        [cell.slider setHidden:YES];
        [cell.btnMonthly setHidden:NO];
        [cell.btnWeekly setHidden:NO];
        [cell.btnFortnightly setHidden:NO];
        [cell.btnAnnual setHidden:NO];
        
        if (indexPath.row == 0)
        {
            
            
            [cell.txtValues setHidden:YES];
            
            [cell.btnMonthly setHidden:YES];
            [cell.btnWeekly setHidden:YES];
            [cell.btnFortnightly setHidden:YES];
            [cell.btnAnnual setHidden:YES];
            [cell.btnSingle setHidden:NO];
            [cell.btnJoint setHidden:NO];
            cell.btnJoint.tag = 11111;
            cell.btnSingle.tag = 11110;
            [cell.btnSingle addTarget:self action:@selector(selectApplicationType:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnJoint addTarget:self action:@selector(selectApplicationType:) forControlEvents:UIControlEventTouchUpInside];
            if (indexofApplicationType == 1)
            {
                [cell.btnSingle setBackgroundImage:[UIImage imageNamed:@"single_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnJoint setBackgroundImage:[UIImage imageNamed:@"joint_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofApplicationType == 2)
            {
                [cell.btnSingle setBackgroundImage:[UIImage imageNamed:@"single_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnJoint setBackgroundImage:[UIImage imageNamed:@"joint_btn_on.png"] forState:UIControlStateNormal];
            }
            cell.lblTemp.text = lblApplicationType;
            
        }
        else if (indexPath.row == 1)
        {
            cell.txtValues.text = strGrossINcome1;
            cell.lblTemp.text = lblGrossINcome1;
            if (indexofGrossIncome1 == 1)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofGrossIncome1 == 2)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofGrossIncome1 == 3)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofGrossIncome1 == 4)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_on.png"] forState:UIControlStateNormal];
            }
            
            
        }
        else if (indexPath.row == 2)
        {
            if (indexofApplicationType == 1)
            {
                [cell.txtValues setEnabled:NO];
            }
            else if (indexofApplicationType == 2)
            {
                [cell.txtValues setEnabled:YES];
            }
            
            if (indexofApplicationType ==1)
            {
                cell.btnAnnual.enabled = NO;
                cell.btnMonthly.enabled = NO;
                cell.btnFortnightly.enabled = NO;
                cell.btnWeekly.enabled = NO;
            }
            else
            {
                cell.btnAnnual.enabled = YES;
                cell.btnMonthly.enabled = YES;
                cell.btnFortnightly.enabled = YES;
                cell.btnWeekly.enabled = YES;
            }
            cell.txtValues.text = strGrossINcome2;
            cell.lblTemp.text = lblGrossINcome2;
            if (indexofGrossIncome2 == 1)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofGrossIncome2 == 2)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofGrossIncome2 == 3)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofGrossIncome2 == 4)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_on.png"] forState:UIControlStateNormal];
            }
            
        }
        else if (indexPath.row == 3)
        {
            cell.lblTemp.text = lblUntaxedAlloeableIncome;
            cell.txtValues.text = strUntaxedAlloeableIncome;
            if (indexofUntaxedIncome == 1)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofUntaxedIncome == 2)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofUntaxedIncome == 3)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofUntaxedIncome == 4)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_on.png"] forState:UIControlStateNormal];
            }
            
        }
        else if (indexPath.row == 4)
        {
            cell.lblTemp.text = lblRentalIncome;
            cell.txtValues.text = strRentalIncome;
            if (indexofRentalIncome == 1)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofRentalIncome == 2)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofRentalIncome == 3)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_on.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_off.png"] forState:UIControlStateNormal];
            }
            else if (indexofRentalIncome == 4)
            {
                [cell.btnAnnual setBackgroundImage:[UIImage imageNamed:@"annual_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnMonthly setBackgroundImage:[UIImage imageNamed:@"borrow_monthly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnFortnightly setBackgroundImage:[UIImage imageNamed:@"fotnightly_btn_off.png"] forState:UIControlStateNormal];
                [cell.btnWeekly setBackgroundImage:[UIImage imageNamed:@"weekly_btn_on.png"] forState:UIControlStateNormal];
            }
            
        }
        
    }
    else if (indexPath.section == 1)
    {
        [cell.btnMonthlyBig setHidden:YES];
        [cell.btnWeeklyBig setHidden:YES];
        [cell.btnFortnightlyBig setHidden:YES];
        
        if (indexPath.row == 0)
        {
            cell.lblTemp.text = lblOtherLoanRepayment;
            cell.txtValues.text = strOtherLoanRepayment;
            [cell.btnMonthlyBig setHidden:NO];
            // [cell.btnWeeklyBig setHidden:NO];
            [cell.btnFortnightlyBig setHidden:NO];
            
            if (indexofOtherLoanRepayment == 1)
            {
                [cell.btnMonthlyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_annually_on@2x.png"] forState:UIControlStateNormal];
                [cell.btnFortnightlyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_monthly_off@2x.png"] forState:UIControlStateNormal];
                /*
                [cell.btnWeeklyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_monthly_off@2x.png"] forState:UIControlStateNormal];
                 */
            }
            else if (indexofOtherLoanRepayment == 2)
            {
                [cell.btnMonthlyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_annually_off@2x.png"] forState:UIControlStateNormal];
                
                [cell.btnFortnightlyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_monthly_on@2x.png"] forState:UIControlStateNormal];
                
                /*
                [cell.btnWeeklyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_monthly_off@2x.png"] forState:UIControlStateNormal];
                 */
            }
            else if (indexofOtherLoanRepayment == 3)
            {
                [cell.btnMonthlyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_monthly_off@2x.png"] forState:UIControlStateNormal];
                
                
                [cell.btnFortnightlyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_fortnightly_off@2x.png"] forState:UIControlStateNormal];
                
                
                /*
                [cell.btnWeeklyBig setBackgroundImage:[UIImage imageNamed:@"borrow_big_weekly_on@2x.png"] forState:UIControlStateNormal];
                 */
            }
            
        }
        else if (indexPath.row == 1)
        {
            cell.txtValues.frame = CGRectMake(215, 10, 75, 20);
            cell.txtValues.text = strCarLoanRepaymentPerMonth;
            cell.lblTemp.text = lblCarLoanRepaymentPerMonth;
        }
        else if (indexPath.row == 2)
        {
            cell.txtValues.text = strTotalCreditcardLimit;
            cell.lblTemp.text = lblTotalCreditcardLimit;
        }
        else if (indexPath.row == 3)
        {
            nslog(@"strNumberOfDepartment == %@",strNumberOfDepartment);
            cell.txtValues.text =strNumberOfDepartment;
            cell.lblTemp.text = lblNumberOfDepartment;
            
            int numberOfDependants = [strNumberOfDepartment intValue];
            nslog(@"numberOfDependants == %d",numberOfDependants);
            //cell.slider.maximumValue = 4;
            //cell.slider.minimumValue = 0;
            // [cell.slider setValue:[strNumberOfDepartment floatValue]];
            //[cell.slider setHidden:NO];
            // cell.txtValues.enabled = NO;
            //cell.slider.tag = 777;
        }
    }
    else if (indexPath.section == 2)
    {
        [cell.slider setHidden:YES];
        if (indexPath.row ==0)
        {
            cell.txtValues.text = strTermOfLoan;
            cell.lblTemp.text = lblTermOfLoan;
            cell.slider.maximumValue = 100.0;
            cell.slider.minimumValue = 0.0;
            cell.slider.value = [strTermOfLoan floatValue] * 2;
            cell.txtValues.enabled = YES;
            cell.slider.tag = 888;
        }
        else if (indexPath.row == 1)
        {
            cell.txtValues.text = strInterestRate;
            cell.lblTemp.text = lblInterestRate;
            cell.slider.maximumValue = 30.0;
            cell.slider.minimumValue = 0.0;
            [cell.slider setValue:[strInterestRate floatValue]];
            cell.lblPersent.frame = CGRectMake(278, 10, 20, 20);
            cell.txtValues.frame = CGRectMake(150, 10, 120, 20);
            [cell.lblPersent setHidden:NO];
            [cell.txtValues setEnabled:YES];
            [cell.txtValues setHidden:NO];
            cell.slider.tag = 999;
        }
    }
    else if (indexPath.section == 3)
    {
        [cell.cellImageView setHidden:NO];
        [cell.txtValues setEnabled:NO];
        cell.txtValues.frame = CGRectMake(150, 10, 140, 20);
        
        cell.lblTemp.textColor = [UIColor whiteColor];
        cell.txtValues.textColor = [UIColor whiteColor];

        
        
        if (indexPath.row == 0)
        {

            cell.lblTemp.text = lblYouMayBorrow;
            cell.txtValues.text = strYoumayBorrow;
            cell.cellImageView.image = [UIImage imageNamed:@"calculation_summary_top_cell_bg.png"];
        }
        else if (indexPath.row == 1)
        {
            
            cell.lblTemp.text = lblMonthlyRepayment;
            cell.txtValues.text = strMonthlyRepayment;
           cell.cellImageView.image = [UIImage imageNamed:@"calculation_summary_middle_cell_bg.png"];
        }
        else if (indexPath.row == 2)
        {
            cell.lblTemp.text = lblTotalInterestPayable;
            //NSString *str = [NSString stringWithFormat:@"%f",[strTotalInterestPayable floatValue]-[strYoumayBorrow floatValue]];
            NSString *str = [strYoumayBorrow substringFromIndex:1];
            nslog(@"str == %@",str);
            NSString *str1 = [strTotalInterestPayable substringFromIndex:1];
            nslog(@"str1 == %@",str1);
            //NSString *str = [NSString stringWithFormat:@"%f",[str floatValue]-[str1 floatValue]];
            float f = [str1 floatValue]-[str floatValue];
            NSString *Str2 = [NSString stringWithFormat:@"%.2f",f];
            cell.txtValues.text = [NSString stringWithFormat:@"$%.2f",[Str2 floatValue]];
            cell.cellImageView.image = [UIImage imageNamed:@"calculation_summary_bottom_cell_bg.png"];
        }
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    return cell;
}





#pragma mark TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL istag = FALSE;
    [textField setKeyboardType:UIKeyboardTypeNumberPad];
    istag = TRUE;
    current_tag = textField.tag;
    return istag;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [toolbar setHidden:YES];
    [textField resignFirstResponder];
    
    if (textField.tag == 101)
    {
        strGrossINcome1 = textField.text;
        [strGrossINcome1 retain];
    }
    else if (textField.tag == 102)
    {
        strGrossINcome2 = textField.text;
        [strGrossINcome2 retain];
    }
    else if (textField.tag == 103)
    {
        strUntaxedAlloeableIncome = textField.text;
        [strUntaxedAlloeableIncome retain];
    }
    else if (textField.tag == 104)
    {
        strRentalIncome = textField.text;
        [strRentalIncome retain];
    }
    else if (textField.tag == 200)
    {
        strOtherLoanRepayment = textField.text;
        [strOtherLoanRepayment retain];
    }
    else if (textField.tag == 201)
    {
        strCarLoanRepaymentPerMonth = textField.text;
        [strCarLoanRepaymentPerMonth retain];
    }
    else if (textField.tag == 202)
    {
        strTotalCreditcardLimit = textField.text;
        [strTotalCreditcardLimit retain];
    }
    else if (textField.tag == 203)
    {
        strNumberOfDepartment = textField.text;
        [strNumberOfDepartment retain];
        nslog(@"strNumberOfDepartment == %@",strNumberOfDepartment);
        if(strNumberOfDepartment.length==0)
        {
            strNumberOfDepartment = @"0";
        }
        if([strNumberOfDepartment intValue]>4)
        {
            UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Number of dependants less than 5." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [Alert show];
            [Alert release];
            
            strNumberOfDepartment = @"4";
        }
    }
    else if (textField.tag == 300)
    {
        strTermOfLoan = textField.text;
        [strTermOfLoan retain];
    }
    else if (textField.tag == 301)
    {
        strInterestRate = textField.text;
        [strInterestRate retain];
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag == 300)
    {
        tg = textField.tag;
        
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:2];
        [tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:TRUE];
    }
    else if(textField.tag == 301)
    {
        tg = textField.tag;
        
        NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:2];
        [tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:TRUE];
    }
    else
    {
        tg = textField.tag;
        CGPoint pnt = [tableView convertPoint:textField.bounds.origin fromView:textField];
        NSIndexPath* path = [tableView indexPathForRowAtPoint:pnt];
        nslog(@"tg = %d",tg);
        [tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    [toolbar setHidden:NO];
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL istag = FALSE;
    // if (textField.tag != 203 && textField.tag != 300 && textField.tag != 301)
    //   if (textField.tag != 300 && textField.tag != 301)
    if (textField.tag != 203)
        
    {
        
        
        NSMutableCharacterSet *numberSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
        [numberSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
        NSCharacterSet *nonNumberSet = [numberSet invertedSet];
        
        BOOL result = NO;
        
        if([string length] == 0){
            result = YES;
        }
        else{
            if([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0){
                result = YES;
            }
        }
        
        if(result){
            NSMutableString* mstring = [[textField text] mutableCopy];
            
            if([string length] > 0){
                [mstring insertString:string atIndex:range.location];
            }
            else {
                [mstring deleteCharactersInRange:range];
            }
            
            NSLocale* locale = [NSLocale currentLocale];
            NSString *localCurrencySymbol = [locale objectForKey:NSLocaleCurrencySymbol];
            NSString *localGroupingSeparator = [locale objectForKey:NSLocaleGroupingSeparator];
            
            NSString* clean_string = [[mstring stringByReplacingOccurrencesOfString:localGroupingSeparator
                                                                         withString:@""]
                                      stringByReplacingOccurrencesOfString:localCurrencySymbol
                                      withString:@""];
            
            if([[Formatters currencyFormatter] maximumFractionDigits] > 0){
                NSMutableString *mutableCleanString = [clean_string mutableCopy];
                
                if([string length] > 0){
                    NSRange theRange = [mutableCleanString rangeOfString:@"."];
                    [mutableCleanString deleteCharactersInRange:theRange];
                    [mutableCleanString insertString:@"." atIndex:(theRange.location + 1)];
                    clean_string = mutableCleanString;
                }
                else {
                    [mutableCleanString insertString:@"0" atIndex:0];
                    NSRange theRange = [mutableCleanString rangeOfString:@"."];
                    [mutableCleanString deleteCharactersInRange:theRange];
                    [mutableCleanString insertString:@"." atIndex:(theRange.location - 1)];
                    clean_string = mutableCleanString;
                }
            }
            
            NSNumber* number = [[Formatters basicFormatter] numberFromString: clean_string];
            NSMutableString *numberString = [[[Formatters currencyFormatter] stringFromNumber:number] mutableCopy];
            [numberString deleteCharactersInRange:NSMakeRange(0, 1)];
            
            [textField setText:numberString];
            //        if (textField.tag == 100 || textField.tag == 200)
            //        {
            if (textField.tag != 300 && textField.tag != 301)
            {
                textField.text = [@"$" stringByAppendingString:textField.text];
            }
           
            
            
        }
    }
    if (textField.tag == 203)
    {
        /*
         nslog(@"textField.text == %@",textField.text);
         
         if(textField.text.length == 0)
         {
         strNumberOfDepartment = 0;
         }
         
         nslog(@"textField.text == %@",textField.text);*/
        istag = TRUE;
    }
    else
    {
        //        istag = TRUE;
    }
    return istag; // we return NO because we have manually edited the textField contents.
}
#pragma mark Segment Clicked
-(void)selectApplicationType:(id)sender
{
    if ([sender tag] == 11110)
    {
        indexofApplicationType = 1;
    }
    else if ([sender tag] == 11111)
    {
        indexofApplicationType = 2;
    }
    
    
    double rate = [strInterestRate doubleValue] + 1.5;
    rate = rate/1200;
    int month = [strTermOfLoan doubleValue] * 12;
    
    double R_plus_1 = rate + 1;
    double R_plus_1_res_month = pow(R_plus_1, month);
    double R_plus_1_res_month_min_1 = R_plus_1_res_month - 1;
    double R_into_R_plus_1_res_month = rate * R_plus_1_res_month;
    double finalInterest = R_plus_1_res_month_min_1/R_into_R_plus_1_res_month;
    
    [print printDescription:[NSString stringWithFormat:@"%.2f",finalInterest] message:@"FinalInterest"];
    int numberOfDependants = [strNumberOfDepartment intValue];
    
    
    NSString *strGross1 = strGrossINcome1;
    
    strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"," withString:@""];
    strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strGross2 = strGrossINcome2;
    
    strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"," withString:@""];
    strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strUntaxedIncome = strUntaxedAlloeableIncome;
    
    strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"," withString:@""];
    strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    NSString *strRental = strRentalIncome;
    
    strRental = [strRental stringByReplacingOccurrencesOfString:@"," withString:@""];
    strRental = [strRental stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    NSString *strOtherLoan = strOtherLoanRepayment;
    
    strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"," withString:@""];
    strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strCarLoanPerMonth = strCarLoanRepaymentPerMonth;
    
    strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"," withString:@""];
    strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strCreditCard = strTotalCreditcardLimit;
    
    strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"," withString:@""];
    strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    double creditCardInterest = [strCreditCard doubleValue] * 0.03 * 12;
    //    [print printDescription:[NSString stringWithFormat:@"%.2f",creditCardInterest] message:@"You Can Borrow"];
    
    double gross1,gross2,untaxedIncomde,rentalIncome,otherLoan;
    gross1 = [strGross1 doubleValue];
    gross2 = [strGross2 doubleValue];
    untaxedIncomde = [strUntaxedIncome doubleValue];
    rentalIncome = [strRental doubleValue];
    otherLoan = [strOtherLoan doubleValue];
    double ExpJointDependants = 0.0;
    if (indexofApplicationType == 1)
    {
        
        if (numberOfDependants == 0)
        {
            ExpJointDependants = 16460.0;
        }
        else if(numberOfDependants == 1)
        {
            ExpJointDependants = 22607.0;
        }
        else if (numberOfDependants == 2)
        {
            ExpJointDependants = 28423.0;
        }
        else if(numberOfDependants == 3)
        {
            ExpJointDependants = 34239.0;
        }
        else if (numberOfDependants == 4)
        {
            ExpJointDependants = 40056.0;
        }
        
        if (indexofGrossIncome1 == 1)
        {
            gross1 = gross1;
        }
        else if (indexofGrossIncome1 == 2)
        {
            gross1 = gross1*12;
        }
        else if (indexofGrossIncome1 == 3)
        {
            gross1 = gross1*26;
        }
        else if (indexofGrossIncome1 == 4)
        {
            gross1 = gross1*52;
        }
        
        
        
        if (indexofGrossIncome2 == 1)
        {
            gross2 = gross2;
        }
        else if (indexofGrossIncome2 == 2)
        {
            gross2 = gross2*12;
        }
        else if (indexofGrossIncome2 == 3)
        {
            gross2 = gross2*26;
        }
        else if (indexofGrossIncome2 == 4)
        {
            gross2 = gross2*52;
        }
        
        
        
        
        
        if (indexofUntaxedIncome ==1)
        {
            untaxedIncomde = untaxedIncomde;
        }
        else if (indexofUntaxedIncome ==2)
        {
            untaxedIncomde = untaxedIncomde*12;
        }
        else if (indexofUntaxedIncome ==3)
        {
            untaxedIncomde = untaxedIncomde*26;
        }
        else if (indexofUntaxedIncome ==4)
        {
            untaxedIncomde = untaxedIncomde*52;
        }
        
        
        
        if (indexofRentalIncome == 1)
        {
            rentalIncome = rentalIncome;
        }
        else if (indexofRentalIncome == 2)
        {
            rentalIncome = rentalIncome*12;
        }
        else if (indexofRentalIncome == 3)
        {
            rentalIncome = rentalIncome*26;
        }
        else if (indexofRentalIncome == 4)
        {
            rentalIncome = rentalIncome*52;
        }
        
        totalIncome = gross1 + untaxedIncomde + rentalIncome;
        
    }
    else if (indexofApplicationType == 2)
    {
        
        if (numberOfDependants == 0)
        {
            ExpJointDependants = 23930.0;
        }
        else if(numberOfDependants == 1)
        {
            ExpJointDependants = 29746.0;
        }
        else if (numberOfDependants == 2)
        {
            ExpJointDependants = 35562.0;
        }
        else if(numberOfDependants == 3)
        {
            ExpJointDependants = 41379.0;
        }
        else if (numberOfDependants == 4)
        {
            ExpJointDependants = 47130.0;
        }
        
        if (indexofGrossIncome1 == 1)
        {
            gross1 = gross1;
        }
        else if (indexofGrossIncome1 == 2)
        {
            gross1 = gross1*12;
        }
        else if (indexofGrossIncome1 == 3)
        {
            gross1 = gross1*26;
        }
        else if (indexofGrossIncome1 == 4)
        {
            gross1 = gross1*52;
        }
        
        
        
        if (indexofGrossIncome2 == 1)
        {
            gross2 = gross2;
        }
        else if (indexofGrossIncome2 == 2)
        {
            gross2 = gross2*12;
        }
        else if (indexofGrossIncome2 == 3)
        {
            gross2 = gross2*26;
        }
        else if (indexofGrossIncome2 == 4)
        {
            gross2 = gross2*52;
        }
        
        
        
        
        
        if (indexofUntaxedIncome ==1)
        {
            untaxedIncomde = untaxedIncomde;
        }
        else if (indexofUntaxedIncome ==2)
        {
            untaxedIncomde = untaxedIncomde*12;
        }
        else if (indexofUntaxedIncome ==3)
        {
            untaxedIncomde = untaxedIncomde*26;
        }
        else if (indexofUntaxedIncome ==4)
        {
            untaxedIncomde = untaxedIncomde*52;
        }
        
        
        
        if (indexofRentalIncome == 1)
        {
            rentalIncome = rentalIncome;
        }
        else if (indexofRentalIncome == 2)
        {
            rentalIncome = rentalIncome*12;
        }
        else if (indexofRentalIncome == 3)
        {
            rentalIncome = rentalIncome*26;
        }
        else if (indexofRentalIncome == 4)
        {
            rentalIncome = rentalIncome*52;
        }
        
        totalIncome = gross1 + gross2 + untaxedIncomde + rentalIncome;
    }
    
    if (indexofOtherLoanRepayment == 1)
    {
        otherLoan = otherLoan * 1;
    }
    else if (indexofOtherLoanRepayment == 2)
    {
        otherLoan = otherLoan * 12;
    }
    if (indexofOtherLoanRepayment == 3)
    {
        otherLoan = otherLoan * 52;
    }
    totalExp = otherLoan + ([strCarLoanPerMonth doubleValue]*12) +creditCardInterest + ExpJointDependants;
    
    totalAnnualAllowns = totalIncome - totalExp;
    totalMonthlyAllowns = totalAnnualAllowns/12;
    youCanBorrow = totalMonthlyAllowns * finalInterest;
    
    if (youCanBorrow<1)
    {
        youCanBorrow = 0.00;
    }
    
    NSString *strAns = [NSString stringWithFormat:@"%.f",youCanBorrow];
    
    NSInteger length = strAns.length;
    if (length >4)
    {
        NSInteger start = length - 3;
        strAns = [strAns stringByReplacingCharactersInRange:NSMakeRange(start, 3) withString:@"000"];
        strAns = [NSString stringWithFormat:@"$%@.00",strAns];
    }
    else
    {
        strAns = [NSString stringWithFormat:@"$%.2f",youCanBorrow];
    }
    [print printDescription:strAns message:@"You Can Borrow"];
    
    strYoumayBorrow = strAns;
    [strYoumayBorrow retain];
    
    double r = [strInterestRate doubleValue];
    r = r/1200;
    
    
    strAns = [strAns stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    double x = 1+r;
    double b = pow(x, month) -1;
    double ans1 = (r +(r/b)) *[strAns doubleValue];
    if (ans1 < 0 || isnan(ans1)) {
        ans1 = 0.00;
    }
    strMonthlyRepayment = [NSString stringWithFormat:@"$%.2f",ans1];
    [strMonthlyRepayment retain];
    ans1 = ans1 *month;
    if (ans1 < 0  || isnan(ans1)) {
        ans1 = 0.00;
    }
    strTotalInterestPayable = [NSString stringWithFormat:@"$%.2f",ans1];
    nslog(@"strTotalInterestPayable == %@",strTotalInterestPayable);
    
    [strTotalInterestPayable retain];
    
    
    
    
    
    totalIncome = 0.0;
    totalExp = 0.0;
    totalAnnualAllowns = 0.0;
    totalMonthlyAllowns = 0.0;
    youCanBorrow = 0.0;
    [tableView reloadData];
    
}
-(void)segment4_Clicked:(id)sender
{
    if ([sender tag] == 1001)
    {
        indexofGrossIncome1 = 1;
    }
    else if ([sender tag] == 1002)
    {
        indexofGrossIncome2 = 1;
    }
    else if ([sender tag] == 1003)
    {
        indexofUntaxedIncome = 1;
    }
    else if ([sender tag] == 1004)
    {
        indexofRentalIncome = 1;
    }
    else if ([sender tag] == 2001)
    {
        indexofGrossIncome1 = 2;
    }
    else if ([sender tag] == 2002)
    {
        indexofGrossIncome2 = 2;
    }
    else if ([sender tag] == 2003)
    {
        indexofUntaxedIncome = 2;
    }
    else if ([sender tag] == 2004)
    {
        indexofRentalIncome = 2;
    }
    else if ([sender tag] == 3001)
    {
        indexofGrossIncome1 = 3;
    }
    else if ([sender tag] == 3002)
    {
        indexofGrossIncome2 = 3;
    }
    else if ([sender tag] == 3003)
    {
        indexofUntaxedIncome = 3;
    }
    else if ([sender tag] == 3004)
    {
        indexofRentalIncome = 3;
    }
    else if ([sender tag] == 4001)
    {
        indexofGrossIncome1 = 4;
    }
    else if ([sender tag] == 4002)
    {
        indexofGrossIncome2 = 4;
    }
    else if ([sender tag] == 4003)
    {
        indexofUntaxedIncome = 4;
    }
    else if ([sender tag] == 4004)
    {
        indexofRentalIncome = 4;
    }
    
    
    double rate = [strInterestRate doubleValue] + 1.5;
    rate = rate/1200;
    int month = [strTermOfLoan doubleValue] * 12;
    
    double R_plus_1 = rate + 1;
    double R_plus_1_res_month = pow(R_plus_1, month);
    double R_plus_1_res_month_min_1 = R_plus_1_res_month - 1;
    double R_into_R_plus_1_res_month = rate * R_plus_1_res_month;
    double finalInterest = R_plus_1_res_month_min_1/R_into_R_plus_1_res_month;
    
    [print printDescription:[NSString stringWithFormat:@"%.2f",finalInterest] message:@"FinalInterest"];
    int numberOfDependants = [strNumberOfDepartment intValue];
    
    
    NSString *strGross1 = strGrossINcome1;
    
    strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"," withString:@""];
    strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strGross2 = strGrossINcome2;
    
    strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"," withString:@""];
    strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strUntaxedIncome = strUntaxedAlloeableIncome;
    
    strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"," withString:@""];
    strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    NSString *strRental = strRentalIncome;
    
    strRental = [strRental stringByReplacingOccurrencesOfString:@"," withString:@""];
    strRental = [strRental stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    NSString *strOtherLoan = strOtherLoanRepayment;
    
    strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"," withString:@""];
    strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strCarLoanPerMonth = strCarLoanRepaymentPerMonth;
    
    strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"," withString:@""];
    strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strCreditCard = strTotalCreditcardLimit;
    
    strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"," withString:@""];
    strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    double creditCardInterest = [strCreditCard doubleValue] * 0.03 * 12;
    //    [print printDescription:[NSString stringWithFormat:@"%.2f",creditCardInterest] message:@"You Can Borrow"];
    
    double gross1,gross2,untaxedIncomde,rentalIncome,otherLoan;
    gross1 = [strGross1 doubleValue];
    gross2 = [strGross2 doubleValue];
    untaxedIncomde = [strUntaxedIncome doubleValue];
    rentalIncome = [strRental doubleValue];
    otherLoan = [strOtherLoan doubleValue];
    double ExpJointDependants = 0.0;
    if (indexofApplicationType == 1)
    {
        
        if (numberOfDependants == 0)
        {
            ExpJointDependants = 16460.0;
        }
        else if(numberOfDependants == 1)
        {
            ExpJointDependants = 22607.0;
        }
        else if (numberOfDependants == 2)
        {
            ExpJointDependants = 28423.0;
        }
        else if(numberOfDependants == 3)
        {
            ExpJointDependants = 34239.0;
        }
        else if (numberOfDependants == 4)
        {
            ExpJointDependants = 40056.0;
        }
        
        if (indexofGrossIncome1 == 1)
        {
            gross1 = gross1;
        }
        else if (indexofGrossIncome1 == 2)
        {
            gross1 = gross1*12;
        }
        else if (indexofGrossIncome1 == 3)
        {
            gross1 = gross1*26;
        }
        else if (indexofGrossIncome1 == 4)
        {
            gross1 = gross1*52;
        }
        
        
        
        if (indexofGrossIncome2 == 1)
        {
            gross2 = gross2;
        }
        else if (indexofGrossIncome2 == 2)
        {
            gross2 = gross2*12;
        }
        else if (indexofGrossIncome2 == 3)
        {
            gross2 = gross2*26;
        }
        else if (indexofGrossIncome2 == 4)
        {
            gross2 = gross2*52;
        }
        
        
        
        
        
        if (indexofUntaxedIncome ==1)
        {
            untaxedIncomde = untaxedIncomde;
        }
        else if (indexofUntaxedIncome ==2)
        {
            untaxedIncomde = untaxedIncomde*12;
        }
        else if (indexofUntaxedIncome ==3)
        {
            untaxedIncomde = untaxedIncomde*26;
        }
        else if (indexofUntaxedIncome ==4)
        {
            untaxedIncomde = untaxedIncomde*52;
        }
        
        
        
        if (indexofRentalIncome == 1)
        {
            rentalIncome = rentalIncome;
        }
        else if (indexofRentalIncome == 2)
        {
            rentalIncome = rentalIncome*12;
        }
        else if (indexofRentalIncome == 3)
        {
            rentalIncome = rentalIncome*26;
        }
        else if (indexofRentalIncome == 4)
        {
            rentalIncome = rentalIncome*52;
        }
        
        totalIncome = gross1 + untaxedIncomde + rentalIncome;
        
    }
    else if (indexofApplicationType == 2)
    {
        
        if (numberOfDependants == 0)
        {
            ExpJointDependants = 23930.0;
        }
        else if(numberOfDependants == 1)
        {
            ExpJointDependants = 29746.0;
        }
        else if (numberOfDependants == 2)
        {
            ExpJointDependants = 35562.0;
        }
        else if(numberOfDependants == 3)
        {
            ExpJointDependants = 41379.0;
        }
        else if (numberOfDependants == 4)
        {
            ExpJointDependants = 47130.0;
        }
        
        if (indexofGrossIncome1 == 1)
        {
            gross1 = gross1;
        }
        else if (indexofGrossIncome1 == 2)
        {
            gross1 = gross1*12;
        }
        else if (indexofGrossIncome1 == 3)
        {
            gross1 = gross1*26;
        }
        else if (indexofGrossIncome1 == 4)
        {
            gross1 = gross1*52;
        }
        
        
        
        if (indexofGrossIncome2 == 1)
        {
            gross2 = gross2;
        }
        else if (indexofGrossIncome2 == 2)
        {
            gross2 = gross2*12;
        }
        else if (indexofGrossIncome2 == 3)
        {
            gross2 = gross2*26;
        }
        else if (indexofGrossIncome2 == 4)
        {
            gross2 = gross2*52;
        }
        
        
        
        
        
        if (indexofUntaxedIncome ==1)
        {
            untaxedIncomde = untaxedIncomde;
        }
        else if (indexofUntaxedIncome ==2)
        {
            untaxedIncomde = untaxedIncomde*12;
        }
        else if (indexofUntaxedIncome ==3)
        {
            untaxedIncomde = untaxedIncomde*26;
        }
        else if (indexofUntaxedIncome ==4)
        {
            untaxedIncomde = untaxedIncomde*52;
        }
        
        
        
        if (indexofRentalIncome == 1)
        {
            rentalIncome = rentalIncome;
        }
        else if (indexofRentalIncome == 2)
        {
            rentalIncome = rentalIncome*12;
        }
        else if (indexofRentalIncome == 3)
        {
            rentalIncome = rentalIncome*26;
        }
        else if (indexofRentalIncome == 4)
        {
            rentalIncome = rentalIncome*52;
        }
        
        totalIncome = gross1 + gross2 + untaxedIncomde + rentalIncome;
    }
    
    if (indexofOtherLoanRepayment == 1)
    {
        otherLoan = otherLoan * 1;
    }
    else if (indexofOtherLoanRepayment == 2)
    {
        otherLoan = otherLoan * 12;
    }
    if (indexofOtherLoanRepayment == 3)
    {
        otherLoan = otherLoan * 52;
    }
    totalExp = otherLoan + ([strCarLoanPerMonth doubleValue]*12) +creditCardInterest + ExpJointDependants;
    
    totalAnnualAllowns = totalIncome - totalExp;
    totalMonthlyAllowns = totalAnnualAllowns/12;
    youCanBorrow = totalMonthlyAllowns * finalInterest;
    
    if (youCanBorrow<1)
    {
        youCanBorrow = 0.00;
    }
    
    NSString *strAns = [NSString stringWithFormat:@"%.f",youCanBorrow];
    
    NSInteger length = strAns.length;
    if (length >4)
    {
        NSInteger start = length - 3;
        strAns = [strAns stringByReplacingCharactersInRange:NSMakeRange(start, 3) withString:@"000"];
        strAns = [NSString stringWithFormat:@"$%@.00",strAns];
    }
    else
    {
        strAns = [NSString stringWithFormat:@"$%.2f",youCanBorrow];
    }
    [print printDescription:strAns message:@"You Can Borrow"];
    
    strYoumayBorrow = strAns;
    [strYoumayBorrow retain];
    
    double r = [strInterestRate doubleValue];
    r = r/1200;
    
    
    strAns = [strAns stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    double x = 1+r;
    double b = pow(x, month) -1;
    double ans1 = (r +(r/b)) *[strAns doubleValue];
    if (ans1 < 0 || isnan(ans1)) {
        ans1 = 0.00;
    }
    strMonthlyRepayment = [NSString stringWithFormat:@"$%.2f",ans1];
    [strMonthlyRepayment retain];
    ans1 = ans1 *month;
    if (ans1 < 0  || isnan(ans1)) {
        ans1 = 0.00;
    }
    strTotalInterestPayable = [NSString stringWithFormat:@"$%.2f",ans1];
    nslog(@"strTotalInterestPayable == %@",strTotalInterestPayable);
    
    [strTotalInterestPayable retain];
    
    
    
    
    
    totalIncome = 0.0;
    totalExp = 0.0;
    totalAnnualAllowns = 0.0;
    totalMonthlyAllowns = 0.0;
    youCanBorrow = 0.0;
    [tableView reloadData];
    
}
-(void)selectOtherLoanRepayment:(id)sender
{
    if ([sender tag] == 22220)
    {
        indexofOtherLoanRepayment = 1;
    }
    else if ([sender tag] == 22221)
    {
        indexofOtherLoanRepayment = 2;
    }
    else if ([sender tag] == 22222)
    {
        indexofOtherLoanRepayment = 3;
    }
    
    
    double rate = [strInterestRate doubleValue] + 1.5;
    rate = rate/1200;
    int month = [strTermOfLoan doubleValue] * 12;
    
    double R_plus_1 = rate + 1;
    double R_plus_1_res_month = pow(R_plus_1, month);
    double R_plus_1_res_month_min_1 = R_plus_1_res_month - 1;
    double R_into_R_plus_1_res_month = rate * R_plus_1_res_month;
    double finalInterest = R_plus_1_res_month_min_1/R_into_R_plus_1_res_month;
    
    [print printDescription:[NSString stringWithFormat:@"%.2f",finalInterest] message:@"FinalInterest"];
    int numberOfDependants = [strNumberOfDepartment intValue];
    
    
    NSString *strGross1 = strGrossINcome1;
    
    strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"," withString:@""];
    strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strGross2 = strGrossINcome2;
    
    strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"," withString:@""];
    strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strUntaxedIncome = strUntaxedAlloeableIncome;
    
    strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"," withString:@""];
    strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    NSString *strRental = strRentalIncome;
    
    strRental = [strRental stringByReplacingOccurrencesOfString:@"," withString:@""];
    strRental = [strRental stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    NSString *strOtherLoan = strOtherLoanRepayment;
    
    strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"," withString:@""];
    strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strCarLoanPerMonth = strCarLoanRepaymentPerMonth;
    
    strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"," withString:@""];
    strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strCreditCard = strTotalCreditcardLimit;
    
    strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"," withString:@""];
    strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    double creditCardInterest = [strCreditCard doubleValue] * 0.03 * 12;
    //    [print printDescription:[NSString stringWithFormat:@"%.2f",creditCardInterest] message:@"You Can Borrow"];
    
    double gross1,gross2,untaxedIncomde,rentalIncome,otherLoan;
    gross1 = [strGross1 doubleValue];
    gross2 = [strGross2 doubleValue];
    untaxedIncomde = [strUntaxedIncome doubleValue];
    rentalIncome = [strRental doubleValue];
    otherLoan = [strOtherLoan doubleValue];
    double ExpJointDependants = 0.0;
    if (indexofApplicationType == 1)
    {
        
        if (numberOfDependants == 0)
        {
            ExpJointDependants = 16460.0;
        }
        else if(numberOfDependants == 1)
        {
            ExpJointDependants = 22607.0;
        }
        else if (numberOfDependants == 2)
        {
            ExpJointDependants = 28423.0;
        }
        else if(numberOfDependants == 3)
        {
            ExpJointDependants = 34239.0;
        }
        else if (numberOfDependants == 4)
        {
            ExpJointDependants = 40056.0;
        }
        
        if (indexofGrossIncome1 == 1)
        {
            gross1 = gross1;
        }
        else if (indexofGrossIncome1 == 2)
        {
            gross1 = gross1*12;
        }
        else if (indexofGrossIncome1 == 3)
        {
            gross1 = gross1*26;
        }
        else if (indexofGrossIncome1 == 4)
        {
            gross1 = gross1*52;
        }
        
        
        
        if (indexofGrossIncome2 == 1)
        {
            gross2 = gross2;
        }
        else if (indexofGrossIncome2 == 2)
        {
            gross2 = gross2*12;
        }
        else if (indexofGrossIncome2 == 3)
        {
            gross2 = gross2*26;
        }
        else if (indexofGrossIncome2 == 4)
        {
            gross2 = gross2*52;
        }
        
        
        
        
        
        if (indexofUntaxedIncome ==1)
        {
            untaxedIncomde = untaxedIncomde;
        }
        else if (indexofUntaxedIncome ==2)
        {
            untaxedIncomde = untaxedIncomde*12;
        }
        else if (indexofUntaxedIncome ==3)
        {
            untaxedIncomde = untaxedIncomde*26;
        }
        else if (indexofUntaxedIncome ==4)
        {
            untaxedIncomde = untaxedIncomde*52;
        }
        
        
        
        if (indexofRentalIncome == 1)
        {
            rentalIncome = rentalIncome;
        }
        else if (indexofRentalIncome == 2)
        {
            rentalIncome = rentalIncome*12;
        }
        else if (indexofRentalIncome == 3)
        {
            rentalIncome = rentalIncome*26;
        }
        else if (indexofRentalIncome == 4)
        {
            rentalIncome = rentalIncome*52;
        }
        
        totalIncome = gross1 + untaxedIncomde + rentalIncome;
        
    }
    else if (indexofApplicationType == 2)
    {
        
        if (numberOfDependants == 0)
        {
            ExpJointDependants = 23930.0;
        }
        else if(numberOfDependants == 1)
        {
            ExpJointDependants = 29746.0;
        }
        else if (numberOfDependants == 2)
        {
            ExpJointDependants = 35562.0;
        }
        else if(numberOfDependants == 3)
        {
            ExpJointDependants = 41379.0;
        }
        else if (numberOfDependants == 4)
        {
            ExpJointDependants = 47130.0;
        }
        
        if (indexofGrossIncome1 == 1)
        {
            gross1 = gross1;
        }
        else if (indexofGrossIncome1 == 2)
        {
            gross1 = gross1*12;
        }
        else if (indexofGrossIncome1 == 3)
        {
            gross1 = gross1*26;
        }
        else if (indexofGrossIncome1 == 4)
        {
            gross1 = gross1*52;
        }
        
        
        
        if (indexofGrossIncome2 == 1)
        {
            gross2 = gross2;
        }
        else if (indexofGrossIncome2 == 2)
        {
            gross2 = gross2*12;
        }
        else if (indexofGrossIncome2 == 3)
        {
            gross2 = gross2*26;
        }
        else if (indexofGrossIncome2 == 4)
        {
            gross2 = gross2*52;
        }
        
        
        
        
        
        if (indexofUntaxedIncome ==1)
        {
            untaxedIncomde = untaxedIncomde;
        }
        else if (indexofUntaxedIncome ==2)
        {
            untaxedIncomde = untaxedIncomde*12;
        }
        else if (indexofUntaxedIncome ==3)
        {
            untaxedIncomde = untaxedIncomde*26;
        }
        else if (indexofUntaxedIncome ==4)
        {
            untaxedIncomde = untaxedIncomde*52;
        }
        
        
        
        if (indexofRentalIncome == 1)
        {
            rentalIncome = rentalIncome;
        }
        else if (indexofRentalIncome == 2)
        {
            rentalIncome = rentalIncome*12;
        }
        else if (indexofRentalIncome == 3)
        {
            rentalIncome = rentalIncome*26;
        }
        else if (indexofRentalIncome == 4)
        {
            rentalIncome = rentalIncome*52;
        }
        
        totalIncome = gross1 + gross2 + untaxedIncomde + rentalIncome;
    }
    
    if (indexofOtherLoanRepayment == 1)
    {
        otherLoan = otherLoan * 1;
    }
    else if (indexofOtherLoanRepayment == 2)
    {
        otherLoan = otherLoan * 12;
    }
    if (indexofOtherLoanRepayment == 3)
    {
        otherLoan = otherLoan * 52;
    }
    totalExp = otherLoan + ([strCarLoanPerMonth doubleValue]*12) +creditCardInterest + ExpJointDependants;
    
    totalAnnualAllowns = totalIncome - totalExp;
    totalMonthlyAllowns = totalAnnualAllowns/12;
    youCanBorrow = totalMonthlyAllowns * finalInterest;
    
    if (youCanBorrow<1)
    {
        youCanBorrow = 0.00;
    }
    
    NSString *strAns = [NSString stringWithFormat:@"%.f",youCanBorrow];
    
    NSInteger length = strAns.length;
    if (length >4)
    {
        NSInteger start = length - 3;
        strAns = [strAns stringByReplacingCharactersInRange:NSMakeRange(start, 3) withString:@"000"];
        strAns = [NSString stringWithFormat:@"$%@.00",strAns];
    }
    else
    {
        strAns = [NSString stringWithFormat:@"$%.2f",youCanBorrow];
    }
    [print printDescription:strAns message:@"You Can Borrow"];
    
    strYoumayBorrow = strAns;
    [strYoumayBorrow retain];
    
    double r = [strInterestRate doubleValue];
    r = r/1200;
    
    
    strAns = [strAns stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    double x = 1+r;
    double b = pow(x, month) -1;
    double ans1 = (r +(r/b)) *[strAns doubleValue];
    if (ans1 < 0 || isnan(ans1)) {
        ans1 = 0.00;
    }
    strMonthlyRepayment = [NSString stringWithFormat:@"$%.2f",ans1];
    [strMonthlyRepayment retain];
    ans1 = ans1 *month;
    if (ans1 < 0  || isnan(ans1)) {
        ans1 = 0.00;
    }
    strTotalInterestPayable = [NSString stringWithFormat:@"$%.2f",ans1];
    nslog(@"strTotalInterestPayable == %@",strTotalInterestPayable);
    
    [strTotalInterestPayable retain];
    
    
    
    
    
    totalIncome = 0.0;
    totalExp = 0.0;
    totalAnnualAllowns = 0.0;
    totalMonthlyAllowns = 0.0;
    youCanBorrow = 0.0;
    [tableView reloadData];
    
}
#pragma mark Slider changed
-(void)sliderValueChanged:(id)sender
{
    
    UISlider *slider = (UISlider*) sender;
    [slider setThumbImage:[UIImage imageNamed:@"slider_top.png"] forState:UIControlStateHighlighted];
    
    UITextField *textField = (UITextField*)[self.view viewWithTag:tg];
    [textField resignFirstResponder];
    [toolbar setHidden:YES];
    
    
    if ([sender tag] == 777)
    {
        
        UITextField *txtdependants = (UITextField*)[self.view viewWithTag:203];
        txtdependants.text = [NSString stringWithFormat:@"%.f", roundf(slider.value)];
        strNumberOfDepartment = txtdependants.text;
    }
    else if ([sender tag] == 888)
    {
        double value = roundf(slider.value);
        value = value/2;
        UITextField *txtTerm = (UITextField*)[self.view viewWithTag:300];
        txtTerm.text = [NSString stringWithFormat:@"%.1f", value];
        strTermOfLoan = txtTerm.text;
        
    }
    else if ([sender tag] == 999)
    {
        UITextField *txtRate= (UITextField*)[self.view viewWithTag:301];
        txtRate.text = [NSString stringWithFormat:@"%.1f", (slider.value)];
        strInterestRate = txtRate.text;
    }
    double rate = [strInterestRate doubleValue] + 1.5;
    rate = rate/1200;
    int month = [strTermOfLoan doubleValue] * 12;
    
    double R_plus_1 = rate + 1;
    double R_plus_1_res_month = pow(R_plus_1, month);
    double R_plus_1_res_month_min_1 = R_plus_1_res_month - 1;
    double R_into_R_plus_1_res_month = rate * R_plus_1_res_month;
    double finalInterest = R_plus_1_res_month_min_1/R_into_R_plus_1_res_month;
    
    [print printDescription:[NSString stringWithFormat:@"%.2f",finalInterest] message:@"FinalInterest"];
    int numberOfDependants = [strNumberOfDepartment intValue];
    
    
    NSString *strGross1 = strGrossINcome1;
    
    strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"," withString:@""];
    strGross1 = [strGross1 stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strGross2 = strGrossINcome2;
    
    strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"," withString:@""];
    strGross2 = [strGross2 stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strUntaxedIncome = strUntaxedAlloeableIncome;
    
    strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"," withString:@""];
    strUntaxedIncome = [strUntaxedIncome stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    NSString *strRental = strRentalIncome;
    
    strRental = [strRental stringByReplacingOccurrencesOfString:@"," withString:@""];
    strRental = [strRental stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    NSString *strOtherLoan = strOtherLoanRepayment;
    
    strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"," withString:@""];
    strOtherLoan = [strOtherLoan stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strCarLoanPerMonth = strCarLoanRepaymentPerMonth;
    
    strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"," withString:@""];
    strCarLoanPerMonth = [strCarLoanPerMonth stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    NSString *strCreditCard = strTotalCreditcardLimit;
    
    strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"," withString:@""];
    strCreditCard = [strCreditCard stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    double creditCardInterest = [strCreditCard doubleValue] * 0.03 * 12;
    //    [print printDescription:[NSString stringWithFormat:@"%.2f",creditCardInterest] message:@"You Can Borrow"];
    
    double gross1,gross2,untaxedIncomde,rentalIncome,otherLoan;
    gross1 = [strGross1 doubleValue];
    gross2 = [strGross2 doubleValue];
    untaxedIncomde = [strUntaxedIncome doubleValue];
    rentalIncome = [strRental doubleValue];
    otherLoan = [strOtherLoan doubleValue];
    double ExpJointDependants = 0.0;
    if (indexofApplicationType == 1)
    {
        
        if (numberOfDependants == 0)
        {
            ExpJointDependants = 16460.0;
        }
        else if(numberOfDependants == 1)
        {
            ExpJointDependants = 22607.0;
        }
        else if (numberOfDependants == 2)
        {
            ExpJointDependants = 28423.0;
        }
        else if(numberOfDependants == 3)
        {
            ExpJointDependants = 34239.0;
        }
        else if (numberOfDependants == 4)
        {
            ExpJointDependants = 40056.0;
        }
        
        if (indexofGrossIncome1 == 1)
        {
            gross1 = gross1;
        }
        else if (indexofGrossIncome1 == 2)
        {
            gross1 = gross1*12;
        }
        else if (indexofGrossIncome1 == 3)
        {
            gross1 = gross1*26;
        }
        else if (indexofGrossIncome1 == 4)
        {
            gross1 = gross1*52;
        }
        
        
        
        if (indexofGrossIncome2 == 1)
        {
            gross2 = gross2;
        }
        else if (indexofGrossIncome2 == 2)
        {
            gross2 = gross2*12;
        }
        else if (indexofGrossIncome2 == 3)
        {
            gross2 = gross2*26;
        }
        else if (indexofGrossIncome2 == 4)
        {
            gross2 = gross2*52;
        }
        
        
        
        
        
        if (indexofUntaxedIncome ==1)
        {
            untaxedIncomde = untaxedIncomde;
        }
        else if (indexofUntaxedIncome ==2)
        {
            untaxedIncomde = untaxedIncomde*12;
        }
        else if (indexofUntaxedIncome ==3)
        {
            untaxedIncomde = untaxedIncomde*26;
        }
        else if (indexofUntaxedIncome ==4)
        {
            untaxedIncomde = untaxedIncomde*52;
        }
        
        
        
        if (indexofRentalIncome == 1)
        {
            rentalIncome = rentalIncome;
        }
        else if (indexofRentalIncome == 2)
        {
            rentalIncome = rentalIncome*12;
        }
        else if (indexofRentalIncome == 3)
        {
            rentalIncome = rentalIncome*26;
        }
        else if (indexofRentalIncome == 4)
        {
            rentalIncome = rentalIncome*52;
        }
        
        totalIncome = gross1 + untaxedIncomde + rentalIncome;
        
    }
    else if (indexofApplicationType == 2)
    {
        
        if (numberOfDependants == 0)
        {
            ExpJointDependants = 23930.0;
        }
        else if(numberOfDependants == 1)
        {
            ExpJointDependants = 29746.0;
        }
        else if (numberOfDependants == 2)
        {
            ExpJointDependants = 35562.0;
        }
        else if(numberOfDependants == 3)
        {
            ExpJointDependants = 41379.0;
        }
        else if (numberOfDependants == 4)
        {
            ExpJointDependants = 47130.0;
        }
        
        if (indexofGrossIncome1 == 1)
        {
            gross1 = gross1;
        }
        else if (indexofGrossIncome1 == 2)
        {
            gross1 = gross1*12;
        }
        else if (indexofGrossIncome1 == 3)
        {
            gross1 = gross1*26;
        }
        else if (indexofGrossIncome1 == 4)
        {
            gross1 = gross1*52;
        }
        
        
        
        if (indexofGrossIncome2 == 1)
        {
            gross2 = gross2;
        }
        else if (indexofGrossIncome2 == 2)
        {
            gross2 = gross2*12;
        }
        else if (indexofGrossIncome2 == 3)
        {
            gross2 = gross2*26;
        }
        else if (indexofGrossIncome2 == 4)
        {
            gross2 = gross2*52;
        }
        
        
        
        
        
        if (indexofUntaxedIncome ==1)
        {
            untaxedIncomde = untaxedIncomde;
        }
        else if (indexofUntaxedIncome ==2)
        {
            untaxedIncomde = untaxedIncomde*12;
        }
        else if (indexofUntaxedIncome ==3)
        {
            untaxedIncomde = untaxedIncomde*26;
        }
        else if (indexofUntaxedIncome ==4)
        {
            untaxedIncomde = untaxedIncomde*52;
        }
        
        
        
        if (indexofRentalIncome == 1)
        {
            rentalIncome = rentalIncome;
        }
        else if (indexofRentalIncome == 2)
        {
            rentalIncome = rentalIncome*12;
        }
        else if (indexofRentalIncome == 3)
        {
            rentalIncome = rentalIncome*26;
        }
        else if (indexofRentalIncome == 4)
        {
            rentalIncome = rentalIncome*52;
        }
        
        totalIncome = gross1 + gross2 + untaxedIncomde + rentalIncome;
    }
    
    if (indexofOtherLoanRepayment == 1)
    {
        otherLoan = otherLoan * 1;
    }
    else if (indexofOtherLoanRepayment == 2)
    {
        otherLoan = otherLoan * 12;
    }
    if (indexofOtherLoanRepayment == 3)
    {
        otherLoan = otherLoan * 52;
    }
    totalExp = otherLoan + ([strCarLoanPerMonth doubleValue]*12) +creditCardInterest + ExpJointDependants;
    
    totalAnnualAllowns = totalIncome - totalExp;
    totalMonthlyAllowns = totalAnnualAllowns/12;
    youCanBorrow = totalMonthlyAllowns * finalInterest;
    
    if (youCanBorrow<1)
    {
        youCanBorrow = 0.00;
    }
    
    NSString *strAns = [NSString stringWithFormat:@"%.f",youCanBorrow];
    
    NSInteger length = strAns.length;
    if (length >4)
    {
        NSInteger start = length - 3;
        strAns = [strAns stringByReplacingCharactersInRange:NSMakeRange(start, 3) withString:@"000"];
        strAns = [NSString stringWithFormat:@"$%@.00",strAns];
    }
    else
    {
        strAns = [NSString stringWithFormat:@"$%.2f",youCanBorrow];
    }
    [print printDescription:strAns message:@"You Can Borrow"];
    
    strYoumayBorrow = strAns;
    [strYoumayBorrow retain];
    
    double r = [strInterestRate doubleValue];
    r = r/1200;
    
    
    strAns = [strAns stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    double x = 1+r;
    double b = pow(x, month) -1;
    double ans1 = (r +(r/b)) *[strAns doubleValue];
    if (ans1 < 0 || isnan(ans1)) {
        ans1 = 0.00;
    }
    strMonthlyRepayment = [NSString stringWithFormat:@"$%.2f",ans1];
    [strMonthlyRepayment retain];
    ans1 = ans1 *month;
    if (ans1 < 0  || isnan(ans1)) {
        ans1 = 0.00;
    }
    strTotalInterestPayable = [NSString stringWithFormat:@"$%.2f",ans1];
    nslog(@"strTotalInterestPayable == %@",strTotalInterestPayable);
    
    [strTotalInterestPayable retain];
    
    
    UITextField *txtYoumayBorrow = (UITextField*) [self.view viewWithTag:400];
    UITextField *txtMonthlyRepayment = (UITextField*) [self.view viewWithTag:401];
    UITextField *txtTotalInterest = (UITextField*) [self.view viewWithTag:402];
    
    
    
    txtYoumayBorrow.text = strYoumayBorrow;
    txtMonthlyRepayment.text = strMonthlyRepayment;
    txtTotalInterest.text = strTotalInterestPayable;
    
    
    totalIncome = 0.0;
    totalExp = 0.0;
    totalAnnualAllowns = 0.0;
    totalMonthlyAllowns = 0.0;
    youCanBorrow = 0.0;
    
    
}

#pragma mark - back_clicked
#pragma mark

-(void)back_clicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
