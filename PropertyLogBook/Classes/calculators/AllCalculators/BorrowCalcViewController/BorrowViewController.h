//
//  BorrowViewController.h
//  Loansactually
//
//  
//  
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@class CustomToolbar;
@interface BorrowViewController : UIViewController<UITextFieldDelegate,MFMailComposeViewControllerDelegate>
{
   
    IBOutlet UILabel *lblTitle1;
    IBOutlet UIButton *btnBack;
    IBOutlet UIButton *btnEmail;
    IBOutlet UIButton *btnCalculator;
    IBOutlet UIButton *btnInfo;
    IBOutlet UITableView *tableView;
    IBOutlet CustomToolbar *toolbar;
    NSInteger indexofApplicationType;
    NSInteger indexofGrossIncome1;
    NSInteger indexofGrossIncome2;
    NSInteger indexofUntaxedIncome;
    NSInteger indexofRentalIncome;
    NSInteger indexofOtherLoanRepayment;
    
    NSString *strGrossINcome1;
    NSString *strGrossINcome2;
    NSString *strUntaxedAlloeableIncome;
    NSString *strRentalIncome;
    NSString *strOtherLoanRepayment;
    NSString *strCarLoanRepaymentPerMonth;
    NSString *strTotalCreditcardLimit;
    NSString *strNumberOfDepartment;
    NSString *strTermOfLoan;
    NSString *strInterestRate;
    NSString *strYoumayBorrow;
    NSString *strMonthlyRepayment;
    NSString *strTotalInterestPayable;
    
    
    
    NSString *lblApplicationType;
    NSString *lblGrossINcome1;
    NSString *lblGrossINcome2;
    NSString *lblUntaxedAlloeableIncome;
    NSString *lblRentalIncome;
    NSString *lblOtherLoanRepayment;
    NSString *lblCarLoanRepaymentPerMonth;
    NSString *lblTotalCreditcardLimit;
    NSString *lblNumberOfDepartment;
    NSString *lblTermOfLoan;
    NSString *lblInterestRate;
    NSString *lblYouMayBorrow;
    NSString *lblMonthlyRepayment;
    NSString *lblTotalInterestPayable;
 
    double totalIncome;
    double totalExp;
    double totalAnnualAllowns;
    double totalMonthlyAllowns;
    double youCanBorrow;
    
    
    IBOutlet UIView *viewforfooter;
    
        int current_tag;
}
-(IBAction)btnBack_Clicked:(id)sender;
-(IBAction)btnEmail_Clicked:(id)sender;
-(IBAction)btnCalculator_Clicked:(id)sender;
-(IBAction)btnInfo_Clicekd:(id)sender;
-(IBAction)btnDoneClicked:(id)sender;
@end
