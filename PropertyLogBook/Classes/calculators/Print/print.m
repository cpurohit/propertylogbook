//
//  print.m
//  Travel Log Book
//
//  
//  
//

#import "print.h"

@implementation print

+(void)printDescription:(id)argument message:(id)message
{
    nslog(@"\n %@ = %@",argument,message);
}

@end
