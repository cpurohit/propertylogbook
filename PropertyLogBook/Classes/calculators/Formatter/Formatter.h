//
//  Formatters.h
//  NegotiatorApp
//
//  
//  
//

#import <Foundation/Foundation.h>

@interface Formatters : NSObject


//data formats
+(NSNumberFormatter*) currencyFormatter;
+(NSNumberFormatter*) currencyFormatterWithNoFraction;
+(NSNumberFormatter*) percentFormatter;
+(NSNumberFormatter*) basicFormatter;

@end
