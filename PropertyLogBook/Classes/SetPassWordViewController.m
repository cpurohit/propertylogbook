//
//  SetPassWordViewController.m
//  MyReferences
//
//  Created by Neel  Shah on 09/12/11.
//  Copyright (c) 2011 Sunshine Infotech. All rights reserved.
//

#import "SetPassWordViewController.h"
#import "PropertyLogBookAppDelegate.h"
#import "CustomeCell_PassWord.h"

@implementation SetPassWordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)BackTopush
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - View lifecycle

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate=(PropertyLogBookAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    
    [tbl_Setpass setBackgroundView:nil];
    [tbl_Setpass setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
    
    self.navigationItem.title=@"Set Password";
    [txt_PassWord becomeFirstResponder];
    
    UIButton *btnBack=[UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame=CGRectMake(0, 0,49.0,29.0);
    btnBack.backgroundColor=[UIColor clearColor];
    [btnBack setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(BackTopush) forControlEvents:UIControlEventTouchUpInside];
    
    
    //Create done button
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0, 60, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(DoneToolBarKey) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    self.navigationItem.rightBarButtonItem=[[[UIBarButtonItem alloc] initWithCustomView:btnDone1] autorelease];
    
    
    UIButton *btnDone2=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone2.frame=CGRectMake(0, 0, 60, 30);
    btnDone2.backgroundColor=[UIColor clearColor];
    [btnDone2 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone2 addTarget:self action:@selector(DoneButtonClick:) forControlEvents:UIControlEventTouchUpInside];

    
    
    UIBarButtonItem *flexiItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    UIBarButtonItem *doneButton = [[[UIBarButtonItem alloc] initWithCustomView:btnDone2] autorelease];
    
    
    [toolBarKeyboard setItems:[NSArray arrayWithObjects:flexiItem,doneButton, nil]];
    //    toolBar.items = array;
    
    
    
    [flexiItem release];
    
    
    toolBarKeyboard.barStyle = UIBarStyleBlack;
    
    
    
    /*
    tbl_Setpass.separatorColor=[UIColor whiteColor];
    */
     tbl_Setpass.backgroundColor=[UIColor clearColor];
    
    
    toolBarKeyboard.hidden=YES;
    // Do any additional setup after loading the view from its nib.
}


-(void)SetPassCodeStatus:(int)TagNumberValue
{
    prefs=[NSUserDefaults standardUserDefaults];
	[prefs setInteger:TagNumberValue forKey:@"PasscodeOn"];
	[prefs synchronize];
}

-(void)SetPassWord:(NSString *)PassWord
{
    prefs=[NSUserDefaults standardUserDefaults];
    [prefs setValue:PassWord forKey:@"PassWordValue"];
    [prefs synchronize];
}

-(void)SetPassWordHint:(NSString *)PassWordHint
{
    prefs=[NSUserDefaults standardUserDefaults];
    [prefs setValue:PassWordHint forKey:@"PassWordValueHint"];
    [prefs synchronize];
}


#pragma mark - SavePassWord

-(void)DoneToolBarKey
{
    UITextField *txt=(UITextField *)[self.view viewWithTag:5000];
    UITextField *txt1=(UITextField *)[self.view viewWithTag:5001];
    UITextField *txt2=(UITextField *)[self.view viewWithTag:5002];
    
    NSString *emailString = txt2.text; // storing the entered email in a string.
	// Regular expression to checl the email format.
	NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    
    [self SetPassCodeStatus:0];
    
    if (txt.text.length==0) {
        UIAlertView *Alert=[[UIAlertView alloc] initWithTitle:@"Password" message:@"Enter password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [Alert show];
        [Alert release];
    }
    else if(txt1.text.length>0)
    {
        if ([txt.text isEqualToString:txt1.text]) {
            
            if (txt2.text.length==0) {
                UIAlertView *Alert=[[UIAlertView alloc] initWithTitle:@"Property Log Book" message:@"Enter your Email Id." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [Alert show];
                [Alert release];
            }
            else
            {
                if (([emailTest evaluateWithObject:emailString] != YES) || ([emailString isEqualToString:@""]))
                {
                    NSString *str =[NSString stringWithFormat:@"Please enter email address in you@example.com format."];
                    UIAlertView *Alert=[[UIAlertView alloc] initWithTitle:@"Invalid email format" message:str delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [Alert show];
                    [Alert release];
                }
                else
                {
                    [self SetPassWordHint:txt2.text];
                    [self SetPassWord:txt1.text];
                    [self SetPassCodeStatus:1];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }
        else
        {
            UIAlertView *Alert=[[UIAlertView alloc] initWithTitle:@"Wrong Password" message:@"Password entered do not match" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [Alert show];
            [Alert release];
        }
        
    }
    else if(txt1.text.length==0)
    {
        UIAlertView *Alert=[[UIAlertView alloc] initWithTitle:@"Password" message:@"Enter confirm password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [Alert show];
        [Alert release];
    }
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    UITextField *txt=(UITextField *)[self.view viewWithTag:5002];
    if (txt==textField) 
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Property Log Book" message:@"Please choose your password Email Id carefully as there is no option of recovering forgotten password as password is stored locally on your phone." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    TagForNext=textField.tag;
    toolBarKeyboard.hidden=NO;
    return YES;
}

#pragma mark - ToolBarDone

-(IBAction)DoneButtonClick:(id)sender
{
    NSInteger nextTag = TagForNext + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    }
    else
    {
        UITextView *txt_View=(UITextView *)[self.view viewWithTag:5001];
        [txt_View resignFirstResponder];
        UITextField *txt_Field=(UITextField *)[self.view viewWithTag:5002];
        [txt_Field resignFirstResponder];
        UITextField *txt_Field1=(UITextField *)[self.view viewWithTag:5003];
        [txt_Field1 resignFirstResponder];
        toolBarKeyboard.hidden=YES;
    }
}

#pragma mark - TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    CustomeCell_PassWord *cell;// = (CustomeCell_PassWord *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (1)
    {
        cell = [[[CustomeCell_PassWord alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    cell.textLabel.textColor=[UIColor blackColor];
    cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:17.0];
    //cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.txt.delegate=self;
    
    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
    {
        cell.txt.frame=CGRectMake(400, 12,250, 20);
    }
    else
    {
        cell.txt.frame=CGRectMake(600, 12,250, 20);
    }

    
    
    if (indexPath.row==0)
    {
        cell.lbl.text=@"Password";
        cell.txt.tag=5000+indexPath.row;
        cell.txt.placeholder=@"Password";
        cell.txt.secureTextEntry=YES;
    }
    else if(indexPath.row==1)
    {
        cell.lbl.text=@"Confirm Password";
        cell.txt.tag=5000+indexPath.row;
        cell.txt.placeholder=@"Confirm Password";
        cell.txt.secureTextEntry=YES;
    }
    else if(indexPath.row==2)
    {
        cell.lbl.text=@"Email Id";
        cell.txt.tag=5000+indexPath.row;
        cell.txt.placeholder=@"you@example.com";
    }

    // Configure the cell...
    
    return cell;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations

    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [tbl_Setpass reloadData];
}

@end
