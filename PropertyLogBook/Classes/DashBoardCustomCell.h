//
//  DashBoardCustomCell.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 9/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

@interface DashBoardCustomCell : UITableViewCell {
    UILabel *headingLabel1, *headingLabel2, *headingLabel3;
    UILabel *label;
    UILabel *lineLabel1, *lineLabel2;
    UILabel *percentageLabel;
    UILabel *amountLabel;
    PropertyLogBookAppDelegate *appDelegate;

    
}

@property (nonatomic, retain)UILabel *headingLabel1, *label,*headingLabel2, *headingLabel3;
@property (nonatomic, retain)UILabel *lineLabel1, *lineLabel2;
@property (nonatomic, retain)UILabel *percentageLabel, *amountLabel;


@end
