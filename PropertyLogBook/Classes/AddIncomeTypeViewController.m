//
//  AddIncomeTypeViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddIncomeTypeViewController.h"
#import "PropertyLogBookAppDelegate.h"


@implementation AddIncomeTypeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    nslog(@"\n initWithNibName");
    if (self = [super initWithNibName:@"AddIncomeTypeViewController" bundle:nibBundleOrNil]) 
    {
        // Custom initialization
    }
    return self;
    
}


/*
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    
    if (self) 
    {
        // Custom initialization
    }
    return self;
}
 */

- (void)dealloc
{
    //[tempRowArray release];
    [tblView release];
    [keytoolBar release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning  in AddIncomeTypeViewController \n");
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    textFieldIndex = -1;
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
        [tblView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
    }


    if(!appDelegate.isIPad)
    {
        
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        
    }


    
    
    self.navigationItem.title = @"INCOME TYPE";
  //  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel_clicked)];

    
    
    [appDelegate selectIncomeType];
    
    tblView.editing = TRUE;

    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }

    
    
    if(appDelegate.isIphone5)
    {
        keytoolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,244, self.view.frame.size.width, 44)];
    }
    else
    {
        keytoolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,156, self.view.frame.size.width, 44)];
    }
    
    
        

    
    
    
    if(appDelegate.isIPad)
    {
        
        
        
        
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                keytoolBar.frame = CGRectMake(0, 671,768, 44);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                keytoolBar.frame = CGRectMake(0,326,1024, 44);
            }

       
         
       
    }
   
    
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(keytoolBar_cancel_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flexiItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(keytoolBar_done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(keytoolBar_done_clicked)];
    */
    
    
    [keytoolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexiItem,doneButton, nil]];
    //    toolBar.items = array;
    keytoolBar.barStyle = UIBarStyleBlack;
    [self.view addSubview:keytoolBar];
    keytoolBar.hidden = TRUE;
    
    [cancelBtn release];
    [flexiItem release];
    [doneButton release];  


    nslog(@"incomeexpense view value===%d",appDelegate.isFromIncomeExpenseView);
    
    
    /*
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_clicked)] autorelease];
    */
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
    }
    else
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
        
    }

    
    if (appDelegate.isFromIncomeExpenseView >0)
    {
        tblView.editing = TRUE;
    }
    else 
    {
        tblView.editing = FALSE;
    }
	tblView.backgroundColor = [UIColor clearColor];
	
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
	
   //  self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    
    
    

    
    
}


-(void)keytoolBar_cancel_clicked
{
    keytoolBar.hidden = TRUE;
    UITextField *text = (UITextField *)[self.view viewWithTag:isClicked];
    nslog(@"\n textFieldTempText = %@",textFieldTempText);
    text.text =  textFieldTempText;
    [text resignFirstResponder];
    [self textFieldShouldReturn:text];
    
}
-(void)keytoolBar_done_clicked
{
    //keytoolBar.hidden = TRUE;
    UITextField *text = (UITextField *)[self.view viewWithTag:isClicked];
    textFieldTempText = text.text;
    [text resignFirstResponder];
    
    [self textFieldShouldReturn:text];
}

-(void)cancel_clicked
{
    
    
    if(appDelegate.isFromIncomeExpenseView != -1)
    {
        if (textFieldIndex >0)
        {
            UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldIndex];
            [textField resignFirstResponder];
        }
        nslog(@"\n incomeType = %@",appDelegate.incomeTypeDBArray);
        for (int i=0;i<[appDelegate.incomeTypeDBArray count];i++)
        {
            
            [appDelegate updateIncomeType:[[appDelegate.incomeTypeDBArray objectAtIndex:i]valueForKey:@"incomeType"] rowid:[[[appDelegate.incomeTypeDBArray objectAtIndex:i]valueForKey:@"rowid"]intValue] ];
        }
        [appDelegate selectIncomeType];

    }
                     
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)add_clicked
{
    nslog(@"\n textFieldIndex = %d",textFieldIndex);
    if(textFieldIndex != 5000 && textFieldIndex>0)
    {
        UITextField*textField = (UITextField*)[self.view viewWithTag:textFieldIndex];
        int index = textField.tag%1000;
        nslog(@"\n text while ad = %@",textField.text);
        [[appDelegate.incomeTypeDBArray objectAtIndex:index] setValue:textField.text forKey:@"incomeType"];
        
        for (int i=0;i<[appDelegate.incomeTypeDBArray count];i++)
        {
            
            [appDelegate updateIncomeType:[[appDelegate.incomeTypeDBArray objectAtIndex:i]valueForKey:@"incomeType"] rowid:[[[appDelegate.incomeTypeDBArray objectAtIndex:i]valueForKey:@"rowid"]intValue] ];
            
        }
        [appDelegate selectIncomeType];

    }
    isAddClicked = YES;
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Enter Income Type" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	
    myAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    incomeText = [myAlertView textFieldAtIndex:0];
    
    //incomeText= [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    
    [incomeText setKeyboardAppearance:UIKeyboardAppearanceDefault];
	
    incomeText.delegate = self;
    incomeText.tag = 5000;
    [incomeText becomeFirstResponder];
	[incomeText setBackgroundColor:[UIColor clearColor]];
	[myAlertView addSubview:incomeText];
	[myAlertView show];
	[myAlertView release];
}

- (void) alertView:(UIAlertView *) actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex 
{
    
    if(!appDelegate.isIPad)
    {

        
        //tableview height in textfieldend editing etc…
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
        
        
        
    }

    
    keytoolBar.hidden = TRUE;
    isAddClicked = NO;
	if (buttonIndex == 1)
	{        
        if ([incomeText.text length]>0)
        {
            [appDelegate AddIncomeType:incomeText.text];
            NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
            [tdict setObject:incomeText.text forKey:@"incomeType"];


            [appDelegate.incomeTypeDBArray addObject:tdict];

            [appDelegate selectIncomeType];

            [lblNoData setHidden:TRUE];
            [lblPlus setHidden:TRUE];
            imageView.hidden = TRUE;
            [tblView setHidden:FALSE];

            [tdict release];
            [tblView reloadData];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid income type" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            [alertView release];
        }
	}
	else 
    {
        if ([appDelegate.incomeTypeDBArray count]==0)
        {
            [lblNoData setHidden:FALSE];
            [lblPlus setHidden:FALSE];
            [tblView setHidden:TRUE];
            
            
            
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
                    lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                    lblPlus.center = CGPointMake(343,456);
                }
                else
                {
                    imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
                    lblNoData.center = CGPointMake(494,655.0f/2.0f);
                    lblPlus.center = CGPointMake(454,329); 
                }

            }

            
           
            UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
            [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            [self.view addSubview:imgView];
            [self.view sendSubviewToBack:imgView];

            [imgView release];


        }
        
        
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
        
        
        [tblView reloadData];
	}

}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    
    //[self.view addSubview:appDelegate.bottomView];
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    
    
    
    isAddClicked = NO;
	if([appDelegate.incomeTypeDBArray count]==0)
	{
		lblNoData.hidden = FALSE;
		lblPlus.hidden = FALSE;
		imageView.hidden = FALSE;
		
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                lblPlus.center = CGPointMake(343,456);
            }
            else
            {
                imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(494,655.0f/2.0f);   
                lblPlus.center = CGPointMake(454,329); 
            }
            
        }

        
        [tblView setHidden:TRUE];
        
        
        
        
		//[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0,1024,1024)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];

		
	}
	else 
    {
        imageView.hidden = TRUE;
		[tblView setHidden:FALSE];
		lblNoData.hidden = TRUE;
		lblPlus.hidden = TRUE;
		[tblView reloadData];
		
	}
    
    
    
    if(appDelegate.isIPad)
    {

    
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
    
   
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    if(appDelegate.isIPad)
    {
        return TRUE;
    }
    return false;
}


-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
    if([appDelegate.incomeTypeDBArray count]==0)
	{
		lblNoData.hidden = FALSE;
        lblPlus.hidden = FALSE;
        imageView.hidden = FALSE;
		
        if(appDelegate.isIPad)
        {  
            if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
            {
                imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                lblPlus.center = CGPointMake(343,456); 
                
              
            }
            else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
            {
                imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(494,655.0f/2.0f);   
                 
                lblPlus.center = CGPointMake(454,329); 
                           
            }
            
        }
        
    }
    else 
    {
        if(appDelegate.isIPad)
        {
            
            
            
                if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
                {
                    keytoolBar.frame = CGRectMake(0, 671,768, 44);
                }
                else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
                {
                    keytoolBar.frame = CGRectMake(0,326,1024, 44);
                    
                }

            
            
            
        }

    }
    
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    /*
    if(appDelegate.isIPad)
    {
        
        if(textFieldIndex != 5000)
        {
            keytoolBar.hidden = TRUE;
            [self.view endEditing:YES];
        }
        
    }
    
    [tblView reloadData];    
    */
    
    
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
    }

    
    
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

#pragma mark - Text Field Delegate Method

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    

    keytoolBar.hidden = FALSE;
    textFieldIndex = textField.tag;
    
    isClicked = textField.tag;
    textFieldTempText = textField.text;
    [textFieldTempText retain];

    /*
    NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:textField.tag%1000 inSection:0];
	[tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
    */ 
    
    if (textField.tag > 1002)
    {
        /*
        tblView.frame = CGRectMake(0, -((textField.tag%1000)*25), tblView.frame.size.width, tblView.frame.size.height);
         */
    }

    

    
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    keytoolBar.hidden = FALSE;
    
    if(!appDelegate.isIPad)
    {
        nslog(@" origint = %f",textField.frame.origin.y);
        
        
        if(appDelegate.isIphone5)
        {
            tblView.frame = CGRectMake(0,tblView.frame.origin.y ,tblView.frame.size.width,240.0f);
            
        }
        else
        {
            tblView.frame = CGRectMake(0,tblView.frame.origin.y , tblView.frame.size.width,155.0f);
            
        }
        
        
        [tblView setContentOffset:CGPointMake(0,textField.frame.origin.y+(44*(isClicked%1000)-44)) animated:YES];
        
        
        //[tblView scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];
    }

    if(!isAddClicked)
    {
        [self scrollViewToTextField:textField];
    }

    
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    //nslog(@"comes in should end");
    
    if(appDelegate.isIPad)
    {
        if(isAddClicked)
        {
            return NO;
        }
    }

    
    int index = textField.tag%1000;
    
    
    nslog(@"reminder = %d",index);
    /*
        Sun:0004
        This was causing crashing...
        nslog(@"\n %@",[appDelegate.incomeTypeDBArray objectAtIndex:index]);
     */
    
    
    
   
    nslog(@"textField = %@",textField.text);
 
    if(textField.tag != 5000)//alert view tag..!
    {
         [[appDelegate.incomeTypeDBArray objectAtIndex:index] setValue:textField.text forKey:@"incomeType"];
        for(int i=0;i<[appDelegate.incomeTypeDBArray count];i++)
        {
            nslog(@"\n appDelegate.reminderTypeArray in loop = %@",[appDelegate.incomeTypeDBArray objectAtIndex:i]);
            
            [appDelegate updateIncomeType:[[appDelegate.incomeTypeDBArray objectAtIndex:i] valueForKey:@"incomeType"] rowid:[[[appDelegate.incomeTypeDBArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        }

    }
    
    if(appDelegate.isIPad)
    {
        keytoolBar.hidden = TRUE;
        [textField resignFirstResponder];
    }

    
    //[tblView reloadData];
    
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(!appDelegate.isIPad)
    {
        
        
        

    
    
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if( isAddClicked)
    {
        return NO;
    }
    
    if (textField.tag > 999)
    {
        
        if(!appDelegate.isIPad)
        {
           
            
            if(appDelegate.isIphone5)
            {
                //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
                
                
                if(appDelegate.isIOS7)
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
                }
                else
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
                }
                
                
                
            }
            else
            {
                
                if(appDelegate.isIOS7)
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
                }
                else
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
                }
                
                
            }
            
        
        
        }

        
        nslog(@"\n textField.tag = %d",textField.tag);
        nslog(@"\n textField.text = %@",textField.text);
        nslog(@"\n textField.tag modulo = %d",textField.tag%1000);
        nslog(@"\n textFieldIndex = %d",textFieldIndex%1000);
        
        
        nslog(@"\n appDelegate.incomeTypeDBArray = %@",appDelegate.incomeTypeDBArray);
        nslog(@"\n appDelegate.incomeTypeDBArray at index = %@",[appDelegate.incomeTypeDBArray objectAtIndex:(textField.tag%1000)]);
        nslog(@"\n text here = %@",textField.text);
        
        
        
        
        NSString *text = @"";        
        for(int i=0;i<[appDelegate.incomeTypeDBArray count];i++)
        {
            nslog(@"\n appDelegate.reminderTypeArray in loop = %@",[appDelegate.incomeTypeDBArray objectAtIndex:i]);
            if(textFieldIndex%1000 == i)
            {
                //text = textField.text;
                text = textFieldTempText;
            }
            else
            {
                text =  [[appDelegate.incomeTypeDBArray objectAtIndex:i]valueForKey:@"incomeType"];
            }
            nslog(@"\n text = %@",text);
            [appDelegate updateIncomeType:text rowid:[[[appDelegate.incomeTypeDBArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        }
        [appDelegate selectIncomeType];
        
        nslog(@"\n appDelegate.incomeTypeDBArray after update! = %@",appDelegate.incomeTypeDBArray);
        
        
        if (textFieldIndex == textField.tag)
        {
            textFieldIndex = -1;
        }
    }

    
    keytoolBar.hidden = TRUE;
    
    
    if(appDelegate.isIphone5)
    {
        //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
        
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
        }
        
        
        
    }
    else
    {
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
        }
        
        
    }

    
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section 
{
     if (appDelegate.isFromIncomeExpenseView >0)
     {
        return @"Note: 1st entry is default entry";    
     }
    
    return @"";
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([appDelegate.incomeTypeDBArray count]>0)
    {
        return [appDelegate.incomeTypeDBArray count];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    static NSString *CellIdentifier = @"Cell";
    UITextField *incomeTextField;
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (1) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        if(appDelegate.isIOS7)
        {
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        }
        
        [cell.textLabel setFont:[UIFont systemFontOfSize:15.0]];
        incomeTextField = [[UITextField alloc]initWithFrame:CGRectMake(20, 12, 200, 31)];
        if(appDelegate.isIPad)
        {
            
           if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
           {
               incomeTextField.frame = CGRectMake(20, 9, 500, 31);
           }

            if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                incomeTextField.frame = CGRectMake(20, 9, 830, 31);
            }

           
        }
        else
        {
           incomeTextField.frame = CGRectMake(20, 12, 200, 31);
        }
        
        
        
        incomeTextField.delegate = self;
        incomeTextField.borderStyle = UITextBorderStyleNone;
        incomeTextField.tag = 1000+indexPath.row;
         incomeTextField.font =[UIFont systemFontOfSize:15.0];
        if (appDelegate.isFromIncomeExpenseView >0)
        {
            [incomeTextField setEnabled:TRUE];
        }
        else
        {
            [incomeTextField setEnabled:FALSE];    
        }
        [cell.contentView addSubview:incomeTextField];
        
    }
   
    incomeTextField.hidden = FALSE;
    //[cell setFont:[UIFont systemFontOfSize:17.0]];
    incomeTextField.font =[UIFont systemFontOfSize:15.0];
    // Configure the cell...
    [incomeTextField release]; 
    
    
    if ([appDelegate.incomeTypeDBArray count]>0)
    {
        cell.textLabel.hidden = TRUE;
        [incomeTextField setText:[[appDelegate.incomeTypeDBArray objectAtIndex:indexPath.row]valueForKey:@"incomeType"]];
        
        if (appDelegate.isFromIncomeExpenseView < 0 )
        {
            if ([incomeTextField.text isEqualToString:appDelegate.incomeString])
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                oldIndex = indexPath;
                [oldIndex retain];
            }
        }
    }

     
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i=0; i<[appDelegate.incomeTypeDBArray count];i++)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:i+1000];
        [textField resignFirstResponder];
    }
    
    for (int i=0;i<[appDelegate.incomeTypeDBArray count];i++)
    {
        
        [appDelegate updateIncomeType:[[appDelegate.incomeTypeDBArray objectAtIndex:i]valueForKey:@"incomeType"] rowid:[[[appDelegate.incomeTypeDBArray objectAtIndex:i]valueForKey:@"rowid"]intValue] ];
        
    }
    [appDelegate selectIncomeType];
    
        // Delete the row from the data source
    keytoolBar.hidden = TRUE;
    textFieldIndex  = -1;
	[appDelegate deleteIncomeType:[[[appDelegate.incomeTypeDBArray objectAtIndex:indexPath.row]valueForKey:@"rowid"]intValue]];

    [appDelegate.incomeTypeDBArray removeObjectAtIndex:indexPath.row];
    [appDelegate selectIncomeType];
	
    if([appDelegate.incomeTypeDBArray count]==0)
	{
        imageView.hidden = FALSE;
		lblNoData.hidden = FALSE;
		lblPlus.hidden = FALSE;
		
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                lblPlus.center = CGPointMake(343,456);
            }
            else
            {
                imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(494,655.0f/2.0f);
                lblPlus.center = CGPointMake(454,329); 
            }
            
        }
        
        
        [tblView setHidden:TRUE];
		//[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0, 1024,1024)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];

		
	}
	else 
    {
        imageView.hidden = TRUE;
		[tblView setHidden:FALSE];
		lblNoData.hidden = TRUE;
		lblPlus.hidden = TRUE;
        
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
        }
        
        
        [tblView reloadData];
		
	}
       
    
}



// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    if ([appDelegate.incomeTypeDBArray count]>0)
    {
        [self moveFromOriginal:fromIndexPath.row toNew:toIndexPath.row];
    }

}

-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew 
{
    
    if (textFieldIndex >0)
    {
        
        UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldIndex];
        [textField resignFirstResponder];
    }
    
    
    
    
    
    //tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
    
    
    //tableview height in textfieldend editing etc…
    if(appDelegate.isIphone5)
    {
        //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
        
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
        }
        
        
        
    }
    else
    {
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
        }
        
        
    }
    
    
    
    [appDelegate selectIncomeType];
     tempRowArray = [[NSMutableArray alloc] initWithArray:appDelegate.incomeTypeDBArray];
    NSMutableArray *rowIdArray = [[NSMutableArray alloc] initWithCapacity:10];
    nslog(@"\n before moving = %@",appDelegate.incomeTypeDBArray);
    for (int i=0;i<[appDelegate.incomeTypeDBArray count];i++)
    {
        [rowIdArray addObject:[[appDelegate.incomeTypeDBArray objectAtIndex:i] valueForKey:@"rowid"]];
    }
    nslog(@"\n rowIdArray  = %@",rowIdArray);

    
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:appDelegate.incomeTypeDBArray];
	id tempObject = [tempArray objectAtIndex:indexOriginal];
	[tempArray removeObjectAtIndex:indexOriginal];
	[tempArray insertObject:tempObject atIndex:indexNew];
	appDelegate.incomeTypeDBArray = tempArray;
    
    
    nslog(@"\n after moving = %@",appDelegate.incomeTypeDBArray);
	
    for (int i=0;i<[appDelegate.incomeTypeDBArray count];i++)
    {
        [[appDelegate.incomeTypeDBArray objectAtIndex:i] setObject:[rowIdArray objectAtIndex:i] forKey:@"rowid"];
    }
    
    nslog(@"\n after moving  and setting index..= %@",appDelegate.incomeTypeDBArray);

    
    
    for (int i=0;i<[appDelegate.incomeTypeDBArray count];i++)
    {
     
     [appDelegate updateIncomeType:[[appDelegate.incomeTypeDBArray objectAtIndex:i]valueForKey:@"incomeType"] rowid:[[[appDelegate.incomeTypeDBArray objectAtIndex:i]valueForKey:@"rowid"]intValue] ];
    }
    [appDelegate selectIncomeType];
  
     keytoolBar.hidden = TRUE;
    [rowIdArray release];
    [tempArray release];

   
    nslog(@"\n before loading...= %@",appDelegate.incomeTypeDBArray);
    
     [tblView reloadData];
    textFieldIndex = -1;    
    
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return YES;
    
    // Return NO if you do not want the item to be re-orderable.
    if ([appDelegate.incomeTypeDBArray count]>0)
    {
    return YES;
    }
    else
    {
        return NO;
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([appDelegate.incomeTypeDBArray count]>0)
    {
    return UITableViewCellEditingStyleDelete;
    }
    else
        return UITableViewCellEditingStyleNone;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tblView.editing)
    {
        UITextField *txtField = (UITextField *)[self.view viewWithTag:1000+indexPath.row];
        [txtField becomeFirstResponder];
                                                
    }
    else
    {
        if (appDelegate.isFromIncomeExpenseView < 0 )
        {
            if ([appDelegate.incomeTypeDBArray count]>0)
            {
                UITableViewCell *oldCell = [tblView cellForRowAtIndexPath:oldIndex];
                oldCell.accessoryType = UITableViewCellAccessoryNone;
            }
            
            UITableViewCell *newCell = [tblView cellForRowAtIndexPath:indexPath];
            newCell.accessoryType = UITableViewCellAccessoryCheckmark;
            
            appDelegate.incomeString = [NSString stringWithFormat:@"%@",[[appDelegate.incomeTypeDBArray objectAtIndex:indexPath.row]valueForKey:@"incomeType"]];
           
            //[appDelegate.incomeString retain];
            
            oldIndex = indexPath;
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)scrollViewToTextField:(id)textField
{
    // Set the current _scrollOffset, so we can return the user after editing
    
	//_scrollOffsetY = self.tableView.contentOffset.y;
    
    // Get a pointer to the text field's cell
    UITableViewCell *theTextFieldCell = (UITableViewCell *)[textField superview];
    
    // Get the text fields location
    CGPoint point = [theTextFieldCell convertPoint:theTextFieldCell.frame.origin toView:tblView];
    
    // Scroll to cell
    [tblView setContentOffset:CGPointMake(0, point.y - 12) animated: YES];
    
    // Add some padding at the bottom to 'trick' the scrollView.
    //[tblView setContentInset:UIEdgeInsetsMake(0, 0, point.y - 60, 0)];
}

@end
