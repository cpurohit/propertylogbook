//
//  HomeViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IncomeExpenseViewController, EquityViewController,equityListViewController, ReminderViewController,DashBoardViewController, PropertyLogBookAppDelegate;
@interface HomeViewController : UIViewController<UIActionSheetDelegate>
{

	IncomeExpenseViewController *incomeExpenseView;
	equityListViewController*equityView;
	ReminderViewController *reminderView;
    DashBoardViewController*dashBoardViewController;
    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UIButton *badgeButton;
    
    IBOutlet UIButton*btnIncomeExpense;
    IBOutlet UIButton*btnCalculators;
    IBOutlet UIButton*btnCheckListAndRemider;
    IBOutlet UIButton*btnDashboardAndReport;
    
    IBOutlet UIImageView *backgroundImage;
    
    
    IBOutlet UIImageView*welcome_image_view;
    IBOutlet UIButton*close_welcome_screen_button;
    
    IBOutlet UIView*homeView_navigation_title;
    
    IBOutlet UIView*welcome_screen_view;
    
    
    
    
}

/*
 Sun:0004
 Phase:3
 */
-(IBAction)calculator_button_pressed:(id)sender;
-(IBAction)dashboard_and_report_button_pressed:(id)sender;


-(IBAction)incomeExpense_click;
-(IBAction)equityButton_click;
-(IBAction)remiderBtn_click;
-(IBAction)reportBtn_click;


-(IBAction)close_welcome_screen_button_pressed:(id)sender;

@end
