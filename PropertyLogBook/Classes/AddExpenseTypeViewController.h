//
//  AddExpenseTypeViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

@interface AddExpenseTypeViewController : UIViewController<UITextFieldDelegate> {
    
    IBOutlet UITableView *tblView;
    UITextField *expenseText;
    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UILabel *lblNoData,*lblPlus;
    NSIndexPath *oldIndex;
    int textFieldIndex;
    IBOutlet UIImageView *imageView;
    
    UIToolbar *keytoolBar;
    int isClicked;
    NSString *textFieldTempText;
    
    BOOL isAddClicked;

}
-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew;
- (void)scrollViewToTextField:(id)textField;
@end
