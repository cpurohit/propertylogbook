//
//  DashBoardViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DashBoardViewController.h"
#import "PropertyLogBookAppDelegate.h"
#import "DashBoardDetailViewController.h"



@implementation DashBoardViewController

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

-(void)viewDidUnload
{
    [super viewDidUnload];
    
    //[datePicker removeFromSuperview];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
   

    
    
   
}


- (BOOL)prefersStatusBarHidden
{
    
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
   appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    /* CP :  */
    
    if(appDelegate.isIPad)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    }
    
    
    
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.title = @"DASHBOARD";
    
    
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height-10.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height-10.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    
    
    
    
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.frame = CGRectMake(0,198,320,180);
    
    [datePicker addTarget:self action:@selector(datePicker_changed:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:datePicker];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    datePicker.backgroundColor = [UIColor whiteColor];
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor],UITextAttributeTextColor,
                                                                     [UIColor whiteColor],UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
                                                                     UITextAttributeTextShadowOffset ,
                                                                     [UIFont boldSystemFontOfSize:19.0f],UITextAttributeFont  , nil]];
    
    
   
   
    
    NSLog(@" bar frame = %@",NSStringFromCGRect(self.navigationController.navigationBar.frame));
    

    
    
     NSLog(@" bar frame = %@",NSStringFromCGRect(self.navigationController.navigationBar.frame));
    
    dictionary = [[NSMutableDictionary alloc]init];
    
    
    rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    netIncomeAmount = netExpenseAmount = 0.0;
    datePicker.hidden = toolBar.hidden = TRUE;
    
    
    tblView.backgroundColor = [UIColor clearColor];
	
    
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    

    
       
    
    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0, 1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    [imgView release];
    
    if(appDelegate.isIPad)
    {
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:1.0];
    }
    
    
    
    
    
    
    
    if(appDelegate.isIPad)
    {
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:1.0];
    }
    
    /*
     Sun:004*/
    [tblView reloadData];
    
    
    
    toButton.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    fromButton.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    
    
    nslog(@"\n dictionary = %@",dictionary);
    
    
    /*
     Start:
     Sun:0004
     Setting custom image in navigation bar..
     */
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0)
    {
        
        if(appDelegate.isIPad)
        {
         
            [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_ipad.png"] forBarMetrics:UIBarMetricsDefault];
            
        }
        else
        {
            [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
        }
        
        
    }
    
    /*
     Ends:
     Sun:0004
     Setting custom image in navigation bar..
     */
    
    
    
    if(!appDelegate.isIPad)
    {
        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
            UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator.width = -12;
            
            [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
            
            
        }
        else
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
        }
        
    }
    
    
    
    
    /*
     Starts:
     Sun:0004
     Checking iphone-5 condition.
     
     */
    if(appDelegate.isIphone5)
    {
        datePicker.frame = CGRectMake(datePicker.frame.origin.x, datePicker.frame.origin.y+88,datePicker.frame.size.width, datePicker.frame.size.height);
    }
    
    /*
     Ends :
     Sun:0004
     Checking iphone-5 condition.
     */
    
    
    
        
    
   [self reload_data];/*Even if reload data was commented,app was crashign after aroudm 26 times push and pop...*/
    
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    /**/
    
    //[appDelegate.bottomView removeFromSuperview];
    
    is_view_will_appear_called = FALSE;
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
     
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
    /*CP : weird condition...*/
    
    if(appDelegate.isIPad)
    {
        if(is_view_will_appear_called)
        {
            return;
        }
        is_view_will_appear_called = TRUE;
    }
    
    
    [super viewWillAppear:YES];
    
    
   
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    is_done_clicked = FALSE;
    
    if(appDelegate.isIPad)
    {
        
        nslog(@"\n tableview frame = %@ \n",NSStringFromCGRect(tblView.frame));
       
        
         nslog(@"\n appDelegate.ipad_settings_bottom_button = %@ \n",appDelegate.ipad_settings_bottom_button);
                
        
        [self reload_data];
    
        
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];

        
        
        if (!appDelegate.isFirst)
        {
            
            

            
            [appDelegate.dashboarad_bottom_button setSelected:FALSE];
            [appDelegate.income_expsne_bottom_button setSelected:FALSE];
            [appDelegate.calculator_bottom_button setSelected:FALSE];
            [appDelegate.reminder_bottom_button setSelected:FALSE];
            [appDelegate.ipad_settings_bottom_button setSelected:TRUE];
            [self.tabBarController setSelectedIndex:4];
            
            //[appDelegate.tabBarController setSelectedIndex:4];
            
            
            [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"isFirst"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //appDelegate.isFirst = TRUE;
            
            
            
            
        }
               
        nslog(@"\n appDelegate.isFirst = %d", [[NSUserDefaults standardUserDefaults]boolForKey:@"isFirst"]);
        
    }
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    /**/
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    
    //[self.view addSubview:appDelegate.add_income_expense_bottom_button];
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    

    if(appDelegate.isIPad)
    {
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:1.0];
    }
    
   /*
    Sun:004*/
    [tblView reloadData];
   // tblView.backgroundColor = [UIColor purpleColor];
   // self.view.backgroundColor = [UIColor blueColor];
    
    nslog(@"\n self.view frame = %@",NSStringFromCGRect(self.view.frame));


    
    //[self.view bringSubviewToFront:appDelegate.bottomView];
    
    
    
    
}


-(void)reload_data
{
    //Date range for filtering record..
    
    df = [[[NSDateFormatter alloc] init] autorelease];
    [df setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    
    
    NSDateComponents *monthComponent = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:[NSDate date]];
    [monthComponent setMonth:[monthComponent month]];
    [monthComponent setDay:1];
    [monthComponent setYear:[monthComponent year]];
    NSDate *firstDateOfMonth = [gregorian dateFromComponents:monthComponent];
    
    
    //[yearComponent setMonth:[monthComponent month]];
    NSDateComponents *yearComponent = [gregorian components:NSYearCalendarUnit fromDate:[NSDate date]];
    [yearComponent setMonth:1];
    [yearComponent setDay:1];
    
    
    
    
    
    NSDate *firstDateOfyear = [gregorian dateFromComponents:yearComponent];
    
    
    
    
    
    
    
    
    NSDate *current = [NSDate date];
    //NSString *currentDate = [df stringFromDate:current];
    
    
    [gregorian release];
    
    
    
    
    /*
     Starts:  SUN:0004
     Date:05-03-2013
     For date filter conditions...
     
     */
    
    NSMutableDictionary*data_dictionary = [[[NSMutableDictionary alloc] initWithDictionary:[appDelegate select_default_date_filter]] autorelease];
    
    nslog(@"\n data_dictionary = %@",data_dictionary);
    
    
    //This is required as we are calling viewwillappear on done button.
    if(!is_done_clicked)
    {
        
        switch (appDelegate.date_filter_type)
        {
            case 0:
            {
                //MTD
                
                [fromButton setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:firstDateOfMonth]] forState:UIControlStateNormal];
                
                [toButton setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:current]] forState:UIControlStateNormal];
                
                break;
            }
            case 1:
            {
                //YTD
                
                
                               
                
                [fromButton setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:firstDateOfyear]] forState:UIControlStateNormal];
            
                [toButton setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:current]] forState:UIControlStateNormal];
                
               
                
                
                break;
            }
            case 2:
            {
                //Date Range
                
                if([[data_dictionary objectForKey:@"1000"] length]==0)
                {
                    [fromButton setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:firstDateOfyear]] forState:UIControlStateNormal];
                }
                else
                {
                    [fromButton setTitle:[NSString stringWithFormat:@" %@",[data_dictionary objectForKey:@"1000"]] forState:UIControlStateNormal];
                }
                
                if([[data_dictionary objectForKey:@"1001"] length]==0)
                {
                    [toButton setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:current]] forState:UIControlStateNormal];
                }
                else
                {
                    [toButton setTitle:[NSString stringWithFormat:@" %@",[data_dictionary objectForKey:@"1001"]] forState:UIControlStateNormal];
                    
                }

                
                break;
            }
                
            default:
                break;
        }
        
                
        
    }
    
    
    /*
     Ends: SUN:0004
     
     Date:05-03-2013
     For date filter conditions...
     
     */
    
    
    
    if(appDelegate.isIPad)
    {
        noDataLabel.frame = CGRectMake(noDataLabel.frame.origin.x,noDataLabel.frame.origin.y,312, noDataLabel.frame.size.height);
    }
    
    
    
    NSDate *fromDate = [appDelegate.regionDateFormatter dateFromString:[fromButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    
    NSDate *toDate = [appDelegate.regionDateFormatter dateFromString:[toButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    
    /*Sun:004
     This might not be needed as it might be filled in setting and when property/income expense is modifying...*/
    
    
    [appDelegate selectPropertyDetail];
    
    
    
   
    
    
    //[appDelegate selectIncomeExpense];
    [appDelegate selectIncomeExpenseForDashBoardBoard:[appDelegate fetchDateInCommonFormate:[fromButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]] endDate: [appDelegate fetchDateInCommonFormate:[toButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]];
    
    
    
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
        [appDelegate selectCurrencyIndex];
    }
    
    
    
    
    
    [dictionary removeAllObjects];
    
    
    toolBar.hidden = TRUE;
    datePicker.hidden = TRUE;
    
    if ([fromButton.titleLabel.text length]>0 && [toButton.titleLabel.text length]>0)
    {
        
        for (int i = 0;i <[appDelegate.propertyDetailArray count];i++)
        {
            NSMutableArray *array = [[NSMutableArray alloc]init];
            for (int  j =0;j<[appDelegate.incomeExpenseArray count];j++)
            {
                int from;
                from = [fromDate compare:[[appDelegate.incomeExpenseArray objectAtIndex:j]valueForKey:@"date"]];
                
                int to ;
                
                if ([toDate isEqualToDate:[[appDelegate.incomeExpenseArray objectAtIndex:j]valueForKey:@"date"]])
                {
                    to = 1;
                }
                else
                {
                    to = [toDate compare:[[appDelegate.incomeExpenseArray objectAtIndex:j]valueForKey:@"date"]];
                }
                
                
                if ([[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"0"] isEqualToString:[[appDelegate.incomeExpenseArray objectAtIndex:j]valueForKey:@"property"]])
                {
                    if (from <= 0  && to > 0)
                    {
                        
                        NSMutableDictionary*temp_dict =[appDelegate.incomeExpenseArray objectAtIndex:j];
                        [temp_dict setObject:[[appDelegate.propertyDetailArray objectAtIndex:i] objectForKey:@"image"] forKey:@"image"];
                        [appDelegate.incomeExpenseArray replaceObjectAtIndex:j withObject:temp_dict];
                        temp_dict = nil;
                        
                        [array addObject:[appDelegate.incomeExpenseArray objectAtIndex:j]];
                    }
                }
            }
            if ([array count]>0)
            {
                
                // [array addObject:[[appDelegate.propertyDetailArray objectAtIndex:i] objectForKey:@"image"]];
                
                [dictionary setObject:array forKey:[NSString stringWithFormat:@"%@",[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"0"]]];
            }
            [array release];
        }
    }
    else
    {
        for (int i = 0;i <[appDelegate.propertyDetailArray count];i++)
        {
            NSMutableArray *array = [[NSMutableArray alloc]init];
            for (int  j =0;j<[appDelegate.incomeExpenseArray count];j++)
            {
                
                if ([[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"0"] isEqualToString:[[appDelegate.incomeExpenseArray objectAtIndex:j]valueForKey:@"property"]])
                {
                    
                    
                    NSMutableDictionary*temp_dict =[appDelegate.incomeExpenseArray objectAtIndex:j];
                    [temp_dict setObject:[[appDelegate.propertyDetailArray objectAtIndex:i] objectForKey:@"image"] forKey:@"image"];
                    [appDelegate.incomeExpenseArray replaceObjectAtIndex:j withObject:temp_dict];
                    
                    temp_dict = nil;
                    
                    [array addObject:[appDelegate.incomeExpenseArray objectAtIndex:j]];
                    
                }
            }
            if ([array count]>0)
            {
                /*Sun:0004
                 This is due to weird behavious of section index...
                 */
                //[array addObject:[[appDelegate.propertyDetailArray objectAtIndex:i] objectForKey:@"image"]];
                
                
                
                
                
                
                [dictionary setObject:array forKey:[NSString stringWithFormat:@"%@",[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"0"]]];
            }
            [array release];
        }
        
    }
    nslog(@"property info dictionary === %@",dictionary);
    
    if(!appDelegate.isIPad)
    {
        if(appDelegate.isIphone5)
        {
            noDataLabel.frame = CGRectMake(noDataLabel.frame.origin.x, noDataLabel.frame.origin.y+22,noDataLabel.frame.size.width, noDataLabel.frame.size.height);
            no_data_dashboard_listing_imageView.frame = CGRectMake(no_data_dashboard_listing_imageView.frame.origin.x, no_data_dashboard_listing_imageView.frame.origin.y+22,no_data_dashboard_listing_imageView.frame.size.width, no_data_dashboard_listing_imageView.frame.size.height);
        }

    }
    
        
    
    //Date:11-09-2012
    noDataLabel.hidden = TRUE;
    no_data_dashboard_listing_imageView.hidden = TRUE;
    if([dictionary count]==0)
    {
        tblView.hidden = TRUE;
        noDataLabel.hidden = FALSE;
        no_data_dashboard_listing_imageView.hidden = FALSE;
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
            {
                noDataLabel.center = CGPointMake(384,self.view.frame.size.height/2);
            }
            else
            {
                noDataLabel.center = CGPointMake(512,self.view.frame.size.height/2);
            }
            
        }
        
        
        
        
    }
    else
    {
        tblView.hidden = FALSE;
    }
    
    /*Sun:004*/
   [tblView reloadData];
     
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

-(IBAction)dateBtn_clicked:(id)sender
{
    
    /*
     NSDate *fromDate = [df dateFromString:[fromButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
     
     NSDate *toDate = [df dateFromString:[toButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
     */
    
    NSDate *fromDate = [appDelegate.regionDateFormatter dateFromString:[fromButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    
    NSDate *toDate = [appDelegate.regionDateFormatter dateFromString:[toButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    
    
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            toolBar.frame = CGRectMake(0, 669,768, 44);
            datePicker.frame =  CGRectMake(0,713,768,216 );
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            toolBar.frame = CGRectMake(0,414,1024, 44);
            datePicker.frame =  CGRectMake(0,458,1024,216 );
        }

    }
    
    
    
    datePicker.hidden = FALSE;
    toolBar.hidden = FALSE;
    
    
    
    UIButton *btn = (UIButton *)sender;
    buttontag = btn.tag;
    if (btn.tag == 20)
    {
        datePicker.date = fromDate;
        
    }
    else if (btn.tag == 21)
    {
        datePicker.date = toDate;
        
        
    }
    
    
    /*
     [btn setTitle:[NSString stringWithFormat:@" %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
     
     */
    
    
    [btn setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
    
    
}

-(IBAction)datePicker_changed:(id)sender
{
    UIButton *btn = (UIButton *)[self.view viewWithTag:buttontag];
    /*
     [btn setTitle:[NSString stringWithFormat:@" %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
     */
    
    [btn setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
    
    
}
-(IBAction)done_clicked
{
    if (buttontag == 21)
    {
        if ([fromButton.titleLabel.text length]>0)
        {
            
            /*
             NSDate *tempDate = [df dateFromString:[fromButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
             */
            
            NSDate *tempDate = [appDelegate.regionDateFormatter dateFromString:[fromButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
            
            nslog(@"date comp: %d", [tempDate compare:datePicker.date]);
            
            if (![tempDate isEqualToDate:datePicker.date])
            {
                int comp = [tempDate compare:datePicker.date];
                if (comp > 0)
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Date should be greater than from date" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    [alert release];
                    return;
                }
            }
        }
    }
    
    datePicker.hidden = toolBar.hidden = TRUE;
    is_done_clicked = TRUE;
    
    [self reload_data];
    
    
}
-(IBAction)tool_bar_cancel_pressed:(id)sender
{
    datePicker.hidden = toolBar.hidden = TRUE;
    is_done_clicked = TRUE;
}
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations.
    if(appDelegate.isIPad)
    {
        return TRUE;
    }
    return FALSE;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
    
    
    
    if(appDelegate.isIPad)
    {
        
        toolBar.hidden = TRUE;
        datePicker.hidden = TRUE;
        
        if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
        {
            imgViewBar.frame = CGRectMake(0, 0,768, imgViewBar.frame.size.height);
            lblFrom.frame = CGRectMake(170,11,112, lblFrom.frame.size.height);
            fromButton.frame = CGRectMake(230,fromButton.frame.origin.y,112, fromButton.frame.size.height);
            
            lblTo.frame = CGRectMake(422,lblTo.frame.origin.y,lblTo.frame.size.width,lblTo.frame.size.height);
            toButton.frame = CGRectMake(460,toButton.frame.origin.y,112, toButton.frame.size.height);
            
            
            
            toolBar.frame = CGRectMake(0, 669,768, 44);
            datePicker.frame =  CGRectMake(0,713,768,216 );
            noDataLabel.center = CGPointMake(384,self.view.frame.size.height/2);
            no_data_dashboard_listing_imageView.center =  CGPointMake(384,self.view.frame.size.height/2 - 55);
            
            self.view.frame = CGRectMake(0, 0,768,1024);
            
            
            
            
        }
        else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
        {
            imgViewBar.frame = CGRectMake(0, 0,1024, imgViewBar.frame.size.height);
            lblFrom.frame = CGRectMake(286,11,lblFrom.frame.size.width, lblFrom.frame.size.height);
            fromButton.frame = CGRectMake(346,fromButton.frame.origin.y,fromButton.frame.size.width, fromButton.frame.size.height);
            
            lblTo.frame = CGRectMake(560,lblTo.frame.origin.y,lblTo.frame.size.width,lblTo.frame.size.height);
            toButton.frame = CGRectMake(600,toButton.frame.origin.y,toButton.frame.size.width, toButton.frame.size.height);
            
            
            toolBar.frame = CGRectMake(0,414,1024, 44);
            datePicker.frame =  CGRectMake(0,458,1024,216 );
            noDataLabel.center = CGPointMake(512,self.view.frame.size.height/2);
            
            no_data_dashboard_listing_imageView.center =  CGPointMake(512,self.view.frame.size.height/2 - 55);
             
            
            self.view.frame = CGRectMake(0, 0,1024,768);
        }
        
    }
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    
    
    if(appDelegate.isIPad)
    {
        
        [appDelegate manageViewControllerHeight];
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            toolBar.frame = CGRectMake(0, 669,768, 44);
            datePicker.frame =  CGRectMake(0,713,768,216 );
            noDataLabel.center = CGPointMake(384,self.view.frame.size.height/2);
            
            no_data_dashboard_listing_imageView.center =  CGPointMake(384,self.view.frame.size.height/2 - 55);
            
           // tblView.frame = CGRectMake(0,44,768, 936);
            
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            toolBar.frame = CGRectMake(0,414,1024, 44);
            datePicker.frame =  CGRectMake(0,458,1024,216 );
            noDataLabel.center = CGPointMake(512,self.view.frame.size.height/2);
            
            no_data_dashboard_listing_imageView.center =  CGPointMake(512,self.view.frame.size.height/2 - 55);
            
           // tblView.frame = CGRectMake(0,44,1024, 660);
        }
        
        welcome_screen_view.center = CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2);
        
        
        
        
        [tblView reloadData];
       
        nslog(@"\n tblview frame = %@",NSStringFromCGRect(tblView.frame));
        
    }

    
    
    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}




- (void)dealloc
{
    [dictionary release];
    [datePicker release];
    [toolBar release];
    [super dealloc];
}



#pragma mark -
#pragma mark tableview method


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    
    
    /*
     Sun:0004
     */
   // NSArray *sectionNameArray  =[dictionary allKeys];
    
    return [[dictionary allKeys] count]+1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    return 1;
    
    
    /*SUN:0004
     if(section==0)
     {
     return 1;
     }
     else
     {
     NSArray *sectionNameArray  =[dictionary allKeys];
     nslog(@"\n sectionNameArray in numberOfSectionsInTableView = %@ ",sectionNameArray);
     return [sectionNameArray count];
     }
     return  0;
     */
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     Sun : 0004 same size for all sections..
     */
    
     
    if (indexPath.section == 0)
    {
        
       

        if(!appDelegate.is_milege_on)
        {
            if(appDelegate.isIPad)
            {
                return 221;
            }
            return 152;
        }
        else
        {
            if(appDelegate.isIPad)
            {
                
                if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
                {
                    return 250;
                }
                else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
                {
                    return 243;
                }
                
            }
            return 152;
        }
        
        
        
    }
    else
    {
        if(!appDelegate.is_milege_on)
        {
            return 125;
        }
        else
        {
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
                {
                    return 168;
                }
                else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
                {
                    return 155;
                }
            }
            else
            {
                return 140;
            }

        }
                
    }
    return 125;
    
}


-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    
    nslog(@"\n sectino in heightForHeaderInSection  = %d",section);
    
    if(appDelegate.isIPad)
    {
        if(is_done_clicked)
        {
            
            if(section>=1)
            {
                return 25.0f;
            }
            
        }
        
        return 25.0f;

    }
    else
    {
        if(is_done_clicked)
        {
            
            if(section>=1)
            {
                return 15.0f;
            }
            
        }
        
        return 5.0f;
    }
    
        
    return 5.0f;
}





// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [numberFormatter setCurrencyCode:appDelegate.curCode];
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [numberFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        
        else if([[numberFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [numberFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [numberFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        
    }
    [numberFormatter setNegativeFormat:@"-¤#,##0.00"];
    
    UILabel *propertyLbl, *incomeLable, *expenseLable, *positionLable, *incomeAmount, *expenseAmount, *positionAmount;
    
    UILabel*milage_label,*mileageAmountLabel;
    
    
    //UILabel*portfolio_summary_label;
    
    UIImageView *cellImageView;
    
    UIImageView*net_income_imageView;
    UIImageView*net_position_imageView;
    UIImageView*net_expense_imageView;
    UIImageView*net_mileage_imageView;
    
    
    UIImageView*portfolio_summary_imageView;
    
    
    
    
    if (indexPath.section == 0)
    {
        static NSString *CellIdentifier = @"Cell";
        
        
        tblView.sectionFooterHeight = 1.0;
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.tag = indexPath.row;
        if (cell == nil)
        //if (1)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
            
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor =[UIColor clearColor];
            cellImageView = [[UIImageView alloc]initWithFrame:CGRectMake(12,40,23,23)];
            
            

            
            
            
            
            cellImageView.tag = 8000;
            [cell.contentView addSubview:cellImageView];
            
            
            portfolio_summary_imageView= [[UIImageView alloc]initWithFrame:CGRectMake(2,15,23,23)];
            
            
            
            
            portfolio_summary_imageView.image = [UIImage imageNamed:@"portfolio_summary_image.png"];
            //[cell.contentView addSubview:portfolio_summary_imageView];
            [portfolio_summary_imageView release];
            
            
            
            
            net_expense_imageView= [[UIImageView alloc]initWithFrame:CGRectMake(12,67,23,23)];
            net_expense_imageView.tag = 8001;
            [cell.contentView addSubview:net_expense_imageView];
            
            
            net_position_imageView= [[UIImageView alloc]initWithFrame:CGRectMake(12,92,23,23)];
            net_position_imageView.tag = 8002;
            [cell.contentView addSubview:net_position_imageView];
            
            
            //Mileage
            net_mileage_imageView= [[UIImageView alloc]initWithFrame:CGRectMake(12,115,23,23)];
            net_mileage_imageView.tag = 8003;
           
            
            propertyLbl = [[UILabel alloc]initWithFrame:CGRectMake(0,15,300, 23)];
            
              
            
            
            propertyLbl.backgroundColor = [UIColor clearColor];
            [propertyLbl setFont:[UIFont boldSystemFontOfSize:13.0]];
            propertyLbl.tag = 1000;
            [cell.contentView addSubview:propertyLbl];
            propertyLbl.text = @"Portfolio Summary";
            propertyLbl.font = [UIFont systemFontOfSize:15.0f];
            
            [propertyLbl setTextColor:[UIColor blackColor]];
            propertyLbl.textAlignment = NSTextAlignmentCenter;
            /*
             Sun:0004
             This will be removed..as new image will contain dash... :-)
             */
            
                        
            
            
            incomeLable = [[UILabel alloc]initWithFrame:CGRectMake(40,35, 120,30)];
            
             
            incomeLable.backgroundColor = [UIColor clearColor];
            incomeLable.text = @"Net Income";
            
            [incomeLable setFont:[UIFont systemFontOfSize:13.0]];
            incomeLable.tag = 2000;
            [cell.contentView addSubview:incomeLable];
            
            
            
            expenseLable = [[UILabel alloc]initWithFrame:CGRectMake(40,62, 120, 30)];
            

            
            
            
            
            expenseLable.backgroundColor = [UIColor clearColor];
            expenseLable.text = @"Net Expense";
            [expenseLable setFont:[UIFont systemFontOfSize:13.0]];
            //        [expenseLable setFont:[UIFont boldSystemFontOfSize:13.0]];
            expenseLable.tag = 3000;
            
            [cell.contentView addSubview:expenseLable];
            
            
            
            positionLable = [[UILabel alloc]initWithFrame:CGRectMake(40,90, 120, 30)];
            
             
            
            positionLable.backgroundColor = [UIColor clearColor];
            positionLable.text = @"Net Position";
            positionLable.tag = 4000;
            [positionLable setTextColor:[UIColor blackColor]];
            [positionLable setFont:[UIFont systemFontOfSize:13.0]];
            [cell.contentView addSubview:positionLable];

            
            //Mileage
            
            
            milage_label = [[UILabel alloc]initWithFrame:CGRectMake(40,113, 120, 30)];
            
            
             
            
            milage_label.textColor=[UIColor colorWithRed:84.0/255.0f green:84.0/255.0f blue:84.0/255.0f alpha:1.0f];
            milage_label.backgroundColor = [UIColor clearColor];
            
            /*
            milage_label.text = @"Total Mileage";
            */
            
            milage_label.text = [NSString stringWithFormat:@"Total Mileage (%@)",[appDelegate.prefs objectForKey:@"milage_unit"]];
            
            milage_label.tag = 4001;
            
            [milage_label setFont:[UIFont systemFontOfSize:13.0]];
            
            
            
            
            

            
            
            
            /*Sun:0004
             iPad 
             
             */
            
            
            incomeAmount = [[UILabel alloc]initWithFrame:CGRectMake(180, 48, 95, 30)];
            
             
            incomeAmount.backgroundColor = [UIColor clearColor];
            incomeAmount.tag = 5000;
            [incomeAmount setFont:[UIFont systemFontOfSize:13.0]];
            
            [incomeAmount setTextColor:[UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0]];
            [incomeAmount setTextAlignment:UITextAlignmentRight];
            [cell.contentView addSubview:incomeAmount];
            
            
            /*Sun:0004
             iPad 
             
             */
            
            expenseAmount = [[UILabel alloc]initWithFrame:CGRectMake(180,62, 95, 30)];
            
            
            expenseAmount.backgroundColor = [UIColor clearColor];
            [expenseAmount setFont:[UIFont systemFontOfSize:13.0]];
            expenseAmount.tag = 6000;
            [expenseAmount setTextColor:[UIColor redColor]];
            [expenseAmount setTextAlignment:UITextAlignmentRight];
            [cell.contentView addSubview:expenseAmount];
            
            /*Sun:0004
             iPad pending
             
             */
            
            
            
            positionAmount = [[UILabel alloc] initWithFrame:CGRectMake(180, 110, 95, 30)];
             
            
            
            positionAmount.backgroundColor = [UIColor clearColor];
            [positionAmount setFont:[UIFont systemFontOfSize:13.0]];
            positionAmount.tag = 7000;
            [positionAmount setTextAlignment:UITextAlignmentRight];
            
            
            
            [cell.contentView addSubview:positionAmount];
            
            
            //Mileage
            mileageAmountLabel = [[UILabel alloc]initWithFrame:CGRectMake(180, 113, 95, 30)];
            
            
            mileageAmountLabel.textColor=[UIColor colorWithRed:84.0/255.0f green:84.0/255.0f blue:84.0/255.0f alpha:1.0f];
            mileageAmountLabel.backgroundColor = [UIColor clearColor];
            [mileageAmountLabel setFont:[UIFont systemFontOfSize:13.0]];
            mileageAmountLabel.tag = 7001;
            [mileageAmountLabel setTextAlignment:UITextAlignmentRight];
            
            
            [cell.contentView addSubview:milage_label];
            [cell.contentView addSubview:mileageAmountLabel];
            [cell.contentView addSubview:net_mileage_imageView];
            
            
                        
        

            
            
            
            
            
        }
        else
        {
            
            cellImageView               = (UIImageView *)[cell.contentView viewWithTag:8000];
            net_expense_imageView   = (UIImageView *)[cell.contentView viewWithTag:8001];
            net_position_imageView  = (UIImageView *)[cell.contentView viewWithTag:8002];
            net_mileage_imageView   = (UIImageView *)[cell.contentView viewWithTag:8003];
            
            propertyLbl = (UILabel *)[cell.contentView viewWithTag:1000];
            incomeLable = (UILabel *)[cell.contentView viewWithTag:2000];
            expenseLable = (UILabel *)[cell.contentView viewWithTag:3000];
            positionLable = (UILabel *)[cell.contentView viewWithTag:4000];
            milage_label = (UILabel *)[cell.contentView viewWithTag:4001];
            incomeAmount = (UILabel *)[cell.contentView viewWithTag:5000];
            expenseAmount = (UILabel *)[cell.contentView viewWithTag:6000];
            positionAmount = (UILabel *)[cell.contentView viewWithTag:7000];
            mileageAmountLabel = (UILabel *)[cell.contentView viewWithTag:7001];
            
            
            
            [cellImageView retain];
            
            [net_expense_imageView retain];
            [net_position_imageView retain];
            [net_mileage_imageView retain];
            
            
            [propertyLbl retain];
            [incomeLable retain];
            [expenseLable retain];
            [positionLable retain];
            [milage_label retain];
            [incomeAmount retain];
            [expenseAmount retain];
            [positionAmount retain];
            [mileageAmountLabel retain];

            
            
            
            
        }
        
        
        if(appDelegate.is_milege_on)
        {
            milage_label.hidden = FALSE;
            mileageAmountLabel.hidden = FALSE;
            net_mileage_imageView.hidden = FALSE;
            
        }
        else
        {
            milage_label.hidden = TRUE;
            mileageAmountLabel.hidden = TRUE;
            net_mileage_imageView.hidden = TRUE;
        }

        
        
        
        if(!appDelegate.is_milege_on)
        {
            cellImageView.frame  = CGRectMake(12,50,23,23);
        }
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                
                cellImageView.frame = CGRectMake(40,92, 23,23);
                if(!appDelegate.is_milege_on)
                {
                    cellImageView.frame = CGRectMake(40,80, 23,23);
                }
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                cellImageView.frame = CGRectMake(50,92, 23,23);
                if(!appDelegate.is_milege_on)
                {
                    cellImageView.frame = CGRectMake(50,80, 23,23);
                    
                }
            }
        }
        
        
        
        
        if(!appDelegate.is_milege_on)
        {
            net_expense_imageView.frame = CGRectMake(12,82,23,23);
            
        }
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                
                
                net_expense_imageView.frame = CGRectMake(40,127, 23,23);
                
                if(!appDelegate.is_milege_on)
                {
                    net_expense_imageView.frame = CGRectMake(40,122, 23,23);
                }
                
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                net_expense_imageView.frame = CGRectMake(50,127, 23,23);
                if(!appDelegate.is_milege_on)
                {
                    net_expense_imageView.frame = CGRectMake(50,122, 23,23);
                }
            }
        }
        else
        {
            net_expense_imageView.frame = CGRectMake(12,67,23,23);
        }
        
        //
        
        
        if(!appDelegate.is_milege_on)
        {
            net_position_imageView.frame = CGRectMake(12,112,23,23);
            
        }
        
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                net_position_imageView.frame = CGRectMake(40,157, 23,23);
                if(!appDelegate.is_milege_on)
                {
                    net_position_imageView.frame = CGRectMake(40,162, 23,23);
                    
                }
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                net_position_imageView.frame = CGRectMake(50,157, 23,23);
                if(!appDelegate.is_milege_on)
                {
                    net_position_imageView.frame = CGRectMake(50,162, 23,23);
                    
                }
            }
        }

        
        
        //
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                net_mileage_imageView.frame = CGRectMake(40,185, 23,23);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                net_mileage_imageView.frame = CGRectMake(50,185, 23,23);
            }
        }

        //
        
        
        
        if(appDelegate.isIPad)
        {
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    propertyLbl.frame = CGRectMake(0,40, 768, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    propertyLbl.frame = CGRectMake(0,45, 1024, 30);
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    propertyLbl.frame = CGRectMake(0,40, 680, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    propertyLbl.frame = CGRectMake(0,45, 935, 30);
                }
            }
            
            
            
        }
        else
        {
            
            
            propertyLbl.frame = CGRectMake(0,15,300, 23);
            if(!appDelegate.is_milege_on)
            {
                propertyLbl.frame = CGRectMake(0,20,300, 23);
            }
            
            
        }
        

        //
        
        
        if(!appDelegate.is_milege_on)
        {
            incomeLable.frame = CGRectMake(40,45, 120,30);
        }
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                incomeLable.frame = CGRectMake(70,85, 120, 30);
                if(!appDelegate.is_milege_on)
                {
                    incomeLable.frame = CGRectMake(70,75, 120, 30);
                    
                }
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                incomeLable.frame = CGRectMake(80,85, 120, 30);
                if(!appDelegate.is_milege_on)
                {
                    incomeLable.frame = CGRectMake(80,75, 120, 30);
                    
                }
                
            }
            
        }
        
        
//
        
        
        if(!appDelegate.is_milege_on)
        {
            expenseLable.frame = CGRectMake(40,77, 120, 30);
        }
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                expenseLable.frame = CGRectMake(70,122, 120, 30);
                if(!appDelegate.is_milege_on)
                {
                    expenseLable.frame = CGRectMake(70,117, 120, 30);
                    
                }
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                expenseLable.frame = CGRectMake(80,122, 120, 30);
                if(!appDelegate.is_milege_on)
                {
                    expenseLable.frame = CGRectMake(80,117, 120, 30);
                    
                }
                
            }
            
        }
        
        //
        
        if(!appDelegate.is_milege_on)
        {
            positionLable.frame = CGRectMake(40,110, 120, 30);
            
        }
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                positionLable.frame = CGRectMake(70,152, 120, 30);
                if(!appDelegate.is_milege_on)
                {
                    positionLable.frame = CGRectMake(70,157, 120, 30);
                    
                }
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                positionLable.frame = CGRectMake(80,152, 120, 30);
                if(!appDelegate.is_milege_on)
                {
                    positionLable.frame = CGRectMake(80,157, 120, 30);
                    
                    
                }
            }
            
        }
        
        
//
        
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                milage_label.frame = CGRectMake(70,182, 120, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                milage_label.frame = CGRectMake(80,182, 120, 30);
            }
            
        }
        

        //
        
        
        
        if(appDelegate.isIPad)
        {
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    incomeAmount.frame = CGRectMake(268, 85, 440, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        incomeAmount.frame = CGRectMake(268, 75, 440, 30);
                        
                    }
                    
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    incomeAmount.frame = CGRectMake(260, 85, 700, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        incomeAmount.frame = CGRectMake(260, 85, 700, 30);
                    }
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    incomeAmount.frame = CGRectMake(188, 85, 440, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        incomeAmount.frame = CGRectMake(188, 75, 440, 30);
                        
                    }
                    
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    incomeAmount.frame = CGRectMake(180, 85, 700, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        incomeAmount.frame = CGRectMake(180, 85, 700, 30);
                    }
                }
            }
            
            
        }
        else
        {
            incomeAmount.frame = CGRectMake(180, 38, 95, 30);
            if(!appDelegate.is_milege_on)
            {
                incomeAmount.frame = CGRectMake(180, 48, 95, 30);
                
            }
        }

        //
        
        
        if(appDelegate.isIPad)
        {
            
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    expenseAmount.frame = CGRectMake(268, 122, 440, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        expenseAmount.frame = CGRectMake(268, 117, 440, 30);
                        
                    }
                    
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    expenseAmount.frame = CGRectMake(260, 122, 700, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        expenseAmount.frame = CGRectMake(260, 117, 700, 30);
                        
                    }
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    expenseAmount.frame = CGRectMake(188, 122, 440, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        expenseAmount.frame = CGRectMake(188, 117, 440, 30);
                        
                    }
                    
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    expenseAmount.frame = CGRectMake(180, 122, 700, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        expenseAmount.frame = CGRectMake(180, 117, 700, 30);
                        
                    }
                }
            }
            
        }
        else
        {
            expenseAmount.frame = CGRectMake(180,62, 95, 30);
            if(!appDelegate.is_milege_on)
            {
                expenseAmount.frame =CGRectMake(180,77, 95, 30);
            }
        }

        
        //
        
        
        if(appDelegate.isIPad)
        {
            
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    positionAmount.frame = CGRectMake(268, 152, 440, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        positionAmount.frame = CGRectMake(268, 157, 440, 30);
                        
                    }
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    positionAmount.frame = CGRectMake(260,152, 700, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        positionAmount.frame = CGRectMake(260, 157, 700, 30);
                    }
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    positionAmount.frame = CGRectMake(188, 152, 440, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        positionAmount.frame = CGRectMake(188, 157, 440, 30);
                        
                    }
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    positionAmount.frame = CGRectMake(180,152, 700, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        positionAmount.frame = CGRectMake(180, 157, 700, 30);
                    }
                }
            }
            
            
            
        }
        else
        {
            positionAmount.frame = CGRectMake(180, 90, 95, 30);
            if(!appDelegate.is_milege_on)
            {
                positionAmount.frame = CGRectMake(180, 110, 95, 30);
            }
        }

        //
        
        if(appDelegate.isIPad)
        {
            
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    mileageAmountLabel.frame = CGRectMake(268, 182, 440, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    mileageAmountLabel.frame = CGRectMake(260,182, 700, 30);
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    mileageAmountLabel.frame = CGRectMake(188, 182, 440, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    mileageAmountLabel.frame = CGRectMake(180,182, 700, 30);
                }
            }
           // mileageAmountLabel.backgroundColor = [UIColor purpleColor];
            
        }
        else
        {
            mileageAmountLabel.frame = CGRectMake(180, 113, 95, 30);
        }
        

        //
        
        
        
        
        
        
        
        float netTotalIncome = 0;
        float netTotalExpense = 0;
        float netTotalMileage = 0;
        
        float netTotalPosition = 0;
        NSArray* sectionNameArray = [dictionary allKeys];
        
        cellImageView.image = [UIImage imageNamed:@"dashboard_net_income_icon.png"];
        net_expense_imageView.image = [UIImage imageNamed:@"dashboard_net_expenses_icon.png"];
        net_position_imageView.image = [UIImage imageNamed:@"dashboard_net_position_icon.png"];
        net_mileage_imageView.image = [UIImage imageNamed:@"mileage_btn_black.png"];
        
        
        
        
        
        
        for ( int i = 0; i <[sectionNameArray count];i++)
        {
            NSMutableArray *arr = [dictionary objectForKey:[sectionNameArray objectAtIndex:i]];
            
            for (int j = 0; j <[arr count];j++)
            {
                if ([[[arr objectAtIndex:j]valueForKey:@"incomeExpense"]intValue] == 1)
                {
                    netTotalIncome = netTotalIncome + [[[arr objectAtIndex:j]valueForKey:@"amount"]floatValue];
                }
                else if ([[[arr objectAtIndex:j]valueForKey:@"incomeExpense"]intValue] == 0)
                {
                    netTotalExpense = netTotalExpense + [[[arr objectAtIndex:j]valueForKey:@"amount"]floatValue];
                }
                else
                {
                    netTotalMileage = netTotalMileage + [[[arr objectAtIndex:j]valueForKey:@"amount"]floatValue];
                }
            }
            
        }
        
        incomeAmount.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:netTotalIncome]];
        expenseAmount.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:netTotalExpense]];
        
        /*
        mileageAmountLabel.text  = [NSString stringWithFormat:@"(%@) %.2f",[appDelegate.prefs objectForKey:@"milage_unit"],netTotalMileage];
        */
        mileageAmountLabel.text  = [NSString stringWithFormat:@"%.2f",netTotalMileage];
        
        
        
        
        if (netTotalIncome >= netTotalExpense)
        {
            netTotalPosition = netTotalIncome - netTotalExpense;
            [positionAmount setTextColor:[UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0]];
        }
        else if (netTotalExpense > netTotalIncome)
        {
            netTotalPosition = netTotalExpense - netTotalIncome;
            [positionAmount setTextColor:[UIColor redColor]];
        }
        
        positionAmount.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:netTotalPosition]];
        
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                
                
                if(!appDelegate.is_milege_on)
                {
                        
                    cell.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dashboard_bar_port_no_mileage.png"]]autorelease];
                }
                else
                {
                    cell.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dashboard_bar_port.png"]]autorelease];
                }
                
                
               
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                if(!appDelegate.is_milege_on)
                {
                    cell.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dashboard_bar_land_no_mileage.png"]] autorelease];
                }
                else
                {
                    cell.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dashboard_bar_land.png"]] autorelease];
                }
                
            }
        }
        else
        {
            
            if(!appDelegate.is_milege_on)
            {
                
                cell.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dashboard_bar_no_mileage.png"]] autorelease];
                 
            }
            else
            {
                cell.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dashboard_bar.png"]] autorelease];
            }
            
        }
        
        
        
        [net_expense_imageView release];
        [net_position_imageView release];
        [net_mileage_imageView release];
        
        [milage_label release];
        [cellImageView release];
        [propertyLbl release];
        
        [incomeLable release];
        [expenseLable release];
        [positionLable release];
        [incomeAmount release];
        [expenseAmount release];
        [positionAmount release];
        
        [mileageAmountLabel release];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    
    else
    {
        static NSString *CellIdentifier1 = @"Cell1";
        
        tblView.sectionHeaderHeight = 0.0;
        tblView.sectionFooterHeight = 0.0;
        
        UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell1 == nil)
        //if (1)
        {
            cell1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier1] autorelease];
            
            
            
            cellImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10,2,23,23)];
            cellImageView.tag = 800;
            
            
            
            net_expense_imageView= [[UIImageView alloc]initWithFrame:CGRectMake(10,60,23,23)];
            net_expense_imageView.tag = 801;
            [cell1.contentView addSubview:net_expense_imageView];
            
            
            net_position_imageView= [[UIImageView alloc]initWithFrame:CGRectMake(10,107,23,23)];
            net_position_imageView.tag = 802;

            
                       
            
            [cell1.contentView addSubview:net_position_imageView];
            
            
            net_mileage_imageView= [[UIImageView alloc]initWithFrame:CGRectMake(10,118,23,23)];
            net_mileage_imageView.tag = 803;

            
            net_income_imageView = [[UIImageView alloc]initWithFrame:CGRectMake(10,33,23,23)];
            net_income_imageView.tag = 804;
            [cell1.contentView addSubview:net_income_imageView]; 
            
            
            
           
            
            /*Sun:004:Chieck this for reuse...*/
            net_income_imageView.image = [UIImage imageNamed:@"dashboard_net_income_icon.png"];
            net_expense_imageView.image = [UIImage imageNamed:@"dashboard_net_expenses_icon.png"];
            net_position_imageView.image = [UIImage imageNamed:@"dashboard_net_position_icon.png"];
            net_mileage_imageView.image = [UIImage imageNamed:@"mileage_btn_black.png"];
            
            
            
            
            
            propertyLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 300, 30)];
            
            propertyLbl.textAlignment = NSTextAlignmentCenter;
            propertyLbl.backgroundColor = [UIColor clearColor];
            propertyLbl.tag = 100;
            [propertyLbl setTextColor:[UIColor whiteColor]];
            [propertyLbl setFont:[UIFont boldSystemFontOfSize:14.0]];
            [cell1.contentView addSubview:propertyLbl];
            
            
            
            
            incomeLable = [[UILabel alloc]init];
            
             
            
            incomeLable.backgroundColor = [UIColor clearColor];
            incomeLable.text = @"Net Income";
            [incomeLable setFont:[UIFont systemFontOfSize:13.0]];
            incomeLable.tag = 200;
            [cell1.contentView addSubview:incomeLable];
            
            
            
            
            
            expenseLable = [[UILabel alloc]init];
            
             
            
            expenseLable.backgroundColor = [UIColor clearColor];
            expenseLable.text = @"Net Expense";
            expenseLable.tag = 300;
            
            [expenseLable setFont:[UIFont systemFontOfSize:13.0]];
            [cell1.contentView addSubview:expenseLable];
            
            
            
            
            
            
            
            /*Sun:0004
             iPad pending
             */
            
            incomeAmount = [[UILabel alloc]initWithFrame:CGRectMake(180, 33, 95, 30)];
            
            
            
            
            incomeAmount.backgroundColor = [UIColor clearColor];
            incomeAmount.tag = 500;
            [incomeAmount setTextAlignment:UITextAlignmentRight];
            [incomeAmount setTextColor:[UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0]];
            [incomeAmount setFont:[UIFont systemFontOfSize:13.0]];
            [cell1.contentView addSubview:incomeAmount];
            
             expenseAmount = [[UILabel alloc]initWithFrame:CGRectMake(180,57, 95, 30)];
            
             
            
            
            expenseAmount.backgroundColor = [UIColor clearColor];
            expenseAmount.tag = 600;
            [expenseAmount setTextAlignment:UITextAlignmentRight];
            [expenseAmount setTextColor:[UIColor redColor]];
            [expenseAmount setFont:[UIFont systemFontOfSize:13.0]];
            
            [cell1.contentView addSubview:expenseAmount];
            
            positionLable = [[UILabel alloc]initWithFrame:CGRectMake(40,93, 120, 30)];
            
                       
             
            positionLable.backgroundColor = [UIColor clearColor];
            positionLable.text = @"Net Position";
            [positionLable setTextColor:[UIColor blackColor]];
            positionLable.tag = 400;
            [positionLable setFont:[UIFont systemFontOfSize:13.0]];
            [cell1.contentView addSubview:positionLable];
            
            
            
            
            
            
            
            /*
             Sun:..
             */
            
            positionAmount = [[UILabel alloc]initWithFrame:CGRectMake(180,93, 95, 30)];
            
            
            
            positionAmount.backgroundColor = [UIColor clearColor];
            [positionAmount setFont:[UIFont systemFontOfSize:13.0]];
            positionAmount.tag = 700;
            [positionAmount setTextAlignment:UITextAlignmentRight];
            [cell1.contentView addSubview:positionAmount];
            
            
            
            
            
            //Mileage
            
            
            milage_label = [[UILabel alloc]initWithFrame:CGRectMake(40,115, 120, 30)];
            
            
              
            
            milage_label.textColor=[UIColor colorWithRed:84.0/255.0f green:84.0/255.0f blue:84.0/255.0f alpha:1.0f];
            milage_label.backgroundColor = [UIColor clearColor];
            /*
            milage_label.text = @"Total Mileage";
             */
            
            milage_label.text = [NSString stringWithFormat:@"Total Mileage (%@)",[appDelegate.prefs objectForKey:@"milage_unit"]];
            
            milage_label.tag = 4001;
            
            [milage_label setFont:[UIFont systemFontOfSize:13.0]];
            
            
            

            
            mileageAmountLabel = [[UILabel alloc]initWithFrame:CGRectMake(180, 115, 95, 30)];
            mileageAmountLabel.textColor=[UIColor colorWithRed:84.0/255.0f green:84.0/255.0f blue:84.0/255.0f alpha:1.0f];
            mileageAmountLabel.backgroundColor = [UIColor clearColor];
            [mileageAmountLabel setFont:[UIFont systemFontOfSize:13.0]];
            mileageAmountLabel.tag = 7002;
            [mileageAmountLabel setTextAlignment:UITextAlignmentRight];
            
            [cell1.contentView addSubview:mileageAmountLabel];
            [cell1.contentView addSubview:milage_label];
            [cell1.contentView addSubview:net_mileage_imageView];
            
            

            
            
             
        }
        else
        {
            
            
            cellImageView            = (UIImageView *)[cell1.contentView viewWithTag:800];
            net_expense_imageView   = (UIImageView *)[cell1.contentView viewWithTag:801];
            net_position_imageView  = (UIImageView *)[cell1.contentView viewWithTag:802];
            net_mileage_imageView   = (UIImageView *)[cell1.contentView viewWithTag:803];
            
            net_income_imageView = (UIImageView *)[cell1.contentView viewWithTag:804];
            
            propertyLbl = (UILabel *)[cell1.contentView viewWithTag:100];
            incomeLable = (UILabel *)[cell1.contentView viewWithTag:200];
            expenseLable = (UILabel *)[cell1.contentView viewWithTag:300];
            positionLable = (UILabel *)[cell1.contentView viewWithTag:400];
            
            milage_label = (UILabel *)[cell1.contentView viewWithTag:4001];
            incomeAmount = (UILabel *)[cell1.contentView viewWithTag:500];
            expenseAmount = (UILabel *)[cell1.contentView viewWithTag:600];
            positionAmount = (UILabel *)[cell1.contentView viewWithTag:700];
            mileageAmountLabel = (UILabel *)[cell1.contentView viewWithTag:7002];
            
            
            
            [cellImageView retain];
            [net_income_imageView retain];
            [net_expense_imageView retain];
            [net_position_imageView retain];
            [net_mileage_imageView retain];
            
            
            [propertyLbl retain];
            [incomeLable retain];
            [expenseLable retain];
            [positionLable retain];
            [milage_label retain];
            [incomeAmount retain];
            [expenseAmount retain];
            [positionAmount retain];
            [mileageAmountLabel retain];
            

            
        }
        
        
        if(appDelegate.isIPad)
        {
            net_expense_imageView.frame = CGRectMake(10,64,23,23);
        }
        //
        
        
        if(!appDelegate.is_milege_on)
        {
            net_position_imageView.frame= CGRectMake(10,97,23,23);
            
        }
        if(!appDelegate.isIPad)
        {
            net_position_imageView.frame = CGRectMake(10,95,23,23);
        }
        

        
        //
        
        
        if(appDelegate.isIPad)
        {
            
            net_income_imageView.frame = CGRectMake(10,38,23,23);
            net_mileage_imageView.frame = CGRectMake(10,131,23,23);
        }

        //
        
        if(appDelegate.isIPad)
        {
            
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    propertyLbl.frame = CGRectMake(0, 0, 768, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    propertyLbl.frame = CGRectMake(0, 0, 1024, 30);
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    propertyLbl.frame = CGRectMake(0, 0, 680, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    propertyLbl.frame = CGRectMake(0, 0, 935, 30);
                }
            }
            
        }
        else
        {
            propertyLbl.frame = CGRectMake(0, 0, 300, 30);
        }
        

        //
        
        if(appDelegate.isIPad)
        {
            
            
            incomeLable.frame = CGRectMake(40,35, 120, 30);
        }
        else
        {
            incomeLable.frame = CGRectMake(40,30, 120, 30);
        }
        

        //
        
        if(appDelegate.isIPad)
        {
            expenseLable.frame = CGRectMake(40,60, 120, 30);
        }
        else
        {
            expenseLable.frame = CGRectMake(40, 57, 120, 30);
        }
//
        
        
        if(appDelegate.isIPad)
        {
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    incomeAmount.frame = CGRectMake(260, 35,440, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    incomeAmount.frame = CGRectMake(260, 35,700, 30);
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    incomeAmount.frame = CGRectMake(180, 35,440, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    incomeAmount.frame = CGRectMake(180, 35,700, 30);
                }
            }
            
            
            
        }
        else
        {
            incomeAmount.frame = CGRectMake(180, 33, 95, 30);
            
        }

        //
        
        if(appDelegate.isIPad)
        {
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    expenseAmount.frame = CGRectMake(260, 60,440, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    expenseAmount.frame = CGRectMake(260, 60,700, 30);
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    expenseAmount.frame = CGRectMake(180, 60,440, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    expenseAmount.frame = CGRectMake(180, 60,700, 30);
                }
            }
            
        }
        else
        {
            expenseAmount.frame = CGRectMake(180,57, 95, 30);
        }
        

        //
        
        if(appDelegate.isIPad)
        {
            positionLable.frame = CGRectMake(40,103, 120, 30);
            if(!appDelegate.is_milege_on)
            {
                positionLable.frame = CGRectMake(40,93, 120, 30);
            }
        }
//
        
        if(appDelegate.isIPad)
        {
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    positionAmount.frame = CGRectMake(260, 103,440, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        positionAmount.frame = CGRectMake(260, 93,440, 30);
                        
                    }
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    positionAmount.frame = CGRectMake(260, 103,700, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        positionAmount.frame = CGRectMake(260, 93,700, 30);
                        
                    }
                }

            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    positionAmount.frame = CGRectMake(180, 103,440, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        positionAmount.frame = CGRectMake(180, 93,440, 30);
                        
                    }
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    positionAmount.frame = CGRectMake(180, 103,700, 30);
                    if(!appDelegate.is_milege_on)
                    {
                        positionAmount.frame = CGRectMake(180, 93,700, 30);
                        
                    }
                }
            }
            
            
        }
        else
        {
            positionAmount.frame = CGRectMake(180,93, 95, 30);
        }

        //
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                milage_label.frame = CGRectMake(40,127, 120, 30);
               
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                milage_label.frame = CGRectMake(40,127, 120, 30);
            }
            
        }
        

        //
        
        if(appDelegate.isIPad)
        {
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    mileageAmountLabel.frame = CGRectMake(260, 127, 440, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    mileageAmountLabel.frame = CGRectMake(260,127, 700, 30);
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    mileageAmountLabel.frame = CGRectMake(180, 127, 440, 30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    mileageAmountLabel.frame = CGRectMake(180,127, 700, 30);
                }
            }
            
           // mileageAmountLabel.backgroundColor = [UIColor purpleColor];
            
        }
        else
        {
            mileageAmountLabel.frame = CGRectMake(180, 115, 95, 30);
        }

        //
        
        
        if(appDelegate.is_milege_on)
        {
        
            mileageAmountLabel.hidden= FALSE;
            milage_label.hidden= FALSE;
            net_mileage_imageView.hidden = FALSE;
            
        }
        else
        {
            mileageAmountLabel.hidden= TRUE;
            milage_label.hidden= TRUE;
            net_mileage_imageView.hidden = TRUE;
        }
        
        
        
        
        
        
        NSArray* sectionNameArray = [dictionary allKeys];
        // NSMutableArray *tempArray = [dictionary objectForKey:[sectionNameArray objectAtIndex:indexPath.row]];
        
        NSMutableArray *tempArray = [dictionary objectForKey:[sectionNameArray objectAtIndex:indexPath.section-1]];
        /*Sun:004
         Crash for test...
         */
        
        //nslog(@"\n crash = %@",[tempArray objectAtIndex:1000]);
        
        //
        
        float totalIncome = 0.0;
        float totalexepense = 0.0;
        float totalMileage = 0.0;
        for (int i = 0; i<[tempArray count];i++)
        {
            if ([[[tempArray objectAtIndex:i]valueForKey:@"incomeExpense"] intValue]==1)
            {
                totalIncome = totalIncome + [[[tempArray objectAtIndex:i]valueForKey:@"amount"]floatValue];
            }
            else if ([[[tempArray objectAtIndex:i]valueForKey:@"incomeExpense"] intValue]==0)
            {
                totalexepense = totalexepense + [[[tempArray objectAtIndex:i]valueForKey:@"amount"]floatValue];
            }
            else
            {
                totalMileage= totalMileage + [[[tempArray objectAtIndex:i]valueForKey:@"amount"]floatValue];
            }
            
        }
        
        
        
        
        mileageAmountLabel.text = [NSString stringWithFormat:@"%.2f",totalMileage];
        
        float net;
    	NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        
        NSString *imagePath = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_thumb.png",[[tempArray objectAtIndex:0] objectForKey:@"property"]]];
        nslog(@"image path == %@",imagePath);
        // cellImageView.image = [UIImage imageNamed:imagePath];
        cellImageView.image = [UIImage imageWithContentsOfFile:imagePath];
        
        
        
        
        
        propertyLbl.text = [[tempArray objectAtIndex:0]valueForKey:@"property"];
        
        incomeAmount.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:totalIncome]];
        expenseAmount.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:totalexepense]];
        
        if (totalIncome >= totalexepense)
        {
            net = totalIncome - totalexepense;
            [positionAmount setTextColor:[UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0]];
        }
        else if (totalexepense > totalIncome)
        {
            net = totalexepense - totalIncome;
            [positionAmount setTextColor:[UIColor redColor]];
        }
        
        //    positionAmount.text = [NSString stringWithFormat:@"Net: %@",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:net]]];
        positionAmount.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:net]];
        
        // positionAmount.text = [NSString stringWithFormat:@"Net: %.2f",net];
        
        nslog(@"temp array count === %d",[tempArray count]);
        
        
        nslog(@"temparray in cell for row at index === %@",tempArray);
        
        cell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
            {
                
                if(!appDelegate.is_milege_on)
                {
                    cell1.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipad_port_dashboard_cell_bg_no_mileage.png"]] autorelease];
                    
                }
                else
                {
                    cell1.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipad_port_dashboard_cell_bg.png"]] autorelease];
                    
                }
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4 )
            {
                if(!appDelegate.is_milege_on)
                {

                    
                    cell1.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipad_land_dashboard_cell_bg_no_mileage.png"]] autorelease];
                    
                }
                else
                {
                    cell1.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipad_land_dashboard_cell_bg.png"]] autorelease];
                }
                
                
            }
        }
        else
        {
            
            if(!appDelegate.is_milege_on)
            {
                cell1.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dashboard_cell_bg_no_mileage.png"]] autorelease];
                
            }
            else
            {
                /**/
                cell1.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dashboard_cell_bg.png"]] autorelease];
                 
                
            }
        }
        
        
        
        [net_expense_imageView release];
        [net_income_imageView release];
        [net_position_imageView release];
        [net_mileage_imageView release];
        
        
        [mileageAmountLabel release];
        [milage_label release];
        [propertyLbl release];
        [cellImageView release];
        [incomeLable release];
        [expenseLable release];
        [incomeAmount release];
        [expenseAmount release];
        [positionLable release];
        [positionAmount release];
        
        
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell1;
    }
    
    return nil;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source.
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
 }
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */



#pragma mark - back_clicked
#pragma mark Comparing date.

-(void)back_clicked
{
    [self.navigationController popViewControllerAnimated:TRUE];
}




#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    nslog(@"\n indexPath.section = %d",indexPath.section);
    
    nslog(@"\n appDelegate.propertyDetailArray = %@",appDelegate.propertyDetailArray);
    
    if (indexPath.section !=0)
    {
        
        self.navigationItem.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil] autorelease];
        
        DashBoardDetailViewController *dashBoardDetail = [[DashBoardDetailViewController alloc]initWithNibName:@"DashBoardDetailViewController" bundle:nil];
        
        
        
        
        dashBoardDetail.arrayindex = (indexPath.section);
        NSArray* sectionNameArray = [dictionary allKeys];
        nslog(@"\n sectionNameArray = %@",sectionNameArray);
        [dashBoardDetail.dashboardArray removeAllObjects];
        nslog(@"\n dictionary = %@",dictionary);
        
        dashBoardDetail.dashboardArray = [dictionary objectForKey:[sectionNameArray objectAtIndex:indexPath.section-1]];
        
        nslog(@"\n dashBoardDetail.dashboardArray  = %@",dashBoardDetail.dashboardArray);
        
        dashBoardDetail.periodString = [fromButton.titleLabel.text stringByAppendingFormat:@" - %@",toButton.titleLabel.text];
        
        dashBoardDetail.from_date = fromButton.titleLabel.text;
        dashBoardDetail.to_date = toButton.titleLabel.text;
        
        for(int i = 0;i<[appDelegate.propertyDetailArray count];i++)
        {
            if([[sectionNameArray objectAtIndex:indexPath.section-1] isEqualToString:[[appDelegate.propertyDetailArray objectAtIndex:i] objectForKey:@"0"]])
            {
                dashBoardDetail.arrayindex = i;
                break;
            }
        }
        
        
        [self.navigationController pushViewController:dashBoardDetail animated:YES];
        [dashBoardDetail release];
    }
    
}

#pragma mark - close_welcome_screen_button_pressed

-(IBAction)close_welcome_screen_button_pressed:(id)sender
{
    
    [self.view setUserInteractionEnabled:TRUE];
    
    welcome_screen_view.hidden = TRUE;
    welcome_image_view.hidden = TRUE;
    close_welcome_screen_button.hidden = TRUE;
    
}

@end
