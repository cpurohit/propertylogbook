//
//  CustomCell2.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

@interface CustomCell2 : UITableViewCell
{
    UITextField *s;
    UISwitch *financialSwitch;
    UILabel *financialLabel;
    UIButton *repaymentButton;
    UISwitch *leaseSwitch;
    UIButton *leaseDateButton;
    UILabel *detailLabel;
    PropertyLogBookAppDelegate *appDelegate;
    BOOL isIOS5;

}
@property (nonatomic, retain)UITextField *financialTextField;
@property (nonatomic, retain)UISwitch *financialSwitch, *leaseSwitch;
@property (nonatomic, retain)UILabel *financialLabel;
@property (nonatomic, retain)UIButton *repaymentButton, *leaseDateButton;
@property (nonatomic, retain)UILabel *detailLabel;

@end
