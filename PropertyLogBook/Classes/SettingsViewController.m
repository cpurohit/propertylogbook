//
//  SettingsViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SettingsViewController.h"
#import "propertyViewController.h"
#import "propertyTypeViewController.h"
#import "AddIncomeTypeViewController.h"
#import "AddExpenseTypeViewController.h"
#import "AddReminderTypeViewController.h"
#import "PropertyLogBookAppDelegate.h"
#import "agentInformationViewController.h"
#import "currencyViewController.h"

#import "AboutViewController.h"
#import "FAQViewController.h"
#import "PasswrodOnOffViewController.h"
#import "MilageViewController.h"

#import "AddPropertyDetailViewController.h"

#import "DateFiltersViewController.h"

//#import "CloudMe.h"

#define kFILENAME @"mydocument.dox"
#import "Note.h"
@implementation SettingsViewController

@synthesize doc = _doc;
@synthesize query = _query;
@synthesize notes = _notes;
@synthesize successLoopCount;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.


- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
   
    
    appDelegate.prefs = [NSUserDefaults standardUserDefaults];
    appDelegate.is_milege_on = [appDelegate.prefs boolForKey:@"is_milage_set"];

    NSLog(@"\n rect = %@",NSStringFromCGRect(tblView.frame));
    NSLog(@"\n self rect = %@",NSStringFromCGRect(self.view.frame));

    
    data_dictionary = [[NSMutableDictionary alloc] init];
    date_filter_start_date = [[NSString alloc] init];
    date_filter_end_date= [[NSString alloc] init];
    
    
    self.navigationItem.title = @"SETTINGS";

    
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor],UITextAttributeTextColor,
                                                                     [UIColor whiteColor],UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
                                                                     UITextAttributeTextShadowOffset ,
                                                                     [UIFont boldSystemFontOfSize:19.0f],UITextAttributeFont  , nil]];
    
    
    minimised_section_array = [[NSMutableArray alloc] init];
    [minimised_section_array addObject:[NSNumber numberWithInt:1000]];
    [minimised_section_array addObject:[NSNumber numberWithInt:1001]];
    [minimised_section_array addObject:[NSNumber numberWithInt:1002]];
    [minimised_section_array addObject:[NSNumber numberWithInt:1003]];
    
    propertyArray = [[NSMutableArray alloc]initWithObjects:@"Property",@"Property Agent",nil];
    customizeArray = [[NSMutableArray alloc]initWithObjects:@"Property Type",@"Income Type",@"Expense Type",@"Reminder Type",nil];
                     
                     
    /*
    preferences_array = [[NSMutableArray alloc]initWithObjects:@"Currency",@"Date Filters",@"Password",nil];
    */
    
    preferences_array = [[NSMutableArray alloc]initWithObjects:@"Currency",@"Date Filters",@"Password",@"Mileage",nil];
    
    backupresoreArray= [[NSMutableArray alloc]initWithObjects:@"Backup",@"Restore",@"Reset the application",nil];
    help_feedback_array= [[NSMutableArray alloc]initWithObjects:@"FAQ's",@"Contact Us",@"Rate and Review",@"Tell a Friend",nil];
    
    detailArray = [[NSMutableArray alloc]init];
    detailDictionary = [[NSMutableDictionary alloc]init];
    
    
    if(appDelegate.isIPad)
    {
        
        footerView.center = CGPointMake(tblView.frame.size.width/2,tblView.frame.size.width/2);
    }
    
    tblView.tableFooterView = footerView;
    
    tblView.backgroundColor = [UIColor clearColor];
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:2];
    [imgView release];
    
    
    
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        button1.bounds = CGRectMake(0, 0, 39.0, 30.0);
        [button1 setBackgroundImage:[UIImage imageNamed:@"info_icon.png"] forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(about_clicked:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button1] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        button1.bounds = CGRectMake(0, 0, 39.0, 30.0);
        [button1 setBackgroundImage:[UIImage imageNamed:@"info_icon.png"] forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(about_clicked:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem=[[[UIBarButtonItem alloc] initWithCustomView:button1] autorelease];
        
    }

    
    
    /*
     Start:
     Sun:0004
     Setting custom image in navigation bar..
     */
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0)
    {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
    }
    
    /*
     Ends:
     Sun:0004
     Setting custom image in navigation bar..
     */
    
    ActivityView = [[UIView alloc]initWithFrame:CGRectMake(160,240,20,20)];
    [self.view addSubview:ActivityView];
    

    
}


- (void)queryDidFinishGatheringForBackup:(NSNotification *)notification
{
    
    query_for_backup  = [notification object];
    [query_for_backup  disableUpdates];
    [query_for_backup  stopQuery];
    
    [self loadDataForBackupDate:query_for_backup ];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NSMetadataQueryDidFinishGatheringNotification
                                                  object:query_for_backup ];
    
    query_for_backup  = nil;
    
}

- (void)loadDataForBackupDate:(NSMetadataQuery *)query_for_backup_local
{
    
    
    BOOL isFileFound = FALSE;
    
    int totalResult = [[query_for_backup_local  results] count];
    
    nslog(@"\n totalResult = %d",[[query_for_backup_local  results] count]);
    int loopCount = 0;
    for (NSMetadataItem *item in [query_for_backup_local  results])
    {
        loopCount ++;
        nslog(@"\n loopCount = %d",loopCount);
        
        /**
        isFileFound = TRUE;
        */
        
        
        NSURL *url = [item valueForAttribute:NSMetadataItemURLKey];
        nslog(@"\n url = %@",url);
        nslog(@"\n data = %@ ",[NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil]);
        
         Note *doc = [[Note alloc] initWithFileURL:url];
         [doc openWithCompletionHandler:^(BOOL success)
         {
             if (success)
             {
                 nslog(@"\n data content = %@",doc.noteContent);
        
                 icloud_backupdate =[NSString stringWithFormat:@"%@",doc.noteContent];
                 [icloud_backupdate retain];
                 [detailDictionary setObject:[NSString stringWithFormat:@"%@",[appDelegate convert_common_date_to_region_date:icloud_backupdate]] forKey:@"BACKUPDATE"];
                 [tblView reloadData];
             }
             else
             {
                 nslog(@"\n url = %@ \n",url);
             }
             
         }];

        
        
        
        //nslog(@"\n data = %@ ",[NSString stringWithContentsOfURL:url]);
        
        
        
        
    }
    
    if(totalResult >0)
    {
        //[tblView reloadData];
    }
}





#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

-(IBAction)about_clicked:(id)sender
{
    
    AboutViewController *obj_about;
    
    
    if([UIApplication sharedApplication].statusBarOrientation == 3  )
    {
        appDelegate.currentOrientation = 3;
    }
    else if([UIApplication sharedApplication].statusBarOrientation == 4)
    {
                appDelegate.currentOrientation = 4;
    }
    
    if(appDelegate.isIPad)
    {
        obj_about = [[AboutViewController alloc] initWithNibName:@"AboutViewController_iPad" bundle:nil];
    }
    else
    {
        obj_about = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
    }
    
    
    
    
    
    UINavigationController *cntrol = [[UINavigationController alloc] initWithRootViewController:obj_about];
    
    cntrol.navigationBar.translucent = NO;
    [cntrol.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    [cntrol.navigationBar setTranslucent:FALSE];
    obj_about.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:cntrol animated:YES];
    [cntrol release];
    [obj_about release];
    
    
    
}



-(void)viewWillDisappear:(BOOL)animated
{
    
    
    
    //[appDelegate.bottomView removeFromSuperview];
    
    
    /*
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];
     */
    
    is_view_will_appear_called = FALSE;
    [super viewWillDisappear:animated]; 
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if(is_view_will_appear_called)
    {
        return;
    }
    is_view_will_appear_called = TRUE;
    [super viewWillAppear:YES];
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    //[tblView setContentOffset:CGPointMake(0, 0) animated:NO];
    
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];
    
    //[self.view addSubview:appDelegate.bottomView];
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];

    
    
    
    //code of viewDidAppear
    
    /*
     SUN:004
     When user first time comes,we need to redirect him to addpropertypage.So need to manage viewcontroller stacks in navigation controller..
     
     */
    
    if(appDelegate.isIPad)
    {
        
        if (!appDelegate.isFirst)
        {
            
            appDelegate.isFirst = TRUE;
            
            propertyViewController *propertyView = [[propertyViewController alloc]initWithNibName:@"propertyViewController" bundle:nil];
            
            appDelegate.isProperty = -1;
            
            if([appDelegate.propertyDetailArray count]==0)
            {
                [appDelegate selectPropertyDetail];
            }
            
            
            
            NSMutableArray*viewControllerArray = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]] ;
            
            [viewControllerArray insertObject:propertyView atIndex:[viewControllerArray count]];
            
            
            
            
            self.navigationController.viewControllers =  [NSArray arrayWithArray:viewControllerArray];
            nslog(@"\n viewControllerArray = %@",self.navigationController.viewControllers);
            AddPropertyDetailViewController *addProperty = [[AddPropertyDetailViewController alloc]initWithNibName:@"AddPropertyDetailViewController" bundle:nil];
            [self.navigationController pushViewController:addProperty animated:NO];
            nslog(@"\n navigationController = %@",self.navigationController.viewControllers);
            
            [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"isFirst"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        else
        {
            /*
             welcome_screen_view.hidden = TRUE;
             welcome_image_view.hidden = TRUE;
             close_welcome_screen_button.hidden =TRUE;
             */
        }
        
        
    }
    
    
    
    
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    
    
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    
    if ([self getPassCodeStatus]==1)
    {
        isPasswordSet = TRUE;
    }
    else
    {
        isPasswordSet = FALSE;
    }
    
    
    NSString *reqSysVer = @"5.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    nslog(@"\n currSysVer = %@",currSysVer);
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
    {
        isIOS5 = YES;
        nslog(@"\n c1");
    }
    else
    {
        isIOS5 = NO;
        nslog(@"\n c2");
    }
    
    
    
    [detailArray removeAllObjects];
    
    
    
    /*
     Date:15-06-2013
     Sun:004*/
    
    if([appDelegate.incomeTypeDBArray count]==0)
    {
        [appDelegate selectIncomeType];
    }
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
        [appDelegate selectCurrencyIndex];
    }
    if([appDelegate.propertyDetailArray count]==0)
    {
        [appDelegate selectPropertyDetail];
    }
    if([appDelegate.agentArray count]==0)
    {
        [appDelegate selectAgentType];
    }
    if([appDelegate.reminderTypeArray count]==0)
    {
        [appDelegate selectReminderType];
    }
    if([appDelegate.propertyTypeArray count]==0)
    {
        [appDelegate selectPropertyType];
    }
    
    
    
    
    
    
    
    
    
    
    data_dictionary = [NSMutableDictionary dictionaryWithDictionary:[appDelegate select_default_date_filter]] ;
    
    //[data_dictionary retain];//as it is getting autorelease object...
    nslog(@"\n data_dictionary = %@",data_dictionary);
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *yearComponent = [gregorian components:NSYearCalendarUnit fromDate:[NSDate date]];
    
    /**
     NSDateComponents *monthComponent = [gregorian components:NSMonthCalendarUnit fromDate:[NSDate date]];
     //[yearComponent setMonth:[monthComponent month]];
     */
    
    [yearComponent setMonth:1];
    
    [yearComponent setDay:1];
    
    /**
     NSInteger month = [yearComponent month];
     nslog(@"\n month = %d",month);
     */
    
    NSDate *firstDateOfyear = [gregorian dateFromComponents:yearComponent];
    
    //NSString *yearString = [df stringFromDate:firstDateOfyear];
    /**
     NSString *yearString = [appDelegate.regionDateFormatter stringFromDate:firstDateOfyear];
     nslog(@"\n yearString = %@",yearString);
     */
    
    NSDate *current = [NSDate date];
    
    /**
     NSString *currentDate = [appDelegate.regionDateFormatter stringFromDate:current];
     */
    
    [gregorian release];
    
    
    
    
    /*
     Starts:  SUN:0004
     Date:05-03-2013
     For date filter conditions...
     
     */
    
    
    
    
    
    if([[data_dictionary objectForKey:@"1000"] length]==0)
    {
        [data_dictionary setObject:[appDelegate.regionDateFormatter stringFromDate:firstDateOfyear] forKey:@"1000"];
    }
    
    if([[data_dictionary objectForKey:@"1001"] length]==0)
    {
        [data_dictionary setObject:[appDelegate.regionDateFormatter stringFromDate:current] forKey:@"1001"];
    }
    
    
    date_filter_start_date = [NSString stringWithFormat:@"%@",[data_dictionary objectForKey:@"1000"]];
    date_filter_end_date = [NSString stringWithFormat:@"%@",[data_dictionary objectForKey:@"1001"]] ;
    
    [date_filter_start_date retain];
    [date_filter_end_date retain];
    
    
    /*
     Ends: SUN:0004
     
     Date:05-03-2013
     For date filter conditions...
     
     */
    
    
    
    [df release];
    
    
    
    
    
    if ([appDelegate.propertyTypeArray count]>0)
    {
        [detailArray addObject:[[appDelegate.propertyTypeArray objectAtIndex:0]valueForKey:@"propertyType"]];
        [detailDictionary setObject:[[appDelegate.propertyTypeArray objectAtIndex:0]valueForKey:@"propertyType"] forKey:@"propertyType"];
    }
    else
    {
        [detailArray addObject:@"Property Type"];
        [detailDictionary setObject:@"Property Type" forKey:@"propertyType"];
        
    }
    
    
    if ([appDelegate.propertyDetailArray count]>0)
    {
        [detailArray addObject:[[appDelegate.propertyDetailArray objectAtIndex:0]valueForKey:@"0"]];
        [detailDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:0]valueForKey:@"0"] forKey:@"Property"];
    }
    else
    {
        [detailArray addObject:@"Property"];
        [detailDictionary setObject:@"Property" forKey:@"Property"];
    }
    
    
    
    
    if ([appDelegate.agentArray count]>0)
    {
        [detailArray addObject:[[appDelegate.agentArray objectAtIndex:0]valueForKey:@"0"]];
        [detailDictionary setObject:[[appDelegate.agentArray objectAtIndex:0]valueForKey:@"0"] forKey:@"Agent"];
    }
    else
    {
        [detailArray addObject:@"Agent"];
        [detailDictionary setObject:@"Agent" forKey:@"Agent"];
    }
    
    
    if ([appDelegate.incomeTypeDBArray count]>0)
    {
        [detailArray addObject:[[appDelegate.incomeTypeDBArray objectAtIndex:0]valueForKey:@"incomeType"]];
        [detailDictionary setObject:[[appDelegate.incomeTypeDBArray objectAtIndex:0]valueForKey:@"incomeType"] forKey:@"incomeType"];
    }
    else
    {
        [detailArray addObject:@"Income Type"];
        [detailDictionary setObject:@"Income Type" forKey:@"incomeType"];
        
    }
    
    if([appDelegate.expenseTypeArray count]==0)
    {
        [appDelegate selectExpenseType];
    }
    
    
    
    if ([appDelegate.expenseTypeArray count]>0)
    {
        [detailArray addObject:[[appDelegate.expenseTypeArray objectAtIndex:0]valueForKey:@"expenseType"]];
        [detailDictionary setObject:[[appDelegate.expenseTypeArray objectAtIndex:0]valueForKey:@"expenseType"] forKey:@"expenseType"];
        
    }
    else
    {
        [detailArray addObject:@"Expense Type"];
        [detailDictionary setObject:@"Expense Type" forKey:@"expenseType"];
    }
    if ([appDelegate.reminderTypeArray count]>0)
    {
        [detailArray addObject:[[appDelegate.reminderTypeArray objectAtIndex:0]valueForKey:@"reminderType"]];
        [detailDictionary setObject:[[appDelegate.reminderTypeArray objectAtIndex:0]valueForKey:@"reminderType"] forKey:@"reminderType"];
    }
    else
    {
        [detailArray addObject:@"Reminder Type"];
        [detailDictionary setObject:@"Reminder Type" forKey:@"reminderType"];
    }
    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDate *dt;
    /*
     NSUbiquitousKeyValueStore *keyStore = [[NSUbiquitousKeyValueStore alloc] init];
     NSString *lastBackupDate = [keyStore stringForKey:@"BACKUPDATE"];
     nslog(@"\n lastBackupDate = %@",lastBackupDate);
     
     
     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
     [formatter setDateFormat:@"yyyy-MM-dd"];
     
     NSDate *dt = [formatter dateFromString:lastBackupDate];
     
     lastBackupDate = [appDelegate.regionDateFormatter stringFromDate:dt];
     nslog(@"\n lastBackUpdate in success = %@",lastBackupDate);
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
     
     [prefs setObject:lastBackupDate forKey:@"BACKUPDATE"];
     if(lastBackupDate == nil)
     {
     [detailDictionary setObject:@"" forKey:@"BACKUPDATE"];
     }
     else
     {
     NSDate*backUpDate = [appDelegate.regionDateFormatter dateFromString:lastBackupDate];
     lastBackupDate = [appDelegate.regionDateFormatter stringFromDate:backUpDate];
     [detailDictionary setObject:lastBackupDate forKey:@"BACKUPDATE"];
     }
     
     [keyStore release];
     */
    
    
   /*Backup date...before we get data from icloud...*/
   
    
    NSString *lastBackupDate = [prefs stringForKey:@"BACKUPDATE"];
    
    dt = [formatter dateFromString:lastBackupDate];
    lastBackupDate = [appDelegate.regionDateFormatter stringFromDate:dt];
    
    nslog(@"\n lastBackupDate = %@",lastBackupDate);
    if(lastBackupDate == nil)
    {
        [detailDictionary setObject:@"" forKey:@"BACKUPDATE"];
    }
    else
    {
        NSDate*backupDate = [appDelegate.regionDateFormatter dateFromString:lastBackupDate];
        lastBackupDate = [appDelegate.regionDateFormatter stringFromDate:backupDate];
        [detailDictionary setObject:lastBackupDate forKey:@"BACKUPDATE"];
    }

    
    
    /*Backup date...before we get data from icloud...*/
    
    
    
    NSString *lastRestoreDate = [prefs stringForKey:@"RESTOREDATE"];
    
    dt = [formatter dateFromString:lastRestoreDate];
    lastRestoreDate = [appDelegate.regionDateFormatter stringFromDate:dt];
    
    nslog(@"\n lastRestoreDate = %@",lastRestoreDate);
    if(lastRestoreDate == nil)
    {
        [detailDictionary setObject:@"" forKey:@"RESTOREDATE"];
    }
    else
    {
        NSDate*restoreDate = [appDelegate.regionDateFormatter dateFromString:lastRestoreDate];
        lastRestoreDate = [appDelegate.regionDateFormatter stringFromDate:restoreDate];
        [detailDictionary setObject:lastRestoreDate forKey:@"RESTOREDATE"];
    }
    
    [formatter release];
    
    [tblView reloadData];
    
    if(appDelegate.isIPad)
    {
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:2];
    }
    
    
    
    
    /*Activity Indicator*/
    
   // ActivityView = [[UIView alloc]initWithFrame:CGRectMake(160,240,20,20)];
    ActivityView.center = CGPointMake(160,240);
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
        {
            ActivityView.center = CGPointMake(384,512);
        }
        else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
        {
            ActivityView.center = CGPointMake(512,384);
        }
    }
    
    
    
    [ActivityView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.0]];
    ActivityView.hidden=NO;
    
    appDelegate.isTakingBackupFromiCloud = YES;
    
    
    //mainActivityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0,0,20,20)];
    mainActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    mainActivityIndicator.frame = CGRectMake(0,0,20,20);
    [mainActivityIndicator setHidesWhenStopped:YES];
    [ActivityView addSubview:mainActivityIndicator];
    mainActivityIndicator.activityIndicatorViewStyle =UIActivityIndicatorViewStyleGray;
    [mainActivityIndicator startAnimating];
    
    UILabel *lblLoading =[[UILabel alloc]initWithFrame:CGRectMake(35,10,80,20)];
    [lblLoading setText:@""];
    [lblLoading setTextAlignment:UITextAlignmentLeft];
    [lblLoading setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    lblLoading.font=[UIFont boldSystemFontOfSize:14];
    [lblLoading setTextColor:[UIColor whiteColor]];
    [lblLoading setBackgroundColor:[UIColor clearColor]];
    //[ActivityView addSubview:lblLoading];
    [lblLoading release];
    
    ActivityView.hidden = YES;
    appDelegate.isTakingBackupFromiCloud = NO;
    
    /*Activity Indicator*/

    
    
    /*TODO: - Starts: CHECKING IF Icloud backup is already taken...*/
    
    
    NSURL *ubiq_for_backup = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    
    if (ubiq_for_backup)
    {
        
        query_for_backup  = [[NSMetadataQuery alloc] init];
        [query_for_backup  setSearchScopes:[NSArray arrayWithObject:NSMetadataQueryUbiquitousDocumentsScope]];
        NSPredicate *pred = [NSPredicate predicateWithFormat: @"%K like 'icloud_backup_date*'", NSMetadataItemFSNameKey];
        [query_for_backup  setPredicate:pred];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(queryDidFinishGatheringForBackup:)
                                                     name:NSMetadataQueryDidFinishGatheringNotification
                                                   object:query_for_backup ];
        
        [query_for_backup  startQuery];
        
    }
    else
    {
        nslog(@"No iCloud access");
    }
    
    /*TODO: -  Ends: Checking IF Icloud backup is already taken...*/

    
    
        
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    
}


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if(appDelegate.isIPad)
    {
        
        if(toInterfaceOrientation == 1 ||toInterfaceOrientation == 2 )
        {
            self.view.frame = CGRectMake(0,0,768,1024);
            footerView.center = CGPointMake(600,tblView.frame.size.width/2);            
            tblView.tableFooterView = footerView;
            
        }
        else if(toInterfaceOrientation == 3 ||toInterfaceOrientation == 4 )
        {
            self.view.frame = CGRectMake(0,0,1024,768);
            footerView.center = CGPointMake(850,tblView.frame.size.width/2);            
            tblView.tableFooterView = footerView;
            
        }
       nslog(@"tblView.frame.size.width = %f",tblView.frame.size.width);
       
    }

}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [tblView reloadData];
    
    

    
    if(appDelegate.isIPad)
    {
       
        
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
        {
            ActivityView.center = CGPointMake(384,512);
        }
        else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
        {
            ActivityView.center = CGPointMake(512,384);
        }

        
        [appDelegate manageViewControllerHeight];
    }

}


/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

#pragma mark - 
#pragma mark restore default method

-(IBAction)restore_clicked
{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"This will delete all the data and application will return to its newly installed state. Would you like to proceed?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alertView show];
    [alertView release];
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    nslog(@"\n alertview tag = %d",alertView.tag);
    if(alertView.tag != 5001 && alertView.tag != 9001)
    {
        if (buttonIndex == 1)
        {
            nslog(@"button index = 1");
            [appDelegate restoreAgent];
            [appDelegate restoreExpenseType];
            [appDelegate restoreIncomeType];
            [appDelegate restoreProperty];
            [appDelegate restorePropertyType];
            [appDelegate restoreReminderType];
            [appDelegate restoreReminder];
            [appDelegate restoreIncomeExpense];
            [appDelegate insertIncomeType];
            [appDelegate insertExpenseType];
            [appDelegate insertPropertyType];
            [appDelegate insertReminderType];
            
            [self viewWillAppear:YES];
        }

    }
    // TODO: Load document...
    else if(alertView.tag == 9001)
    {
        if (buttonIndex == 1)
        {
            
            
            if([[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"date_filter_type"]!=nil)
            {
                
               appDelegate.date_filter_type =[[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"date_filter_type"] intValue];
            }
            
            
            if([[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"milage_unit"]!=nil)
            {
                [appDelegate.prefs setObject:[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"milage_unit"] forKey:@"milage_unit"];
            }
            
            
            
            if([[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"is_milage_set"]!=nil)
            {
                [appDelegate.prefs setObject:[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"is_milage_set"] forKey:@"is_milage_set"];
                appDelegate.is_milege_on = [[appDelegate.prefs objectForKey:@"is_milage_set"] boolValue];
            }
            
            
            if([[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"PasscodeOn"]!=nil)
            {
                [appDelegate.prefs setInteger:[[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"PasscodeOn"] intValue] forKey:@"PasscodeOn"];
                
            }

            if([[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"PassWordValue"]!=nil)
            {
                
                [appDelegate.prefs setObject:[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"PassWordValue"]  forKey:@"PassWordValue"];
                
            }
            if([[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"PassWordValue"]!=nil)
            {
                
                [appDelegate.prefs setObject:[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"PassWordValueHint"]  forKey:@"PassWordValueHint"];
                
            }
            
            if ([self getPassCodeStatus]==1)
            {
                isPasswordSet = TRUE;
            }
            else
            {
                isPasswordSet = FALSE;
            }

            
            nslog(@"\n Restore..");
            NSURL *ubiq = [[NSFileManager defaultManager] 
                           URLForUbiquityContainerIdentifier:nil];
            if (ubiq) 
            {
                nslog(@"iCloud access at %@", ubiq);
                
                ActivityView.hidden = NO;
                [self.view setUserInteractionEnabled:FALSE];
                appDelegate.isTakingBackupFromiCloud = YES;
                [self loadNotes];
            } 
            else 
            {
                nslog(@"No iCloud access");
                UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"iCloud is not accessible." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                [alertView release];
            }

        }
    }
    
}

#pragma mark -
#pragma mark tableview method


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    // Return the number of sections.
    return 5;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 44.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 48.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView*sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0,320,44)] autorelease];
    
    
    
    
    UIImageView*cell_section_imageView = [[[UIImageView alloc] init] autorelease];
    
    if(appDelegate.isIPad)
    {
        


        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            
        
        {
            cell_section_imageView.frame = CGRectMake(0, 0,768,44);
            //cell_section_imageView.image = [UIImage imageNamed:@"section_header_bg_port.png"];
            cell_section_imageView.image = [UIImage imageNamed:@"section_header_bg_land.png"];

        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            cell_section_imageView.frame = CGRectMake(0, 0,1024,44);
            cell_section_imageView.image = [UIImage imageNamed:@"section_header_bg_land.png"];
        }
        
        
        
    }
    else
    {
        cell_section_imageView.frame = CGRectMake(0, 0,320,44);
        cell_section_imageView.image = [UIImage imageNamed:@"section_header_bg.png"];

    }
    
    
    
    
    [sectionView addSubview:cell_section_imageView];
    
    
    UILabel*cell_section_title_label = [[[UILabel alloc] init] autorelease];
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            cell_section_title_label.frame = CGRectMake(10,0,768,44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            cell_section_title_label.frame = CGRectMake(10,0,1024,44);
        }
    }
    else
    {
        cell_section_title_label.frame = CGRectMake(10,0,320,44);
    }
    
    
    
    
    
    
    cell_section_title_label.backgroundColor = [UIColor clearColor];
    NSString*title_string = @"";
    switch (section)
    {
        case 0:
        {
            title_string = @"Property Information";
            break;
        }
        case 1:
        {
            title_string = @"Customise";
            break;
        }
        case 2:
        {
            title_string = @"Preferences";
            break;
        }
        case 3:
        {
            title_string = @"Backup & Restore";
            break;
        }
        case 4:
        {
            title_string = @"Help / Feedback";
            break;
        }
        default:
        {
            break;
        }
    
    }
    
    cell_section_title_label.textColor = [UIColor colorWithRed:56.0/255.0 green:49.0/255.0 blue:49.0/255.0 alpha:1.0];
    cell_section_title_label.text = title_string;
    [sectionView addSubview:cell_section_title_label];
    
    
    
    
    UIButton*bg_button = [UIButton buttonWithType:UIButtonTypeCustom];
    bg_button.tag = section+1000;
    [bg_button addTarget:self action:@selector(showHideSectionByBgImage:) forControlEvents:UIControlEventTouchUpInside];
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            bg_button.frame = CGRectMake(10,0,768,44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            bg_button.frame = CGRectMake(10,0,1024,44);
        }

    }
    else
    {
        bg_button.frame = CGRectMake(10,0,320,44);
    }
    
    [sectionView addSubview:bg_button];
    
    UIButton*arrowButon = [UIButton buttonWithType:UIButtonTypeCustom];
    arrowButon.tag = section+1000;
    
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
             arrowButon.frame = CGRectMake(726,10,27,26);;
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            arrowButon.frame = CGRectMake(982,10,27,26);
        }
        
       
    }
    else
    {
            arrowButon.frame = CGRectMake(278,10,27,26);
    }
    
    
    if([minimised_section_array indexOfObject:[NSNumber numberWithInt:arrowButon.tag]]==NSNotFound)
    {
        [arrowButon setImage:[UIImage imageNamed:@"arrow_down.png"] forState:UIControlStateNormal];
    }
    else
    {
        [arrowButon setImage:[UIImage imageNamed:@"arrow_open.png"] forState:UIControlStateNormal];
    }
    [arrowButon addTarget:self action:@selector(showHideSection:) forControlEvents:UIControlEventTouchUpInside];
    
    [sectionView addSubview:arrowButon];
    //sectionView.backgroundColor = [UIColor redColor];
    return sectionView;
}

/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 
{
    if (section == 0)
    {
        return @"Property Settings";
    }
    else if (section == 1)
    {
        return @"Customization";
    }
    else if(section == 2)
    {
        return @"Preferences";
    }

    else if(section == 3)
    {
        return @"Backup & Restore";
    }

    else if(section == 4)
    {
        return @"Help / Feedback";
    }

    return nil;
}
*/




//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 30;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
   
    
    if([minimised_section_array indexOfObject:[NSNumber numberWithInt:section+1000]]!=NSNotFound)
    {
        return 0;
    }

    
    
    if (section == 0)
    {
        return [propertyArray count];
    }
    else if (section == 1)
    {
        
        return [customizeArray count];
    }
    else if (section == 2)
    {
        
        return [preferences_array count];
        
        
    }
    else if (section == 3)
    {
        
        return [backupresoreArray count];
    }

    else if (section == 4)
    {
        
        return [help_feedback_array count];
    }
    
    
    
    
    
    return 0;
    
}





// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (cell == nil) 
    if (1) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
    
    
    UIImageView*bg_imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,320,44)];
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
        {
            bg_imageView.frame = CGRectMake(0, 0,768, 44);
             bg_imageView.image = [UIImage imageNamed:@"settings_cell_bg.png"];
        }
        else
        {
            bg_imageView.frame = CGRectMake(0, 0,1024, 44);
             bg_imageView.image = [UIImage imageNamed:@"settings_cell_bg.png"];
        }
        
        
    }
    else
    {
        bg_imageView.image = [UIImage imageNamed:@"settings_cell_bg.png"];
    }
    
    
    
    [cell.contentView addSubview:bg_imageView];
    [bg_imageView release];
    
    cell.detailTextLabel.text = @"";
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //cell.imageView.image.size = CGSizeMake(22,22);
    
    
    
    cell.detailTextLabel.font = [UIFont systemFontOfSize:15.0f];
    
    cell.detailTextLabel.textColor = [UIColor colorWithRed:3.0/255.0f green:80.0f/255.0f blue:103.0f/255.0f alpha:1.0f];
    
    
    cell.textLabel.font =[UIFont systemFontOfSize:15.0f];
    
    
   

    
    

    if (indexPath.section == 0)
    {
       // cell.frame.size.height
        
        cell.textLabel.text = [propertyArray objectAtIndex:indexPath.row];
        //cell.detailTextLabel.text = [detailArray objectAtIndex:indexPath.row];
		if(indexPath.row == 0)
		{
			cell.imageView.image = [UIImage imageNamed:@"property_icon.png"];
            cell.detailTextLabel.text = [detailDictionary objectForKey:@"Property"];
            
		}
		else if(indexPath.row == 1)
		{
            cell.imageView.image = [UIImage imageNamed:@"property_agent_icon.png"];
            cell.detailTextLabel.text = [detailDictionary objectForKey:@"Agent"];
			
		}
		
		
        
    }
    else if (indexPath.section == 1)
    {
        cell.textLabel.text = [customizeArray objectAtIndex:indexPath.row];
        
        
        
        if(indexPath.row == 0)
		{
			cell.imageView.image = [UIImage imageNamed:@"property_type_icon.png"];
            cell.detailTextLabel.text = [detailDictionary objectForKey:@"propertyType"];
		}
		else if(indexPath.row == 1)
		{
			cell.imageView.image = [UIImage imageNamed:@"income_type_icon.png"];
            cell.detailTextLabel.text = [detailDictionary objectForKey:@"incomeType"];
		}
		else if(indexPath.row == 2)
		{
			cell.imageView.image = [UIImage imageNamed:@"exp_type_icon.png"];
            cell.detailTextLabel.text = [detailDictionary objectForKey:@"expenseType"];
		}
        
		else if(indexPath.row == 3)
		{
			cell.imageView.image = [UIImage imageNamed:@"reminder_type_icon.png"];
            cell.detailTextLabel.text = [detailDictionary objectForKey:@"reminderType"];
		}
       
        
        
        
				        
    }

    else if (indexPath.section == 2)
    {
        cell.textLabel.text = [preferences_array objectAtIndex:indexPath.row];
        
        if(1)
        {
            
            
            if(indexPath.row == 0)
            {
                cell.detailTextLabel.text = appDelegate.curCode;
                cell.imageView.image = [UIImage imageNamed:@"currency_type_icon.png"];
                
            }

            
            if(indexPath.row == 1)
            {
                cell.imageView.image = [UIImage imageNamed:@"date_fiters_icon.png"];
                
                
                if(appDelegate.date_filter_type==0)
                {
                    cell.detailTextLabel.text = @"MTD";
                }
                else if(appDelegate.date_filter_type==1)
                {
                    cell.detailTextLabel.text = @"YTD";
                }
                else if(appDelegate.date_filter_type==2)
                {
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@ ",date_filter_start_date,date_filter_end_date];
                }
                
                
               
                
            }
            else if(indexPath.row == 2)
            {
                cell.imageView.image = [UIImage imageNamed:@"password_icon.png"];
                if(isPasswordSet)
                {
                    cell.detailTextLabel.text = @"ON";
                }
                else
                {
                    cell.detailTextLabel.text = @"OFF";
                }
            }
            else if(indexPath.row == 3)
            {
                
                //cell.accessoryType = UITableViewCellAccessoryNone;
                cell.imageView.image = [UIImage imageNamed:@"mileage_icon.png"];
                
                
                 
                   
                
                
                if(appDelegate.is_milege_on)
                {
                    
                     cell.detailTextLabel.text = @"ON";
                }
                else
                {
                  
                     cell.detailTextLabel.text = @"OFF";
                }

                
                
                
                                
                
            }

            
            
          
        }
        
        
		        
    }

    else if (indexPath.section == 3)
    {
        cell.textLabel.text = [backupresoreArray objectAtIndex:indexPath.row];
        
        if(indexPath.row == 0)
        {
            cell.imageView.image = [UIImage imageNamed:@"backup_icon.png"];
            cell.detailTextLabel.text = [detailDictionary objectForKey:@"BACKUPDATE"];
            
        }
        else if(indexPath.row == 1)
        {
            cell.imageView.image = [UIImage imageNamed:@"restore_icon.png"];
            cell.detailTextLabel.text = [detailDictionary objectForKey:@"RESTOREDATE"];
            
        }
        else if(indexPath.row == 2)
        {
            cell.imageView.image = [UIImage imageNamed:@"reset_icon.png"];
            
        }
        
        
        
        
    }

    else if (indexPath.section == 4)
    {
        cell.textLabel.text = [help_feedback_array objectAtIndex:indexPath.row];
        
        
        
        if(indexPath.row == 0)
		{
			cell.imageView.image = [UIImage imageNamed:@"faq_asked_icon.png"];
            
		}
       else if(indexPath.row == 1)
		{
			cell.imageView.image = [UIImage imageNamed:@"contact_us_icon.png"];
            //What to do?
            
		}
		else if(indexPath.row == 2)
		{
			cell.imageView.image = [UIImage imageNamed:@"rate_us_icon.png"];
            
		}
        else if(indexPath.row == 3)
		{
			cell.imageView.image = [UIImage imageNamed:@"tell_a_friend.png"];
            
		}
        
    }

    
    
    cell.textLabel.font = [UIFont systemFontOfSize:15.0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}



-(void)loadFAQ:(id)sender

{
    
}
#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    appDelegate.isFromIncomeExpenseView = 1;
    
    self.navigationItem.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil] autorelease];
    

    
    if (indexPath.section == 0)
    {
        
        if (indexPath.row == 0)
        {
            
            propertyViewController *propertyView = [[propertyViewController alloc]initWithNibName:@"propertyViewController" bundle:nil];
            
            [self.navigationController pushViewController:propertyView animated:YES];
            [propertyView release];
            
            
        }
        else if (indexPath.row == 1)
        {
            appDelegate.isFromPropertyAgent = FALSE;
            agentInformationViewController *agentInfo = [[agentInformationViewController alloc]initWithNibName:@"agentInformationViewController" bundle:nil];
            [self.navigationController pushViewController:agentInfo animated:YES];
            [agentInfo release];
        }
        
        

        
    }
    
    
    
    
    else if (indexPath.section == 1)
    {
        
        
        
        
        if (indexPath.row == 0)
        {
            propertyTypeViewController *propertyType = [[propertyTypeViewController alloc]initWithNibName:@"propertyTypeViewController" bundle:nil];
            appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
            appDelegate.fromProperty = FALSE;
            [self.navigationController pushViewController:propertyType animated:YES];
            [propertyType release];
        }
        else if (indexPath.row == 1)
        {
            
            
            [NSThread sleepForTimeInterval:.30];
            appDelegate.isFromIncomeExpenseView = 1;
            
            AddIncomeTypeViewController *addIncome = [[AddIncomeTypeViewController alloc]initWithNibName:NSStringFromClass([AddIncomeTypeViewController class]) bundle:nil];
            
            
            @try
            {
                [self.navigationController pushViewController:addIncome animated:YES];
            }
            @catch (NSException *exception)
            {
                nslog(@"\n push controller..%@",self.navigationController.viewControllers);
                
                NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray: self.navigationController.viewControllers];
                
                [allViewControllers removeObjectAtIndex:([allViewControllers count]-1)];
                self.navigationController.viewControllers = allViewControllers;
                
                [self viewWillAppear:YES];
            }
            @finally
            {
                
            }
            
            [addIncome release];
        }
        else if (indexPath.row == 2)
        {
            AddExpenseTypeViewController *addExpense = [[AddExpenseTypeViewController alloc]initWithNibName:@"AddExpenseTypeViewController" bundle:nil];
            [self.navigationController pushViewController:addExpense animated:YES];
            [addExpense release];
        }
        else if (indexPath.row == 3)
        {
            AddReminderTypeViewController *addReminderType = [[AddReminderTypeViewController alloc]initWithNibName:@"AddReminderTypeViewController" bundle:nil];
            [self.navigationController pushViewController:addReminderType animated:YES];
            [addReminderType release];
        }
        
        
        
    }
    else if (indexPath.section == 2)
    {

        
        
        
        if (indexPath.row == 0)
        {
            currencyViewController *currecyView = [[currencyViewController alloc]initWithNibName:@"currencyViewController" bundle:nil];
            [self.navigationController pushViewController:currecyView animated:YES];
            [currecyView release];
            
        }
        else if(indexPath.row == 1)
        {
            nslog(@"Date Filters...");
            
            DateFiltersViewController*viewController = [[DateFiltersViewController alloc] initWithNibName:@"DateFiltersViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:TRUE];
            [viewController release];
            
        }
        else if(indexPath.row == 2)
        {
            nslog(@"\n Password..");
            
            PasswrodOnOffViewController *passOnOffViewController = [[PasswrodOnOffViewController alloc]initWithNibName:@"PasswrodOnOffViewController" bundle:nil];
            [self.navigationController pushViewController:passOnOffViewController animated:YES];
            [passOnOffViewController release];
            
        }
        else if(indexPath.row == 3)
        {
            nslog(@"\n Password..");
            
            MilageViewController *viewController = [[MilageViewController alloc]initWithNibName:@"MilageViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
            [viewController release];
            
        }
        


        
    }
    
    else if (indexPath.section == 3)
    {
        
        
        
        if(indexPath.row == 0)
        {
            nslog(@"\n Backup..");
            
            if(!isIOS5)
            {
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Backup & Restore functionality is only available for IOS5 and above platforms." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                [alertView release];
                return;
            }
            
            
            NSUbiquitousKeyValueStore *kvStore = [NSUbiquitousKeyValueStore defaultStore];

            [kvStore setObject:[NSNumber numberWithInt:appDelegate.date_filter_type] forKey:@"date_filter_type"];
            [kvStore setObject:[appDelegate.prefs objectForKey:@"milage_unit"] forKey:@"milage_unit"];
            [kvStore setObject:[NSNumber numberWithBool:appDelegate.is_milege_on] forKey:@"is_milage_set"];
            [kvStore setObject:[NSNumber numberWithInt:[appDelegate.prefs integerForKey:@"PasscodeOn"]] forKey:@"PasscodeOn"];
            [kvStore setObject:[appDelegate.prefs objectForKey:@"PassWordValue"] forKey:@"PassWordValue"];
            [kvStore setObject:[appDelegate.prefs objectForKey:@"PassWordValueHint"] forKey:@"PassWordValueHint"];
            
            [kvStore synchronize];

            
            
            NSURL *ubiq = [[NSFileManager defaultManager]
                           URLForUbiquityContainerIdentifier:nil];
            if (ubiq)
            {
                nslog(@"iCloud access at %@", ubiq);
                
                
                
                
                
                ActivityView.hidden = NO;
                [self.view setUserInteractionEnabled:FALSE];
                appDelegate.isTakingBackupFromiCloud = YES;
                
                [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(addData) userInfo:nil repeats:NO];
                
                //[self addData];
                
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDir = [paths objectAtIndex:0];
                NSString *filePath = [documentsDir stringByAppendingPathComponent:@"PropertyDB.sqlite"];
                nslog(@"\n filePath = %@",filePath);
                
            }
            else
            {
                
                
                nslog(@"No iCloud access");
                UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"iCloud is not accessible." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                [alertView release];
                
            }
            
            
            
        }
        if(indexPath.row == 1)
        {
            
            if(!isIOS5)
            {
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Backup & Restore functionality is only available for IOS5 and above platforms." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                [alertView release];
                return;
            }
            
            NSURL *ubiq = [[NSFileManager defaultManager]
                           URLForUbiquityContainerIdentifier:nil];
            if (ubiq)
            {
                
                NSUbiquitousKeyValueStore *keyStore = [[NSUbiquitousKeyValueStore alloc] init];
                nslog(@"\n backupdate = %@",[keyStore stringForKey:@"BACKUPDATE"]);
                
                
               // nslog(@"\n icloud_backupdate = %@",icloud_backupdate);
                
                
                //if ([[keyStore stringForKey:@"BACKUPDATE"] length] != 0)
                if (icloud_backupdate != nil)
                {
                    if([detailDictionary objectForKey:@"BACKUPDATE"]== nil)
                    {
                        
                        
                        
                        //[detailDictionary setObject:[keyStore stringForKey:@"BACKUPDATE"] forKey:@"BACKUPDATE"];
                        
                        [detailDictionary setObject:[NSString stringWithFormat:@"%@",[appDelegate convert_common_date_to_region_date:icloud_backupdate]] forKey:@"BACKUPDATE"];
                        
                    }
                    
                    nslog(@"\n dt in keyStore = %@",[keyStore stringForKey:@"BACKUPDATE"]);
                    nslog(@"\n dt in detailDictionary = %@",detailDictionary);
                    
                    NSString*alertViewString = [NSString stringWithFormat:@"Property Log Book backup dated: %@ will be restored. Do you want to proceed?", [detailDictionary objectForKey:@"BACKUPDATE"] ];
                    
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:alertViewString delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                    alertView.tag = 9001;
                    [alertView show];
                    [alertView release];
                    
                }
                else
                {
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No iCloud backup found." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alertView show];
                    [alertView release];
                }
                
                [keyStore release];
            }
            else
            {
                nslog(@"No iCloud access");
                UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"iCloud is not accessible." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                [alertView release];
            }
            
            
            
        }
        if(indexPath.row == 2)
        {
            nslog(@"\n Reset to default setttings....");
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"This will delete all the data and application will return to its newly installed state. Would you like to proceed?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            [alertView show];
            [alertView release];
        }
        

        
    }
    else if (indexPath.section == 4)
    {
        
        if (indexPath.row == 0)
        {
            
            NSURL *url = [NSURL URLWithString:@"http://www.iapplab.com/propertylogbook_faq.pdf"];
            
            if (![[UIApplication sharedApplication] openURL:url])
            {
                nslog(@"%@%@",@"Failed to open url:",[url description]);
            }
        }
        
        else if (indexPath.row == 1)
        {
            nslog(@"\n Support");
            
            Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
            if (mailClass != nil)
            {
                
                emailRecepient = @"contactus@iapplab.com";
                // We must always check whether the current device is configured for sending emails
                if ([mailClass canSendMail])
                {
                    [self displayComposerSheetforsupport];
                }
                else
                {
                    [self launchMailAppOnDevice];
                }
            }
            else
            {
                [self launchMailAppOnDevice];
            }
            
            
        }

        
        
        if (indexPath.row == 2)
         {
             NSURL *url = [NSURL URLWithString:@"http://itunes.apple.com/us/app/property-log-book-manage-your/id482865530?ls=1&mt=8"];
             
             if (![[UIApplication sharedApplication] openURL:url])
             {
                 nslog(@"%@%@",@"Failed to open url:",[url description]);
             }

         }
        else if(indexPath.row == 3)
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Select" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"E-mail",@"SMS", nil];
            actionSheet.delegate = self;
            [actionSheet showFromTabBar:self.tabBarController.tabBar];
            [actionSheet release];
        }

        
        

    }
}

#pragma mark - icloud Methods

#pragma mark - addData 
-(void)addData
{
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    NSString *docsDir = documentsDir;
	NSFileManager *localFileManager=[[NSFileManager alloc] init];
	
	NSDirectoryEnumerator *dirEnum =[localFileManager enumeratorAtPath:docsDir];
	[localFileManager release];
    /**
    NSFileManager *fileManager = [NSFileManager defaultManager];
	*/
	
	/*
    NSString *fileName = @"PropertyLogBookBackup";
    */
    
    
    NSString *file;
    NSString*filePath;
	while (file = [dirEnum nextObject]) 
	{
        nslog(@"\n file = %@",[docsDir stringByAppendingPathComponent:file]);
        NSArray*originalFileNameArray = [file componentsSeparatedByString:@"/"];
        NSString*originalFileName = [originalFileNameArray objectAtIndex:([originalFileNameArray count]-1)];
        NSString*backUpFileName = [NSString stringWithFormat:@"PropertyLogBookBackup____%@",originalFileName];
        
        nslog(@"\n originalFileName = %@",originalFileName);
        filePath = [docsDir stringByAppendingPathComponent:file];
        NSURL *ubiq = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
        
        NSURL *ubiquitousPackage = [[ubiq URLByAppendingPathComponent:@"Documents"] 
                                    URLByAppendingPathComponent:backUpFileName];
        
        
        Note *doc = [[Note alloc] initWithFileURL:ubiquitousPackage];
        
        doc.noteDataContent = [NSData dataWithContentsOfFile:filePath];
        [doc saveToURL:[doc fileURL] forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            
            if(success) 
            {
                [self.notes addObject:doc];
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSDate *backUpDate = [NSDate date];
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd"];
                NSString *lastBackupDate = [formatter stringFromDate:backUpDate];
                nslog(@"\n lastBackUpdate in success = %@",lastBackupDate);
                [formatter release];
                icloud_backupdate = [NSString stringWithFormat:@"%@",lastBackupDate];
                
                /*
                [[CloudMe shared] uploadDocumentWithcontents:lastBackupDate result:^(BOOL success)
                 {
                     
                     nslog(@"\n uploaded to icloud successfully...");
                     
                 }];
                */
                
                
                [prefs setObject:lastBackupDate forKey:@"BACKUPDATE"];
                
                NSUbiquitousKeyValueStore *keyStore = [[NSUbiquitousKeyValueStore alloc] init];
                [keyStore setString:lastBackupDate forKey:@"BACKUPDATE"];
                [keyStore synchronize];
                [keyStore release];
                
                
                lastBackupDate = [appDelegate.regionDateFormatter stringFromDate:backUpDate];
                [detailDictionary setObject:lastBackupDate forKey:@"BACKUPDATE"];
                
                
                
                [tblView reloadData];
                
                
            }
            else
            {
                nslog(@"No iCloud access");
                UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"iCloud is not accessible." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                [alertView release];
                ActivityView.hidden = TRUE;
            }
            
        }];
    }
    
    ActivityView.hidden = YES;
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Property Log Book" message:@"Backup to iCloud completed successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
    [alertView release];
    
    [tblView reloadData];
    
    
    
    appDelegate.isTakingBackupFromiCloud = NO;
    
    [self.view setUserInteractionEnabled:YES];



    nslog(@"\n filePath = %@",filePath);
    
    
    
       
}



/*Starts:Checking if file exists at icloud

-(void)loadiCloudDataForValidation
{
    nslog(@"\n loadNotes in ListViewController");
    NSURL *ubiq = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    
    if (ubiq) 
    {
        
        self.query = [[NSMetadataQuery alloc] init];
        [self.query setSearchScopes:[NSArray arrayWithObject:NSMetadataQueryUbiquitousDocumentsScope]];
        NSPredicate *pred = [NSPredicate predicateWithFormat: @"%K like 'PropertyLogBookBackup____*'", NSMetadataItemFSNameKey];
        [self.query setPredicate:pred];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(queryDidFinishGatheringForValidation:) 
                                                     name:NSMetadataQueryDidFinishGatheringNotification 
                                                   object:self.query];
        
        [self.query startQuery];
        
    } else 
    {
        
        nslog(@"No iCloud access");
        
    }

}


- (void)queryDidFinishGatheringForValidation:(NSNotification *)notification 
{
    
    NSMetadataQuery *query = [notification object];
    [query disableUpdates];
    [query stopQuery];
    
    [self loadDataForValidation:query];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:NSMetadataQueryDidFinishGatheringNotification
                                                  object:query];
    
    self.query = nil;
    
}


- (void)loadDataForValidation:(NSMetadataQuery *)query 
{
    
    [self.notes removeAllObjects];
    
    
    int totalResult = [[query results] count];
    
    if(totalResult > 0)
    {
        
    }
    
    
    
} 
*/

/*Ends:Checking if file exists at icloud*/
-(void)loadNotes
{
    nslog(@"\n loadNotes in ListViewController");
    NSURL *ubiq = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    
    if (ubiq) 
    {
        
        self.query = [[[NSMetadataQuery alloc] init] autorelease];
        [self.query setSearchScopes:[NSArray arrayWithObject:NSMetadataQueryUbiquitousDocumentsScope]];
        NSPredicate *pred = [NSPredicate predicateWithFormat: @"%K like 'PropertyLogBookBackup____*'", NSMetadataItemFSNameKey];
        [self.query setPredicate:pred];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(queryDidFinishGathering:) 
                                                     name:NSMetadataQueryDidFinishGatheringNotification 
                                                   object:self.query];
        
        [self.query startQuery];
        
    }
    else
    {
        
        nslog(@"No iCloud access");
        
    }
    
}

- (void)queryDidFinishGathering:(NSNotification *)notification 
{
    
    NSMetadataQuery *query = [notification object];
    [query disableUpdates];
    [query stopQuery];
    
    [self loadData:query];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:NSMetadataQueryDidFinishGatheringNotification
                                                  object:query];
    
    self.query = nil;
    
}

- (void)loadData:(NSMetadataQuery *)query
{
    
    [self.notes removeAllObjects];
    BOOL isFileFound = FALSE;
      
    int totalResult = [[query results] count];
    
    nslog(@"\n totalResult = %d",[[query results] count]);
    
    int loopCount = 0;
    self.successLoopCount = 0;
    for (NSMetadataItem *item in [query results]) 
    {
        loopCount ++;
        nslog(@"\n loopCount = %d",loopCount);
        isFileFound = TRUE;
        NSURL *url = [item valueForAttribute:NSMetadataItemURLKey];
        nslog(@"\n url = %@",url);
        Note *doc = [[Note alloc] initWithFileURL:url];
        NSString*fileNameFromiCloud = [NSString stringWithFormat:@"%@",url];
        NSArray*originalFileNameArray = [fileNameFromiCloud componentsSeparatedByString:@"/"];
        
        NSString*originalFileName = [originalFileNameArray objectAtIndex:([originalFileNameArray count]-1)];
        nslog(@"\n originalFileName = %@",originalFileName);
        originalFileNameArray = [originalFileName componentsSeparatedByString:@"____"];
        originalFileName = [originalFileNameArray objectAtIndex:([originalFileNameArray count]-1)];
        nslog(@"\n final originalFileName = %@",originalFileName);
        
         [doc openWithCompletionHandler:^(BOOL success) 
         {
             if (success) 
             {
                 self.successLoopCount++;
                 [self.notes addObject:doc];
                 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                 NSString *documentsDir = [paths objectAtIndex:0];
                 //NSString *root = [documentsDir stringByAppendingPathComponent:@"PropertyDB.sqlite"];
                 NSString *root = [documentsDir stringByAppendingPathComponent:originalFileName];
                 nslog(@"\n root == %@",root);
                 [doc.noteDataContent writeToFile:root atomically:YES];
                 
                 nslog(@"\n self.successLoopCount in success = %d",self.successLoopCount);          
                 if(self.successLoopCount == totalResult)
                 {
                     
                     [detailArray removeAllObjects];

                     /*
                      
                      Sun:004
                      This is required as after backup we need to set default options again.*/
                      
                     
                     
                     [appDelegate createEditableCopyOfDatabaseIfNeeded];
                     [appDelegate selectIncomeType];
                     [appDelegate selectCurrencyIndex];
                     [appDelegate selectPropertyType];
                     [appDelegate selectPropertyDetail];
                     [appDelegate selectAgentType];
                     [appDelegate selectReminderType];
                     
                     /*
                      
                      Sun:004
                      This is required as after backup we need to set default options again.*/
                     
                     if ([appDelegate.propertyTypeArray count]>0)
                     {
                         [detailArray addObject:[[appDelegate.propertyTypeArray objectAtIndex:0]valueForKey:@"propertyType"]];
                         [detailDictionary setObject:[[appDelegate.propertyTypeArray objectAtIndex:0]valueForKey:@"propertyType"] forKey:@"propertyType"];
                     }
                     else
                     {
                         [detailArray addObject:@"Property Type"];
                         [detailDictionary setObject:@"Property Type" forKey:@"propertyType"];
                         
                     }
                     
                     
                     if ([appDelegate.propertyDetailArray count]>0)
                     {
                         [detailArray addObject:[[appDelegate.propertyDetailArray objectAtIndex:0]valueForKey:@"0"]];
                         [detailDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:0]valueForKey:@"0"] forKey:@"Property"];
                     }
                     else
                     {
                         [detailArray addObject:@"Property"];
                         [detailDictionary setObject:@"Property" forKey:@"Property"];
                     }
                     
                     
                     
                     
                     if ([appDelegate.agentArray count]>0)
                     {
                         [detailArray addObject:[[appDelegate.agentArray objectAtIndex:0]valueForKey:@"0"]];
                         [detailDictionary setObject:[[appDelegate.agentArray objectAtIndex:0]valueForKey:@"0"] forKey:@"Agent"];
                     }
                     else
                     {
                         [detailArray addObject:@"Agent"];
                         [detailDictionary setObject:@"Agent" forKey:@"Agent"];
                     }
                     
                     
                     if ([appDelegate.incomeTypeDBArray count]>0)
                     {
                         [detailArray addObject:[[appDelegate.incomeTypeDBArray objectAtIndex:0]valueForKey:@"incomeType"]];
                         [detailDictionary setObject:[[appDelegate.incomeTypeDBArray objectAtIndex:0]valueForKey:@"incomeType"] forKey:@"incomeType"];
                     }
                     else
                     {
                         [detailArray addObject:@"Income Type"];
                         [detailDictionary setObject:@"Income Type" forKey:@"incomeType"];
                         
                     }
                     
                     [appDelegate selectExpenseType];
                     
                     if ([appDelegate.expenseTypeArray count]>0)
                     {
                         [detailArray addObject:[[appDelegate.expenseTypeArray objectAtIndex:0]valueForKey:@"expenseType"]];
                         [detailDictionary setObject:[[appDelegate.expenseTypeArray objectAtIndex:0]valueForKey:@"expenseType"] forKey:@"expenseType"];
                         
                     }
                     else
                     {
                         [detailArray addObject:@"Expense Type"];
                         [detailDictionary setObject:@"Expense Type" forKey:@"expenseType"];
                     }
                     if ([appDelegate.reminderTypeArray count]>0)
                     {
                         [detailArray addObject:[[appDelegate.reminderTypeArray objectAtIndex:0]valueForKey:@"reminderType"]];
                         [detailDictionary setObject:[[appDelegate.reminderTypeArray objectAtIndex:0]valueForKey:@"reminderType"] forKey:@"reminderType"];
                     }
                     else
                     {
                         [detailArray addObject:@"Reminder Type"];
                         [detailDictionary setObject:@"Reminder Type" forKey:@"reminderType"];
                     }
                     
                     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                     NSDate *restoreDate = [NSDate date];
                     
                     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                     [formatter setDateFormat:@"yyyy-MM-dd"];
                     NSString *lastRestoreDate = [formatter stringFromDate:restoreDate];
                     [prefs setObject:lastRestoreDate forKey:@"RESTOREDATE"];

                    
                     NSDate*tempDate = [formatter dateFromString:lastRestoreDate];
                     lastRestoreDate = [appDelegate.regionDateFormatter stringFromDate:tempDate];
                     [detailDictionary setObject:lastRestoreDate forKey:@"RESTOREDATE"];
                     [formatter release];
                     
                    [self.view setUserInteractionEnabled:YES];
                     ActivityView.hidden = YES;
                    appDelegate.isTakingBackupFromiCloud = NO;

                     [tblView reloadData];
                     UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Property Log Book" message:@"Restore from iCloud completed successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alertView show];
                     [alertView release]; 
                 }
                 
             } 
             else 
             {
                 nslog(@"failed to open from iCloud");
                 ActivityView.hidden = YES;
                 [self.view setUserInteractionEnabled:YES];
                 appDelegate.isTakingBackupFromiCloud = NO;
                 /*
                 UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No iCloud backup found." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 alertView.tag = 5001;
                 [alertView show];
                 [alertView release];
                  */
             }
             
         }];
        
    }
    
    if(isFileFound == FALSE)
    {
        ActivityView.hidden = YES;
        [self.view setUserInteractionEnabled:YES];
        appDelegate.isTakingBackupFromiCloud = NO;
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No iCloud backup found." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alertView.tag = 5001;
        [alertView show];
        [alertView release];
    }
    
} 




#pragma mark - ActionSheet Methods

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self sendSms];
    }
    else if (buttonIndex == 0)
    {
        [self showEmail];
    }
    
}

#pragma mark -
#pragma mark sms method
-(IBAction)sendSms
{
	MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
		[controller setBody:@"Check out Property Log Book application in iTunes, its quite handy \n\n http://itunes.apple.com/us/app/property-log-book-manage-your/id482865530?ls=1&mt=8"];
		controller.recipients = [NSArray arrayWithObjects:nil];
		controller.messageComposeDelegate = self;
		[self presentModalViewController:controller animated:YES];
    }
    [controller release];
	
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
	
	[self dismissModalViewControllerAnimated:YES];
	
}


#pragma mark -
#pragma mark email method

-(IBAction)showEmail
{
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}
}


// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
    //	[[picker navigationBar] setTintColor:[UIColor blackColor]];
	//[[picker navigationBar] setTintColor:[UIColor colorWithRed:0.36 green:0.09 blue:0.39 alpha:1.00]];

    
    
    [picker setSubject:@"Property Log Book"];
    // Set up recipients
    NSArray *toRecipients = [NSArray arrayWithObject:@""]; 


    [picker setToRecipients:toRecipients];

    NSString *emailBody = @"Check out Property Log Book application in iTunes, its quite handy \n\n http://itunes.apple.com/us/app/property-log-book-manage-your/id482865530?ls=1&mt=8";

    [picker setMessageBody:emailBody isHTML:NO];
    [self presentModalViewController:picker animated:YES];
    [picker release];
    }




-(void)displayComposerSheetforsupport
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
    //	[[picker navigationBar] setTintColor:[UIColor blackColor]];
	//[[picker navigationBar] setTintColor:[UIColor colorWithRed:0.36 green:0.09 blue:0.39 alpha:1.00]];
    
	[picker setSubject:@"Contact: Property Log Book"];
	
	
	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:emailRecepient]; 
    NSString *deviceModel = [UIDevice currentDevice].model;
    nslog(@"Device Model------%@",[UIDevice currentDevice].model);
    NSString *deviceOS = [UIDevice currentDevice].systemVersion;
    nslog(@"Device Current os-----%@",[UIDevice currentDevice].systemVersion);
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    nslog(@"App Version-----%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]);
    
    NSLocale *currentUsersLocale = [NSLocale currentLocale];
    NSString *region = [currentUsersLocale localeIdentifier];
    nslog(@"Current Locale: %@", [currentUsersLocale localeIdentifier]);
	
	[picker setToRecipients:toRecipients];
    
    NSString *emailBody = [NSString stringWithFormat:@"\n \n \n Device : %@ \n OS Version: %@ \n App Version: %@ \n Region Code: %@",deviceModel, deviceOS, version, region];
    
   	[picker setMessageBody:emailBody isHTML:NO];
    
	
	[self presentModalViewController:picker animated:YES];
    [picker release];
    
}

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheetForFeedback 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
    
	[picker setSubject:@"Feedback: Property Log Book"];
	
	
	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:emailRecepient]; 
    
	
	[picker setToRecipients:toRecipients];
    
    NSString *emailBody = @"";
    
   	[picker setMessageBody:emailBody isHTML:YES];
    
    
    /*
     Start:
     Sun:0004
     Setting custom image in navigation bar..
     */
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0)
    {
        [picker.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
    }
    
    /*
     Ends:
     Sun:0004
     Setting custom image in navigation bar..
     */

    
    
	
	[self presentModalViewController:picker animated:YES];
    [picker release];
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
    [self dismissModalViewControllerAnimated:YES];
	
}

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:&subject=Property Log Book";
	NSString *body = @"&body=Check out Property Log Book application in iTunes, its quite handy";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}



#pragma mark -





- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class])); 
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(int)getPassCodeStatus
{
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	int Number=[prefs integerForKey:@"PasscodeOn"];
	if (!Number) 
    {
		Number=0;
	}
	return Number;
}

-(IBAction)header_button_pressed:(id)sender
{
//some code..
}

#pragma mark -
#pragma mark  showHideSection
#pragma mark  Manages showing and hiding sections..
-(void)showHideSection:(id)sender
{
    UIButton *tempButton = (UIButton*)sender;
    nslog(@"\n tempButton = %d",tempButton.tag);
    pressed_button_tag = tempButton.tag;
    
    
    
    if([minimised_section_array indexOfObject:[NSNumber numberWithInt:pressed_button_tag]]==NSNotFound)
    {
        [minimised_section_array addObject:[NSNumber numberWithInt:pressed_button_tag]];
        [tempButton setImage:[UIImage imageNamed:@"arrow_down.png"] forState:UIControlStateNormal];
        [tempButton setImage:[UIImage imageNamed:@"arrow_down.png"] forState:UIControlStateHighlighted];
    }
    else
    {
        [minimised_section_array removeObject:[NSNumber numberWithInt:pressed_button_tag]];
        [tempButton setImage:[UIImage imageNamed:@"arrow_open.png"] forState:UIControlStateNormal];
        [tempButton setImage:[UIImage imageNamed:@"arrow_open.png"] forState:UIControlStateHighlighted];
    }
    
    
    [tblView reloadSections:[NSIndexSet indexSetWithIndex:pressed_button_tag-1000] withRowAnimation:UITableViewRowAnimationFade];
    
    
    
    
    
    
}

-(void)showHideSectionByBgImage:(id)sender
{
    UIButton *tempButton = (UIButton*)sender;
    nslog(@"\n tempButton = %d",tempButton.tag);
    pressed_button_tag = tempButton.tag;
    
    
    
    if([minimised_section_array indexOfObject:[NSNumber numberWithInt:pressed_button_tag]]==NSNotFound)
    {
        [minimised_section_array addObject:[NSNumber numberWithInt:pressed_button_tag]];
       
    }
    else
    {
        [minimised_section_array removeObject:[NSNumber numberWithInt:pressed_button_tag]];
    }
    
    
    [tblView reloadData];
    //[tblView reloadSections:[NSIndexSet indexSetWithIndex:pressed_button_tag-1000] withRowAnimation:UITableViewRowAnimationFade];
    
    
    
    
    
    
}





#pragma mark -

- (void)dealloc
{
    [super dealloc];
    
    [minimised_section_array release];
    
    [date_filter_end_date release];
    [date_filter_start_date release];
    
    [detailArray release];
    [data_dictionary release];
    [customizeArray release];
    [preferences_array release];
    [backupresoreArray release];    
    [help_feedback_array release];
    [propertyArray release];
    
    [ActivityView release];
    [mainActivityIndicator release];
    [tblView release];
    
}



@end
