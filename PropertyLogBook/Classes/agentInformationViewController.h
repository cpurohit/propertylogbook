//
//  agentInformationViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

@interface agentInformationViewController : UIViewController {
    
    IBOutlet UITableView *tblView;
    PropertyLogBookAppDelegate *appDelegate;
    NSIndexPath *oldIndex;
    IBOutlet UILabel *lblNoData,*lblPlus;
    IBOutlet UIImageView *imageView;

    NSMutableArray *tempRowArray;

}
-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew;
@end
