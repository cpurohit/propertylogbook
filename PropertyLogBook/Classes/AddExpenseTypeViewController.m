//
//  AddExpenseTypeViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddExpenseTypeViewController.h"


@implementation AddExpenseTypeViewController



- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    MemoryLog(@"\n didReceiveMemoryWarning  in AddExpenseTypeViewController \n");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

#pragma mark - View lifecycle




- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
         [tblView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
        
    }
    
    
    if(!appDelegate.isIPad)
    {
        
        
        //tableview height in textfieldend editing etc…
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        
    }

    
    textFieldIndex = -1;
    
    self.navigationItem.title = @"EXPENSE TYPE";
   // self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel_clicked)];
   
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }

    
    /*
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_clicked)] autorelease];
    */
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
        UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator1.width = -12;
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
        
    }
    else
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
    }

    
    
    if(appDelegate.isIphone5)
    {
        keytoolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,244, self.view.frame.size.width, 44)];
    }
    else
    {
        keytoolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 156, self.view.frame.size.width, 44)];
    }

    
    

   
       

    
    
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            keytoolBar.frame = CGRectMake(0, 671,768, 44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            keytoolBar.frame = CGRectMake(0,326,1024, 44);

        }
        
    }

    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(keytoolBar_cancel_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flexiItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(keytoolBar_done_clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(keytoolBar_done_clicked)];
    
     */
     
    [keytoolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexiItem,doneButton, nil]];
    //    toolBar.items = array;
    keytoolBar.barStyle = UIBarStyleBlack;
    [self.view addSubview:keytoolBar];
    keytoolBar.hidden = TRUE;

    [cancelBtn release];
    [flexiItem release];
    [doneButton release];  
    
    if (appDelegate.isFromIncomeExpenseView >0)
    {
        
        
        /*
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_clicked)] autorelease];
         */
        
        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
        {
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
            UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator1.width = -12;
            
            [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
            
        }
        else
        {
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
        }

    
        
        
        tblView.editing = TRUE;
    }
    else
    {
        tblView.editing = FALSE;
    }
    
    [appDelegate selectExpenseType];
	tblView.backgroundColor = [UIColor clearColor];
	
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0, 1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
	
   
    
}

-(void)keytoolBar_cancel_clicked
{
    keytoolBar.hidden = TRUE;
    UITextField *text = (UITextField *)[self.view viewWithTag:isClicked];
    nslog(@"\n textFieldTempText = %@",textFieldTempText);
    text.text =  textFieldTempText;
    [text resignFirstResponder];
    [self textFieldShouldReturn:text];
}
-(void)keytoolBar_done_clicked:(id)sender
{
    keytoolBar.hidden = TRUE;
    UITextField *text = (UITextField *)[self.view viewWithTag:isClicked];
    textFieldTempText = text.text;
    [text resignFirstResponder];
	[self textFieldShouldReturn:text];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];
    
    //[self.view addSubview:appDelegate.bottomView];
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    
    
    
    isAddClicked = NO;
	if([appDelegate.expenseTypeArray count]==0)
	{
        imageView.hidden = FALSE;
		lblNoData.hidden = FALSE;
        lblPlus.hidden = FALSE;
		[tblView setHidden:TRUE];
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                lblPlus.center = CGPointMake(284,466);  
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(494,655.0f/2.0f);   
                lblPlus.center = CGPointMake(393,338); 
            }
            
            
        }
        
        [tblView setBackgroundView:nil];
        [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
        
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0, 1024, 1024)];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        [imgView release];

		
	}
	else 
    {
        
        imageView.hidden = TRUE;
		[tblView setHidden:FALSE];
		lblNoData.hidden = TRUE;
        lblPlus.hidden = TRUE;
		[tblView reloadData];
		
	}
    
    

    
    if(appDelegate.isIPad)
    {
    
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
     nslog(@"\n tb = %f",tblView.frame.size.height);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}


-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
    if(appDelegate.isIPad)
    {
        
        if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
        {
            imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
            lblNoData.center = CGPointMake(384,911.0f/2.0f);   
            lblPlus.center = CGPointMake(284,466);  
        }
        else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
        {
            imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
            lblNoData.center = CGPointMake(494,655.0f/2.0f);   
            lblPlus.center = CGPointMake(393,338); 
        }
        
        
    }

    
    if(appDelegate.isIPad)
    {
        
        if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
        {
            
                           keytoolBar.frame = CGRectMake(0, 671,768, 44);
            
            
            
        }
        else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
        {
                            keytoolBar.frame = CGRectMake(0,326,1024, 44);
            
            
           
            
        }
        
    }
    
    
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    /*
    if(textFieldIndex != 5000)
    {
        keytoolBar.hidden = TRUE;
        [self.view endEditing:YES];
    }

    [tblView reloadData];
     */
    
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
    }

    
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

#pragma mark -
-(void)back_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)cancel_clicked
{
    
    
    if(appDelegate.isFromIncomeExpenseView != -1)
    {
        if (textFieldIndex > 0)
        {
            UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldIndex];
            [textField resignFirstResponder];
        }
        
        
        for (int i=0;i<[appDelegate.expenseTypeArray count];i++)
        {
            [appDelegate updateExpenseType:[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"expenseType"] rowid:[[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
            
        }
        [appDelegate selectExpenseType];

    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)add_clicked
{
    isAddClicked = YES;
    if(textFieldIndex != 5000 && textFieldIndex>0)
    {
        UITextField*textField = (UITextField*)[self.view viewWithTag:textFieldIndex];
        int index = textField.tag%1000;
        nslog(@"\n text while ad = %@",textField.text);
        
        [[appDelegate.expenseTypeArray objectAtIndex:index] setValue:textField.text forKey:@"expenseType"];
        
        for (int i=0;i<[appDelegate.expenseTypeArray count];i++)
        {
            [appDelegate updateExpenseType:[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"expenseType"] rowid:[[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
            
        }


    }
    
    [appDelegate selectExpenseType];
    
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Enter Expense Type" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	
    myAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    expenseText = [myAlertView textFieldAtIndex:0];

    
    //expenseText= [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    
    
    [expenseText setKeyboardAppearance:UIKeyboardAppearanceDefault];
    expenseText.delegate = self;
    expenseText.tag = 5000;
	[expenseText becomeFirstResponder];
	[expenseText setBackgroundColor:[UIColor clearColor]];
	[myAlertView addSubview:expenseText];
	[myAlertView show];
	[myAlertView release];
}

- (void) alertView:(UIAlertView *) actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex 
{
    
    isAddClicked = NO;
    
    keytoolBar.hidden = TRUE;
    
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }

    
	
    
    if (buttonIndex == 1)
	{      
        if ([expenseText.text length]>0)
        {
            
            
            for (int i=0;i<[appDelegate.expenseTypeArray count];i++)
            {
                [appDelegate updateExpenseType:[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"expenseType"] rowid:[[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
                
            }
            [appDelegate selectExpenseType];

            
        [appDelegate AddExpenseType:expenseText.text];
        
        
        NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
        [tdict setObject:expenseText.text forKey:@"expenseType"];
        
		[appDelegate.expenseTypeArray addObject:tdict];
		[appDelegate selectExpenseType];
      //  [tblView setEditing:YES];

		[tblView setHidden:FALSE];
                        imageView.hidden = TRUE;
		[lblNoData setHidden:TRUE];
            lblPlus.hidden = TRUE;
            
            [tdict release];    
		[tblView reloadData];
        }
        else
        {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid expense type" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
        }
	}
	else 
    {
        if ([appDelegate.expenseTypeArray count] == 0)
        {
		
            [lblNoData setHidden:FALSE];
            lblPlus.hidden = FALSE;
            imageView.hidden = FALSE;
            
            
            
            if(appDelegate.isIPad)
            {
                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
                    lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                    lblPlus.center = CGPointMake(284,466);  
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
                    lblNoData.center = CGPointMake(494,655.0f/2.0f);   
                    lblPlus.center = CGPointMake(393,338); 
                }
                
                
            }

            
                      
		[tblView setHidden:TRUE];
           // [self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
			UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
            [imgView setFrame:CGRectMake(0, 0,1024,1024)];
            //  [imgView setContentMode:UIViewContentModeScaleToFill];
            [self.view addSubview:imgView];
            [self.view sendSubviewToBack:imgView];
            
            [imgView release];


		}
        
        
        if(!appDelegate.isIPad)
        {
            
            if(appDelegate.isIphone5)
            {
                //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
                
                
                if(appDelegate.isIOS7)
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
                }
                else
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
                }
                
                
                
            }
            else
            {
                
                if(appDelegate.isIOS7)
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
                }
                else
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
                }
                
                
            }
            
        }
        
        
        
        [tblView reloadData];
	}

}


#pragma mark text field

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

    textFieldIndex = textField.tag;
    
    keytoolBar.hidden = FALSE;
    isClicked = textField.tag;
    textFieldTempText = textField.text;
    [textFieldTempText retain];

    /*
    NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:textField.tag%1000 inSection:0];
	[tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
     */
    if (textField.tag > 1002)
    {
        /*
        tblView.frame = CGRectMake(0, -((textField.tag%1000)*25), tblView.frame.size.width, tblView.frame.size.height);
         */
    }
    
    
    
    return YES;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(appDelegate.isIPad)
    {
        if(isAddClicked)
        {
            return NO;
        }
    }

    
    int index = textField.tag%1000;
    nslog(@"reminder = %d",index);
    nslog(@"\n %@",[appDelegate.expenseTypeArray objectAtIndex:index]);
    
    nslog(@"textField = %@",textField.text);
    
    if(textField.tag != 5000)//alert view tag..!
    {
        [[appDelegate.expenseTypeArray objectAtIndex:index] setValue:textField.text forKey:@"expenseType"];
        for(int i=0;i<[appDelegate.expenseTypeArray count];i++)
        {
            
            [appDelegate updateExpenseType:[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"expenseType"] rowid:[[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        }

    }

    if(appDelegate.isIPad)
    {
        keytoolBar.hidden = TRUE;
        [textField resignFirstResponder];
    }

    
    
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }



}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    keytoolBar.hidden = FALSE;
    
    nslog(@" keytoolBar = %@",NSStringFromCGRect(keytoolBar.frame));
    
    if(!appDelegate.isIPad)
    {
        nslog(@" origint = %f",textField.frame.origin.y);
        
        if(appDelegate.isIphone5)
        {
            tblView.frame = CGRectMake(0,tblView.frame.origin.y ,tblView.frame.size.width,240.0f);
            
        }
        else
        {
            tblView.frame = CGRectMake(0,tblView.frame.origin.y ,tblView.frame.size.width,155.0f);
            
        }
        
        nslog(@"\n pos = %f",textField.frame.origin.y+(44*(isClicked%1000))-44);
        
        [tblView setContentOffset:CGPointMake(0,textField.frame.origin.y+(44*(isClicked%1000)-44)) animated:YES];
        [tblView setContentOffset:CGPointMake(0,449) animated:YES];
        
        
        
        //[tblView scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];
    }
    if(!isAddClicked)
    {
        [self scrollViewToTextField:textField];
    }
    
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(isAddClicked== TRUE)
    {
        return  NO;
    }
    keytoolBar.hidden = TRUE;
    nslog(@"comes is should end editing");
    if (textField.tag > 999)
    {
        
        
        
        if(!appDelegate.isIPad)
        {
            
            if(appDelegate.isIphone5)
            {
                //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
                
                
                if(appDelegate.isIOS7)
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
                }
                else
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
                }
                
                
                
            }
            else
            {
                
                if(appDelegate.isIOS7)
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
                }
                else
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
                }
                
                
            }
            
        }

        
        
        nslog(@"\n textField.tag = %d",textField.tag);
        nslog(@"\n textField.text = %@",textField.text);
        nslog(@"\n textField.tag = %d",textField.tag%1000);
        nslog(@"\n textFieldIndex = %d",textFieldIndex%1000);
        
        
        nslog(@"\n appDelegate.reminderTypeArray = %@",appDelegate.expenseTypeArray);
        nslog(@"\n appDelegate.reminderTypeArray at index = %@",[appDelegate.expenseTypeArray objectAtIndex:(textField.tag%1000)]);
        nslog(@"\n text here = %@",textField.text);
        
        
        NSString *text = @"";        
        for(int i=0;i<[appDelegate.expenseTypeArray count];i++)
        {
            nslog(@"\n appDelegate.expenseTypeArray in loop = %@",appDelegate.expenseTypeArray);
            if(textFieldIndex%1000 == i)
            {
                //text = textField.text;
                text = textFieldTempText;
            }
            else
            {
                text =  [[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"expenseType"];
            }
            nslog(@"\n text = %@",text);
            [appDelegate updateExpenseType:text rowid:[[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
            
            //
            
        }
        [appDelegate selectExpenseType];
        
        

        
        
        if (textField.tag == textFieldIndex)
        {
            textFieldIndex = -1;
        }
    }

    
    keytoolBar.hidden = TRUE;
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }

    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Table view data source


- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    
    if (appDelegate.isFromIncomeExpenseView > 0)
    {
        return @"Note: 1st entry is default entry";
    }
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([appDelegate.expenseTypeArray count]>0)
    {
        return [appDelegate.expenseTypeArray count];
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITextField *expenseTextField;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (cell==nil)
    if (1)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        if(appDelegate.isIOS7)
        {
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        }
        
         [cell.textLabel setFont:[UIFont systemFontOfSize:15.0]];
        
        expenseTextField = [[UITextField alloc]initWithFrame:CGRectMake(20, 12, 200, 31)];    
        expenseTextField.delegate = self;
        expenseTextField.borderStyle = UITextBorderStyleNone;
        expenseTextField.tag = 1000 +indexPath.row;
        
        
        if (appDelegate.isFromIncomeExpenseView >0)
        {
            [expenseTextField setEnabled:TRUE];
        }
        else
        {
            [expenseTextField setEnabled:FALSE];    
        }
        [cell.contentView addSubview:expenseTextField];
      
    }
    else
    {
        expenseTextField = (UITextField *)[cell.contentView viewWithTag:1000];
        [expenseTextField retain];
    }
    
    
    
    
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            expenseTextField.frame = CGRectMake(20, 9,500, 31);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            expenseTextField.frame = CGRectMake(20, 9,800, 31);
        }
        
        
    }
    else
    {
        expenseTextField.frame = CGRectMake(20, 12, 200, 31);
    }
    
    
    
    expenseTextField.tag = 1000+indexPath.row;
    [expenseTextField setText:[[appDelegate.expenseTypeArray objectAtIndex:indexPath.row]valueForKey:@"expenseType"]];
   
    expenseTextField.font =[UIFont systemFontOfSize:15.0];
    expenseTextField.hidden = TRUE;
    if ([appDelegate.expenseTypeArray count]>0)
    {
        cell.textLabel.hidden = TRUE;
        expenseTextField.hidden = FALSE;
        [expenseTextField setText:[[appDelegate.expenseTypeArray objectAtIndex:indexPath.row]valueForKey:@"expenseType"]];

        cell.textLabel.textColor = [UIColor blackColor];
        
        if (appDelegate.isFromIncomeExpenseView < 0 )
        {
            if ([expenseTextField.text isEqualToString:appDelegate.incomeString])
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                oldIndex = indexPath;
                [oldIndex retain];
            }
        }

        
    }
    else
    {
        cell.textLabel.hidden = FALSE;
        [cell.textLabel setFont:[UIFont systemFontOfSize:17.0]];
        [cell.textLabel setTextAlignment:UITextAlignmentCenter];

        cell.textLabel.text = @"No Expense Type Found";
        cell.textLabel.textColor = [UIColor grayColor];
    }
    
    [expenseTextField release];
    // Configure the cell...
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [self moveFromOriginal:fromIndexPath.row toNew:toIndexPath.row];
    
}

-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew 
{
    if (textFieldIndex >0)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldIndex];
        [textField resignFirstResponder];
    }
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }


    [appDelegate selectExpenseType];

    
    NSMutableArray *rowIdArray = [[NSMutableArray alloc] initWithCapacity:10];
    nslog(@"\n before moving = %@",appDelegate.expenseTypeArray);
    for (int i=0;i<[appDelegate.expenseTypeArray count];i++)
    {
        [rowIdArray addObject:[[appDelegate.expenseTypeArray objectAtIndex:i] valueForKey:@"rowid"]];
    }
    nslog(@"\n rowIdArray  = %@",rowIdArray);

    
    NSMutableArray *tempRowArray = [[NSMutableArray alloc]initWithArray:appDelegate.expenseTypeArray];
	NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:appDelegate.expenseTypeArray];
	id tempObject = [tempArray objectAtIndex:indexOriginal];
	[tempArray removeObjectAtIndex:indexOriginal];
	[tempArray insertObject:tempObject atIndex:indexNew];
	appDelegate.expenseTypeArray = tempArray;
    
    
    nslog(@"\n after moving = %@",appDelegate.expenseTypeArray);
	
    for (int i=0;i<[appDelegate.expenseTypeArray count];i++)
    {
        [[appDelegate.expenseTypeArray objectAtIndex:i] setObject:[rowIdArray objectAtIndex:i] forKey:@"rowid"];
    }
    
    nslog(@"\n after moving  and setting index..= %@",appDelegate.expenseTypeArray);
    
    
    for (int i=0;i<[appDelegate.expenseTypeArray count];i++)
    {
        [appDelegate updateExpenseType:[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"expenseType"] rowid:[[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        
    }
    [appDelegate selectExpenseType];
    
    
    
    keytoolBar.hidden = TRUE;
    [rowIdArray release];
    [tempArray release];
    [tempRowArray release];
    
    [tblView reloadData];

    textFieldIndex = -1;
    
    
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
   
    
    
    if ([appDelegate.expenseTypeArray count]>0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([appDelegate.expenseTypeArray count]>0)
    {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Delete the row from the data source
    for (int i = 0; i <[appDelegate.expenseTypeArray count];i++)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:i+1000];
        [textField resignFirstResponder];
    }
    
    for (int i=0;i<[appDelegate.expenseTypeArray count];i++)
    {
        [appDelegate updateExpenseType:[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"expenseType"] rowid:[[[appDelegate.expenseTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        
    }
    [appDelegate selectExpenseType];
    keytoolBar.hidden = TRUE;
    textFieldIndex  = -1;
    [appDelegate deleteExpenseType:[[[appDelegate.expenseTypeArray objectAtIndex:indexPath.row]valueForKey:@"rowid"]intValue]];
 //   [appDelegate deleteExpenseType:indexPath.row+1];
    [appDelegate.expenseTypeArray removeObjectAtIndex:indexPath.row];
    
    [appDelegate selectExpenseType];
    nslog(@"after delete expesen array.....%@",appDelegate.expenseTypeArray);
	if([appDelegate.expenseTypeArray count]==0)
	{
        		imageView.hidden = FALSE;
		lblNoData.hidden = FALSE;
        lblPlus.hidden = FALSE;
		[tblView setHidden:TRUE];
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                lblPlus.center = CGPointMake(284,466);  
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(494,655.0f/2.0f);   
                lblPlus.center = CGPointMake(393,338); 
            }
            
            
        }
        
		//[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];

		
	}
	else {
        
        
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
        }

        
        
        
        
        
        
        imageView.hidden = TRUE;
		[tblView setHidden:FALSE];
		lblNoData.hidden = TRUE;
        lblPlus.hidden = TRUE;
		[tblView reloadData];
		
	}

    //[tblView reloadData];
    
    
}

#pragma mark - Table view delegate

- (void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    [tblView setEditing:editing animated:animated];
    if (editing)
    {
        for (int i =0; i <[appDelegate.expenseTypeArray count];i++)
        {
            UITextField *text = (UITextField *)[self.view viewWithTag:1000+i];
            text.enabled = TRUE;
        }
        

        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
        {
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
            UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator1.width = -12;
            
            [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
            
        }
        else
        {
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
        }

    }
    else
    {
        for (int i =0; i <[appDelegate.expenseTypeArray count];i++)
        {
            UITextField *text = (UITextField *)[self.view viewWithTag:1000+i];
            text.enabled = FALSE;
        }
        
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel_clicked)] autorelease];
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (tblView.editing)
        {
            UITextField *txtField = (UITextField *)[self.view viewWithTag:1000-indexPath.row];
            [txtField becomeFirstResponder];
        }
        else
        {
            if (appDelegate.isFromIncomeExpenseView < 0 )
            {
                if ([appDelegate.expenseTypeArray count]>0)
                {
                    UITableViewCell *oldCell = [tblView cellForRowAtIndexPath:oldIndex];
                    
                    oldCell.accessoryType = UITableViewCellAccessoryNone;
                }
                
                UITableViewCell *newCell = [tblView cellForRowAtIndexPath:indexPath];
                newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                appDelegate.incomeString = [NSString stringWithFormat:@"%@",[[appDelegate.expenseTypeArray objectAtIndex:indexPath.row]valueForKey:@"expenseType"]];
                //            appDelegate.propertyTypeStr = newCell.textLabel.text;
                
                /*Memory*/
                //[appDelegate.incomeString retain];
                oldIndex = indexPath;
                [self.navigationController popViewControllerAnimated:YES];
            }
        }

}

- (void)scrollViewToTextField:(id)textField
{
    // Set the current _scrollOffset, so we can return the user after editing
    
	//_scrollOffsetY = self.tableView.contentOffset.y;
    
    // Get a pointer to the text field's cell
    UITableViewCell *theTextFieldCell = (UITableViewCell *)[textField superview];
    
    // Get the text fields location
    CGPoint point = [theTextFieldCell convertPoint:theTextFieldCell.frame.origin toView:tblView];
    
    // Scroll to cell
    [tblView setContentOffset:CGPointMake(0, point.y - 12) animated: YES];
    
    // Add some padding at the bottom to 'trick' the scrollView.
   // [tblView setContentInset:UIEdgeInsetsMake(0, 0, point.y - 60, 0)];
}



@end
