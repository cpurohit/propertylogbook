//
//  DateFiltersViewController.h
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 05/03/13.
//
//


/*
 DateButon tag starts from 1000,1001
 
 
 */


#import <UIKit/UIKit.h>
#import "MilageTableViewCell.h"
#import "PropertyLogBookAppDelegate.h"


@interface MilageViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet  UITableView*milage_tableView;
    MilageTableViewCell *tblCell;
    UINib *cellNib;
    PropertyLogBookAppDelegate*appDelegate;
    NSMutableArray*   preferences_array;
 
    
    
}


@property (nonatomic, retain) IBOutlet MilageTableViewCell *tblCell;
@property (nonatomic, retain) UINib *cellNib;
-(IBAction)dateValueChanged:(id)sender;

-(IBAction)done_button_clicked:(id)sender;
-(IBAction)cancel_button_clicked:(id)sender;





@end
