//
//  DateFiltersTableViewCell.h
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 05/03/13.
//
//

#import <UIKit/UIKit.h>

@interface MilageTableViewCell : UITableViewCell
{
    IBOutlet UIImageView*cellIcon_ImageView;
    
    IBOutlet  UILabel*cellTitle_Label;
    
    IBOutlet UISwitch *milage_on_off_switch;
    IBOutlet UIButton*km_button;
    IBOutlet  UIButton*mile_button;
    
    
    
}

@property (nonatomic,retain) IBOutlet UIImageView*cellIcon_ImageView;
@property (nonatomic,retain) IBOutlet UILabel*cellTitle_Label;

@property (nonatomic,retain) IBOutlet UISwitch *milage_on_off_switch;
@property (nonatomic,retain) IBOutlet UIButton*km_button;
@property (nonatomic,retain) IBOutlet UIButton*mile_button;






@end
