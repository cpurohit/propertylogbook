//
//  DateFiltersTableViewCell.m
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 05/03/13.
//
//

#import "MilageTableViewCell.h"

@implementation MilageTableViewCell

@synthesize cellIcon_ImageView,cellTitle_Label,milage_on_off_switch,km_button,mile_button;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)dealloc
{
    
    [cellIcon_ImageView release];
    [cellTitle_Label release];
    [km_button release];
    [mile_button release];
    
    [super dealloc];
}


@end
