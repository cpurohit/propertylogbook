//
//  DateFiltersViewController.m
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 05/03/13.
//
//

#import "MilageViewController.h"

@interface MilageViewController ()

@end

@implementation MilageViewController

@synthesize tblCell,cellNib;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.navigationItem.title = @"SET MILAGE";
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    
    
   
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    self.cellNib = [UINib nibWithNibName:@"MilageTableViewCell" bundle:nil];
    

    preferences_array = [[NSMutableArray alloc]initWithObjects:@"Mileage",@"Mileage Unit",nil];

    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
         [milage_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }

    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //milage_tableView.frame = CGRectMake(milage_tableView.frame.origin.x,milage_tableView.frame.origin.y,milage_tableView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                milage_tableView.frame = CGRectMake(milage_tableView.frame.origin.x,milage_tableView.frame.origin.y-25.0f,milage_tableView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                milage_tableView.frame = CGRectMake(milage_tableView.frame.origin.x,milage_tableView.frame.origin.y,milage_tableView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                milage_tableView.frame = CGRectMake(milage_tableView.frame.origin.x,milage_tableView.frame.origin.y-25.0f,milage_tableView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                milage_tableView.frame = CGRectMake(milage_tableView.frame.origin.x,milage_tableView.frame.origin.y,milage_tableView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            milage_tableView.frame = CGRectMake(milage_tableView.frame.origin.x,milage_tableView.frame.origin.y-25.0f,milage_tableView.frame.size.width,self.view.frame.size.height+25.0f);
        }
    }
    
    
    

    milage_tableView.backgroundColor = [UIColor clearColor];
    [milage_tableView setBackgroundView:nil];
    [milage_tableView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
    

    
    
    
    
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }

    
    

    
    

    
}




-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    //[self.view addSubview:appDelegate.bottomView];
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    
    if(appDelegate.isIPad)
    {
        
		[self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    	[self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
	}
    
}

- (void)didReceiveMemoryWarning
{
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class])); 
    [super didReceiveMemoryWarning];
    
}



#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

#pragma mark
#pragma mark Tableview methods 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(appDelegate.is_milege_on)
    {
        return [preferences_array count];
    }
    else
    {
        return [preferences_array count]-1;
    }
    return 0;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     static NSString *MyIdentifier = @"MyIdentifier";
    
    MilageTableViewCell *cell = (MilageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];

    
	
	// If no cell is available, create a new one using the given identifier.
	if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
		cell = tblCell;
		self.tblCell = nil;

    }
	
    cell.cellTitle_Label.text = [preferences_array objectAtIndex:indexPath.row];
 
    if(indexPath.row == 0)
    {
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        //cell.imageView.image = [UIImage imageNamed:@"mileage_icon.png"];
        
        UIImageView*mileage_imageView = [[UIImageView alloc] init];
        mileage_imageView.frame = CGRectMake(0, 0,40,40);
        mileage_imageView.image = [UIImage imageNamed:@"mileage_icon.png"];
        [cell.contentView addSubview:mileage_imageView];
        [mileage_imageView release];

        
        cell.milage_on_off_switch.hidden = FALSE;
        cell.km_button.hidden = TRUE;
        cell.mile_button.hidden = TRUE;
        
        //milage_on_off_switch = [[UISwitch alloc]initWithFrame:CGRectMake(211, 8, 94, 27)];
        cell.milage_on_off_switch.frame = CGRectMake(211, 8, 94, 27);
        
        if(appDelegate.isIOS7)
        {
            cell.milage_on_off_switch.frame = CGRectMake(231, 8, 94, 27);
        }
        
        
        if(appDelegate.isIPad)
        {
            //
            
            
            if(appDelegate.isIOS7)
            {
                
                if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
                {
                    cell.milage_on_off_switch.frame = CGRectMake(688, 8, 94, 27);
                }
                else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
                {
                    cell.milage_on_off_switch.frame = CGRectMake(942, 8, 94, 27);
                }
                
            }
            else
            {
                
                if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
                {
                    cell.milage_on_off_switch.frame = CGRectMake(598, 8, 94, 27);
                }
                else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
                {
                    cell.milage_on_off_switch.frame = CGRectMake(852, 8, 94, 27);
                }

                
            }
            
            
            
            
        }
        
        
        
        
        
        cell.milage_on_off_switch.onTintColor = [UIColor colorWithRed:10.0f/255.0f green:142.0f/255.0f blue:184.0f/255.0f alpha:1.0];
        
        
        
        
        if(appDelegate.is_milege_on)
        {
            [cell.milage_on_off_switch setOn:TRUE];
        }
        else
        {
            [cell.milage_on_off_switch setOn:FALSE];
        }
        
        
        [cell.milage_on_off_switch addTarget:self action:@selector(milage_on_off_switch_changed:) forControlEvents:UIControlEventValueChanged];
        
        
        
        
        
        
    }
    else if(indexPath.row == 1)
    {
        
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        cell.milage_on_off_switch.hidden = TRUE;
        
        
        /*
        [km_button setImage:[UIImage imageNamed:@"km_off.png"] forState:UIControlStateNormal];
        [km_button setImage:[UIImage imageNamed:@"km_on.png"] forState:UIControlStateSelected];
        
        [mile_button setImage:[UIImage imageNamed:@"mile_off.png"] forState:UIControlStateNormal];
        [mile_button setImage:[UIImage imageNamed:@"mile_on.png"] forState:UIControlStateSelected];
        */
        
        cell.km_button.tag=8001;
        cell.mile_button.tag=8002;
        
        [cell.km_button setTitle:@"Km" forState:UIControlStateNormal];
        [cell.mile_button setTitle:@"Mile" forState:UIControlStateNormal];
        
        [cell.km_button addTarget:self action:@selector(milage_unit_selected:) forControlEvents:UIControlEventTouchUpInside];
        [cell.mile_button addTarget:self action:@selector(milage_unit_selected:) forControlEvents:UIControlEventTouchUpInside];
        
        if(appDelegate.isIPad)
        {
            
            
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
                {
                    cell.km_button.frame = CGRectMake(690, 7, 38, 29);
                    cell.mile_button.frame = CGRectMake(728,7, 38, 29);
                }
                else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
                {
                    cell.km_button.frame = CGRectMake(943, 7, 38, 29);
                    cell.mile_button.frame = CGRectMake(981,7, 38, 29);
                }

                
            }
            else
            {
                
                if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
                {
                    cell.km_button.frame = CGRectMake(600, 7, 38, 29);
                    cell.mile_button.frame = CGRectMake(638,7, 38, 29);
                }
                else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
                {
                    cell.km_button.frame = CGRectMake(853, 7, 38, 29);
                    cell.mile_button.frame = CGRectMake(891,7, 38, 29);
                }

                
            }
            
            
            
        }
        else
        {
            cell.km_button.frame = CGRectMake(210,7,38, 29);
           cell. mile_button.frame = CGRectMake(248,7, 38,29);
        }
        
        
        if([[appDelegate.prefs objectForKey:@"milage_unit"] isEqualToString:@"Kms"])
        {
            [cell.km_button setSelected:TRUE];
            [cell.mile_button setSelected:FALSE];
        }
        else
        {
            [cell.km_button setSelected:FALSE];
            [cell.mile_button setSelected:TRUE];
        }
        
        UIImageView*mileage_unit_imageView = [[UIImageView alloc] init];
        mileage_unit_imageView.frame = CGRectMake(0, 0,40,40);
        mileage_unit_imageView.image = [UIImage imageNamed:@"mileage_unit.png"];
        [cell.contentView addSubview:mileage_unit_imageView];
        [mileage_unit_imageView release];

        
        //cell.imageView.image = [UIImage imageNamed:@"mileage_unit.png"];
        
    }

    
   
    
	
	return cell;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}






#pragma mark - back_clicked 
#pragma mark Comparing date.

-(void)back_clicked
{
    
    [self.navigationController popViewControllerAnimated:TRUE];
}

#pragma mark 
#pragma mark viewWillDisappear

-(void)viewWillDisappear:(BOOL)animated
{
    //db methods here...

    [super viewWillDisappear:animated];
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
   
    
    
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    if(appDelegate.isIPad)
    {
        [milage_tableView reloadData];
        [appDelegate manageViewControllerHeight];
    }
    
}


#pragma mark - milage_unit_selected

-(void)milage_unit_selected:(id)sender
{

    UIButton*km_button = (UIButton*)[self.view viewWithTag:8001];
    UIButton*mile_button = (UIButton*)[self.view viewWithTag:8002];

    
    [km_button setSelected:FALSE];
    [mile_button setSelected:FALSE];
    
    UIButton*temp_button = (UIButton*)sender;
    appDelegate.prefs=[NSUserDefaults standardUserDefaults];
    if(temp_button.tag == 8001)
    {
        [appDelegate.prefs setObject:@"Kms" forKey:@"milage_unit"];
    }
    else if(temp_button.tag == 8002)
    {
        [appDelegate.prefs setObject:@"Miles" forKey:@"milage_unit"];
    }
    [appDelegate.prefs synchronize];
    
    
    
    
   
    

    
    
    [temp_button setSelected:TRUE];
}

#pragma mark - milage_on_off_switch_changed

-(void)milage_on_off_switch_changed:(id)sender
{
    
    appDelegate.prefs=[NSUserDefaults standardUserDefaults];
    appDelegate.is_milege_on =!appDelegate.is_milege_on;
    
    nslog(@"\n appDelegate.is_milege_on = %d \n ",appDelegate.is_milege_on);
    
    
    /*
    [appDelegate.prefs setBool:appDelegate.is_milege_on forKey:@"is_milage_set"];
    */
    [appDelegate.prefs setObject:[NSString stringWithFormat:@"%d",appDelegate.is_milege_on]  forKey:@"is_milage_set"];
    [appDelegate.prefs synchronize];
    
    
    [milage_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
    
}

#pragma mark
#pragma mark dealloc

-(void)dealloc
{
    
    [super dealloc];
}


@end
