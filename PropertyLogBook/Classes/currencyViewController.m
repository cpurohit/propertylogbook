//
//  currencyViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "currencyViewController.h"


@implementation currencyViewController
@synthesize currencyArray;

#pragma mark -
#pragma mark View lifecycle

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
    self.navigationItem.title = @"CURRENCY";
     appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    /*
    currencyArray = [[NSMutableArray alloc]initWithObjects:@"Default Region Currency",@"AUD",@"CAD",@"CHF",@"CNY",@"DKK",@"EUR",@"GBP",@"HKD",@"IDR",@"INR",@"JPY",@"MYR",@"NOK",@"NZD",@"SEK",@"SGD",@"THB",@"USD",@"ZAR", nil];
   
     */
    
    
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
       
    }
    
    
    
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        
    }

    
    

    
    
    currencyDictionary = [[NSMutableDictionary alloc] init];
    
    [currencyDictionary setValue:@"Default Region Currency" forKey:@"AAA Default Region Currency"];
    [currencyDictionary setValue:@"AUD" forKey:@"Australian Dollars (AUD)"];
    [currencyDictionary setValue:@"GBP" forKey:@"British Pounds (GBP)"];
    [currencyDictionary setValue:@"CAD" forKey:@"Canadian Dollars (CAD)"];
    [currencyDictionary setValue:@"CNY" forKey:@"Chinese Yuan (CNY)"];
    [currencyDictionary setValue:@"DKK" forKey:@"Danish Kroner (DKK)"];
    [currencyDictionary setValue:@"EUR" forKey:@"Euro (EUR)"];
    [currencyDictionary setValue:@"HKD" forKey:@"Hong Kong Dollars (HKD)"];
    [currencyDictionary setValue:@"INR" forKey:@"Indian Rupees (INR)"];
    [currencyDictionary setValue:@"IDR" forKey:@"Indonesian Rupiah (IDR)"];
    [currencyDictionary setValue:@"JPY" forKey:@"Japanese Yen (JPY)"];
    [currencyDictionary setValue:@"MYR" forKey:@"Malaysian Ringgit (MYR)"];
    [currencyDictionary setValue:@"NZD" forKey:@"New Zealand Dollars (NZD)"];
    [currencyDictionary setValue:@"NOK" forKey:@"Norwegian Kroner (NOK)"];
    [currencyDictionary setValue:@"SGD" forKey:@"Singapore Dollars (SGD)"];
    [currencyDictionary setValue:@"ZAR" forKey:@"South African Rand (ZAR)"];
    [currencyDictionary setValue:@"SEK" forKey:@"Swedish Kroner (SEK)"];
    [currencyDictionary setValue:@"CHF" forKey:@"Swiss Francs (CHF)"];
    [currencyDictionary setValue:@"THB" forKey:@"Thai Baht (THB)"];
    [currencyDictionary setValue:@"USD" forKey:@"US Dollars (USD)"];
    [currencyDictionary setValue:@"AED" forKey:@"UAE Dirham (AED)"];
    
    
    //currencyArray = [[NSMutableArray alloc] initWithArray:[currencyDictionary allKeys]];
    //[currencyArray addObjectsFromArray:[currencyDictionary allKeys]];
    
    NSArray*tempArray = [[currencyDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    currencyArray = [[NSMutableArray alloc] initWithArray:tempArray];
    
    [currencyArray replaceObjectAtIndex:0 withObject:@"Default Region Currency"];


    [currencyDictionary setValue:@"Default Region Currency" forKey:@"Default Region Currency"];
   
    
    
    

       
    
    [appDelegate.currencyArray addObjectsFromArray:currencyArray];
    
    
    
	tblView.backgroundColor = [UIColor clearColor];
	
    
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];

    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }

    

    
    
    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,  1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
 
 appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
 
 /*Sun:0004
 Managing bottom toolbar
 */
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

//[self.view addSubview:appDelegate.bottomView];
[appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];


/*Sun:0004
 Managing bottom toolbar
 */

    if(appDelegate.isIPad)
    {
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
        
    }
    
    


}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    //return @"Select Currency";
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [currencyArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    [appDelegate selectCurrencyIndex];
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (cell == nil) 
    if (1) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    cell.textLabel.text = [currencyArray objectAtIndex:indexPath.row];
    [cell.textLabel setFont:[UIFont systemFontOfSize:15.0]];
    
    // Configure the cell...
    
    //if ([cell.textLabel.text isEqualToString:appDelegate.curCode])
    if ([ [currencyDictionary objectForKey:cell.textLabel.text]  isEqualToString:appDelegate.curCode])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        oldIndex = indexPath;
        [oldIndex retain];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please note that your selected currency will be applied to all the existing and new entries. Do you want to proceed?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    tempIndex = indexPath;
    [tempIndex retain];
    [alert show];
    [alert release];
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        nslog(@"cancel clicked");
    }
    else if (buttonIndex == 1)
    {
        UITableViewCell *oldCell = [tblView cellForRowAtIndexPath:oldIndex];
        
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        UITableViewCell *newCell = [tblView cellForRowAtIndexPath:tempIndex];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        
        
        nslog(@"\n new val = %@",[currencyDictionary objectForKey:[currencyArray objectAtIndex:tempIndex.row]]);
        

        
        
        //[appDelegate updateCurrency:newCell.textLabel.text];
        [appDelegate updateCurrency:[currencyDictionary objectForKey:[currencyArray objectAtIndex:tempIndex.row]]];
        
        [appDelegate selectCurrencyIndex];
        oldIndex = tempIndex;
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
     
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
    }
    
    
}

#pragma mark - back_clicked

-(void)back_clicked
{
     [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end

