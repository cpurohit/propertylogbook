//
//  PassOnOffViewController.h
//  MyReferences
//
//  Created by Neel  Shah on 02/01/12.
//  Copyright (c) 2012 Sunshine Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PropertyLogBookAppDelegate;
@interface PasswrodOnOffViewController : UIViewController<UITextFieldDelegate>
{
    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UITableView *tbl_PassOn_Off;
    NSString *PassWord;
    UISwitch *SwtPassCode;
    NSString *nib_Name;
    NSUserDefaults* prefs;
    
    int isPasswordChangeClicked;
    int isSetPasswordClicked;
    
    /*Starts:Password*/
    IBOutlet UIToolbar *toolBarKeyboard;
    BOOL Bool_SetTextField;
    int TagForNext;
    
    NSMutableDictionary *textFieldDictionary;
    BOOL isPasswordEmailFirstTime;//if first time only than load email Id from user defaults.
    /*Ends:Password*/
    
    BOOL isDonePressed;
    
}
@property(nonatomic,retain)UITableView *tbl_PassOn_Off;
@end
