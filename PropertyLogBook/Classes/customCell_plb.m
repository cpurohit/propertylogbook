//
//  customCell.m
//  productTracker
//
//  Created by Smit Sunshine on 12/08/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "customCell.h"
#import "AppDelegate.h"

@implementation customCell_plb
@synthesize lbl,txt_triptype,imgView,lbl_triptype,lbl_TripName,lbl_startEndDate,lbl_totalBudget,lbl_currencyName,lbl_tripbudget,imgviewBudget,lbl_destination,imgviewTriptype,imgviewDestination,imgviewAccessory;
//@synthesize lbl,txtfield,txtview,swtch_reminder,btn_dateTime,imgView,btn_chapterNew;//lbl_productname,lbl_productAmount;//,nameLabel,discriptionLabel;
//@synthesize lbl,btn_dateandtime,swtch_alarm,txt_alarmName,lbl_sound,imgView;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        
        obj_appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
		
        lbl = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 250, 30)];
        [lbl setFont:[UIFont systemFontOfSize:15.0]];
		[lbl setBackgroundColor:[UIColor clearColor]];
        //lbl.textColor = [UIColor whiteColor];
		[self.contentView addSubview:lbl];
		[lbl release];
        txt_triptype = [[UITextField alloc]initWithFrame:CGRectMake(10, 10, 220, 30)];
        [txt_triptype setFont:[UIFont systemFontOfSize:15.0]];
        //[txtfield setBackgroundColor:[UIColor grayColor]];
        //[txtfield setFont:[UIFont fontWithName:@"Arial" size:15.0]];
        txt_triptype.textAlignment = UITextAlignmentLeft;
       // txt_triptype.keyboardAppearance = UIKeyboardAppearanceAlert;
       // txt_triptype.textColor = [UIColor whiteColor];
        //txt_triptype.placeholder = @"";
        txt_triptype.autocorrectionType = UITextAutocorrectionTypeNo;
		[self.contentView addSubview:txt_triptype];
		[txt_triptype release];

        imgView = [[UIImageView alloc]initWithFrame:CGRectMake(255, 10, 30, 30)];
		[self.contentView addSubview:imgView];
        imgviewBudget = [[UIImageView alloc]initWithFrame:CGRectMake(10, 105, 25, 25)];
		[self.contentView addSubview:imgviewBudget];
        imgviewDestination = [[UIImageView alloc]initWithFrame:CGRectMake(10, 80, 25, 25)];
		[self.contentView addSubview:imgviewDestination];
        imgviewTriptype = [[UIImageView alloc]initWithFrame:CGRectMake(10, 50, 25, 25)];
		[self.contentView addSubview:imgviewTriptype];
        imgviewAccessory = [[UIImageView alloc]initWithFrame:CGRectMake(270, 70, 20, 20)];
		[self.contentView addSubview:imgviewAccessory];

        lbl_triptype = [[UILabel alloc]initWithFrame:CGRectMake(170, 5, 100, 30)];
       // [lbl_triptype setFont:[UIFont systemFontOfSize:13.0]];
		[lbl_triptype setBackgroundColor:[UIColor clearColor]];
       // [lbl_triptype setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];

        //lbl.textColor = [UIColor whiteColor];
        lbl_triptype.textAlignment = UITextAlignmentLeft;
		[self.contentView addSubview:lbl_triptype];
		[lbl_triptype release];

        
        lbl_TripName = [[UILabel alloc]initWithFrame:CGRectMake(40, 5, 130, 22)];
        [lbl_TripName setFont:[UIFont systemFontOfSize:15.0]];
		[lbl_TripName setBackgroundColor:[UIColor clearColor]];
		[self.contentView addSubview:lbl_TripName];
		[lbl_TripName release];

        lbl_startEndDate = [[UILabel alloc]initWithFrame:CGRectMake(5, 3, 270, 22)];
        [lbl_startEndDate setFont:[UIFont systemFontOfSize:13.0]];
		[lbl_startEndDate setBackgroundColor:[UIColor clearColor]];
        lbl_startEndDate.textAlignment = UITextAlignmentCenter;
        //lbl.textColor = [UIColor whiteColor];
		[self.contentView addSubview:lbl_startEndDate];
		[lbl_startEndDate release];

        lbl_totalBudget = [[UILabel alloc]initWithFrame:CGRectMake(190, 5, 100, 22)];
        [lbl_totalBudget setFont:[UIFont systemFontOfSize:13.0]];
		[lbl_totalBudget setBackgroundColor:[UIColor clearColor]];
        lbl_totalBudget.textAlignment = UITextAlignmentLeft;
        //lbl.textColor = [UIColor whiteColor];
		[self.contentView addSubview:lbl_totalBudget];
		[lbl_totalBudget release];

        lbl_tripbudget = [[UILabel alloc]initWithFrame:CGRectMake(40, 50, 130, 22)];
        [lbl_tripbudget setFont:[UIFont systemFontOfSize:13.0]];
		[lbl_tripbudget setBackgroundColor:[UIColor clearColor]];
        lbl_tripbudget.textAlignment = UITextAlignmentLeft;
        //lbl.textColor = [UIColor whiteColor];
		[self.contentView addSubview:lbl_tripbudget];
		[lbl_tripbudget release];

        lbl_currencyName = [[UILabel alloc]initWithFrame:CGRectMake(190, 5, 40, 22)];
        [lbl_currencyName setFont:[UIFont systemFontOfSize:13.0]];
		[lbl_currencyName setBackgroundColor:[UIColor clearColor]];
        lbl_currencyName.textAlignment = UITextAlignmentLeft;
        [self.contentView addSubview:lbl_currencyName];
		[lbl_currencyName release];
        
        lbl_destination = [[UILabel alloc]initWithFrame:CGRectMake(50, 80, 110, 22)];
        [lbl_destination setFont:[UIFont systemFontOfSize:13.0]];
		[lbl_destination setBackgroundColor:[UIColor clearColor]];
        lbl_destination.textAlignment = UITextAlignmentLeft;
        [self.contentView addSubview:lbl_destination];
		[lbl_destination release];

        /*
        swtch_alarm = [[UISwitch alloc]init];
		[self.contentView addSubview:swtch_alarm];
        
        btn_dateandtime = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//        [btn_dateandtime setBackgroundImage:[UIImage imageNamed:@"btn_box.png"] forState:UIControlStateNormal];
     //  btn_dateandtime.titleLabel.textColor = [UIColor blackColor];
        [btn_dateandtime.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12.0f]];
//        [btn_dateandtime setBackgroundImage:[UIImage imageNamed:@"btn_box.png"] forState:UIControlStateHighlighted];
        [btn_dateandtime setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
		[self.contentView addSubview:btn_dateandtime];


        txt_alarmName = [[UITextField alloc]initWithFrame:CGRectMake(180, 12, 110, 30)];
        //[txtfield setBackgroundColor:[UIColor grayColor]];
        //[txtfield setFont:[UIFont fontWithName:@"Arial" size:15.0]];
        txt_alarmName.textAlignment = UITextAlignmentRight;
        txt_alarmName.keyboardAppearance = UIKeyboardAppearanceAlert;
        txt_alarmName.textColor = [UIColor whiteColor];
        txt_alarmName.placeholder = @"Alarm Name";
		[self.contentView addSubview:txt_alarmName];
		[txt_alarmName release];
        
        lbl_sound = [[UILabel alloc]initWithFrame:CGRectMake(170, 12, 100, 30)];
        //[lbl setFont:[UIFont fontWithName:@"Arial" size:15.0]];
		[lbl_sound setBackgroundColor:[UIColor clearColor]];
        lbl_sound.textColor = [UIColor whiteColor];
        lbl_sound.textAlignment = UITextAlignmentRight;
		[self.contentView addSubview:lbl_sound];
		[lbl_sound release];
        
        imgView = [[UIImageView alloc]initWithFrame:CGRectMake(12, 22, 20, 20)];
		[self.contentView addSubview:imgView];
*/

/*
        txtview = [[UITextView alloc]initWithFrame:CGRectMake(5, 7, 290, 130)];
        [txtview setBackgroundColor:[UIColor clearColor]];
        //[txtview setFont:[UIFont fontWithName:@"Arial" size:15.0]];
        txtview.keyboardAppearance = UIKeyboardAppearanceAlert;
        txtview.textColor = [UIColor blackColor];
		[self.contentView addSubview:txtview];
		[txtview release];
*/

        /*

        btn_chapterNew = [[UIButton buttonWithType:UIButtonTypeCustom]retain];
        [btn_chapterNew setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn_chapterNew.titleLabel setFont:[UIFont fontWithName:@"TimesNewRomanPSMT" size:17.0f]];
        [btn_chapterNew setBackgroundImage:[UIImage imageNamed:@"btn_box.png"] forState:UIControlStateNormal];
        [btn_chapterNew setBackgroundImage:[UIImage imageNamed:@"btn_box.png"] forState:UIControlStateHighlighted];

        [self.contentView addSubview:btn_chapterNew];
        */
		/*
		nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 260, 30)];
		[nameLabel setFont:[UIFont fontWithName:@"Courier-Bold" size:18.0]];
		[nameLabel setBackgroundColor:[UIColor clearColor]];
		
		[self.contentView addSubview:nameLabel];
		
		[nameLabel release];

		
		discriptionLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 25, 260, 30)];
		[discriptionLabel setFont:[UIFont fontWithName:@"Courier" size:13.0]];
		[discriptionLabel setBackgroundColor:[UIColor clearColor]];
		
		[self.contentView addSubview:discriptionLabel];
		
		[discriptionLabel release];
		
*/
        // Initialization code.
    }
    return self;
}


-(void)layoutSubviews
{

    if(obj_appdelegate.isIpad)
    {
        //imgView.frame = CGRectMake(625, 10, 30, 30);
        if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)
        {
            lbl_startEndDate.frame = CGRectMake(330, 6, 270, 22);
            imgviewAccessory.frame = CGRectMake(880, 70, 20, 20);

        }
        else
        {
            lbl_startEndDate.frame = CGRectMake(215, 6, 270, 22);
            imgviewAccessory.frame = CGRectMake(625, 70, 20, 20);
        }
    }
    else
    {
        //imgView.frame = CGRectMake(249, 10, 30, 30);
        lbl_startEndDate.frame = CGRectMake(5, 3, 270, 22);
        imgviewAccessory.frame = CGRectMake(270, 70, 20, 20);

    }
	//imgView.frame = CGRectMake(5, 10, 40, 40);
    /*
    if(obj_appdelegate.isIpad)
    {
        if(([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight))
        {
            swtch_alarm.frame = CGRectMake(845, 15, 94, 27);
            btn_dateandtime.frame = CGRectMake(690, 15, 230, 30);
             lbl_sound.frame = CGRectMake(780, 12, 100, 30);
             txt_alarmName.frame = CGRectMake(680, 17, 210, 30);
        }
        else
        {
            swtch_alarm.frame = CGRectMake(590, 15, 94, 27);
            btn_dateandtime.frame = CGRectMake(440, 15, 230, 30);
            lbl_sound.frame = CGRectMake(534, 12, 100, 30);
             txt_alarmName.frame = CGRectMake(454, 17, 210, 30);
        }
    }
    else
    {*//*
        swtch_alarm.frame = CGRectMake(210, 15, 94, 27);
        btn_dateandtime.frame = CGRectMake(160, 15, 130, 30);
        lbl_sound.frame = CGRectMake(170, 12, 100, 30);
        txt_alarmName.frame = CGRectMake(180, 12, 110, 30);*/
  //  }
    [super layoutSubviews];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}


- (void)dealloc {
    [super dealloc];
}


@end
