//
//  propertyViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "propertyViewController.h"
#import "AddPropertyViewController.h"
#import "AddPropertyDetailViewController.h"
#import "EquityViewController.h"
@implementation propertyViewController

//- (id)initWithStyle:(UITableViewStyle)style
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)dealloc
{
    [super dealloc];
    [tblView release];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class])); 
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    if(appDelegate.isIOS7)
    {
        tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25, tblView.frame.size.width, tblView.frame.size.height+25);
        [tblView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
    }

    
    
    self.navigationItem.title = @"PROPERTY";
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_clicked)];
       appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSLog(@"\n rect = %@",NSStringFromCGRect(tblView.frame));
    NSLog(@"\n self rect = %@",NSStringFromCGRect(self.view.frame));
    
    
    
    
    
    /*
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_clicked)] autorelease];
    */
    
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
        UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator1.width = -12;
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
        
    }
    else
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
    }

    
    
    
    
    
    if(appDelegate.isIphone5)
    {
        lblNoData.frame = CGRectMake(lblNoData.frame.origin.x, lblNoData.frame.origin.y+30,lblNoData.frame.size.width, lblNoData.frame.size.height);
        imageView.frame =  CGRectMake(imageView.frame.origin.x, imageView.frame.origin.y+30,imageView.frame.size.width, imageView.frame.size.height);
        lblPlus.frame =  CGRectMake(lblPlus.frame.origin.x, lblPlus.frame.origin.y+30,lblPlus.frame.size.width, lblPlus.frame.size.height);
    }
    
    if (appDelegate.isFromIncomeExpenseView >= 0)
    {
    
        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
            UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator.width = -12;
            
            [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
            
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
            UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator1.width = -12;
            
            [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
            
        }
        else
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
            
            
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
            
            
        }

        
        
        
        
        tblView.editing = TRUE;
        tblView.allowsSelectionDuringEditing = TRUE;
    
    }
    else
    {
        
        
        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
            UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator.width = -12;
            
            [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
            
            
        }
        else
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
        }


    }
	tblView.backgroundColor = [UIColor clearColor];
	
    if(appDelegate.isIphone5)
    {
        //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
        
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
        }

        
        
    }
    else
    {
       
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
        }
        
        
    }
    
    
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
	
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,  1024,1024)];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
	
 
    
}
-(void)back_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    

    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    //[self.view addSubview:appDelegate.bottomView];
    
    
    /**/
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    
    [appDelegate selectPropertyDetail];
    //nslog(@"property array ====== %@",appDelegate.propertyDetailArray);
	if([appDelegate.propertyDetailArray count]==0)
	{
		lblNoData.hidden = FALSE;
        imageView.hidden = FALSE;
		lblPlus.hidden = FALSE;
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                lblPlus.center = CGPointMake(284,467); 
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(512,655.0f/2.0f);   
                lblPlus.center = CGPointMake(412,338);
            }
        }

        
        
		[tblView setHidden:TRUE];
		
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0,1024,1024)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];

	}
	else 
    {
        [tblView setHidden:FALSE];
        imageView.hidden = TRUE;

        [lblNoData setHidden:TRUE];
        lblPlus.hidden = TRUE;
        [tblView reloadData];
		
	}
    
    if(appDelegate.isIPad)
    {
        
		[self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    	[self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
	}
    
    [tblView reloadData];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}




-(void)viewWillDisappear:(BOOL)animated
{
   
     nslog(@"\n viewWillDisappear");
    [super viewWillDisappear:animated];
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
    if([appDelegate.propertyDetailArray count]==0)
	{
		lblNoData.hidden = FALSE;
        lblPlus.hidden = FALSE;
        imageView.hidden = FALSE;
		
                
    }
     
    
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
   // [tblView reloadData];    
    
    if(appDelegate.isIPad)
    {
        
                [appDelegate manageViewControllerHeight];
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
            lblNoData.center = CGPointMake(384,911.0f/2.0f);   
            lblPlus.center = CGPointMake(284,467); 
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
            lblNoData.center = CGPointMake(512,655.0f/2.0f);
            lblPlus.center = CGPointMake(412,338); 
        }
        
    }

}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

#pragma mark -

-(IBAction)add_clicked
{
    
    
    nslog(@"\n appDelegate.propertyDetailArray= %@ \n",appDelegate.propertyDetailArray);
    
    
    for (int i=0;i<[appDelegate.propertyDetailArray count];i++)
    {
        //-(void)UpdateAgentInfo:(NSMutableArray *)agentType rowid:(int)rowid
        
        NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:[appDelegate.propertyDetailArray objectAtIndex:i], nil];
        appDelegate.isPropertyImageUpdate = FALSE;
        [appDelegate updatePropertyDetail:array rowid:[[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        [array release];
		
    }
    [appDelegate selectPropertyDetail];
    
    appDelegate.isProperty = -1;
    
    AddPropertyDetailViewController *addProperty = [[AddPropertyDetailViewController alloc]initWithNibName:@"AddPropertyDetailViewController" bundle:nil];
    [self.navigationController pushViewController:addProperty animated:YES];
    [addProperty release];
}
-(void)cancel_clicked
{
    
    nslog(@"\n appDelegate.propertyDetailArray = %@ \n",appDelegate.propertyDetailArray);
    for (int i=0;i<[appDelegate.propertyDetailArray count];i++)
    {
        //-(void)UpdateAgentInfo:(NSMutableArray *)agentType rowid:(int)rowid
        
        NSString*leaseStartDate = [[appDelegate.propertyDetailArray objectAtIndex:i] objectForKey:@"leasestart"];
         NSString*leaseEndDate = [[appDelegate.propertyDetailArray objectAtIndex:i] objectForKey:@"leaseend"];

         

        leaseStartDate = [appDelegate fetchDateInCommonFormate:leaseStartDate];
        leaseEndDate = [appDelegate fetchDateInCommonFormate:leaseEndDate];
        
        if(leaseStartDate != nil)
        {
            [[appDelegate.propertyDetailArray objectAtIndex:i] setObject:leaseStartDate forKey:@"leasestart"];
    
        }
        else
        {
            [[appDelegate.propertyDetailArray objectAtIndex:i] setObject:@"" forKey:@"leasestart"];
        }
        if(leaseEndDate != nil)
        {
            [[appDelegate.propertyDetailArray objectAtIndex:i] setObject:leaseEndDate forKey:@"leaseend"];
        }
        else
        {
            [[appDelegate.propertyDetailArray objectAtIndex:i] setObject:@"" forKey:@"leaseend"];
        }
        
        
        
        
        NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:[appDelegate.propertyDetailArray objectAtIndex:i], nil];
        appDelegate.isPropertyImageUpdate = FALSE;
        [appDelegate updatePropertyDetail:array rowid:[[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        [array release];
		
    }
    [appDelegate selectPropertyDetail];

    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}




- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
        if (appDelegate.isFromIncomeExpenseView >= 0)
        {
            return @"Note: 1st entry is default entry";
        }
    return @"";
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([appDelegate.propertyDetailArray count]>0)
    {
        return [appDelegate.propertyDetailArray count];
    }
    else
    {
        return 0;
    }
        
}

/*
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(tintColor)]) {
        if (tableView == tblView) {    // self.tableview
            CGFloat cornerRadius = 5.f;
            cell.backgroundColor = UIColor.clearColor;
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CGMutablePathRef pathRef = CGPathCreateMutable();
            CGRect bounds = CGRectInset(cell.bounds, 5, 0);
            BOOL addLine = NO;
            if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            } else if (indexPath.row == 0) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }
            layer.path = pathRef;
            CFRelease(pathRef);
            layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
            
            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds)+5, bounds.size.height-lineHeight, bounds.size.width-5, lineHeight);
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }
            UIView *testView = [[UIView alloc] initWithFrame:bounds];
            [testView.layer insertSublayer:layer atIndex:0];
            testView.backgroundColor = UIColor.clearColor;
            cell.backgroundView = testView;
        }
    }
}
*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UILabel *label;
    UIImageView *cellImgView;
    
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (cell == nil) 
    if (1) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        label = [[UILabel alloc]initWithFrame:CGRectMake(50, 7, 500, 30)];
        
        
        if(appDelegate.isIOS7)
        {
             [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        }
        
       
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                label.frame = CGRectMake(50, 7, 500, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)

            {
                label.frame = CGRectMake(50, 7, 750, 30);    
            }
            
            
        }
        else
        {
            label.frame = CGRectMake(50, 7, 150, 30);    
        }
        
        
        
        
        
        label.backgroundColor = [UIColor clearColor];
        [label setTextAlignment:UITextAlignmentLeft];
        label.tag = 3000;
        label.font = [UIFont systemFontOfSize:15.0];
        [cell.contentView addSubview:label];
        
        cellImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
        cellImgView.tag = 6000;
        [cell.contentView addSubview:cellImgView];
        [label release];
        [cellImgView release];
        
    }
    
    else 
    {
        label = (UILabel *)[cell.contentView viewWithTag:3000];
        cellImgView = (UIImageView *)[cell.contentView viewWithTag:6000];
                            
    }
    cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //
  //  cell.textLabel.font = [UIFont systemFontOfSize:15.0];
    // Configure the cell...
    if ([appDelegate.propertyDetailArray count]>0)
    {

        label.text = [[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"0"];
        cellImgView.image = [[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"image"];

    
    }
       if (appDelegate.isFromIncomeExpenseView < 0)
    {
        if ([cell.textLabel.text isEqualToString:appDelegate.incomeProperty])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            oldIndex = indexPath;
            [oldIndex retain];
        }
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [self moveFromOriginal:fromIndexPath.row toNew:toIndexPath.row];
    
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    nslog(@"\n appDelegate.propertyDetailArray = %@",appDelegate.propertyDetailArray);
    
    
    
    
    
    for (int i=0;i<[appDelegate.propertyDetailArray count];i++)
    {
        //-(void)UpdateAgentInfo:(NSMutableArray *)agentType rowid:(int)rowid
        
        NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:[appDelegate.propertyDetailArray objectAtIndex:i], nil];
        appDelegate.isPropertyImageUpdate = FALSE;
        [appDelegate updatePropertyDetail:array rowid:[[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        [array release];
		
    }
    [appDelegate selectPropertyDetail];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please note that all of the associated values in Equity and Reminders will be deleted. Are you sure?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alert.tag = indexPath.row;
    [alert show];
    [alert release];
      
    
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
   
    
    if (buttonIndex == 0)
    {
        nslog(@"alert tag === %d",alertView.tag);
        
        nslog(@"button index 1");
    }
    else if (buttonIndex == 1)
    {
        [appDelegate selectReminderByProperty:[[appDelegate.propertyDetailArray objectAtIndex:alertView.tag]valueForKey:@"0"]];
        
        [appDelegate deletePropertyDetail:[[[appDelegate.propertyDetailArray objectAtIndex:alertView.tag]valueForKey:@"rowid"]intValue]];
        
        [appDelegate deleteReminderByProperty:[[appDelegate.propertyDetailArray objectAtIndex:alertView.tag]valueForKey:@"0"]];
        
        [appDelegate DeleteincomeExpenseByProperty:[[appDelegate.propertyDetailArray objectAtIndex:alertView.tag]valueForKey:@"0"]];
        for (int i =0; i<[appDelegate.reminderArrayByProperty count];i++)
        {
            [self cancelAlarm:[[[appDelegate.reminderArrayByProperty objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        }
        
        [appDelegate.propertyDetailArray removeObjectAtIndex:alertView.tag];
        [appDelegate selectPropertyDetail];
        if([appDelegate.propertyDetailArray count]==0)
        {
            lblNoData.hidden = FALSE;
            imageView.hidden = FALSE;
            lblPlus.hidden = FALSE;
            
            
            if(appDelegate.isIPad)
            {
            
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
                    lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                    lblPlus.center = CGPointMake(284,467); 
                }
                else
                {
                    imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
                    lblNoData.center = CGPointMake(494,655.0f/2.0f);   
                    lblPlus.center = CGPointMake(393,338); 
                }
            }
            
            
            [tblView setHidden:TRUE];
            //[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
            [imgView setFrame:CGRectMake(0, 0,1024,1024)];
            //  [imgView setContentMode:UIViewContentModeScaleToFill];
            [self.view addSubview:imgView];
            [self.view sendSubviewToBack:imgView];
            
            [imgView release];
            
            
        }
        else {
            [tblView setHidden:FALSE];
            imageView.hidden = TRUE;
            
            [lblNoData setHidden:TRUE];
            lblPlus.hidden = TRUE;
            [tblView reloadData];
            
        }
        [tblView reloadData];
    }
}

-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew 
{
    
    nslog(@"\n  appDelegate.propertyDetailArray in moveFromOriginal = %@", appDelegate.propertyDetailArray);
    
    
    NSMutableArray *rowIdArray = [[NSMutableArray alloc] initWithCapacity:10];
    nslog(@"\n before moving = %@",appDelegate.propertyDetailArray);
    for (int i=0;i<[appDelegate.propertyDetailArray count];i++)
    {
        [rowIdArray addObject:[[appDelegate.propertyDetailArray objectAtIndex:i] valueForKey:@"rowid"]];
    }
    nslog(@"\n rowIdArray  = %@",rowIdArray);

    
   
	tempArray = [[NSMutableArray alloc] initWithArray:appDelegate.propertyDetailArray];
	id tempObject = [tempArray objectAtIndex:indexOriginal];
	
    [tempArray removeObjectAtIndex:indexOriginal];
    [tempArray insertObject:tempObject atIndex:indexNew];
	appDelegate.propertyDetailArray = tempArray;
    
    
    
    nslog(@"\n after moving = %@",appDelegate.propertyDetailArray);
	
    for (int i=0;i<[appDelegate.propertyDetailArray count];i++)
    {
        [[appDelegate.propertyDetailArray objectAtIndex:i] setObject:[rowIdArray objectAtIndex:i] forKey:@"rowid"];
    }

    nslog(@"\n after moving  and setting index..= %@",appDelegate.propertyDetailArray);

    
    [tempArray release];
    nslog(@"\n  appDelegate.propertyDetailArray in moveFromOriginal = %@", appDelegate.propertyDetailArray);
   
    [rowIdArray release];
    [tblView reloadData];

    
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([appDelegate.propertyDetailArray count]>0)
    {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

// Override to support rearranging the table view.




// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
    // Return NO if you do not want the item to be re-orderable.
    if ([appDelegate.propertyDetailArray count]>0)
    {
        return YES;
    }
    return NO;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    for (int i=0;i<[appDelegate.propertyDetailArray count];i++)
    {
        //-(void)UpdateAgentInfo:(NSMutableArray *)agentType rowid:(int)rowid
        
        NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:[appDelegate.propertyDetailArray objectAtIndex:i], nil];
        appDelegate.isPropertyImageUpdate = FALSE;
        [appDelegate updatePropertyDetail:array rowid:[[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        [array release];
		
    }
    
    
    
    [appDelegate selectPropertyDetail];
    
    if (appDelegate.isFromIncomeExpenseView > 0)
    {
        if ([appDelegate.propertyDetailArray count]>0)
        {
            appDelegate.isProperty = indexPath.row;
            AddPropertyDetailViewController *addProperty = [[AddPropertyDetailViewController alloc]initWithNibName:@"AddPropertyDetailViewController" bundle:nil];
            [self.navigationController pushViewController:addProperty animated:YES];
            [addProperty release];
        }
    }
    else
    {
        
        if ([appDelegate.propertyDetailArray count]>0)
        {
            UITableViewCell *oldCell = [tblView cellForRowAtIndexPath:oldIndex];
            
            oldCell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        UITableViewCell *newCell = [tblView cellForRowAtIndexPath:indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        appDelegate.incomeProperty = [NSString stringWithFormat:@"%@",[[appDelegate.propertyDetailArray objectAtIndex:indexPath.row]valueForKey:@"0"]];
        //            appDelegate.propertyTypeStr = newCell.textLabel.text;
        
        /*Memory
        [appDelegate.incomeProperty retain];
        */
        
        oldIndex = indexPath;
        appDelegate.equityIndex= indexPath.row;
        [self.navigationController popViewControllerAnimated:YES];
       // [equityView release];
    }

    }
   


#pragma mark - 
#pragma remidner alarm cancel method

-(void) cancelAlarm:(int)reminderRowID
{
    
	UIApplication *app = [UIApplication sharedApplication];
	NSArray *alarmArray = [app scheduledLocalNotifications];
    int temp = reminderRowID;
    //    NSString *temp = [NSString stringWithFormat:@"%d",[[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"rowid"]intValue]]];
    
	//nslog(@"Trying to cancel alarm %@ date Type:%@", alarm.alertBody, temp);
	for (int i=0; i<[alarmArray count]; i++) 
    {
		UILocalNotification* oneAlarm = [alarmArray objectAtIndex:i];
		int FinalTemp = [[oneAlarm.userInfo objectForKey:@"Key"]intValue];
        nslog(@"final temp unser info ===%d",FinalTemp);
		if (FinalTemp==temp) 
        {
			nslog(@"Cancelling alarm: %@", oneAlarm.alertBody);
			[app cancelLocalNotification:oneAlarm];
		}
	}
    
}



@end
