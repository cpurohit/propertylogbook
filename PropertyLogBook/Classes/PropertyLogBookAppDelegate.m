//
//  PropertyLogBookAppDelegate.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PropertyLogBookAppDelegate.h"
#import "MasterCheckPassViewController.h"
#import "AddIncomeExpenseViewController.h"
#import "DashBoardViewController.h"
#import "ReceiptViewController.h"
#import <sqlite3.h>

@implementation UITabBarController(UITabBarControllerCategory)
-(BOOL)shouldAutorotateToInterfaceOrientation:


(UIInterfaceOrientation)toInterfaceOrientation
{
    
#if (__IPHONE_OS_VERSION_MAX_ALLOWED >= 30200)
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
	{
        return YES;
	}
	else
	{
		return NO;
	}
#else
	
#endif
    
    
    
}

@end


@interface UINavigationBar (MyCustomNavBar)
@end

@implementation UINavigationBar (MyCustomNavBar)

- (void) drawRect:(CGRect)rect
{
    //[self setTintColor:[UIColor colorWithRed:0.85f green: 0 blue:0 alpha:1]];
    UIImage *barImage = [UIImage imageNamed:@"navigationbar.png"];
    [barImage drawInRect:rect];
}

@end




@implementation PropertyLogBookAppDelegate


@synthesize window, incomeTypeDBArray, expenseTypeArray, reminderTypeArray, reminderRowid, propertyTypeArray, propertyListArray, agentArray, isAgent, isFromPropertyAgent, propertyDetailArray, isIncomeExpenseUpdate, reminderTypeStr, reminderPropertyStr, reminderArray,propertyImage, curCode, numberScreen,reminderRowID,badgeCount;


@synthesize tabBarController, fromProperty, propertyTypeStr, currencyTypeStr, isProperty, propertyAddArray, currencyArray, innerUpdate, agentTypeStr,agent_pk, isIncome, incomeString, incomeProperty, incomeExpenseArray, incomeExpenseIndex, isFromIncomeExpenseView,equityIndex,isPropertyImageUpdate,reminderArrayByProperty, incomeExpenseSummery,decimalScale;

@synthesize divider;

@synthesize is_receipt_image_updated;

@synthesize isIOS7;

/*
@synthesize dash_ipad_expense,dash_ipad_expense_selected,dash_ipad_income_selected,dash_ipad_income,expense_ipad_selected,expense_ipad,income_ipad_selected,income_ipad
,both_ipad_selected,both_ipad;
*/


@synthesize isIPad,currencyWithDollarSymbol,regionDateFormatter;
@synthesize  isTakingBackupFromiCloud;
@synthesize isIphone5;
@synthesize is_receipt_image_choosen,receipt_image_name,receipt_small_image_name,receipt_image,income_expense_is_recursive;
@synthesize temp_receipt_image;
@synthesize isFirst;
@synthesize is_milege_on;
@synthesize isMilege;

@synthesize home_bottom_button,add_income_expense_bottom_button,settings_bottom_button,bottomView,isFromButtonClick;

@synthesize imgView_bottom_tabBar;

@synthesize prefs;
@synthesize current_selected_reminder_type;
@synthesize date_filter_type;

static sqlite3 *database = nil;

@synthesize currentOrientation;

@synthesize receiptViewNavigationController;


/*
@synthesize all_selected,all,barchart_selected,barchart,both_selected,both,dash_expense_selected,dash_expense,dash_income_selected,dash_income,expense_selected,expense,income_selected,income,mtd_selected,mtd,pie_chart,piechart_selected,table_selected,table,ytd_selected,ytd,divider;
*/


@synthesize dashboarad_bottom_button,income_expsne_bottom_button,calculator_bottom_button,reminder_bottom_button,ipad_settings_bottom_button;

@synthesize is_tabBar_added;

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //[Crittercism enableWithAppID:@"51b1a07097c8f25177000007"];
    
    
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    nslog(@"\n screenBounds. = %f",screenBounds.size.height);
    
    
    is_tabBar_added = FALSE;
    
    
    
    
   float sysVersion =  [[[UIDevice currentDevice] systemVersion] floatValue];
    isIOS7 = FALSE;
    
    if(sysVersion>=7.0f)
    {
        isIOS7 = TRUE;
    }
    
    
    
    if(isIPad)
    {
        //some code..
        
       // [self.tabBarController.tabBar setHidden:YES];
        
    }
    else
    {
        [self.tabBarController.tabBar setHidden:YES];
    }
    
    
    is_receipt_image_choosen = FALSE;
    is_receipt_image_updated = FALSE;
    
    receipt_image_name = [[NSString alloc] init];
    
    //temp_receipt_image = [[UIImage alloc] init];
    
    receipt_small_image_name = [[NSString alloc] init];
    receipt_image = [[UIImage alloc] init];
    income_expense_is_recursive = FALSE;
    
    /*Customize segmented control*/
    
    agent_pk = [[NSString alloc] init];
    
    
    NSString *reqSysVer = @"5.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    nslog(@"\n currSysVer = %@",currSysVer);
    
    
    
    
    
    /*
    
    all_selected = [UIImage imageNamed:@"all_selected.png"];
    all = [UIImage imageNamed:@"all.png"];
    
    
    
    pie_chart = [UIImage imageNamed:@"pie.png"];
    piechart_selected = [UIImage imageNamed:@"pie_selected.png"];
    
    
    table_selected = [UIImage imageNamed:@"table_selected.png"];
    table = [UIImage imageNamed:@"table.png"];
    
    
    barchart_selected = [UIImage imageNamed:@"barchart_selected.png"];
    barchart = [UIImage imageNamed:@"barchart.png"];
    */
    
    
    /*Starts income expense:images
    
    expense_selected = [UIImage imageNamed:@"expense_selected.png"];
    expense = [UIImage imageNamed:@"expense.png"];
    
    income_selected = [UIImage imageNamed:@"income_selected.png"];
    income = [UIImage imageNamed:@"income.png"];
    
    both_selected = [UIImage imageNamed:@"both_selected.png"];
    both = [UIImage imageNamed:@"both.png"];
    
    
    
    
    
    expense_ipad_selected= [UIImage imageNamed:@"expense_ipad_selected.png"];
    expense_ipad= [UIImage imageNamed:@"expense_ipad.png"];
    
    income_ipad_selected= [UIImage imageNamed:@"income_ipad_selected.png"];
    income_ipad= [UIImage imageNamed:@"income_ipad.png"];
    
    both_ipad_selected= [UIImage imageNamed:@"both_ipad_selected.png"];
    both_ipad= [UIImage imageNamed:@"both_ipad.png"];
    */
    
    /*Ends income expense*/
    
    
    /*Starts:Dashboard
    
    dash_expense_selected = [UIImage imageNamed:@"expense_dash_selected.png"];
    dash_expense = [UIImage imageNamed:@"expense_dash.png"];
    
    
    dash_income_selected = [UIImage imageNamed:@"income_dash_selected.png"];
    dash_income = [UIImage imageNamed:@"income_dash.png"];
    */
    
    /*
     SUN:0004
     ipad pending.
     
    
    dash_ipad_expense= [UIImage imageNamed:@"dash_ipad_expense.png"];
    dash_ipad_expense_selected= [UIImage imageNamed:@"dash_ipad_expense_selected.png"];;
    
    
    
    dash_ipad_income_selected= [UIImage imageNamed:@"dash_ipad_income_selected.png"];
    dash_ipad_income= [UIImage imageNamed:@"dash_ipad_income.png"];
    
    */
    
    /*Ends:Dashboard*/
    
    /*
    
    mtd_selected = [UIImage imageNamed:@"mtd_selected.png"];
    mtd = [UIImage imageNamed:@"mtd.png"];
    
    ytd_selected = [UIImage imageNamed:@"ytd_selected.png"];
    ytd = [UIImage imageNamed:@"ytd.png"];
    
    */
    
    
    
    divider= [UIImage imageNamed:@"divider.png"];
    
    
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
    {
        
        
        /*
         
         UIImage *segmentUnselected = [[UIImage imageNamed:@"segcontrol_uns.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
         UIImage *segmentSelected = [[UIImage imageNamed:@"segcontrol_sel.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
         
         
         UIImage *segmentUnselected = [UIImage imageNamed:@"segcontrol_uns.png"] ;
         UIImage *segmentSelected = [UIImage imageNamed:@"segcontrol_sel.png"];
         
         
         
         UIImage *segmentSelectedUnselected = [UIImage imageNamed:@"segcontrol_sel-uns.png"];
         UIImage *segUnselectedSelected = [UIImage imageNamed:@"segcontrol_uns-sel.png"];
         UIImage *segmentUnselectedUnselected = [UIImage imageNamed:@"segcontrol_uns-uns.png"];
         
         
         
         
         
         [[UISegmentedControl appearance] setBackgroundImage:segmentUnselected forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
         
         [[UISegmentedControl appearance] setBackgroundImage:segmentSelected forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
         
         
         
         
         [[UISegmentedControl appearance] setDividerImage:segmentUnselectedUnselected forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
         
         /*
         
         [[UISegmentedControl appearance] setDividerImage:segmentSelectedUnselected forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
         
         
         
         [[UISegmentedControl appearance] setDividerImage:segUnselectedSelected forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
         */
         /*Customize segmented control*/
        
    }
    
    
    incomeTypeDBArray = [[NSMutableArray alloc]init];
    expenseTypeArray = [[NSMutableArray alloc]init];
    reminderTypeArray = [[NSMutableArray alloc]init];
    propertyTypeArray = [[NSMutableArray alloc]init];
    propertyAddArray = [[NSMutableArray alloc]init];
    propertyListArray = [[NSMutableArray alloc]init];
    currencyArray = [[NSMutableArray alloc]init];
    agentArray = [[NSMutableArray alloc]init];
    propertyDetailArray = [[NSMutableArray alloc]init];
    incomeExpenseArray = [[NSMutableArray alloc]init];
    reminderArray = [[NSMutableArray alloc]init];
    reminderArrayByProperty = [[NSMutableArray alloc]init];
    incomeExpenseSummery = [[NSMutableArray alloc]init];
    
    
    currencyTypeStr = @"USD ($)";
    
    isTakingBackupFromiCloud = NO;
    
#if (__IPHONE_OS_VERSION_MAX_ALLOWED >= 30200)
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
	{
		isIPad=TRUE;
	}
	else
	{
		isIPad=FALSE;
	}
#else
	isIPad=FALSE;
#endif
    
    
	
        
    
    if(!isIPad && screenBounds.size.height == 568)
    {
        isIphone5 = TRUE;
    }
    else
    {
        isIphone5 = FALSE;
    }
    
    
    
    id currentiCloudToken = [[NSFileManager defaultManager] ubiquityIdentityToken];
    NSLog(@"\n currentiCloudToken = %@ \n",currentiCloudToken);
        

    
    
    
    
    // Override point for customization after application launch.
	
    /*Original DB method..
     [self copyDatabseIfNeeded];
     */
    
    
    regionDateFormatter = [[NSDateFormatter alloc] init];
    [regionDateFormatter setDateStyle:NSDateFormatterMediumStyle];
    nslog(@"\n btn.titleLabel.text =  %d",[regionDateFormatter dateStyle]);
    [regionDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    [self createEditableCopyOfDatabaseIfNeeded];
    
    [self selectCurrencyIndex];
    
    //[self selectNewCurrencyIndex];
    
    currencyWithDollarSymbol = [[NSMutableArray alloc] init];
    [currencyWithDollarSymbol addObject:@"XCD"];
    [currencyWithDollarSymbol addObject:@"AUD"];
    [currencyWithDollarSymbol addObject:@"BSD"];
    [currencyWithDollarSymbol addObject:@"BBD"];
    [currencyWithDollarSymbol addObject:@"BZD"];
    [currencyWithDollarSymbol addObject:@"BND"];
    [currencyWithDollarSymbol addObject:@"CAD"];
    [currencyWithDollarSymbol addObject:@"USD"];
    [currencyWithDollarSymbol addObject:@"FJD"];
    [currencyWithDollarSymbol addObject:@"GYD"];
    [currencyWithDollarSymbol addObject:@"HKD"];
    [currencyWithDollarSymbol addObject:@"JMD"];
    [currencyWithDollarSymbol addObject:@"LRD"];
    [currencyWithDollarSymbol addObject:@"KWD"];
    [currencyWithDollarSymbol addObject:@"NAD"];
    [currencyWithDollarSymbol addObject:@"NZD"];
    [currencyWithDollarSymbol addObject:@"SGD"];
    [currencyWithDollarSymbol addObject:@"SBD"];
    [currencyWithDollarSymbol addObject:@"SRD"];
    [currencyWithDollarSymbol addObject:@"TWD"];
    [currencyWithDollarSymbol addObject:@"TTD"];
    [currencyWithDollarSymbol addObject:@"TVD"];
    
    //[currencyWithDollarSymbol addObject:@"AED"];
    
    

    
    //curCode = @"USD";
    
    isFirst = [[NSUserDefaults standardUserDefaults]boolForKey:@"isFirst"];
    
    //isFirst = FALSE;
    
    
    
    UILocalNotification *localNotif =
    [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotif)
    {
        nslog(@"Recieved Notification %@",localNotif);
		//[[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
        
    }
	badgeCount = [[UIApplication sharedApplication]applicationIconBadgeNumber];
    nslog(@"currency code ===== appdelegate === %@",curCode);
    [curCode retain];
	// Set the tab bar controller as the window's root view controller and display.
    
    
    
    
    
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    if (![curCode isEqualToString:@"Default Region Currency"])
    {
        [currencyFormatter setCurrencyCode:curCode];
        
        if([currencyWithDollarSymbol indexOfObject:curCode] != NSNotFound)
        {
            [currencyFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[currencyWithDollarSymbol indexOfObject:curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [currencyFormatter setLocale:locale];
            [locale release];
        }
        
        else if([[currencyFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [currencyFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
        else if([[currencyFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [currencyFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [currencyFormatter setLocale:locale];
            [locale release];
            
        }
        
    }
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    
    
    decimalScale = [currencyFormatter maximumFractionDigits];
    [currencyFormatter release];
    
    imgView_bottom_tabBar = [[UIImageView alloc] init];
    imgView_bottom_tabBar.image = [UIImage imageNamed:@"footer.png"];
    
    
    /*Managing bottom*/
    
    
    
    
    if(isIPad)
    {
        bottomView = [[UIView alloc] initWithFrame:CGRectMake(0,935,768,49)];
        
        if([UIApplication sharedApplication].statusBarOrientation==3 ||[UIApplication sharedApplication].statusBarOrientation==4 )
        {
            bottomView.frame = CGRectMake(0,675,1024,49);
        }
        else
        {
             bottomView.frame = CGRectMake(0,935,768,49);
        }
        
        nslog(@"\n bottomView = %@",bottomView);
        
        UIImageView*bottomBGImageVIew = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"footer.png"]];
        bottomBGImageVIew.frame = CGRectMake(0,0,1024,49);
        
        
        [bottomView addSubview:bottomBGImageVIew];
        [bottomBGImageVIew release];
        
        /*
        dashboarad_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [dashboarad_bottom_button setImage:[UIImage imageNamed:@"ipad_dashboard.png"] forState:UIControlStateNormal];
        [dashboarad_bottom_button setImage:[UIImage imageNamed:@"ipad_dashboard_selected.png"] forState:UIControlStateSelected];
        [dashboarad_bottom_button setTag:0];//only settings view...
        [dashboarad_bottom_button setSelected:TRUE];
        [dashboarad_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:dashboarad_bottom_button];
        
        
        income_expsne_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        
        [income_expsne_bottom_button setTag:1];//only settings view...
        [income_expsne_bottom_button setSelected:FALSE];
        [income_expsne_bottom_button setImage:[UIImage imageNamed:@"ipad_income_expense.png"] forState:UIControlStateNormal];
        [income_expsne_bottom_button setImage:[UIImage imageNamed:@"ipad_income_expense_selected.png"] forState:UIControlStateSelected];
        [income_expsne_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:income_expsne_bottom_button];
        
        
        calculator_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [calculator_bottom_button setTag:2];//only settings view...
        [calculator_bottom_button setSelected:FALSE];
        [calculator_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [calculator_bottom_button setImage:[UIImage imageNamed:@"ipad_calculator.png"] forState:UIControlStateNormal];
        [calculator_bottom_button setImage:[UIImage imageNamed:@"ipad_calculator_selected.png"] forState:UIControlStateSelected];
        
        [self.bottomView addSubview:calculator_bottom_button];
        
        
        reminder_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [reminder_bottom_button setTag:3];//only settings view...
        [reminder_bottom_button setSelected:FALSE];
        [reminder_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [reminder_bottom_button setImage:[UIImage imageNamed:@"ipad_reminder.png"] forState:UIControlStateNormal];
        [reminder_bottom_button setImage:[UIImage imageNamed:@"ipad_reminder_selected.png"] forState:UIControlStateSelected];
        
        
        [self.bottomView addSubview:reminder_bottom_button];
        
        
        
        
        ipad_settings_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [ipad_settings_bottom_button setImage:[UIImage imageNamed:@"ipad_settings.png"] forState:UIControlStateNormal];
        [ipad_settings_bottom_button setImage:[UIImage imageNamed:@"ipad_settings_selected.png"] forState:UIControlStateSelected];
        [ipad_settings_bottom_button setTag:4];
        [ipad_settings_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:ipad_settings_bottom_button];
        
        */
        
        
    }
    else
    {
        
        
        
        if(isIphone5)
        {
            bottomView = [[UIView alloc] initWithFrame:CGRectMake(0,455,320,49)];
        }
        else
        {
            bottomView = [[UIView alloc] initWithFrame:CGRectMake(0,367,320,49)];
        }
        
        
        //add_income_expense_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        
    
        /*
        UIImageView*bottomBGImageVIew = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"footer.png"]];
        bottomBGImageVIew.frame = CGRectMake(0,0,480,49);
        [bottomView addSubview:bottomBGImageVIew];
        [bottomBGImageVIew release];
        
    
        home_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [home_bottom_button setImage:[UIImage imageNamed:@"home_btn_off.png"] forState:UIControlStateNormal];
        [home_bottom_button setImage:[UIImage imageNamed:@"home_btn_on.png"] forState:UIControlStateSelected];
        [home_bottom_button setFrame:CGRectMake(0,0,107,49)];
        [home_bottom_button setTag:0];
        [home_bottom_button setSelected:TRUE];
        [home_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        //[self.window addSubview:home_bottom_button];
        [self.bottomView addSubview:home_bottom_button];
        
        
        
        
        
                
        //[self.bottomView addSubview:add_income_expense_bottom_button];
        
        
        settings_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        
        [settings_bottom_button setImage:[UIImage imageNamed:@"settings_btn_off.png"] forState:UIControlStateNormal];
        [settings_bottom_button setImage:[UIImage imageNamed:@"settings_btn_on.png"] forState:UIControlStateSelected];
        [settings_bottom_button setFrame:CGRectMake(214,0,106,49)];
        [settings_bottom_button setTag:2];
        [settings_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:settings_bottom_button];
        //[self.window addSubview:settings_bottom_button];
        
        */
        
        
        /*Ends:Managing bottom*/
        
        
        
        
        
        
    }
    
    
    prefs = [NSUserDefaults standardUserDefaults];
    
    //milage_unit
    
    if([prefs objectForKey:@"milage_unit"]==nil)
    {
        [prefs setObject:@"Kms" forKey:@"milage_unit"];
    }
    
    
    //is_milage_set
    
    if([prefs objectForKey:@"is_milage_set"]==nil)
    {
        is_milege_on = FALSE;
    }
    else
    {
        is_milege_on = [[prefs objectForKey:@"is_milage_set"] boolValue];
    }
    

    
    
    
    tabBarController.delegate = self;
    
    int PassValue=(int)[self  getPassCodeStatus];
    if (PassValue==1)
    {
        NSString *nib_Name;
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            nib_Name = @"MasterCheckPassViewController_iPad";
        }
        else
        {
            nib_Name=@"MasterCheckPassViewController";
        }
        
        MasterCheckPassViewController *obj_Mas=[[MasterCheckPassViewController alloc] initWithNibName:nib_Name bundle:nil];
        
        
        //NSString *reqSysVer = @"5.1.1";
        NSString *reqSysVer = @"5.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        nslog(@"\n currSysVer = %@",currSysVer);
         
        
        
        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        {
            window.rootViewController=obj_Mas;
        }
        else
        {
            [self.window addSubview:obj_Mas.view];
        }
        
        [obj_Mas release];
        
    }
    else
    {
        window.rootViewController = self.tabBarController;
    }
    //[self.tabBarController.tabBar setHidden:TRUE];
    
    
    
    nslog(@"\n windows frame = %@",NSStringFromCGRect(window.frame));
    nslog(@"\n bottom frame = %@",NSStringFromCGRect(bottomView.frame));
   
    /*Sun:004*/
    
    //[self manageViewControllerHeight];
   
    
    /*
     SUN:004
     PEFERENCES
     */
    
    
    nslog(@"\n date_fileter on cloud = %@ \n",[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"date_filter_type"]);
    
    nslog(@"\n milage_unit on cloud = %@ \n",[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"milage_unit"]);
    
    nslog(@"\n is_milage_set on cloud = %@ \n",[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"is_milage_set"]);
    
    nslog(@"\n PasscodeOn on cloud = %@ \n",[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"PasscodeOn"]);
    
    
    nslog(@"\n PassWordValue on cloud = %@ \n",[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"PassWordValue"]);
    
    
    nslog(@"\n PassWordValueHint on cloud = %@ \n",[[NSUbiquitousKeyValueStore defaultStore] objectForKey:@"PassWordValueHint"]);
    
    
  
    
     
    
    
    
    if([prefs objectForKey:@"date_filter_type"]==nil)
    {
            date_filter_type = 0;
    }
    else
    {
        date_filter_type = [[prefs objectForKey:@"date_filter_type"] intValue];
    }
    
    
    
    
    
    /*
     SUN:004
     PEFERENCES
     */
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    NSString *docsDir = documentsDir;
	NSFileManager *localFileManager=[[NSFileManager alloc] init];
	
	NSDirectoryEnumerator *dirEnum =[localFileManager enumeratorAtPath:docsDir];
    //NSFileManager *fileManager = [NSFileManager defaultManager];
	[localFileManager release];
	
    
    
	NSString *file;
	while (file = [dirEnum nextObject])
	{
        //nslog(@"\n file = %@",[docsDir stringByAppendingPathComponent:file]);
        //NSArray*originalFileNameArray = [file componentsSeparatedByString:@"/"];
        //NSString*originalFileName = [originalFileNameArray objectAtIndex:([originalFileNameArray count]-1)];
        //nslog(@"\n originalFileName = %@",originalFileName);
    }
    
    
    
    
    [self.window makeKeyAndVisible];
    
    
    return YES;
}


-(void)manageViewControllerHeight
{
    
    
    
    
    
    if(isIPad)
    {
        
        

        
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
        {
            
            [imgView_bottom_tabBar setFrame:CGRectMake(0,975,1024,49)];
            

            dashboarad_bottom_button.frame =CGRectMake(0,975,154,49);
            
            calculator_bottom_button.frame =CGRectMake(154,975,154,49);
            income_expsne_bottom_button.frame =CGRectMake(308,975,154,49);
            reminder_bottom_button.frame =CGRectMake(462,975,154,49);
            ipad_settings_bottom_button.frame =CGRectMake(616,975,154,49);
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4 )
        {
            
            [imgView_bottom_tabBar setFrame:CGRectMake(0,719,1024,49)];
            
            [dashboarad_bottom_button setFrame:CGRectMake(0,719,205,49)];
            [income_expsne_bottom_button setFrame:CGRectMake(410,719,205,49)];
            [calculator_bottom_button setFrame:CGRectMake(205,719,205,49)];
            [reminder_bottom_button setFrame:CGRectMake(615,719,205,49)];
            [ipad_settings_bottom_button setFrame:CGRectMake(820,719,205,49)];
            
            
        }


        /*
        if([UIApplication sharedApplication].statusBarOrientation==1 ||[UIApplication sharedApplication].statusBarOrientation==2 )
        {
            bottomView.frame = CGRectMake(0,931,768,49);
            
            dashboarad_bottom_button.frame =CGRectMake(0,0,154,49);
            
            
            
            income_expsne_bottom_button.frame =CGRectMake(308,0,154,49);
            calculator_bottom_button.frame =CGRectMake(154,0,154,49);

            
            
            reminder_bottom_button.frame =CGRectMake(462,0,154,49);
            ipad_settings_bottom_button.frame =CGRectMake(616,0,154,49);
            
            
            
            
            
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation==3 ||[UIApplication sharedApplication].statusBarOrientation==4 )
        {
            bottomView.frame = CGRectMake(0,675,1024,49);
            
            
            
            
            [dashboarad_bottom_button setFrame:CGRectMake(0,0,205,49)];
            
            
            
            
            [income_expsne_bottom_button setFrame:CGRectMake(410,0,205,49)];
            [calculator_bottom_button setFrame:CGRectMake(205,0,205,49)];

            
            [reminder_bottom_button setFrame:CGRectMake(615,0,205,49)];
            [ipad_settings_bottom_button setFrame:CGRectMake(820,0,205,49)];
            
        }
        */
        
        /*
        for(id x in [self.tabBarController.view subviews])
        {
            
            NSLog(@"\n x = %@ \n ",x);
            
            if(![x isKindOfClass:[UITabBar class]])
            {
                
                if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
                {
                    //[(UIView*)x setFrame:CGRectMake(0,0,768,1024)];
                    
                    UIView*view = (UIView*)x;
                    view.frame = CGRectMake(0,0,768,1024);
                    view.backgroundColor = [UIColor redColor];
                    
                }
                else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4 )
                {
                    [(UIView*)x setFrame:CGRectMake(0,0,1024,768)];
                }
                
                
            }
        }
        */

    }
    else
    {
        for(id x in [self.tabBarController.view subviews])
        {
            if(![x isKindOfClass:[UITabBar class]])
            {
                if(isIphone5)
                {
                    [(UIView*)x setFrame:CGRectMake(0,0,320,568)];
                }
                else
                {
                    [(UIView*)x setFrame:CGRectMake(0,0,320,480)];
                }
                
            }
        }
        
        
    }
    
   
    
}



#pragma mark - buttonTabBarPressed
#pragma mark  handling tabbarview click events..
-(void)buttonTabBarPressed:(id)sender
{
    UIButton*temp_button = (UIButton*)sender;
    
    
    NSArray *arr = [tabBarController viewControllers];
    for(UINavigationController *view1 in arr)
    {
        nslog(@"view1 %@",view1);
        [view1 popToRootViewControllerAnimated:NO];
        
        
    }
    
    if(isIPad)
    {
        
        
        [dashboarad_bottom_button setSelected:FALSE];
        [income_expsne_bottom_button setSelected:FALSE];
        [calculator_bottom_button setSelected:FALSE];
        [reminder_bottom_button setSelected:FALSE];
        [ipad_settings_bottom_button setSelected:FALSE];
        
        
        
    }
    else
    {
        [home_bottom_button setSelected:FALSE];
        [add_income_expense_bottom_button setSelected:FALSE];
        [settings_bottom_button setSelected:FALSE];
        
    }
    
    
    nslog(@"\n temp_button.tag = %d",temp_button.tag);
    
    self.tabBarController.selectedIndex = temp_button.tag;
    [sender setSelected:YES];
}


#pragma mark - show_income_expense_button_clicked
#pragma mark showing actionsheet.....

-(void)show_income_expense_button_clicked:(UINavigationController*)navigation_controller
{
    
    if(isTakingBackupFromiCloud)
    {
        
        return;
        
    }
    
    
    temp_navigation_controller = navigation_controller;
    
    UIActionSheet *actionSheet;
    is_milege_on = [prefs boolForKey:@"is_milage_set"];
    if(is_milege_on)
    {

        actionSheet = [[UIActionSheet alloc]initWithTitle:@"Select Income/Expense/Mileage" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Expense" otherButtonTitles:@"Income",@"Mileage", nil];
    }
    else
    {
         actionSheet = [[UIActionSheet alloc]initWithTitle:@"Select Income/Expense" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Expense" otherButtonTitles:@"Income", nil];
    }
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
    [actionSheet release];
    
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    nslog(@"\n buttonIndex = %d",buttonIndex);
    
    
    if(buttonIndex==2 && !is_milege_on)
    {
        return;
    }

    
    
    
    if(buttonIndex==0|| buttonIndex==1 || buttonIndex==2)
    {
        isFromButtonClick = TRUE;
        NSString *nibName = @"";
        nibName = @"AddIncomeExpenseViewController";
        
        isIncomeExpenseUpdate = FALSE;
        is_receipt_image_choosen = FALSE;
        is_receipt_image_updated=FALSE;
        
        
        [home_bottom_button setSelected:TRUE];
        [add_income_expense_bottom_button setSelected:FALSE];
        [settings_bottom_button setSelected:FALSE];
        
        isMilege = FALSE;
        
        if (buttonIndex == 1)
        {
            isIncome = TRUE;
        }
        else if (buttonIndex == 0)
        {
            isIncome = FALSE;
        }
        
        
        
        else if(buttonIndex==2)
        {
            isIncome = FALSE;
            isMilege = TRUE;
        }
        
        AddIncomeExpenseViewController* addIncome = [[AddIncomeExpenseViewController alloc]initWithNibName:nibName bundle:nil];
        [temp_navigation_controller pushViewController:addIncome animated:NO];
        [addIncome release];
       
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}

#pragma mark -

-(int)getPassCodeStatus
{
    prefs=[NSUserDefaults standardUserDefaults];
	int Number=[prefs integerForKey:@"PasscodeOn"];
	if (!Number)
    {
		Number=0;
	}
	return Number;
}

-(void)correctPassword
{
    //window.rootViewController = self.tabBarController;
    
    [window setRootViewController:self.tabBarController];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        nslog(@"buttonindex 0");
    }
    else
    {
        nslog(@"buttonindex 1");
        
        if(isIPad)
        {
            self.tabBarController.selectedIndex = 5;
        }
        else
        {
            self.tabBarController.selectedIndex = 2;
        }
        
        
    }
}

- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif {
    // Handle the notificaton when the app is running
	[[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
    
    //badgeCount = [[UIApplication sharedApplication]applicationIconBadgeNumber];
	badgeCount =1;
    //NSString *meeting = [notif.userInfo valueForKey:@"Key"];
    //[self selectReminder:current_selected_reminder_type];
    [self selectAllReminders];
    
    int reminderRow = [[notif.userInfo valueForKey:@"Key"]intValue];
    
    nslog(@"\n badgeCount = %d \n",badgeCount);
    
    
    /*SUN:004
     NEED TO ASK WHETHER WE WANT TO KEEP LOCAL*/
    
    if([reminderArray count]>0)
    {
        NSString *alertReminder, *alertProperty, *alertNotes;
        for (int i =0;i <[reminderArray count];i++)
        {
            if (reminderRow == [[[reminderArray objectAtIndex:i]valueForKey:@"rowid"]intValue])
            {
                alertReminder = [[reminderArray objectAtIndex:i]valueForKey:@"reminderType"];
                alertNotes = [[reminderArray objectAtIndex:i]valueForKey:@"notes"];
                alertProperty = [[reminderArray objectAtIndex:i]valueForKey:@"property"];
                break;
            }
            
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@",alertReminder] message:[NSString stringWithFormat:@"Property: %@ \n Notes: %@",alertProperty, alertNotes] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];

    }
    
    nslog(@"Recieved Notification %@",notif);
     
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissControllerIfNeeded" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissAboutControllerIfNeeded" object:nil];
    

}



- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    
    
    
    /*
    
    all_selected = [UIImage imageNamed:@"all_selected.png"];
    all = [UIImage imageNamed:@"all.png"];
    
    
    
    pie_chart = [UIImage imageNamed:@"pie.png"];
    piechart_selected = [UIImage imageNamed:@"pie_selected.png"];
    
    
    table_selected = [UIImage imageNamed:@"table_selected.png"];
    table = [UIImage imageNamed:@"table.png"];
    
    
    barchart_selected = [UIImage imageNamed:@"barchart_selected.png"];
    barchart = [UIImage imageNamed:@"barchart.png"];
    */
     
    /*Starts income expense:images
    
    expense_selected = [UIImage imageNamed:@"expense_selected.png"];
    expense = [UIImage imageNamed:@"expense.png"];
    
    income_selected = [UIImage imageNamed:@"income_selected.png"];
    income = [UIImage imageNamed:@"income.png"];
    
    both_selected = [UIImage imageNamed:@"both_selected.png"];
    both = [UIImage imageNamed:@"both.png"];
    
    
    
    
    
    expense_ipad_selected= [UIImage imageNamed:@"expense_ipad_selected.png"];
    expense_ipad= [UIImage imageNamed:@"expense_ipad.png"];
    
    income_ipad_selected= [UIImage imageNamed:@"income_ipad_selected.png"];
    income_ipad= [UIImage imageNamed:@"income_ipad.png"];
    
    both_ipad_selected= [UIImage imageNamed:@"both_ipad_selected.png"];
    both_ipad= [UIImage imageNamed:@"both_ipad.png"];
    */
    
    /*Ends income expense*/
    
    
    /*Starts:Dashboard
    
    dash_expense_selected = [UIImage imageNamed:@"expense_dash_selected.png"];
    dash_expense = [UIImage imageNamed:@"expense_dash.png"];
    
    
    dash_income_selected = [UIImage imageNamed:@"income_dash_selected.png"];
    dash_income = [UIImage imageNamed:@"income_dash.png"];
    */
    
    /*
     SUN:0004
     ipad pending.
     
    
    dash_ipad_expense= [UIImage imageNamed:@"dash_ipad_expense.png"];
    dash_ipad_expense_selected= [UIImage imageNamed:@"dash_ipad_expense_selected.png"];;
    
    
    
    dash_ipad_income_selected= [UIImage imageNamed:@"dash_ipad_income_selected.png"];;
    dash_ipad_income= [UIImage imageNamed:@"dash_ipad_income.png"];;
    */
    
    /*Ends:Dashboard*/
    
    
    /*
    mtd_selected = [UIImage imageNamed:@"mtd_selected.png"];
    mtd = [UIImage imageNamed:@"mtd.png"];
    
    
    
    
    
    ytd_selected = [UIImage imageNamed:@"ytd_selected.png"];
    ytd = [UIImage imageNamed:@"ytd.png"];
    */
     
    divider= [UIImage imageNamed:@"divider.png"];
    
    int PassValue=(int)[self  getPassCodeStatus];
    if (PassValue==1)
    {
        NSString *nib_Name;
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            nib_Name = @"MasterCheckPassViewController_iPad";
        }
        else
        {
            nib_Name=@"MasterCheckPassViewController";
        }
        
        MasterCheckPassViewController *obj_Mas=[[MasterCheckPassViewController alloc] initWithNibName:nib_Name bundle:nil];
        
        
        NSString *reqSysVer = @"5.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        
        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        {
            nslog(@"\n currSysVer -1  = %@",currSysVer);
            window.rootViewController=obj_Mas;
        }
        else
        {
            nslog(@"\n currSysVer - 2 = %@",currSysVer);
            [self.window addSubview:obj_Mas.view];
        }
        [obj_Mas release];
        [self.tabBarController.view setUserInteractionEnabled: FALSE];
        
    }


    
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    
    
        
    //[self.tabBarController.tabBar setHidden:TRUE];

    
    nslog(@"\n applicationDidBecomeActive \n");
    
    
        
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}

#pragma mark -
#pragma mark tabbar Methods



-(void)tabBarController:(UITabBarController *)tabBarController_local didSelectViewController:(UIViewController *)viewController
{
    nslog(@"\n selected index in didSelectViewController = %d",tabBarController_local.selectedIndex);
    
    if(tabBarController_local.selectedIndex == 4)
    {
        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
        badgeCount = 0;
    }
    
    NSArray *arr = [tabBarController viewControllers];
    for(UINavigationController *view1 in arr)
    {
        nslog(@"view1 %@",view1);
        [view1 popToRootViewControllerAnimated:TRUE];
        
        
        /*
        removeTarget:view1 action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
        */
    }
}


- (void)tabBarController:(UITabBarController *)tabBarController willBeginCustomizingViewControllers:(NSArray *)viewControllers NS_AVAILABLE_IOS(3_0)
{
    nslog(@"\n willBeginCustomizingViewControllers = %@",viewControllers);
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController_local shouldSelectViewController:(UIViewController *)viewController
{
    nslog(@"\n viewController = %@",tabBarController_local.selectedViewController);
    
    
    
    
    if([tabBarController_local.selectedViewController isKindOfClass:[DashBoardViewController class]])
    {
        //  return NO;
    }
    
    nslog(@"\n shouldSelectViewController");
    
    if(isTakingBackupFromiCloud)
    {
        return NO;
    }
    else
    {
        return YES;
    }
    return YES;
    
}

#pragma mark -
#pragma mark DataBase Methods

-(void)createEditableCopyOfDatabaseIfNeeded
{
	
	BOOL success;
	NSFileManager *fileManager=[[NSFileManager defaultManager]autorelease];
	NSError *error;
	NSArray *paths= NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	
	success = [fileManager fileExistsAtPath:[[paths objectAtIndex:0] stringByAppendingPathComponent:@"PropertyDB.sqlite"]];
	if(success)
	{
		if(sqlite3_open([[[paths objectAtIndex:0] stringByAppendingPathComponent:@"PropertyDB.sqlite"] UTF8String],&database)!=SQLITE_OK)
		{
			sqlite3_close(database);
		}
        
        /*
         CREATE TABLE "IncomeExpenseTB" ("IncomeExpense" NUMERIC,"amount" TEXT,"date" Date,"notes" TEXT,"property" TEXT,"type" TEXT)
         INSERT INTO "IncomeExpenseTB" SELECT "IncomeExpense","amount","date","notes","property","type" FROM "oXHFcGcd04oXHFcGcd04_IncomeExpenseTB"
         DROP TABLE "oXHFcGcd04oXHFcGcd04_IncomeExpenseTB"
         */
        
        prefs = [NSUserDefaults standardUserDefaults];
        
        NSString *isTableModified = [prefs stringForKey:@"isIncomeExpenseTBModified6"];
        
        nslog(@"\n isIncomeExpenseTBModified6 = %@",isTableModified);
        
        
        if(isTableModified == nil)
        {
            NSString *sql;
            int Value;
            sqlite3_stmt *selectstmt;
            
            /*Stats:IncomeExpenseTB*/
            
            sql = [NSString stringWithFormat:@"ALTER TABLE IncomeExpenseTB RENAME TO IncomeExpenseTB_TEMP"];
            
            Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
            
            if(Value==1)
            {
                NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
            }
            
            if(Value==SQLITE_OK)
            {
                nslog(@"\n table renamed..");
                int success=sqlite3_step(selectstmt);
                nslog(@"\n success = %d",success);
                sqlite3_finalize(selectstmt);
                
                
                
                
                sql = [NSString stringWithFormat:@"CREATE TABLE \"IncomeExpenseTB\" (\"IncomeExpense\" NUMERIC,\"amount\" TEXT,\"date\" Date DEFAULT (NULL) ,\"notes\" TEXT,\"property\" TEXT,\"type\" TEXT,\"isrecursive\" VARCHAR,\"frequency\" TEXT,\"enddate\" DATETIME,\"parentid\" INTEGER,\"subparentid\" INTEGER, \"is_receipt_image_choosen\" INTEGER DEFAULT 0, \"receipt_image_name\" TEXT, \"receipt_small_image_name\" TEXT)"];
                
                
                /*
                 sql = [NSString stringWithFormat:@"CREATE TABLE IncomeExpenseTB (IncomeExpense NUMERIC,amount TEXT,date Date,notes TEXT,property TEXT,type TEXT,isrecursive VARCHAR,frequency TEXT,enddate DATETIME,parentid INTEGER,subparentid INTEGER)"];
                 */
                
                Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                if(Value==1)
                {
                    NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                }
                
                if(Value==SQLITE_OK)
                {
                    nslog(@"\n table renamed..");
                    int success=sqlite3_step(selectstmt);
                    nslog(@"\n success = %d",success);
                    sqlite3_finalize(selectstmt);
                    
                    
                    
                    /*
                     sql = [NSString stringWithFormat:@"INSERT INTO IncomeExpenseTB SELECT IncomeExpense,amount,date,notes,property,type FROM IncomeExpenseTB_TEMP"];
                     */
                    sql = [NSString stringWithFormat:@"INSERT INTO IncomeExpenseTB SELECT IncomeExpense,amount,date,notes,property,type,isrecursive,frequency,enddate,parentid,subparentid,0,'',''FROM IncomeExpenseTB_TEMP"];
                    
                    Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                    if(Value==1)
                    {
                        NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                    }
                    
                    if(Value==SQLITE_OK)
                    {
                        nslog(@"\n data inserted..");
                        int success=sqlite3_step(selectstmt);
                        nslog(@"\n success = %d",success);
                        sqlite3_finalize(selectstmt);
                        
                        
                        sql = [NSString stringWithFormat:@" DROP TABLE IncomeExpenseTB_TEMP"];
                        
                        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                        if(Value==1)
                        {
                            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                        }
                        
                        if(Value==SQLITE_OK)
                        {
                            nslog(@"\n table renamed..");
                            int success=sqlite3_step(selectstmt);
                            nslog(@"\n success = %d",success);
                            sqlite3_finalize(selectstmt);
                            
                            [prefs setObject:@"YES" forKey:@"isIncomeExpenseTBModified6"];
                            
                            /**
                            isTableModified = [prefs stringForKey:@"isIncomeExpenseTBModified6"];
                            nslog(@"\n isTableModified = %@",isTableModified);
                             */
                        }
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                }
                
                
                
            }
            
            /*Ends:IncomeExpenseTB*/
            
            
        }
        
        
        
        /* Start: PropertyDetailDB */
        
        
        prefs = [NSUserDefaults standardUserDefaults];
        NSString *is_PropertyDetailDB_modified6 = [prefs stringForKey:@"is_PropertyDetailDB_modified6"];
        
        nslog(@"\n is_PropertyDetailDB_modified6 = %@",is_PropertyDetailDB_modified6);
        
        if(is_PropertyDetailDB_modified6 == nil)
        {
            NSString *sql;
            int Value;
            sqlite3_stmt *selectstmt;
            
            
            sql = [NSString stringWithFormat:@"ALTER TABLE PropertyDetailDB RENAME TO PropertyDetailDB_TEMP"];
            
            Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
            
            if(Value==1)
            {
                NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
            }
            if(Value==SQLITE_OK)
            {
                nslog(@"\n table renamed..");
                int success=sqlite3_step(selectstmt);
                nslog(@"\n success = %d",success);
                sqlite3_finalize(selectstmt);
                
                
                //
                
                
                sql = [NSString stringWithFormat:@"CREATE TABLE PropertyDetailDB (\"AgentManage\" NUMERIC,\"Currency\" TEXT,\"EstimatedPrice\" TEXT,\"FullyPaid\" NUMERIC,\"MortgageBank\" TEXT,\"ProAddress\" TEXT,\"ProName\" TEXT,\"PropertyAgent\" TEXT,\"PropertyType\" TEXT,\"PurchasePrice\" TEXT,\"RepaymentAmount\" TEXT DEFAULT (0),\"RepaymentFrequency\" TEXT,\"leaseEnd\" TEXT,\"leaseStart\" TEXT,\"onLease\" NUMERIC,\"zLoan\" TEXT,\"zDate\" TEXT,\"zEquity\" TEXT,\"imageCount\" integer,\"total_loan_borrowed\" NUMERIC NOT NULL  DEFAULT (0),\"loan_outstanding\" NUMERIC NOT NULL  DEFAULT (0),\"loan_term\" INTEGER NOT NULL  DEFAULT (0),\"rental_income\" NUMERIC DEFAULT (0),\"tenant_information\" TEXT,\"repayment_term\" TEXT,agentpk TEXT)"];
                
                Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                if(Value==1)
                {
                    NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                }
                
                if(Value==SQLITE_OK)
                {
                    
                    int success=sqlite3_step(selectstmt);
                    nslog(@"\n success = %d",success);
                    sqlite3_finalize(selectstmt);
                    nslog(@"\n table created..");
                    
                    
                      sql = [NSString stringWithFormat:@"INSERT INTO PropertyDetailDB SELECT AgentManage,Currency,EstimatedPrice,FullyPaid,MortgageBank,ProAddress,ProName,PropertyAgent,PropertyType,PurchasePrice,RepaymentAmount,RepaymentFrequency,leaseEnd,leaseStart,onLease,zLoan,zDate,zEquity,imageCount,0,0,0,0,'','','' FROM PropertyDetailDB_TEMP"];
                    
                    
                    Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                    if(Value==1)
                    {
                        NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                    }
                    if(Value==SQLITE_OK)
                    {
                        
                        int success=sqlite3_step(selectstmt);
                        nslog(@"\n success = %d",success);
                        sqlite3_finalize(selectstmt);
                        
                        nslog(@"\n PropertyDetailDB data inserted..");
                        
                        //
                        
                        
                        sql = [NSString stringWithFormat:@" DROP TABLE PropertyDetailDB_TEMP"];
                        
                        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                        if(Value==1)
                        {
                            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                        }
                        if(Value==SQLITE_OK)
                        {
                            
                            int success=sqlite3_step(selectstmt);
                            nslog(@"\n success = %d",success);
                            sqlite3_finalize(selectstmt);
                            nslog(@"\n table dropped..");
                            
                            
                            
                            [prefs setObject:@"YES" forKey:@"is_PropertyDetailDB_modified6"];
                            
                            
                            /**
                            is_PropertyDetailDB_modified6 = [prefs stringForKey:@"is_PropertyDetailDB_modified6"];
                            nslog(@"\n is_PropertyDetailDB_modified6 = %@",is_PropertyDetailDB_modified6);
                             */
                        }

                        
                        
                    
                    }
                    
                    
                    

                }

                
                
                
                
            }
            
            
            
            
        }
        
        
        /* End: PropertyDetailDB */
        
        
        
        /*Start : tbl_default_date_filters*/
        
        prefs = [NSUserDefaults standardUserDefaults];
        NSString *istbl_default_date_filters_added = [prefs stringForKey:@"is_tbl_default_date_filters_added6"];
        
        nslog(@"\n isIncomeExpenseTBModified6 = %@",istbl_default_date_filters_added);
        
        if(istbl_default_date_filters_added == nil)
        {
            NSString *sql;
            int Value;
            sqlite3_stmt *selectstmt;
            
            
            sql = [NSString stringWithFormat:@"CREATE TABLE \"tbl_default_date_filters\" (\"int\" INTEGER PRIMARY KEY  DEFAULT (NULL) ,\"start_date\" Date DEFAULT (1970-01-01) ,\"end_date\" Date DEFAULT (1970-01-01) )"];
            Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
            if(Value==1)
            {
                NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
            }
            
            if(Value==SQLITE_OK)
            {
                nslog(@"\n table created...");
                
                
                nslog(@"\n table renamed..");
                int success=sqlite3_step(selectstmt);
                nslog(@"\n success = %d",success);
                sqlite3_finalize(selectstmt);
                
                
                sql = [NSString stringWithFormat:@"INSERT INTO \"tbl_default_date_filters\" VALUES(1,0,0)"];
                Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                if(Value==1)
                {
                    NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                }
                
                if(Value==SQLITE_OK)
                {
                    nslog(@"\n record ....");
                    
                    
                    nslog(@"\n data inserted..");
                    int success=sqlite3_step(selectstmt);
                    nslog(@"\n success = %d",success);
                    sqlite3_finalize(selectstmt);
                    
                    
                    [prefs setObject:@"YES" forKey:@"is_tbl_default_date_filters_added6"];
                    
                    
                    /**
                    istbl_default_date_filters_added = [prefs stringForKey:@"is_tbl_default_date_filters_added6"];
                    nslog(@"\n istbl_default_date_filters_added = %@",istbl_default_date_filters_added);
                     */
                    
                    
                }
                
            }
            
        }
        
        /*Ends : tbl_default_date_filters*/
        
        
        
        
        //DB already exists...Make compitable for new version...
        
        prefs = [NSUserDefaults standardUserDefaults];
        NSString *isDatesUpdated = [prefs stringForKey:@"isDatesUpdated6"];
        
        
        nslog(@"\n isDatesUpdated = %@",isDatesUpdated);
        if(isDatesUpdated == nil)
        {
            
            NSString *sql;
            int Value;
            sqlite3_stmt *selectstmt;
            if (1)
            {
                /*Starts:PropertyDetailDB*/
                sql = [NSString stringWithFormat:@"SELECT leaseStart,leaseEnd,zDate, rowid FROM PropertyDetailDB"];
                Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                if(Value==1)
                {
                    NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                }
                
                if(Value==SQLITE_OK)
                {
                    
                    while(sqlite3_step(selectstmt) == SQLITE_ROW)
                    {
                        
                        /*
                        NSMutableDictionary *tdict = [[NSMutableDictionary alloc]init];
                        */
                        
                        
                        
                        NSString *leaseStartString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                        
                        
                        NSString *leaseEndString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 1)];
                        
                        NSString *zDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                        
                        int rowid = sqlite3_column_int(selectstmt,3);
                        nslog(@"\n date1 in appDelegate = %@",leaseStartString);
                        nslog(@"\n leaseEndString in appDelegate = %@",leaseEndString);
                        nslog(@"\n zDate in appDelegate = %@",zDate);
                        
                        NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                        
                        //[dtFormatter setDateFormat:@"dd/MM/yyyy"];
                        [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                        
                        NSDate *date;
                        if([leaseStartString length]>0)
                        {
                            date = [dtFormatter dateFromString:leaseStartString];
                            nslog(@"\n date using dateFormatter = %@",date);
                            NSString* tmpDateString = [regionDateFormatter stringFromDate:date];
                            nslog(@"\n date1 = %@",tmpDateString);
                            
                            if(tmpDateString == nil)
                            {
                                NSError *error = NULL;
                                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeDate error:&error];
                                
                                
                                /*
                                NSUInteger numberOfMatches = [detector numberOfMatchesInString:leaseStartString options:0
                                                                                         range:NSMakeRange(0, [leaseStartString length])];
                                
                                */
                                
                                NSArray *matches = [detector matchesInString:leaseStartString
                                                                     options:0
                                                                       range:NSMakeRange(0, [leaseStartString length])];
                                
                                NSDate*tempDate;
                                for (NSTextCheckingResult *match in matches)
                                {
                                    // NSRange matchRange = [match range];
                                    if ([match resultType] == NSTextCheckingTypeDate)
                                    {
                                        tempDate = [match date];
                                        nslog(@"\n date after long operation = %@",tempDate);
                                    }
                                }
                                
                                
                                if([matches count]>0)
                                {
                                    leaseStartString = [dtFormatter stringFromDate:tempDate];
                                    nslog(@"\n final leaseStartString = %@",leaseStartString);
                                    if(leaseStartString != nil)
                                    {
                                        
                                        
                                        
                                        sqlite3_stmt *update_stmt;
                                        
                                        
                                        sql = [NSString stringWithFormat:@"update PropertyDetailDB SET leaseStart = ? WHERE rowid = %d",rowid];
                                        
                                        if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
                                            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                                        
                                        
                                        
                                        sqlite3_bind_text(update_stmt, 1, [leaseStartString UTF8String], -1, SQLITE_TRANSIENT);
                                        
                                        
                                        int success = sqlite3_step(update_stmt);
                                        sqlite3_reset(update_stmt);
                                        sqlite3_finalize(update_stmt);
                                        update_stmt = nil;
                                        
                                        if (success != SQLITE_DONE)
                                        {
                                            NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
                                        }
                                        
                                        
                                        
                                    }
                                }
                            }
                        }
                        
                        if([leaseEndString length]>0)
                        {
                            date = [dtFormatter dateFromString:leaseEndString];
                            nslog(@"\n date using dateFormatter = %@",date);
                            NSString *tmpDateString = [regionDateFormatter stringFromDate:date];
                            nslog(@"\n date1 = %@",leaseEndString);
                            
                            
                            if(tmpDateString == nil)
                            {
                                
                                NSError *error = NULL;
                                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeDate error:&error];
                                
                                /*
                                NSUInteger numberOfMatches = [detector numberOfMatchesInString:leaseEndString options:0
                                                                                         range:NSMakeRange(0, [leaseEndString length])];
                                 
                                 */
                                
                                NSArray *matches = [detector matchesInString:leaseEndString
                                                                     options:0
                                                                       range:NSMakeRange(0, [leaseEndString length])];
                                
                                NSDate*tempDate;
                                for (NSTextCheckingResult *match in matches)
                                {
                                    // NSRange matchRange = [match range];
                                    if ([match resultType] == NSTextCheckingTypeDate)
                                    {
                                        tempDate = [match date];
                                        nslog(@"\n date after long operation = %@",tempDate);
                                    }
                                }
                                
                                
                                if([matches count]>0)
                                {
                                    leaseEndString = [dtFormatter stringFromDate:tempDate];
                                    nslog(@"\n final  leaseEndString = %@",leaseEndString);
                                    if(leaseEndString != nil)
                                    {
                                        sqlite3_stmt *update_stmt;
                                        
                                        
                                        sql = [NSString stringWithFormat:@"update PropertyDetailDB SET leaseEnd = ? WHERE rowid = %d",rowid];
                                        
                                        if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
                                            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                                        
                                        
                                        
                                        sqlite3_bind_text(update_stmt, 1, [leaseEndString UTF8String], -1, SQLITE_TRANSIENT);
                                        
                                        
                                        int success = sqlite3_step(update_stmt);
                                        sqlite3_reset(update_stmt);
                                        sqlite3_finalize(update_stmt);
                                        update_stmt = nil;
                                        
                                        if (success != SQLITE_DONE)
                                        {
                                            NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
                                        }
                                        
                                    }
                                }
                                
                            }
                            
                        }
                        
                        if([zDate length]>0)
                        {
                            date = [dtFormatter dateFromString:zDate];
                            nslog(@"\n date using dateFormatter = %@",date);
                            NSString *tmpDateString = [regionDateFormatter stringFromDate:date];
                            nslog(@"\n tmpDateString = %@",tmpDateString);
                            
                            
                            if(tmpDateString == nil)
                            {
                                
                                NSError *error = NULL;
                                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeDate error:&error];
                                
                                
                                /*
                                NSUInteger numberOfMatches = [detector numberOfMatchesInString:zDate options:0
                                                                                         range:NSMakeRange(0, [zDate length])];
                                 */
                                
                                
                                
                                NSArray *matches = [detector matchesInString:zDate
                                                                     options:0
                                                                       range:NSMakeRange(0, [zDate length])];
                                
                                NSDate*tempDate;
                                for (NSTextCheckingResult *match in matches)
                                {
                                    // NSRange matchRange = [match range];
                                    if ([match resultType] == NSTextCheckingTypeDate)
                                    {
                                        tempDate = [match date];
                                        nslog(@"\n date after long operation = %@",tempDate);
                                    }
                                }
                                
                                
                                if([matches count]>0)
                                {
                                    zDate = [dtFormatter stringFromDate:tempDate];
                                    nslog(@"\n final  leaseEndString = %@",zDate);
                                    if(zDate != nil)
                                    {
                                        sqlite3_stmt *update_stmt;
                                        
                                        
                                        sql = [NSString stringWithFormat:@"update PropertyDetailDB SET zDate = ? WHERE rowid = %d",rowid];
                                        
                                        if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
                                            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                                        
                                        
                                        
                                        sqlite3_bind_text(update_stmt, 1, [zDate UTF8String], -1, SQLITE_TRANSIENT);
                                        
                                        
                                        int success = sqlite3_step(update_stmt);
                                        sqlite3_reset(update_stmt);
                                        sqlite3_finalize(update_stmt);
                                        update_stmt = nil;
                                        
                                        if (success != SQLITE_DONE)
                                        {
                                            NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
                                        }
                                        
                                    }
                                }
                                
                            }
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                }
                
                
                /*Ends:PropertyDetailDB*/
                
                
                /*Starts: incomeExpenseTB*/
                
                sql = [NSString stringWithFormat:@"SELECT date,rowid FROM IncomeExpenseTB"];
                Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                if(Value==1)
                {
                    NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                }
                
                
                if(Value==SQLITE_OK)
                {
                    
                    while(sqlite3_step(selectstmt) == SQLITE_ROW)
                    {
                        
                       
                        
                        NSString *proeprtyDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                        
                        int rowid = sqlite3_column_int(selectstmt,1);
                        nslog(@"\n proeprtyDate in appDelegate = %@",proeprtyDate);
                        
                        NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                        
                        //[dtFormatter setDateFormat:@"dd/MM/yyyy"];
                        [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                        
                        NSDate *date;
                        
                        
                        if([proeprtyDate length]>0)
                        {
                            date = [dtFormatter dateFromString:proeprtyDate];
                            nslog(@"\n date using dateFormatter = %@",date);
                            NSString *tmpDateString = [regionDateFormatter stringFromDate:date];
                            nslog(@"\n date1 = %@",proeprtyDate);
                            
                            
                            if(tmpDateString == nil)
                            {
                                
                                NSError *error = NULL;
                                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeDate error:&error];
                                
                                
                                /*
                                NSUInteger numberOfMatches = [detector numberOfMatchesInString:proeprtyDate options:0
                                                                                         range:NSMakeRange(0, [proeprtyDate length])];
                                
                                 */
                                 
                                NSArray *matches = [detector matchesInString:proeprtyDate
                                                                     options:0
                                                                       range:NSMakeRange(0, [proeprtyDate length])];
                                
                                NSDate*tempDate;
                                for (NSTextCheckingResult *match in matches)
                                {
                                    // NSRange matchRange = [match range];
                                    if ([match resultType] == NSTextCheckingTypeDate)
                                    {
                                        tempDate = [match date];
                                        nslog(@"\n date after long operation = %@",tempDate);
                                    }
                                }
                                
                                
                                if([matches count]>0)
                                {
                                    proeprtyDate = [dtFormatter stringFromDate:tempDate];
                                    nslog(@"\n final  leaseEndString = %@",proeprtyDate);
                                    if(proeprtyDate != nil)
                                    {
                                        sqlite3_stmt *update_stmt;
                                        
                                        
                                        sql = [NSString stringWithFormat:@"UPDATE IncomeExpenseTB SET date = ? WHERE rowid = %d",rowid];
                                        
                                        if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
                                            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                                        
                                        
                                        
                                        sqlite3_bind_text(update_stmt, 1, [proeprtyDate UTF8String], -1, SQLITE_TRANSIENT);
                                        
                                        
                                        int success = sqlite3_step(update_stmt);
                                        sqlite3_reset(update_stmt);
                                        sqlite3_finalize(update_stmt);
                                        update_stmt = nil;
                                        
                                        if (success != SQLITE_DONE)
                                        {
                                            NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
                                        }
                                        
                                    }
                                }
                                
                            }
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                }
                
                /*Ends: incomeExpenseTB*/
                
                
                
                /*Starts: reminderTB*/
                
                sql = [NSString stringWithFormat:@"SELECT date,rowid FROM reminderTB"];
                Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                if(Value==1)
                {
                    NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                }
                
                
                if(Value==SQLITE_OK)
                {
                    
                    while(sqlite3_step(selectstmt) == SQLITE_ROW)
                    {
                        //NSMutableDictionary *tdict = [[NSMutableDictionary alloc]init];
                        
                        
                        NSString *reminderDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                        
                        
                        
                        
                        int rowid = sqlite3_column_int(selectstmt,1);
                        nslog(@"\n proeprtyDate in appDelegate = %@",reminderDate);
                        
                        NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                        
                        //[dtFormatter setDateFormat:@"dd/MM/yyyy"];
                        [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                        
                        NSDate *date;
                        
                        
                        if([reminderDate length]>0)
                        {
                            date = [dtFormatter dateFromString:reminderDate];
                            nslog(@"\n date using dateFormatter = %@",date);
                            NSString *tmpDateString = [regionDateFormatter stringFromDate:date];
                            nslog(@"\n date1 = %@",reminderDate);
                            
                            
                            if(tmpDateString == nil)
                            {
                                
                                NSError *error = NULL;
                                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeDate error:&error];
                                
                                
                                /*
                                NSUInteger numberOfMatches = [detector numberOfMatchesInString:reminderDate options:0
                                                                                         range:NSMakeRange(0, [reminderDate length])];
                                 */
                                
                                
                                
                                NSArray *matches = [detector matchesInString:reminderDate
                                                                     options:0
                                                                       range:NSMakeRange(0, [reminderDate length])];
                                
                                NSDate*tempDate;
                                for (NSTextCheckingResult *match in matches)
                                {
                                    // NSRange matchRange = [match range];
                                    if ([match resultType] == NSTextCheckingTypeDate)
                                    {
                                        tempDate = [match date];
                                        nslog(@"\n date after long operation = %@",tempDate);
                                    }
                                }
                                
                                
                                if([matches count]>0)
                                {
                                    reminderDate = [dtFormatter stringFromDate:tempDate];
                                    nslog(@"\n final  leaseEndString = %@",reminderDate);
                                    if(reminderDate != nil)
                                    {
                                        sqlite3_stmt *update_stmt;
                                        
                                        
                                        sql = [NSString stringWithFormat:@"UPDATE reminderTB SET date = ? WHERE rowid = %d",rowid];
                                        
                                        if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
                                            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                                        
                                        
                                        
                                        sqlite3_bind_text(update_stmt, 1, [reminderDate UTF8String], -1, SQLITE_TRANSIENT);
                                        
                                        
                                        int success = sqlite3_step(update_stmt);
                                        sqlite3_reset(update_stmt);
                                        sqlite3_finalize(update_stmt);
                                        update_stmt = nil;
                                        
                                        if (success != SQLITE_DONE)
                                        {
                                            NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
                                        }
                                        
                                    }
                                }
                                
                            }
                            
                        }
                        
                    }
                }
                
                /*Ends: reminderTB*/
                
                
                
                
            }
            
            
            [prefs setObject:@"YES" forKey:@"isDatesUpdated6"];
            
            /**
            isDatesUpdated = [prefs stringForKey:@"isDatesUpdated6"];
            nslog(@"\n isDatesUpdated = %@",isDatesUpdated);
             */
            
        }
        
        
        
        //new filed for reminderTB
        
        prefs = [NSUserDefaults standardUserDefaults];
        NSString *reminderTB_update7 = [prefs stringForKey:@"reminderTB_update7"];
        if(reminderTB_update7==nil)
        {
            
            
            
            @try
            {
                
                
                NSString *sql;
                int Value;
                sqlite3_stmt *selectstmt;
                
                
                sql = [NSString stringWithFormat:@"ALTER TABLE reminderTB RENAME TO reminderTB_TEMP"];
                
                Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                
                if(Value==1)
                {
                    NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                }
                if(Value==SQLITE_OK)
                {
                    nslog(@"\n table renamed..");
                    int success=sqlite3_step(selectstmt);
                    nslog(@"\n success = %d",success);
                    sqlite3_finalize(selectstmt);
                    
                    
                    //
                    
                    
                    sql = [NSString stringWithFormat:@"CREATE TABLE reminderTB (time TEXT, reminder NUMERIC, date TEXT, notes TEXT, property TEXT, reminderType TEXT, repeat TEXT, \"next_fire_date\" DATETIME);"];
                    
                    Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                    if(Value==1)
                    {
                        NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                    }
                    
                    if(Value==SQLITE_OK)
                    {
                        
                        int success=sqlite3_step(selectstmt);
                        nslog(@"\n success = %d",success);
                        sqlite3_finalize(selectstmt);
                        nslog(@"\n table created..");
                        
                        
                        sql = [NSString stringWithFormat:@"INSERT INTO reminderTB SELECT time,reminder,date,notes,property,reminderType,repeat,date FROM reminderTB_TEMP"];
                        
                        
                        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                        if(Value==1)
                        {
                            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                        }
                        if(Value==SQLITE_OK)
                        {
                            
                            int success=sqlite3_step(selectstmt);
                            nslog(@"\n success = %d",success);
                            sqlite3_finalize(selectstmt);
                            
                            nslog(@"\n reminderTB data inserted..");
                            
                            //
                            
                            
                            sql = [NSString stringWithFormat:@" DROP TABLE reminderTB_TEMP"];
                            
                            Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
                            if(Value==1)
                            {
                                NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
                            }
                            if(Value==SQLITE_OK)
                            {
                                
                                int success=sqlite3_step(selectstmt);
                                nslog(@"\n success = %d",success);
                                sqlite3_finalize(selectstmt);
                                nslog(@"\n table dropped..");
                                
                                
                                
                                [prefs setObject:@"YES" forKey:@"reminderTB_update7"];
                                
                                
                            }
                            
                            
                            
                            
                        }
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                }
                
                
            }
            @catch (NSException *exception)
            {
                nslog(@"\n exception = %@",[exception description]);
            }
            @finally
            {
              //Some code...
            }
            
            
            

            
            
            
            
            
            
            
            
            
            
        }
        
        
        
        
        
        
        //Ends....
        
		return;
	}
	success=[fileManager copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"PropertyDB.sqlite"] toPath:[[paths objectAtIndex:0] stringByAppendingPathComponent:@"PropertyDB.sqlite"] error:&error];
	
    
    
    if(!success)
	{
		NSAssert1(0,@"Failed to create writable database file with message '%@' .",[error localizedDescription]);
	}
	else//First Time Loaded Application....
	{
        prefs = [NSUserDefaults standardUserDefaults];
        //[prefs setObject:@"YES" forKey:@"isTableModified1"];
        [prefs setObject:@"YES" forKey:@"isDatesUpdated6"];
        [prefs setObject:@"YES" forKey:@"isIncomeExpenseTBModified6"];
        [prefs setObject:@"YES" forKey:@"is_PropertyDetailDB_modified6"];
        [prefs setObject:@"YES" forKey:@"is_tbl_default_date_filters_added6"];
        
        [prefs setObject:@"YES" forKey:@"reminderTB_update7"];

        
        
        
        
		if(sqlite3_open([[[paths objectAtIndex:0] stringByAppendingPathComponent:@"PropertyDB.sqlite"] UTF8String],&database)!=SQLITE_OK)
		{
			sqlite3_close(database);
		}
	}
}


-(void)copyDatabseIfNeeded
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSString *dbPath = [self getDBPath];
	BOOL success = [fileManager fileExistsAtPath:dbPath];
	
	if(!success)
    {
		
		NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"PropertyDB.sqlite"];
		success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
		
		if (!success)
			NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
	}
	
}

-(NSString *)getDBPath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
	NSString *documentsDir = [paths objectAtIndex:0];
	return [documentsDir stringByAppendingPathComponent:@"PropertyDB.sqlite"];
}



#pragma mark -
#pragma mark profile image

-(void) storeProfileThumb : (UIImage *) img :(NSString *)name
{
	NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
	NSString *imagePath = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_thumb.png",name]];
	NSData *imageDate = [NSData dataWithData:UIImagePNGRepresentation(img)];
	if(imageDate)
		[imageDate writeToFile: imagePath atomically: YES];
}

-(UIImage *) getProfileThumb : (NSString *)name
{
	//nslog(@"name======%@",name);
	NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
	NSString *imagePath = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_thumb.png",name]];
	//nslog(@"imagepath-%@",imagePath);
	return [UIImage imageWithContentsOfFile:imagePath];
    
}

-(BOOL)Deleteimage:(NSString *)fName
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectoryPath = [paths objectAtIndex:0];
	NSString *myFilePath = [documentsDirectoryPath stringByAppendingPathComponent:fName];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	BOOL success = [fileManager removeItemAtPath:myFilePath error:NULL];
	return success;
}

#pragma mark -
#pragma mark number screen method

-(int)selectNumberOfScreen
{
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"select * from numberScreen"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                numberScreen = sqlite3_column_int(selectstmt, 0);
                
            }
        }
    }
    
    
    return numberScreen;
    
}
-(void)updateNumberOfScreen:(int)numberOfScreen
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
				sql = @"update numberScreen set number = ?";
                
                
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
            
            sqlite3_bind_int(update_stmt, 1, numberOfScreen);
            
            
            int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}


#pragma mark -
#pragma mark currency methods

-(NSString *)selectNewCurrencyIndex
{
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    //if (1)
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"select * from currencyTB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSString *currencyCode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                //nslog(@"\n currencyCode = %@",currencyCode);
                curCode = currencyCode;
                [curCode retain];
            }
        }
    }
    
    return curCode;
}

-(NSString *)selectCurrencyIndex
{
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"select * from currencyTB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSString *currencyCode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                //nslog(@"\n currencyCode = %@",currencyCode);
                curCode = currencyCode;
                [curCode retain];
            }
        }
    }
    
    return curCode;
}
-(void)updateCurrency:(NSString *)currencyCode
{
    
    nslog(@"\n currencyCode = %@",currencyCode);
    
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
				sql = @"update currencyTB set currencyCode = ? where rowid = 1";
                
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
            
            sqlite3_bind_text(update_stmt, 1, [currencyCode UTF8String], -1, SQLITE_TRANSIENT);
            
            
            int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

#pragma mark -
#pragma mark reminder view methods


-(NSMutableArray *)selectAllReminders
{
    if([reminderArray count]>0)
    {
        [reminderArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"SELECT *, rowid FROM reminderTB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                int reminder = sqlite3_column_int(selectstmt, 1);
                NSString *time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                NSString *date =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                
                NSString *notes =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 3)];
                NSString *property =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)];
                NSString *type =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)];
                NSString *repeat =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)];
                
                /*
                NSString *next_fire_date =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 7)];
                */
                
                int rowid = sqlite3_column_int(selectstmt, 8);
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
                [tdict setObject:time forKey:@"time"];
                
                
                nslog(@"\n date1 in appDelegate = %@",date);
                NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *tempDate = [dtFormatter dateFromString:date];
                nslog(@"\n date using dateFormatter = %@",tempDate);
                date = [regionDateFormatter stringFromDate:tempDate];
                nslog(@"\n date1 = %@",date);
                
                
                
                
                
                [tdict setObject:date forKey:@"date"];
				[tdict setObject:notes forKey:@"notes"];
				[tdict setObject:property forKey:@"property"];
				[tdict setObject:type forKey:@"reminderType"];
                [tdict setObject:repeat forKey:@"repeat"];
                [tdict setObject:[NSNumber numberWithInt:reminder] forKey:@"reminder"];
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
                [reminderArray addObject:tdict];
                [dtFormatter release];
                [tdict release];
				
            }
        }
    }
    return reminderArray;
    
}

-(NSMutableArray *)selectReminder:(int)reminder_on_off
{
    if([reminderArray count]>0)
    {
        [reminderArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"SELECT *, rowid FROM reminderTB WHERE reminder = %d",reminder_on_off];
        nslog(@"\n sql = %@ \n ",sql);
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                int reminder = sqlite3_column_int(selectstmt, 1);
                NSString *time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                NSString *date =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                
                NSString *notes =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 3)];
                NSString *property =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)];
                NSString *type =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)];
                NSString *repeat =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)];
                NSString *next_fire_date =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 7)];
                
                int rowid = sqlite3_column_int(selectstmt, 8);
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
                time = [time stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                [tdict setObject:[NSString stringWithFormat:@"%@ %@",date,time] forKey:@"reminderDueDateTime"];
                
                
                [tdict setObject:time forKey:@"time"];
                
                
                nslog(@"\n date1 in appDelegate = %@",date);
                NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                
                NSDate *tempDate = [dtFormatter dateFromString:date];
                nslog(@"\n date using dateFormatter = %@",tempDate);
                date = [regionDateFormatter stringFromDate:tempDate];
                nslog(@"\n date1 = %@",date);
                
                
                
                                
                [tdict setObject:next_fire_date forKey:@"next_fire_date"];
                [tdict setObject:date forKey:@"date"];
				[tdict setObject:notes forKey:@"notes"];
				[tdict setObject:property forKey:@"property"];
				[tdict setObject:type forKey:@"reminderType"];
                [tdict setObject:repeat forKey:@"repeat"];
                [tdict setObject:[NSNumber numberWithInt:reminder] forKey:@"reminder"];
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
                [reminderArray addObject:tdict];
                [dtFormatter release];
                [tdict release];
				
            }
        }
    }
    return reminderArray;
    
}

-(NSMutableArray *)selectReminderByProperty:(NSString *)property
{
    nslog(@"property string === %@",property);
    if([reminderArrayByProperty count]>0)
    {
        [reminderArrayByProperty removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1) {
		
        sql = [NSString stringWithFormat:@"select *, rowid from reminderTB where property = ?"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        sqlite3_bind_text(selectstmt, 1, [property UTF8String], -1, SQLITE_TRANSIENT);
        
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                int reminder = sqlite3_column_int(selectstmt, 1);
                NSString *time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                NSString *date =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                
                NSString *notes =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 3)];
                NSString *property =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)];
                NSString *type =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)];
                NSString *repeat =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)];
                NSString *next_fire_date =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 7)];
                
                
                int rowid = sqlite3_column_int(selectstmt, 8);
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
                [tdict setObject:time forKey:@"time"];
                
                [tdict setObject:date forKey:@"date"];
				[tdict setObject:notes forKey:@"notes"];
				[tdict setObject:property forKey:@"property"];
				[tdict setObject:type forKey:@"reminderType"];
                [tdict setObject:repeat forKey:@"repeat"];
                [tdict setObject:next_fire_date forKey:@"next_fire_date"];
                
                
                
                [tdict setObject:[NSNumber numberWithInt:reminder] forKey:@"reminder"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [reminderArrayByProperty addObject:tdict];
                [tdict release];
				
            }
        }
    }
    return reminderArrayByProperty;
    
}

-(void)AddReminder:(NSString *)type property:(NSString *)property date:(NSString *)date repeat:(NSString *)repeat notes:(NSString *)notes reminder:(int)reminder time:(NSString *)time
{
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		const char *sql = "insert into reminderTB(reminderType, property, date, repeat, notes, reminder, time,next_fire_date) Values(?,?,?,?,?,?,?,?)";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
		
		sqlite3_bind_text(add_stmt, 1, [type UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(add_stmt, 2, [property UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(add_stmt, 3, [date UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(add_stmt, 4, [repeat UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(add_stmt, 5, [notes UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(add_stmt, 6, reminder);
        sqlite3_bind_text(add_stmt, 7, [time UTF8String], -1, SQLITE_TRANSIENT);
        
        
        
        sqlite3_bind_text(add_stmt, 8,[[NSString stringWithFormat:@"%@ %@",[date stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],[time stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] UTF8String]  , -1, SQLITE_TRANSIENT);
        
        
        
        
        
		
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		
        reminderRowID = sqlite3_last_insert_rowid(database);
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
    }
    
}
-(void)UpdateReminder:(NSString *)type property:(NSString *)property date:(NSString *)date repeat:(NSString *)repeat notes:(NSString *)notes reminder:(int)reminder time:(NSString *)time rowid:(int)rowid
{
    nslog(@"row id === %d",rowid);
    nslog(@"reminder ==== %d",reminder);
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
				sql = @"UPDATE reminderTB SET reminderType = ?, property= ?, date= ?, repeat= ?, notes= ?, reminder = ?, time = ?, next_fire_date = ? WHERE rowid = ?";
                
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            sqlite3_bind_text(update_stmt, 1, [type UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 2, [property UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 3, [date UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 4, [repeat UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 5, [notes UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 6, reminder);
            sqlite3_bind_text(update_stmt, 7, [time UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 8,[[NSString stringWithFormat:@"%@ %@",[date stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],[time stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 9, rowid);
            
            
            int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
    
}

-(void)UpdateReminderNextFireDate:(NSString *)nextFireDate  rowid:(int)rowid
{
    nslog(@"row id === %d",rowid);
    nslog(@"nextFireDate ==== %@",nextFireDate);
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
				sql = @"UPDATE reminderTB SET next_fire_date = ? WHERE rowid = ?";
                
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            sqlite3_bind_text(update_stmt, 1, [nextFireDate UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 2, rowid);
            
            
            int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    

}

-(void)DeleteReminder:(int)rowid
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM reminderTB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

-(void)deleteReminderByProperty:(NSString *)property
{
    nslog(@"\n deleteReminderByProperty = %@ ",property);
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM reminderTB WHERE property = ?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
            sqlite3_bind_text(deleteSatement, 1, [property UTF8String], -1, SQLITE_TRANSIENT);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}
#pragma mark -
#pragma mark Converting Date According to region Format

-(NSString*)fetchDateInCommonFormate:(NSString*)stringToConvert
{
    
    NSDate*tempDate = [regionDateFormatter dateFromString:stringToConvert];
    
    
    NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
    [dtFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dtFormatter stringFromDate:tempDate];
    nslog(@"\n date using dateFormatter = %@",dateString);
    [dtFormatter release];
    
    return dateString;
}



#pragma mark -
#pragma mark income expense detail

-(NSMutableArray *)selectIncomeExpense
{
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    
    if([incomeExpenseArray count]>0)
    {
        [incomeExpenseArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"select *, rowid from IncomeExpenseTB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                int incomeExpense = sqlite3_column_int(selectstmt, 0);
                
                NSString *amount =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 1)];
                NSString *date1 =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                NSString *notes =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 3)];
                NSString *property =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)];
                NSString *type =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)];
                
                
                NSString *isrecursive =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)];
                NSString *frequency =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,7)];
                NSString *enddate =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 8)];
                NSString *parentid =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)];
                
                int is_receipt_image_choosen_local = sqlite3_column_int(selectstmt,11);
                NSString *receipt_image_name_local =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 12)];
                NSString *receipt_small_image_name_local =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 13)];
                
                
                
                
                int rowid = sqlite3_column_int(selectstmt,14);
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				
                amount = [amount stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                
                
                [tdict setObject:[NSNumber numberWithInt:is_receipt_image_choosen_local] forKey:@"is_receipt_image_choosen"];
                [tdict setObject:receipt_image_name_local forKey:@"receipt_image_name"];
                [tdict setObject:receipt_small_image_name_local forKey:@"receipt_small_image_name"];
                
                
                [tdict setObject:amount forKey:@"amount"];
                
                
                
                nslog(@"\n date1 in appDelegate = %@",date1);
                NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *date = [dtFormatter dateFromString:date1];
                nslog(@"\n date using dateFormatter = %@",date);
                
                /**
                date1 = [regionDateFormatter stringFromDate:date];
                nslog(@"\n date1 = %@",date1);
                */
                
                
                
                
                
                [tdict setObject:date forKey:@"date"];
				[tdict setObject:notes forKey:@"notes"];
				[tdict setObject:property forKey:@"property"];
				[tdict setObject:type forKey:@"type"];
                
                [tdict setObject:isrecursive forKey:@"isrecursive"];
                [tdict setObject:frequency forKey:@"frequency"];
                
                nslog(@"\n enddate = %@",enddate);
                if(enddate != nil)
                {
                    if([[enddate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]>0)
                    {
                        NSDate*date = [dtFormatter dateFromString:enddate];
                        [tdict setObject:date forKey:@"enddate"];
                    }
                    else
                    {
                        [tdict setObject:@"" forKey:@"enddate"];
                    }
                }
                else
                {
                    [tdict setObject:@"" forKey:@"enddate"];
                }
                
                
                [tdict setObject:parentid forKey:@"parentid"];
                [tdict setObject:[NSNumber numberWithInt:incomeExpense] forKey:@"incomeExpense"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [incomeExpenseArray addObject:tdict];
                [dtFormatter release];
                [tdict release];
				
            }
        }
    }
    
    
    [numberFormatter release];
    
    nslog(@"income expense array in app delegate === %@",incomeExpenseArray);
    return incomeExpenseArray;
    
}



-(NSMutableArray *)selectIncomeExpenseByProperty:(NSString *)property
{
    //select statement starts from 0,bind starts from 1
    
    if([incomeExpenseSummery count]>0)
    {
        [incomeExpenseSummery removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"select *, rowid from IncomeExpenseTB WHERE property = ? "];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
        sqlite3_bind_text(selectstmt, 1, [property UTF8String], -1, SQLITE_TRANSIENT);
        
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                int incomeExpense = sqlite3_column_int(selectstmt, 0);
                
                NSString *amount =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 1)];
                NSString *date1 =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                NSString *notes =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 3)];
                NSString *property =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)];
                NSString *type =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)];
                
                
                
                NSString *isrecursive =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)];
                NSString *frequency =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,7)];
                NSString *enddate =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 8)];
                NSString *parentid =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)];
                
                
                
                
                int is_receipt_image_choosen_local = sqlite3_column_int(selectstmt,11);
                NSString *receipt_image_name_local =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 12)];
                NSString *receipt_small_image_name_local =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 13)];
                int rowid = sqlite3_column_int(selectstmt,14);
                
                
                
                
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				
                
                
                [tdict setObject:[NSNumber numberWithInt:is_receipt_image_choosen_local] forKey:@"is_receipt_image_choosen"];
                [tdict setObject:receipt_image_name_local forKey:@"receipt_image_name"];
                [tdict setObject:receipt_small_image_name_local forKey:@"receipt_small_image_name"];
                
                
                
                [tdict setObject:amount forKey:@"amount"];
                
                nslog(@"\n date1 in appDelegate = %@",date1);
                
                nslog(@"\n date1 in appDelegate = %@",date1);
                NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *date = [dtFormatter dateFromString:date1];
                nslog(@"\n date using dateFormatter = %@",date);
                date1 = [regionDateFormatter stringFromDate:date];
                nslog(@"\n date1 = %@",date1);
                date = [regionDateFormatter dateFromString:date1];
                /*
                 NSDate *date;
                 NSError *error = NULL;
                 NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeDate
                 error:&error];
                 
                 
                 NSUInteger numberOfMatches = [detector numberOfMatchesInString:date1
                 options:0
                 range:NSMakeRange(0, [date1 length])];
                 
                 
                 
                 NSArray *matches = [detector matchesInString:date1
                 options:0
                 range:NSMakeRange(0, [date1 length])];
                 
                 
                 for (NSTextCheckingResult *match in matches)
                 {
                 // NSRange matchRange = [match range];
                 if ([match resultType] == NSTextCheckingTypeDate)
                 {
                 date = [match date];
                 nslog(@"\n date after long operation = %@",date);
                 }
                 }
                 
                 
                 date1 = [regionDateFormatter stringFromDate:date];
                 
                 
                 date = [regionDateFormatter dateFromString:date1];
                 
                 nslog(@"\n date = %@",date);
                 */
                
                [tdict setObject:date forKey:@"date"];
				[tdict setObject:notes forKey:@"notes"];
				[tdict setObject:property forKey:@"property"];
				[tdict setObject:type forKey:@"type"];
                
                
                
                [tdict setObject:isrecursive forKey:@"isrecursive"];
                [tdict setObject:frequency forKey:@"frequency"];
                
                nslog(@"\n enddate = %@",enddate);
                if(enddate != nil)
                {
                    if([[enddate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]>0)
                    {
                        NSDate*date = [dtFormatter dateFromString:enddate];
                        [tdict setObject:date forKey:@"enddate"];
                    }
                    else
                    {
                        [tdict setObject:@"" forKey:@"enddate"];
                    }
                }
                else
                {
                    [tdict setObject:@"" forKey:@"enddate"];
                }
                
                
                [tdict setObject:parentid forKey:@"parentid"];
                
                
                
                [tdict setObject:[NSNumber numberWithInt:incomeExpense] forKey:@"incomeExpense"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [incomeExpenseSummery addObject:tdict];
                
                [tdict release];
                [dtFormatter release];
				
            }
        }
    }
    
    nslog(@"income expense array in app delegate (selectIncomeExpenseByProperty) === %@",incomeExpenseSummery);
    return incomeExpenseSummery;
    
}

-(NSMutableArray *)selectIncomeExpenseByPropertyForRange:(NSString *)property startDate:(NSString *)startDate endDate:(NSString *)endDate
{
    //select statement starts from 0,bind starts from 1
    
    if([incomeExpenseSummery count]>0)
    {
        [incomeExpenseSummery removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
       
        
        sql = [NSString stringWithFormat:@"select *, rowid from IncomeExpenseTB WHERE property = ? AND date Between '%@' And '%@'",startDate,endDate];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
        sqlite3_bind_text(selectstmt, 1, [property UTF8String], -1, SQLITE_TRANSIENT);
        
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                int incomeExpense = sqlite3_column_int(selectstmt, 0);
                
                NSString *amount =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 1)];
                NSString *date1 =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                NSString *notes =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 3)];
                NSString *property =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)];
                NSString *type =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)];
                
                
                
                NSString *isrecursive =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)];
                NSString *frequency =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,7)];
                NSString *enddate =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 8)];
                NSString *parentid =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)];
                
                
                
                
                int is_receipt_image_choosen_local = sqlite3_column_int(selectstmt,11);
                NSString *receipt_image_name_local =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 12)];
                NSString *receipt_small_image_name_local =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 13)];
                int rowid = sqlite3_column_int(selectstmt,14);
                
                
                
                
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				
                
                
                [tdict setObject:[NSNumber numberWithInt:is_receipt_image_choosen_local] forKey:@"is_receipt_image_choosen"];
                [tdict setObject:receipt_image_name_local forKey:@"receipt_image_name"];
                [tdict setObject:receipt_small_image_name_local forKey:@"receipt_small_image_name"];
                
                
                
                [tdict setObject:amount forKey:@"amount"];
                
                nslog(@"\n date1 in appDelegate = %@",date1);
                
                nslog(@"\n date1 in appDelegate = %@",date1);
                NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *date = [dtFormatter dateFromString:date1];
                nslog(@"\n date using dateFormatter = %@",date);
                date1 = [regionDateFormatter stringFromDate:date];
                nslog(@"\n date1 = %@",date1);
                date = [regionDateFormatter dateFromString:date1];
                /*
                 NSDate *date;
                 NSError *error = NULL;
                 NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeDate
                 error:&error];
                 
                 
                 NSUInteger numberOfMatches = [detector numberOfMatchesInString:date1
                 options:0
                 range:NSMakeRange(0, [date1 length])];
                 
                 
                 
                 NSArray *matches = [detector matchesInString:date1
                 options:0
                 range:NSMakeRange(0, [date1 length])];
                 
                 
                 for (NSTextCheckingResult *match in matches)
                 {
                 // NSRange matchRange = [match range];
                 if ([match resultType] == NSTextCheckingTypeDate)
                 {
                 date = [match date];
                 nslog(@"\n date after long operation = %@",date);
                 }
                 }
                 
                 
                 date1 = [regionDateFormatter stringFromDate:date];
                 
                 
                 date = [regionDateFormatter dateFromString:date1];
                 
                 nslog(@"\n date = %@",date);
                 */
                
                [tdict setObject:date forKey:@"date"];
				[tdict setObject:notes forKey:@"notes"];
				[tdict setObject:property forKey:@"property"];
				[tdict setObject:type forKey:@"type"];
                
                
                
                [tdict setObject:isrecursive forKey:@"isrecursive"];
                [tdict setObject:frequency forKey:@"frequency"];
                
                nslog(@"\n enddate = %@",enddate);
                if(enddate != nil)
                {
                    if([[enddate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]>0)
                    {
                        NSDate*date = [dtFormatter dateFromString:enddate];
                        [tdict setObject:date forKey:@"enddate"];
                    }
                    else
                    {
                        [tdict setObject:@"" forKey:@"enddate"];
                    }
                }
                else
                {
                    [tdict setObject:@"" forKey:@"enddate"];
                }
                
                
                [tdict setObject:parentid forKey:@"parentid"];
                
                
                
                [tdict setObject:[NSNumber numberWithInt:incomeExpense] forKey:@"incomeExpense"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [incomeExpenseSummery addObject:tdict];
                
                [tdict release];
                [dtFormatter release];
				
            }
        }
    }
    
    nslog(@"income expense array in app delegate (selectIncomeExpenseByProperty) === %@",incomeExpenseSummery);
    return incomeExpenseSummery;
    
}


-(NSMutableArray *)selectIncomeExpenseByMDTOrYDT:(NSString *)MTDOrYTD
{
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    
    if([incomeExpenseArray count]>0)
    {
        [incomeExpenseArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		NSDate*date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSString*dateString = [formatter stringFromDate:date];
        [formatter release];
        NSArray*dateStringArray = [dateString componentsSeparatedByString:@"-"];
        NSString*fromString = [NSString stringWithFormat:@"%@-%@-01",[dateStringArray objectAtIndex:0],[dateStringArray objectAtIndex:1]];
        
        NSString*toString = [NSString stringWithFormat:@"%@-%@-%@",[dateStringArray objectAtIndex:0],[dateStringArray objectAtIndex:1],[dateStringArray objectAtIndex:2]];
        
        if([MTDOrYTD isEqualToString:@"MTD"])
        {
            
            
            /*
             sql = @"select *, rowid from IncomeExpenseTB WHERE date Between strftime('%Y-%m-01', date()) And strftime('%Y-%m-%d', date())";
             */
            
            
            
            sql =[NSString stringWithFormat:@"select *, rowid from IncomeExpenseTB WHERE date Between '%@' And '%@'",fromString,toString];
            
            
            
            
            
        }
        else
        {
            /*
             sql = @"select *, rowid from IncomeExpenseTB WHERE date Between strftime('01/01/\%Y', date()) And strftime('\%d/\%m/\%Y', date())";
             */
            
            fromString = [NSString stringWithFormat:@"%@-01-01",[dateStringArray objectAtIndex:0]];
            
            sql =[NSString stringWithFormat:@"select *, rowid from IncomeExpenseTB WHERE date Between '%@' And '%@'",fromString,toString];
            
            
            
            /*
             sql = @"select *, rowid from IncomeExpenseTB WHERE date Between strftime('%Y-01-01', date()) And strftime('%Y-%m-%d', date())";
             */
            
            
        }
        
        nslog(@"\n sql = %@",sql);
        
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                int incomeExpense = sqlite3_column_int(selectstmt, 0);
                
                NSString *amount =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 1)];
                NSString *date1 =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                NSString *notes =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 3)];
                NSString *property =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)];
                NSString *type =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)];
                
                
                
                NSString *isrecursive =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)];
                NSString *frequency =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,7)];
                NSString *enddate =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 8)];
                NSString *parentid =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)];
                
                
                int is_receipt_image_choosen_local = sqlite3_column_int(selectstmt,11);
                NSString *receipt_image_name_local =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 12)];
                NSString *receipt_small_image_name_local =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 13)];
                
                
                
                
                
                
                
                
                
                
                
                int rowid = sqlite3_column_int(selectstmt,14);
                
                
                amount = [amount stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                
                
                
                
                
                
                
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				[tdict setObject:amount forKey:@"amount"];
                
                nslog(@"\n date1 in appDelegate = %@",date1);
                NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *date = [dtFormatter dateFromString:date1];
                nslog(@"\n date using dateFormatter = %@",date);
                
                /**
                date1 = [regionDateFormatter stringFromDate:date];
                nslog(@"\n date1 = %@",date1);
                */
                
                [tdict setObject:date forKey:@"date"];
				[tdict setObject:notes forKey:@"notes"];
				[tdict setObject:property forKey:@"property"];
				[tdict setObject:type forKey:@"type"];
                
                [tdict setObject:[NSNumber numberWithInt:is_receipt_image_choosen_local] forKey:@"is_receipt_image_choosen"];
                [tdict setObject:receipt_image_name_local forKey:@"receipt_image_name"];
                [tdict setObject:receipt_small_image_name_local forKey:@"receipt_small_image_name"];
                
                
                
                
                [tdict setObject:isrecursive forKey:@"isrecursive"];
                [tdict setObject:frequency forKey:@"frequency"];
                
                nslog(@"\n enddate = %@",enddate);
                if(enddate != nil)
                {
                    if([[enddate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]>0)
                    {
                        NSDate*date = [dtFormatter dateFromString:enddate];
                        [tdict setObject:date forKey:@"enddate"];
                    }
                    else
                    {
                        [tdict setObject:@"" forKey:@"enddate"];
                    }
                }
                else
                {
                    [tdict setObject:@"" forKey:@"enddate"];
                }
                
                
                [tdict setObject:parentid forKey:@"parentid"];
                
                
                [tdict setObject:[NSNumber numberWithInt:incomeExpense] forKey:@"incomeExpense"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [incomeExpenseArray addObject:tdict];
                [dtFormatter release];
                [tdict release];
				
            }
        }
    }
    
    
    [numberFormatter release];
    
    nslog(@"income expense array in app delegate MTD-YTD === %@",incomeExpenseArray);
    return incomeExpenseArray;
    
    
}


-(NSMutableArray *)selectIncomeExpenseForDashBoardBoard:(NSString *)startDate endDate:(NSString *)endDate
{
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    
    if([incomeExpenseArray count]>0)
    {
        [incomeExpenseArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        /*
        NSDate*date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSString*dateString = [formatter stringFromDate:date];
        [formatter release];
        NSArray*dateStringArray = [dateString componentsSeparatedByString:@"-"];
        NSString*fromString = [NSString stringWithFormat:@"%@-%@-01",[dateStringArray objectAtIndex:0],[dateStringArray objectAtIndex:1]];
        
        NSString*toString = [NSString stringWithFormat:@"%@-%@-%@",[dateStringArray objectAtIndex:0],[dateStringArray objectAtIndex:1],[dateStringArray objectAtIndex:2]];
        */
        
        sql =[NSString stringWithFormat:@"SELECT *, rowid from IncomeExpenseTB WHERE date Between '%@' And '%@'",startDate,endDate];
        
        

        
        nslog(@"\n sql = %@",sql);
        
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                int incomeExpense = sqlite3_column_int(selectstmt, 0);
                
                NSString *amount =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 1)];
                NSString *date1 =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                NSString *notes =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 3)];
                NSString *property =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)];
                NSString *type =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)];
                
                
                
                NSString *isrecursive =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)];
                NSString *frequency =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,7)];
                NSString *enddate =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 8)];
                NSString *parentid =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)];
                
                
                int is_receipt_image_choosen_local = sqlite3_column_int(selectstmt,11);
                NSString *receipt_image_name_local =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 12)];
                NSString *receipt_small_image_name_local =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 13)];
                
                
                
                
                
                
                
                
                
                
                
                int rowid = sqlite3_column_int(selectstmt,14);
                
                
                amount = [amount stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                
                
                
                
                
                
                
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				[tdict setObject:amount forKey:@"amount"];
                
                nslog(@"\n date1 in appDelegate = %@",date1);
                NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *date = [dtFormatter dateFromString:date1];
                nslog(@"\n date using dateFormatter = %@",date);
                
                /**
                 date1 = [regionDateFormatter stringFromDate:date];
                 nslog(@"\n date1 = %@",date1);
                 */
                
                [tdict setObject:date forKey:@"date"];
				[tdict setObject:notes forKey:@"notes"];
				[tdict setObject:property forKey:@"property"];
				[tdict setObject:type forKey:@"type"];
                
                [tdict setObject:[NSNumber numberWithInt:is_receipt_image_choosen_local] forKey:@"is_receipt_image_choosen"];
                [tdict setObject:receipt_image_name_local forKey:@"receipt_image_name"];
                [tdict setObject:receipt_small_image_name_local forKey:@"receipt_small_image_name"];
                
                
                
                
                [tdict setObject:isrecursive forKey:@"isrecursive"];
                [tdict setObject:frequency forKey:@"frequency"];
                
                nslog(@"\n enddate = %@",enddate);
                if(enddate != nil)
                {
                    if([[enddate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]>0)
                    {
                        NSDate*date = [dtFormatter dateFromString:enddate];
                        [tdict setObject:date forKey:@"enddate"];
                    }
                    else
                    {
                        [tdict setObject:@"" forKey:@"enddate"];
                    }
                }
                else
                {
                    [tdict setObject:@"" forKey:@"enddate"];
                }
                
                
                [tdict setObject:parentid forKey:@"parentid"];
                
                
                [tdict setObject:[NSNumber numberWithInt:incomeExpense] forKey:@"incomeExpense"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [incomeExpenseArray addObject:tdict];
                [dtFormatter release];
                [tdict release];
				
            }
        }
    }
    
    
    [numberFormatter release];
    
    nslog(@"income expense array in app delegate for dashboard === %@ \n",incomeExpenseArray);
    return incomeExpenseArray;
    
    
}


-(void)AddIncomeExpense:(NSString *)property type:(NSString *)type date:(NSString *)date amount:(NSString *)amount notes:(NSString *)notes incomeExpense:(int)incomeExpense
            isrecursive:(NSString*)isrecursive
              frequency:(NSString*)frequency
                enddate:(NSString*)enddate
               parentid:(int)parentid
is_receipt_image_choosen:(int)is_receipt_image_choosen_local
     receipt_image_name:(NSString*)receipt_image_name_local
receipt_small_image_name:(NSString*)receipt_small_image_name_local


{
    
    nslog(@"\n property = %@",property);
    nslog(@"\n type = %@",type);
    nslog(@"\n date = %@",date);
    nslog(@"\n amount = %@",amount);
    nslog(@"\n notes = %@",notes);
    nslog(@"\n incomeExpense = %d",incomeExpense);
    nslog(@"\n notes = %@",notes);
    nslog(@"\n isrecursive = %@",isrecursive);
    nslog(@"\n frequency = %@",frequency);
    nslog(@"\n enddate = %@",enddate);
    nslog(@"\n parentid = %d",parentid);
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		
        
        const char *sql = "INSERT INTO IncomeExpenseTB(property, type, date, amount, notes, IncomeExpense,isrecursive,frequency,enddate,parentid,is_receipt_image_choosen,receipt_image_name,receipt_small_image_name) Values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
        
        /*
         const char *sql = "INSERT INTO IncomeExpenseTB(property, type, date, amount, notes, IncomeExpense) Values(?,?,?,?,?,?)";
         */
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
        {
            NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        }
        
        
		
		sqlite3_bind_text(add_stmt, 1, [property UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(add_stmt, 2, [type UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(add_stmt, 3, [date UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(add_stmt, 4, [amount UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(add_stmt, 5, [notes UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(add_stmt, 6, incomeExpense);
        sqlite3_bind_text(add_stmt, 7, [isrecursive UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(add_stmt, 8, [frequency UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(add_stmt, 9, [enddate UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(add_stmt, 10, parentid);
        sqlite3_bind_int(add_stmt, 11, is_receipt_image_choosen_local);
        sqlite3_bind_text(add_stmt, 12, [receipt_image_name_local UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(add_stmt, 13, [receipt_small_image_name_local UTF8String], -1, SQLITE_TRANSIENT);
        
        
        
        
        
        /*Not in this version
         sqlite3_bind_text(add_stmt, 7, [isrecursive UTF8String], -1, SQLITE_TRANSIENT);
         sqlite3_bind_text(add_stmt, 8, [frequency UTF8String], -1, SQLITE_TRANSIENT);
         sqlite3_bind_text(add_stmt, 9, [enddate UTF8String], -1, SQLITE_TRANSIENT);
         sqlite3_bind_int(add_stmt, 10, parentid);
         */
        
        
		
		if(SQLITE_DONE != sqlite3_step(add_stmt))
        {
            NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
        }
        
		
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		//sqlite3_close(database);
    }
    
}

#pragma mark  - UpdateIncomeExpense


-(void)UpdateIncomeExpense:(NSString *)property type:(NSString *)type date:(NSString *)date amount:(NSString *)amount notes:(NSString *)notes isrecursive:(NSString *)isrecursive enddate:(NSString *)enddate parentid:(int)parentid rowid:(int)rowid  is_receipt_image_choosen:(int)is_receipt_image_choosen_local
        receipt_image_name:(NSString*)receipt_image_name_local
  receipt_small_image_name:(NSString*)receipt_small_image_name_local
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
        
        
        
		if (1)
		{
			if (update_stmt == nil)
			{
                /*
                 sql = @"UPDATE IncomeExpenseTB SET property = ?, type= ?, date= ?, amount= ?, notes= ? WHERE rowid = ?";
                 */
                nslog(@"\n receipt_small_image_name_local = %@",receipt_small_image_name_local);
                
                
                sql = @"UPDATE IncomeExpenseTB SET property = ?, type= ?, date= ?, amount= ?, notes= ?,isrecursive = ?, enddate=?, parentid = ?,is_receipt_image_choosen=?,receipt_image_name=?,receipt_small_image_name=? WHERE rowid = ?";
                
                nslog(@"\n sql in update = %@",sql);
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
                {
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                }
			}
            sqlite3_bind_text(update_stmt, 1, [property UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 2, [type UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 3, [date UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 4, [amount UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 5, [notes UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 6, [isrecursive UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 7, [enddate UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 8, parentid);
            sqlite3_bind_int(update_stmt,9, is_receipt_image_choosen_local);
            sqlite3_bind_text(update_stmt, 10, [receipt_image_name_local UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt,11, [receipt_small_image_name_local UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_int(update_stmt, 12, rowid);
            
            
            
            /*Recursive
             sqlite3_bind_text(update_stmt, 6, [isrecursive UTF8String], -1, SQLITE_TRANSIENT);
             sqlite3_bind_text(update_stmt, 7, [enddate UTF8String], -1, SQLITE_TRANSIENT);
             */
            
            
			
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

-(void)UpdateIncomeExpenseByProperty:(NSString *)newProperty oldProperty:(NSString *)oldProperty
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
				sql = @"update IncomeExpenseTB set property = ? where property = ?";
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
                {
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                    
                }
			}
            sqlite3_bind_text(update_stmt, 1, [newProperty UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 2, [oldProperty UTF8String], -1, SQLITE_TRANSIENT);
            
			
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

-(void)DeleteIncomeExpense:(int)rowid
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM IncomeExpenseTB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

-(void)DeleteIncomeExpense:(int)rowid parentId:(int)parentId deleteType:(int)deleteType dateString:(NSString*)dateString
{
    @try
	{
        /*
         0 for only selected income/expense
         1 for (This + )Previous entries of selected income/expense
         2 for all entries of selected income/expense
         */
        
        nslog(@"\n dateString = %@",dateString);
        
        
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
            
            if(parentId==0)
            {
                parentId = rowid;
            }
            
            NSArray*dateStringArray = [dateString componentsSeparatedByString:@"-"];
            NSString*fromString = [NSString stringWithFormat:@"%@-%@-%@",[dateStringArray objectAtIndex:0],[dateStringArray objectAtIndex:1],[dateStringArray objectAtIndex:2]];
            
            
            
            
            nslog(@"\n fromString = %@",fromString);
            
            
			if (deleteSatement == nil)
			{
                if(deleteType == 0)
                {
                    sql = [NSString stringWithFormat:@"DELETE FROM IncomeExpenseTB WHERE rowid=%d",rowid];
                }
                else if(deleteType == 1)
                {
                    sql = [NSString stringWithFormat:@"DELETE FROM IncomeExpenseTB WHERE rowid=%d OR ((parentid = %d OR rowid = %d) AND date > '%@') AND isrecursive = '1'",rowid,parentId,parentId,fromString];
                }
                else if(deleteType == 2)
                {
                    sql = [NSString stringWithFormat:@"DELETE FROM IncomeExpenseTB WHERE rowid=%d  OR ( parentid = %d OR rowid = %d) AND isrecursive = '1'",rowid,parentId,parentId];
                }
                
                nslog(@"\n sql = %@",sql);
				
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			//sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

-(void)DeleteincomeExpenseByProperty:(NSString *)property
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM IncomeExpenseTB WHERE property=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
            sqlite3_bind_text(deleteSatement, 1, [property UTF8String], -1, SQLITE_TRANSIENT);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
}




#pragma mark -
#pragma mark property detail method

-(void)addPropertyDetail:(NSMutableArray *)propertyArrayDetail
{
    
    nslog(@"[propertyArrayDetail objectAtIndex:0] == %@",[propertyArrayDetail objectAtIndex:0]);
    
    
    
    
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		
        
        const char *sql = "INSERT INTO PropertyDetailDB(ProName, ProAddress, PropertyType, Currency, PurchasePrice, EstimatedPrice,FullyPaid, MortgageBank, RepaymentAmount, RepaymentFrequency, onLease, leaseStart, leaseEnd, AgentManage, PropertyAgent,zEquity, zDate, zLoan, imageCount,total_loan_borrowed,loan_outstanding,loan_term,rental_income,tenant_information,repayment_term,agentpk) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        
        sqlite3_bind_text(add_stmt, 1, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"0"] UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_text(add_stmt, 2, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"address"] UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_text(add_stmt, 3, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"propertytype"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(add_stmt, 4, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"currencytype"] UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_text(add_stmt, 5, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"8"] UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_text(add_stmt, 6, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"9"] UTF8String], -1, SQLITE_TRANSIENT);
        
        //sqlite3_bind_text(add_stmt, 7, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"fullyPaid"] UTF8String], -1, SQLITE_TRANSIENT);
        
        
        sqlite3_bind_int(add_stmt, 7 ,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"fullypaid"]intValue]);
        //nslog(@"[propertyArrayDetail objectAtIndex:0] == %@",[propertyArrayDetail objectAtIndex:0]);
        //nslog(@"fully paid == %d",[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"fullypaid"] intValue]);
        
        sqlite3_bind_text(add_stmt, 8, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"11"] UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_text(add_stmt, 9, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"15"] UTF8String], -1, SQLITE_TRANSIENT);
        
        
        
        sqlite3_bind_text(add_stmt, 10, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"repayment"] UTF8String], -1, SQLITE_TRANSIENT);
		
        sqlite3_bind_int(add_stmt, 11 ,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"lease"]boolValue]);
        
        sqlite3_bind_text(add_stmt, 12, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"leasestart"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(add_stmt, 13, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"leaseend"] UTF8String], -1, SQLITE_TRANSIENT);
        
        
        sqlite3_bind_int(add_stmt, 14,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"agentManage"]boolValue]);
        
        sqlite3_bind_text(add_stmt, 15, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"agenttype"] UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_text(add_stmt, 16, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"zEquity"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(add_stmt, 17, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"zDate"] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(add_stmt, 18, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"zLoan"] UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_int(add_stmt, 19 ,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"imageCount"]boolValue]);
        
        
        
        
        
        sqlite3_bind_double(add_stmt, 20 ,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"12"]doubleValue]);
        sqlite3_bind_double(add_stmt, 21 ,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"13"]doubleValue]);
        sqlite3_bind_int(add_stmt, 22 ,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"14"]intValue]);
        
        //23 : rental income
        
        
        sqlite3_bind_text(add_stmt,23, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"rental_income"] UTF8String], -1, SQLITE_TRANSIENT);
        
        //24: tenanat information
        sqlite3_bind_text(add_stmt,24, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"tenant_information"] UTF8String], -1, SQLITE_TRANSIENT);
        
        //25:reqpayment term
        sqlite3_bind_text(add_stmt,25, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"repayment_term"] UTF8String], -1, SQLITE_TRANSIENT);
        
        //26:agent primary key
        
        
        nslog(@"\n  agent = %@",[[propertyArrayDetail objectAtIndex:0]valueForKey:@"agent_pk"]);
        
        NSString*agent_str =@"";
        if([[[propertyArrayDetail objectAtIndex:0]valueForKey:@"agent_pk"] isKindOfClass:[NSNumber class]])
        {
            nslog(@"\n this is number..");
            
            agent_str = [NSString stringWithFormat:@"%@",[[propertyArrayDetail objectAtIndex:0]valueForKey:@"agent_pk"]];
        }
        else
        {
            agent_str = [NSString stringWithFormat:@"%@",[[propertyArrayDetail objectAtIndex:0]valueForKey:@"agent_pk"]];
        }
        
        /*
         sqlite3_bind_text(add_stmt,26, [[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"agent_pk"] stringValue] UTF8String], -1, SQLITE_TRANSIENT);
         */
        
        sqlite3_bind_text(add_stmt,26, [agent_str UTF8String], -1, SQLITE_TRANSIENT);
        
		
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		//SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
        
        [self storeProfileThumb:propertyImage :[[propertyArrayDetail objectAtIndex:0]valueForKey:@"0"]];
		//lastInsertedProfileRow_Id =  sqlite3_last_insert_rowid(database);
		
		//Reset the add statement.
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
    
}


-(NSMutableArray *)selectPropertyDetail
{
    if([propertyDetailArray count]>0)
    {
        [propertyDetailArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    
    
    nslog(@"\n\n\n selectPropertyDetail => [numberFormatter currencyCode] = %@",[numberFormatter currencyCode]);
    nslog(@"\n\n\n selectPropertyDetail => [numberFormatter locale].localeIdentifier = %@",[numberFormatter locale].localeIdentifier);
    nslog(@"\n\n\n selectPropertyDetail => curCode = %@",curCode);
    
    
    if (1)
    {
		
        
        
        sql = [NSString stringWithFormat:@"SELECT *, rowid from PropertyDetailDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc]init];
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)] forKey:@"0"];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)] forKey:@"address"];
                
                
                /*SUNSHINE:0004
                 */
                
                
                if ([curCode isEqualToString:@"Default Region Currency"])
                {
                    
                    if(!([[numberFormatter currencyCode] isEqualToString:@"ZAR"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"]))
                        
                    {
                        
                        
                        
                        NSString*tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        double tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"8"];
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"9"];
                        
                        
                        
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 19)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"12"];
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,20)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"13"];
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,21)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%d",(int)tempAmount] forKey:@"14"];
                        
                        
                        //22 rental income
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,22)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"rental_income"];
                        
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 10)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"15"];
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 15)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"zLoan"];
                        
                        //
                        
                        
                        /**
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 17)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                         */
                        
                        
                        
                        [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 17)] forKey:@"zEquity"];
                        
                        
                        
                        
                        
                    }
                    else
                    {
                        
                        
                        NSString*tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        double tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"8"];
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"9"];
                        
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 19)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"12"];
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,20)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"13"];
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,21)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%d",(int)tempAmount] forKey:@"14"];
                        
                        
                        //22 rental income
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,22)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"rental_income"];
                        
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 10)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"15"];
                        
                        
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 15)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        
                        tempAmount = [tempString doubleValue];
                        [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"zLoan"];
                        
                        //
                        
                        /**
                        tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 17)];
                        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                        */
                        
                        
                        
                        
                        
                        [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 17)] forKey:@"zEquity"];
                        
                        
                        
                    }
                    
                    
                }
                else if([curCode isEqualToString:@"ZAR"])
                {
                    
                    /*
                     NSString*tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)];
                     tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                     
                     double tempAmount = [tempString doubleValue]/100;
                     [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"8"];
                     
                     
                     tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                     tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                     
                     tempAmount = [tempString doubleValue]/100;
                     [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"9"];
                     
                     
                     tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 10)];
                     tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                     
                     
                     tempAmount = [tempString doubleValue]/100;
                     [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"12"];
                     
                     
                     
                     tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 15)];
                     tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                     
                     
                     tempAmount = [tempString doubleValue]/100;
                     [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"zLoan"];
                     
                     //
                     
                     tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 17)];
                     tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                     */
                    
                    NSString*tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    double tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"8"];
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"9"];
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 19)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"12"];
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,20)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"13"];
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,21)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%d",(int)tempAmount] forKey:@"14"];
                    
                    
                    //22 rental income
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,22)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"rental_income"];
                    
                    
                    
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 10)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"15"];
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 15)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"zLoan"];
                    
                    
                    /**
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 17)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    */
                    
                    
                    //[tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)] forKey:@"8"];
                    //[tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)] forKey:@"9"];
                    //[tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 10)] forKey:@"12"];
                    //[tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 15)] forKey:@"zLoan"];
                    
                    
                    
                    [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 17)] forKey:@"zEquity"];
                    
                    
                    
                }
                
                else
                {
                    
                    
                    NSString*tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    double tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"8"];
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"9"];
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 19)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"12"];
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,20)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"13"];
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,21)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%d",(int)tempAmount] forKey:@"14"];
                    
                    
                    //22 rental income
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,22)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"rental_income"];
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 10)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"15"];
                    
                    
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 15)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    
                    tempAmount = [tempString doubleValue];
                    [tdict setObject:[NSString stringWithFormat:@"%f",tempAmount] forKey:@"zLoan"];
                    
                    
                    /**
                    tempString =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 17)];
                    tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    */
                    
                    
                    //[tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 9)] forKey:@"8"];
                    //[tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)] forKey:@"9"];
                    //[tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 10)] forKey:@"12"];
                    //[tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 15)] forKey:@"zLoan"];
                    
                    
                    
                    [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 17)] forKey:@"zEquity"];
                    
                }
                
                
                
                /*SUNSHINE:*/
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 17)] forKey:@"zEquity"];
                
                
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)] forKey:@"11"];
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 1)] forKey:@"currencytype"];
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 8)] forKey:@"propertytype"];
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 11)] forKey:@"repayment"];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 23)] forKey:@"tenant_information"];
                
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 24)] forKey:@"repayment_term"];
                
                
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 25)] forKey:@"agent_pk"];
                
                
                
                
                NSString*dateString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 13)];
                
                
                nslog(@"\n date1 in appDelegate = %@",dateString);
                
                NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *date;
                if([dateString length]>0)
                {
                    date = [dtFormatter dateFromString:dateString];
                    nslog(@"\n date using dateFormatter = %@",date);
                    NSString* tmpDateString = [regionDateFormatter stringFromDate:date];
                    nslog(@"\n date1 = %@",tmpDateString);
                    
                    if(tmpDateString == nil)
                    {
                        [tdict setObject:dateString forKey:@"leasestart"];
                    }
                    else
                    {
                        [tdict setObject:tmpDateString forKey:@"leasestart"];
                    }
                    
                    
                }
                else
                {
                    [tdict setObject:@"" forKey:@"leasestart"];
                }
                
                
                
                
                
                dateString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 12)];
                
                
                nslog(@"\n date2 in appDelegate = %@",dateString);
                
                if([dateString length]>0)
                {
                    date = [dtFormatter dateFromString:dateString];
                    nslog(@"\n date using dateFormatter = %@",date);
                    NSString *tmpDateString = [regionDateFormatter stringFromDate:date];
                    nslog(@"\n date1 = %@",dateString);
                    
                    
                    if(tmpDateString == nil)
                    {
                        [tdict setObject:dateString forKey:@"leaseend"];
                    }
                    else
                    {
                        [tdict setObject:tmpDateString forKey:@"leaseend"];
                    }
                    
                    
                    
                }
                else
                {
                    [tdict setObject:@"" forKey:@"leaseend"];
                }
                
                
                [dtFormatter release];
                
                
                
                
                
                /* ORIGINAL
                 [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 13)] forKey:@"leasestart"];
                 [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 12)] forKey:@"leaseend"];
                 */
                
                
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 7)] forKey:@"agenttype"];
                
                [tdict setObject:[NSNumber numberWithInt:sqlite3_column_int(selectstmt, 0)] forKey:@"agentManage"];
                [tdict setObject:[NSNumber numberWithInt:sqlite3_column_int(selectstmt, 3)] forKey:@"fullypaid"];
                [tdict setObject:[NSNumber numberWithInt:sqlite3_column_int(selectstmt, 14)] forKey:@"lease"];
                
                
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 16)] forKey:@"zDate"];
                
                [tdict setObject:[NSNumber numberWithInt:sqlite3_column_int(selectstmt, 18)] forKey:@"imageCount"];
                
                int rowid = sqlite3_column_int(selectstmt, 26);
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
                
                UIImage *img;// = [[[UIImage alloc]init] autorelease];
                //nslog(@"name ==== %@",name);
                img = [self getProfileThumb:name];
                /*Check..*/
                
                if(img != nil)
                {
                    [tdict setObject:img forKey:@"image"];
                }
                else
                {
                    img  = [UIImage imageNamed:@"new-cell-property.png"];
                    [tdict setObject:img forKey:@"image"];
                }
                [propertyDetailArray addObject:tdict];
                //[img release];
                [tdict release];
            }
        }
    }
    
    nslog(@"\n propertyDetailArray in app delegate = %@",propertyDetailArray);
    [numberFormatter release];
    
    
    return propertyDetailArray;
    
}


-(void)updatePropertyDetail:(NSMutableArray *)propertyArrayDetail rowid:(int)rowid
{
    nslog(@"\n property detail array in update...======%@",propertyArrayDetail);
    
    nslog(@"\n rowid...======%d",rowid);
    
    
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
                sql = @"update PropertyDetailDB set ProName = ?, ProAddress = ?, PropertyType = ?, Currency = ?, PurchasePrice = ?, EstimatedPrice = ?,FullyPaid = ?, MortgageBank = ?, RepaymentAmount = ?, RepaymentFrequency = ?, onLease = ?, leaseStart = ?, leaseEnd = ?, AgentManage = ?, PropertyAgent = ?, zEquity = ?, imageCount = ?,total_loan_borrowed=?,loan_outstanding=?,loan_term=?,rental_income=?,tenant_information=?,repayment_term = ?,agentpk = ? WHERE rowid = ?";
                
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
            
            // nslog(@"\n sql in updatePropertyDetail = %@",sql);
            
            sqlite3_bind_text(update_stmt, 1, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"0"] UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(update_stmt, 2, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"address"] UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(update_stmt, 3, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"propertytype"] UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 4, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"currencytype"] UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(update_stmt, 5, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"8"] UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(update_stmt, 6, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"9"] UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 7 ,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"fullypaid"]boolValue]);
            nslog(@"fully paid update ==== %d",[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"fullypaid"]boolValue]);
            
            sqlite3_bind_text(update_stmt, 8, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"11"] UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(update_stmt, 9, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"15"] UTF8String], -1, SQLITE_TRANSIENT);
            
            
            
            
            sqlite3_bind_text(update_stmt, 10, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"repayment"] UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_int(update_stmt, 11 ,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"lease"]boolValue]);
            
            sqlite3_bind_text(update_stmt, 12, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"leasestart"] UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 13, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"leaseend"] UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_int(update_stmt, 14,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"agentManage"]boolValue]);
            
            sqlite3_bind_text(update_stmt, 15, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"agenttype"] UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 16, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"zEquity"] UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_int(update_stmt, 17 ,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"imageCount"]boolValue]);
            
            
            
            /*
             Sun:0004
             Phase:3
             
             */
            
            
            sqlite3_bind_double(update_stmt, 18,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"12"]doubleValue]);
            sqlite3_bind_double(update_stmt, 19,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"13"]doubleValue]);
            sqlite3_bind_int(update_stmt, 20,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"14"]intValue]);
            
            
            
            
            sqlite3_bind_double(update_stmt,21,[[[propertyArrayDetail objectAtIndex:0]valueForKey:@"rental_income"]doubleValue]);
            sqlite3_bind_text(update_stmt, 22, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"tenant_information"] UTF8String], -1, SQLITE_TRANSIENT);
            
            
            nslog(@"\n repayment_term = %@",[[propertyArrayDetail objectAtIndex:0]objectForKey:@"repayment_term"] );
            
            sqlite3_bind_text(update_stmt, 23, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"repayment_term"] UTF8String], -1, SQLITE_TRANSIENT);
            
            
            @try
            {
                int agent_pk_id  = [[[propertyArrayDetail objectAtIndex:0]objectForKey:@"agent_pk"] intValue];
                NSString*agent_pk_str = [NSString stringWithFormat:@"%d",agent_pk_id];
                [[propertyArrayDetail objectAtIndex:0]setObject:agent_pk_str forKey:@"agent_pk"];
                
            }
            @catch (NSException *exception)
            {
                
            }
            @finally
            {
                //   [[propertyArrayDetail objectAtIndex:0]objectForKey:@"agent_pk"]
                
                //[[propertyArrayDetail objectAtIndex:0]setObject:@"" forKey:@"agent_pk"];
                
            }
            
            if([[propertyArrayDetail objectAtIndex:0]objectForKey:@"agent_pk"])
            {
                
            }
            
            nslog(@"\n ageng pk = %@",[[propertyArrayDetail objectAtIndex:0]objectForKey:@"agent_pk"] );
            
            sqlite3_bind_text(update_stmt, 24, [[[propertyArrayDetail objectAtIndex:0]valueForKey:@"agent_pk"]  UTF8String], -1, SQLITE_TRANSIENT);
            
            
            /*
             Sun:0004
             Phase:3
             
             */
            
            sqlite3_bind_int(update_stmt, 25, rowid);
            
            if (isPropertyImageUpdate)
            {
                [self Deleteimage:[[propertyArrayDetail objectAtIndex:0]valueForKey:@"0"]];
                [self storeProfileThumb:propertyImage :[[propertyArrayDetail objectAtIndex:0]valueForKey:@"0"]];
            }
            
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}


-(void)updatePropertyEquity:(NSString *)purchase maketValue:(NSString *)marketValue Loan:(NSString *)Loan equity:(NSString *)equity date:(NSString *)date rowid:(int)rowid
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
                sql = @"update PropertyDetailDB set PurchasePrice = ?, EstimatedPrice = ?, loan_outstanding = ?, zEquity = ?, zDate = ? where rowid = ?";
                
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
            
            sqlite3_bind_text(update_stmt, 1, [purchase UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 2, [marketValue UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_double(update_stmt,3,[Loan doubleValue]);
            sqlite3_bind_text(update_stmt, 4, [equity UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(update_stmt, 5, [date UTF8String], -1, SQLITE_TRANSIENT);
            
            
            sqlite3_bind_int(update_stmt, 6, rowid);
            
            
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
    
}

-(void)deletePropertyDetail:(int)rowid
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM PropertyDetailDB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}
#pragma mark -
#pragma mark Agent Type Method

-(NSMutableArray *)selectNewAgentType
{
    if([agentArray count]>0)
    {
        [agentArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"select *, rowid from AgentDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc]init];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)] forKey:@"9999"];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 1)] forKey:@"2"];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)] forKey:@"1"];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 3)] forKey:@"5"];
                
                
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)] forKey:@"4"];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)] forKey:@"3"];
                
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)] forKey:@"0"];
                
                
                int rowid = sqlite3_column_int(selectstmt, 7);
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
                
                
                [agentArray addObject:tdict];
                [tdict release];
            }
        }
    }
    return agentArray;
}

-(NSMutableArray *)selectAgentType
{
    if([agentArray count]>0)
    {
        [agentArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1) {
		
        sql = [NSString stringWithFormat:@"select *, rowid from AgentDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc]init];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)] forKey:@"9999"];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 1)] forKey:@"2"];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)] forKey:@"1"];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 3)] forKey:@"5"];
                
                
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)] forKey:@"4"];
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)] forKey:@"3"];
                
                
                [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)] forKey:@"0"];
                
                
                int rowid = sqlite3_column_int(selectstmt, 7);
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
                
                
                [agentArray addObject:tdict];
                [tdict release];
            }
        }
    }
    return agentArray;
}
-(void)AddAgentInfo:(NSMutableArray *)agentType
{
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		
        
        const char *sql = "insert into AgentDB(propertyAgent, ContactPerson,AgentCommission,WorkPhone, Mobile, Email, Address) Values(?,?,?,?,?,?,?)";
        
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        
        for (int i = 1; i <7;i++)
        {
            sqlite3_bind_text(add_stmt, i, [[[agentType objectAtIndex:0]valueForKey:[NSString stringWithFormat:@"%d",i-1]] UTF8String], -1, SQLITE_TRANSIENT);
        }
        
        sqlite3_bind_text(add_stmt, 7, [[[agentType objectAtIndex:0]valueForKey:@"9999"] UTF8String], -1, SQLITE_TRANSIENT);
		
		//sqlite3_bind_int(add_stmt, 1, cat_table_id);
        //         sqlite3_bind_int(add_stmt, i ,[[proArray objectAtIndex:i-1]intValue]);
        
		//sqlite3_bind_text(add_stmt, i, [[proArray objectAtIndex:i-1] UTF8String], -1, SQLITE_TRANSIENT);
        
		
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		//SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
		
		//lastInsertedProfileRow_Id =  sqlite3_last_insert_rowid(database);
		
		//Reset the add statement.
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
    
}
-(void)UpdateAgentInfo:(NSMutableArray *)agentType rowid:(int)rowid
{
    //nslog(@"row id ===== %d",rowid);
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
                
                
                sql = @"update AgentDB set propertyAgent=?, ContactPerson=?,AgentCommission=?,WorkPhone=?, Mobile=?, Email=?, Address=? where rowid = ?";
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
            
            
            for (int i = 1; i <7;i++)
            {
                sqlite3_bind_text(update_stmt, i, [[[agentType objectAtIndex:0]valueForKey:[NSString stringWithFormat:@"%d",i-1]] UTF8String], -1, SQLITE_TRANSIENT);
            }
            sqlite3_bind_text(update_stmt, 7, [[[agentType objectAtIndex:0]valueForKey:@"9999"] UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 8, rowid);
            
            
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

-(void)DeleteAgentInfo:(int)rowid
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM AgentDB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}



-(NSMutableDictionary*)getAgentInforById:(int)rowid
{
    NSMutableDictionary*record_dictionary = [[[NSMutableDictionary alloc] init] autorelease];
    
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    
    
    sql = [NSString stringWithFormat:@"SELECT *,rowid FROM AgentDB WHERE rowid = %d",rowid];
    Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
    if(Value==1)
    {
        NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
    }
    
    if(Value==SQLITE_OK)
    {
        
        while(sqlite3_step(selectstmt) == SQLITE_ROW)
        {
            
            int rowid = sqlite3_column_int(selectstmt, 7);
            
            
            
            
            [record_dictionary setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)] forKey:@"address"];
            [record_dictionary setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 1)] forKey:@"AgentCommission"];
            [record_dictionary setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)] forKey:@"ContactPerson"];
            [record_dictionary setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 3)] forKey:@"Email"];
            [record_dictionary setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 4)] forKey:@"Mobile"];
            [record_dictionary setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 5)] forKey:@"WorkPhone"];
            [record_dictionary setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 6)] forKey:@"propertyAgent"];
            
            [record_dictionary setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
            
            
        }
    }
    
    
    return record_dictionary;
    
    
}

#pragma mark -
#pragma mark income type methods

-(NSMutableArray *)selectIncomeType
{
    
    
    
    
    if([incomeTypeDBArray count]>0)
    {
        [incomeTypeDBArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1) {
		
        sql = [NSString stringWithFormat:@"select *, rowid from IncomeTypeDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSString *incomeType =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                int rowid = sqlite3_column_int(selectstmt, 1);
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				[tdict setObject:incomeType forKey:@"incomeType"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [incomeTypeDBArray addObject:tdict];
                [tdict release];
				
            }
        }
    }
    return incomeTypeDBArray;
}

-(void)AddIncomeType:(NSString *)incomeType
{
	sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		const char *sql = "insert into IncomeTypeDB(IncomeType) Values(?)";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
		
		sqlite3_bind_text(add_stmt, 1, [incomeType UTF8String], -1, SQLITE_TRANSIENT);
        
		
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
	
}

-(void)updateIncomeType:(NSString *)incomeType rowid:(int)rowid
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
				sql = @"update IncomeTypeDB set IncomeType=? where rowid = ?";
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            sqlite3_bind_text(update_stmt, 1, [incomeType UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 2, rowid);
            
			
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}


-(void)deleteIncomeType:(int)rowid
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM IncomeTypeDB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

#pragma mark -
#pragma mark Expense Type

-(NSMutableArray *)selectExpenseType
{
    if([expenseTypeArray count]>0)
    {
        [expenseTypeArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1) {
		
        sql = [NSString stringWithFormat:@"select *, rowid from ExpenseTypeDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSString *expenseType =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                int rowid = sqlite3_column_int(selectstmt, 1);
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				[tdict setObject:expenseType forKey:@"expenseType"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [expenseTypeArray addObject:tdict];
                [tdict release];
				
            }
        }
    }
    return expenseTypeArray;
}

-(void)AddExpenseType:(NSString *)ExpenseType;
{
	sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		const char *sql = "insert into ExpenseTypeDB(ExpenseType) Values(?)";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
		
		sqlite3_bind_text(add_stmt, 1, [ExpenseType UTF8String], -1, SQLITE_TRANSIENT);
        
		
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
	
}

-(void)updateExpenseType:(NSString *)ExpenseType rowid:(int)rowid;
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
				sql = @"update ExpenseTypeDB set ExpenseType=? where rowid = ?";
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            sqlite3_bind_text(update_stmt, 1, [ExpenseType UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 2, rowid);
            
			
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}


-(void)deleteExpenseType:(int)rowid;
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM ExpenseTypeDB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}



#pragma mark -
#pragma mark New Expense Type

-(NSMutableArray *)selectNewExpenseType
{
    if([expenseTypeArray count]>0)
    {
        [expenseTypeArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1) {
		
        sql = [NSString stringWithFormat:@"select *, rowid from ExpenseTypeDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSString *expenseType =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                int rowid = sqlite3_column_int(selectstmt, 1);
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				[tdict setObject:expenseType forKey:@"expenseType"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [expenseTypeArray addObject:tdict];
                [tdict release];
				
            }
        }
    }
    return expenseTypeArray;
}

-(void)AddNewExpenseType:(NSString *)ExpenseType;
{
	sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		const char *sql = "insert into ExpenseTypeDB(ExpenseType) Values(?)";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
		
		sqlite3_bind_text(add_stmt, 1, [ExpenseType UTF8String], -1, SQLITE_TRANSIENT);
        
		
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
	
}

-(void)updateNewExpenseType:(NSString *)ExpenseType rowid:(int)rowid;
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
				sql = @"update ExpenseTypeDB set ExpenseType=? where rowid = ?";
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            sqlite3_bind_text(update_stmt, 1, [ExpenseType UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 2, rowid);
            
			
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}


-(void)deleteNewExpenseType:(int)rowid;
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM ExpenseTypeDB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}


#pragma mark -
#pragma mark reminder type methods

-(NSMutableArray *)selectNewReminderType
{
    if([reminderTypeArray count]>0)
    {
        [reminderTypeArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"select *, rowid from ReminderTypeDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSString *reminderType =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                int rowid = sqlite3_column_int(selectstmt, 1);
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				[tdict setObject:reminderType forKey:@"reminderType"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [reminderTypeArray addObject:tdict];
                [tdict release];
				
            }
        }
    }
    return reminderTypeArray;
}

-(NSMutableArray *)selectReminderType
{
    if([reminderTypeArray count]>0)
    {
        [reminderTypeArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1) {
		
        sql = [NSString stringWithFormat:@"select *, rowid from ReminderTypeDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSString *reminderType =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                
                
                int rowid = sqlite3_column_int(selectstmt, 1);
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				[tdict setObject:reminderType forKey:@"reminderType"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [reminderTypeArray addObject:tdict];
                [tdict release];
				
            }
        }
    }
    return reminderTypeArray;
}
-(void)AddReminderType:(NSString *)ReminderType
{
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		const char *sql = "insert into ReminderTypeDB(ReminderType) Values(?)";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
		
		sqlite3_bind_text(add_stmt, 1, [ReminderType UTF8String], -1, SQLITE_TRANSIENT);
        
        reminderRowid =  sqlite3_last_insert_rowid(database);
        
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
    
}
-(void)updateReminderType:(NSString *)ReminderType rowid:(int)rowid
{
    
    nslog(@"reminder rowid======%d",rowid);
    nslog(@"reminder tyep====%@",ReminderType);
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
				sql = @"update ReminderTypeDB set ReminderType=? where rowid = ?";
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            sqlite3_bind_text(update_stmt, 1, [ReminderType UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 2, rowid);
            
			
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}
-(void)deleteReminderType:(int)rowid
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM ReminderTypeDB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}


#pragma mark -
#pragma mark property type methods






-(NSMutableArray *)selectPropertyType
{
    if([propertyTypeArray count]>0)
    {
        [propertyTypeArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"select *, rowid from PropertyTypeDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSString *propertyType =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                int rowid = sqlite3_column_int(selectstmt, 1);
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				[tdict setObject:propertyType forKey:@"propertyType"];
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
                [propertyTypeArray addObject:tdict];
                [tdict release];
				
            }
        }
    }
    // nslog(@"property type array in appdelegate......%@",propertyTypeArray);
    return propertyTypeArray;
}




-(void)addPropertyType:(NSString *)PropertyType
{
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		const char *sql = "insert into PropertyTypeDB(PropertyType) Values(?)";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
		
		sqlite3_bind_text(add_stmt, 1, [PropertyType UTF8String], -1, SQLITE_TRANSIENT);
        
        reminderRowid =  sqlite3_last_insert_rowid(database);
        
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
    
    
}



-(void)updatePropertyType:(NSString *)PropertyType rowid:(int)rowid
{
    
    nslog(@"\n PropertyType = %@",PropertyType);
    nslog(@"\n rowid = %d",rowid);
    
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
				sql = @"update PropertyTypeDB set PropertyType=? where rowid = ?";
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            sqlite3_bind_text(update_stmt, 1, [PropertyType UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 2, rowid);
            
			
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}



-(void)deletePropertyType:(int)rowid
{
    
    //nslog(@"row id ==== %d",rowid);
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM PropertyTypeDB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}



#pragma mark -
#pragma mark New property method

-(void)updateNewPropertyType:(NSString *)PropertyType rowid:(int)rowid
{
    
    nslog(@"\n PropertyType = %@",PropertyType);
    nslog(@"\n rowid = %d",rowid);
    
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
				sql = @"update PropertyTypeDB set PropertyType=? where rowid = ?";
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            sqlite3_bind_text(update_stmt, 1, [PropertyType UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(update_stmt, 2, rowid);
            
			
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

-(void)deleteNewPropertyType:(int)rowid
{
    
    //nslog(@"row id ==== %d",rowid);
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM PropertyTypeDB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

-(NSMutableArray *)selectNewPropertyType
{
    if([propertyTypeArray count]>0)
    {
        [propertyTypeArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"select *, rowid from PropertyTypeDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSString *propertyType =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                int rowid = sqlite3_column_int(selectstmt, 1);
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
				[tdict setObject:propertyType forKey:@"propertyType"];
                
				[tdict setObject:[NSNumber numberWithInt:rowid] forKey:@"rowid"];
				
                [propertyTypeArray addObject:tdict];
                [tdict release];
				
            }
        }
    }
    // nslog(@"property type array in appdelegate......%@",propertyTypeArray);
    return propertyTypeArray;
}


-(void)addNewPropertyType:(NSString *)PropertyType
{
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		const char *sql = "insert into PropertyTypeDB(PropertyType) Values(?)";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
		
		sqlite3_bind_text(add_stmt, 1, [PropertyType UTF8String], -1, SQLITE_TRANSIENT);
        
        reminderRowid =  sqlite3_last_insert_rowid(database);
        
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
    
    
}

#pragma mark -
#pragma mark property method

-(void)addProperty:(NSMutableArray *)proArray
{
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		
		const char *sql = "insert into PropertyDB(PropertyName,ProAddress,ProCity,ProState,ProCountry,ProType,ProCurrency,ProAgentSwitch,AgentName,AgentPerson,AgentAddress,AgentCity,AgentState,AgentCountry,AgentWorkPhone,AgentMobile,AgentEmail,AgentCommision,AgentLease,LeaseStart,LeaseEnd,PurchasePrice,EstimatedPrice,FullyPaid,MortgageBank,RepaymentAmount,RepaymentFrequency) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
		
		//sqlite3_bind_int(add_stmt, 1, cat_table_id);
		for(int i=1;i<=[proArray count];i++){
            if (i==8 || i==19  || i==24)
            {
                sqlite3_bind_int(add_stmt, i ,[[proArray objectAtIndex:i-1]intValue]);
                
            }
            else
            {
                sqlite3_bind_text(add_stmt, i, [[proArray objectAtIndex:i-1] UTF8String], -1, SQLITE_TRANSIENT);
            }
		}
		
		
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		//SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
		
		//lastInsertedProfileRow_Id =  sqlite3_last_insert_rowid(database);
		
		//Reset the add statement.
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
}

-(NSMutableArray *)selectProperty
{
    if([propertyListArray count]>0)
    {
        [propertyListArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1) {
		
        sql = [NSString stringWithFormat:@"select *, rowid from PropertyDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc]init];
                
                for (int i=0;i<28;i++)
                {
                    if (i == 5 || i==12 || i==17)
                    {
                        [tdict setObject:[NSNumber numberWithInt:sqlite3_column_int(selectstmt, i)] forKey:[NSString stringWithFormat:@"%d",i]];
                    }
                    else
                    {
                        [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, i)] forKey:[NSString stringWithFormat:@"%d",i]];
                    }
                }
                
                
                
                [propertyListArray addObject:tdict];
                
                
				
            }
        }
    }
    return propertyListArray;
    
}

-(void)deleteProperty:(int)rowid
{
    //nslog(@"row id ==== %d",rowid);
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM PropertyDB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
    
}

-(void)updateProperty:(int)rowid proArray:(NSMutableArray *)proArray
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
                
                
                sql = @"update PropertyDB set PropertyName = ?,ProAddress = ?,ProCity = ?,ProState = ?,ProCountry = ?,ProType = ?,ProCurrency = ?,ProAgentSwitch = ?,AgentName = ?,AgentPerson = ?,AgentAddress= ?,AgentCity = ?,AgentState = ?,AgentCountry = ?,AgentWorkPhone = ?,AgentMobile = ?,AgentEmail = ?,AgentCommision = ?,AgentLease = ?,LeaseStart = ?,LeaseEnd = ?,PurchasePrice = ?,EstimatedPrice = ?,FullyPaid = ?,MortgageBank = ?,RepaymentAmount = ?,RepaymentFrequency = ? where rowid = ?";
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
            
            
            
            for(int i=1;i<=[proArray count];i++){
                if (i==8 || i==19  || i==24)
                {
                    sqlite3_bind_int(update_stmt, i ,[[proArray objectAtIndex:i-1]intValue]);
                    
                }
                else
                {
                    sqlite3_bind_text(update_stmt, i, [[proArray objectAtIndex:i-1] UTF8String], -1, SQLITE_TRANSIENT);
                }
            }
            
            sqlite3_bind_int(update_stmt, [proArray count]+1, rowid);
            
            //            else
            //            {
            //                for (int i =1;i<=[proArray count];i++)
            //                {
            //                    for (int j=0;j<28;j++)
            //                    {
            //                        if (j==7 || j==18  || j==23)
            //                        {
            //                            sqlite3_bind_int(update_stmt, i ,[[[proArray objectAtIndex:j]valueForKey:[NSString stringWithFormat:@"%d",j]]intValue]);
            //
            //                        }
            //                        else if (j==27)
            //                        {
            //                            sqlite3_bind_int(update_stmt, i, rowid);
            //
            //
            //                        }
            //                        else
            //                        {
            //                            sqlite3_bind_text(update_stmt, i, [[[proArray objectAtIndex:j]valueForKey:[NSString stringWithFormat:@"%d",j]] UTF8String], -1, SQLITE_TRANSIENT);
            //                        }
            //
            //                    }
            //
            //                }
            //            }
            //
            
			
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}


#pragma mark -
#pragma mark New property method


-(void)addNewProperty:(NSMutableArray *)proArray
{
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		
		const char *sql = "insert into PropertyDB(PropertyName,ProAddress,ProCity,ProState,ProCountry,ProType,ProCurrency,ProAgentSwitch,AgentName,AgentPerson,AgentAddress,AgentCity,AgentState,AgentCountry,AgentWorkPhone,AgentMobile,AgentEmail,AgentCommision,AgentLease,LeaseStart,LeaseEnd,PurchasePrice,EstimatedPrice,FullyPaid,MortgageBank,RepaymentAmount,RepaymentFrequency) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
		
		//sqlite3_bind_int(add_stmt, 1, cat_table_id);
		for(int i=1;i<=[proArray count];i++){
            if (i==8 || i==19  || i==24)
            {
                sqlite3_bind_int(add_stmt, i ,[[proArray objectAtIndex:i-1]intValue]);
                
            }
            else
            {
                sqlite3_bind_text(add_stmt, i, [[proArray objectAtIndex:i-1] UTF8String], -1, SQLITE_TRANSIENT);
            }
		}
		
		
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		//SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
		
		//lastInsertedProfileRow_Id =  sqlite3_last_insert_rowid(database);
		
		//Reset the add statement.
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
}

-(NSMutableArray *)selectNewProperty
{
    if([propertyListArray count]>0)
    {
        [propertyListArray removeAllObjects];
    }
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1) {
		
        sql = [NSString stringWithFormat:@"select *, rowid from PropertyDB"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSMutableDictionary *tdict = [[NSMutableDictionary alloc]init];
                
                for (int i=0;i<28;i++)
                {
                    if (i == 5 || i==12 || i==17)
                    {
                        [tdict setObject:[NSNumber numberWithInt:sqlite3_column_int(selectstmt, i)] forKey:[NSString stringWithFormat:@"%d",i]];
                    }
                    else
                    {
                        [tdict setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, i)] forKey:[NSString stringWithFormat:@"%d",i]];
                    }
                }
                
                
                
                [propertyListArray addObject:tdict];
                
                
				
            }
        }
    }
    return propertyListArray;
    
}

-(void)deleteNewProperty:(int)rowid
{
    //nslog(@"row id ==== %d",rowid);
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM PropertyDB WHERE rowid=?";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			sqlite3_bind_int(deleteSatement, 1,rowid);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
    
}

-(void)updateNewProperty:(int)rowid proArray:(NSMutableArray *)proArray
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
		if (1)
		{
			if (update_stmt == nil)
			{
                
                
                sql = @"update PropertyDB set PropertyName = ?,ProAddress = ?,ProCity = ?,ProState = ?,ProCountry = ?,ProType = ?,ProCurrency = ?,ProAgentSwitch = ?,AgentName = ?,AgentPerson = ?,AgentAddress= ?,AgentCity = ?,AgentState = ?,AgentCountry = ?,AgentWorkPhone = ?,AgentMobile = ?,AgentEmail = ?,AgentCommision = ?,AgentLease = ?,LeaseStart = ?,LeaseEnd = ?,PurchasePrice = ?,EstimatedPrice = ?,FullyPaid = ?,MortgageBank = ?,RepaymentAmount = ?,RepaymentFrequency = ? where rowid = ?";
                
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
            
            
            
            for(int i=1;i<=[proArray count];i++){
                if (i==8 || i==19  || i==24)
                {
                    sqlite3_bind_int(update_stmt, i ,[[proArray objectAtIndex:i-1]intValue]);
                    
                }
                else
                {
                    sqlite3_bind_text(update_stmt, i, [[proArray objectAtIndex:i-1] UTF8String], -1, SQLITE_TRANSIENT);
                }
            }
            
            sqlite3_bind_int(update_stmt, [proArray count]+1, rowid);
            
            
            
			
            
			int success = sqlite3_step(update_stmt);
			sqlite3_reset(update_stmt);
			sqlite3_finalize(update_stmt);
			update_stmt = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}


#pragma mark -
#pragma mark restore factory method

-(void)restoreReminderType
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM ReminderTypeDB";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
    
}
-(void)restoreIncomeType
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM IncomeTypeDB";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}
-(void)restoreExpenseType
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM ExpenseTypeDB";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
    
}
-(void)restorePropertyType
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM PropertyTypeDB";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
			
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
    
}
-(void)restoreProperty
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM PropertyDetailDB";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}
-(void)restoreAgent
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM AgentDB";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
    
}


-(void)restoreIncomeExpense
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM IncomeExpenseTB";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}
-(void)restoreReminder
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				sql = @"DELETE FROM reminderTB";
				if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			}
            
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

-(void)insertPropertyType
{
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		const char *sql = "insert into PropertyTypeDB(PropertyType) select * from restore_propertyType";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        
        
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
    
}
-(void)insertIncomeType
{
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		const char *sql = "insert into IncomeTypeDB(IncomeType) select * from restore_incomeType";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
		
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
    
}
-(void)insertExpenseType
{
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		const char *sql = "insert into ExpenseTypeDB(ExpenseType) select * from restore_expenseType";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        
		
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
    
}
-(void)insertReminderType
{
    sqlite3_stmt *add_stmt;
	if (1)
	{
		add_stmt = nil;
		const char *sql = "insert into ReminderTypeDB(ReminderType) select * from restore_reminderType";
		if(sqlite3_prepare_v2(database, sql, -1, &add_stmt, NULL) != SQLITE_OK)
			
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
		
        
        reminderRowid =  sqlite3_last_insert_rowid(database);
        
		if(SQLITE_DONE != sqlite3_step(add_stmt))
			NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
		
		sqlite3_reset(add_stmt);
		sqlite3_finalize(add_stmt);
		sqlite3_close(database);
	}
    
}



#pragma mark -
#pragma mark UITabBarControllerDelegate methods

/*
 // Optional UITabBarControllerDelegate method.
 - (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
 }
 */

/*
 // Optional UITabBarControllerDelegate method.
 - (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed {
 }
 */
-(BOOL)validateEmail:(NSString*)email{
	
	if( (0 != [email rangeOfString:@"@"].length) && (0 != [email rangeOfString:@"."].length) )
	{
		NSMutableCharacterSet *invalidCharSet = [[[[NSCharacterSet alphanumericCharacterSet] invertedSet]mutableCopy]autorelease];
		[invalidCharSet removeCharactersInString:@"_-"];
		
		NSRange range1 = [email rangeOfString:@"@" options:NSCaseInsensitiveSearch];
		
		// If username part contains any character other than "."  "_" "-"
		
		NSString *usernamePart = [email substringToIndex:range1.location];
		NSArray *stringsArray1 = [usernamePart componentsSeparatedByString:@"."];
		for (NSString *string in stringsArray1) {
			NSRange rangeOfInavlidChars=[string rangeOfCharacterFromSet: invalidCharSet];
			if(rangeOfInavlidChars.length !=0 || [string isEqualToString:@""])
				return NO;
		}
		
		NSString *domainPart = [email substringFromIndex:range1.location+1];
		NSArray *stringsArray2 = [domainPart componentsSeparatedByString:@"."];
		
		for (NSString *string in stringsArray2) {
			NSRange rangeOfInavlidChars=[string rangeOfCharacterFromSet:invalidCharSet];
			if(rangeOfInavlidChars.length !=0 || [string isEqualToString:@""])
				return NO;
		}
		
		return YES;
	}
	else // no '@' or '.' present
		return NO;
}

#pragma mark - getting max(rowid)


-(int)getMaxRowidFromTable:(NSString*)tableName
{
    
    NSString *sql;
    int Value;
    int rowId = 0;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"select max(rowid) FROM %@",tableName];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                NSString *rowid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 0)];
                if(rowid != nil)
                {
                    rowId = [rowid intValue];
                }
                
                
            }
        }
    }
    
    return rowId;
    
    
}


#pragma mark - updating/deleting recursive record

-(void)updateParentFrequency:(NSString*)recurrenceTime pk:(int)pk
{
    @try
	{
		NSString *sql;
		sqlite3_stmt *updateStatement;
		updateStatement = nil;
        
        if (updateStatement == nil)
        {
            
            
            
            sql = @"UPDATE IncomeExpenseTB SET frequency = ?  WHERE rowid=?";
            
            nslog(@"\n deleteRecursiveRecord sql = %@",sql);
            
            if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &updateStatement, NULL) != SQLITE_OK)
            {
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            }
        }
        
        
        
        sqlite3_bind_text(updateStatement, 1,[recurrenceTime UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(updateStatement, 2,pk);
        
        int success = sqlite3_step(updateStatement);
        sqlite3_reset(updateStatement);
        sqlite3_finalize(updateStatement);
        updateStatement = nil;
        
        if (success != SQLITE_DONE)
        {
            NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
        }
		
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
}

-(void)deleteRecursiveRecord:(NSString*)parentId condition:(NSString*)condition
{
    
    @try
	{
		NSString *sql;
		sqlite3_stmt *deleteSatement;
		deleteSatement = nil;
		if (1)
		{
			if (deleteSatement == nil)
			{
				
                if([[condition stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]>0)
                {
                    sql = [NSString stringWithFormat:@"DELETE FROM IncomeExpenseTB WHERE parentid=? AND date > '%@'",condition];
                    
                }
                else
                {
                    sql = @"DELETE FROM IncomeExpenseTB WHERE parentid=?";
                }
                
				nslog(@"\n deleteRecursiveRecord sql = %@",sql);
                
                if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &deleteSatement, NULL) != SQLITE_OK)
                {
					NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                }
			}
			
			sqlite3_bind_int(deleteSatement, 1,[parentId intValue]);
			
			int success = sqlite3_step(deleteSatement);
			sqlite3_reset(deleteSatement);
			sqlite3_finalize(deleteSatement);
			deleteSatement = nil;
			
			if (success != SQLITE_DONE)
			{
				NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
			}
		}
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
    
}


#pragma mark
#pragma mark update_default_date_filter

-(void)update_default_date_filter:(NSMutableDictionary *)dictionary
{
    
    nslog(@"\n dictionary = %@",dictionary);
    
    
    @try
	{
		NSString *sql;
		sqlite3_stmt *update_stmt;
		update_stmt = nil;
        
        
        if (update_stmt == nil)
        {
            
            
            sql = @"UPDATE tbl_default_date_filters SET start_date = ?, end_date= ?";
            
            nslog(@"\n sql in update = %@",sql);
            
            if (sqlite3_prepare_v2(database, [sql UTF8String], -1, &update_stmt, NULL) != SQLITE_OK)
            {
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            }
        }
        
        
        
        sqlite3_bind_text(update_stmt, 1, [[self fetchDateInCommonFormate:[dictionary objectForKey:@"1000"]] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(update_stmt, 2, [[self fetchDateInCommonFormate:[dictionary objectForKey:@"1001"]] UTF8String], -1, SQLITE_TRANSIENT);
        
        
        int success = sqlite3_step(update_stmt);
        sqlite3_reset(update_stmt);
        sqlite3_finalize(update_stmt);
        update_stmt = nil;
        
        if (success != SQLITE_DONE)
        {
            NSAssert1(0, @"Error: failed to delete from database with message '%s'.", sqlite3_errmsg(database));
        }
		
	}
	@catch (NSException *e)
	{
		//nslog(@"Exception");
	}
    
}

#pragma mark - convert_common_date_to_region_date
-(NSString*)convert_common_date_to_region_date:(NSString*)commonDate

{
    
    NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
    [dtFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dtFormatter dateFromString:commonDate];
    nslog(@"\n date using dateFormatter = %@",date);
    [dtFormatter release];
    return [regionDateFormatter stringFromDate:date];
    
}

#pragma mark
#pragma mark select_default_date_filter



-(NSMutableDictionary *)select_default_date_filter
{
    
    
    NSMutableDictionary*temp_data_dictionary = [[[NSMutableDictionary alloc] init] autorelease];
    
    
    NSString *sql;
    int Value;
    sqlite3_stmt *selectstmt;
    if (1)
    {
		
        sql = [NSString stringWithFormat:@"SELECT * FROM tbl_default_date_filters"];
        Value = sqlite3_prepare_v2(database, [sql UTF8String], -1, &selectstmt, NULL);
        if(Value==1)
        {
            NSAssert1(0, @"Error: failed to select the database with message '%s'.", sqlite3_errmsg(database));
        }
		
        if(Value==SQLITE_OK)
        {
			
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                
                
                
                
                NSString *start_date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 1)];
                NSString *end_date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt, 2)];
                
                if([start_date isEqualToString:@"0"])
                {
                    [temp_data_dictionary setObject:@"" forKey:@"1000"];
                }
                if([end_date isEqualToString:@"0"])
                {
                    [temp_data_dictionary setObject:@"" forKey:@"1001"];
                }
                
                if( (![start_date isEqualToString:@"0"]) && (![start_date isEqualToString:@"0"]))
                {
                    [temp_data_dictionary setObject:[self convert_common_date_to_region_date:start_date] forKey:@"1000"];
                    [temp_data_dictionary setObject:[self convert_common_date_to_region_date:end_date] forKey:@"1001"];
                }
                
            }
        }
    }
    
    
    
    nslog(@"\n temp_data_dictionary = %@",temp_data_dictionary);
    
    
    return temp_data_dictionary;
    
    
    
    
    
    
    
}


#pragma mark -
#pragma mark StoreImageLocally
-(int)storeImageLocally:(NSString *)fileName image:(UIImage *)image
{
	//UIImage *IM;
	NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent: fileName];
    nslog(@"\n image = %@", image);
    nslog(@"\n filePathf = %@", filePath);
    
    
	[UIImagePNGRepresentation(image)  writeToFile: filePath atomically:YES];
	return 0;
}

#pragma mark -
#pragma mark checkFileExistance

-(BOOL)checkFileExistance:(NSString *)FileName
{
    
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:FileName];
	
    return ([[NSFileManager defaultManager] fileExistsAtPath:filePath]);
    
}

#pragma mark -
#pragma mark GetImage

-(UIImage *)getImage:(NSString *)FileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:FileName];
	
    UIImage*IM = [UIImage imageWithContentsOfFile:filePath];
	return IM;
}


#pragma mark -
#pragma mark GetImage

-(void)removeImageFromDirectory:(NSString *)FileName
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:FileName];
	
    NSFileManager *fileManager = [NSFileManager defaultManager];
    @try
    {
        if([fileManager fileExistsAtPath:filePath]==TRUE)
        {
            [fileManager removeItemAtPath:filePath error:NULL];
        }
    }
    @catch (NSException *exception)
    {
        nslog(@"\n Error in deleting file..  ");
    }
    
}

#pragma mark - manage_orientation
#pragma mark  Manages bottomview frame.

-(void)manage_orientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    
    
    
    if(!isIPad)
    {
        return;
    }
    

    
    switch (toInterfaceOrientation)
    {
        case UIInterfaceOrientationLandscapeLeft:
        {
            
            
        }
        case UIInterfaceOrientationLandscapeRight:
        {
            
            
            
            [dashboarad_bottom_button setFrame:CGRectMake(0,0,154,49)];
            [income_expsne_bottom_button setFrame:CGRectMake(154,0,205,49)];
            [calculator_bottom_button setFrame:CGRectMake(308,0,205,49)];
            [reminder_bottom_button setFrame:CGRectMake(462,0,205,49)];
            [ipad_settings_bottom_button setFrame:CGRectMake(616,0,205,49)];
            
            bottomView.frame = CGRectMake(0,220,480,44);
            break;
        }
            
            
            
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            
            
        }
            
            
        case UIInterfaceOrientationPortrait:
        {
            
            bottomView.frame =  CGRectMake(0,300,320,44);
            [dashboarad_bottom_button setFrame:CGRectMake(0,0,205,49)];
            [income_expsne_bottom_button setFrame:CGRectMake(205,0,205,49)];
            [calculator_bottom_button setFrame:CGRectMake(410,0,205,49)];
            [reminder_bottom_button setFrame:CGRectMake(615,0,205,49)];
            [ipad_settings_bottom_button setFrame:CGRectMake(820,0,205,49)];
            
            
            break;
        }
        default:
        {
            
            break;
        }
            
            
    }
    
    
}

#pragma mark - 
#pragma mark manageBottomBar_ios7_Hack

-(void)manageBottomBar_ios7_Hack
{
    
    if(isIPad)
    {
        for(id x in [self.tabBarController.view subviews])
        {
            if(![x isKindOfClass:[UITabBar class]])
            {
                
                if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
                {
                    //[(UIView*)x setFrame:CGRectMake(0,0,768,1024)];
                    
                    UIView*view = (UIView*)x;
                    view.frame = CGRectMake(0,0,768,1024);
                    view.backgroundColor = [UIColor redColor];
                    
                }
                else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4 )
                {
                    [(UIView*)x setFrame:CGRectMake(0,0,1024,768)];
                }
                
                
            }
        }
    }
    else
    {
        for(id x in [self.tabBarController.view subviews])
        {
            if(![x isKindOfClass:[UITabBar class]])
            {
                if(isIphone5)
                {
                    [(UIView*)x setFrame:CGRectMake(0,0,320,568)];
                }
                else
                {
                    [(UIView*)x setFrame:CGRectMake(0,0,320,480)];
                }
                
            }
        }
    }
    
    
}

#pragma mark - generateRandomImageName

- (NSString *)generateRandomImageName
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return [(NSString *)uuidStringRef autorelease];
}

#pragma mark - Memory management


- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
    
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    
}



- (void)dealloc
{
    [curCode release];
    
    [tabBarController release];
    [window release];
    [regionDateFormatter release];
    [receipt_image_name release];
    [receipt_small_image_name release];
    [receipt_image release];
    [agent_pk release];

    
    [incomeTypeDBArray release];
    [expenseTypeArray release];
    [reminderTypeArray release];
    [propertyTypeArray release];
    [propertyAddArray release];
    [propertyListArray release];
    [currencyArray release];
    [agentArray release];
    [propertyDetailArray release];
    [incomeExpenseArray release];
    [reminderArray release];
    [reminderArrayByProperty release];
    [incomeExpenseSummery release];
    
    [imgView_bottom_tabBar release];
    
    
    [super dealloc];
}

-(void)iCloudAccountAvailabilityChanged
{
    NSLog(@"\n bla bla bl \n");
}




@end

