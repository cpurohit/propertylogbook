//
//  propertyViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"


@class EquityViewController;
@interface propertyViewController : UIViewController<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate> {
    
    IBOutlet UITableView *tblView;
    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UILabel *lblNoData,*lblPlus;
	
    NSIndexPath *oldIndex;
    IBOutlet UIImageView *imageView;

    EquityViewController *equityView;
    NSMutableArray *tempRowArray;
    NSMutableArray *tempArray;
    
   
    
}

-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew;
-(void) cancelAlarm:(int)reminderRowID;
@end
