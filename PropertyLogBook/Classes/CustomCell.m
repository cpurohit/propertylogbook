//
//  CustomCell.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomCell.h"


@implementation CustomCell
@synthesize propertyLabel, PropertyTextField, agentSwitch, detailLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (self) 
    {
        PropertyTextField = [[UITextField alloc]init];
        PropertyTextField.borderStyle = UITextBorderStyleRoundedRect;
        [self.contentView addSubview:PropertyTextField];
       // [PropertyTextField release];  
    
        agentSwitch = [[UISwitch alloc]init];
        //        agentSwitch.hidden = TRUE;
        [agentSwitch setOn:TRUE];
        
        [self.contentView addSubview:agentSwitch];
        [agentSwitch release];
    
    
        propertyLabel = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        propertyLabel.numberOfLines = 0;
        propertyLabel.lineBreakMode = UILineBreakModeWordWrap;
        [propertyLabel setFont:[UIFont systemFontOfSize:15.0]];
        [propertyLabel setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:propertyLabel];
        
        [propertyLabel release];
        
        detailLabel = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        detailLabel.textAlignment = UITextAlignmentRight;
        
        detailLabel.numberOfLines = 0;
        detailLabel.lineBreakMode = UILineBreakModeTailTruncation;
        [detailLabel setFont:[UIFont systemFontOfSize:15.0]];
        [detailLabel setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:detailLabel];
        
        [detailLabel release];
    
    }
    return self;
}

-(void)layoutSubviews
{
    if(appDelegate.isIPad)
    {
        
    }
    else
    {
        PropertyTextField.frame = CGRectMake(150, 7, 140, 30);
        agentSwitch.frame = CGRectMake(196, 8, 94, 27);
        propertyLabel.frame = CGRectMake(10, 7, 152, 30);
        detailLabel.frame = CGRectMake(150, 7, 140, 30);
    
    }
    
        
	[super layoutSubviews];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
