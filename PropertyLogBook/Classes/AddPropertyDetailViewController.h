//
//  AddPropertyDetailViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"
#import "UIImage+Resize.h"


@interface AddPropertyDetailViewController : UIViewController<UITextFieldDelegate, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource,UIGestureRecognizerDelegate,UIActionSheetDelegate, UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate> {
    
    IBOutlet UITableView *tblView;
    PropertyLogBookAppDelegate *appDelegate;
    NSMutableArray *propertyInfoArray, *agentInfoArray, *financialArray/*this is used for property values*/,*loanInformatio,*leaseInformation;
    NSMutableDictionary *agenInfoDictionary;
    NSMutableDictionary *financialInfoDictionary;
    NSString *propertyName;
    NSMutableDictionary *propertyInfoDictionary;
    UIPopoverController *popController;
    BOOL isFullyPaid, isLease, isAgent;
    UIPickerView *pickerView;
    UIToolbar *toolBar;
    NSMutableArray *pickerArray;
    UIDatePicker *datePicker;
    NSString *leaseStart, *leaseEnd;
    int isStart;
    BOOL imageCount;
    
    int textFieldIndex, textViewIndex;
    
    NSMutableArray *propertyArray;
    UIToolbar *keytoolBar;
	
	IBOutlet UIView *headerView;
    IBOutlet UIButton*headerViewButton;
    IBOutlet UILabel*headerLineLabel;
	IBOutlet UITextField *txtName,*txtPropertyType;
	IBOutlet UILabel *lblName,*lblPropertyType;
	IBOutlet UIImageView *PropertyIcon;
	IBOutlet UIImageView *bkImgView;

	int used;
	UIImagePickerController *picker;
	BOOL isIOS5;
    
    
    UITextView *txtView;
    
    NSMutableArray*repayment_term_picker_array;
    int repayment_or_repayment_term;//1 repayment,2 repayment_term.Manages which data to show in pickerview;
    
    BOOL is_view_will_appear_called;

}
-(UIImage*)correctImageOrientation:(CGImageRef)image;
-(IBAction)property_select;

-(void)done_clicked;
@end
