//
//  AddPropertyViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"


@interface AddPropertyViewController : UIViewController<UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource,UITextViewDelegate> 
{
    IBOutlet UITableView *tblView;
    NSMutableArray *propertyInfoArray, *agentInfoArray, *financialArray;
    int isStart;
    UIDatePicker *datePicker;
    UIToolbar *toolBar;
    UISwitch *agentSwitch, *leaseSwitch, *fullyPaid;
   
    PropertyLogBookAppDelegate *appDelegate;
    BOOL isLeaseOn;
    BOOL isFullyPaid;
    BOOL isAgent;
    UIPickerView *pickerView;
    NSMutableArray *pickerArray;
    
    NSMutableArray *AddPropertyArray;
    //property information var
    NSString *nameStr, *address, *city, *state, *country, *propertyType, *currency, *leaseStart, *leaseEnd;
    
    // agent information
    
    NSString *agencyName, *contactPerson, *agentAddress, *agentCity, *agentState, *agentCountry, *agentPhone, *agentMobile, *agentEmail, *agentComission;
    
    // financial infomation
    
    NSString *purchasePrice, *estimatedPrice, *mortgageBank, *repaymentAmount, *repayment;
    
    /*Starts:Chirag*/
    
    NSMutableDictionary *agenInfoDictionary;
     NSMutableDictionary *financialInfoDictionary;
    
    NSMutableDictionary *propertyInfoDictionary;
    
    /*Ends:Chirag*/
    
}

@end
