//
//  CustomCell1.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomCell1.h"


@implementation CustomCell1
@synthesize agentManageSwitch, agentLabel, agentDateButton, agentTextField, agentDetailLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (self) 
    {
        // Initialization code
        agentTextField = [[UITextField alloc]init];
        agentTextField.borderStyle = UITextBorderStyleNone;
        agentTextField.textAlignment = UITextAlignmentRight;
        [agentTextField setFont:[UIFont systemFontOfSize:15.0]];
        [agentTextField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
        [self.contentView addSubview:agentTextField];
      //  [agentTextField release];  
     
        
        agentManageSwitch = [[UISwitch alloc]init];
     
        agentManageSwitch.onTintColor = [UIColor colorWithRed:10.0f/255.0f green:142.0f/255.0f blue:184.0f/255.0f alpha:1.0];
        
        [self.contentView addSubview:agentManageSwitch];
      //  [agentManageSwitch release];
        
        
        agentLabel = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        agentLabel.numberOfLines = 0;
        agentLabel.lineBreakMode = UILineBreakModeWordWrap;
        [agentLabel setFont:[UIFont systemFontOfSize:15.0]];
        [agentLabel setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:agentLabel];
        
        [agentLabel release];
        
        agentDetailLabel = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        agentDetailLabel.textAlignment = UITextAlignmentRight;
        agentDetailLabel.numberOfLines = 0;
        agentDetailLabel.lineBreakMode = UILineBreakModeTailTruncation;
        [agentDetailLabel setFont:[UIFont systemFontOfSize:15.0]];
        [agentDetailLabel setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:agentDetailLabel];
        
        [agentDetailLabel release];

        
        
        //agentDateButton = [[UIButton alloc]init];
        agentDateButton = [[UIButton buttonWithType:UIButtonTypeCustom]retain];
		[agentDateButton setBackgroundImage:[UIImage imageNamed:@"dropDownBlue.png"] forState:UIControlStateNormal];
		[agentDateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [agentDateButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
		[self.contentView addSubview:agentDateButton];
        

    }
    return self;
}

-(void)layoutSubviews
{
    
    if(appDelegate.isIPad)
    {
       
         
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            agentTextField.frame = CGRectMake(150, 13, 500, 30);
            agentLabel.frame = CGRectMake(10, 7, 152, 30);
            agentDetailLabel.frame = CGRectMake(140, 7, 480, 30);
            agentDateButton.frame = CGRectMake(150, 7, 140, 30);
            
            NSString *reqSysVer = @"5.0";
            NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
            nslog(@"\n currSysVer = %@",currSysVer);
            if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
            {
                agentManageSwitch.frame = CGRectMake(580, 8, 94, 27);
                
            }
            else
            {
                
                agentManageSwitch.frame = CGRectMake(550, 8, 94, 27);
            }

           
            if(appDelegate.isIOS7)
            {
                
                agentManageSwitch.frame = CGRectMake(agentManageSwitch.frame.origin.x+90,agentManageSwitch.frame.origin.y,agentManageSwitch.frame.size.width, agentManageSwitch.frame.size.height);
                
                agentDateButton.frame = CGRectMake(agentDateButton.frame.origin.x+90,agentDateButton.frame.origin.y,agentDateButton.frame.size.width, agentDateButton.frame.size.height);
                
                agentDetailLabel.frame = CGRectMake(agentDetailLabel.frame.origin.x+90,agentDetailLabel.frame.origin.y,agentDetailLabel.frame.size.width, agentDetailLabel.frame.size.height);
                
                
                
            }

            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            
            agentTextField.frame = CGRectMake(150, 13, 750, 30);
            
            agentDateButton.frame = CGRectMake(150, 7, 140, 30);
            agentLabel.frame = CGRectMake(10, 7, 152, 30);
            agentDetailLabel.frame = CGRectMake(140, 7,750, 30);
            
            NSString *reqSysVer = @"5.0";
            NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
            nslog(@"\n currSysVer = %@",currSysVer);
            if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
            {
               agentManageSwitch.frame = CGRectMake(840, 8, 94, 27);
                
            }
            else
            {
                
                agentManageSwitch.frame = CGRectMake(810, 8, 94, 27);
            }
            
            
            if(appDelegate.isIOS7)
            {
                
                agentManageSwitch.frame = CGRectMake(agentManageSwitch.frame.origin.x+90,agentManageSwitch.frame.origin.y,agentManageSwitch.frame.size.width, agentManageSwitch.frame.size.height);
                
                 agentDateButton.frame = CGRectMake(agentDateButton.frame.origin.x+90,agentDateButton.frame.origin.y,agentDateButton.frame.size.width, agentDateButton.frame.size.height);
                
                
                
                agentDetailLabel.frame = CGRectMake(agentDetailLabel.frame.origin.x+90,agentDetailLabel.frame.origin.y,agentDetailLabel.frame.size.width, agentDetailLabel.frame.size.height);
                
            }
            
        }
        
        

    }
    else
    {
        agentTextField.frame = CGRectMake(150, 13, 140, 30);
        
        
        NSString *reqSysVer = @"5.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        nslog(@"\n currSysVer = %@",currSysVer);
        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        {
            agentManageSwitch.frame = CGRectMake(211, 8, 94, 27);
            
            if(appDelegate.isIOS7)
            {
                
                agentManageSwitch.frame = CGRectMake(238, 8, 94, 27);
            }

            

        }
        else
        {
            agentManageSwitch.frame = CGRectMake(196, 8, 94, 27);
        }

        
        agentLabel.frame = CGRectMake(10, 7, 152, 30);
        agentDetailLabel.frame = CGRectMake(140, 7, 130, 30);
        agentDateButton.frame = CGRectMake(150, 7, 140, 30);

    }
    
        
	[super layoutSubviews];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
