//
//  DashBoardCustomCell.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 9/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DashBoardCustomCell.h"


@implementation DashBoardCustomCell
@synthesize headingLabel1, label, percentageLabel, amountLabel, headingLabel2, headingLabel3, lineLabel1, lineLabel2;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (self) {
        // Initialization code
        headingLabel1 = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        headingLabel1.numberOfLines = 0;
        [headingLabel1 setTextColor:[UIColor blackColor]];
        // [agentLabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];      
        headingLabel1.lineBreakMode = UILineBreakModeWordWrap;
         [headingLabel1 setFont:[UIFont boldSystemFontOfSize:15.0]];
        [headingLabel1 setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:headingLabel1];
        
        [headingLabel1 release];
        
        headingLabel2 = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        headingLabel2.numberOfLines = 0;
        [headingLabel2 setTextColor:[UIColor blackColor]];
        // [agentLabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];      
        headingLabel2.lineBreakMode = UILineBreakModeWordWrap;
        // [agentLabel setFont:[UIFont boldSystemFontOfSize:15.0]];
         [headingLabel2 setFont:[UIFont boldSystemFontOfSize:15.0]];      
        [headingLabel2 setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:headingLabel2];
        
        [headingLabel2 release];

        
        headingLabel3 = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        headingLabel3.numberOfLines = 0;
        [headingLabel3 setTextColor:[UIColor blackColor]];
        // [agentLabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];      
        //headingLabel3.lineBreakMode = UILineBreakModeTailTruncation;
        [headingLabel3 setFont:[UIFont boldSystemFontOfSize:15.0]];
        [headingLabel3 setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:headingLabel3];
        
        [headingLabel3 release];

        
        label = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        label.numberOfLines = 0;
        [label setTextColor:[UIColor blackColor]];
        // [agentLabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];      
        label.lineBreakMode = UILineBreakModeWordWrap;
        // [agentLabel setFont:[UIFont boldSystemFontOfSize:15.0]];
        [label setFont:[UIFont systemFontOfSize:13.0]];
        [label setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:label];
        
        [label release];
        
        percentageLabel = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        percentageLabel.numberOfLines = 0;
        [percentageLabel setTextColor:[UIColor blackColor]];
        // [agentLabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];      
        percentageLabel.lineBreakMode = UILineBreakModeWordWrap;
        // [agentLabel setFont:[UIFont boldSystemFontOfSize:15.0]];
        [percentageLabel setFont:[UIFont systemFontOfSize:13.0]];
        [percentageLabel setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:percentageLabel];
        
        [percentageLabel release];
        
        amountLabel = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        amountLabel.numberOfLines = 0;
        [amountLabel setTextColor:[UIColor blackColor]];
        // [agentLabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];      
        amountLabel.lineBreakMode = UILineBreakModeWordWrap;
        // [agentLabel setFont:[UIFont boldSystemFontOfSize:15.0]];
        [amountLabel setFont:[UIFont systemFontOfSize:13.0]];
        [amountLabel setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:amountLabel];
        
        [amountLabel release];

        
        lineLabel1 = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        lineLabel1.numberOfLines = 0;
        [lineLabel1 setTextColor:[UIColor blackColor]];
        // [agentLabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];      
        lineLabel1.lineBreakMode = UILineBreakModeWordWrap;
        // [agentLabel setFont:[UIFont boldSystemFontOfSize:15.0]];
        [lineLabel1 setBackgroundColor:[UIColor blackColor]];
        
        [self.contentView addSubview:lineLabel1];
        
        [lineLabel1 release];
        
        lineLabel2 = [[UILabel alloc]init];
        lineLabel2.numberOfLines = 0;
        [lineLabel2 setTextColor:[UIColor blackColor]];
         
        lineLabel2.lineBreakMode = UILineBreakModeWordWrap;
        [lineLabel2 setBackgroundColor:[UIColor blackColor]];
        
        [self.contentView addSubview:lineLabel2];
        
        [lineLabel2 release];

        
        

    }
    return self;
}

-(void)layoutSubviews
{
	[super layoutSubviews];
    
    if(appDelegate.isIPad)
    {
        headingLabel1.frame = CGRectMake(10, 10, 220, 30);
        headingLabel2.frame = CGRectMake(235, 10, 50, 30);
        headingLabel3.frame = CGRectMake(300, 10, 100, 30);
        
        label.frame = CGRectMake(10, 7, 220, 30);
        percentageLabel.frame = CGRectMake(235, 7, 50, 30);
        amountLabel.frame = CGRectMake(300, 7, 100, 30);
        
        lineLabel1.frame = CGRectMake(230, 0, 1, 44);
        lineLabel2.frame = CGRectMake(290, 0, 1, 44);

    }
    else
    {
        headingLabel1.frame = CGRectMake(10, 10, 120, 30);
        headingLabel2.frame = CGRectMake(135, 10, 50, 30);
        headingLabel3.frame = CGRectMake(200, 10, 100, 30);
        label.frame = CGRectMake(10, 7, 120, 30);
        percentageLabel.frame = CGRectMake(135, 7, 50, 30);
        amountLabel.frame = CGRectMake(200, 7, 100, 30);
        
        lineLabel1.frame = CGRectMake(130, 0, 1, 44);
        lineLabel2.frame = CGRectMake(190, 0, 1, 44);

    }

    
       
	//AddressTextView.frame = CGRectMake(150, 7, 140, 100);
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
