//
//  pdfViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 9/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "pdfViewController.h"
#import "PropertyLogBookAppDelegate.h"
#define FONT @"Arial-ItalicMT"
#define FONTSIZE 10.38
@implementation pdfViewController

static const CGFloat colorLookupTable[20][3] =
{
	{248.0/255.0, 155.0/255.0, 150.0/255.0},
	{206.0/255.0, 235.0/255.0, 145.0/255.0},
	{175.0/255.0, 151.0/255.0, 211.0/255.0},
	{102.0/255.0, 210.0/255.0, 236.0/255.0},
	{102.0/255.0, 236.0/255.0, 186.0/255.0},
	{236.0/255.0, 221.0/255.0, 102.0/255.0},
    {243.0/255.0, 203.0/255.0, 81.0/255.0},
    {236.0/255.0, 143.0/255.0, 102.0/255.0},
    {236.0/255.0, 102.0/255.0, 101.0/255.0},
    {192.0/255.0, 104.0/255.0, 103.0/255.0},
    {141.0/255.0, 221.0/255.0, 132.0/255.0},
    {170.0/255.0, 233.0/255.0, 202.0/255.0},
    {120.0/255.0, 200.0/255.0, 165.0/255.0},
    {247.0/255.0, 191.0/255.0, 254.0/255.0},
    {221.0/255.0, 156.0/255.0, 238.0/255.0},
    {198.0/255.0, 167.0/255.0, 120.0/255.0},
    {158.0/255.0, 133.0/255.0, 92.0/255.0},
    {154.0/255.0, 165.0/255.0, 229.0/255.0},
    {98.0/255.0, 141.0/255.0, 186.0/255.0},
    {116.0/255.0, 170.0/255.0, 230.0/255.0}
    
};



@synthesize fromString, toString, arrayIndex,currencySymbolImage;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/*
 - (void)dealloc
 {
 [super dealloc];
 [incomeTypeSummaryArray release];
 [expenseTypeSummaryArray release];
 [milageTypeSummaryArray release];
 
 [tempIncomeArray release];
 
 [incomeSummeryDictionary release];
 [expenseSummeryDictionary release];
 [milageSummeryDictionary release];
 
 [tempExpenseArray release];
 [tempMilageArray release];
 
 
 }
 */
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark -
#pragma mark  PDF Mainupaletion Methods

CGContextRef CreatePDFFile (CGRect pageRect, const char *filename)
{
	
    @autoreleasepool
    {
        // This code block sets up our PDF Context so that we can draw to it
        CGContextRef pdfContext;
        CFStringRef path;
        CFURLRef url;
        CFMutableDictionaryRef myDictionary = NULL;
        
        //Create a CFString from the filename we provide to this method when we call it
        path = CFStringCreateWithCString (NULL, filename,kCFStringEncodingUTF8);
        
        //Create a CFURL using the CFString we just defined
        url = CFURLCreateWithFileSystemPath (NULL, path,kCFURLPOSIXPathStyle, 0);
        CFRelease (path);
        
        //This dictionary contains extra options mostly for "signing" the PDF
        myDictionary = CFDictionaryCreateMutable(NULL, 0,
                                                 &kCFTypeDictionaryKeyCallBacks,
                                                 &kCFTypeDictionaryValueCallBacks);
        CFDictionarySetValue(myDictionary, kCGPDFContextTitle, CFSTR(""));
        CFDictionarySetValue(myDictionary, kCGPDFContextCreator, CFSTR(""));
        CFDictionarySetValue(myDictionary, kCGPDFContextAuthor , CFSTR(""));
        
        
        
        
        //Create our PDF Context with the CFURL, the CGRect we provide, and the above defined dictionary
        pdfContext = CGPDFContextCreateWithURL(url, &pageRect, myDictionary);
        
        //Cleanup our mess
        CFRelease(myDictionary);
        CFRelease(url);
        
        
        return pdfContext;
        
    }
    
    
    
}

void writeingContextToPDF(CGContextRef pdfContext, CGFloat x, CGFloat y, NSString *textContext)
{
    @autoreleasepool
    {
        nslog(@"\n starting point x  = %f",x);
        nslog(@"\n textContext x  = %@",textContext);
        
        //CGContextSetTextDrawingMode (pdfContext,kCGTextFill);
        
        
        CGContextShowTextAtPoint(pdfContext, x, y, [textContext UTF8String], [textContext length]);
    }
	
}
void drawImageToPDF(CGContextRef pdfContext,CGRect pageRect, NSString *imageName){
	
    
    // This code block will create an image that we then draw to the page
	//const char *picture = "letter_head";
	CGImageRef image;
	CGDataProviderRef provider;
	const char *filename;
    
	filename=[imageName UTF8String];
	provider =CGDataProviderCreateWithFilename(filename);
 	
	image = CGImageCreateWithPNGDataProvider (provider, NULL, true, kCGRenderingIntentDefault);
	CGDataProviderRelease (provider);
	
	CGContextDrawImage (pdfContext,pageRect,image);
	CGImageRelease (image);
	// End image code
}


-(void)getGlyph
{
    
    @autoreleasepool
    {
        
        /*
         NSLocale *locale;
         NSString *currencySymbol = [locale objectForKey:NSLocaleCurrencySymbol];
         */
        // nslog(@"\n currencySymbol = %@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencySymbol]);
        NSString *dollar;
        
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
        
        NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
        
        if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
        {
            [currencyFormatter setCurrencyCode:appDelegate.curCode];
            
            if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
            {
                [currencyFormatter setCurrencyCode:@"USD"];
                nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                [currencyFormatter setLocale:locale];
                dollar = [locale displayNameForKey:NSLocaleCurrencySymbol value:@"USD"];
                //[locale release];
            }
            
            else if([[currencyFormatter currencyCode] isEqualToString:@"CNY"])
            {
                [currencyFormatter setCurrencyCode:@"CNY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                [currencyFormatter setLocale:locale];
                dollar = [locale displayNameForKey:NSLocaleCurrencySymbol value:@"CNY"];
                // [locale release];
                
            }
            
            else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
            {
                [currencyFormatter setCurrencyCode:@"JPY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                [currencyFormatter setLocale:locale];
                dollar = [locale displayNameForKey:NSLocaleCurrencySymbol value:@"JPY"];
                // [locale release];
                
            }
            
            else
            {
                dollar = [locale displayNameForKey:NSLocaleCurrencySymbol value:appDelegate.curCode];
            }
            
            
        }
        else
        {
            [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            dollar = [currencyFormatter currencySymbol];
        }
        nslog(@"\n dollar = %@",dollar);
        //[locale release];
        localeString = [currencyFormatter currencyCode];
        // [currencyFormatter release];
        len = [dollar length];
        if([localeString isEqualToString:@"PKR"] || [localeString isEqualToString:@"KRW"])
        {
            len++;
        }
        
        if([localeString isEqualToString:@"CHF"])
        {
            len = 2;
        }
        
        if([localeString isEqualToString:@"DKK"] || [localeString isEqualToString:@"IDR"] || [localeString isEqualToString:@"MYR"] || [localeString isEqualToString:@"NOK"] || [localeString isEqualToString:@"ZAR"]|| [localeString isEqualToString:@"SEK"])
        {
            len =3;
        }
        
        nslog(@"\n dollar len = %d",len);
        nslog(@"\n localeString = %@",localeString);
        if([localeString isEqualToString:@"INR"])
        {
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(160,160),NO,10.0f);
            UIImage *img = [UIImage imageNamed:@"rupee_symbol.jpg"];
            
            /*
             currencySymbolImage = [[UIImage alloc] initWithCGImage:img.CGImage scale:1.0f orientation:UIImageOrientationUp];
             */
            [img drawAtPoint:CGPointMake(0,4)];
            currencySymbolImage = UIGraphicsGetImageFromCurrentImageContext();
        }
        else
        {
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(100,100),NO,10.0f);
            
            if([localeString isEqualToString:@"JPY"]  )
            {
                [dollar drawAtPoint:CGPointMake(-1,2)
                           withFont:[UIFont fontWithName:@"Arial" size:18.0f]];
            }
            else if([localeString isEqualToString:@"CNY"])
            {
                [dollar drawAtPoint:CGPointMake(-2,2)
                           withFont:[UIFont fontWithName:@"Arial" size:18.0f]];
            }
            else if([localeString isEqualToString:@"MKD"] || [localeString isEqualToString:@"CHF"] ||  [localeString isEqualToString:@"RUB"])
            {
                [dollar drawAtPoint:CGPointMake(0,4)
                           withFont:[UIFont fontWithName:@"Arial" size:15.0f]];
                
            }
            else
            {
                [dollar drawAtPoint:CGPointMake(0,0)
                           withFont:[UIFont fontWithName:@"Arial" size:20.0f]];
            }
            currencySymbolImage = UIGraphicsGetImageFromCurrentImageContext();
            
        }
        
        
        
        
        
        
        
        /*Chirag:Starts:Glyph...*/
        
        
        NSString*string ;//= [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"8"]floatValue]]];
        
        
        
        string = dollar;
        
        nslog(@"\n string = %@",string);
        
        
        // get attributed-string from string
        
		UIFont* font = [UIFont fontWithName:FONT
									   size:FONTSIZE];
        
		assert (font != nil);
		CTFontRef ctFont = CTFontCreateWithName((__bridge CFStringRef)font.fontName,
												font.pointSize,
												NULL);
		
		NSNumber* NS_0 = [NSNumber numberWithInteger:0];
		NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
									(__bridge id) ctFont,kCTFontAttributeName, // NSFontAttributeName
									(id) NS_0,kCTLigatureAttributeName,  // NSLigatureAttributeName
									nil];
		assert(attributes != nil);
		
		NSAttributedString* ns_attrString = [[NSAttributedString alloc] initWithString:string
																			attributes:attributes];
		//[ns_attrString autorelease];
		nslog(@"\n ns_attrString = %@",ns_attrString);
		attStr = (__bridge CFAttributedStringRef) ns_attrString;
        
        
        
        nslog(@"\n attrString = %@",attStr);
        CTLineRef line = CTLineCreateWithAttributedString( attStr ) ;
        CFArrayRef runArray = CTLineGetGlyphRuns(line);
        nslog(@"\n runArray = %@",runArray);
        
        // for each GLYPH in run
        
        CGContextRef X = UIGraphicsGetCurrentContext();
        for (CFIndex runIndex = 0; runIndex < CFArrayGetCount(runArray); runIndex++)
        {
            // Get FONT for this run
            CTRunRef run = (CTRunRef)CFArrayGetValueAtIndex(runArray, runIndex);
            CTFontRef runFont = CFDictionaryGetValue(CTRunGetAttributes(run), kCTFontAttributeName);
            cfIndex = CTRunGetGlyphCount(run);
            nslog(@"\n runIndex = %ld",runIndex);
            for (CFIndex runGlyphIndex = 0; runGlyphIndex < 1; runGlyphIndex++)
            {
                nslog(@"\n runGlyphIndex = %ld",runGlyphIndex);
                CFRange thisGlyphRange = CFRangeMake(runGlyphIndex,CTRunGetGlyphCount(run));
                CTRunGetGlyphs(run, thisGlyphRange, &glyph);
                {
                    CGFontRef cgFont = CTFontCopyGraphicsFont(runFont, NULL);
                    
                    //CGAffineTransform textMatrix = CTRunGetTextMatrix(run);
                    
                    CGContextSetFontSize(X, CTFontGetSize(runFont));
                    CFRelease(cgFont);
                }
                
            }
        }
        
        
        
    }
    
    /*Chirag:Ends:Glyph...*/
    
}





#pragma mark - View lifecycle

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //UIImage *image = [UIImage imageNamed:@"propertyLogBook.png"];
    
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    @autoreleasepool
    {
        
        // Code, such as a loop that creates a large number of temporary objects.
        
        
        UIImage *image = [UIImage imageNamed:@"propertyLogBook_pdf_icon.png"];
        
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        //[button setTitle:@"  Back" forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        incomeTypeSummaryArray = [[NSMutableArray alloc]init];
        expenseTypeSummaryArray = [[NSMutableArray alloc]init];
        milageTypeSummaryArray = [[NSMutableArray alloc]init];
        
        
        appDelegate=(PropertyLogBookAppDelegate *)[[UIApplication sharedApplication] delegate];
        
        
        if([appDelegate.propertyDetailArray count]==0)
        {
            [appDelegate selectPropertyDetail];
        }
        
        if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
        {
            [appDelegate selectCurrencyIndex];
        }
        if([appDelegate.agentArray count]==0)
        {
            [appDelegate selectAgentType];
        }
        
        
        
        
        
        
        
        
        numberFormatter = [[NSNumberFormatter alloc] init] ;
        [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
        if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
        {
            [numberFormatter setCurrencyCode:appDelegate.curCode];
            
            if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
            {
                [numberFormatter setCurrencyCode:@"USD"];
                nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                [numberFormatter setLocale:locale];
                // [locale release];
                
            }
            else if([[numberFormatter currencyCode] isEqualToString:@"CNY"])
            {
                [numberFormatter setCurrencyCode:@"CNY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                [numberFormatter setLocale:locale];
                // [locale release];
                
            }
            
            else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
            {
                [numberFormatter setCurrencyCode:@"JPY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                [numberFormatter setLocale:locale];
                // [locale release];
                
            }
            
        }
        
        nslog(@"\n appDelegate.propertyDetailArray  = %@",[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]);
        
        
        
        agent_information = [appDelegate getAgentInforById:[[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] valueForKey:@"agent_pk"] intValue]];
        
        
        nslog(@"\n agent_information  = %@",agent_information);
        
        
        self.navigationItem.title = [[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] valueForKey:@"0"];
        
        segmentedControl = [[UISegmentedControl alloc] initWithItems:
                            [NSArray arrayWithObjects:@"Print",@"Email",nil]];
        [segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
        segmentedControl.frame = CGRectMake(0, 0, 90, 30);
        segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
        segmentedControl.momentary = YES;
        
        
        
        
        
        segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView:segmentedControl];
        //[segmentedControl release];
        
        self.navigationItem.rightBarButtonItem = segmentBarItem;
        //[segmentBarItem release];
        
        
        if ([UIImage instancesRespondToSelector:@selector(imageWithRenderingMode:)]) {
            
            [segmentedControl setImage:[[UIImage imageNamed:@"pdf.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:0];
            
        }
        
        
        nslog(@"currency code === %@",appDelegate.curCode);
        
        NSString *error;
        rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        
        /*
         saveFileName=[NSString stringWithFormat:@"%@%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"],@".plist"];
         saveFileName  = [saveFileName stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
         plistPath = [rootPath stringByAppendingPathComponent:saveFileName];
         */
        
        
        saveFile_Name=[NSString stringWithFormat:@"%@%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"],@".pdf"];
        saveFile_Name  = [saveFile_Name stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
        
        newFilePath =	[rootPath stringByAppendingPathComponent:saveFile_Name];
        
        //[newFilePath retain];
        CGRect pageRect = CGRectMake(0, 0,640,842 ); // set PDF Size,actual width is 820...
        
        
        
        pdf=CreatePDFFile(pageRect, [newFilePath UTF8String]); // Generate Empty PDF file
        
        
        
        CGContextBeginPage (pdf, &pageRect);
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 18);
        
        
        
        CGSize text_size;
        
        NSString*temp_string = [NSString stringWithFormat:@"%@ - Transaction Report",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"0"]];
        
        text_size = [self calculate_size:temp_string font_size:18.0];
        
        
        
        
        
        
        
        writeingContextToPDF(pdf,(640-text_size.width)/2,800,[NSString stringWithFormat:@"%@ - Transaction Report",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"0"]]);
        
        
        //writeingContextToPDF(pdf,pdf_half_width-string_count,800,@"A");
        
        
        [self getGlyph];
        
        
        
        
        //Address
        
        
        CGContextSetFontSize(pdf, 14);
        NSString *address = [NSString stringWithFormat:@"%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"address"]];
        address = [address stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        
        temp_string = [NSString stringWithFormat:@"Address: %@",address];
        text_size = [self calculate_size:temp_string font_size:14.0];
        
        
        
        
        writeingContextToPDF(pdf,(640-text_size.width)/2, 782,[NSString stringWithFormat:@"Address: %@",address]);
        
        temp_string = [NSString stringWithFormat:@"Property Type: %@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"propertytype"]];
        text_size = [self calculate_size:temp_string font_size:14.0];
        
        writeingContextToPDF(pdf,(640-text_size.width)/2, 768,[NSString stringWithFormat:@"Property Type: %@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"propertytype"]]);
        
        
        
        
        
        
        
        
        CGContextSetFontSize(pdf, 16);
        
        temp_string = [NSString stringWithFormat:@"From: %@ - To: %@",fromString,toString];
        text_size = [self calculate_size:temp_string font_size:16.0];
        
        writeingContextToPDF(pdf,(640-text_size.width)/2,740,[NSString stringWithFormat:@"From: %@ - To: %@",fromString,toString]);
        
        
        CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 14);
        writeingContextToPDF(pdf,56,704,@"Property Financials");
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        
        
        
        temp_string = [NSString stringWithFormat:@"Property Financials%@",@""];
        text_size = [self calculate_size:temp_string font_size:15.0];
        
        
        CGContextMoveToPoint(pdf,56, 694);
        CGContextAddLineToPoint(pdf,56+text_size.width,694);
        CGContextStrokePath(pdf);
        
        
        
        
        /*
         as box lies between 56 to 600
         640-56 = 584
         584-40 = 544
         */
        
        /*Starts : drawing*/
        
        CGContextMoveToPoint(pdf,52, 680);
        CGContextAddLineToPoint(pdf,600,680);
        CGContextStrokePath(pdf);
        
        
        
        CGContextMoveToPoint(pdf,52, 680);
        CGContextAddLineToPoint(pdf,52,625);
        CGContextStrokePath(pdf);
        
        
        
        CGContextMoveToPoint(pdf,326, 625);
        CGContextAddLineToPoint(pdf,326,680);
        CGContextStrokePath(pdf);
        
        
        
        CGContextMoveToPoint(pdf,52, 625);
        CGContextAddLineToPoint(pdf,600,625);
        CGContextStrokePath(pdf);
        
        
        
        
        
        
        CGContextMoveToPoint(pdf,600, 680);
        CGContextAddLineToPoint(pdf,600,625);
        CGContextStrokePath(pdf);
        
        
        
        /*Ends : Draw*/
        
        
        //Starts Purchase price
        
        CGContextSetFontSize(pdf, 12);
        writeingContextToPDF(pdf, 56, 661,[NSString stringWithFormat:@"Purchase Price: %@",@""]);
        CGContextDrawImage(pdf,CGRectMake(149,612,60,60),currencySymbolImage.CGImage);//Drawing Image...
        
        
        
        
        
        
        //NSString*purchasePrice = [[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"8"];
        
        NSString*purchasePrice = [NSString stringWithFormat:@"%.2f",    [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"8"] floatValue]];
        
        nslog(@"\n purchase price = %@",purchasePrice);
        if([[purchasePrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
        {
            purchasePrice = @"0.00";
        }
        if(len == 1)
        {
            writeingContextToPDF(pdf,160, 661,purchasePrice);
        }
        else if(len == 2)
        {
            writeingContextToPDF(pdf,170, 661,purchasePrice);
        }
        else if(len == 4)
        {
            writeingContextToPDF(pdf,190, 661,purchasePrice);
        }
        else if(len == 3)
        {
            
            nslog(@"\n purchasePrice = %@",purchasePrice);
            
            if([localeString isEqualToString:@"CNY"])
            {
                writeingContextToPDF(pdf,185, 661,purchasePrice);
            }
            else
            {
                writeingContextToPDF(pdf,180, 661,purchasePrice);
            }
            
            
        }
        
        //Ends Purchase price
        
        CGContextMoveToPoint(pdf,52, 655);
        CGContextAddLineToPoint(pdf,326,655);
        CGContextStrokePath(pdf);
        
        
        /*Starts Market Value*/
        
        
        
        writeingContextToPDF(pdf, 56,631,[NSString stringWithFormat:@"Market Value: %@",@""]);
        
        
        
        
        //NSString*marketValue = [[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"9"];
        NSString*marketValue = [NSString stringWithFormat:@"%.2f",[[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"9"] floatValue]];
        
        if([[marketValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
        {
            marketValue = @"0.0";
        }
        
        
        
        //CGContextShowGlyphsAtPoint(pdf,165,586,&glyph,cfIndex);
        CGContextDrawImage(pdf,CGRectMake(136,582,60,60),currencySymbolImage.CGImage);//Drawing Image...
        if(len == 1)
        {
            writeingContextToPDF(pdf, 150, 631,marketValue);
        }
        else if(len == 2)
        {
            writeingContextToPDF(pdf, 160, 631,marketValue);
        }
        else if(len == 3)
        {
            writeingContextToPDF(pdf, 170, 631,marketValue);
        }
        else if(len == 4)
        {
            writeingContextToPDF(pdf, 180, 631,marketValue);
        }
        
        /*Ends Market Value*/
        
        
        /*Starts : Equity*/
        
        
        //  start 56 to 600
        //middle 264
        //so for equity 600-264 == 336
        //336
        
        
        text_size = [self calculate_size:@"EQUITY%@" font_size:16.0];
        
        nslog(@"\n equity text size = %f",text_size.width);
        
        /*cell background*/
        
        CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
        CGContextFillRect(pdf, CGRectMake(326,625,274, 55));
        CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
        
        
        
        CGContextSetFontSize(pdf, 16);
        writeingContextToPDF(pdf,(475-text_size.width/2),661,[NSString stringWithFormat:@"EQUITY%@",@""]);
        
        
        /*Sun:004
         CGContextMoveToPoint(pdf,52, 681);
         CGContextAddLineToPoint(pdf,600,681);
         CGContextStrokePath(pdf);
         */
        
        
        //CGContextShowGlyphsAtPoint(pdf,120,526,&glyph,cfIndex);
        CGContextDrawImage(pdf,CGRectMake(430,582,60,60),currencySymbolImage.CGImage);//Drawing Image...
        
        CGContextSetFontSize(pdf, 12);
        
        
        if(len==1)
        {
            writeingContextToPDF(pdf,444,631,[NSString stringWithFormat:@"%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"zEquity"]]);
            
        }
        else if(len == 2)
        {
            writeingContextToPDF(pdf,454, 631,[NSString stringWithFormat:@"%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"zEquity"]]);
            
        }
        else if(len == 3)
        {
            writeingContextToPDF(pdf,464, 631,[NSString stringWithFormat:@"%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"zEquity"]]);
        }
        else if(len == 4)
        {
            writeingContextToPDF(pdf,474, 631,[NSString stringWithFormat:@"%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"zEquity"]]);
        }
        
        
        /*Ends : Equity*/
        
        
        /*Starts: Loan Information*/
        
        int yPosition = 595;
        
        if([[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"fullypaid"] intValue]==0)
        {
            
            yPosition = 477.0f;
            
            /*Starts : drawing box */
            
            CGContextMoveToPoint(pdf,52, 571);
            CGContextAddLineToPoint(pdf,600,571);
            CGContextStrokePath(pdf);
            
            
            
            CGContextMoveToPoint(pdf,52, 543);
            CGContextAddLineToPoint(pdf,600,543);
            CGContextStrokePath(pdf);
            
            
            
            CGContextMoveToPoint(pdf,52, 510);
            CGContextAddLineToPoint(pdf,600,510);
            CGContextStrokePath(pdf);
            
            
            CGContextMoveToPoint(pdf,52, 477);
            CGContextAddLineToPoint(pdf,600,477);
            CGContextStrokePath(pdf);
            
            
            //left side v line
            
            CGContextMoveToPoint(pdf,52, 571);
            CGContextAddLineToPoint(pdf,52,477);
            CGContextStrokePath(pdf);
            
            
            //right side v line
            
            CGContextMoveToPoint(pdf,600, 571);
            CGContextAddLineToPoint(pdf,600,477);
            CGContextStrokePath(pdf);
            
            
            
            
            
            
            /*Ends : Drawing box*/
            
            
            
            
            
            
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 14);
            writeingContextToPDF(pdf,56,595,@"Loan Information");
            
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            
            
            temp_string = [NSString stringWithFormat:@"Loan Information%@",@""];
            text_size = [self calculate_size:temp_string font_size:15.0];
            
            
            CGContextMoveToPoint(pdf,56,585);
            CGContextAddLineToPoint(pdf,56+text_size.width,585);
            CGContextStrokePath(pdf);
            
            
            /*Starts: cell background*/
            
            CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
            CGContextFillRect(pdf, CGRectMake(53,544,546,26));
            CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
            
            /* Ends: cell background*/
            
            //Center vertical line
            
            CGContextMoveToPoint(pdf,328, 571);
            CGContextAddLineToPoint(pdf,328,477);
            CGContextStrokePath(pdf);
            
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            writeingContextToPDF(pdf,56,552,[NSString stringWithFormat:@"Mortgage Bank: %@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"11"]]);
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            /*Starts : Loan Borrowed*/
            
            writeingContextToPDF(pdf,56, 522,[NSString stringWithFormat:@"Loan Borrowed: %@",@""]);
            
            CGContextDrawImage(pdf,CGRectMake(162,473,60,60),currencySymbolImage.CGImage);//Drawing Image...
            
            if(len == 1)
            {
                
                
                
                writeingContextToPDF(pdf,175, 522,[NSString stringWithFormat:@"%.2f", [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"12"] floatValue]]);
            }
            else if(len == 2)
            {
                writeingContextToPDF(pdf,185, 522,[NSString stringWithFormat:@"%.2f", [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"12"] floatValue]]);
            }
            else if(len == 3)
            {
                writeingContextToPDF(pdf,195, 522,[NSString stringWithFormat:@"%.2f", [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"12"] floatValue]]);
            }
            else if(len == 4)
            {
                writeingContextToPDF(pdf,205, 522,[NSString stringWithFormat:@"%.2f", [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"12"] floatValue]]);
            }
            /*Ends : Loan Borrowed*/
            
            
            
            
            /*Starts : Loan Outstanding*/
            
            writeingContextToPDF(pdf, 56, 492,[NSString stringWithFormat:@"Loan Outstanding: %@",@""]);
            
            
            
            CGContextDrawImage(pdf,CGRectMake(162,443,60,60),currencySymbolImage.CGImage);//Drawing Image...
            
            if(len == 1)
            {
                
                
                writeingContextToPDF(pdf,175, 492, [NSString stringWithFormat:@"%.2f",    [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"13"] floatValue]]);
            }
            else if(len == 2)
            {
                writeingContextToPDF(pdf,185, 492,[NSString stringWithFormat:@"%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"13"]]);
            }
            else if(len == 3)
            {
                writeingContextToPDF(pdf,195, 492, [NSString stringWithFormat:@"%.2f",    [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"13"] floatValue]]);
            }
            else if(len == 4)
            {
                writeingContextToPDF(pdf,205, 492, [NSString stringWithFormat:@"%.2f",[[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"13"] floatValue]]);
            }
            
            
            /*Ends : Loan Outstanding*/
            
            
            
            
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            
            writeingContextToPDF(pdf,335,552,[NSString stringWithFormat:@"Loan Term: %@ years ( %@ )",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"14"],[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"repayment_term"]]);
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            /*Starts : Repayment Amount */
            
            writeingContextToPDF(pdf,335, 522,[NSString stringWithFormat:@"Repayment Amount: %@",@""]);
            
            CGContextDrawImage(pdf,CGRectMake(447,473,60,60),currencySymbolImage.CGImage);//Drawing Image...
            
            
            
            if(len == 1)
            {
                writeingContextToPDF(pdf,459, 522,[NSString stringWithFormat:@"%.2f",[[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"15"] floatValue]]);
            }
            else if(len == 2)
            {
                writeingContextToPDF(pdf,469, 522,[NSString stringWithFormat:@"%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"15"]]);
            }
            else if(len == 3)
            {
                writeingContextToPDF(pdf,479, 522,[NSString stringWithFormat:@"%.2f",[[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"15"] floatValue]]);
            }
            else if(len == 4)
            {
                writeingContextToPDF(pdf,489, 522,[NSString stringWithFormat:@"%.2f",[[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"15"] floatValue]]);
            }
            /*Ends : Repayment Amount */
            
            
            /*Starts : Repayment frequency */
            
            writeingContextToPDF(pdf, 335, 492,[NSString stringWithFormat:@"Repayment frequency: %@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"repayment"]]);
            
            
            /*Ends :  Repayment frequency */
            
            
            
        }
        
        
        
        final_position_after_bloack =yPosition;
        
        /*Ends: Loan Information*/
        
        
        
        
        /*Starts: Lease Information*/
        
        
        
        if([[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"lease"] intValue]==1)
        {
            
            
            
            /*Starts : drawing box */
            
            CGContextMoveToPoint(pdf,52, yPosition-54);
            CGContextAddLineToPoint(pdf,600,yPosition-54);
            CGContextStrokePath(pdf);
            
            
            
            /* Line after  rental income*/
            CGContextMoveToPoint(pdf,52, yPosition-84);
            CGContextAddLineToPoint(pdf,600,yPosition-84);
            CGContextStrokePath(pdf);
            /* Line after  rental income*/
            
            
            
            
            
            
            
            /*Ends : Drawing box*/
            
            
            
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf,14);
            writeingContextToPDF(pdf,56,yPosition-30,@"Lease Information");
            
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            
            
            temp_string = [NSString stringWithFormat:@"Lease Information%@",@""];
            text_size = [self calculate_size:temp_string font_size:16.0];
            
            
            CGContextMoveToPoint(pdf,56,yPosition-40);
            CGContextAddLineToPoint(pdf,56+text_size.width,yPosition-40);
            CGContextStrokePath(pdf);
            
            
            
            
            
            /*Starts: cell background*/
            
            CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
            CGContextFillRect(pdf, CGRectMake(52,yPosition-83,548,28));
            CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
            
            /* Ends: cell background*/
            
            
            /*Starts : Rental Income */
            
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            writeingContextToPDF(pdf,56,yPosition-73,[NSString stringWithFormat:@"Rental Income:%@",@""]);
            CGContextDrawImage(pdf,CGRectMake(162,yPosition-123,60,60),currencySymbolImage.CGImage);//Drawing Image...
            
            
            
            
            
            if(len == 1)
            {
                writeingContextToPDF(pdf,175, yPosition-73,        [NSString stringWithFormat:@"%.2f",    [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"rental_income"] floatValue]]);
            }
            else if(len == 2)
            {
                writeingContextToPDF(pdf,185, yPosition-73,        [NSString stringWithFormat:@"%.2f",    [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"rental_income"] floatValue]]);
            }
            else if(len == 3)
            {
                writeingContextToPDF(pdf,195, yPosition-73,        [NSString stringWithFormat:@"%.2f",    [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"rental_income"] floatValue]]);
            }
            else if(len == 4)
            {
                writeingContextToPDF(pdf,205, yPosition-73,        [NSString stringWithFormat:@"%.2f",    [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"rental_income"] floatValue]]);
            }
            /*Ends : Loan Borrowed*/
            
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            /*Starts : Tenanat Information*/
            
            writeingContextToPDF(pdf, 56, yPosition-103,[NSString stringWithFormat:@"Tenant Information: %@",@""]);
            
            NSString *tenant_info = [[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"tenant_information"];
            
            //nslog(@"length == %d",tenant_info.length);
            
            int string_length = [tenant_info length];
            
            
            
            
            array_notes = [[NSMutableArray alloc] init];
            
            nslog(@"\n len = %d",string_length);
            nslog(@"\n len/50 = %d",string_length/50);
            int count = string_length/50;
            if(string_length%50!=0)
            {
                count++;
            }
            
            int j=0;
            for(int i=0;i<count;i++)
            {
                if([tenant_info length] >=50)
                {
                    NSRange stringRange = {0, MIN([tenant_info length],50)};
                    // adjust the range to include dependent chars
                    stringRange = [tenant_info rangeOfComposedCharacterSequencesForRange:stringRange];
                    // Now you can create the short string
                    NSString *shortString = [tenant_info substringWithRange:stringRange];
                    //nslog(@"shortString == %@",shortString);
                    tenant_info = [tenant_info substringFromIndex:50];
                    // [tenant_info retain];
                    [array_notes addObject:shortString];
                    j = j+50;
                }
                else
                {
                    
                    NSRange stringRange = {0, MIN([tenant_info length],[tenant_info length])};
                    // adjust the range to include dependent chars
                    stringRange = [tenant_info rangeOfComposedCharacterSequencesForRange:stringRange];
                    // Now you can create the short string
                    NSString *shortString = [tenant_info substringWithRange:stringRange];
                    //nslog(@"shortString == %@",shortString);
                    tenant_info = [tenant_info substringFromIndex:[tenant_info length]];
                    //   [tenant_info retain];
                    [array_notes addObject:shortString];
                    
                }
            }
            
            nslog(@"\n array_notes = %@",array_notes);
            
            int start_y_pos = 120;
            
            if([array_notes count]==0)
            {
                writeingContextToPDF(pdf, 56, yPosition-120,[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"tenant_information"]);
            }
            else
            {
                
                
                for(int z=0;z<[array_notes count];z++)
                {
                    writeingContextToPDF(pdf, 56, yPosition-start_y_pos,[array_notes objectAtIndex:z]);
                    start_y_pos+=14;
                }
                
                
            }
            
            
            /*Drawing line at bottom lease box*/
            
            
            float lease_bottom_line_pos;
            
            if(yPosition-152 < yPosition-start_y_pos)
            {
                lease_bottom_line_pos = yPosition-152;
                
                final_position_after_bloack = yPosition-152;
                
            }
            else
            {
                lease_bottom_line_pos = yPosition-(start_y_pos);
                final_position_after_bloack =yPosition-(start_y_pos);
            }
            
            CGContextMoveToPoint(pdf,52, lease_bottom_line_pos);
            CGContextAddLineToPoint(pdf,600,lease_bottom_line_pos);
            CGContextStrokePath(pdf);
            
            /*Drawing line at bottom lease box*/
            
            
            
            /*Drawing  v line at middle lease box*/
            
            
            CGContextMoveToPoint(pdf,328, yPosition-54);
            CGContextAddLineToPoint(pdf,328,lease_bottom_line_pos);
            CGContextStrokePath(pdf);
            
            
            /*Drawing  v line at middle lease box*/
            
            
            
            /* Line left side of box*/
            CGContextMoveToPoint(pdf,52, yPosition-54);
            CGContextAddLineToPoint(pdf,52,lease_bottom_line_pos);
            CGContextStrokePath(pdf);
            /* Line left side of box*/
            
            
            /* Line right side of box*/
            CGContextMoveToPoint(pdf,600, yPosition-54);
            CGContextAddLineToPoint(pdf,600,lease_bottom_line_pos);
            CGContextStrokePath(pdf);
            /* Line right sideo of box*/
            
            
            /*
             text_size = [self calculate_size:[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"tenant_information"] font_size:12.0];
             */
            
            
            /*Ends :  Tenanat Information */
            
            
            
            
            
            
            /*Starts :  Lease start date */
            
            writeingContextToPDF(pdf,335,yPosition-103,[NSString stringWithFormat:@"Lease Start Date: %@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"leasestart"]]);
            /*Ends :  Lease start date */
            
            
            
            /* Line after lease start date*/
            CGContextMoveToPoint(pdf,328, yPosition-115);
            CGContextAddLineToPoint(pdf,600,yPosition-115);
            CGContextStrokePath(pdf);
            /* Line after lease start date*/
            
            
            /*Starts : Lease End Date */
            
            writeingContextToPDF(pdf, 335,yPosition-133,[NSString stringWithFormat:@"Lease End Date: %@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"leaseend"]]);
            
            
            /*Ends :  Lease End Date */
            
            
            
        }
        
        /*Ends: Lease Information*/
        
        
        
        
        
        
        
        
        
        
        /*Starts: Agent Information*/
        
        
        CGContextSetFontSize(pdf, 14);
        
        
        yPosition = final_position_after_bloack;
        
        
        if([[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"agentManage"] intValue]==1)
        {
            
            /* SUN:004:REMOVED NEW PAGE
             if ( ([[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"fullypaid"] intValue]==0)
             
             && ([[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"lease"] intValue]==1)
             )
             {
             //if both exists start agent in new page...
             
             
             CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
             CGContextSetFontSize(pdf, 10);
             writeingContextToPDF(pdf,440, 18, @"Powered By");
             
             CGContextDrawImage(pdf,CGRectMake(496, 10,32, 32),image.CGImage);
             
             CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
             CGContextSetFontSize(pdf, 12);
             
             writeingContextToPDF(pdf,530,18, @"Property Log Book");
             
             CGContextEndPage(pdf);
             
             
             
             
             
             
             CGContextBeginPage (pdf, &pageRect);
             CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
             
             
             
             yPosition = 800;
             
             }
             //else
             */
            
            {
                
                
                
                /*Starts : drawing box */
                
                
                
                
                
                
                CGContextMoveToPoint(pdf,52, yPosition-54);
                CGContextAddLineToPoint(pdf,600,yPosition-54);
                CGContextStrokePath(pdf);
                
                
                
                /* Line after  agent information */
                CGContextMoveToPoint(pdf,52, yPosition-84);
                CGContextAddLineToPoint(pdf,600,yPosition-84);
                CGContextStrokePath(pdf);
                /* Line after  rental income*/
                
                
                
                
                /*Ends : Drawing box*/
                
                
                
                
                
                CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 14);
                
                writeingContextToPDF(pdf,56,yPosition-30,[NSString stringWithFormat:@"Property managed by: %@",[agent_information objectForKey:@"propertyAgent"]]);
                
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                
                temp_string = [NSString stringWithFormat:@"Property managed by: %@",[agent_information objectForKey:@"propertyAgent"]];
                text_size = [self calculate_size:temp_string font_size:16.0];
                
                
                
                
                
                
                CGContextMoveToPoint(pdf,56,yPosition-40);
                CGContextAddLineToPoint(pdf,56+text_size.width,yPosition-40);
                CGContextStrokePath(pdf);
                
                
                /*Starts : Agent Commission */
                
                
                
                /*Starts: cell background*/
                
                CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
                CGContextFillRect(pdf, CGRectMake(52,yPosition-83,548,28));
                CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
                
                /* Ends: cell background*/
                
                
                
                
                CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 12);
                
                writeingContextToPDF(pdf,56,yPosition-73,[NSString stringWithFormat:@"Agent Commission: %@ %@",[agent_information objectForKey:@"AgentCommission"],@"%"]);
                
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 12);
                
                /*Ends : Agent Commission */
                
                
                
                
                
                
                
                
                /*Starts : Agent Address */
                
                
                writeingContextToPDF(pdf, 56, yPosition-103,[NSString stringWithFormat:@"%@",[agent_information objectForKey:@"ContactPerson"]]);
                
                
                
                NSString *agent_info = [agent_information objectForKey:@"address"];
                agent_info = [agent_info stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
                nslog(@"length == %@",agent_info);
                
                int string_length = [agent_info length];
                
                
                
                
                array_notes = [[NSMutableArray alloc] init];
                
                nslog(@"\n len = %d",string_length);
                nslog(@"\n len/50 = %d",string_length/50);
                int count = string_length/50;
                if(string_length%50!=0)
                {
                    count++;
                }
                
                int j=0;
                for(int i=0;i<count;i++)
                {
                    if([agent_info length] >=50)
                    {
                        NSRange stringRange = {0, MIN([agent_info length],50)};
                        // adjust the range to include dependent chars
                        stringRange = [agent_info rangeOfComposedCharacterSequencesForRange:stringRange];
                        // Now you can create the short string
                        NSString *shortString = [agent_info substringWithRange:stringRange];
                        //nslog(@"shortString == %@",shortString);
                        agent_info = [agent_info substringFromIndex:50];
                        //[agent_info retain];
                        [array_notes addObject:shortString];
                        j = j+50;
                    }
                    else
                    {
                        
                        NSRange stringRange = {0, MIN([agent_info length],[agent_info length])};
                        // adjust the range to include dependent chars
                        stringRange = [agent_info rangeOfComposedCharacterSequencesForRange:stringRange];
                        // Now you can create the short string
                        NSString *shortString = [agent_info substringWithRange:stringRange];
                        //nslog(@"shortString == %@",shortString);
                        agent_info = [agent_info substringFromIndex:[agent_info length]];
                        // [agent_info retain];
                        [array_notes addObject:shortString];
                        
                    }
                }
                
                nslog(@"\n array_notes = %@",array_notes);
                
                int start_y_pos = 120;
                
                if([array_notes count]==0)
                {
                    writeingContextToPDF(pdf, 56, yPosition-120,[agent_information objectForKey:@"address"]);
                }
                else
                {
                    
                    
                    for(int z=0;z<[array_notes count];z++)
                    {
                        writeingContextToPDF(pdf, 56, yPosition-start_y_pos,[array_notes objectAtIndex:z]);
                        start_y_pos+=14;
                    }
                    
                    
                }
                
                /*Ends : Agent Address */
                
                
                
                
                /*Drawing line at bottom agent box*/
                
                
                float agent_bottom_line_pos;
                
                if(yPosition-185 < yPosition-start_y_pos)
                {
                    agent_bottom_line_pos = yPosition-185;
                    
                    final_position_after_bloack = yPosition-185;
                    
                }
                else
                {
                    agent_bottom_line_pos = yPosition-(start_y_pos);
                    final_position_after_bloack =yPosition-(start_y_pos);
                }
                
                CGContextMoveToPoint(pdf,52, agent_bottom_line_pos);
                CGContextAddLineToPoint(pdf,600,agent_bottom_line_pos);
                CGContextStrokePath(pdf);
                
                /*Drawing line at bottom lease box*/
                
                
                
                /*Drawing  v line at middle lease box*/
                
                
                CGContextMoveToPoint(pdf,328, yPosition-54);
                CGContextAddLineToPoint(pdf,328,agent_bottom_line_pos);
                CGContextStrokePath(pdf);
                
                
                /*Drawing  v line at middle lease box*/
                
                
                
                /* Line left side of box*/
                CGContextMoveToPoint(pdf,52, yPosition-54);
                CGContextAddLineToPoint(pdf,52,agent_bottom_line_pos);
                CGContextStrokePath(pdf);
                /* Line left side of box*/
                
                
                /* Line right side of box*/
                CGContextMoveToPoint(pdf,600, yPosition-54);
                CGContextAddLineToPoint(pdf,600,agent_bottom_line_pos);
                CGContextStrokePath(pdf);
                /* Line right sideo of box*/
                
                
                
                
                /*Starts :  Phone */
                
                writeingContextToPDF(pdf,335,yPosition-103,[NSString stringWithFormat:@"Phone: %@",[agent_information objectForKey:@"WorkPhone"]]);
                /*Ends :  Phone */
                
                
                /*Starts: Line after phone*/
                CGContextMoveToPoint(pdf,328, yPosition-115);
                CGContextAddLineToPoint(pdf,600,yPosition-115);
                CGContextStrokePath(pdf);
                /*Ends: Line after phone*/
                
                
                /*Starts : Mobile */
                
                writeingContextToPDF(pdf, 335,yPosition-133,[NSString stringWithFormat:@"Mobile: %@",[agent_information objectForKey:@"Mobile"]]);
                
                
                /*Ends :  Mobile */
                
                
                
                /*Starts: Line after mobile*/
                CGContextMoveToPoint(pdf,328, yPosition-145);
                CGContextAddLineToPoint(pdf,600,yPosition-145);
                CGContextStrokePath(pdf);
                /*Ends: Line after mobile*/
                
                
                /*Starts : email */
                
                writeingContextToPDF(pdf, 335,yPosition-163,[NSString stringWithFormat:@"Email: %@",[agent_information objectForKey:@"Email"]]);
                /*Ends :  email */
                
                
                
                
                
                
            }
        }
        
        
        
        
        
        
        /*Ends: Agent Information*/
        
        
        CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 10);
        writeingContextToPDF(pdf,438, 18, @"Powered By");
        
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 10);
        
        CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
        CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
        writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
        
        
        
        CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        writeingContextToPDF(pdf,530,18, @"Property Log Book");
        
        CGContextEndPage(pdf);
        
        
        
        /*2nd page starts...*/
        
        CGContextBeginPage (pdf, &pageRect);
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        
        CGContextSetFontSize(pdf, 14);
        
        
        CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 14);
        
        writeingContextToPDF(pdf, 56, 800,@"Income Summary");
        
        temp_string = @"Income Summary";
        text_size = [self calculate_size:temp_string font_size:15.0];
        
        
        
        CGContextMoveToPoint(pdf,56,790);
        CGContextAddLineToPoint(pdf,56+text_size.width,790);
        CGContextStrokePath(pdf);
        
        
        
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        
        CGContextSetFontSize(pdf, 12);
        
        
        /*
         NSString*propertyName;// = [[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"];
         propertyName  = [saveFile_Name stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
         */
        
        
        
        /*
         [appDelegate selectIncomeExpenseByProperty:[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"]];
         */
        
        [appDelegate selectIncomeExpenseByPropertyForRange:[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"] startDate:[appDelegate fetchDateInCommonFormate:[fromString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]] endDate:[appDelegate fetchDateInCommonFormate:[toString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]];
        
        
        
        
        
        
        
        
        df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd"];
        
        
        /*
         NSDate *fromDate = [df dateFromString:[fromString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
         
         NSDate *toDate = [df dateFromString:[toString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
         
         */
        
        NSDate*fromDate = [appDelegate.regionDateFormatter dateFromString:[fromString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        
        
        NSDate*toDate = [appDelegate.regionDateFormatter dateFromString:[toString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        
        
        nslog(@"\n fromDate in pdf= %@",fromDate);
        nslog(@"\n toDate in pdf = %@",toDate);
        
        for (int i =0; i<[appDelegate.incomeExpenseSummery count];i++)
        {
            int from;
            from = [fromDate compare:[[appDelegate.incomeExpenseSummery objectAtIndex:i]valueForKey:@"date"]];
            
            int to ;
            
            if ([toDate isEqualToDate:[[appDelegate.incomeExpenseSummery objectAtIndex:i]valueForKey:@"date"]])
            {
                to = 1;
            }
            else
            {
                to = [toDate compare:[[appDelegate.incomeExpenseSummery objectAtIndex:i]valueForKey:@"date"]];
            }
            if (from <= 0  && to > 0)
            {
                if ([[[appDelegate.incomeExpenseSummery objectAtIndex:i]valueForKey:@"incomeExpense"]intValue] == 1)
                {
                    
                    [incomeTypeSummaryArray addObject:[appDelegate.incomeExpenseSummery objectAtIndex:i]];
                }
                else if([[[appDelegate.incomeExpenseSummery objectAtIndex:i]valueForKey:@"incomeExpense"]intValue] == 0)
                {
                    
                    [expenseTypeSummaryArray addObject:[appDelegate.incomeExpenseSummery objectAtIndex:i]];
                }
                else/*
                     SUN:004
                     MILAGE....
                     */
                {
                    [milageTypeSummaryArray addObject:[appDelegate.incomeExpenseSummery objectAtIndex:i]];
                    
                }
            }
        }
        
        
        
        tempIncomeArray = [[NSMutableArray alloc]init];
        incomeSummeryDictionary = [[NSMutableDictionary alloc]init];
        
        /*Start: Income Array images..*/
        
        income_types_dictionary = [[NSMutableDictionary alloc] init] ;
        
        
        NSMutableArray*income_type_summary_images_array;// = [[NSMutableArray alloc] init];
        NSString*keyString;
        for (int i =0;i<[incomeTypeSummaryArray count];i++)
        {
            
            
            income_type_summary_images_array = [[NSMutableArray alloc] init];
            keyString = [[incomeTypeSummaryArray objectAtIndex:i]valueForKey:@"type"];
            
            for (int j =0;j<[incomeTypeSummaryArray count];j++)
            {
                
                
                if ([keyString isEqualToString:[[incomeTypeSummaryArray objectAtIndex:j]valueForKey:@"type"]])
                {
                    [income_type_summary_images_array addObject: [[incomeTypeSummaryArray objectAtIndex:j] objectForKey:@"receipt_small_image_name"] ];
                }
            }
            
            [income_types_dictionary setObject:income_type_summary_images_array forKey:keyString];
            //[income_type_summary_images_array release];
        }
        
        
        
        
        nslog(@"\n 1.income_types_dictionary == %@",income_types_dictionary);
        
        /*Ends:Income Array images.. */
        
        
        
        
        /*Start: expense Array images..*/
        
        NSMutableDictionary*expense_types_dictionary = [[NSMutableDictionary alloc] init] ;
        
        
        NSMutableArray*expense_type_summary_images_array;// = [[NSMutableArray alloc] init];
        //NSString*keyString;
        
        
        
        for (int i =0;i<[expenseTypeSummaryArray count];i++)
        {
            
            
            expense_type_summary_images_array = [[NSMutableArray alloc] init];
            keyString = [[expenseTypeSummaryArray objectAtIndex:i]valueForKey:@"type"];
            
            for (int j =0;j<[expenseTypeSummaryArray count];j++)
            {
                
                
                if ([keyString isEqualToString:[[expenseTypeSummaryArray objectAtIndex:j]valueForKey:@"type"]])
                {
                    [expense_type_summary_images_array addObject: [[expenseTypeSummaryArray objectAtIndex:j] objectForKey:@"receipt_small_image_name"] ];
                }
            }
            
            [expense_types_dictionary setObject:expense_type_summary_images_array forKey:keyString];
            
            //[expense_type_summary_images_array release];
        }
        
        nslog(@"\n 1.expense_types_dictionary == %@",expense_types_dictionary);
        
        /*Ends: expense Array receipt_images.. */
        
        
        
        /*Start: milage Array images..*/
        
        NSMutableDictionary*milage_types_dictionary = [[NSMutableDictionary alloc] init] ;
        NSMutableArray*milage_type_summary_images_array;// = [[NSMutableArray alloc] init];
        //NSString*keyString;
        
        
        
        for (int i =0;i<[milageTypeSummaryArray count];i++)
        {
            
            
            milage_type_summary_images_array = [[NSMutableArray alloc] init];
            keyString = [[milageTypeSummaryArray objectAtIndex:i]valueForKey:@"type"];
            
            for (int j =0;j<[milageTypeSummaryArray count];j++)
            {
                if ([keyString isEqualToString:[[milageTypeSummaryArray objectAtIndex:j]valueForKey:@"type"]])
                {
                    [milage_type_summary_images_array addObject: [[milageTypeSummaryArray objectAtIndex:j] objectForKey:@"receipt_small_image_name"] ];
                }
            }
            
            [milage_types_dictionary setObject:milage_type_summary_images_array forKey:keyString];
            // [milage_type_summary_images_array release];
            
            //nslog(@"\n 1.milage_type_summary_images_array == %@",milage_type_summary_images_array);
        }
        
        
        
        /*Ends: milage Array receipt_images.. */
        
        
        
        
        
        for (int i =0;i<[incomeTypeSummaryArray count];i++)
        {
            float amount = 0.0;
            
            
            
            NSString *str  = [[incomeTypeSummaryArray objectAtIndex:i]valueForKey:@"type"];
            
            NSArray *arr = [incomeSummeryDictionary allKeys];
            int count =0 ;
            for (int i =0; i<[arr count];i++)
            {
                if ([str isEqualToString:[arr objectAtIndex:i]])
                {
                    count ++;
                }
            }
            
            
            if (count == 0)
            {
                for (int j =0;j<[incomeTypeSummaryArray count];j++)
                {
                    if ([str isEqualToString:[[incomeTypeSummaryArray objectAtIndex:j]valueForKey:@"type"]])
                    {
                        amount = amount + [[[incomeTypeSummaryArray objectAtIndex:j]valueForKey:@"amount"]floatValue];
                    }
                    
                    
                }
                
                
                totalIncomeAmount = totalIncomeAmount + amount;
                [incomeSummeryDictionary setObject:[NSNumber numberWithFloat:amount] forKey:str];
                
                
                
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                [dict setObject:[NSString stringWithFormat:@"%.2f",amount] forKey:@"amount"];
                [dict setObject:str forKey:@"type"];
                [tempIncomeArray addObject:dict];
                //[dict release];
            }
        }
        
        
        
        
        
        
        
        
        
        tempExpenseArray = [[NSMutableArray alloc]init];
        
        nslog(@"\n expenseTypeSummaryArray = %@",expenseTypeSummaryArray);
        expenseSummeryDictionary = [[NSMutableDictionary alloc]init];
        for (int i =0;i<[expenseTypeSummaryArray count];i++)
        {
            float amount = 0.0;
            
            NSString *str  = [[expenseTypeSummaryArray objectAtIndex:i]valueForKey:@"type"];
            
            NSArray *arr = [expenseSummeryDictionary allKeys];
            nslog(@"\n arr = %@",arr);
            int count =0 ;
            for (int i =0; i<[arr count];i++)
            {
                if ([str isEqualToString:[arr objectAtIndex:i]])
                {
                    count ++;
                }
            }
            if (count == 0)
            {
                for (int j =0;j<[expenseTypeSummaryArray count];j++)
                {
                    if ([str isEqualToString:[[expenseTypeSummaryArray objectAtIndex:j]valueForKey:@"type"]])
                    {
                        amount = amount + [[[expenseTypeSummaryArray objectAtIndex:j]valueForKey:@"amount"]floatValue];
                    }
                }
                totalExpenseAmount = totalExpenseAmount + amount;
                
                
                [expenseSummeryDictionary setObject:[NSNumber numberWithFloat:amount] forKey:str];
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                [dict setObject:[NSString stringWithFormat:@"%.2f",amount] forKey:@"amount"];
                [dict setObject:str forKey:@"type"];
                [tempExpenseArray addObject:dict];
                //[dict release];
            }
        }
        
        
        
        
        tempMilageArray = [[NSMutableArray alloc]init];
        milageSummeryDictionary = [[NSMutableDictionary alloc]init];
        
        
        for (int i =0;i<[milageTypeSummaryArray count];i++)
        {
            float amount = 0.0;
            
            NSString *str  = [[milageTypeSummaryArray objectAtIndex:i]valueForKey:@"type"];
            
            NSArray *arr = [milageSummeryDictionary allKeys];
            nslog(@"\n arr = %@",arr);
            int count =0 ;
            for (int i =0; i<[arr count];i++)
            {
                if ([str isEqualToString:[arr objectAtIndex:i]])
                {
                    count ++;
                }
            }
            if (count == 0)
            {
                for (int j =0;j<[milageTypeSummaryArray count];j++)
                {
                    if ([str isEqualToString:[[milageTypeSummaryArray objectAtIndex:j]valueForKey:@"type"]])
                    {
                        amount = amount + [[[milageTypeSummaryArray objectAtIndex:j]valueForKey:@"amount"]floatValue];
                    }
                }
                totalMilage = totalMilage + amount;
                
                
                [milageSummeryDictionary setObject:[NSNumber numberWithFloat:amount] forKey:str];
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                [dict setObject:[NSString stringWithFormat:@"%.2f",amount] forKey:@"amount"];
                [dict setObject:str forKey:@"type"];
                [tempMilageArray addObject:dict];
                //[dict release];
            }
        }
        
        
        
        
        
        
        
        nslog(@"\n tempIncomeArray = %@",tempIncomeArray);
        
        nslog(@"\n 2.income_types_dictionary == %@",income_types_dictionary);
        
        //incomeY = 720;
        incomeY = 745;
        
        
        /*Top line income sumary box*/
        CGContextMoveToPoint(pdf, 56, 775);
        CGContextAddLineToPoint(pdf, 600, 775);
        CGContextStrokePath(pdf);
        // CGContextAddLineToPoint(pdf, 56, 586);
        
        
        /*Starts: cell background*/
        
        CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
        CGContextFillRect(pdf, CGRectMake(56,incomeY,544,30));
        CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
        
        /* Ends: cell background*/
        
        
        
        CGContextSelectFont(pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        writeingContextToPDF(pdf, 60, incomeY +10,@"Income Type");
        writeingContextToPDF(pdf, 365, incomeY +10,@"%");
        writeingContextToPDF(pdf, 465, incomeY +10,@"Amount");
        
        CGContextSelectFont(pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        CGContextMoveToPoint(pdf, 56, incomeY);
        CGContextAddLineToPoint(pdf, 600, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 56, incomeY );
        CGContextAddLineToPoint(pdf, 56, incomeY+30);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 600, incomeY);
        CGContextAddLineToPoint(pdf, 600, incomeY+30);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 360, incomeY);
        CGContextAddLineToPoint(pdf, 360, incomeY+30);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 460, incomeY);
        CGContextAddLineToPoint(pdf, 460, incomeY+30);
        CGContextStrokePath(pdf);
        
        
        
        incomeY -=30;
        
        if ([tempIncomeArray count]>0)
        {
            for (int i =0;i<[tempIncomeArray count];i++)
            {
                if (incomeY < 50)
                {
                    //incomeY = 750;
                    CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    writeingContextToPDF(pdf, 438, 18, @"Powered By");
                    
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    
                    CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                    
                    CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                    writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                    
                    CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 12);
                    
                    writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                    CGContextEndPage(pdf);
                    
                    /*Third page starts....*/
                    
                    CGContextBeginPage (pdf, &pageRect);
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 14);
                    
                    /*
                     writeingContextToPDF(pdf, 56, 800,@"Income Summary");
                     CGContextSetFontSize(pdf, 12);
                     
                     CGContextMoveToPoint(pdf, 56, 780);
                     CGContextAddLineToPoint(pdf, 600, 780);
                     CGContextStrokePath(pdf);
                     */
                    // CGContextAddLineToPoint(pdf, 56, 586);
                    
                    incomeY = 750;
                    
                }
                
                writeingContextToPDF(pdf, 60, incomeY +10,[[tempIncomeArray objectAtIndex:i]valueForKey:@"type"]);
                
                
                
                if(len == 1)
                {
                    writeingContextToPDF(pdf, 480, incomeY +10,[[tempIncomeArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else if(len == 2)
                {
                    writeingContextToPDF(pdf, 490, incomeY +10,[[tempIncomeArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else if(len == 3)
                {
                    writeingContextToPDF(pdf, 500, incomeY +10,[[tempIncomeArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else if(len == 4)
                {
                    writeingContextToPDF(pdf, 510, incomeY +10,[[tempIncomeArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                
                //CGContextShowGlyphsAtPoint(pdf,465,incomeY +10,&glyph,cfIndex);
                
                CGContextDrawImage(pdf,CGRectMake(465,incomeY-39,60,60),currencySymbolImage.CGImage);//Drawing Image...
                float amt = ([[[tempIncomeArray objectAtIndex:i]valueForKey:@"amount"] floatValue]*100)/totalIncomeAmount;
                
                NSString *per = [NSString stringWithFormat:@"%.2f",amt];
                per = [per stringByAppendingFormat:@"%@",@"%"];
                
                writeingContextToPDF(pdf, 365, incomeY +10,[NSString stringWithFormat:@"%@",per]);
                
                
                CGContextMoveToPoint(pdf, 56, incomeY);
                CGContextAddLineToPoint(pdf, 600, incomeY);
                CGContextStrokePath(pdf);
                
                CGContextMoveToPoint(pdf, 56, incomeY );
                CGContextAddLineToPoint(pdf, 56, incomeY+30);
                CGContextStrokePath(pdf);
                
                CGContextMoveToPoint(pdf, 600, incomeY);
                CGContextAddLineToPoint(pdf, 600, incomeY+30);
                CGContextStrokePath(pdf);
                
                CGContextMoveToPoint(pdf, 360, incomeY);
                CGContextAddLineToPoint(pdf, 360, incomeY+30);
                CGContextStrokePath(pdf);
                
                CGContextMoveToPoint(pdf, 460, incomeY);
                CGContextAddLineToPoint(pdf, 460, incomeY+30);
                CGContextStrokePath(pdf);
                
                incomeY = incomeY-30;
                
                
                
                
            }
            
            
            
            /*Starts: cell background*/
            
            CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
            CGContextFillRect(pdf, CGRectMake(56,incomeY,544,30));
            CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
            
            /* Ends: cell background*/
            
            
            CGContextMoveToPoint(pdf, 56, incomeY+30 );
            // CGContextAddLineToPoint(pdf, 56, incomeY-30);
            CGContextAddLineToPoint(pdf, 56, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 600, incomeY+30);
            //CGContextAddLineToPoint(pdf, 600, incomeY-30);
            CGContextAddLineToPoint(pdf, 600, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 460, incomeY+30);
            // CGContextAddLineToPoint(pdf, 460, incomeY-30);
            CGContextAddLineToPoint(pdf, 460, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 360, incomeY+30);
            //CGContextAddLineToPoint(pdf, 360, incomeY-30);
            CGContextAddLineToPoint(pdf, 360, incomeY);
            CGContextStrokePath(pdf);
            
            /*Old,big box.
             writeingContextToPDF(pdf, 56, incomeY-20,@"Total Income");
             writeingContextToPDF(pdf, 365, incomeY-20, @"100 %");
             */
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            writeingContextToPDF(pdf, 60, incomeY +10,@"Total Income");
            
            
            
            writeingContextToPDF(pdf, 365, incomeY +10, @"100 %");
            
            /*
             if(len == 1)
             {
             writeingContextToPDF(pdf, 479, incomeY-20, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
             }
             else if(len == 2)
             {
             writeingContextToPDF(pdf, 489, incomeY-20, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
             }
             else if(len == 3)
             {
             writeingContextToPDF(pdf, 499, incomeY-20, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
             }
             else if(len == 4)
             {
             writeingContextToPDF(pdf, 519, incomeY-20, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
             }
             
             //CGContextShowGlyphsAtPoint(pdf,465,incomeY-20,&glyph,cfIndex);
             
             CGContextDrawImage(pdf,CGRectMake(465,incomeY-69,60,60),currencySymbolImage.CGImage);//Drawing Image...
             */
            
            
            if(len == 1)
            {
                writeingContextToPDF(pdf, 479, incomeY +10, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
            }
            else if(len == 2)
            {
                
                writeingContextToPDF(pdf, 489, incomeY +10, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
            }
            else if(len == 3)
            {
                writeingContextToPDF(pdf, 499, incomeY +10, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
            }
            else if(len == 4)
            {
                writeingContextToPDF(pdf, 519, incomeY +10, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
            }
            
            //CGContextShowGlyphsAtPoint(pdf,465,incomeY-20,&glyph,cfIndex);
            
            CGContextDrawImage(pdf,CGRectMake(465,incomeY-39,60,60),currencySymbolImage.CGImage);//Drawing Image...
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            /*Draws bottom line of box
             CGContextMoveToPoint(pdf, 56, incomeY-30);
             CGContextAddLineToPoint(pdf, 600, incomeY-30);
             CGContextStrokePath(pdf);
             */
            
            CGContextMoveToPoint(pdf, 56, incomeY);
            CGContextAddLineToPoint(pdf, 600, incomeY);
            CGContextStrokePath(pdf);
            
            incomeY = incomeY-30;
            
            nslog(@"\n incomeY = %d",incomeY);
            
            if(incomeY < 300)
            {
                
                
                CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 10);
                writeingContextToPDF(pdf, 438, 18, @"Powered By");
                
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 10);
                
                CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                
                
                CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 12);
                
                writeingContextToPDF(pdf,530,18, @"Property Log Book");
                CGContextEndPage(pdf);
                
                
                
                CGContextBeginPage (pdf, &pageRect);
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 14);
                
                
                
                incomeY = 800;
                
                
                
            }
            
            
            
            
            float sum=totalIncomeAmount;
            float mult=(360/sum);
            
            float startDeg=0;
            float endDeg=0;
            
            for(int i=0;i<[tempIncomeArray count];i++)
            {
                startDeg=endDeg;
                endDeg=endDeg+([[[tempIncomeArray objectAtIndex:i] objectForKey:@"amount"] floatValue] * mult);
                if(startDeg != endDeg)
                {
                    
                    CGContextSetRGBFillColor(pdf, (colorLookupTable[i % 10][0] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][1] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][2] + (CGFloat)(i / 10) * 0.1), 1.0);
                    
                    CGContextMoveToPoint(pdf, 300, incomeY-100);
                    CGContextAddArc(pdf, 300, incomeY-100, 100, (startDeg)*M_PI/180.0, (endDeg)*M_PI/180.0, 0);
                    CGContextClosePath(pdf);
                    CGContextFillPath(pdf);
                }
            }
            CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
            incomeY = incomeY -100;
            
            
            
            for(int i=0;i<[tempIncomeArray count];i++)
            {
                //int count = i/2;
                if(incomeY < 300)
                {
                    
                    
                    //incomeY = 750;
                    CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    writeingContextToPDF(pdf, 438, 18, @"Powered By");
                    
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    
                    CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                    CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                    writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                    
                    CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 12);
                    
                    writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                    CGContextEndPage(pdf);
                    
                    
                    
                    CGContextBeginPage (pdf, &pageRect);
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 14);
                    
                    
                    incomeY = 800;
                    
                    
                    
                    
                }
                
                
                float amt = ([[[tempIncomeArray objectAtIndex:i]valueForKey:@"amount"] floatValue]*100)/totalIncomeAmount;
                NSString *per = [NSString stringWithFormat:@"%.2f %@",amt,@"%"];
                
                if(i%3==0)
                {
                    CGContextSetFontSize(pdf, 12);
                    writeingContextToPDF(pdf, 40, incomeY-150,[NSString stringWithFormat:@"%@",[[tempIncomeArray objectAtIndex:i] objectForKey:@"type"]]);
                    CGContextSetRGBFillColor(pdf, (colorLookupTable[i % 10][0] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][1] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][2] + (CGFloat)(i / 10) * 0.1), 1.0);
                    
                    // CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
                    CGContextFillRect(pdf, CGRectMake(10, incomeY-155, 20, 20));
                    CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
                    
                    CGContextSetFontSize(pdf, 12);
                    
                    
                    
                    writeingContextToPDF(pdf, 170, incomeY-150,per);
                    
                }
                else if(i%3==1)
                {
                    CGContextSetFontSize(pdf, 12);
                    writeingContextToPDF(pdf, 259, incomeY-150,[NSString stringWithFormat:@"%@",[[tempIncomeArray objectAtIndex:i] objectForKey:@"type"]]);
                    CGContextSetRGBFillColor(pdf, (colorLookupTable[i % 10][0] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][1] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][2] + (CGFloat)(i / 10) * 0.1), 1.0);
                    
                    // CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
                    CGContextFillRect(pdf, CGRectMake(229, incomeY-155, 20, 20));
                    CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
                    
                    CGContextSetFontSize(pdf, 12);
                    writeingContextToPDF(pdf, 389, incomeY-150,per);
                    
                }
                else
                {
                    CGContextSetFontSize(pdf, 12);
                    writeingContextToPDF(pdf, 479, incomeY-150,[NSString stringWithFormat:@"%@",[[tempIncomeArray objectAtIndex:i] objectForKey:@"type"]]);
                    CGContextSetRGBFillColor(pdf, (colorLookupTable[i % 10][0] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][1] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][2] + (CGFloat)(i / 10) * 0.1), 1.0);
                    
                    // CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
                    CGContextFillRect(pdf, CGRectMake(449, incomeY-155, 20, 20));
                    CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
                    
                    CGContextSetFontSize(pdf, 12);
                    writeingContextToPDF(pdf, 589, incomeY-150,per);
                    incomeY = incomeY - 30;
                    
                }
                
                
            }
            
            
            
            
            incomeY = incomeY -300;
            
            
            
        }
        
        else
        {
            writeingContextToPDF(pdf, 56, incomeY,@"No Income Type Found");
            
            /*
             CGContextMoveToPoint(pdf, 56, incomeY-20);
             CGContextAddLineToPoint(pdf, 600, incomeY-20);
             CGContextStrokePath(pdf);
             */
            
            incomeY = incomeY-30;
            
        }
        
        /*
         
         Starts: Expense Summary
         
         */
        
        
        
        //incomeY = 750;
        CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 10);
        writeingContextToPDF(pdf, 438, 18, @"Powered By");
        
        
        /*Writing URL*/
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 10);
        
        
        CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
        writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
        
        CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
        CGContextEndPage(pdf);
        
        
        
        CGContextBeginPage (pdf, &pageRect);
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 14);
        
        /*
         writeingContextToPDF(pdf, 56, 800,@"Expense Summary");
         
         CGContextSetFontSize(pdf, 14);
         
         CGContextMoveToPoint(pdf, 56, 780);
         CGContextAddLineToPoint(pdf, 600, 780);
         CGContextStrokePath(pdf);
         // CGContextAddLineToPoint(pdf, 56, 586);
         */
        
        
        incomeY = 800;
        
        
        
        
        
        
        
        
        CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 14);
        writeingContextToPDF(pdf, 56,incomeY,@"Expense Summary");
        
        temp_string = @"Expense Summary";
        text_size = [self calculate_size:temp_string font_size:15.0];
        CGContextMoveToPoint(pdf,56,790);
        CGContextAddLineToPoint(pdf,56+text_size.width,incomeY-10);
        CGContextStrokePath(pdf);
        
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        nslog(@"\n incomeY = %d",incomeY);
        
        expenseY = incomeY = incomeY-80;
        tempY =incomeY;
        
        //incomeY = 720;
        incomeY = 745;
        
        
        /*Starts: cell background*/
        
        CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
        CGContextFillRect(pdf, CGRectMake(56,incomeY,544,30));
        CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
        
        /* Ends: cell background*/
        
        
        
        CGContextMoveToPoint(pdf, 56, 775);
        CGContextAddLineToPoint(pdf, 600, 775);
        CGContextStrokePath(pdf);
        
        
        
        
        CGContextSelectFont(pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        writeingContextToPDF(pdf, 60, incomeY +10,@"Expense Type");
        writeingContextToPDF(pdf, 365, incomeY +10,@"%");
        writeingContextToPDF(pdf, 465, incomeY +10,@"Amount");
        
        
        CGContextSelectFont(pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        CGContextMoveToPoint(pdf, 56, incomeY);
        CGContextAddLineToPoint(pdf, 600, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 56, incomeY );
        CGContextAddLineToPoint(pdf, 56, incomeY+30);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 600, incomeY);
        CGContextAddLineToPoint(pdf, 600, incomeY+30);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 360, incomeY);
        CGContextAddLineToPoint(pdf, 360, incomeY+30);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 460, incomeY);
        CGContextAddLineToPoint(pdf, 460, incomeY+30);
        CGContextStrokePath(pdf);
        
        
        
        incomeY -=30;
        
        
        
        nslog(@"tempExpenseArray == %@",tempExpenseArray);
        if ([tempExpenseArray count]>0)
        {
            
            for (int i =0;i<[tempExpenseArray count];i++)
            {
                if (incomeY < 90)
                {
                    //incomeY = 750;
                    CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    writeingContextToPDF(pdf, 438, 18, @"Powered By");
                    
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    
                    CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                    CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                    writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                    
                    
                    CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 12);
                    
                    writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                    CGContextEndPage(pdf);
                    
                    
                    
                    CGContextBeginPage (pdf, &pageRect);
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 14);
                    
                    /*
                     writeingContextToPDF(pdf, 56, 800,@"Expense Summary");
                     
                     CGContextSetFontSize(pdf, 14);
                     
                     CGContextMoveToPoint(pdf, 56, 780);
                     CGContextAddLineToPoint(pdf, 600, 780);
                     CGContextStrokePath(pdf);
                     // CGContextAddLineToPoint(pdf, 56, 586);
                     */
                    
                    incomeY = 750;
                    
                }
                writeingContextToPDF(pdf, 60, incomeY+10,[[tempExpenseArray objectAtIndex:i]valueForKey:@"type"]);
                
                
                if(len == 1)
                {
                    writeingContextToPDF(pdf, 480, incomeY+10,[[tempExpenseArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else if(len == 2)
                {
                    writeingContextToPDF(pdf, 490, incomeY+10,[[tempExpenseArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else if(len == 3)
                {
                    writeingContextToPDF(pdf, 500, incomeY+10,[[tempExpenseArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else if(len == 4)
                {
                    writeingContextToPDF(pdf, 510, incomeY+10,[[tempExpenseArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                
                
                
                
                
                // CGContextShowGlyphsAtPoint(pdf,465,incomeY+10,&glyph,cfIndex);
                CGContextDrawImage(pdf,CGRectMake(465,incomeY-39,60,60),currencySymbolImage.CGImage);//Drawing Image...
                
                
                float amt = ([[[tempExpenseArray objectAtIndex:i]valueForKey:@"amount"] floatValue]*100)/totalExpenseAmount;
                
                NSString *per = [NSString stringWithFormat:@"%.2f",amt];
                writeingContextToPDF(pdf, 365, incomeY +10,[NSString stringWithFormat:@"%@ %@",per,@"%"]);
                
                CGContextMoveToPoint(pdf, 56, incomeY);
                CGContextAddLineToPoint(pdf, 600, incomeY);
                CGContextStrokePath(pdf);
                
                CGContextMoveToPoint(pdf, 56, incomeY );
                CGContextAddLineToPoint(pdf, 56, incomeY+30);
                CGContextStrokePath(pdf);
                
                CGContextMoveToPoint(pdf, 600, incomeY);
                CGContextAddLineToPoint(pdf, 600, incomeY+30);
                CGContextStrokePath(pdf);
                
                CGContextMoveToPoint(pdf, 360, incomeY);
                CGContextAddLineToPoint(pdf, 360, incomeY+30);
                CGContextStrokePath(pdf);
                CGContextMoveToPoint(pdf, 460, incomeY);
                CGContextAddLineToPoint(pdf, 460, incomeY+30);
                CGContextStrokePath(pdf);
                incomeY = incomeY-30;
            }
            
            
            
            
            /*Starts: cell background for total expense*/
            
            CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
            CGContextFillRect(pdf, CGRectMake(56,incomeY,544,30));
            CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
            
            /* Ends: cell background  total expense */
            
            
            CGContextMoveToPoint(pdf, 56, incomeY+30 );
            //CGContextAddLineToPoint(pdf, 56, incomeY-30);
            CGContextAddLineToPoint(pdf, 56, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 600, incomeY+30);
            //CGContextAddLineToPoint(pdf, 600, incomeY-30);
            CGContextAddLineToPoint(pdf, 600, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 360, incomeY+30);
            //CGContextAddLineToPoint(pdf, 360, incomeY-30);
            CGContextAddLineToPoint(pdf, 360, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 460, incomeY+30);
            //CGContextAddLineToPoint(pdf, 460, incomeY-30);
            CGContextAddLineToPoint(pdf, 460, incomeY);
            CGContextStrokePath(pdf);
            
            
            /*OLD
             writeingContextToPDF(pdf, 56, incomeY-20,@"Total Expense");
             writeingContextToPDF(pdf, 365, incomeY-20, @"100 %");
             */
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            writeingContextToPDF(pdf, 60, incomeY+10,@"Total Expense");
            writeingContextToPDF(pdf, 365, incomeY+10, @"100 %");
            
            
            /*
             if(len == 1)
             {
             writeingContextToPDF(pdf, 480, incomeY-20, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
             }
             else if(len == 2)
             {
             writeingContextToPDF(pdf, 490, incomeY-20, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
             }
             else if(len == 3)
             {
             writeingContextToPDF(pdf, 500, incomeY-19, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
             }
             else if(len == 4)
             {
             writeingContextToPDF(pdf, 510, incomeY-19, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
             }
             */
            
            
            if(len == 1)
            {
                writeingContextToPDF(pdf, 480, incomeY+10, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
            }
            else if(len == 2)
            {
                writeingContextToPDF(pdf, 490, incomeY+10, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
            }
            else if(len == 3)
            {
                writeingContextToPDF(pdf, 500, incomeY+10, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
            }
            else if(len == 4)
            {
                writeingContextToPDF(pdf, 510, incomeY+10, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
            }
            
            
            
            
            //CGContextShowGlyphsAtPoint(pdf,465,incomeY-20,&glyph,cfIndex);
            CGContextDrawImage(pdf,CGRectMake(465,incomeY-39,60,60),currencySymbolImage.CGImage);//Drawing Image...
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            /*Bottom Box
             CGContextMoveToPoint(pdf, 56, incomeY-30);
             CGContextAddLineToPoint(pdf, 600, incomeY-30);
             CGContextStrokePath(pdf);
             */
            
            CGContextMoveToPoint(pdf, 56, incomeY);
            CGContextAddLineToPoint(pdf, 600, incomeY);
            CGContextStrokePath(pdf);
            
            
            
            incomeY = incomeY-30;
            
            
            if(incomeY < 300)
            {
                
                
                CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 10);
                writeingContextToPDF(pdf, 438, 18, @"Powered By");
                
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 10);
                
                CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                
                
                CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 12);
                
                writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                CGContextEndPage(pdf);
                
                
                
                CGContextBeginPage (pdf, &pageRect);
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 14);
                
                /*
                 writeingContextToPDF(pdf, 56, 800,@"Expense Summary");
                 
                 CGContextSetFontSize(pdf, 14);
                 
                 CGContextMoveToPoint(pdf, 56, 780);
                 CGContextAddLineToPoint(pdf, 600, 780);
                 CGContextStrokePath(pdf);
                 // CGContextAddLineToPoint(pdf, 56, 586);
                 */
                
                incomeY = 800;
                
                
                
            }
            
            float sum=totalExpenseAmount;
            float mult=(360/sum);
            
            float startDeg=0;
            float endDeg=0;
            
            for(int i=0;i<[tempExpenseArray count];i++)
            {
                startDeg=endDeg;
                endDeg=endDeg+([[[tempExpenseArray objectAtIndex:i] objectForKey:@"amount"] floatValue] * mult);
                if(startDeg != endDeg)
                {
                    
                    CGContextSetRGBFillColor(pdf, (colorLookupTable[i % 10][0] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][1] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][2] + (CGFloat)(i / 10) * 0.1), 1.0);
                    
                    CGContextMoveToPoint(pdf, 300, incomeY-100);
                    CGContextAddArc(pdf, 300, incomeY-100, 100, (startDeg)*M_PI/180.0, (endDeg)*M_PI/180.0, 0);
                    CGContextClosePath(pdf);
                    CGContextFillPath(pdf);
                }
            }
            CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
            
            incomeY = incomeY -99;
            
            
            for(int i=0;i<[tempExpenseArray count];i++)
            {
                //int count = i/2;
                if(incomeY < 300)
                {
                    
                    
                    //incomeY = 750;
                    CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    writeingContextToPDF(pdf, 438, 18, @"Powered By");
                    
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    
                    CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                    CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                    writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                    
                    CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 12);
                    
                    writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                    CGContextEndPage(pdf);
                    
                    
                    
                    CGContextBeginPage (pdf, &pageRect);
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 14);
                    
                    
                    incomeY = 800;
                    
                    
                    
                    
                }
                
                
                float amt = ([[[tempExpenseArray objectAtIndex:i]valueForKey:@"amount"] floatValue]*100)/totalExpenseAmount;
                NSString *per = [NSString stringWithFormat:@"%.2f %@",amt,@"%"];
                
                if(i%3==0)
                {
                    CGContextSetFontSize(pdf, 12);
                    writeingContextToPDF(pdf, 40, incomeY-150,[NSString stringWithFormat:@"%@",[[tempExpenseArray objectAtIndex:i] objectForKey:@"type"]]);
                    CGContextSetRGBFillColor(pdf, (colorLookupTable[i % 10][0] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][1] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][2] + (CGFloat)(i / 10) * 0.1), 1.0);
                    
                    // CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
                    CGContextFillRect(pdf, CGRectMake(10, incomeY-155, 20, 20));
                    CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
                    
                    CGContextSetFontSize(pdf, 12);
                    
                    
                    
                    writeingContextToPDF(pdf, 170, incomeY-150,per);
                    
                }
                else if(i%3==1)
                {
                    CGContextSetFontSize(pdf, 12);
                    writeingContextToPDF(pdf, 259, incomeY-150,[NSString stringWithFormat:@"%@",[[tempExpenseArray objectAtIndex:i] objectForKey:@"type"]]);
                    CGContextSetRGBFillColor(pdf, (colorLookupTable[i % 10][0] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][1] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][2] + (CGFloat)(i / 10) * 0.1), 1.0);
                    
                    // CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
                    CGContextFillRect(pdf, CGRectMake(229, incomeY-155, 20, 20));
                    CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
                    
                    CGContextSetFontSize(pdf, 12);
                    writeingContextToPDF(pdf, 389, incomeY-150,per);
                    
                }
                else
                {
                    CGContextSetFontSize(pdf, 12);
                    writeingContextToPDF(pdf, 479, incomeY-150,[NSString stringWithFormat:@"%@",[[tempExpenseArray objectAtIndex:i] objectForKey:@"type"]]);
                    CGContextSetRGBFillColor(pdf, (colorLookupTable[i % 10][0] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][1] + (CGFloat)(i / 10) * 0.1), (colorLookupTable[i % 10][2] + (CGFloat)(i / 10) * 0.1), 1.0);
                    
                    // CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
                    CGContextFillRect(pdf, CGRectMake(449, incomeY-155, 20, 20));
                    CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
                    
                    CGContextSetFontSize(pdf, 12);
                    writeingContextToPDF(pdf, 589, incomeY-150,per);
                    incomeY = incomeY - 30;
                    
                }
                
                
            }
            
            
            
            
            
        }
        else
        {
            writeingContextToPDF(pdf, 56, incomeY,@"No Expense Type Found");
            
            /*
             CGContextMoveToPoint(pdf, 56, incomeY-20);
             CGContextAddLineToPoint(pdf, 600, incomeY-20);
             CGContextStrokePath(pdf);
             */
            incomeY = incomeY-50;
        }
        
        
        
        CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 10);
        writeingContextToPDF(pdf, 438, 18, @"Powered By");
        
        //CFURLRef url = (CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
        
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 10);
        
        CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
        writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
        
        CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
        CGContextEndPage(pdf);
        
        CGContextBeginPage (pdf, &pageRect);
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        //    	writeingContextToPDF(pdf,50 , 940,@"Property Detail");
        //    CGContextSetFontSize(pdf, 26);
        
        
        
        CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 14);
        
        writeingContextToPDF(pdf, 56, 800,@"Income Transactions");
        CGContextSetFontSize(pdf, 14);
        
        temp_string = @"Income Transactions";
        text_size = [self calculate_size:temp_string font_size:15.0];
        
        
        CGContextMoveToPoint(pdf,56,790);
        CGContextAddLineToPoint(pdf,56+text_size.width,790);
        CGContextStrokePath(pdf);
        
        
        
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        
        CGContextSetFontSize(pdf, 12);
        
        
        
        // incomeY = 720;
        incomeY = 745;
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        [incomeTypeSummaryArray sortUsingDescriptors:[NSArray arrayWithObject:dateDescriptor]];
        
        [expenseTypeSummaryArray sortUsingDescriptors:[NSArray arrayWithObject:dateDescriptor]];
        // [dateDescriptor release];
        
        
        //Transactions...
        
        
        /*Starts: cell background for income transaction header*/
        
        CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
        CGContextFillRect(pdf, CGRectMake(56,incomeY,544,30));
        CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
        
        /* Ends: cell background income transaction header*/
        
        
        
        /*Income transaction top line*/
        
        CGContextMoveToPoint(pdf, 56, incomeY+30);
        CGContextAddLineToPoint(pdf, 600, incomeY+30);
        CGContextStrokePath(pdf);
        
        
        /*Starts : top header..*/
        
        CGContextSelectFont(pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        writeingContextToPDF(pdf, 60, incomeY +10,@"Date");
        writeingContextToPDF(pdf, 150, incomeY +10,@"Income Type");
        writeingContextToPDF(pdf, 365, incomeY +10,@"Amount");
        writeingContextToPDF(pdf, 480, incomeY +10,@"Note");
        
        CGContextSelectFont(pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        CGContextMoveToPoint(pdf, 56, incomeY+30);
        CGContextAddLineToPoint(pdf, 56, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 140, incomeY+30);
        CGContextAddLineToPoint(pdf, 140, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 600, incomeY+30);
        CGContextAddLineToPoint(pdf, 600, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 360, incomeY+30);
        CGContextAddLineToPoint(pdf, 360, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 470, incomeY+30);
        CGContextAddLineToPoint(pdf, 470, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 56, incomeY);
        CGContextAddLineToPoint(pdf, 600, incomeY);
        CGContextStrokePath(pdf);
        
        
        
        
        incomeY -=30;
        
        /*Ends : top header..*/
        
        
        
        if ([incomeTypeSummaryArray count]>0)
        {
            for (int i =0;i<[incomeTypeSummaryArray count];i++)
            {
                if (incomeY < 50)
                {
                    //incomeY = 750;
                    CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    writeingContextToPDF(pdf, 438, 18, @"Powered By");
                    
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    
                    CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                    CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                    writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                    
                    CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 12);
                    
                    writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                    CGContextEndPage(pdf);
                    
                    CGContextBeginPage (pdf, &pageRect);
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 14);
                    
                    
                    incomeY = 750;
                    
                }
                NSString *Str = [[incomeTypeSummaryArray objectAtIndex:i]valueForKey:@"notes"];
                nslog(@"length == %d",Str.length);
                int l = Str.length;
                array_notes = [[NSMutableArray alloc] init];
                int j=0;
                for(int i=0;i<l/15;i++)
                {
                    if(Str.length >14)
                    {
                        nslog(@"j == %d",j);
                        nslog(@"Str == %@",Str);
                        NSRange stringRange = {0, MIN([Str length], 15)};
                        // adjust the range to include dependent chars
                        stringRange = [Str rangeOfComposedCharacterSequencesForRange:stringRange];
                        // Now you can create the short string
                        NSString *shortString = [Str substringWithRange:stringRange];
                        nslog(@"shortString == %@",shortString);
                        Str = [Str substringFromIndex:15];
                        //  [Str retain];
                        [array_notes addObject:shortString];
                        j = j+15;
                    }
                    else
                    {
                        break;
                    }
                }
                
                if(Str.length>0)
                {
                    [array_notes addObject:Str];
                }
                
                
                nslog(@"array_notes == %@",array_notes);
                // incomeY = incomeY-[array_notes count]*30;
                nslog(@"incomeY == %d",incomeY);
                nslog(@"([array_notes count]*30) === %d",([array_notes count]*30));
                int k1 = incomeY-[array_notes count]*30;
                if (k1 < 100)
                {
                    //incomeY = 750;
                    CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    writeingContextToPDF(pdf, 438, 18, @"Powered By");
                    
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    
                    CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                    CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                    writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                    
                    CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);//Drawing Image...
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 12);
                    
                    writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                    CGContextEndPage(pdf);
                    
                    CGContextBeginPage (pdf, &pageRect);
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 14);
                    
                    
                    incomeY = 750;
                    
                }
                
                /*
                 writeingContextToPDF(pdf, 60, incomeY+10,[df stringFromDate:[[incomeTypeSummaryArray objectAtIndex:i]valueForKey:@"date"]]);
                 */
                CGContextSetFontSize(pdf, 12);
                
                writeingContextToPDF(pdf, 60, incomeY+10,[appDelegate.regionDateFormatter stringFromDate:[[incomeTypeSummaryArray objectAtIndex:i]valueForKey:@"date"]]);
                
                writeingContextToPDF(pdf, 150, incomeY+10,[[incomeTypeSummaryArray objectAtIndex:i]valueForKey:@"type"]);
                
                
                //CGContextShowGlyphsAtPoint(pdf,465,incomeY+10,&glyph,cfIndex);
                CGContextDrawImage(pdf,CGRectMake(365,incomeY-39,60,60),currencySymbolImage.CGImage);//Drawing Image...
                
                
                for(int i =0;i<[array_notes count];i++)
                {
                    writeingContextToPDF(pdf, 480, incomeY+10-(i*30),[array_notes objectAtIndex:i]);
                }
                
                nslog(@"arrray notes === %@",array_notes);
                
                if(len == 1)
                {
                    writeingContextToPDF(pdf, 380, incomeY+10,[[incomeTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else if(len == 2)
                {
                    writeingContextToPDF(pdf, 390, incomeY+10,[[incomeTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else if(len == 3)
                {
                    writeingContextToPDF(pdf, 400, incomeY+10,[[incomeTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else if(len == 4)
                {
                    writeingContextToPDF(pdf, 410, incomeY+10,[[incomeTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                
                if([array_notes count]>0)
                {
                    incomeY = incomeY-([array_notes count]-1)*30;
                }
                
                CGContextMoveToPoint(pdf, 56, incomeY);
                CGContextAddLineToPoint(pdf, 600, incomeY);
                CGContextStrokePath(pdf);
                
                if([array_notes count]>0)
                {
                    CGContextMoveToPoint(pdf, 56, incomeY+([array_notes count]*30));
                    CGContextAddLineToPoint(pdf, 56, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 140, incomeY+([array_notes count]*30));
                    CGContextAddLineToPoint(pdf, 140, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 600, incomeY+([array_notes count]*30));
                    CGContextAddLineToPoint(pdf, 600, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 360, incomeY+([array_notes count]*30));
                    CGContextAddLineToPoint(pdf, 360, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 470, incomeY+([array_notes count]*30));
                    CGContextAddLineToPoint(pdf, 470, incomeY);
                    CGContextStrokePath(pdf);
                }
                else
                {
                    CGContextMoveToPoint(pdf, 56, incomeY+30);
                    CGContextAddLineToPoint(pdf, 56, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 140, incomeY+30);
                    CGContextAddLineToPoint(pdf, 140, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 600, incomeY+30);
                    CGContextAddLineToPoint(pdf, 600, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 360, incomeY+30);
                    CGContextAddLineToPoint(pdf, 360, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 470, incomeY+30);
                    CGContextAddLineToPoint(pdf, 470, incomeY);
                    CGContextStrokePath(pdf);
                    
                }
                
                
                incomeY = incomeY-30;
            }
            
            
            /* Old code
             CGContextMoveToPoint(pdf, 56, incomeY+30 );
             CGContextAddLineToPoint(pdf, 56, incomeY-30);
             CGContextStrokePath(pdf);
             
             
             CGContextMoveToPoint(pdf, 600, incomeY+30);
             CGContextAddLineToPoint(pdf, 600, incomeY-30);
             CGContextStrokePath(pdf);
             
             CGContextMoveToPoint(pdf, 360, incomeY+30);
             CGContextAddLineToPoint(pdf, 360, incomeY-30);
             CGContextStrokePath(pdf);
             
             CGContextMoveToPoint(pdf, 470, incomeY+30);
             CGContextAddLineToPoint(pdf, 470, incomeY-30);
             CGContextStrokePath(pdf);
             */
            
            
            
            /*Starts: cell background for total INcome*/
            
            CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
            CGContextFillRect(pdf, CGRectMake(56,incomeY,544,30));
            CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
            
            /* Ends: cell background  total INcome */
            
            CGContextMoveToPoint(pdf, 56, incomeY+30 );
            //CGContextAddLineToPoint(pdf, 56, incomeY-30);
            CGContextAddLineToPoint(pdf, 56, incomeY);
            CGContextStrokePath(pdf);
            
            
            CGContextMoveToPoint(pdf, 600, incomeY+30);
            //CGContextAddLineToPoint(pdf, 600, incomeY-30);
            CGContextAddLineToPoint(pdf, 600, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 360, incomeY+30);
            // CGContextAddLineToPoint(pdf, 360, incomeY-30);
            CGContextAddLineToPoint(pdf, 360, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 470, incomeY+30);
            //CGContextAddLineToPoint(pdf, 470, incomeY-30);
            CGContextAddLineToPoint(pdf, 470, incomeY);
            CGContextStrokePath(pdf);
            
            
            
            
            //writeingContextToPDF(pdf, 56, incomeY-20,@"Total Income");
            
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            writeingContextToPDF(pdf, 60, incomeY+10,@"Total Income");
            
            
            
            if(len == 1)
            {
                writeingContextToPDF(pdf, 380, incomeY+10, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
            }
            else if(len == 2)
            {
                
                writeingContextToPDF(pdf, 390, incomeY+10, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
            }
            else if(len == 3)
            {
                if([localeString isEqualToString:@"MYR"] || [localeString isEqualToString:@"JPY"])
                {
                    writeingContextToPDF(pdf, 400, incomeY+8, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
                }
                else
                {
                    writeingContextToPDF(pdf, 400, incomeY+10, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
                }
                
            }
            else if(len == 4)
            {
                
                writeingContextToPDF(pdf, 410, incomeY+10, [NSString stringWithFormat:@"%.2f",totalIncomeAmount]);
            }
            
            
            
            
            
            //CGContextShowGlyphsAtPoint(pdf,465,incomeY-20,&glyph,cfIndex);
            CGContextDrawImage(pdf,CGRectMake(365,incomeY-39,60,60),currencySymbolImage.CGImage);//Drawing Image...
            
            /*income Y:drawingt bottom Y
             CGContextMoveToPoint(pdf, 56, incomeY-30);
             CGContextAddLineToPoint(pdf, 600, incomeY-30);
             CGContextStrokePath(pdf);
             */
            
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            
            CGContextMoveToPoint(pdf, 56, incomeY);
            CGContextAddLineToPoint(pdf, 600, incomeY);
            CGContextStrokePath(pdf);
            
            
            incomeY = incomeY-30;
            
        }
        else
        {
            //  incomeY = incomeY - 110;
            writeingContextToPDF(pdf, 56, incomeY-20,@"No Income Transaction Found");
            
            incomeY = incomeY-50;
            
            /*
             CGContextMoveToPoint(pdf, 56, incomeY-40);
             CGContextAddLineToPoint(pdf, 600, incomeY-40);
             CGContextStrokePath(pdf);
             */
        }
        
        
        nslog(@"\n 2.income_types_dictionary == %@",income_types_dictionary);
        
        //Starts:income transaction receipts...
        
        //New page
        
        
        if(incomeY<200)
        {
            
            
            CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 10);
            writeingContextToPDF(pdf, 438, 18, @"Powered By");
            
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 10);
            
            CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
            CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
            writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
            
            CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
            CGContextEndPage(pdf);
            
            CGContextBeginPage (pdf, &pageRect);
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 14);
            
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 14);
            
            
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            
            incomeY = 720;
            
        }
        else
        {
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 14);
            
            
            writeingContextToPDF(pdf, 56, incomeY,@"Income Transactions Receipts");
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            temp_string = @"Income Transactions Receipts";
            text_size = [self calculate_size:temp_string font_size:15.0];
            
            CGContextMoveToPoint(pdf,56,incomeY-10);
            CGContextAddLineToPoint(pdf,56+text_size.width,incomeY-10);
            CGContextStrokePath(pdf);
            
            incomeY -=70;
            
        }
        
        
        
        /*
         CGContextMoveToPoint(pdf, 56, incomeY+30);
         CGContextAddLineToPoint(pdf, 600, incomeY+30);
         CGContextStrokePath(pdf);
         */
        
        
        
        
        
        
        int img_income_count = 0;
        
        NSArray*income_types_dictionary_keys = [[NSArray alloc] initWithArray:[income_types_dictionary allKeys]] ;
        
        
        nslog(@"\n income_types_dictionary_keys = %@",income_types_dictionary_keys);
        nslog(@"\n [income_types_dictionary = %@",income_types_dictionary);
        if ([income_types_dictionary_keys count]>0)
        {
            for (int i =0;i<[income_types_dictionary_keys count];i++)
            {
                
                
                
                nslog(@"\n key  = %@",[income_types_dictionary_keys objectAtIndex:i]);
                
                
                
                
                
                NSMutableArray*temp_receipt_images_array = [income_types_dictionary objectForKey:[income_types_dictionary_keys objectAtIndex:i]];
                
                
                
                nslog(@"\n temp_receipt_images_array = %@",temp_receipt_images_array);
                
                for(int j =0;j<[temp_receipt_images_array count];j++)
                {
                    
                    if (incomeY < 300)
                    {
                        //incomeY = 750;
                        CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 10);
                        writeingContextToPDF(pdf, 438, 18, @"Powered By");
                        
                        
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 10);
                        CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                        CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                        writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                        
                        
                        
                        CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                        
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 12);
                        
                        writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                        CGContextEndPage(pdf);
                        
                        CGContextBeginPage (pdf, &pageRect);
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 14);
                        
                        incomeY = 750;
                        
                    }
                    
                    
                    
                    UIImage *img = [appDelegate getImage:[temp_receipt_images_array objectAtIndex:j]];
                    
                    if(img != nil)
                    {
                        
                        writeingContextToPDF(pdf, 56,incomeY,[NSString stringWithFormat:@"%@ Receipts",[income_types_dictionary_keys objectAtIndex:i]]);
                        
                        
                        CGContextDrawImage(pdf,CGRectMake(56,incomeY-(img.size.height+30) ,img.size.width,img.size.height),img.CGImage);
                        incomeY = incomeY-(img.size.height+50);
                        img_income_count++;
                    }
                    else
                    {
                        
                        /*
                         writeingContextToPDF(pdf, 56, incomeY-30,@"No Receipt Found");
                         incomeY = incomeY-80;
                         */
                    }
                    
                    
                    
                    
                }
                
                
                
            }
            
        }
        else
        {
            // incomeY = incomeY - 110;
            writeingContextToPDF(pdf, 56, incomeY-20,@"No Income Transactions Receipts Found");
            
            /*
             CGContextMoveToPoint(pdf, 56, incomeY-40);
             CGContextAddLineToPoint(pdf, 600, incomeY-40);
             CGContextStrokePath(pdf);
             */
        }
        
        /*if there is no any image for income transaction..*/
        if(img_income_count==0)
        {
            writeingContextToPDF(pdf, 56, incomeY-20,@"No Income Transactions Receipts Found");
            
            
            
        }
        
        
        
        
        //Expense transactions start..
        //New page
        
        
        nslog(@"incomeY == %d",incomeY);
        
        
        CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 10);
        writeingContextToPDF(pdf, 438, 18, @"Powered By");
        
        //CFURLRef url = (CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
        
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 10);
        
        CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
        writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
        
        CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);//Drawing Image...
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
        CGContextEndPage(pdf);
        
        CGContextBeginPage (pdf, &pageRect);
        
        
        
        CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
        
        CGContextSetFontSize(pdf, 14);
        writeingContextToPDF(pdf, 56, 800,@"Expense Transactions");
        
        temp_string = @"Expense Transactions";
        text_size = [self calculate_size:temp_string font_size:15.0];
        
        
        CGContextMoveToPoint(pdf,56,790);
        CGContextAddLineToPoint(pdf,56+text_size.width,790);
        CGContextStrokePath(pdf);
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        
        // incomeY = 720;
        incomeY = 745;
        
        
        
        
        
        
        /*Starts: cell background for header*/
        
        CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
        CGContextFillRect(pdf, CGRectMake(56,incomeY,544,30));
        CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
        
        /* Ends: cell background for header*/
        
        
        
        /* starts: expense transaction top line*/
        
        CGContextMoveToPoint(pdf, 56, incomeY+30);
        CGContextAddLineToPoint(pdf, 600, incomeY+30);
        CGContextStrokePath(pdf);
        
        
        
        /*Starts : top header..*/
        
        
        
        CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        writeingContextToPDF(pdf, 60, incomeY +10,@"Date");
        writeingContextToPDF(pdf, 150, incomeY +10,@"Expense Type");
        writeingContextToPDF(pdf, 365, incomeY +10,@"Amount");
        writeingContextToPDF(pdf, 480, incomeY +10,@"Note");
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        
        
        
        CGContextMoveToPoint(pdf, 56, incomeY+30);
        CGContextAddLineToPoint(pdf, 56, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 140, incomeY+30);
        CGContextAddLineToPoint(pdf, 140, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 600, incomeY+30);
        CGContextAddLineToPoint(pdf, 600, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 360, incomeY+30);
        CGContextAddLineToPoint(pdf, 360, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 470, incomeY+30);
        CGContextAddLineToPoint(pdf, 470, incomeY);
        CGContextStrokePath(pdf);
        
        CGContextMoveToPoint(pdf, 56, incomeY);
        CGContextAddLineToPoint(pdf, 600, incomeY);
        CGContextStrokePath(pdf);
        
        
        
        
        incomeY -=30;
        
        /*Ends : top header..*/
        
        
        
        
        
        
        
        
        
        if ([expenseTypeSummaryArray count]>0)
        {
            
            for (int i =0;i<[expenseTypeSummaryArray count];i++)
            {
                if (incomeY < 50)
                {
                    //incomeY = 750;
                    CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    writeingContextToPDF(pdf, 438, 18, @"Powered By");
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    
                    CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                    CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                    writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                    
                    CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);//Drawing Image...
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 12);
                    
                    writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                    CGContextEndPage(pdf);
                    
                    CGContextBeginPage (pdf, &pageRect);
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 14);
                    
                    /*
                     writeingContextToPDF(pdf, 56, 800,@"Expense Transactions");
                     
                     CGContextSetFontSize(pdf, 12);
                     
                     CGContextMoveToPoint(pdf, 56, 780);
                     CGContextAddLineToPoint(pdf, 600, 780);
                     CGContextStrokePath(pdf);
                     // CGContextAddLineToPoint(pdf, 56, 586);
                     */
                    
                    incomeY = 750;
                    
                }
                
                NSString *Str = [[expenseTypeSummaryArray objectAtIndex:i]valueForKey:@"notes"];
                nslog(@"length == %d",Str.length);
                int l = Str.length;
                array_notes = [[NSMutableArray alloc] init];
                int j=0;
                for(int i=0;i<l/15;i++)
                {
                    if(Str.length >14)
                    {
                        nslog(@"j == %d",j);
                        nslog(@"Str == %@",Str);
                        NSRange stringRange = {0, MIN([Str length], 15)};
                        // adjust the range to include dependent chars
                        stringRange = [Str rangeOfComposedCharacterSequencesForRange:stringRange];
                        // Now you can create the short string
                        NSString *shortString = [Str substringWithRange:stringRange];
                        nslog(@"shortString == %@",shortString);
                        Str = [Str substringFromIndex:15];
                        // [Str retain];
                        [array_notes addObject:shortString];
                        j = j+15;
                    }
                    else
                    {
                        break;
                    }
                }
                
                if(Str.length>0)
                {
                    [array_notes addObject:Str];
                }
                
                
                nslog(@"array_notes == %@",array_notes);
                // incomeY = incomeY-[array_notes count]*30;
                nslog(@"incomeY == %d",incomeY);
                nslog(@"([array_notes count]*30) === %d",([array_notes count]*30));
                int k1 = incomeY-[array_notes count]*30;
                if (k1 < 100)
                {
                    //incomeY = 750;
                    CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    writeingContextToPDF(pdf, 438, 18, @"Powered By");
                    
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 10);
                    
                    CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                    CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                    writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                    
                    CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);//Drawing Image...
                    
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 12);
                    
                    writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                    CGContextEndPage(pdf);
                    
                    CGContextBeginPage (pdf, &pageRect);
                    CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                    CGContextSetFontSize(pdf, 14);
                    
                    /*
                     writeingContextToPDF(pdf, 56, 800,@"Expense Transactions");
                     
                     CGContextSetFontSize(pdf, 12);
                     
                     CGContextMoveToPoint(pdf, 56, 780);
                     CGContextAddLineToPoint(pdf, 600, 780);
                     CGContextStrokePath(pdf);
                     // CGContextAddLineToPoint(pdf, 56, 586);
                     */
                    
                    incomeY = 750;
                    
                }
                
                
                writeingContextToPDF(pdf, 60, incomeY+10,[appDelegate.regionDateFormatter stringFromDate:[[expenseTypeSummaryArray objectAtIndex:i]valueForKey:@"date"]]);
                
                
                writeingContextToPDF(pdf, 150, incomeY+10,[[expenseTypeSummaryArray objectAtIndex:i]valueForKey:@"type"]);
                
                for(int i =0;i<[array_notes count];i++)
                {
                    writeingContextToPDF(pdf, 480, incomeY+10-(i*30),[array_notes objectAtIndex:i]);
                }
                
                nslog(@"arrray notes === %@",array_notes);
                if(len == 1)
                {
                    writeingContextToPDF(pdf, 380, incomeY+10,[[expenseTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else if(len == 2)
                {
                    writeingContextToPDF(pdf, 390, incomeY+10,[[expenseTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else if(len == 3)
                {
                    writeingContextToPDF(pdf, 400, incomeY+10,[[expenseTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                else
                {
                    writeingContextToPDF(pdf, 410, incomeY+10,[[expenseTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]);
                }
                
                
                
                CGContextDrawImage(pdf,CGRectMake(365,incomeY-39,60,60),currencySymbolImage.CGImage);//Drawing Image...
                
                if([array_notes count]>0)
                {
                    incomeY = incomeY-([array_notes count]-1)*30;
                }
                else
                {
                    // incomeY = incomeY - 30;
                }
                CGContextMoveToPoint(pdf, 56, incomeY);
                CGContextAddLineToPoint(pdf, 600, incomeY);
                CGContextStrokePath(pdf);
                
                if([array_notes count]>0)
                {
                    CGContextMoveToPoint(pdf, 56, incomeY+30*([array_notes count]));
                    CGContextAddLineToPoint(pdf, 56, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 140, incomeY+30*([array_notes count]));
                    CGContextAddLineToPoint(pdf, 140, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 600, incomeY+30*([array_notes count]));
                    CGContextAddLineToPoint(pdf, 600, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 360, incomeY+30*([array_notes count]));
                    CGContextAddLineToPoint(pdf, 360, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 470, incomeY+30*([array_notes count]));
                    CGContextAddLineToPoint(pdf, 470, incomeY);
                    CGContextStrokePath(pdf);
                }
                else
                {
                    CGContextMoveToPoint(pdf, 56, incomeY+30);
                    CGContextAddLineToPoint(pdf, 56, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 140, incomeY+30);
                    CGContextAddLineToPoint(pdf, 140, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 600, incomeY+30);
                    CGContextAddLineToPoint(pdf, 600, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 360, incomeY+30);
                    CGContextAddLineToPoint(pdf, 360, incomeY);
                    CGContextStrokePath(pdf);
                    
                    CGContextMoveToPoint(pdf, 470, incomeY+30);
                    CGContextAddLineToPoint(pdf, 470, incomeY);
                    CGContextStrokePath(pdf);
                    
                }
                incomeY = incomeY-30;
            }
            
            
            
            
            /*Starts: cell background for total expense*/
            
            CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
            CGContextFillRect(pdf, CGRectMake(56,incomeY,544,30));
            CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
            
            /* Ends: cell background  total expense */
            
            CGContextMoveToPoint(pdf, 56, incomeY+30);
            CGContextAddLineToPoint(pdf, 56, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 600, incomeY+30);
            CGContextAddLineToPoint(pdf, 600, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 360, incomeY+30);
            CGContextAddLineToPoint(pdf, 360, incomeY);
            CGContextStrokePath(pdf);
            
            
            CGContextMoveToPoint(pdf, 470, incomeY+30);
            CGContextAddLineToPoint(pdf, 470, incomeY);
            CGContextStrokePath(pdf);
            
            
            
            //writeingContextToPDF(pdf, 56, incomeY-20,@"Total Expense");
            
            
            
            
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            writeingContextToPDF(pdf, 60, incomeY+10,@"Total Expense");
            
            
            
            if(len == 1)
            {
                writeingContextToPDF(pdf, 380, incomeY+10, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
            }
            else if(len == 2)
            {
                writeingContextToPDF(pdf, 390, incomeY+10, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
            }
            else if(len == 3)
            {
                
                if([localeString isEqualToString:@"MYR"] || [localeString isEqualToString:@"JPY"])
                {
                    writeingContextToPDF(pdf, 400, incomeY+8, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
                }
                else
                {
                    writeingContextToPDF(pdf, 400, incomeY+10, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
                }
                
                
            }
            else if(len == 4)
            {
                if([localeString isEqualToString:@"MYR"] || [localeString isEqualToString:@"JPY"])
                {
                    writeingContextToPDF(pdf, 410, incomeY+8, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
                }
                else
                {
                    writeingContextToPDF(pdf, 410, incomeY+10, [NSString stringWithFormat:@"%.2f",totalExpenseAmount]);
                }
                
            }
            
            //CGContextShowGlyphsAtPoint(pdf,465,incomeY-20,&glyph,cfIndex);
            CGContextDrawImage(pdf,CGRectMake(365,incomeY-39,60,60),currencySymbolImage.CGImage);//Drawing Image...
            
            /*
             CGContextMoveToPoint(pdf, 56, incomeY-30);
             CGContextAddLineToPoint(pdf, 600, incomeY-30);
             CGContextStrokePath(pdf);
             */
            
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            CGContextMoveToPoint(pdf, 56, incomeY);
            CGContextAddLineToPoint(pdf, 600, incomeY);
            CGContextStrokePath(pdf);
            
            
            
            incomeY = incomeY-30;
            
            
        }
        else
        {
            // incomeY = incomeY - 110;
            writeingContextToPDF(pdf, 56, incomeY-20,@"No Expense Transaction Found");
            
            incomeY = incomeY-50;
            
            /*
             CGContextMoveToPoint(pdf, 56, incomeY-40);
             CGContextAddLineToPoint(pdf, 600, incomeY-40);
             CGContextStrokePath(pdf);
             */
            
        }
        
        
        
        
        
        
        if(incomeY<200)
        {
            
            
            CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 10);
            writeingContextToPDF(pdf, 438, 18, @"Powered By");
            
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 10);
            
            CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
            CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
            writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
            
            CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
            
            
            
            nslog(@"\n 2.income_types_dictionary == %@",income_types_dictionary);
            
            //Starts:income transaction receipts...
            
            //New page
            
            
            
            
            CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 10);
            writeingContextToPDF(pdf, 438, 18, @"Powered By");
            
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 10);
            
            CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
            writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
            
            CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
            CGContextEndPage(pdf);
            
            CGContextBeginPage (pdf, &pageRect);
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 14);
            
            
            
            
            incomeY = 720;
            
        }
        else
        {
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 14);
            
            
            writeingContextToPDF(pdf, 56, incomeY,@"Expense Transactions Receipts");
            
            
            temp_string = @"Expense Transactions Receipts";
            text_size = [self calculate_size:temp_string font_size:15.0];
            
            CGContextMoveToPoint(pdf,56,incomeY-10);
            CGContextAddLineToPoint(pdf,56+text_size.width,incomeY-10);
            CGContextStrokePath(pdf);
            
            
            incomeY -=70;
            
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 14);
            
        }
        
        
        
        
        
        
        
        int img_expense_count = 0;
        NSArray*expense_types_dictionary_keys = [[NSArray alloc] initWithArray:[expense_types_dictionary allKeys]] ;
        nslog(@"\n income_types_dictionary_keys = %@",expense_types_dictionary_keys);
        nslog(@"\n [income_types_dictionary = %@",expense_types_dictionary);
        if ([expense_types_dictionary_keys count]>0)
        {
            for (int i =0;i<[expense_types_dictionary_keys count];i++)
            {
                
                
                
                
                
                nslog(@"\n key  = %@",[expense_types_dictionary_keys objectAtIndex:i]);
                
                
                
                
                NSMutableArray*temp_receipt_images_array = [expense_types_dictionary objectForKey:[expense_types_dictionary_keys objectAtIndex:i]];
                
                
                
                nslog(@"\n temp_receipt_images_array = %@",temp_receipt_images_array);
                
                for(int j =0;j<[temp_receipt_images_array count];j++)
                {
                    
                    if (incomeY < 300)
                    {
                        //incomeY = 750;
                        CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 10);
                        writeingContextToPDF(pdf, 438, 18, @"Powered By");
                        
                        
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 10);
                        
                        CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                        CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                        writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                        
                        CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                        
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 12);
                        
                        writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                        CGContextEndPage(pdf);
                        
                        CGContextBeginPage (pdf, &pageRect);
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 14);
                        
                        /*
                         
                         writeingContextToPDF(pdf, 56, 800,@"Expense Transactions Receipts");
                         
                         CGContextSetFontSize(pdf, 12);
                         
                         CGContextMoveToPoint(pdf, 56, 780);
                         CGContextAddLineToPoint(pdf, 600, 780);
                         CGContextStrokePath(pdf);
                         // CGContextAddLineToPoint(pdf, 56, 586);
                         */
                        incomeY = 750;
                        
                    }
                    
                    
                    
                    UIImage *img = [appDelegate getImage:[temp_receipt_images_array objectAtIndex:j]];
                    
                    if(img != nil)
                    {
                        
                        writeingContextToPDF(pdf, 56,incomeY,[NSString stringWithFormat:@"%@ Receipts",[expense_types_dictionary_keys objectAtIndex:i]]);
                        
                        CGContextDrawImage(pdf,CGRectMake(56,incomeY-(img.size.height+30) ,img.size.width,img.size.height),img.CGImage);
                        incomeY = incomeY-(img.size.height+50);
                        img_expense_count ++;
                        
                    }
                    else
                    {
                        
                        /*
                         writeingContextToPDF(pdf, 56, incomeY-30,@"No Receipt Found");
                         incomeY = incomeY-80;
                         */
                    }
                    
                    
                }
                
                
                
            }
            
        }
        else
        {
            // incomeY = incomeY - 110;
            writeingContextToPDF(pdf, 56, incomeY-20,@"No Expense Transactions Receipts Found");
            
            /*
             CGContextMoveToPoint(pdf, 56, incomeY-40);
             CGContextAddLineToPoint(pdf, 600, incomeY-40);
             CGContextStrokePath(pdf);
             */
            
        }
        
        
        /*if there is no any image for expense transaction..*/
        if(img_expense_count==0)
        {
            writeingContextToPDF(pdf, 56, incomeY-20,@"No Expense Transactions Receipts Found");
            
            
            /*
             CGContextMoveToPoint(pdf, 56, incomeY-40);
             CGContextAddLineToPoint(pdf, 600, incomeY-40);
             CGContextStrokePath(pdf);
             */
        }
        
        
        //Ends:expense transaction receipts...
        
        
        
        
        
        CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 10);
        writeingContextToPDF(pdf, 438, 18, @"Powered By");
        
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 10);
        
        
        CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
        writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
        
        CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
        
        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
        CGContextSetFontSize(pdf, 12);
        
        writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
        
        
        
        if(appDelegate.is_milege_on)
        {
            
            /*Sun:004
             
             Starts:Mileage...
             */
            
            
            
            //Mileage transactions start..
            //New page
            
            /**/
            nslog(@"incomeY == %d",incomeY);
            
            
            CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 10);
            writeingContextToPDF(pdf, 438, 18, @"Powered By");
            
            //CFURLRef url = (CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
            
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 10);
            
            CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
            writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
            
            CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);//Drawing Image...
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
            CGContextEndPage(pdf);
            
            CGContextBeginPage (pdf, &pageRect);
            
            
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            
            CGContextSetFontSize(pdf, 14);
            writeingContextToPDF(pdf, 56, 800,@"Mileage Transactions");
            
            temp_string = @"Mileage Transactions";
            text_size = [self calculate_size:temp_string font_size:15.0];
            
            
            CGContextMoveToPoint(pdf,56,790);
            CGContextAddLineToPoint(pdf,56+text_size.width,790);
            CGContextStrokePath(pdf);
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            
            
            incomeY = 745;
            
            
            
            
            
            
            /*Starts: cell background for header*/
            
            CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
            CGContextFillRect(pdf, CGRectMake(56,incomeY,544,30));
            CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
            
            /* Ends: cell background for header*/
            
            
            
            /* starts: expense transaction top line*/
            
            CGContextMoveToPoint(pdf, 56, incomeY+30);
            CGContextAddLineToPoint(pdf, 600, incomeY+30);
            CGContextStrokePath(pdf);
            
            
            
            /*Starts : top header..*/
            
            
            
            CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            writeingContextToPDF(pdf, 60, incomeY +10,@"Date");
            writeingContextToPDF(pdf, 150, incomeY +10,@"Type");
            writeingContextToPDF(pdf, 365, incomeY +10,@"Total units");
            writeingContextToPDF(pdf, 480, incomeY +10,@"Note");
            
            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
            CGContextSetFontSize(pdf, 12);
            
            
            
            
            CGContextMoveToPoint(pdf, 56, incomeY+30);
            CGContextAddLineToPoint(pdf, 56, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 140, incomeY+30);
            CGContextAddLineToPoint(pdf, 140, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 600, incomeY+30);
            CGContextAddLineToPoint(pdf, 600, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 360, incomeY+30);
            CGContextAddLineToPoint(pdf, 360, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 470, incomeY+30);
            CGContextAddLineToPoint(pdf, 470, incomeY);
            CGContextStrokePath(pdf);
            
            CGContextMoveToPoint(pdf, 56, incomeY);
            CGContextAddLineToPoint(pdf, 600, incomeY);
            CGContextStrokePath(pdf);
            
            
            
            
            incomeY -=30;
            
            /*Ends : top header..*/
            
            
            
            
            
            
            
            
            
            if ([milageTypeSummaryArray count]>0)
            {
                
                for (int i =0;i<[milageTypeSummaryArray count];i++)
                {
                    if (incomeY < 50)
                    {
                        //incomeY = 750;
                        CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 10);
                        writeingContextToPDF(pdf, 438, 18, @"Powered By");
                        
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 10);
                        
                        CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                        CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                        writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                        
                        CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);//Drawing Image...
                        
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 12);
                        
                        writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                        CGContextEndPage(pdf);
                        
                        CGContextBeginPage (pdf, &pageRect);
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 14);
                        
                        
                        incomeY = 750;
                        
                    }
                    
                    NSString *Str = [[milageTypeSummaryArray objectAtIndex:i]valueForKey:@"notes"];
                    nslog(@"length == %d",Str.length);
                    int l = Str.length;
                    array_notes = [[NSMutableArray alloc] init];
                    int j=0;
                    for(int i=0;i<l/15;i++)
                    {
                        if(Str.length >14)
                        {
                            nslog(@"j == %d",j);
                            nslog(@"Str == %@",Str);
                            NSRange stringRange = {0, MIN([Str length], 15)};
                            // adjust the range to include dependent chars
                            stringRange = [Str rangeOfComposedCharacterSequencesForRange:stringRange];
                            // Now you can create the short string
                            NSString *shortString = [Str substringWithRange:stringRange];
                            nslog(@"shortString == %@",shortString);
                            Str = [Str substringFromIndex:15];
                            // [Str retain];
                            [array_notes addObject:shortString];
                            j = j+15;
                        }
                        else
                        {
                            break;
                        }
                    }
                    
                    if(Str.length>0)
                    {
                        [array_notes addObject:Str];
                    }
                    
                    
                    nslog(@"array_notes == %@",array_notes);
                    // incomeY = incomeY-[array_notes count]*30;
                    nslog(@"incomeY == %d",incomeY);
                    nslog(@"([array_notes count]*30) === %d",([array_notes count]*30));
                    int k1 = incomeY-[array_notes count]*30;
                    if (k1 < 100)
                    {
                        //incomeY = 750;
                        CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 10);
                        writeingContextToPDF(pdf, 438, 18, @"Powered By");
                        
                        
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 10);
                        
                        CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                        CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                        writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                        
                        CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);//Drawing Image...
                        
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 12);
                        
                        writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                        CGContextEndPage(pdf);
                        
                        CGContextBeginPage (pdf, &pageRect);
                        CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                        CGContextSetFontSize(pdf, 14);
                        
                        
                        
                        incomeY = 750;
                        
                    }
                    
                    
                    writeingContextToPDF(pdf, 60, incomeY+10,[appDelegate.regionDateFormatter stringFromDate:[[milageTypeSummaryArray objectAtIndex:i]valueForKey:@"date"]]);
                    
                    
                    writeingContextToPDF(pdf, 150, incomeY+10,[[milageTypeSummaryArray objectAtIndex:i]valueForKey:@"type"]);
                    
                    for(int i =0;i<[array_notes count];i++)
                    {
                        writeingContextToPDF(pdf, 480, incomeY+10-(i*30),[array_notes objectAtIndex:i]);
                    }
                    
                    nslog(@"arrray notes === %@",array_notes);
                    
                    writeingContextToPDF(pdf, 365, incomeY+10,[NSString stringWithFormat:@"%@ %@",[[milageTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"],[appDelegate.prefs objectForKey:@"milage_unit"]]);
                    
                    
                    
                    
                    
                    
                    //CGContextDrawImage(pdf,CGRectMake(365,incomeY-39,60,60),currencySymbolImage.CGImage);//Drawing Image...
                    
                    if([array_notes count]>0)
                    {
                        incomeY = incomeY-([array_notes count]-1)*30;
                    }
                    else
                    {
                        // incomeY = incomeY - 30;
                    }
                    CGContextMoveToPoint(pdf, 56, incomeY);
                    CGContextAddLineToPoint(pdf, 600, incomeY);
                    CGContextStrokePath(pdf);
                    
                    if([array_notes count]>0)
                    {
                        CGContextMoveToPoint(pdf, 56, incomeY+30*([array_notes count]));
                        CGContextAddLineToPoint(pdf, 56, incomeY);
                        CGContextStrokePath(pdf);
                        
                        CGContextMoveToPoint(pdf, 140, incomeY+30*([array_notes count]));
                        CGContextAddLineToPoint(pdf, 140, incomeY);
                        CGContextStrokePath(pdf);
                        
                        CGContextMoveToPoint(pdf, 600, incomeY+30*([array_notes count]));
                        CGContextAddLineToPoint(pdf, 600, incomeY);
                        CGContextStrokePath(pdf);
                        
                        CGContextMoveToPoint(pdf, 360, incomeY+30*([array_notes count]));
                        CGContextAddLineToPoint(pdf, 360, incomeY);
                        CGContextStrokePath(pdf);
                        
                        CGContextMoveToPoint(pdf, 470, incomeY+30*([array_notes count]));
                        CGContextAddLineToPoint(pdf, 470, incomeY);
                        CGContextStrokePath(pdf);
                    }
                    else
                    {
                        CGContextMoveToPoint(pdf, 56, incomeY+30);
                        CGContextAddLineToPoint(pdf, 56, incomeY);
                        CGContextStrokePath(pdf);
                        
                        CGContextMoveToPoint(pdf, 140, incomeY+30);
                        CGContextAddLineToPoint(pdf, 140, incomeY);
                        CGContextStrokePath(pdf);
                        
                        CGContextMoveToPoint(pdf, 600, incomeY+30);
                        CGContextAddLineToPoint(pdf, 600, incomeY);
                        CGContextStrokePath(pdf);
                        
                        CGContextMoveToPoint(pdf, 360, incomeY+30);
                        CGContextAddLineToPoint(pdf, 360, incomeY);
                        CGContextStrokePath(pdf);
                        
                        CGContextMoveToPoint(pdf, 470, incomeY+30);
                        CGContextAddLineToPoint(pdf, 470, incomeY);
                        CGContextStrokePath(pdf);
                        
                    }
                    incomeY = incomeY-30;
                }
                
                
                
                
                /*Starts: cell background for total expense*/
                
                CGContextSetRGBFillColor(pdf, 217.0/255.0,217.0/255.0,217.0/255.0, 1.0);
                CGContextFillRect(pdf, CGRectMake(56,incomeY,544,30));
                CGContextSetRGBFillColor(pdf, 0.0, 0.0, 0.0, 1.0);
                
                /* Ends: cell background  total expense */
                
                CGContextMoveToPoint(pdf, 56, incomeY+30);
                CGContextAddLineToPoint(pdf, 56, incomeY);
                CGContextStrokePath(pdf);
                
                CGContextMoveToPoint(pdf, 600, incomeY+30);
                CGContextAddLineToPoint(pdf, 600, incomeY);
                CGContextStrokePath(pdf);
                
                CGContextMoveToPoint(pdf, 360, incomeY+30);
                CGContextAddLineToPoint(pdf, 360, incomeY);
                CGContextStrokePath(pdf);
                
                
                CGContextMoveToPoint(pdf, 470, incomeY+30);
                CGContextAddLineToPoint(pdf, 470, incomeY);
                CGContextStrokePath(pdf);
                
                
                
                //writeingContextToPDF(pdf, 56, incomeY-20,@"Total Expense");
                
                
                
                
                
                CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 12);
                writeingContextToPDF(pdf, 60, incomeY+10,@"Total Mileage");
                
                
                writeingContextToPDF(pdf, 365, incomeY+10,[NSString stringWithFormat:@"%.2f %@",totalMilage,[appDelegate.prefs objectForKey:@"milage_unit"]]);
                
                
                
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 12);
                
                CGContextMoveToPoint(pdf, 56, incomeY);
                CGContextAddLineToPoint(pdf, 600, incomeY);
                CGContextStrokePath(pdf);
                
                
                
                incomeY = incomeY-30;
                
                
            }
            else
            {
                // incomeY = incomeY - 110;
                writeingContextToPDF(pdf, 56, incomeY-20,@"No Mileage Entries Found");
                
                incomeY = incomeY-50;
                
                
            }
            
            
            //images..
            
            
            
            
            
            if(incomeY<200)
            {
                
                
                CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 10);
                writeingContextToPDF(pdf, 438, 18, @"Powered By");
                
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 10);
                
                CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                
                CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 12);
                
                writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                
                
                
                nslog(@"\n 2.income_types_dictionary == %@",income_types_dictionary);
                
                //Starts:income transaction receipts...
                
                //New page
                
                
                
                
                CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 10);
                writeingContextToPDF(pdf, 438, 18, @"Powered By");
                
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 10);
                
                CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                
                CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 12);
                
                writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                CGContextEndPage(pdf);
                
                CGContextBeginPage (pdf, &pageRect);
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                
                
                CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 14);
                
                
                
                
                incomeY = 720;
                
            }
            else
            {
                
                CGContextSelectFont (pdf,"Arial-BoldMT", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 14);
                
                
                writeingContextToPDF(pdf, 56, incomeY,@"Mileage Transactions Receipts");
                
                
                temp_string = @"Mileage Transactions Receipts";
                text_size = [self calculate_size:temp_string font_size:15.0];
                
                CGContextMoveToPoint(pdf,56,incomeY-10);
                CGContextAddLineToPoint(pdf,56+text_size.width,incomeY-10);
                CGContextStrokePath(pdf);
                
                
                incomeY -=70;
                
                
                CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                CGContextSetFontSize(pdf, 14);
                
            }
            
            
            
            
            
            
            
            int img_transaction_count = 0;
            NSArray*milage_types_dictionary_keys = [[NSArray alloc] initWithArray:[milage_types_dictionary allKeys]] ;
            
            
            nslog(@"\n income_types_dictionary_keys = %@",milage_types_dictionary_keys);
            nslog(@"\n [income_types_dictionary = %@",milage_types_dictionary);
            if ([milage_types_dictionary_keys count]>0)
            {
                for (int i =0;i<[milage_types_dictionary_keys count];i++)
                {
                    
                    
                    
                    
                    
                    nslog(@"\n key  = %@",[milage_types_dictionary_keys objectAtIndex:i]);
                    
                    
                    
                    
                    NSMutableArray*temp_receipt_images_array = [milage_types_dictionary objectForKey:[milage_types_dictionary_keys objectAtIndex:i]];
                    
                    
                    
                    nslog(@"\n temp_receipt_images_array = %@",temp_receipt_images_array);
                    
                    for(int j =0;j<[temp_receipt_images_array count];j++)
                    {
                        
                        if (incomeY < 300)
                        {
                            //incomeY = 750;
                            CGContextSelectFont (pdf,"Arial-ItalicMT", 35, kCGEncodingMacRoman);
                            CGContextSetFontSize(pdf, 10);
                            writeingContextToPDF(pdf, 438, 18, @"Powered By");
                            
                            
                            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                            CGContextSetFontSize(pdf, 10);
                            
                            CFURLRef url = (__bridge CFURLRef)[NSURL URLWithString:@"http://www.iapplab.com/"];
                            CGPDFContextSetURLForRect(pdf,url,CGRectMake(0,10,120,20));
                            writeingContextToPDF(pdf,10, 18, @"www.iapplab.com");
                            
                            CGContextDrawImage(pdf,CGRectMake(495, 10,32, 32),image.CGImage);
                            
                            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                            CGContextSetFontSize(pdf, 12);
                            
                            writeingContextToPDF(pdf, 530, 18, @"Property Log Book");
                            CGContextEndPage(pdf);
                            
                            CGContextBeginPage (pdf, &pageRect);
                            CGContextSelectFont (pdf,"Arial", 35, kCGEncodingMacRoman);
                            CGContextSetFontSize(pdf, 14);
                            
                            incomeY = 750;
                            
                        }
                        
                        
                        
                        UIImage *img = [appDelegate getImage:[temp_receipt_images_array objectAtIndex:j]];
                        
                        if(img != nil)
                        {
                            
                            
                            /*SUN:004
                             writeingContextToPDF(pdf, 56,incomeY,[NSString stringWithFormat:@"%@ Receipts",[milage_types_dictionary_keys objectAtIndex:i]]);
                             */
                            
                            CGContextDrawImage(pdf,CGRectMake(56,incomeY-(img.size.height+30) ,img.size.width,img.size.height),img.CGImage);
                            incomeY = incomeY-(img.size.height+50);
                            img_transaction_count ++;
                            
                        }
                        else
                        {
                            
                            /*
                             writeingContextToPDF(pdf, 56, incomeY-30,@"No Receipt Found");
                             incomeY = incomeY-80;
                             */
                        }
                        
                        
                    }
                    
                    
                    
                }
                
            }
            else
            {
                // incomeY = incomeY - 110;
                writeingContextToPDF(pdf, 56, incomeY-20,@"No Mileage Transactions Receipts Found");
                
                /*
                 CGContextMoveToPoint(pdf, 56, incomeY-40);
                 CGContextAddLineToPoint(pdf, 600, incomeY-40);
                 CGContextStrokePath(pdf);
                 */
                
            }
            
            
            /*if there is no any image for expense transaction..*/
            if(img_transaction_count==0)
            {
                writeingContextToPDF(pdf, 56, incomeY-20,@"No Mileage Transactions Receipts Found");
                
                
                /*
                 CGContextMoveToPoint(pdf, 56, incomeY-40);
                 CGContextAddLineToPoint(pdf, 600, incomeY-40);
                 CGContextStrokePath(pdf);
                 */
            }
            
            
            //Ends:expense transaction receipts...
            
            
            /*
             Sun:004
             Ends:Mileage
             */
            
            
            
        }
        
        
        
        CGContextEndPage(pdf);
        CGContextRelease(pdf);
       
        /*
        CGContextFlush(pdf);
        CGPDFContextClose(pdf);
        */
        
        
        
        
        
        
        
        
        
        /*
         id plist=[NSPropertyListSerialization dataFromPropertyList:(id)appDelegate.propertyDetailArray format:NSPropertyListXMLFormat_v1_0 errorDescription:&error];
         
         if(plist)
         {
         [plist writeToFile:plistPath atomically:YES];
         }
         else
         {
         nslog(@"%@",error);
         // [error release];
         }
         */
        
        /**/
        NSURL *filePath=[[NSURL alloc] initFileURLWithPath:newFilePath];
        [pdfView loadRequest:[NSURLRequest requestWithURL:filePath]];
        
        
        CGSize fitsSize = [pdfView sizeThatFits:CGSizeMake(CGRectGetWidth(self.view.bounds), CGFLOAT_MAX)];
        nslog(@"\n fitsSize = %@ \n",NSStringFromCGSize(fitsSize));
        
        
        // [filePath release];
        // Do any additional setup after loading the view from its nib.
        
        
        
    }
    
    
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
}

- (void)segmentAction:(id)sender
{
	
	if([sender selectedSegmentIndex] == 1)
    {
        
		[self showEmail];
		nslog(@"Segment 1 preesed");
	}else{
        [self showPrint];
		nslog(@"Segment 2 preesed");
	}
}
-(void)showPrint
{
    rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    saveFile_Name=[NSString stringWithFormat:@"%@%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"],@".pdf"];
    
    saveFile_Name  = [saveFile_Name stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    
    
    newFilePath =	[rootPath stringByAppendingPathComponent:saveFile_Name];
    NSString *path = newFilePath;
    NSData *dataFromPath = [NSData dataWithContentsOfFile:path];
    
    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    
    if(printController && [UIPrintInteractionController canPrintData:dataFromPath]) {
        
        printController.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [path lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        printController.printInfo = printInfo;
        printController.showsPageRange = YES;
        printController.printingItem = dataFromPath;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                nslog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
            }
        };
        
        
        if(appDelegate.isIPad)
        {
            
            //[printController presentAnimated:YES completionHandler:completionHandler];
            
            /*
             [printController presentFromRect:segmentedControl.bounds inView:self.view animated:TRUE completionHandler:completionHandler];
             
             */
            
            [printController presentFromBarButtonItem:segmentBarItem animated:TRUE completionHandler:completionHandler];
            
            
        }
        else
        {
            [printController presentAnimated:YES completionHandler:completionHandler];
        }
        
        
    }
}

-(void)showEmail
{
    if( [MFMailComposeViewController canSendMail])
    {
        rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        saveFile_Name=[NSString stringWithFormat:@"%@%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"],@".pdf"];
        saveFile_Name  = [saveFile_Name stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
        
        
        
        newFilePath =	[rootPath stringByAppendingPathComponent:saveFile_Name];
        
        MFMailComposeViewController *sendEmail=[[MFMailComposeViewController alloc] init];
        [sendEmail setSubject:@"Property Report"];
        sendEmail.mailComposeDelegate = self;
        NSData *data = [NSData dataWithContentsOfFile:newFilePath];
        
        [sendEmail addAttachmentData:data mimeType:@"Application/pdf" fileName:saveFile_Name];
        
        [sendEmail.navigationBar setBarStyle:UIBarStyleBlackOpaque];
        
        [self presentModalViewController:sendEmail animated:YES];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please setup your mail account." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
		[alert show];
		//[alert release];
    }
    
}

#pragma mark - calculate_size
#pragma mark calculates the size of space...

-(CGSize)calculate_size:(NSString*)string_to_calculate font_size:(float)font_size
{
    
    
    
    CGSize text_size =  [string_to_calculate drawInRect:CGRectMake(195, 800, 640, 50) withFont:[UIFont fontWithName:@"Arial" size:font_size] lineBreakMode:UILineBreakModeClip alignment:UITextAlignmentCenter];
    return text_size;
    
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

-(void)cancel_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)dealloc
{
    //[super dealloc];
    pdfView = nil;
    
    
    incomeTypeSummaryArray = nil;
    expenseTypeSummaryArray = nil;
    milageTypeSummaryArray = nil;
    
}


@end
