//
//  agentCustomCell.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

@interface agentCustomCell : UITableViewCell {
    
    UILabel *agentLabel;
    UITextField *agentTextField;
    UITextView *AddressTextView;
    PropertyLogBookAppDelegate *appDelegate; 
}
@property (nonatomic, retain)UILabel *agentLabel;
@property (nonatomic, retain)UITextField *agentTextField;
@property (nonatomic,retain) UITextView *AddressTextView;
@end
