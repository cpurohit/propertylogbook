//
//  ReportViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 9/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PropertyLogBookAppDelegate;
@interface ReportViewController : UIViewController<UITableViewDelegate> {
    
    IBOutlet UITableView *tblView;
    PropertyLogBookAppDelegate *appDelegate;

    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIToolbar *toolBar;
    IBOutlet UIButton *fromButton, *toButton;
    int buttontag;
    NSDateFormatter *df;

    
    IBOutlet UILabel *noDataLabel;
    
    IBOutlet UILabel *lblFrom;
    IBOutlet UILabel *lblTo; 
    IBOutlet UIImageView*imgViewBar;
    
    IBOutlet UIImageView *noDataImageView;

}


@property(nonatomic,retain)IBOutlet UILabel *noDataLabel;

@property(nonatomic,retain) IBOutlet UIImageView *noDataImageView;

-(IBAction)pdf_clicked:(id)sender;

-(IBAction)dateBtn_clicked:(id)sender;

-(IBAction)done_clicked;

-(IBAction)datePicker_changed:(id)sender;
@end
