//
//  AddPropertyDetailViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddPropertyDetailViewController.h"
#import "CustomCell.h"
#import "CustomCell1.h"
#import "CustomCell2.h"
#import "propertyTypeViewController.h"
#import "currencyViewController.h"
#import "agentInformationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+Resize.h"
#import "UIImage+Alpha.h"


#define NUMBERS	@"0123456789."


@implementation AddPropertyDetailViewController

#pragma mark - View lifecycle

- (void)dealloc
{
    [picker release];
    [pickerView release];
    [toolBar release];
    [datePicker release];
    [keytoolBar release];
    [propertyArray release];
    [propertyInfoArray release];
    [agentInfoArray release];
    [financialArray release];
    [pickerArray release];
    [repayment_term_picker_array release];
    
    if(!appDelegate.isIOS7)
    {
         [super dealloc];
    }
    
   
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    
    [super didReceiveMemoryWarning];
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    
    //For ios 7
    /*CP:Date- 03-03-14*/
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    
    keytoolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 156, self.view.frame.size.width, 44)];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 156, 320, 44)];
    repayment_or_repayment_term = 1;
    
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(keytoolBar_cancel_clicked)];
    
    
    if(appDelegate.isIOS7)
    {
        cancel.tintColor = [UIColor whiteColor];
    }
    
    
    UIBarButtonItem *flexiItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(keytoolBar_done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(keytoolBar_done_clicked)];
    */
    
    
    [keytoolBar setItems:[NSArray arrayWithObjects:cancel,flexiItem,doneButton, nil]];
    //    toolBar.items = array;
    
    
    [cancel release];
    [flexiItem release];
    [doneButton release];
    
    keytoolBar.barStyle = UIBarStyleBlack;
    
    keytoolBar.hidden = TRUE;
    
    
    picker = [[UIImagePickerController alloc]init];
	picker.delegate = self;
    
    /*PickerView*/
    
    
    pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 200, 320, 216)];
    
    pickerView.backgroundColor = [UIColor whiteColor];
    
    pickerView.dataSource = self;
    pickerView.delegate = self;
    pickerView.tag = 1052;
    [pickerView setShowsSelectionIndicator:YES];
    pickerView.hidden = YES;
    /*ToolBar*/    
    
    toolBar.tag = 1051;
    toolBar.hidden = YES;
    
    /*datePicker*/
    pickerView.hidden = TRUE;
    datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 200, 320,216 )];
    
    datePicker.backgroundColor = [UIColor whiteColor];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.tag = 1050;
    [datePicker addTarget:self action:@selector(datePicker_change) forControlEvents:UIControlEventValueChanged];
    datePicker.hidden = YES;
    
    /*
     Starts:
     Sun:0004
     Checking iphone-5 condition.
     
     */
    if(appDelegate.isIphone5)
    {
        datePicker.frame = CGRectMake(datePicker.frame.origin.x, datePicker.frame.origin.y+44,datePicker.frame.size.width, datePicker.frame.size.height);
        
        keytoolBar.frame = CGRectMake(keytoolBar.frame.origin.x, keytoolBar.frame.origin.y+88,keytoolBar.frame.size.width, keytoolBar.frame.size.height);
        
        
        pickerView.frame = CGRectMake(pickerView.frame.origin.x, pickerView.frame.origin.y+44,pickerView.frame.size.width, pickerView.frame.size.height);
        
        
        
        toolBar.frame = CGRectMake(toolBar.frame.origin.x, toolBar.frame.origin.y+44,toolBar.frame.size.width, toolBar.frame.size.height);
    }
    
    
       
        

    
    /*
     Ends :
     Sun:0004
     Checking iphone-5 condition.
     */
    
    
    [self.view addSubview:keytoolBar];
    [self.view addSubview:datePicker];
    [self.view addSubview:toolBar];
    [self.view addSubview:pickerView];
    
    textFieldIndex = -1;
    used = 0;
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    propertyInfoDictionary = [[NSMutableDictionary alloc]init];
    appDelegate.propertyImage = [UIImage imageNamed:@"noimage3.png"];
 
    
    if([appDelegate.agentArray count]==0)
    {
        [appDelegate selectAgentType];
    }
    
    
    
    
    if ([appDelegate.agentArray count]>0)
    {
        appDelegate.agentTypeStr    = [[appDelegate.agentArray objectAtIndex:0]valueForKey:@"0"];
        appDelegate.agent_pk        = [NSString stringWithFormat:@"%d",[[[appDelegate.agentArray objectAtIndex:0]objectForKey:@"rowid"] intValue]];
    }
    else
    {
        appDelegate.agentTypeStr = @"Agent Type";
        appDelegate.agent_pk = @"0";
    }
    if (appDelegate.isProperty >=0)
    {
        self.navigationItem.title = @"EDIT PROPERTY";    
    }
    else
    {
        imageCount = FALSE;
        self.navigationItem.title = @"ADD PROPERTY";
    }
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
   // [button setTitle:@"  Back" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    button.bounds = CGRectMake(0, 0,49.0,29.0);
    [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];

    /*
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save_clicked)] autorelease];
    */
    
    
    UIButton * save_button = [UIButton buttonWithType:UIButtonTypeCustom];
    [save_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    save_button.bounds = CGRectMake(0, 0,51.0,30.0);
    [save_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_save_button.png"] forState:UIControlStateNormal];
    [save_button addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:save_button] autorelease];
    
    propertyArray = [[NSMutableArray alloc]init];
    propertyInfoArray = [[NSMutableArray alloc]initWithObjects:@"Address",nil];
    agentInfoArray = [[NSMutableArray alloc]initWithObjects:@"Managed by agent",@"Property Agent", nil];
    
    financialArray = [[NSMutableArray alloc]initWithObjects:@"Purchase Price",@"Market Value",
                      nil];
    
    
    loanInformatio = [[NSMutableArray alloc] initWithObjects:@"Fully Paid",
                      @"Mortgage Bank",
                      @"Total Loan borrowed",
                      @"Loan Outstanding",
                      @"Loan Term (years)",
                      @"Repayment Amount",
                      @"Repayment Freq.",
                      @"Repayment Term",
                      nil];
    
    
    leaseInformation = [[NSMutableArray alloc] initWithObjects:@"On Lease",
                        @"Rental Income",
                        @"Lease Start Date",
                        @"Lease End Date",nil];
    
    pickerArray = [[NSMutableArray alloc]initWithObjects:@"Weekly",@"Bi Weekly",@"Monthly",@"Yearly", nil];
    
    [propertyInfoDictionary setObject:@"" forKey:@"0"];
    [propertyInfoDictionary setObject:@"" forKey:@"address"];
    [propertyInfoDictionary setObject:@"" forKey:@"8"];
    [propertyInfoDictionary setObject:@"" forKey:@"9"];
    [propertyInfoDictionary setObject:@"" forKey:@"11"];
    [propertyInfoDictionary setObject:@"" forKey:@"12"];
    [propertyInfoDictionary setObject:@"" forKey:@"15"];
    
    [propertyInfoDictionary setObject:@"" forKey:@"loan_outstanding"];
    [propertyInfoDictionary setObject:@"" forKey:@"loan_terms"];
    [propertyInfoDictionary setObject:@"" forKey:@"total_loan_borrowed"];
    [propertyInfoDictionary setObject:@"" forKey:@"rental_income"];
    
    
    [propertyInfoDictionary setObject:@"" forKey:@"tenant_information"];
    
    
    
    [propertyInfoDictionary setObject:@"Weekly" forKey:@"repayment"];
    [propertyInfoDictionary setObject:@"Interest only" forKey:@"repayment_term"];
    [propertyInfoDictionary setObject:@"" forKey:@"leasestart"];
    [propertyInfoDictionary setObject:@"" forKey:@"leaseend"];
    [propertyInfoDictionary setObject:@"" forKey:@"propertytype"];
    [propertyInfoDictionary setObject:@"" forKey:@"agenttype"];
    [propertyInfoDictionary setObject:@"" forKey:@"agent_pk"];
    
    
    [propertyInfoDictionary setObject:@"" forKey:@"currencytype"];
    [propertyInfoDictionary setObject:[NSNumber numberWithInt:0] forKey:@"fullypaid"];
    [propertyInfoDictionary setObject:[NSNumber numberWithInt:0] forKey:@"lease"];
    [propertyInfoDictionary setObject:[NSNumber numberWithInt:0] forKey:@"agentManage"];
    [propertyInfoDictionary setObject:@"0" forKey:@"zLoan"];
    
    
    repayment_term_picker_array = [[NSMutableArray alloc] initWithObjects:
                                   @"Principal & Interest",
                                   @"Interest only",
                                   nil];
    
    
    isFullyPaid = FALSE;
    isAgent = FALSE;
    isLease = FALSE;
    
    
    if([appDelegate.propertyTypeArray count]==0)
    {
        [appDelegate selectPropertyType];
    }
    
    
    
    
    if ([appDelegate.propertyTypeArray count]>0)
    {
        appDelegate.propertyTypeStr = [[appDelegate.propertyTypeArray objectAtIndex:0]valueForKey:@"propertyType"];
    }
    else
    {
        appDelegate.propertyTypeStr = @"Property Type";
    }
    

    
    if (appDelegate.isProperty >=0)
    {
        nslog(@"property detail array  === %@",appDelegate.propertyDetailArray);
        
        propertyName = [[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"0"];
        [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"0"] forKey:@"0"];
        
        for (int i=8;i<10; i++)
        {
            [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:[NSString stringWithFormat:@"%d",i]] forKey:[NSString stringWithFormat:@"%d",i]];
        }
        for (int i = 11; i<16; i++)
        {
            [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:[NSString stringWithFormat:@"%d",i]] forKey:[NSString stringWithFormat:@"%d",i]];
        }
        
        
        [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"address"] forKey:@"address"];

        
        
       

        
        
        
        [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"repayment"] forKey:@"repayment"];
        [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"leasestart"] forKey:@"leasestart"];
        
        [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"leaseend"] forKey:@"leaseend"];
        [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"propertytype"] forKey:@"propertytype"];
        appDelegate.propertyTypeStr = [propertyInfoDictionary objectForKey:@"propertytype"];
        
        [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"agenttype"] forKey:@"agenttype"];
        
        [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"agent_pk"] forKey:@"agent_pk"];

        
        /*Sun:0004
         Rental income
         */
        [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"rental_income"] forKey:@"rental_income"];

        /*Sun:0004
         
         Tenant information..
         */
        
         [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"tenant_information"] forKey:@"tenant_information"];
        
        /*Sun:0004
         
         Repayment_term
         */
        
        [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"repayment_term"] forKey:@"repayment_term"];
        
        
        appDelegate.agentTypeStr = [propertyInfoDictionary objectForKey:@"agenttype"];
        appDelegate.agent_pk = [propertyInfoDictionary objectForKey:@"agent_pk"];
        
        
        
        
        if ([[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"imageCount"]intValue]==1)
        {
            imageCount = TRUE;
            PropertyIcon.image = [[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"image"];

        }
       
        
        
        
        [propertyInfoDictionary setObject:[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"currencytype"] forKey:@"currencytype"];
        appDelegate.currencyTypeStr = [propertyInfoDictionary objectForKey:@"currencytype"];
        [propertyInfoDictionary setObject:[NSNumber numberWithInt:[[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"fullypaid"]intValue]] forKey:@"fullypaid"];
        
        
        isFullyPaid = [[propertyInfoDictionary objectForKey:@"fullypaid"] boolValue];
        [propertyInfoDictionary setObject:[NSNumber numberWithInt:[[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"lease"]intValue]] forKey:@"lease"];
        isLease = [[propertyInfoDictionary objectForKey:@"lease"]boolValue];
        [propertyInfoDictionary setObject:[NSNumber numberWithInt:[[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"agentManage"]intValue]] forKey:@"agentManage"];
        isAgent = [[propertyInfoDictionary objectForKey:@"agentManage"]boolValue];
        
        [propertyInfoDictionary setObject:[NSNumber numberWithFloat:[[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"zLoan"]floatValue]] forKey:@"zLoan"];
        
        
        nslog(@"property info dictionary.....===%@",propertyInfoDictionary);
    }

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //self.navigationItem.rightBarButtonItem = self.editButtonItem;
	tblView.backgroundColor = [UIColor clearColor];
	    
    tblView.tableHeaderView = headerView;
	
	
	[[bkImgView layer] setCornerRadius:8.0];
	
	
	[[bkImgView layer] setBorderWidth:1.0];
	[[bkImgView layer] setBorderColor:[UIColor lightGrayColor].CGColor];
	
	

	
	
	[bkImgView setBackgroundColor:[UIColor whiteColor]];
	

    
    
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
		
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,  1024,1024)];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
	
	UIGestureRecognizer *recognizer;
	
	recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMethod)];
	[(UITapGestureRecognizer *)recognizer setNumberOfTouchesRequired:1];
	
	[PropertyIcon addGestureRecognizer:recognizer];
	recognizer.delegate = self;
	[recognizer release];
    
    nslog(@"\n propertyInfoDictionary = %@",propertyInfoDictionary);
	
}


- (void)viewWillAppear:(BOOL)animated
{
    
    if(is_view_will_appear_called)
    {
        return;
    }
    
    is_view_will_appear_called = TRUE;
    
    [super viewWillAppear:animated];
    

    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    
    //[self.view addSubview:appDelegate.bottomView];
   
    
    
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
        [appDelegate selectCurrencyIndex];
    }
  
    
    if ([appDelegate.propertyTypeStr isEqualToString:@"Property Type"])
    {
        lblPropertyType.textColor = [UIColor grayColor];
    }
    else
    {
        lblPropertyType.textColor = [UIColor blackColor];
    }
    lblPropertyType.text = appDelegate.propertyTypeStr;
    txtName.text = [propertyInfoDictionary objectForKey:@"0"];
    [propertyInfoDictionary setObject:lblPropertyType.text forKey:@"propertytype"];

    keytoolBar.hidden = TRUE;
    toolBar.hidden = TRUE;
    pickerView.hidden = TRUE;
    
    datePicker.hidden = YES;
    
    [tblView reloadData];
    
     nslog(@"\n tblView.contentSize = %f",tblView.contentSize.height);
    
    if(appDelegate.isIPad)
    {

    
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation  duration:1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
   keytoolBar.hidden = TRUE;
    toolBar.hidden = TRUE;
    
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
    [self.view endEditing:TRUE];
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    keytoolBar.hidden = TRUE;
    toolBar.hidden = TRUE;
    [super viewDidDisappear:animated];
}





-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    
    if(appDelegate.isIPad)
    {
        
        datePicker.hidden = YES;
        toolBar.hidden = YES;
        pickerView.hidden = YES;
        keytoolBar.hidden = YES;
        
        if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
        {
            
            self.view.frame = CGRectMake(0,0,768,1024);   
            
            headerView.frame = CGRectMake(0,0,720,headerView.frame.size.height);   
            bkImgView.frame =  CGRectMake(113,44,610,bkImgView.frame.size.height);
            txtName.frame=  CGRectMake(128,54,595,txtName.frame.size.height);
            lblPropertyType.frame=  CGRectMake(128,89,588,lblPropertyType.frame.size.height);
            headerViewButton.frame=  CGRectMake(128,89,588,headerViewButton.frame.size.height);
            headerViewButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            headerLineLabel.frame=  CGRectMake(114,82,608,headerLineLabel.frame.size.height);
        }
        else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
        {
            self.view.frame = CGRectMake(0,0,1024,768);
            headerView.frame = CGRectMake(0,0,1024,headerView.frame.size.height);   
            bkImgView.frame =  CGRectMake(113,44,864,bkImgView.frame.size.height);
            txtName.frame=  CGRectMake(128,54,840,txtName.frame.size.height);
            lblPropertyType.frame=  CGRectMake(128,89,840,lblPropertyType.frame.size.height);
            headerViewButton.frame=  CGRectMake(128,89,840,headerViewButton.frame.size.height);
            headerViewButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            headerLineLabel.frame=  CGRectMake(114,82,862,headerLineLabel.frame.size.height);
        }
        
        
    }
    
   
    
    [self.view endEditing:YES];
    
    
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    /*
    if(appDelegate.isIPad)
    {
        keytoolBar.hidden = TRUE;
        toolBar.hidden = TRUE;
        [self.view endEditing:YES];
    }

   [tblView reloadData];
     */
    
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
    }
    
    
    
    [tblView reloadData];
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

#pragma mark -

-(void)tapMethod
{

	//nslog(@"Called");
	UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"Select Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Gallery",@"Camera",nil];
    sheet.delegate = self;
    [sheet showFromTabBar:self.tabBarController.tabBar];
//	[sheet showInView:self.view];
    [sheet release];
	
	
}

#pragma mark -
#pragma mark ACTION SHEET METHOD
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
   
	if (buttonIndex == 0)
    {
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        
        if(appDelegate.isIPad)
        {
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
            popover.delegate = self;
            [popover presentPopoverFromRect:[actionSheet bounds] inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
        else
        {
           [self presentModalViewController:picker animated:YES];     
        }
        
        
    }
    else if (buttonIndex == 1)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
		{
            [picker setAllowsEditing:NO];
			picker.sourceType = UIImagePickerControllerSourceTypeCamera;
			picker.delegate = self;
            
            if(appDelegate.isIPad)
            {
                UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
                popover.delegate = self;
                [popover presentPopoverFromRect:[actionSheet bounds] inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            }
            else
            {
               [self presentModalViewController:picker animated:YES];     
            }
            
			
			
		}
		else
		{
			UIAlertView *showalert = [[UIAlertView alloc]initWithTitle:nil message:@"No camera device found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[showalert show];
			[showalert release];
		}

    }
}

#pragma mark -

-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker_local
{
	[picker_local dismissModalViewControllerAnimated:YES];
	//secondPieceView.image = [UIImage imageNamed:@"images.jpg"];
	
}
-(void) imagePickerController:(UIImagePickerController *)picker_local didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *tempImage = (UIImage *)[info objectForKey:@"UIImagePickerControllerOriginalImage"];

     tempImage =  [tempImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(93,79) interpolationQuality:kCGInterpolationHigh];
    
	if (picker_local.sourceType == UIImagePickerControllerSourceTypePhotoLibrary)
    {
        PropertyIcon.image = tempImage;
        
        /*
        PropertyIcon.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
         */
    }
	else if (picker_local.sourceType == UIImagePickerControllerSourceTypeCamera)
	{
        
      
        PropertyIcon.image= tempImage;
        /*
        PropertyIcon.image=[self correctImageOrientation:[[info objectForKey:@"UIImagePickerControllerOriginalImage"] CGImage]];
         */
	}
    
    imageCount = TRUE;
    appDelegate.propertyImage = PropertyIcon.image;
    
//	obj_appdelegate.imgviewFinal=imageview.image;
	
	//secondPieceView.image = [UIImage imageNamed:@"images.jpg "];
	[picker_local dismissModalViewControllerAnimated:YES];
}

-(UIImage*)correctImageOrientation:(CGImageRef)image
{
	CGFloat width = CGImageGetHeight(image);//189;
	CGFloat height = CGImageGetWidth(image);//134;
	CGRect bounds = CGRectMake(0.0f, 0.0f, width, height);
	
	CGFloat boundHeight = bounds.size.height;
	bounds.size.height = bounds.size.width;
	bounds.size.width = boundHeight;
	
	CGAffineTransform transform = CGAffineTransformMakeTranslation(height, 0.0f);
	transform = CGAffineTransformRotate(transform, M_PI / 2.0f);
	
	UIGraphicsBeginImageContext(bounds.size);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextScaleCTM(context, - 1.0f, 1.0f);
	CGContextTranslateCTM(context, -height, 0.0f);
	CGContextConcatCTM(context, transform);
	
	CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, width, height), image);
	
	UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	
	UIGraphicsEndImageContext();
	
	return imageCopy;
}

#pragma mark - cancel_clicked
#pragma mark manages popViewControllerAnimated

-(void)cancel_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)save_clicked
{
    UITextView *textView = (UITextView *)[self.view viewWithTag:3456];
    [textView resignFirstResponder];
    if (textFieldIndex >= 0 )
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldIndex];
        [textField resignFirstResponder];
    }


    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];

    NSString* currentTime = [dateFormatter stringFromDate:[NSDate date]];
    [dateFormatter release];
    
    //float loan = [[propertyInfoDictionary objectForKey:@"zLoan"]floatValue];
    float loan = [[propertyInfoDictionary objectForKey:@"13"]floatValue];
    NSString *loanStr = [NSString stringWithFormat:@"%0.2f",loan];
    
    float estimated;
    
    if ([[propertyInfoDictionary objectForKey:@"9"] length]>0)
    {
        NSString *str = [NSString stringWithFormat:@"%@",[propertyInfoDictionary objectForKey:@"9"]];
        estimated = [str floatValue];
    }
    else
    {
        estimated = 0;
    }
    
    float equity = estimated - loan;
    
    NSString *equityStr = [NSString stringWithFormat:@"%0.2f",equity];
    
    [propertyInfoDictionary setObject:currentTime forKey:@"zDate"];
    [propertyInfoDictionary setObject:loanStr forKey:@"zLoan"];
    [propertyInfoDictionary setObject:equityStr forKey:@"zEquity"];

    
    if (imageCount == TRUE)
    {
        appDelegate.propertyImage = PropertyIcon.image;
    }
    else
    {
        appDelegate.propertyImage = [UIImage imageNamed:@"new-cell-property.png"];
    }

   
    
    [propertyInfoDictionary setObject:appDelegate.propertyImage forKey:@"image"];
    
    [propertyInfoDictionary setObject:[NSNumber numberWithBool:imageCount] forKey:@"imageCount"];
    if ([propertyArray count]>0)
   {
       [propertyArray removeAllObjects];
   }
    [propertyArray addObject:propertyInfoDictionary];
    
    //nslog(@"property array ===== %@",propertyArray);
    
    for (int i=0;i<[appDelegate.propertyDetailArray count];i++)
    {
        if ([[propertyInfoDictionary objectForKey:@"0"] isEqualToString:[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"0"]])
        {
            if (i != appDelegate.isProperty)
            {
                used++;
            }
        }
    }
    if (used ==0)
    {
    if (appDelegate.isProperty <0)
    {
        if ([[[propertyArray objectAtIndex:0]valueForKey:@"0"] length]>0)
        {
            nslog(@"property details array while saving..=== %@",propertyArray);
            
            [appDelegate addPropertyDetail:propertyArray];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            
            [self.view endEditing:TRUE];
            keytoolBar.hidden = TRUE;
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Property Name is required" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
    }
    else
    {
        //nslog(@"\n propertyArray = %@",propertyArray);
        if ([[[propertyArray objectAtIndex:0]valueForKey:@"0"] length]>0)
        {
            appDelegate.isPropertyImageUpdate = TRUE;
            nslog(@"property details array while updating....=== %@",appDelegate.propertyDetailArray);

            [appDelegate updatePropertyDetail:propertyArray rowid:[[[appDelegate.propertyDetailArray objectAtIndex:appDelegate.isProperty]valueForKey:@"rowid"]intValue]];
        
        if (![propertyName isEqualToString:[propertyInfoDictionary valueForKey:@"0"]])
        {
            [appDelegate UpdateIncomeExpenseByProperty:[propertyInfoDictionary valueForKey:@"0"] oldProperty:propertyName];
        }

            
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Property Name is required" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }

    }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Property Name already in use. Please choose different" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        used = 0;
    }
       
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark -
#pragma mark button methods
-(void)keytoolBar_cancel_clicked
{
    tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
    keytoolBar.hidden = TRUE;
    
    
    [self.view endEditing:TRUE];
    
    for (int i = 1000; i<1015; i++)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:i];
        [textField resignFirstResponder];
    }

    
    UITextView *textView = (UITextView *)[self.view viewWithTag:3456];
    [textView resignFirstResponder];
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            //tblView.contentSize = CGSizeMake(1024,800);
        }
        
    }

   
}

-(void)keytoolBar_done_clicked
{
    
    
    keytoolBar.hidden = YES;
    
    
    
    if (textViewIndex == 3456)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:1008];
        [textField becomeFirstResponder];
        textViewIndex = 0;
    }
    else if (textFieldIndex == 1008)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:1009];
        [textField becomeFirstResponder];

    }
    else if (textFieldIndex == 1009)
    {
        if (!isFullyPaid)
        {
            UITextField *textField = (UITextField *)[self.view viewWithTag:1011];
            [textField becomeFirstResponder];
        }
        else
        {
            UITextField *textField = (UITextField *)[self.view viewWithTag:1009];
            [textField resignFirstResponder];
            tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
        }
    }
    else if (textFieldIndex == 1011)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:1012];
        [textField becomeFirstResponder];
    }
    else if (textFieldIndex == 1012)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:1013];
        [textField becomeFirstResponder];
    }
    else if (textFieldIndex == 1013)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:1014];
        [textField becomeFirstResponder];
    }
    else if (textFieldIndex == 1014)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:1015];
        [textField becomeFirstResponder];
    }
    else
    {
        tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
        [self.view endEditing:TRUE];
        
        for (int i = 1000; i<1015; i++)
        {
            UITextField *textField = (UITextField *)[self.view viewWithTag:i];
            [textField resignFirstResponder];
        }
    }
    
    
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            // tblView.contentSize = CGSizeMake(1024,800);
        }

    }
     
     
}




-(void)date_changed:(id)sender
{
    
    [self.view endEditing:TRUE];
    
    [self done_clicked];
    
    UIButton *btn = (UIButton *)sender;
    
     
    
    if(!appDelegate.isIPad)
    {
        //[tblView setFrame:CGRectMake(0, -216, tblView.frame.size.width, tblView.frame.size.height)];     
        nslog(@"\n tblView.contentOffset.y =  %f",tblView.contentOffset.y);
        nslog(@"\n tblView.contentSize.height = %f", tblView.contentSize.height);        
        
        if(!isAgent)
        {
             tblView.contentOffset = CGPointMake(0,585);
        }
        else
        {
             tblView.contentOffset = CGPointMake(0,600);
        }
        
        
    
        
        
    }
    
    
    
    toolBar.tag = 1051;
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(done_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flextSpance = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done_clicked)];
    */
    
    
    
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flextSpance,doneButton, nil]];
    
    [cancelBtn release];
    [flextSpance release];
    [doneButton release];    
    
    toolBar.barStyle = UIBarStyleBlack;
    
    datePicker.hidden = NO;
    toolBar.hidden = NO;
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            datePicker.frame =  CGRectMake(0,715, 768,216 );
            toolBar.frame = CGRectMake(0, 671,768, 44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            
           // tblView.contentSize = CGSizeMake(1024,1000);
            tblView.contentOffset = CGPointMake(0,300);
            
            datePicker.frame =  CGRectMake(0,513,1024,116 );
            toolBar.frame = CGRectMake(0,469,1024, 44);
        }
        
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
	df.dateStyle = NSDateFormatterMediumStyle;
    
    
    
    
    isStart = btn.tag;
    if (btn.tag == 200)
    {
        
        
        if(!appDelegate.isIPad)
        {
            if(isFullyPaid == YES)
            {
                tblView.contentOffset = CGPointMake(0,600);
            }
            else 
            {
                tblView.contentOffset = CGPointMake(0,850);
            }

        }
        else
        {
            if(isFullyPaid == NO)
            {
               // tblView.contentOffset = CGPointMake(0,100);
            }
        }
                

        
        if (appDelegate.isProperty >=0)
        {
            
                       
            if ([[propertyInfoDictionary objectForKey:@"leasestart"] length]>0)
            {
                
                 nslog(@"\n leasestart = %@",[propertyInfoDictionary objectForKey:@"leasestart"]);
                 NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                 [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                 NSDate *date = [dtFormatter dateFromString:[propertyInfoDictionary objectForKey:@"leasestart"]];
                 nslog(@"\n date using dateFormatter = %@",date);
                
                if(date == nil)
                {
                    datePicker.date = [appDelegate.regionDateFormatter dateFromString:[propertyInfoDictionary objectForKey:@"leasestart"]];
                }
                else
                {
                    datePicker.date = date;
                }
                
                [dtFormatter release];
                
                
            }
        }
        
        
        /*
         [btn setTitle:[NSString stringWithFormat:@"  %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
         */
        [btn setTitle:[NSString stringWithFormat:@"%@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
        
        
        
        //leaseStart = btn.titleLabel.text;
        leaseStart = [appDelegate fetchDateInCommonFormate:btn.titleLabel.text];
        [propertyInfoDictionary setObject:leaseStart forKey:@"leasestart"];
        [leaseStart retain];
    }
    else if (btn.tag == 201)
    {
         if(!appDelegate.isIPad)
         {
             if(isFullyPaid == YES)
             {
                 tblView.contentOffset = CGPointMake(0,630);
             }
             else
             {
                 tblView.contentOffset = CGPointMake(0,870);
             }
             

         }
         else
         {
            if(isFullyPaid == NO)
            {
               // tblView.contentOffset = CGPointMake(0,150);
            }
         }
               
        if (appDelegate.isProperty >=0)
        {
            if ([[propertyInfoDictionary objectForKey:@"leaseend"] length]>0)
            {
                
               
                
                
                 NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                 [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                 NSDate *date = [dtFormatter dateFromString:[propertyInfoDictionary objectForKey:@"leaseend"]];
                 nslog(@"\n date using dateFormatter = %@",date);
                if(date == nil)
                {
                    datePicker.date = [appDelegate.regionDateFormatter dateFromString:[propertyInfoDictionary objectForKey:@"leaseend"]];
                }
                else
                {
                   datePicker.date = date; 
                }
                
                
                
                 [dtFormatter release];
                 
                
            }
        }
        
        /*
         [btn setTitle:[NSString stringWithFormat:@"  %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
         */ 
        [btn setTitle:[NSString stringWithFormat:@"  %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
        
        
        
        //leaseEnd = btn.titleLabel.text;
        leaseEnd = [appDelegate fetchDateInCommonFormate:btn.titleLabel.text];
        [propertyInfoDictionary setObject:leaseEnd forKey:@"leaseend"];
        
        [leaseEnd retain];
    }
    [df release];
    
    [self scrollViewToUIButton:btn];
    
}

-(void)datePicker_change
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
	df.dateStyle = NSDateFormatterMediumStyle;
    
    if (isStart == 200)
    {
        UIButton *btn = (UIButton *)[self.view viewWithTag:200];
     
        [btn setTitle:[NSString stringWithFormat:@"  %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
        leaseStart = [appDelegate fetchDateInCommonFormate:btn.titleLabel.text];
        [propertyInfoDictionary setObject:leaseStart forKey:@"leasestart"];
        
        [leaseStart retain];
    }
    else if (isStart == 201)
    {
        UIButton *btn = (UIButton *)[self.view viewWithTag:201];
        
        /*
        [btn setTitle:[NSString stringWithFormat:@"  %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
        */

        [btn setTitle:[NSString stringWithFormat:@"  %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
        
        //leaseEnd = btn.titleLabel.text;
        leaseEnd = [appDelegate fetchDateInCommonFormate:btn.titleLabel.text];
        [propertyInfoDictionary setObject:leaseEnd forKey:@"leaseend"];
        
        [leaseEnd retain];
    }
    [df release];

}


#pragma mark - repayment_clicked
#pragma mark when repayment button clicked
-(void)repayment_clicked:(id)sender
{
    
    repayment_or_repayment_term = 1;
    [pickerView reloadComponent:0];
    [self.view endEditing:TRUE];
    UIButton *tmpBtn = (UIButton*)sender;
    
    [self done_clicked];
    
    if(!appDelegate.isIPad)
    {
      //[tblView setFrame:CGRectMake(0, -180, tblView.frame.size.width, tblView.frame.size.height)];      
        
        nslog(@"\n %f",tblView.contentOffset.y);
        nslog(@"\n %f", tblView.contentSize.height);        
        
        //tblView.contentSize = CGSizeMake(320,1050);
        
         tblView.contentOffset = CGPointMake(0,630);
    }
    
    UIButton *btn = (UIButton *)[self.view viewWithTag:102];
    for (int i = 0;i <[pickerArray count];i++)
    {
        if ([[pickerArray objectAtIndex:i] isEqualToString:btn.titleLabel.text])
            {
                [pickerView selectRow:i inComponent:0 animated:YES];
            }
     }


    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(done_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flexiSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    
       
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexiSpace, doneButton, nil]];
   
    
    [cancelBtn release];
    [flexiSpace release];
    [doneButton release];
    
    toolBar.barStyle = UIBarStyleBlack;

    toolBar.hidden = NO;
    pickerView.hidden = NO;
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            pickerView.frame =  CGRectMake(0,715, 768,216 );
            toolBar.frame = CGRectMake(0, 671,768, 44);

        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            
            nslog(@"\n content = %f",tblView.contentSize.height);
            
           // tblView.contentSize = CGSizeMake(1024,1000);
            tblView.contentOffset = CGPointMake(0,300);
            
            toolBar.frame = CGRectMake(0,469,1024, 44);
            pickerView.frame =  CGRectMake(0,513,1024,116 );
        }
        
    }
   

     [self scrollViewToUIButton:tmpBtn];
   
}



#pragma mark - repayment_term_clicked
#pragma mark when repayment term button clicked
-(void)repayment_term_clicked:(id)sender
{
    
    
    repayment_or_repayment_term = 2;
    [self.view endEditing:TRUE];
    UIButton *tmpBtn = (UIButton*)sender;
    
    [self scrollViewToUIButton:tmpBtn];
    
    [self done_clicked];
    
    if(!appDelegate.isIPad)
    {
        
        
        nslog(@"\n %f",tblView.contentOffset.y);
        nslog(@"\n %f", tblView.contentSize.height);
        
        //tblView.contentSize = CGSizeMake(320,1050);
        
        tblView.contentOffset = CGPointMake(0,630);
    }
    [pickerView reloadComponent:0];
    
    
    UIButton *btn = (UIButton *)[self.view viewWithTag:103];
    for (int i = 0;i <[repayment_term_picker_array count];i++)
    {
        if ([[repayment_term_picker_array objectAtIndex:i] isEqualToString:btn.titleLabel.text])
        {
            [pickerView selectRow:i inComponent:0 animated:YES];
        }
    }
    
    
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(done_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flexiSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexiSpace, doneButton, nil]];
    
    
    [cancelBtn release];
    [flexiSpace release];
    [doneButton release];
    
    toolBar.barStyle = UIBarStyleBlack;
    
    toolBar.hidden = NO;
    pickerView.hidden = NO;
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            pickerView.frame =  CGRectMake(0,715, 768,216 );
            toolBar.frame = CGRectMake(0, 671,768, 44);
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            
            nslog(@"\n content = %f",tblView.contentSize.height);
            
           // tblView.contentSize = CGSizeMake(1024,1000);
            tblView.contentOffset = CGPointMake(0,300);
            
            toolBar.frame = CGRectMake(0,469,1024, 44);
            pickerView.frame =  CGRectMake(0,513,1024,116 );
        }
        
    }
    
    
    
    
}


#pragma mark - scrollViewToUIButton

- (void)scrollViewToUIButton:(id)button
{
    // Set the current _scrollOffset, so we can return the user after editing
    //   _scrollOffsetY = self.tableView.contentOffset.y;
    
    // Get a pointer to the text field's cell
    
    
    
    
    UITableViewCell *theUIButtonCell = (UITableViewCell *)[button superview];
    
    // Get the text fields location
    CGPoint point = [theUIButtonCell convertPoint:theUIButtonCell.frame.origin toView:tblView];
    
    // Scroll to cell
    [tblView setContentOffset:CGPointMake(0, point.y - 12) animated: YES];
    
    /*
     // Add some padding at the bottom to 'trick' the scrollView.
     [tblView setContentInset:UIEdgeInsetsMake(0, 0, point.y - 60, 0)];
     */
}



#pragma mark - scrollViewToTextField

- (void)scrollViewToTextField:(id)textField
{
    // Set the current _scrollOffset, so we can return the user after editing
 //   _scrollOffsetY = self.tableView.contentOffset.y;
    
    // Get a pointer to the text field's cell
    
    
    
   
    UITableViewCell *theTextFieldCell = (UITableViewCell *)[textField superview];
    
    // Get the text fields location
    CGPoint point = [theTextFieldCell convertPoint:theTextFieldCell.frame.origin toView:tblView];
    
    // Scroll to cell
    [tblView setContentOffset:CGPointMake(0, point.y - 12) animated: YES];
    
     /*
    // Add some padding at the bottom to 'trick' the scrollView.
    [tblView setContentInset:UIEdgeInsetsMake(0, 0, point.y - 60, 0)];
     */
}

#pragma mark - scrollViewToTextView


- (void)scrollViewToTextView:(id)textView
{
    // Set the current _scrollOffset, so we can return the user after editing
    //   _scrollOffsetY = self.tableView.contentOffset.y;
    
    // Get a pointer to the text field's cell
    UITableViewCell *theTextFieldCell = (UITableViewCell *)[textView superview];
    
    // Get the text fields location
    CGPoint point = [theTextFieldCell convertPoint:theTextFieldCell.frame.origin toView:tblView];
    
    
    
    // Scroll to cell
    
    if(appDelegate.isIPad)
    {
        
        [tblView setContentOffset:CGPointMake(0,point.y-100) animated: YES];
        

    }
    else
    {
        if(appDelegate.isIphone5)
        {
            [tblView setContentOffset:CGPointMake(0,point.y-80) animated: YES];
        }
        else
        {
            [tblView setContentOffset:CGPointMake(0,point.y-65) animated: YES];
        }

    }
    
        
    
    
    // Add some padding at the bottom to 'trick' the scrollView.
   // [tblView setContentInset:UIEdgeInsetsMake(0, 0, point.y - 60, 0)];
}




#pragma mark - 
#pragma mark pickerview data

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{	
	return 1;
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	
    if(repayment_or_repayment_term==1)
    {
        return [pickerArray count];
    }
    else if(repayment_or_repayment_term==2)
    {
        return [repayment_term_picker_array count];
    }
    return 0;

}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	
    if(repayment_or_repayment_term==1)
    {
       return [pickerArray objectAtIndex:row];
    }
    else if(repayment_or_repayment_term==2)
    {
       return [repayment_term_picker_array objectAtIndex:row];
        
    }
    return @"";
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if(repayment_or_repayment_term ==1)
    {
        UIButton *btn = (UIButton *)[self.view viewWithTag:102];
        btn.hidden = FALSE;
        [btn setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
        [propertyInfoDictionary setObject:btn.titleLabel.text forKey:@"repayment"];
    }
    else if(repayment_or_repayment_term ==2)
    {
        UIButton *btn = (UIButton *)[self.view viewWithTag:103];
        btn.hidden = FALSE;
        [btn setTitle:[repayment_term_picker_array objectAtIndex:row] forState:UIControlStateNormal];
        [propertyInfoDictionary setObject:btn.titleLabel.text forKey:@"repayment_term"];
    }
    

}




-(void)done_clicked
{
    if(appDelegate.isIPad)
    {
        [popController dismissPopoverAnimated:YES];
    }
    
    nslog(@"\n frame before done.. %f",tblView.frame.size.height);
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
           // tblView.contentSize = CGSizeMake(1024,800);
            
        }
    }
    else
    {
       //  tblView.contentSize = CGSizeMake(320,1000);
    }
    
    
    if(!appDelegate.isIPad)
    {
        if(appDelegate.isIphone5)
        {
            
            tblView.frame =CGRectMake(0, 0, tblView.frame.size.width,455.0f);
        }
        else
        {
            
            tblView.frame =CGRectMake(0, 0, tblView.frame.size.width,455.0f);
        }

    }
    
        
    
    datePicker.hidden = YES;
    toolBar.hidden = YES;
    pickerView.hidden = YES;
    keytoolBar.hidden = YES;
   
}

#pragma mark switch methods

-(void)financialSwitch_chaged:(id)sender
{
    [self done_clicked];
    [self keytoolBar_done_clicked];

    keytoolBar.hidden = TRUE;
    UISwitch *swtch = (UISwitch *)sender;
    if (swtch.on)
    {
        isFullyPaid = TRUE;
    }
    else
    {
        isFullyPaid = FALSE;
    }
    [propertyInfoDictionary setObject:[NSNumber numberWithBool:isFullyPaid] forKey:@"fullypaid"];
    nslog(@"propertyInfoDictionary == %@",propertyInfoDictionary);
    [tblView reloadData];
}

-(void)leaseSwitch_changed:(id)sender
{
    [self done_clicked];
    [self keytoolBar_done_clicked];

    UISwitch *swtch = (UISwitch *)sender;
    if (swtch.on)
    {
        isLease = TRUE;
    }
    else
    {
        isLease = FALSE;
    }
    [propertyInfoDictionary setObject:[NSNumber numberWithBool:isLease] forKey:@"lease"];

    [tblView reloadData];
}

-(void)agentManageSwitch_changed:(id)sender
{
    [self done_clicked];
    [self keytoolBar_done_clicked];

    UISwitch *swtch = (UISwitch *)sender;
    if (swtch.on)
    {
        isAgent = TRUE;
    }
    else
    {
        isAgent = FALSE;
    }
    [propertyInfoDictionary setObject:[NSNumber numberWithBool:isAgent] forKey:@"agentManage"];
    
    [tblView reloadData];
}


#pragma mark - Table  viewForHeaderInSection

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView*sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0,320,44)] autorelease];
    
    UIImageView*cell_section_bg_imageView = [[[UIImageView alloc] init] autorelease];
    
    
    cell_section_bg_imageView.frame = CGRectMake(0, 0,320,44);
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 ||  [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            cell_section_bg_imageView.frame = CGRectMake(0, 0,768,44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 ||  [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            cell_section_bg_imageView.frame = CGRectMake(0, 0,1024,44);
        }
        
    }
    
    
    cell_section_bg_imageView.image = [UIImage imageNamed:@"section_header_bg.png"];
    [sectionView addSubview:cell_section_bg_imageView];
    
    UIImageView*cell_section_imageView = [[[UIImageView alloc] init] autorelease];
    cell_section_imageView.frame = CGRectMake(5,0,44,44);
    
    
    
    UILabel*cell_section_title_label = [[[UILabel alloc] init] autorelease];
    cell_section_title_label.font = [UIFont systemFontOfSize:15.0];
    
    
    cell_section_title_label.frame = CGRectMake(50,0,270,44);
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 ||  [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            cell_section_title_label.frame = CGRectMake(50,0,718,44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 ||  [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            cell_section_title_label.frame = CGRectMake(50,0,974,44);
        }
        
    }

    
    
    cell_section_title_label.backgroundColor = [UIColor clearColor];
    NSString*title_string = @"";
    switch (section)
    {
        case 0:
        {
            
            cell_section_imageView.image = [UIImage imageNamed:@"property_detail_address_icon.png"];
            title_string = @"Address";
            break;
        }
        case 1:
        {
            cell_section_imageView.image = [UIImage imageNamed:@"property_detail_property_value_icon.png"];
            title_string = @"Property Value";
            break;
        }
        case 2:
        {
            cell_section_imageView.image = [UIImage imageNamed:@"property_detail_loan_info_icon.png"];
            title_string = @"Loan Information";
            break;
        }
        case 3:
        {
            cell_section_imageView.image = [UIImage imageNamed:@"property_detail_lease_info_icon.png"];
            title_string = @"Lease Information";
            break;
        }
        case 4:
        {
            
            cell_section_imageView.image = [UIImage imageNamed:@"property_detail_agent_info_icon.png"];
            title_string = @"Agent Information";
            break;
        }
        case 5:
        {
            
            cell_section_imageView.image = [UIImage imageNamed:@"property_detail_tenant_info_icon.png"];
            title_string = @"Tenant Information";
            break;
        }

            
        default:
        {
            break;
        }
            
    }
    

    [sectionView addSubview:cell_section_imageView];
    
    cell_section_title_label.textColor = [UIColor colorWithRed:56.0/255.0 green:49.0/255.0 blue:49.0/255.0 alpha:1.0];
    cell_section_title_label.text = title_string;
    [sectionView addSubview:cell_section_title_label];
    
    UIButton*arrowButon = [UIButton buttonWithType:UIButtonTypeCustom];
    arrowButon.tag = section+1000;
    
    
    
   
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 48.0f;
}

#pragma mark - Table view data source

/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"Address";
    }
    else if (section == 1)
    {
        return @"Financials";
    }
    else  if(section == 2)
    {
        return @"Agent Information";
    }
    else  if(section == 3)
    {
        return @"Tenant Information";
    }
    return nil;
}
*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    //return 4;
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0)
    {
        return [propertyInfoArray count];
    }
    else if (section == 1)
    {
        return [financialArray count];
        
    }
    else if(section==2)
    {
        return [loanInformatio count];
        
        
        /*
        if (isFullyPaid)
        {
            
            return 1;
            
        }
        else
        {
             return [loanInformatio count];
        }
         */
        
       
    }
    else if(section==3)
    {
        if (isLease)
        {
            return [leaseInformation count];
        }
        else
        {
            return [leaseInformation count]-3;
            
        }

    }
    else if (section == 4)
    {
        if (isAgent)
        {
            return [agentInfoArray count];
        }
        
        else
        {
            return 1;
        }
        
    }
    else if(section==5)
    {
        return 1;
    }

    return 0;
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if(section == 0)
    {
        return 40;
    }
	else if(section == 5)
    {
        return 40;
    }
	else
    {
		return 30;
	}

	return 0;
}
*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
   
    if (indexPath.section == 2)
    {
        if (isFullyPaid)
        {
            
            if(indexPath.row !=0)
            {
                 return 0;
            }
            
            
        }
        
       else
        {
            return 44;
        }
        
    }
    
    
    
    if(indexPath.section == 0)
	{
		return 90;
	}
    else if(indexPath.section ==5)//Tenant information.
    {
        return 90;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
    
    nslog(@"\n propertyInfoDictionary = %@",propertyInfoDictionary);
    
    //keytoolBar.hidden = TRUE;
   
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        //[numberFormatter setCurrencyCode:appDelegate.curCode]; 
        numberFormatter.currencyCode = appDelegate.curCode;
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [numberFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        } 
        else if([[numberFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [numberFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [numberFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        
        
        
    }
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    
	
    if(indexPath.section == 0)
	{
		static NSString *CellIdentifier1 = @"Address";
		txtView = [[UITextView alloc]init];
		
        UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        cell1.backgroundColor = [UIColor clearColor];
        //if (cell1 == nil)
        if (1)
        {
            cell1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
			cell1.selectionStyle = UITableViewCellSelectionStyleNone;
            
			
           			
			txtView.font = [UIFont systemFontOfSize:15];
			txtView.tag = 3456;

            txtView.delegate = self;
            
            txtView.backgroundColor = [UIColor clearColor];
            
            [cell1.contentView addSubview:txtView];
			
        }
		
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                txtView.frame = CGRectMake(10, 5,550, 70);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                txtView.frame = CGRectMake(10, 5,850, 70);
            }
            
            
        }
        else
        {
            txtView.frame = CGRectMake(10, 5, 280, 70);
        }

        

        txtView.delegate = self;
		txtView.text = [propertyInfoDictionary objectForKey:@"address"];
		cell1.selectionStyle = UITableViewCellSelectionStyleNone;
		return cell1;
		
		
	}
    
    // financial information
    
    else if (indexPath.section == 1)
    {
        static NSString *CellIdentifier2 = @"Cell2";
        
        CustomCell2 *cell2 = (CustomCell2 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        //if (cell2 == nil) /*Will do in next version,as need to do major modification...*/
        if (1)
        {
            cell2 = [[[CustomCell2 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
                    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        
        
        cell2.detailLabel.hidden = TRUE;
        cell2.accessoryType = UITableViewCellAccessoryNone;
        [cell2.repaymentButton addTarget:self action:@selector(repayment_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell2.leaseDateButton addTarget:self action:@selector(date_changed:) forControlEvents:UIControlEventTouchUpInside];
        
        cell2.repaymentButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        cell2.leaseDateButton.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        
        nslog(@"propertyInfoDictionary ****** %@",propertyInfoDictionary);
        nslog(@"Switch value == ***** %d",[[propertyInfoDictionary objectForKey:@"fullypaid"]intValue]);
        [cell2.financialSwitch setOn:[[propertyInfoDictionary objectForKey:@"fullypaid"]intValue]];
        [cell2.leaseSwitch setOn:[[propertyInfoDictionary objectForKey:@"lease"]intValue]];
        
        cell2.financialLabel.hidden = FALSE;
        cell2.financialTextField.delegate = self;
        cell2.financialTextField.tag = 1008+indexPath.row;
        cell2.financialSwitch.hidden = TRUE;
        cell2.repaymentButton.hidden = TRUE;
        cell2.leaseSwitch.hidden = TRUE;
        cell2.leaseDateButton.hidden = TRUE;
        [cell2.leaseSwitch addTarget:self action:@selector(leaseSwitch_changed:) forControlEvents:UIControlEventValueChanged];
        [cell2.financialSwitch addTarget:self action:@selector(financialSwitch_chaged:) forControlEvents:UIControlEventValueChanged];
        
        [cell2.financialTextField setText:[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(8+indexPath.row)]]];
        
        cell2.financialLabel.text = [financialArray objectAtIndex:indexPath.row];

        
        if (indexPath.row == 0 || indexPath.row == 1)
        {
            if (indexPath.row == 0)
            {
                if ([[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(8+indexPath.row)]] length]>0)
                {
                    cell2.financialTextField.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(8+indexPath.row)]]floatValue]]];
                }
                cell2.financialTextField.placeholder = @"Purchase Price";
            }
            else if (indexPath.row == 1)
            {
                if ([[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(8+indexPath.row)]] length]>0)
                {
                    cell2.financialTextField.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(8+indexPath.row)]]floatValue]]];
                }
                cell2.financialTextField.placeholder = @"Market Value";
            }
            cell2.financialTextField.hidden = FALSE;
            cell2.financialTextField.keyboardType = UIKeyboardTypeNumberPad;
        }
        
                
                
        // Configure the cell...
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell2;
    }
    else if(indexPath.section==2)
    {
         static NSString *CellIdentifier2 = @"Cell2";
        CustomCell2 *cell2 = (CustomCell2 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        //if (cell2 == nil)/*Will do in next version,as need to do major modification...*/
        //if (1)
        {
            cell2 = [[[CustomCell2 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell2.detailLabel.hidden = TRUE;
        cell2.accessoryType = UITableViewCellAccessoryNone;
        [cell2.repaymentButton addTarget:self action:@selector(repayment_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell2.leaseDateButton addTarget:self action:@selector(date_changed:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cell2.leaseDateButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        cell2.repaymentButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        nslog(@"propertyInfoDictionary ****** %@",propertyInfoDictionary);
        nslog(@"Switch value == ***** %d",[[propertyInfoDictionary objectForKey:@"fullypaid"]intValue]);
        [cell2.financialSwitch setOn:[[propertyInfoDictionary objectForKey:@"fullypaid"]intValue]];
        [cell2.leaseSwitch setOn:[[propertyInfoDictionary objectForKey:@"lease"]intValue]];
        
        
        cell2.leaseSwitch.onTintColor = [UIColor colorWithRed:10.0f/255.0f green:142.0f/255.0f blue:184.0f/255.0f alpha:1.0];
        
        
        cell2.financialLabel.hidden = FALSE;
        cell2.financialTextField.delegate = self;
        cell2.financialTextField.tag = 1010+indexPath.row;
        cell2.financialSwitch.hidden = TRUE;
        cell2.repaymentButton.hidden = TRUE;
        cell2.leaseSwitch.hidden = TRUE;
        cell2.leaseDateButton.hidden = TRUE;
        [cell2.leaseSwitch addTarget:self action:@selector(leaseSwitch_changed:) forControlEvents:UIControlEventValueChanged];
        [cell2.financialSwitch addTarget:self action:@selector(financialSwitch_chaged:) forControlEvents:UIControlEventValueChanged];
        
        [cell2.financialTextField setText:[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(10+indexPath.row)]]];
        
        cell2.financialLabel.text = [loanInformatio objectAtIndex:indexPath.row];
        
        
        if (indexPath.row == 0)
        {
            cell2.financialTextField.hidden = TRUE;
            cell2.financialSwitch.hidden = FALSE;
        }
        
        if (indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3|| indexPath.row == 4|| indexPath.row == 5)
        {
            if (indexPath.row == 5)//first it was 4
            {
                if ([[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(10+indexPath.row)]] length]>0)
                {
                    cell2.financialTextField.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(10+indexPath.row)]]floatValue]]];
                }
                cell2.financialTextField.placeholder = @"Repayment Amount";
                cell2.financialTextField.keyboardType = UIKeyboardTypeNumberPad;
                
            }
            else if (indexPath.row == 1)
            {
                cell2.financialTextField.placeholder = @"Mortgage Bank";
            }
            else if(indexPath.row == 2)
            {
                
                
                if ([[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(10+indexPath.row)]] length]>0)
                {
                    cell2.financialTextField.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(10+indexPath.row)]]floatValue]]];
                }
                
                
                //cell2.financialTextField.placeholder = @"Total Loan Borrowed";
                cell2.financialTextField.placeholder = @"Loan Borrowed";
                cell2.financialTextField.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if(indexPath.row == 3)
            {
                
                if ([[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(10+indexPath.row)]] length]>0)
                {
                    cell2.financialTextField.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",(10+indexPath.row)]]floatValue]]];
                }
                
                cell2.financialTextField.placeholder = @"Loan Out Standing";
                cell2.financialTextField.keyboardType = UIKeyboardTypeNumberPad;
            }
            
            else if(indexPath.row == 4)
            {
                cell2.financialTextField.placeholder = @"Loan Terms(years)";
                cell2.financialTextField.keyboardType = UIKeyboardTypeNumberPad;
            }
            
            
            
            
            if (isFullyPaid)
            {
                cell2.repaymentButton.hidden = TRUE;
                cell2.financialLabel.hidden = TRUE;
                cell2.financialTextField.hidden = TRUE;
                
            }
            else if (!isFullyPaid)
            {
                cell2.repaymentButton.hidden = TRUE;
                cell2.financialLabel.hidden = FALSE;
                cell2.financialTextField.hidden = FALSE;
                
            }
        }
        
        
        else if (indexPath.row == 6)
        {
            if (isFullyPaid)
            {
                cell2.repaymentButton.hidden = TRUE;
                cell2.financialLabel.hidden = TRUE;
                cell2.financialTextField.hidden = TRUE;
            }
            else
            {
                cell2.repaymentButton.hidden = FALSE;
                cell2.repaymentButton.tag = 102;
                [cell2.repaymentButton setTitle:[propertyInfoDictionary objectForKey:@"repayment"] forState:UIControlStateNormal];
                cell2.financialLabel.hidden = FALSE;
                cell2.financialTextField.hidden = TRUE;
                
            }
            cell2.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        
        
        
        else if (indexPath.row == 7)
        {
            if (isFullyPaid)
            {
                cell2.repaymentButton.hidden = TRUE;
                cell2.financialLabel.hidden = TRUE;
                cell2.financialTextField.hidden = TRUE;
            }
            else
            {
                cell2.repaymentButton.hidden = FALSE;
                cell2.repaymentButton.tag = 103;
                [cell2.repaymentButton setTitle:[propertyInfoDictionary objectForKey:@"repayment_term"] forState:UIControlStateNormal];
                 [cell2.repaymentButton addTarget:self action:@selector(repayment_term_clicked:) forControlEvents:UIControlEventTouchUpInside];
                cell2.financialLabel.hidden = FALSE;
                cell2.financialTextField.hidden = TRUE;
                
            }
            cell2.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }

        
                
        
        
        
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
       
        return cell2;
    }
    
    else if(indexPath.section==3)
    {
        
        
        static NSString *CellIdentifier2 = @"Cell2";
        CustomCell2 *cell2 = (CustomCell2 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        //if (cell2 == nil)/*Will do in next version,as need to do major modification...*/
        if (1)
        {
            cell2 = [[[CustomCell2 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell2.detailLabel.hidden = TRUE;
        cell2.accessoryType = UITableViewCellAccessoryNone;
        [cell2.repaymentButton addTarget:self action:@selector(repayment_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell2.leaseDateButton addTarget:self action:@selector(date_changed:) forControlEvents:UIControlEventTouchUpInside];
        cell2.leaseDateButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        
        [cell2.financialSwitch setOn:[[propertyInfoDictionary objectForKey:@"fullypaid"]intValue]];
        [cell2.leaseSwitch setOn:[[propertyInfoDictionary objectForKey:@"lease"]intValue]];
        
        
        
        
        cell2.financialSwitch.onTintColor = [UIColor colorWithRed:10.0f/255.0f green:142.0f/255.0f blue:184.0f/255.0f alpha:1.0];
        cell2.leaseSwitch.onTintColor = [UIColor colorWithRed:10.0f/255.0f green:142.0f/255.0f blue:184.0f/255.0f alpha:1.0];
        
        
        
        cell2.financialLabel.hidden = FALSE;
        cell2.financialTextField.delegate = self;
        cell2.financialTextField.tag = 1017+indexPath.row;
        cell2.financialSwitch.hidden = TRUE;
        cell2.repaymentButton.hidden = TRUE;
        cell2.leaseSwitch.hidden = TRUE;
        cell2.leaseDateButton.hidden = TRUE;
        cell2.leaseDateButton.tag = 200;
        [cell2.leaseSwitch addTarget:self action:@selector(leaseSwitch_changed:) forControlEvents:UIControlEventValueChanged];
        [cell2.financialSwitch addTarget:self action:@selector(financialSwitch_chaged:) forControlEvents:UIControlEventValueChanged];
        
        
        
        
        cell2.financialLabel.text = [leaseInformation objectAtIndex:indexPath.row];
        

        cell2.financialTextField.hidden = FALSE;
        cell2.leaseSwitch.hidden = TRUE;
        
        
        
        if (indexPath.row == 0)
        {
            cell2.financialTextField.hidden = TRUE;
            cell2.leaseSwitch.hidden = FALSE;
        }
        
        else if(indexPath.row == 1)
        {
            nslog(@"\n  number = %@",[propertyInfoDictionary objectForKey:@"rental_income"]);
            
            if ([[propertyInfoDictionary objectForKey:@"rental_income"] length]>0)
            {
                cell2.financialTextField.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[propertyInfoDictionary objectForKey:@"rental_income"]floatValue]]];
            }
            else
            {
                cell2.financialTextField.text = @"";
            }
            cell2.financialTextField.placeholder = @"Rental Income";
            cell2.financialTextField.keyboardType = UIKeyboardTypeNumberPad;
            
        }
        
        else if (indexPath.row == 2)
        {
            
            cell2.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            if ([[propertyInfoDictionary objectForKey:@"leasestart"] length]>0)
            {
                
                
                
                
                
                NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *date = [dtFormatter dateFromString:[propertyInfoDictionary objectForKey:@"leasestart"]];
                
                nslog(@"\n date in cell = %@",date);
                if(date == nil)
                {
                    [cell2.leaseDateButton setTitle:[NSString stringWithFormat:@"%@",[propertyInfoDictionary objectForKey:@"leasestart"]]
                                           forState:UIControlStateNormal];
                }
                else
                {
                    
                    [cell2.leaseDateButton setTitle:[NSString stringWithFormat:@"%@",[appDelegate.regionDateFormatter stringFromDate:date]]
                                           forState:UIControlStateNormal];
                    
                }
                
                
                [dtFormatter release];
                
                
            }
            
            
            
            
            
            cell2.financialTextField.hidden = TRUE;
            cell2.leaseDateButton.hidden = FALSE;
            
        }
        else if (indexPath.row == 3)
        {
            cell2.leaseDateButton.tag = 201;
            cell2.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            if ([[propertyInfoDictionary objectForKey:@"leaseend"] length]>0)
            {
                
                
                
                
                nslog(@"\n leaseend = %@",[propertyInfoDictionary objectForKey:@"leaseend"]);
                NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
                [dtFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *date = [dtFormatter dateFromString:[propertyInfoDictionary objectForKey:@"leaseend"]];
                nslog(@"\n date using dateFormatter = %@",date);
                
                if(date == nil)
                {
                    [cell2.leaseDateButton setTitle:[NSString stringWithFormat:@"%@",[propertyInfoDictionary objectForKey:@"leaseend"]] forState:UIControlStateNormal];
                }
                else
                {
                    
                    [cell2.leaseDateButton setTitle:[NSString stringWithFormat:@"%@",[appDelegate.regionDateFormatter stringFromDate:date]]
                                           forState:UIControlStateNormal];
                    
                }
                
                
                
                
                
                [dtFormatter release];
                
                
            }
            
            /*
             [cell2.leaseDateButton setTitle:[propertyInfoDictionary objectForKey:@"leaseend"] forState:UIControlStateNormal];
             */
            cell2.financialTextField.hidden = TRUE;
            cell2.leaseDateButton.hidden = FALSE;
        }
        
        
        
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell2;
    }
    
    
    /// agent information
    
   else if (indexPath.section == 4)
    {
        static NSString *CellIdentifier1 = @"Cell1";
        
        CustomCell1 *cell1 = (CustomCell1 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell1 == nil) 
        {
            cell1 = [[[CustomCell1 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
            cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        
                
        cell1.accessoryType = UITableViewCellAccessoryNone;
    
        cell1.agentTextField.delegate = self;
        cell1.agentTextField.hidden = TRUE;
        cell1.agentDateButton.hidden = TRUE;
        cell1.agentManageSwitch.hidden = FALSE;
        cell1.agentLabel.text = [agentInfoArray objectAtIndex:indexPath.row];
        cell1.agentDetailLabel.hidden = TRUE;                         
        [cell1.agentManageSwitch addTarget:self action:@selector(agentManageSwitch_changed:) forControlEvents:UIControlEventValueChanged];
        [cell1.agentManageSwitch setOn:[[propertyInfoDictionary objectForKey:@"agentManage"]intValue]];
        if (indexPath.row == 1)
        {
            cell1.agentManageSwitch.hidden = TRUE;
            cell1.agentDetailLabel.hidden = FALSE;
            cell1.agentDetailLabel.textAlignment = UITextAlignmentRight;

            cell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            if ([appDelegate.agentArray count]>0)
            {
                cell1.agentDetailLabel.textColor = [UIColor blackColor];
                cell1.agentDetailLabel.text= appDelegate.agentTypeStr;
                [propertyInfoDictionary setObject:appDelegate.agentTypeStr forKey:@"agenttype"];
                [propertyInfoDictionary setObject:appDelegate.agent_pk forKey:@"agent_pk"];
                
                
                
            }
            else
            {
                cell1.agentDetailLabel.textColor = [UIColor grayColor];
                cell1.agentDetailLabel.text = @"Property Agent";
            }
        }
        
        // Configure the cell...
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell1;
    }
    
    
    
    else if(indexPath.section == 5)//Tenant information.
	{
		static NSString *CellIdentifier1 = @"tenant_information";
		txtView = [[UITextView alloc]init];
		
        UITableViewCell *cell1  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
       
        if (cell1==nil)
        {
            cell1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
			cell1.selectionStyle = UITableViewCellSelectionStyleNone;
			
			/*
             Sun:0004
             ipad pending..
            */
            txtView.font = [UIFont systemFontOfSize:15];
			txtView.tag = 3457;
            
            txtView.delegate = self;
            
            txtView.backgroundColor = [UIColor clearColor];
			[cell1.contentView addSubview:txtView];
        }
		else
        {
			txtView = (UITextView *)[cell1.contentView viewWithTag:3456];
		}
        
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                txtView.frame = CGRectMake(10, 5,550, 70);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                txtView.frame = CGRectMake(10, 5,850, 70);
            }
            
            
        }
        else
        {
            txtView.frame = CGRectMake(10, 5, 280, 70);
        }
        

        
        txtView.delegate = self;
		txtView.text = [propertyInfoDictionary objectForKey:@"tenant_information"];
		cell1.selectionStyle = UITableViewCellSelectionStyleNone;
		return cell1;
		
		
	}
    
    
    return nil;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (indexPath.section == 4)
    {
        if (indexPath.row == 1)
        {
            keytoolBar.hidden = TRUE;
            toolBar.hidden = TRUE;
            
            appDelegate.isFromPropertyAgent = TRUE;
            agentInformationViewController *agentInformation = [[agentInformationViewController alloc]initWithNibName:@"agentInformationViewController" bundle:nil];
            [self.navigationController pushViewController:agentInformation animated:YES];
            [agentInformation release];
        }
    }
}



-(IBAction)property_select
{

    keytoolBar.hidden = TRUE;
    toolBar.hidden = TRUE;
    
    appDelegate.fromProperty = TRUE;
    propertyTypeViewController *propertyTypeView = [[propertyTypeViewController alloc]initWithNibName:@"propertyTypeViewController" bundle:nil];
    appDelegate.fromProperty = TRUE;
    [self.navigationController pushViewController:propertyTypeView animated:YES];
    [propertyTypeView release];

}


#pragma mark text view method
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    keytoolBar.hidden = FALSE;
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    
    keytoolBar.hidden = FALSE;
    textView.delegate = self;

    nslog(@"\n y = %f",keytoolBar.frame.origin.y);
    
    datePicker.hidden = TRUE;
    pickerView.hidden = TRUE;
    toolBar.hidden = TRUE;
    
    
    
    
    
   
    
    if(appDelegate.isIPad)
    {
        
        
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                keytoolBar.frame = CGRectMake(0,671,768,44);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                keytoolBar.frame = CGRectMake(0,327,1024,44);
            }

            
            
       
        
        
        
        
    }
    else
    {
        if(appDelegate.isIphone5)
        {
            keytoolBar.frame = CGRectMake(toolBar.frame.origin.x,244,toolBar.frame.size.width, toolBar.frame.size.height);
        }
        else
            
        {
            keytoolBar.frame = CGRectMake(toolBar.frame.origin.x,156,toolBar.frame.size.width, toolBar.frame.size.height);
        }
    }
   
    textViewIndex = textView.tag;
    
    if(textViewIndex==3457)
    {
        
        if(!isFullyPaid && isLease && isAgent)
        {
            [tblView setContentOffset:CGPointMake(0,1150)];
        }
        else if(isLease && isAgent)
        {
            [tblView setContentOffset:CGPointMake(0,920)];
        }
        else if(!isFullyPaid && isAgent)
        {
            [tblView setContentOffset:CGPointMake(0,950)];
        }
        else if(!isFullyPaid && isLease)
        {
            [tblView setContentOffset:CGPointMake(0,1000)];
        }

        else if(!isFullyPaid )
        {
            [tblView setContentOffset:CGPointMake(0,920)];
        }

        else if(isLease)
        {
            [tblView setContentOffset:CGPointMake(0,880)];
        }
        else
        {
            [tblView setContentOffset:CGPointMake(0,780)];
        }
        
        
        
        //NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:0 inSection:3];
        //[tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else
    {
        NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:0 inSection:0];
        [tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
    [self scrollViewToTextView:textView];
    
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    //keytoolBar.hidden = TRUE;
    
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{

    if(appDelegate.isIPad)
    {
        keytoolBar.hidden = TRUE;
    }
    textViewIndex = 0;
    
    if(textView.tag == 3456)
    {
        [propertyInfoDictionary setObject:textView.text forKey:@"address"];
    }
    else if(textView.tag == 3457)
    {
        [propertyInfoDictionary setObject:textView.text forKey:@"tenant_information"];
    }
    
    
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    nslog(@"\n textView.text = %@",textView.text);
    nslog(@"\n replacement = %@",text);
    
    
    if(textView.tag ==  3457)//tenant information
    {
        NSString*tempString = [NSString stringWithFormat:@"%@%@",textView.text,text];
        [propertyInfoDictionary setObject:tempString forKey:@"tenant_information"];
    }
    else//3456(address)
    {
        NSString*tempString = [NSString stringWithFormat:@"%@%@",textView.text,text];
        [propertyInfoDictionary setObject:tempString forKey:@"address"];
    }
    
     return TRUE;
}

- (void)textViewDidChangeSelection:(UITextView *)textView
{
    nslog(@"\n textViewDidChangeSelection");
}

#pragma mark textfield method
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    
    
    
     nslog(@"text field should being editing tag....%d",textField.tag);
    
    keytoolBar.hidden = FALSE;
    toolBar.hidden = TRUE;
   
    datePicker.hidden = TRUE;
    pickerView.hidden = TRUE;
    
    if(appDelegate.isIPad)
    {
        
        
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                keytoolBar.frame = CGRectMake(0,671,768,44);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                keytoolBar.frame = CGRectMake(0,327,1024,44);
            }

       
        
        
        
        
    }
    else
    {
        
    }
    
    
    tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
    keytoolBar.hidden = FALSE;
    textFieldIndex = textField.tag;

	if (textField.tag >1007 && textField.tag < 1010)
    {
        
       
        NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:textField.tag-1008 inSection:1];
        [tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
     else if (textField.tag >1010 && textField.tag < 1012)
     {
         if(appDelegate.isIPad)
         {
             
             if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
             {
                 [tblView setContentOffset:CGPointMake(0,260)];
                                 
             }
         }
         else
         {
             NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:textField.tag-1010 inSection:2];
             [tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
         }
         

         
     }
    else if(textField.tag == 1012)
    {
        nslog(@"\n tblView.contentOffset.y = %f",tblView.contentOffset.y);
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                [tblView setContentOffset:CGPointMake(0,260)];
                               
            }
        }
        else
        {    
           
            if(tblView.contentOffset.y<366)
            {
               [tblView setContentOffset:CGPointMake(0,366)];     
            }
            
        }
        

    }
    else if(textField.tag == 1013|| textField.tag == 1014 ||textField.tag == 1015)
    {
        
        /*Need to change this condition...*/
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                [tblView setContentOffset:CGPointMake(0,260)];
                
            }
        }
        else
        {
            nslog(@"\n tblView.contentOffset.y = %f",tblView.contentOffset.y);
            //if(tblView.contentOffset.y<350)
            {
                [tblView setContentOffset:CGPointMake(0,600)];
            }
            
        }

        
        
    }
    
    
    else if(textField.tag == 1018)
    {
        
        if(!appDelegate.isIPad)
        {
            if(isFullyPaid == YES)
            {
                tblView.contentOffset = CGPointMake(0,600);
            }
            else
            {
                tblView.contentOffset = CGPointMake(0,850);
            }
        }
        
        

    }
    [self scrollViewToTextField:textField];
   
    
    
    nslog(@"\n toolBar = %f",toolBar.frame.origin.y);
    nslog(@"\n keytoolbar = %f",keytoolBar.frame.origin.y);
    
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    nslog(@"\n textFieldDidEndEditing");
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];


    if (textField.tag == textFieldIndex)
    {
        textFieldIndex = -1;
    }
    
    
    
    if(textField.tag == 1008 || textField.tag == 1009 || textField.tag == 1012 ||  textField.tag == 1013 || textField.tag == 1015 || textField.tag == 1018)
	{
        
        
        
        
        
        textField.text = [textField.text stringByReplacingOccurrencesOfString:@"%20" withString:@""];
        
        
        //NSString *amountStr = textField.text;
        
        NSString *textFieldStr = [NSString stringWithFormat:@"%@",textField.text];
        
        NSMutableString *amountStr = [NSMutableString stringWithString:textFieldStr];
        
        //NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        
        
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        
        
        
        
        [amountStr replaceOccurrencesOfString:numberFormatter.currencySymbol
                                   withString:@""
                                      options:NSLiteralSearch
                                        range:NSMakeRange(0, [amountStr length])];
        
        [amountStr replaceOccurrencesOfString:numberFormatter.groupingSeparator
                                   withString:@""
                                      options:NSLiteralSearch
                                        range:NSMakeRange(0, [amountStr length])];
        
        [amountStr replaceOccurrencesOfString:numberFormatter.decimalSeparator
                                   withString:@"."
                                      options:NSLiteralSearch
                                        range:NSMakeRange(0, [amountStr length])];
        
        
        
        
        NSLog(@"\n textFieldStrValue = %@",amountStr);
        
        
        
        
        
        
        
        
        /*
        
        NSString *amountStr = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet symbolCharacterSet]];
        amountStr = [amountStr stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
        
        amountStr = [amountStr stringByReplacingOccurrencesOfString:@"[^A-Z]" withString:@""];
        amountStr = [amountStr stringByReplacingOccurrencesOfString:@"[^a-z]" withString:@""];
        amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        
        
        
        
        NSString *reqSysVer = @"7.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        nslog(@"\n currSysVer = %@",currSysVer);
        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        {
            
            
            if([[numberFormatter locale].localeIdentifier isEqualToString:@"pt_PT"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_BR"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_AO"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_CV"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_GW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_ST"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_TL"] ||
               [[numberFormatter locale].localeIdentifier isEqualToString:@"bg_BG"]
               
                )
            {
                
                amountStr = [amountStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"€0123456789,."] invertedSet];
                amountStr = [[amountStr componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
                
            }
        }

        
        
        
        
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        
        
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        
        
        
        
        NSMutableString *reversedStr;
        int len = [amountStr length];
        reversedStr = [NSMutableString stringWithCapacity:len];
        
        while (len > 0)
        {
            [reversedStr appendString:[NSString stringWithFormat:@"%C", [amountStr characterAtIndex:--len]]];
        }
        
        nslog(@"\n reversedStr = %@",reversedStr);
        
        NSRange d = [reversedStr rangeOfString:@","];//for example 3,50(normal 3.50)
        
        
        
        nslog(@"\n d = %d",d.location);
        
        
        
        if(appDelegate.isIOS7)
        {
            
            
            if ([amountStr rangeOfString:@" "].location != NSNotFound || d.location == 3)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
        }

        
        
        
        
        if ( d.location == 2)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }

        
        
        if ([amountStr rangeOfString:@"'"].location != NSNotFound)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"'" withString:@""];
        }
        
        
        
        
        
        amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([appDelegate.curCode isEqualToString:@"ZAR"])
        {
            if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            else
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@""];
            }
            
        }
        
       
        
        else if([[numberFormatter locale].localeIdentifier isEqualToString:@"en_BE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_BE"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_AW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_CW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_NL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_SX"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"id_ID"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_AO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_BR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_GW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_PT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_ST"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_AT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_DE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_LU"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_CY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_GR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"da_DK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"it_IT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_MD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_RO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_AR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_BO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_EC"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_GQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_PY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_UY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_VE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"seh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sg_CF"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rn_BI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mua_CM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ms_BN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mgh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mk_MK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"lu_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rw_RW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kl_GL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kea_CV"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"is_IS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ka_GE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"gl_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"fo_FO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"swc_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ca_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"bs_BA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"eu_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az-Cyrl_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sq_AL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MA"])
        {
            nslog(@"Str_budget -- %@",amountStr);
            
           
            NSString *stringToFilter =[NSString stringWithFormat:@"%@",amountStr];
            NSMutableString *targetString = [NSMutableString string];
            //set of characters which are required in the string......
            NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"."];
            nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
            
            for(int i = 0; i < [stringToFilter length]; i++)
            {
                unichar currentChar = [stringToFilter characterAtIndex:i];
                nslog(@"currentChar -- %C",currentChar);
                
                if(i == [stringToFilter length]-3)
                {
                    nslog(@"currentChar -- %C",currentChar);
                    [targetString appendFormat:@"%C", currentChar];
                }
                else
                {
                    if([okCharacterSet characterIsMember:currentChar])
                    {
                        [targetString appendFormat:@""];
                    }
                    else
                    {
                        [targetString appendFormat:@"%C", currentChar];
                    }
                }
            }
            nslog(@"targetString -- %@",targetString);
            amountStr = [NSString stringWithFormat:@"%@",targetString];
            nslog(@"Str_budget -- %@",amountStr);
        }
        
        else if([[numberFormatter locale].localeIdentifier isEqualToString:@"gsw_CH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rm_CH"])
        {
            nslog(@"Str_budget -- %@",amountStr);
            
                        NSString *stringToFilter =[NSString stringWithFormat:@"%@",amountStr];
            NSMutableString *targetString = [NSMutableString string];
            //set of characters which are required in the string......
            NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
            
            for(int i = 0; i < [stringToFilter length]; i++)
            {
                unichar currentChar = [stringToFilter characterAtIndex:i];
                nslog(@"currentChar -- %C",currentChar);
                
                if(i == [stringToFilter length]-3)
                {
                    nslog(@"currentChar -- %C",currentChar);
                    [targetString appendFormat:@"%C", currentChar];
                }
                else
                {
                    if([okCharacterSet characterIsMember:currentChar])
                    {
                        [targetString appendFormat:@"%C", currentChar];
                    }
                    else
                    {
                        [targetString appendFormat:@""];
                    }
                }
            }
            nslog(@"targetString -- %@",targetString);
            amountStr = [NSString stringWithFormat:@"%@",targetString];
            nslog(@"Str_budget -- %@",amountStr);
        }
        else if([[numberFormatter locale].localeIdentifier isEqualToString:@"ar_IQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_BH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_TD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DJ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_ER"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_JO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LB"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_OM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_PS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_QA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_AE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_001"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_YE"])
        {
            nslog(@"Str_budget -- %@",amountStr);
            NSString *stringToFilter =[NSString stringWithFormat:@"%@",amountStr];
            NSMutableString *targetString = [NSMutableString string];
           
            nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
            
            for(int i = 0; i < [stringToFilter length]; i++)
            {
                unichar currentChar = [stringToFilter characterAtIndex:i];
                nslog(@"currentChar -- %C",currentChar);
                
                if(i == [stringToFilter length]-3)
                {
                    nslog(@"currentChar -- %C",currentChar);
                    [targetString appendFormat:@"."];
                }
                else
                {
                    [targetString appendFormat:@"%C", currentChar];
                }
            }
            nslog(@"targetString -- %@",targetString);
            amountStr = [NSString stringWithFormat:@"%@",targetString];
            nslog(@"Str_budget -- %@",amountStr);
        }
        
        
        
        
        
        else
        {
            
            if ([appDelegate.curCode isEqualToString:@"Default Region Currency"])
            {
                
                
                if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
                {
                    amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
                }
                else
                {
                    amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@""];
                }
                
                
            }
            else
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@""];
            }
        }
        
        */
        
        
        
        
        if(textField.tag == 1018)
        {
            [propertyInfoDictionary setObject:amountStr forKey:@"rental_income"];
        }
        else
        {
           [propertyInfoDictionary setObject:amountStr forKey:[NSString stringWithFormat:@"%d",textField.tag%1000]]; 
        }
        
        
        nslog(@"proprty info dictionary at should end editing.....%@",propertyInfoDictionary);
        //return YES;
        
    }
    else
    {
        [propertyInfoDictionary setObject:textField.text forKey:[NSString stringWithFormat:@"%d",textField.tag%1000]];
        
    }
    
    nslog(@"proprty info dictionary at should end editing.....%@",propertyInfoDictionary);
    [numberFormatter release];
    if(appDelegate.isIPad)
    {
        keytoolBar.hidden = TRUE;
        [textField resignFirstResponder];
    }


    
    
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
        
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    keytoolBar.hidden = FALSE;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    
	if (textField.tag >1007 && textField.tag < 1009)
    {
        UITextField *txt = (UITextField *)[self.view viewWithTag:textField.tag+1];
         keytoolBar.hidden = NO;
        [txt becomeFirstResponder];
        return NO;
    }
    else if (textField.tag >1010 && textField.tag < 1012)
    {
        UITextField *txt = (UITextField *)[self.view viewWithTag:textField.tag+1];
        keytoolBar.hidden = NO;
        [txt becomeFirstResponder];
         return NO;
    }

    else
    {

        [textField resignFirstResponder];
    }
    keytoolBar.hidden = YES;
     
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{// return NO to not change text
	
    
    //if(textField.tag == 1008 || textField.tag == 1009 || textField.tag == 1012)
    if(textField.tag == 1008 || textField.tag == 1009 ||textField.tag == 1012 || textField.tag == 1013 || textField.tag == 1015 || textField.tag == 1018)
	{
        
        if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
        {
        
            [appDelegate selectCurrencyIndex];
        }
        
        if(appDelegate.isIPad)
        {
            if([string length]!=0)
            {
                NSMutableString *strippedString = [NSMutableString
                                                   stringWithCapacity:string.length];
                NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
                NSScanner *scanner = [NSScanner scannerWithString:string];
                
                
                while ([scanner isAtEnd] == NO)
                {
                    NSString *buffer;
                    if ([scanner scanCharactersFromSet:numbers intoString:&buffer])
                    {
                        [strippedString appendString:buffer];
                        
                    } else
                    {
                        [scanner setScanLocation:([scanner scanLocation] + 1)];
                    }
                }
                
                nslog(@"strippedString  = %@", strippedString); // "123123123"
                if([strippedString length]==0)
                {
                    return NO;
                }
                string = strippedString;
                
            }
            
        }

        
        
        
        NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
        
        
        
        if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
        {
            [currencyFormatter setCurrencyCode:appDelegate.curCode];
            
            
            if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
            {
				[currencyFormatter setCurrencyCode:@"USD"];
                nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                [currencyFormatter setLocale:locale];
                [locale release];
                
            }  
            else if([[currencyFormatter currencyCode] isEqualToString:@"CNY"])
            {
                [currencyFormatter setCurrencyCode:@"CNY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                [currencyFormatter setLocale:locale];
                [locale release];
                
            }
            else if([[currencyFormatter currencyCode] isEqualToString:@"JPY"])
            {
                [currencyFormatter setCurrencyCode:@"JPY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                [currencyFormatter setLocale:locale];
                [locale release];
                
            }
            
            
            
        }
        [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        
        
        int currencyScale = [currencyFormatter maximumFractionDigits];
        appDelegate.decimalScale = [currencyFormatter maximumFractionDigits];
        [currencyFormatter release];
        
        nslog(@"\n scale = %d",currencyScale);
        
        if(currencyScale == 0)
        {
            
            NSString *cleanCentString = [[textField.text
                                          componentsSeparatedByCharactersInSet:
                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                         componentsJoinedByString:@""];
            
            nslog(@"\n cleanCentString = %@",cleanCentString);
            // Parse final integer value
            nslog(@"\n textField.text = %@",textField.text);
            double centAmount = [cleanCentString doubleValue];
            // Check the user input
            nslog(@"\n centAmount = %f",centAmount);
            if (string.length > 0)
            {
                // Digit added
                centAmount = centAmount * 10 + [string doubleValue];
            }
            else
            {
                // Digit deleted
                centAmount = centAmount / 10;
            }
            // Update call amount value
            NSNumber *_amount;
            //_amount = [[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f];
            _amount = [[NSNumber alloc]initWithInt:(int)centAmount];
            
            
            NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            
            
            if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
            {
                [_currencyFormatter setCurrencyCode:appDelegate.curCode];
                
                
                if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
                {
                    [_currencyFormatter setCurrencyCode:@"USD"];
                    nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }  
                
                else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
                {
                    [_currencyFormatter setCurrencyCode:@"CNY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
                {
                    [_currencyFormatter setCurrencyCode:@"JPY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                
               
            }
            else
            {
                nslog(@"\n NSLocaleCurrencyCode = %@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]);
                [_currencyFormatter setCurrencyCode:[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
            }
            [_currencyFormatter setNegativeFormat:@"-¤#,##0.00"];
            
            nslog(@"\n _amount = %@",_amount);
            
           
            textField.text = [_currencyFormatter stringFromNumber:_amount];
            [_currencyFormatter release];
            [_amount release];
            // Since we already wrote our changes to the textfield
            // we don't want to change the textfield again
        }

        else
        {
            NSString *cleanCentString = [[textField.text
                                          componentsSeparatedByCharactersInSet:
                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                         componentsJoinedByString:@""];
            // Parse final integer value
            NSLog(@"\n textField.text = %@",textField.text);
            NSLog(@"\n cleanCentString = %@",cleanCentString);
            
            double centAmount = [cleanCentString doubleValue];
            // Check the user input
            if (string.length > 0)
            {
                // Digit added
                centAmount = centAmount * 10 + [string doubleValue];
            }
            else
            {
                // Digit deleted
                centAmount = centAmount / 10;
            }
            // Update call amount value
            NSNumber *_amount;
            _amount = [[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f];
            //    _amount = [[NSNumber alloc] initWithFloat:(float)centAmount / 100.0f];
            // Write amount with currency symbols to the textfield
            NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
            {
                [_currencyFormatter setCurrencyCode:appDelegate.curCode];
                
                
                if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
                {
                    [_currencyFormatter setCurrencyCode:@"USD"];
                    nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                    
                }  
                
                else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
                {
                    [_currencyFormatter setCurrencyCode:@"CNY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
                {
                    [_currencyFormatter setCurrencyCode:@"JPY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                
                
                
            }
            textField.text = [_currencyFormatter stringFromNumber:_amount];
            [_currencyFormatter release];
            [_amount release];
            // Since we already wrote our changes to the textfield
            // we don't want to change the textfield again

        }
            NSLog (@"\n Result: %@ \n", textField.text);
        
        
             return NO;
        }

	return TRUE;	
}


@end
