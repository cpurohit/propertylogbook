//
//  propertyTypeViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "propertyTypeViewController.h"
#import "PropertyLogBookAppDelegate.h"


@implementation propertyTypeViewController

//- (id)initWithStyle:(UITableViewStyle)style
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)dealloc
{
    [super dealloc];
    [tempRowArray release];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class])); 
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
          [tblView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
        
    }

    
    
    
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        
    }

    
    
    textFieldIndex = -1;
    self.navigationItem.title = @"PROPERTY TYPE";
    
    
    
    
    //[appDelegate selectPropertyType:[appDelegate getDBPath]];
    
    [appDelegate selectPropertyType];
    
    
    if (!appDelegate.fromProperty)
    {
        tblView.editing = TRUE;
        
        
        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
        {
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
            UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator1.width = -12;
            
            [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
            
        }
        else
        {
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
            
        }

        
    }
    else
    {
        for (int i =0; i <[appDelegate.propertyTypeArray count];i++)
        {
            UITextField *text = (UITextField *)[self.view viewWithTag:1000+i];
            text.enabled = FALSE;
        }

    }
    
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
            
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }

    
    
    if(appDelegate.isIphone5)
    {
        keytoolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 244, self.view.frame.size.width, 44)];
    }
    else
    {
        keytoolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 156, self.view.frame.size.width, 44)];
    }
    
   
    
    if(appDelegate.isIPad)
    {
        
        
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                keytoolBar.frame = CGRectMake(0, 671,768, 44);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                keytoolBar.frame = CGRectMake(0,326,1024, 44);
            }

        
        
        
    }

    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(keytoolBar_cancel_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flexiItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(keytoolBar_done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(keytoolBar_done_clicked)];
     */
    
    [keytoolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexiItem,doneButton, nil]];
    //    toolBar.items = array;
    keytoolBar.barStyle = UIBarStyleBlack;
    [self.view addSubview:keytoolBar];
    keytoolBar.hidden = TRUE;
    
    [cancelBtn release];
    [flexiItem release];
    [doneButton release];  
    
   // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     //self.navigationItem.rightBarButtonItem = self.editButtonItem;
	tblView.backgroundColor = [UIColor clearColor];
	
    
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
	
}

-(void)keytoolBar_cancel_clicked
{
    keytoolBar.hidden = TRUE;
    UITextField *text = (UITextField *)[self.view viewWithTag:isClicked];
    nslog(@"\n textFieldTempText = %@",textFieldTempText);
    text.text =  textFieldTempText;
    [text resignFirstResponder];
    [self textFieldShouldReturn:text];
    
    
    
    
}
-(void)keytoolBar_done_clicked
{
    UITextField *text = (UITextField *)[self.view viewWithTag:isClicked];
    textFieldTempText = text.text;
    [text resignFirstResponder];
    [self textFieldShouldReturn:text];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
     isAddClicked = NO;
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    //[self.view addSubview:appDelegate.bottomView];
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    
	if([appDelegate.propertyTypeArray count]==0)
	{
        
        /*
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_clicked)] autorelease];
         */
        
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
		
        lblNoData.hidden = FALSE;
		lblPlus.hidden = FALSE;
        [imageView setHidden:FALSE];

        
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:1.0f];
        
                

        [tblView setBackgroundView:nil];
        [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
        
		[tblView setHidden:TRUE];
		//[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0,  1024,1024)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];

		
	}
	else 
    {
		[tblView setHidden:FALSE];
        [imageView setHidden:TRUE];
		lblNoData.hidden = TRUE;
		lblPlus.hidden = TRUE;
		[tblView reloadData];
		
	}

    if(appDelegate.isIPad)
    {
        
		[self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    	[self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
	}
    
	tempRowArray = [[NSMutableArray alloc]initWithArray:appDelegate.propertyTypeArray];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    if(appDelegate.isIPad)
    {
        if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
        {
            imageView.center = CGPointMake(385, 405);
            lblNoData.center = CGPointMake(384,self.view.frame.size.height/2);
            lblPlus.center = CGPointMake(284,466);
        }
        else  if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
        {
            imageView.center = CGPointMake(512,275);
            lblNoData.center = CGPointMake(512,self.view.frame.size.height/2);
            lblPlus.center = CGPointMake(410,338);
            
        }

    }
    
    
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(appDelegate.isIPad)
    {
        return  YES;
    }
    
    return NO;
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    if(appDelegate.isIPad)
    {
        
        [appDelegate manageViewControllerHeight];
        
        if(isAddClicked == FALSE)//alertview tag..
        {
            keytoolBar.hidden = TRUE;
        }
        
        
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            imageView.center = CGPointMake(385, 405);
            lblNoData.center = CGPointMake(384,self.view.frame.size.height/2);   
            lblPlus.center = CGPointMake(284,466); 
            
            
            
                keytoolBar.frame = CGRectMake(0, 671,768, 44);
            
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            imageView.center = CGPointMake(512,275);
            lblNoData.center = CGPointMake(512,self.view.frame.size.height/2);   
            lblPlus.center = CGPointMake(410,338); 
            
           
            
                keytoolBar.frame = CGRectMake(0,326,1024, 44);
            
            
            
            
        }
          
        
        
        [self.view endEditing:YES];
    }
    [tblView reloadData];
     
    
    
    
}

#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

#pragma mark -
-(IBAction)add_clicked
{
    isAddClicked = YES;
    nslog(@"\n textFieldIndex = %d",textFieldIndex);
       
    if(textFieldIndex != 5000 && textFieldIndex>0)//alert view tag..!
    {
        
        UITextField*textField = (UITextField*)[self.view viewWithTag:textFieldIndex];
        
        
        
        int index = textField.tag%1000;
        nslog(@"\n text while ad = %@",textField.text);
        
        [[appDelegate.propertyTypeArray objectAtIndex:index] setValue:textField.text forKey:@"propertyType"];
        
        
        for(int i=0;i<[appDelegate.propertyTypeArray count];i++)
        {
            [appDelegate updatePropertyType:[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"propertyType"] rowid:[[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        }
        
    }


    nslog(@"\n add_clicked = %@",appDelegate.propertyTypeArray);
    nslog(@"\n tempRowArray = %@",tempRowArray);
    
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Enter New Property Type" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];

    myAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    newType = [myAlertView textFieldAtIndex:0];
    
    
    //newType= [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    newType.borderStyle = UITextBorderStyleNone;
    
    [newType setKeyboardAppearance:UIKeyboardAppearanceDefault];
	
    newType.delegate = self;
    newType.tag = 5000;
	[newType becomeFirstResponder];
    [newType setBackgroundColor:[UIColor clearColor]];
	//[myAlertView addSubview:newType];
    //[myAlertView insertSubview:newType atIndex:0];

	[myAlertView show];
	[myAlertView release];
}

- (void) alertView:(UIAlertView *) actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex 
{
    isAddClicked = NO;
    
    
    keytoolBar.hidden = TRUE;
    
	if (buttonIndex == 1)
	{        
        if ([newType.text length]>0)
        {
            
            for (int i=0;i<[appDelegate.propertyTypeArray count];i++)
            {
                
                [appDelegate updatePropertyType:[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"propertyType"] rowid:[[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
            }
            
           
           
            [appDelegate selectPropertyType];
            [appDelegate addPropertyType:newType.text];
		
            
        [appDelegate selectPropertyType];
            
            
        [tblView setEditing:YES];
		[lblNoData setHidden:TRUE];
			lblPlus.hidden = TRUE;
            imageView.hidden = TRUE;
		[tblView setHidden:FALSE];
            
		[tblView reloadData];
            
            
            

            
            
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid property type" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            [alertView release];
        }
	}
	else 
    {
        if ([appDelegate.propertyTypeArray count] == 0)
        {
            [lblNoData setHidden:FALSE];
            lblPlus.hidden = FALSE;
                imageView.hidden = FALSE;
            [tblView setHidden:TRUE];
        }
        
        
        
        
        if(!appDelegate.isIPad)
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
            }
         
            
            
            
            
            

        }

        
        
        [tblView reloadData];
	}
    
    
   
    
}


-(void)cancel_clicked
{
    if (textFieldIndex > 0)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldIndex];
        [textField resignFirstResponder];
    }
    
    for (int i=0;i<[appDelegate.propertyTypeArray count];i++)
    {
        
        [appDelegate updatePropertyType:[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"propertyType"] rowid:[[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
    }
    [appDelegate selectPropertyType];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (!appDelegate.fromProperty)
    {
    return @"Note: 1st entry is default entry";
    }
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([appDelegate.propertyTypeArray count]>0)
    {
         nslog(@"\n count = %d",[appDelegate.propertyTypeArray count]);
        return [appDelegate.propertyTypeArray count];
    }
	return 0;
    //return 1;
}






- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITextField *propertyTypeTextField;
    
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (cell == nil) 
    if (1) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        propertyTypeTextField = [[UITextField alloc]initWithFrame:CGRectMake(20,12, 200, 31)];
        
        if(appDelegate.isIOS7)
        {
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        }

        
        if(appDelegate.isIPad)
        {
            
             if([UIApplication sharedApplication].statusBarOrientation == 1 ||[UIApplication sharedApplication].statusBarOrientation == 2)
             {
                 propertyTypeTextField.frame = CGRectMake(20, 9, 500, 31);
             }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 ||[UIApplication sharedApplication].statusBarOrientation == 4)
            {
                 propertyTypeTextField.frame = CGRectMake(20, 9, 700, 31);
            }
            
            
        }
        else
        {
             propertyTypeTextField.frame = CGRectMake(20,12, 200, 31);
        }

       
        
        propertyTypeTextField.delegate = self;
        propertyTypeTextField.borderStyle = UITextBorderStyleNone;
        propertyTypeTextField.tag = 1000+indexPath.row;
        propertyTypeTextField.enabled = FALSE;
        [cell.contentView addSubview:propertyTypeTextField];
    }
    else
    {
        propertyTypeTextField = (UITextField *)[cell.contentView viewWithTag:1000+indexPath.row];
    }
    if (!appDelegate.fromProperty)
    {
        propertyTypeTextField.enabled = TRUE;
    }
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    propertyTypeTextField.hidden = TRUE;    
    
    propertyTypeTextField.font =[UIFont systemFontOfSize:15.0];
    
    [cell.textLabel setFont:[UIFont systemFontOfSize:15.0]];
    // Configure the cell...
    if ([appDelegate.propertyTypeArray count]>0)
    {
        propertyTypeTextField.hidden = FALSE;    
        cell.textLabel.hidden = TRUE;
        [propertyTypeTextField setText:[[appDelegate.propertyTypeArray objectAtIndex:indexPath.row]valueForKey:@"propertyType"]];
        if (appDelegate.fromProperty)
        {
            if ([propertyTypeTextField.text isEqualToString:appDelegate.propertyTypeStr])
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                oldIndex = indexPath;
                
                [oldIndex retain];
            }
        }
    }
    //else
//    {
//        propertyTypeTextField.hidden = TRUE;
//        cell.textLabel.hidden = FALSE;
//        [cell.textLabel setTextAlignment:UITextAlignmentCenter];
//        cell.textLabel.text = @"Create New Property Type";
//        cell.textLabel.textColor = [UIColor grayColor];
//    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}
 
 */

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i=0;i<[appDelegate.propertyTypeArray count];i++)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:i+1000];
        [textField resignFirstResponder];
    }

    for (int i=0;i<[appDelegate.propertyTypeArray count];i++)
    {
        
        [appDelegate updatePropertyType:[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"propertyType"] rowid:[[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];

        
    }
    
    
    [appDelegate selectPropertyType];
    keytoolBar.hidden = TRUE;
	[appDelegate deletePropertyType:[[[appDelegate.propertyTypeArray objectAtIndex:indexPath.row]valueForKey:@"rowid"]intValue]];
    [appDelegate selectPropertyType];
	textFieldIndex  = -1;
    


    
    if([appDelegate.propertyTypeArray count]==0)
	{
		lblNoData.hidden = FALSE;
		lblPlus.hidden = FALSE;
        [imageView setHidden:FALSE];
		[tblView setHidden:TRUE];
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:1.0f];
		//[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];

		
	}
	else 
    {
		[tblView setHidden:FALSE];
        
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
        }

        
        
		lblPlus.hidden = TRUE;
        [imageView setHidden:TRUE];
		[lblNoData setHidden:TRUE];
		[tblView reloadData];

	}

}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([appDelegate.propertyTypeArray count]>0)
    {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
        [self moveFromOriginal:fromIndexPath.row toNew:toIndexPath.row];

}

-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew {
    
   if (textFieldIndex > 0)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldIndex];
        [textField resignFirstResponder];
    }
    
    //tableview height in textfieldend editing etc…
    if(appDelegate.isIphone5)
    {
        //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
        
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
        }
        
        
        
    }
    else
    {
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
        }
        
        
    }

    
    [appDelegate selectPropertyType];
    
    NSMutableArray *rowIdArray = [[NSMutableArray alloc] initWithCapacity:10];
    nslog(@"\n before moving = %@",appDelegate.propertyTypeArray);
    for (int i=0;i<[appDelegate.propertyTypeArray count];i++)
    {
        [rowIdArray addObject:[[appDelegate.propertyTypeArray objectAtIndex:i] valueForKey:@"rowid"]];
    }
     nslog(@"\n rowIdArray  = %@",rowIdArray);
    
	NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:appDelegate.propertyTypeArray];
    id tempOriginalObject = [appDelegate.propertyTypeArray objectAtIndex:indexOriginal];
    
    /**
    id tempNewObject = [appDelegate.propertyTypeArray objectAtIndex:indexNew];
	*/
    
    nslog(@"\n indexOriginal = %d",indexOriginal);
    nslog(@"\n indexNew = %d",indexNew);
    [tempArray removeObjectAtIndex:indexOriginal];
    [tempArray insertObject:tempOriginalObject atIndex:indexNew];
    appDelegate.propertyTypeArray = tempArray;
	
    
    nslog(@"\n after moving = %@",appDelegate.propertyTypeArray);
	
    for (int i=0;i<[appDelegate.propertyTypeArray count];i++)
    {
        [[appDelegate.propertyTypeArray objectAtIndex:i] setObject:[rowIdArray objectAtIndex:i] forKey:@"rowid"];
    }
    
    nslog(@"\n after moving  and setting index..= %@",appDelegate.propertyTypeArray);
    
    for (int i=0;i<[appDelegate.propertyTypeArray count];i++)
    {
        [appDelegate updatePropertyType:[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"propertyType"] rowid:[[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
    }
    [appDelegate selectPropertyType];
    
    keytoolBar.hidden = YES; 
    [tempArray release];
    [rowIdArray release];
    
    [tblView reloadData];
    textFieldIndex = -1;
	
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
     nslog(@"\n indexpath = %d",indexPath.row);
    // Return NO if you do not want the item to be re-orderable.
    if ([appDelegate.propertyTypeArray count]>0)
    {
          return YES;
    }
    return NO;
}

#pragma mark text field

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    keytoolBar.hidden = FALSE;
    textFieldIndex = textField.tag;
   
    
    /*
    NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:textField.tag%1000 inSection:0];
	[tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
    */
    isClicked = textField.tag;
    textFieldTempText = textField.text;
    [textFieldTempText retain];
   
    nslog(@"\n textFieldTempText = %@",textFieldTempText);
    if (textField.tag > 1002)
    {
        /*
        tblView.frame = CGRectMake(0, -((textField.tag%1000)*25), tblView.frame.size.width, tblView.frame.size.height);
         */
    }
    
   
    
   
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
     keytoolBar.hidden = FALSE;
    
    if(!appDelegate.isIPad)
    {
        nslog(@" origint = %f",textField.frame.origin.y);
        
        
        if(appDelegate.isIphone5)
        {
            tblView.frame = CGRectMake(0,tblView.frame.origin.y ,tblView.frame.size.width,240.0f);
            
        }
        else
        {
        
        
        tblView.frame = CGRectMake(0,tblView.frame.origin.y , tblView.frame.size.width,155.0f);
        }
        [tblView setContentOffset:CGPointMake(0,textField.frame.origin.y+(44*(isClicked%1000)-44)) animated:YES];
        //[tblView scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];
    }

}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
 
    if(appDelegate.isIPad)
    {
        if(isAddClicked)
        {
            return NO;
        }
    }
    
   
    
    nslog(@"\n textField.tag = %d",textField.tag);
    int index = textField.tag%1000;
    nslog(@"reminder = %d",index);
   
    
    
    
    //nslog(@"\n %@",[appDelegate.propertyTypeArray objectAtIndex:index]);
    
    
    
    nslog(@"textField = %@",textField.text);

    if(textField.tag != 5000)//alert view tag..!
    {
        
        [[appDelegate.propertyTypeArray objectAtIndex:index] setValue:textField.text forKey:@"propertyType"];

        
        for(int i=0;i<[appDelegate.propertyTypeArray count];i++)
        {
            [appDelegate updatePropertyType:[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"propertyType"] rowid:[[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        }

    }

    if(appDelegate.isIPad)
    {
        keytoolBar.hidden = TRUE;
        [textField resignFirstResponder];
    }
    
    if(!appDelegate.isIPad)
    {
        
        
        if(appDelegate.isIphone5)
        {
            
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
        }
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
    }
    


    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(isAddClicked)
    {
        return NO;
    }
    
    nslog(@"comes in should end property type");
    if (textField.tag > 999)
    {
        
        if(!appDelegate.isIPad)
        {
            
            if(appDelegate.isIphone5)
            {
               
                
                
                if(appDelegate.isIOS7)
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
                }
                else
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
                }

            
            
            
            
            }
            else
            {
                tblView.frame = CGRectMake(0,tblView.frame.origin.y , tblView.frame.size.width,367.0f);
            }
            
            
        }

        
        nslog(@"\n textField.tag = %d",textField.tag);
        nslog(@"\n textField.text = %@",textField.text);
        nslog(@"\n textField.tag = %d",textField.tag%1000);
        nslog(@"\n textFieldIndex = %d",textFieldIndex%1000);
        
        
        nslog(@"\n appDelegate.reminderTypeArray = %@",appDelegate.propertyTypeArray);
        nslog(@"\n appDelegate.reminderTypeArray at index = %@",[appDelegate.propertyTypeArray objectAtIndex:(textField.tag%1000)]);
        nslog(@"\n text here = %@",textField.text);
        if([textField.text length]==0)
        {
            textField.text =@"";
        }
        
        NSString *text = @"";        
        for(int i=0;i<[appDelegate.propertyTypeArray count];i++)
        {
            nslog(@"\n appDelegate.propertyTypeArray in loop = %@",appDelegate.propertyTypeArray);
            if(textFieldIndex%1000 == i)
            {
               // text = textField.text;
                text = textFieldTempText;
            }
            else
            {
                text =  [[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"propertyType"];
            }
            nslog(@"\n text = %@",text);
            
            
            [appDelegate updatePropertyType:text rowid:[[[appDelegate.propertyTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        }
        
        [appDelegate selectPropertyType];
        
        
        /*
         [appDelegate updatePropertyType:textField.text rowid:[[[appDelegate.propertyTypeArray objectAtIndex:(textField.tag%1000)]valueForKey:@"rowid"]intValue]];
         
         */
        
        
        nslog(@"property type array in should end editing.....%@",appDelegate.propertyTypeArray);
        if (textFieldIndex == textField.tag)
        {
            textFieldIndex = -1;
        }
    }

    
    
     keytoolBar.hidden = TRUE;
    [textField resignFirstResponder];
    
    
    
    if(appDelegate.isIphone5)
    {
        //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
        
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
        }
        
        
        
    }
    else
    {
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
        }
        
        
    }

    
   // tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
    
    
    return YES;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!tblView.editing)
    {
        if (appDelegate.fromProperty)
        {
            if ([appDelegate.propertyTypeArray count]>0)
            {
            UITableViewCell *oldCell = [tblView cellForRowAtIndexPath:oldIndex];
                
                nslog(@"comes in oldcell");
           
                oldCell.accessoryType = UITableViewCellAccessoryNone;
            }
            
        UITableViewCell *newCell = [tblView cellForRowAtIndexPath:indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
            
            appDelegate.propertyTypeStr = [NSString stringWithFormat:@"%@",[[appDelegate.propertyTypeArray objectAtIndex:indexPath.row]valueForKey:@"propertyType"]];
//            appDelegate.propertyTypeStr = newCell.textLabel.text;
            
            /*Memory*/
            //[appDelegate.propertyTypeStr retain];
            
            oldIndex = indexPath;
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if (tblView.editing)
    {
        UITextField *txtField = (UITextField *)[self.view viewWithTag:1000+indexPath.row];
        [txtField becomeFirstResponder];
    }
    
    //nslog(@"property string=======%@",appDelegate.propertyTypeStr);
}

@end
