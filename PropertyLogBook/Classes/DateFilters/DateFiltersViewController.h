//
//  DateFiltersViewController.h
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 05/03/13.
//
//


/*
 DateButon tag starts from 1000,1001
 
 
 */


#import <UIKit/UIKit.h>
#import "DateFiltersTableViewCell.h"
#import "PropertyLogBookAppDelegate.h"


@interface DateFiltersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet  UITableView*dateFilters_tableView;
    
    DateFiltersTableViewCell *tblCell;
    UINib *cellNib;
    
    IBOutlet UIDatePicker*filterView_dateFilter;
    
    IBOutlet UIToolbar *filterView_toolBar;
    
    int selected_date_buttonTag;
    
    //NSDate*startDate;
    //NSDate*endDate;
    
    PropertyLogBookAppDelegate*appDelegate;
    UIButton*tempButton;//for managing button;
    
    NSMutableDictionary*data_dictionary;

    NSString *monthString;
    NSString *yearString;
    NSString *currentDate;
    
    IBOutlet UIBarButtonItem*cancelBtn;
    
    
}


@property (nonatomic, retain) IBOutlet DateFiltersTableViewCell *tblCell;
@property (nonatomic, retain) UINib *cellNib;
-(IBAction)dateValueChanged:(id)sender;

-(IBAction)done_button_clicked:(id)sender;
-(IBAction)cancel_button_clicked:(id)sender;





@end
