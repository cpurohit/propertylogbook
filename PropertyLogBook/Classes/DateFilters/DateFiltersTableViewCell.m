//
//  DateFiltersTableViewCell.m
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 05/03/13.
//
//

#import "DateFiltersTableViewCell.h"

@implementation DateFiltersTableViewCell

@synthesize cellIcon_ImageView,cellTitle_Label,datePicker_button;
@synthesize btn_checkMark;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)dealloc
{
    
    [cellIcon_ImageView release];
    [cellTitle_Label release];
    [datePicker_button release];
    [btn_checkMark release];
    
    
    [super dealloc];
}


@end
