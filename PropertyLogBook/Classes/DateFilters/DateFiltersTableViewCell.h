//
//  DateFiltersTableViewCell.h
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 05/03/13.
//
//

#import <UIKit/UIKit.h>

@interface DateFiltersTableViewCell : UITableViewCell
{
    IBOutlet UIImageView*cellIcon_ImageView;
    IBOutlet  UILabel*cellTitle_Label;
    IBOutlet  UIButton*datePicker_button;
    
    IBOutlet UIButton*btn_checkMark;
}

@property (nonatomic,retain) IBOutlet UIImageView*cellIcon_ImageView;
@property (nonatomic,retain) IBOutlet UILabel*cellTitle_Label;
@property (nonatomic,retain) IBOutlet UIButton*datePicker_button;

@property (nonatomic,retain) IBOutlet UIButton*btn_checkMark;




@end
