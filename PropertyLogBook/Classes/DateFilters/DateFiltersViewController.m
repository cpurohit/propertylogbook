//
//  DateFiltersViewController.m
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 05/03/13.
//
//

#import "DateFiltersViewController.h"

@interface DateFiltersViewController ()

@end

@implementation DateFiltersViewController

@synthesize tblCell,cellNib;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.navigationItem.title = @"SET DEFAULT FILTERS";
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    
    
   
    [super viewDidLoad];
    
    
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        [dateFilters_tableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
    }
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    
    filterView_dateFilter.backgroundColor = [UIColor whiteColor];
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                dateFilters_tableView.frame = CGRectMake(dateFilters_tableView.frame.origin.x,dateFilters_tableView.frame.origin.y-25.0f,dateFilters_tableView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                dateFilters_tableView.frame = CGRectMake(dateFilters_tableView.frame.origin.x,dateFilters_tableView.frame.origin.y,dateFilters_tableView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                dateFilters_tableView.frame = CGRectMake(dateFilters_tableView.frame.origin.x,dateFilters_tableView.frame.origin.y-25.0f,dateFilters_tableView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                dateFilters_tableView.frame = CGRectMake(dateFilters_tableView.frame.origin.x,dateFilters_tableView.frame.origin.y,dateFilters_tableView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            dateFilters_tableView.frame = CGRectMake(dateFilters_tableView.frame.origin.x,dateFilters_tableView.frame.origin.y-25.0f,dateFilters_tableView.frame.size.width,self.view.frame.size.height+25.0f);
        }

    }
    

    
    
    
    
    // Do any additional setup after loading the view from its nib.
     self.cellNib = [UINib nibWithNibName:@"DateFiltersTableViewCell" bundle:nil];
    
    filterView_toolBar.hidden = TRUE;
    filterView_dateFilter.hidden = TRUE;
    
   
    

    dateFilters_tableView.backgroundColor = [UIColor clearColor];
    [dateFilters_tableView setBackgroundView:nil];
    [dateFilters_tableView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
    

    
    
    
    
    data_dictionary = [[NSMutableDictionary alloc] initWithDictionary:[appDelegate select_default_date_filter]];

    nslog(@"\n data_dictionary = %@",data_dictionary);
    
    
    
   NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    
    
    
    
    NSDateComponents *monthComponent = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:[NSDate date]];
    [monthComponent setMonth:[monthComponent month]];
    [monthComponent setDay:1];
    [monthComponent setYear:[monthComponent year]];
    
    
    
    
    
    
    NSDateComponents *yearComponent = [gregorian components:NSYearCalendarUnit fromDate:[NSDate date]];
    //[yearComponent setMonth:[monthComponent month]];
    [yearComponent setMonth:1];
    [yearComponent setDay:1];
    
    
    NSDate *firstDateOfyear = [gregorian dateFromComponents:yearComponent];
    
    
    NSDate *current = [NSDate date];
    
    
    
    [gregorian release];
    
    
    
    
    /*
     Starts:  SUN:0004
     Date:05-03-2013
     For date filter conditions...
     
     */
    
    
    
    
    if([[data_dictionary objectForKey:@"1000"] length]==0)
    {
        [data_dictionary setObject:[appDelegate.regionDateFormatter stringFromDate:firstDateOfyear] forKey:@"1000"];
    }
    
    if([[data_dictionary objectForKey:@"1001"] length]==0)
    {
        [data_dictionary setObject:[appDelegate.regionDateFormatter stringFromDate:current] forKey:@"1001"];
    }
    
    
    /*
     Ends: SUN:0004
     
     Date:05-03-2013
     For date filter conditions...
     
     */
    

    
    [df release];
    
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(back_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }

    

    
    

    
}




-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    //[self.view addSubview:appDelegate.bottomView];
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    
    
    
    
    /*Sun:0004
     Stats:Managing MTD,
     */
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *monthComponent = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:[NSDate date]];
    [monthComponent setMonth:[monthComponent month]];
    [monthComponent setDay:1];
    [monthComponent setYear:[monthComponent year]];
    NSDate *firstDateOfMonth = [gregorian dateFromComponents:monthComponent];
    
    
    monthString = [NSString stringWithString:[appDelegate.regionDateFormatter stringFromDate:firstDateOfMonth]];
    [monthString retain];//dont know but if no retain app crashes in tableview..
    nslog(@"\n monthString = %@ \n",monthString);
    
    //[yearComponent setMonth:[monthComponent month]];
    NSDateComponents *yearComponent = [gregorian components:NSYearCalendarUnit fromDate:[NSDate date]];
    [yearComponent setMonth:1];
    [yearComponent setDay:1];
    
    NSDate *firstDateOfyear = [gregorian dateFromComponents:yearComponent];
    
    [gregorian release];
    
    
    yearString = [NSString stringWithString:[appDelegate.regionDateFormatter stringFromDate:firstDateOfyear]];
    [yearString retain];
    nslog(@"\n yearString = %@ \n",yearString);
    
    
    NSDate *current = [NSDate date];
    //NSString *currentDate = [df stringFromDate:current];
    currentDate = [NSString stringWithString:[appDelegate.regionDateFormatter stringFromDate:current]] ;
 nslog(@"\n currentDate = %@ \n",currentDate);
    
    [currentDate retain];
    /*Sun0004
     
     Ends:
     */
    
    
    /*
     Starts:
     Sun:0004
     Checking iphone-5 condition.
     
     */
    filterView_toolBar.frame = CGRectMake(0,120,320,44);
    if(appDelegate.isIphone5)
    {
        filterView_dateFilter.frame = CGRectMake(filterView_dateFilter.frame.origin.x, filterView_dateFilter.frame.origin.y+86,filterView_dateFilter.frame.size.width, filterView_dateFilter.frame.size.height);
        
        filterView_toolBar.frame = CGRectMake(0,202,320,44);
    }
    
    
    
    /*
     Ends :
     Sun:0004
     Checking iphone-5 condition.
     */
    
    
    if(appDelegate.isIPad)
    {
        
		[self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    	[self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
	}
    
}

- (void)didReceiveMemoryWarning
{
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
}



#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

#pragma mark
#pragma mark Tableview methods 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if( appDelegate.date_filter_type == 2)
    {
        return 5;
    }
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 51.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     static NSString *MyIdentifier = @"MyIdentifier";
    
    DateFiltersTableViewCell *cell = (DateFiltersTableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];

    if(appDelegate.isIOS7)
    {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
	
	// If no cell is available, create a new one using the given identifier.
	if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
		cell = tblCell;
		self.tblCell = nil;

    }
	
    
    if(appDelegate.isIPad)
    {
        
        
        if(appDelegate.isIOS7)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                cell.frame = CGRectMake(0, 0,766,51);
                cell.datePicker_button.frame = CGRectMake(614,4,124, 44);
            }
            else
            {
                cell.frame = CGRectMake(0, 0,1024,51);
                cell.datePicker_button.frame = CGRectMake(872,4,124, 44);
            }

            
        }
        else
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                cell.frame = CGRectMake(0, 0,676,51);
                cell.datePicker_button.frame = CGRectMake(524,4,124, 44);
            }
            else
            {
                cell.frame = CGRectMake(0, 0,934,51);
                cell.datePicker_button.frame = CGRectMake(782,4,124, 44);
            }

            
        }
    }
    
    //cell.datePicker_button.backgroundColor = [UIColor redColor];
    
    //cell.backgroundColor = [UIColor redColor];
    
	// Set up the cell.

        
    cell.cellTitle_Label.font = [UIFont systemFontOfSize:15.0f];
    
     cell.btn_checkMark.selected = FALSE;
    cell.btn_checkMark.hidden = TRUE;
    if(indexPath.row == 0)
    {
        if(appDelegate.date_filter_type==0)
        {
            cell.btn_checkMark.selected = TRUE;
        }
        
        if(appDelegate.isIPad)
        {
            
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                {
                    
                    cell.datePicker_button.frame = CGRectMake(565,4,174, 44);
                }
                else
                {
                    
                    cell.datePicker_button.frame = CGRectMake(822,4,174, 44);
                }
                
            }
            else
            {
                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                {
                    
                    cell.datePicker_button.frame = CGRectMake(475,4,174, 44);
                }
                else
                {
                    
                    cell.datePicker_button.frame = CGRectMake(732,4,174, 44);
                }
                
            }
            
            

            
            
        }
        else
        {
            cell.datePicker_button.frame = CGRectMake(105,4,167, 44);
        }
        
        
        cell.btn_checkMark.hidden = FALSE;
        cell.btn_checkMark.tag = 2000+indexPath.row;
        
        
         nslog(@"\n currentDate = %@ \n",currentDate);
         nslog(@"\n monthString = %@ \n",monthString);
        
        
        [cell.datePicker_button setTitle:[NSString stringWithFormat:@" %@ - %@ ",monthString,currentDate] forState:UIControlStateNormal];
        
        [cell.btn_checkMark addTarget:self action:@selector(btn_checkMakr_clicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.cellTitle_Label.text = @"MTD";
    }
    else if(indexPath.row == 1)
    {
        
        if(appDelegate.date_filter_type==1)
        {
            cell.btn_checkMark.selected = TRUE;
        }
        
        [cell.datePicker_button setTitle:[NSString stringWithFormat:@" %@ - %@ ",yearString,currentDate] forState:UIControlStateNormal];
        if(appDelegate.isIPad)
        {
            if(appDelegate.isIOS7)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                {
                    
                    cell.datePicker_button.frame = CGRectMake(565,4,174, 44);
                }
                else
                {
                    
                    cell.datePicker_button.frame = CGRectMake(822,4,174, 44);
                }
            }
            else
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                {
                    
                    cell.datePicker_button.frame = CGRectMake(475,4,174, 44);
                }
                else
                {
                    
                    cell.datePicker_button.frame = CGRectMake(732,4,174, 44);
                }

            }
            
            
        }
        else
        {
            cell.datePicker_button.frame = CGRectMake(105,4,167, 44);
        }

        
        cell.btn_checkMark.hidden = FALSE;
        cell.btn_checkMark.tag = 2000+indexPath.row;
        [cell.btn_checkMark addTarget:self action:@selector(btn_checkMakr_clicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.cellTitle_Label.text = @"YTD";
    }
    else if(indexPath.row == 2)
    {
        if(appDelegate.date_filter_type==2)
        {
            cell.btn_checkMark.selected = TRUE;
        }
        
        cell.datePicker_button.hidden = TRUE;
        cell.btn_checkMark.hidden = FALSE;
        [cell.btn_checkMark addTarget:self action:@selector(btn_checkMakr_clicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn_checkMark.tag = 2000+indexPath.row;
        cell.cellTitle_Label.text = @"Preferred Date Range";
    }
    
    
    if(indexPath.row == 3)
    {
        
        if([data_dictionary objectForKey:[NSString stringWithFormat:@"%d",1000]]!=nil)
        {
            
            nslog(@" %@",[data_dictionary objectForKey:[NSString stringWithFormat:@"%d",1000]]);
            
            [cell.datePicker_button setTitle:[data_dictionary objectForKey:[NSString stringWithFormat:@"%d",1000]] forState:UIControlStateNormal];
        }

        cell.datePicker_button.tag = 1000;
        [cell.datePicker_button addTarget:self action:@selector(datePicker_button_pressed:) forControlEvents:UIControlEventTouchUpInside];

        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        //cell.imageView.image = [UIImage imageNamed:@"filter_start_date.png"];
        
        UIImageView*filter_start_date_imageView = [[UIImageView alloc] init];
        filter_start_date_imageView.frame = CGRectMake(0, 0,40,40);
        filter_start_date_imageView.image = [UIImage imageNamed:@"filter_start_date.png"];
        [cell.contentView addSubview:filter_start_date_imageView];
        [filter_start_date_imageView release];

        
        
        
        
        cell.cellTitle_Label.text = @"Start Date";
    }
    else if(indexPath.row == 4)
    {
        
        //cell.imageView.image = [UIImage imageNamed:@"filter_end_date.png"];
        
        
        UIImageView*filter_end_date_imageView = [[UIImageView alloc] init];
        filter_end_date_imageView.frame = CGRectMake(0, 0,40,40);
        filter_end_date_imageView.image = [UIImage imageNamed:@"filter_end_date.png"];
        [cell.contentView addSubview:filter_end_date_imageView];
        [filter_end_date_imageView release];
        
        
        cell.cellTitle_Label.text = @"End Date";
        
        cell.datePicker_button.tag = 1001;
        [cell.datePicker_button addTarget:self action:@selector(datePicker_button_pressed:) forControlEvents:UIControlEventTouchUpInside];

        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if([data_dictionary objectForKey:[NSString stringWithFormat:@"%d",1001]]!=nil)
        {
            
            nslog(@" %@",[data_dictionary objectForKey:[NSString stringWithFormat:@"%d",1001]]);
            
            [cell.datePicker_button setTitle:[data_dictionary objectForKey:[NSString stringWithFormat:@"%d",1001]] forState:UIControlStateNormal];
            
            
        }
        

        
    }
    


    
    [cell.datePicker_button setTitleColor:[UIColor colorWithRed:3.0/255.0f green:80.0f/255.0f blue:103.0f/255.0f alpha:1.0f] forState:UIControlStateHighlighted];
    [cell.datePicker_button setTitleColor:[UIColor colorWithRed:3.0/255.0f green:80.0f/255.0f blue:103.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    
    
   
    
	
	return cell;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"Set your own date range preference (example: Tax year). This date range will be applied throughout the application.";
}


#pragma mark - btn_checkMakr_clicked
#pragma mark check mark button for date filter.

-(void)btn_checkMakr_clicked:(id)sender
{
    UIButton*temp_button = (UIButton*)sender;
    switch (temp_button.tag)
    {
        case 2000:
        {
            appDelegate.date_filter_type = 0;
            break;
        }
        case 2001:
        {
            appDelegate.date_filter_type = 1;
            break;
        }
        case 2002:
        {
            appDelegate.date_filter_type = 2;
            break;
        }
    
       
            
            
        default:
            break;
    }
    
    
    /*Sun:004
    [appDelegate.prefs setInteger:appDelegate.date_filter_type forKey:@"date_filter_type"];
    */
    
    [appDelegate.prefs setObject:[NSString stringWithFormat:@"%d",appDelegate.date_filter_type] forKey:@"date_filter_type"];
    
    
    [dateFilters_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
    

    
   
       
    
    
    
}


#pragma mark
#pragma mark dateValueChanged for date picker.

-(IBAction)dateValueChanged:(id)sender
{

    
    
    
    switch (selected_date_buttonTag)
    {
        case 1000:
        {
            
            
            [data_dictionary setObject:[appDelegate.regionDateFormatter stringFromDate:filterView_dateFilter.date] forKey:@"1000"];
            
            break;
        }
        case 1001:
        {
            
            [data_dictionary setObject:[appDelegate.regionDateFormatter stringFromDate:filterView_dateFilter.date] forKey:@"1001"];
            
            break;
        }
            
        default:
            break;
    }
    
}


#pragma mark datePicker_button_pressed
#pragma mark


-(void)datePicker_button_pressed:(id)sender
{
    tempButton = (UIButton*)sender;
    nslog(@"\n tempButton = %d",tempButton.tag);
    selected_date_buttonTag = tempButton.tag;
    

    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
        {
            filterView_toolBar.frame= CGRectMake(0,733,768,44);
            filterView_dateFilter.frame = CGRectMake(0,778,768,216);
        }
        else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
        {
            filterView_toolBar.frame= CGRectMake(0,455,1024,44);
            filterView_dateFilter.frame = CGRectMake(0,500,1024,216);
        }
    }
    
    
    if(tempButton.tag==1000)
    {
        filterView_dateFilter.date = [appDelegate.regionDateFormatter dateFromString:[data_dictionary objectForKey:@"1000"]];
    }
    else if(tempButton.tag==1001)
    {
        filterView_dateFilter.date = [appDelegate.regionDateFormatter dateFromString:[data_dictionary objectForKey:@"1001"]];
    }

    
    filterView_dateFilter.hidden = FALSE;
    filterView_toolBar.hidden = FALSE;
    
    
        
    
    
    
    
    
}


#pragma mark
#pragma mark done_button_clicked

-(IBAction)done_button_clicked:(id)sender
{
    
    [tempButton setTitle:[appDelegate.regionDateFormatter stringFromDate:filterView_dateFilter.date] forState:UIControlStateNormal] ;
    
    filterView_toolBar.hidden = TRUE;
    filterView_dateFilter.hidden = TRUE;
    
    
    if(selected_date_buttonTag ==1000)
    {
        [data_dictionary setObject:[appDelegate.regionDateFormatter stringFromDate:filterView_dateFilter.date] forKey:@"1000"];
         
        
    }
    else if(selected_date_buttonTag ==1001)
    {
        [data_dictionary setObject:[appDelegate.regionDateFormatter stringFromDate:filterView_dateFilter.date] forKey:@"1001"];
       
        
    }

    
    
}


#pragma mark - back_clicked 
#pragma mark Comparing date.

-(void)back_clicked
{
    
    
    //[appDelegate.regionDateFormatter stringFromDate:filterView_dateFilter.date] forKey:@"1001"]
    
    NSDate*startDate = [appDelegate.regionDateFormatter dateFromString:[data_dictionary objectForKey:@"1000"]];
    NSDate*endDate =[appDelegate.regionDateFormatter dateFromString:[data_dictionary objectForKey:@"1001"]];

    nslog(@"\n startDate = %@",startDate);
    nslog(@"\n endDate = %@",endDate);
    
    
    
    if([startDate compare:endDate]==NSOrderedDescending)
    {
        UIAlertView*alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Start date can not greater than end date" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
        return;
    }
    
    
    //db methods here...
    [appDelegate update_default_date_filter:data_dictionary];
    [self.navigationController popViewControllerAnimated:TRUE];
}

#pragma mark 
#pragma mark viewWillDisappear

-(void)viewWillDisappear:(BOOL)animated
{
    //db methods here...

    [super viewWillDisappear:animated];
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
   
    
    
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    filterView_toolBar.hidden = TRUE;
    filterView_dateFilter.hidden = TRUE;
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{


    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
    }

    
    
    [dateFilters_tableView reloadData];
}

#pragma mark
#pragma mark cancel_button_clicked


-(IBAction)cancel_button_clicked:(id)sender
{
    
    
    filterView_toolBar.hidden = TRUE;
    filterView_dateFilter.hidden = TRUE;

    
}



#pragma mark
#pragma mark dealloc

-(void)dealloc
{
    
    
    [super dealloc];
    [filterView_dateFilter release];
    [filterView_toolBar release];
    [tblCell release];
    [cellNib release];
    
}


@end
