//
//  HomeViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HomeViewController.h"
#import "IncomeExpenseViewController.h"
#import "equityListViewController.h"
#import "EquityViewController.h"
#import "ReminderViewController.h"
#import "PropertyLogBookAppDelegate.h"
#import "ReportViewController.h"
#import "DashBoardViewController.h"
#import "ListsViewController.h"

@implementation HomeViewController

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

#pragma mark -
#pragma mark View lifecycle

-(void)viewDidUnload
{
    [super viewDidUnload];
    
   
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    
    UIImageView *imgView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]] autorelease];
    [imgView setFrame:CGRectMake(0, 0,  1024,1024)];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    nslog(@"\n appDelegate.add_income_expense_bottom_button on home viewdidload = %@ \n",appDelegate.add_income_expense_bottom_button);
    
    if(appDelegate.add_income_expense_bottom_button==nil)
    {
        appDelegate.add_income_expense_bottom_button=[UIButton buttonWithType:UIButtonTypeCustom];
    }
    
    
    [appDelegate.add_income_expense_bottom_button setBackgroundImage:[UIImage imageNamed:@"add_btn_off.png"] forState:UIControlStateNormal];
    [appDelegate.add_income_expense_bottom_button setBackgroundImage:[UIImage imageNamed:@"add_btn_on.png"] forState:UIControlStateSelected];
    
    if(appDelegate.isIphone5)
    {
        [appDelegate.add_income_expense_bottom_button setFrame:CGRectMake(107,519,107,49)];
    }
    else
    {
        [appDelegate.add_income_expense_bottom_button setFrame:CGRectMake(107,431,107,49)];
    }
    
    
    [appDelegate.add_income_expense_bottom_button setTag:1];
    
    
    
    [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:1];
    
    //self.navigationItem.title = @"PROPERTY LOG BOOK";
    self.navigationItem.titleView = homeView_navigation_title;
    nslog(@"badgecount ==== %d",appDelegate.badgeCount);
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor],UITextAttributeTextColor,
                                                                     [UIColor whiteColor],UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
                                                                     UITextAttributeTextShadowOffset ,
                                                                     [UIFont boldSystemFontOfSize:19.0f],UITextAttributeFont  , nil]];
    
    
    
    /*
     Start:
     Sun:0004
     Setting custom image in navigation bar..
     */
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0)
        {
            [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
                       
        }

    /*
     Ends:
     Sun:0004
     Setting custom image in navigation bar..
     */

    
    if(appDelegate.isIphone5)
    {
     
        btnIncomeExpense.frame = CGRectMake(btnIncomeExpense.frame.origin.x,btnIncomeExpense.frame.origin.y+44,btnIncomeExpense.frame.size.width, btnIncomeExpense.frame.size.height);
        
        btnCalculators.frame= CGRectMake(btnCalculators.frame.origin.x,btnCalculators.frame.origin.y+44,btnCalculators.frame.size.width, btnCalculators.frame.size.height);
        
        btnCheckListAndRemider.frame= CGRectMake(btnCheckListAndRemider.frame.origin.x,btnCheckListAndRemider.frame.origin.y+44,btnCheckListAndRemider.frame.size.width, btnCheckListAndRemider.frame.size.height);

        btnDashboardAndReport.frame= CGRectMake(btnDashboardAndReport.frame.origin.x,btnDashboardAndReport.frame.origin.y+44,btnDashboardAndReport.frame.size.width, btnDashboardAndReport.frame.size.height);

     
         badgeButton.frame= CGRectMake(badgeButton.frame.origin.x,badgeButton.frame.origin.y+44,badgeButton.frame.size.width, badgeButton.frame.size.height);
        
        welcome_screen_view.frame = CGRectMake(welcome_screen_view.frame.origin.x,welcome_screen_view.frame.origin.y+44,welcome_screen_view.frame.size.width, welcome_screen_view.frame.size.height);
        
    }
    
    /*Sun:0004
     Adding bottomview to currentview;
     */

    
    welcome_screen_view.hidden = TRUE;
    welcome_image_view.hidden = TRUE;
    close_welcome_screen_button.hidden = TRUE;

        
    
      
    
}



-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
    

}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //   [badgeButton setBackgroundColor:[UIColor redColor]];
    
    
    
    [super viewWillAppear:YES];
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    appDelegate.isFirst = [[NSUserDefaults standardUserDefaults]boolForKey:@"isFirst"];
    
    nslog(@"\n appDelegate.add_income_expense_bottom_button = %@ \n",appDelegate.add_income_expense_bottom_button);
    
    if (!appDelegate.isFirst)
    {
        welcome_image_view.hidden = FALSE;
        welcome_screen_view.hidden = FALSE;
        close_welcome_screen_button.hidden = FALSE;
        
        [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"isFirst"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        
        welcome_screen_view.hidden = TRUE;
        welcome_image_view.hidden = TRUE;
        close_welcome_screen_button.hidden =TRUE;
    }
    
    nslog(@"\n appDelegate.isFirst = %d", [[NSUserDefaults standardUserDefaults]boolForKey:@"isFirst"]);
    

    
    nslog(@"\n add_income_expense_bottom_button = %@",appDelegate.add_income_expense_bottom_button);
    
            

        
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    

    
    
   
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    
    /*if put in app delegate,app got crashed on second time click.*/
    


   
    
    
    //[self.view addSubview:appDelegate.add_income_expense_bottom_button];
    
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    
    

    /**/
    [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:1];
    
    if (appDelegate.badgeCount > 0)
    {
        [badgeButton setTitle:[NSString stringWithFormat:@"%d",appDelegate.badgeCount] forState:UIControlStateNormal];
        badgeButton.hidden = FALSE;
    }
    else
    {
        badgeButton.hidden = TRUE;
    }
    appDelegate.tabBarController.selectedIndex = 0;
    
     
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    nslog(@"\n homeview ViewDidAppear...");
    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}



- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if(appDelegate.isIPad)
    {
        
        
        
        
        if(toInterfaceOrientation== 1 ||toInterfaceOrientation == 2 )
        {
            self.view.frame  = CGRectMake(0, 0,768,1024);
            btnIncomeExpense.frame = CGRectMake(25,144,327,310);
            btnCalculators.frame = CGRectMake(418, 144,327,310);
            btnDashboardAndReport.frame = CGRectMake(25, 521,327,310);
            btnCheckListAndRemider.frame = CGRectMake(418,521,327,310);
            badgeButton.frame = CGRectMake(712,523,30,30);
            
            backgroundImage.frame = CGRectMake(0, 0,768,1024);
            backgroundImage.image = [UIImage imageNamed:@"ipad_home_bg.png"];
            
        }
        else if(toInterfaceOrientation == 3 ||toInterfaceOrientation == 4 )
        {
            self.view.frame  = CGRectMake(0, 0,1024,768);
            btnIncomeExpense.frame = CGRectMake(256,52,256,246);
            btnCalculators.frame = CGRectMake(560, 52,256,246);
            btnDashboardAndReport.frame = CGRectMake(256, 346,256,246);
            btnCheckListAndRemider.frame = CGRectMake(560,346,256,246);
            backgroundImage.frame = CGRectMake(0, 0,1024,768);
            backgroundImage.image = [UIImage imageNamed:@"ipad_home_bg_land.png"];
            badgeButton.center = CGPointMake(801,364);
            
            
        }
        
        
        
        
        
    }

}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
}

#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}


#pragma mark - calculator_button_pressed
#pragma mark Calculators...


-(IBAction)calculator_button_pressed:(id)sender
{
    nslog(@"Integrated after all other stuff...");
    
    ListsViewController *viewController = [[ListsViewController alloc] initWithNibName:@"ListsViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
    [viewController release];
    
    
}

-(IBAction)incomeExpense_click
{
    
    NSString *nibName = @"";
    if(appDelegate.isIPad)
    {
        nibName =  @"IncomeExpenseViewController_iPad"; 
    }
    else
    {
        nibName =  @"IncomeExpenseViewController";
    }
    incomeExpenseView = [[IncomeExpenseViewController alloc]initWithNibName:nibName bundle:nil];
    
    [self.navigationController pushViewController:incomeExpenseView animated:YES];
    [incomeExpenseView release];

}


/*
    Sun:0004
    Phase:3
    This will be moved to calculators...
 */

-(IBAction)equityButton_click
{
	equityView = [[equityListViewController alloc]initWithNibName:@"equityListViewController" bundle:nil];
	[self.navigationController pushViewController:equityView animated:YES];
	[equityView release];
}

/*
 Sun:0004
 Phase:3
 This will be moved to calculators...
 */


-(IBAction)remiderBtn_click
{
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];

    appDelegate.badgeCount = 0;
	
    reminderView = [[ReminderViewController alloc]initWithNibName:@"ReminderViewController" bundle:nil];
	[self.navigationController pushViewController:reminderView animated:YES];
	[reminderView release];
}


#pragma mark - dashboard_and_button_pressed
#pragma mark dashboard and report...


-(IBAction)dashboard_and_report_button_pressed:(id)sender
{
    
    /*
    dashBoardViewController = [[DashBoardViewController alloc] initWithNibName:@"DashBoardViewController" bundle:nil];
    */

    dashBoardViewController = [[DashBoardViewController alloc] init];
    [self.navigationController pushViewController:dashBoardViewController animated:YES];
    [dashBoardViewController release];
    
    
}


/*Sun:0004
 Phase:3
 This section is removed...
 
 */

-(IBAction)reportBtn_click
{
    ReportViewController *reportView = [[ReportViewController alloc]initWithNibName:@"ReportViewController" bundle:nil];
    [self.navigationController pushViewController:reportView animated:YES];
    [reportView release];
}


#pragma mark - close_welcome_screen_button_pressed

-(IBAction)close_welcome_screen_button_pressed:(id)sender
{
    
    [self.view setUserInteractionEnabled:TRUE];
    
    welcome_screen_view.hidden = TRUE;
    welcome_image_view.hidden = TRUE;
    close_welcome_screen_button.hidden = TRUE;
    
}

- (void)didReceiveMemoryWarning {
    
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}



- (void)dealloc
{
    [super dealloc];
}


@end
