//
//  reportCustomCell.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 9/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PropertyLogBookAppDelegate.h"
@interface reportCustomCell : UITableViewCell 
{
 
    UILabel *propertyLbl, *detailLabel;
    UIButton *pdfButton;
    UIImageView *cellImage;
    PropertyLogBookAppDelegate *appDelegate;
}
@property (nonatomic, retain)UILabel *propertyLbl,*detailLabel;
@property (nonatomic, retain)UIButton *pdfButton;

@property(nonatomic, retain)UIImageView *cellImage;
           

@end
