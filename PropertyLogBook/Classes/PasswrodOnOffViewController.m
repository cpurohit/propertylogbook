//
//  PassOnOffViewController.m
//  MyReferences
//
//  Created by Neel  Shah on 02/01/12.
//  Copyright (c) 2012 Sunshine Infotech. All rights reserved.
//

#import "PasswrodOnOffViewController.h"
#import "PropertyLogBookAppDelegate.h"
#import "SetPassWordViewController.h"
#import "CustomeCell_PassWord.h"
@implementation PasswrodOnOffViewController
@synthesize tbl_PassOn_Off;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)BackTopush
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - View lifecycle

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate=(PropertyLogBookAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.navigationItem.title=@"PASSWORD";
    
    isPasswordEmailFirstTime = TRUE;
    
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y-25.0f,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y-25.0f,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y-25.0f,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height+25.0f);
        }

    }

    
    
    
    [tbl_PassOn_Off setBackgroundView:nil];
    [tbl_PassOn_Off setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
    
    
    
    
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton *btnBack=[UIButton buttonWithType:UIButtonTypeCustom];
        btnBack.frame=CGRectMake(0, 0,49.0,29.0);
        btnBack.backgroundColor=[UIColor clearColor];
        [btnBack setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [btnBack addTarget:self action:@selector(BackTopush) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:btnBack] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton *btnBack=[UIButton buttonWithType:UIButtonTypeCustom];
        btnBack.frame=CGRectMake(0, 0,49.0,29.0);
        btnBack.backgroundColor=[UIColor clearColor];
        [btnBack setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [btnBack addTarget:self action:@selector(BackTopush) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:btnBack] autorelease];
        
    }

    
    
    
    
    SwtPassCode=[[UISwitch alloc] init];
    SwtPassCode.onTintColor = [UIColor colorWithRed:10.0f/255.0f green:142.0f/255.0f blue:184.0f/255.0f alpha:1.0];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0)
    {
        //SwtPassCode.onTintColor=[UIColor colorWithRed:97.0/255.0 green:36.0/255.0 blue:11.0/255.0 alpha:1.0];
    }
    [SwtPassCode addTarget:self action:@selector(PassOnOff:) forControlEvents:UIControlEventValueChanged];
    
    tbl_PassOn_Off.backgroundColor=[UIColor clearColor];
    
    /*Starts:Password*/
    
    //Create done button
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(DoneButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
       
    
    UIBarButtonItem *flexiItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    
    [toolBarKeyboard setItems:[NSArray arrayWithObjects:flexiItem,doneButton, nil]];
    
    
    if(appDelegate.isIphone5)
    {
         //toolBarKeyboard.frame = CGRectMake(0,toolBarKeyboard.frame.origin.y+36,toolBarKeyboard.frame.size.width,toolBarKeyboard.frame.size.height);
    }
    
    if(appDelegate.isIPad)
    {
        self.view.frame = CGRectMake(0, 0,768,1024);
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            toolBarKeyboard.frame = CGRectMake(0, 785,768, 44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            toolBarKeyboard.frame = CGRectMake(0,326,1024, 44);
        }
        
    }
    
    
    
    
    [flexiItem release];
    [doneButton release];
    
    toolBarKeyboard.barStyle = UIBarStyleBlack;
    toolBarKeyboard.hidden=YES;
    
    
    /**/
    
    
}

-(NSString*)getPassWord
{
    prefs=[NSUserDefaults standardUserDefaults];
    NSString *Password=[prefs objectForKey:@"PassWordValue"];
    nslog(@"\n password in getPassWord = %@",Password);
    return Password;
}

-(void)SetPassCodeStatus:(int)TagNumberValue
{
    prefs=[NSUserDefaults standardUserDefaults];
	[prefs setInteger:TagNumberValue forKey:@"PasscodeOn"];
	[prefs synchronize];
}

-(int)getPassCodeStatus
{
    prefs=[NSUserDefaults standardUserDefaults];
	int Number=[prefs integerForKey:@"PasscodeOn"];
	if (!Number)
    {
		Number=0;
	}
	return Number;
}

-(void)SetPassWord:(NSString *)passWordString
{
    prefs=[NSUserDefaults standardUserDefaults];
    [prefs setValue:passWordString forKey:@"PassWordValue"];
    [prefs synchronize];
}
-(NSString *)GetPassWord
{
    prefs=[NSUserDefaults standardUserDefaults];
    NSString *Password=[prefs objectForKey:@"PassWordValue"];
    return Password;
}

-(void)SetPassWordHint:(NSString *)PassWordHint
{
    prefs=[NSUserDefaults standardUserDefaults];
    [prefs setValue:PassWordHint forKey:@"PassWordValueHint"];
    [prefs synchronize];
}

-(NSString *)GetPassWordHint
{
    prefs=[NSUserDefaults standardUserDefaults];
    NSString *Password=[prefs objectForKey:@"PassWordValueHint"];
    return Password;
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    //[self.view addSubview:appDelegate.bottomView];
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    isDonePressed = NO;
    
    PassWord = [self getPassWord];
    //[PassWord retain];
    nslog(@"PassWord in viewWillAppear = %@",PassWord);
    
    nslog(@"PassWord in viewWillAppear = %f",tbl_PassOn_Off.frame.size.height);
    
    textFieldDictionary = [[NSMutableDictionary alloc] init];
    [textFieldDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",5000]];
    [textFieldDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",5001]];
    [textFieldDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",5002]];
    
    //if (![PassWord isEqualToString:@"(null)"])
    if(PassWord!= nil)
    {
        if ([self getPassCodeStatus]==1)
        {
            SwtPassCode.on=TRUE;
        }
        else
        {
            [self SetPassWord:nil];
            [self SetPassWordHint:nil];
            [self SetPassCodeStatus:0];
            SwtPassCode.on=FALSE;
        }
    }
    else
    {
        [self SetPassCodeStatus:0];
        SwtPassCode.on=FALSE;
    }
    
    if(appDelegate.isIPad)
    {
        
		[self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    	[self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
	}
    
    [tbl_PassOn_Off reloadData];
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1)
    {
        
        
        //UITextField *txt=(UITextField *)[alertView viewWithTag:2000];
        
        UITextField *txt= [alertView textFieldAtIndex:0];
        
        NSString *Pass=[self getPassWord];
        
        NSLog(@"\n Pass = %@",Pass);
        NSLog(@"\n Pass = %@",txt.text);
        
        
        if ([Pass isEqualToString:txt.text])
        {
                       
            isPasswordChangeClicked = 1;
            
            
            
            UIButton *btnDone=[UIButton buttonWithType:UIButtonTypeCustom];
            btnDone.frame=CGRectMake(0, 0,51.0,30.0);
            btnDone.backgroundColor=[UIColor clearColor];
            [btnDone setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
            [btnDone addTarget:self action:@selector(DoneToolBarKey) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:btnDone] autorelease];
            
            
            toolBarKeyboard.hidden = YES;
            [tbl_PassOn_Off reloadData];
            
        }
        else
        {
            UIAlertView *Wrongpass=[[UIAlertView alloc] initWithTitle:@"Wrong Password" message:@"You have Entered wrong Password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            toolBarKeyboard.hidden = YES;
            [Wrongpass show];
            [Wrongpass release];
        }
    }
    else
    {
        toolBarKeyboard.hidden = YES;
    }
}


-(IBAction)PassOnOff:(id)sender
{
    
    if(appDelegate.isIPad)
    {
        tbl_PassOn_Off.frame = CGRectMake(0,tbl_PassOn_Off.frame.origin.y , tbl_PassOn_Off.frame.size.width,911.0f);
    }
    else
    {
        
        if(appDelegate.isIphone5)
        {
            //tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
    
    
    }
    
    
    if ([SwtPassCode  isOn])
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            nib_Name=@"";
        }
        else
        {
            nib_Name=@"SetPassWordViewController";
        }
        
        isSetPasswordClicked = 1;
        
        
        UIButton *btnDone=[UIButton buttonWithType:UIButtonTypeCustom];
        btnDone.frame=CGRectMake(0, 0,51.0,30.0);
        btnDone.backgroundColor=[UIColor clearColor];
        [btnDone setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
        [btnDone addTarget:self action:@selector(DoneToolBarKey) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:btnDone] autorelease];
        
        
        [tbl_PassOn_Off reloadData];
        /*
         SetPassWordViewController *obj_Pass=[[SetPassWordViewController alloc] initWithNibName:nib_Name bundle:nil];
         [self.navigationController pushViewController:obj_Pass animated:YES];
         [obj_Pass release];
         SwtPassCode.on=TRUE;
         [self SetPassCodeStatus:1];
         */
    }
    else
    {
        self.navigationItem.rightBarButtonItem=nil;
        
        isSetPasswordClicked = 0;
        [self SetPassWord:nil];
        [self SetPassWordHint:nil];
        SwtPassCode.on=FALSE;
        [self SetPassCodeStatus:0];
        PassWord = [self getPassWord];
        [tbl_PassOn_Off reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    toolBarKeyboard.hidden = YES;
    nslog(@"\n password = %@",PassWord);
    //if (![PassWord isEqualToString:@"(null)"])
    if (PassWord != nil)
    {
        if ([self getPassCodeStatus]==0)
        {
            return 1;
        }
        else if([self getPassCodeStatus]==1 && isPasswordChangeClicked == 0)
        {
            return 2;
        }
        else if([self getPassCodeStatus]==1 && isPasswordChangeClicked == 1)
        {
            return 3;
        }
        
    }
    //else if([PassWord isEqualToString:@"(null)"] && isSetPasswordClicked == 0)
    else if(PassWord == nil  && isSetPasswordClicked == 0)
    {
        return 1;
    }
    else if(PassWord == nil && isSetPasswordClicked == 1)
    {
        return 2;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    //if (![PassWord isEqualToString:@"(null)"])
    if (PassWord != nil)
    {
        if ([self getPassCodeStatus]==0)
        {
            return 1;
        }
        else if([self getPassCodeStatus]==1 && isPasswordChangeClicked == 0)
        {
            return 1;
        }
        else if([self getPassCodeStatus]==1 && isPasswordChangeClicked == 1)
        {
            if(section==0 || section == 1)
            {
                return 1;
            }
            else if(section == 2)
            {
                return 3;
            }
            
        }
        
    }
    //else if([PassWord isEqualToString:@"(null)"] && isSetPasswordClicked == 0)
    else if(PassWord == nil  && isSetPasswordClicked == 0)
    {
        return 1;
    }
    //else if([PassWord isEqualToString:@"(null)"] && isSetPasswordClicked == 1)
    else if(PassWord == nil && isSetPasswordClicked == 1)
    {
        if(section==0)
        {
            return 1;
        }
        else if(section ==1)
        {
            return 3;
        }
        
    }
    
    
    /*
     return 1;
     if(section==0)
     {
     return 1;
     }
     else if(section == 1)
     {
     return 3;
     }
     */
    return 0;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UILabel *lbl;
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    nslog(@"\n getPassCodeStatus = %d",[self getPassCodeStatus]);
    
    
    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    
    cell.textLabel.textColor=[UIColor blackColor];
    cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:15.0];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    //cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    //if (![PassWord isEqualToString:@"(null)"])
    if (PassWord != nil)
    {
        if ([self getPassCodeStatus]==0)
        {
            
            if (indexPath.section==0)
            {
                
                //cell.textLabel.text=@"Password";
                cell.accessoryView=SwtPassCode;
                
                
                lbl=[[UILabel alloc] initWithFrame:CGRectMake(40, 5, 200, 30)];
                
                
               
                
                if(appDelegate.isIPad)
                {
                    lbl.frame = CGRectMake(40, 5, 200, 30);
                }
                
                
                
                
                
                lbl.text=@"Password";
                lbl.backgroundColor=[UIColor clearColor];
                lbl.textColor=[UIColor blackColor];
                lbl.font = [UIFont systemFontOfSize:15.0f];
                [cell.contentView addSubview:lbl];
                [lbl release];
                
                
                
                
                //cell.imageView.image = [UIImage imageNamed:@"password_lock_icon.png"];
                
                
                UIImageView*password_lock_imageView = [[UIImageView alloc] init];
                password_lock_imageView.frame = CGRectMake(0, 0,40,40);
                password_lock_imageView.image = [UIImage imageNamed:@"password_lock_icon.png"];
                [cell.contentView addSubview:password_lock_imageView];
                [password_lock_imageView release];
                
                
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
        }
        else if([self getPassCodeStatus]==1 && isPasswordChangeClicked == 0)
        {
            if (indexPath.section==0)
            {
                
                
                //cell.textLabel.text=@"Password";
                
                lbl=[[UILabel alloc] initWithFrame:CGRectMake(40, 5, 200, 30)];
                
                if(appDelegate.isIPad)
                {
                    lbl.frame = CGRectMake(40, 5, 200, 30);
                }
                
                lbl.text=@"Password";
                
                lbl.backgroundColor=[UIColor clearColor];
                
                lbl.textColor=[UIColor blackColor];
                lbl.font = [UIFont systemFontOfSize:15.0f];
                [cell.contentView addSubview:lbl];
                [lbl release];

                
                cell.accessoryView=SwtPassCode;
                
                
                UIImageView*password_lock_imageView = [[UIImageView alloc] init];
                password_lock_imageView.frame = CGRectMake(0, 0,40,40);
                password_lock_imageView.image = [UIImage imageNamed:@"password_lock_icon.png"];
                [cell.contentView addSubview:password_lock_imageView];
                [password_lock_imageView release];

                
                
                //cell.imageView.image = [UIImage imageNamed:@"password_lock_icon.png"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            else if(indexPath.section == 1)
            {
                lbl=[[UILabel alloc] initWithFrame:CGRectMake(40, 5, 200, 30)];
                
                if(appDelegate.isIPad)
                {
                    lbl.frame = CGRectMake(40, 5, 200, 30);
                }
                
                lbl.text=@"Change Password";
                lbl.font = [UIFont systemFontOfSize:15.0f];
                lbl.backgroundColor=[UIColor clearColor];
                lbl.textColor=[UIColor blackColor];
                [cell.contentView addSubview:lbl];
                [lbl release];
                
                
                UIImageView*password_change_imageView = [[UIImageView alloc] init];
                password_change_imageView.frame = CGRectMake(0, 0,40,40);
                password_change_imageView.image = [UIImage imageNamed:@"password_change_icon.png"];
                [cell.contentView addSubview:password_change_imageView];
                [password_change_imageView release];

                
                //cell.imageView.image = [UIImage imageNamed:@"password_change_icon.png"];
            }
            
        }
        else if([self getPassCodeStatus]==1 && isPasswordChangeClicked == 1)
        {
            if(indexPath.section==0)
            {
               // cell.textLabel.text=@"Password";
                cell.accessoryView=SwtPassCode;
                
                lbl=[[UILabel alloc] initWithFrame:CGRectMake(40, 5, 200, 30)];
                
                if(appDelegate.isIPad)
                {
                    lbl.frame = CGRectMake(40, 5, 200, 30);
                }
                
                lbl.text=@"Password";
                lbl.font = [UIFont systemFontOfSize:15.0f];
                lbl.backgroundColor=[UIColor clearColor];
                lbl.textColor=[UIColor blackColor];
                [cell.contentView addSubview:lbl];
                [lbl release];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                //cell.imageView.image = [UIImage imageNamed:@"password_lock_icon.png"];
                
                
                UIImageView*password_lock_imageView = [[UIImageView alloc] init];
                password_lock_imageView.frame = CGRectMake(0, 0,40,40);
                password_lock_imageView.image = [UIImage imageNamed:@"password_lock_icon.png"];
                [cell.contentView addSubview:password_lock_imageView];
                [password_lock_imageView release];
                
            }
            else if(indexPath.section == 1)
            {
                lbl=[[UILabel alloc] initWithFrame:CGRectMake(40, 5, 200, 30)];
                
                if(appDelegate.isIPad)
                {
                    lbl.frame = CGRectMake(40, 5, 200, 30);
                }
                
                //cell.imageView.image = [UIImage imageNamed:@"password_change_icon.png"];
                
                
                UIImageView*password_change_imageView = [[UIImageView alloc] init];
                password_change_imageView.frame = CGRectMake(0, 0,40,40);
                password_change_imageView.image = [UIImage imageNamed:@"password_change_icon.png"];
                [cell.contentView addSubview:password_change_imageView];
                [password_change_imageView release];
                
                
                lbl.text=@"Change Password";
                lbl.font = [UIFont systemFontOfSize:15.0f];
                lbl.backgroundColor=[UIColor clearColor];
                lbl.textColor=[UIColor blackColor];
                [cell.contentView addSubview:lbl];
                [lbl release];
            }
            else if(indexPath.section == 2)
            {
                CustomeCell_PassWord *cell;// = (CustomeCell_PassWord *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                //if(cell == nil)
                if(1)
                {
                    cell = [[[CustomeCell_PassWord alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                }
                cell.txt.delegate=self;
                cell.textLabel.textColor=[UIColor blackColor];
                //cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:15.0];
                cell.textLabel.font=[UIFont systemFontOfSize:15.0];

                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.txt.delegate=self;
                cell.txt.keyboardType = UIKeyboardTypeDefault;
                cell.txt.tag=5000+indexPath.row;
                cell.txt.autocorrectionType = UITextAutocorrectionTypeNo;
                cell.txt.autocapitalizationType = UITextAutocapitalizationTypeNone;
                cell.txt.delegate=self;
                if (indexPath.row==0)
                {
                    cell.lbl.text=@"Password";
                    cell.txt.placeholder=@"Password";
                    cell.txt.secureTextEntry=YES;
                    cell.txt.text = [textFieldDictionary objectForKey:@"5000"];
                    
                    //cell.imageView.image = [UIImage imageNamed:@"password_password_icon.png"];
                    
                    
                    UIImageView*password_password_imageView = [[UIImageView alloc] init];
                    password_password_imageView.frame = CGRectMake(0, 0,40,40);
                    password_password_imageView.image = [UIImage imageNamed:@"password_password_icon.png"];
                    [cell.contentView addSubview:password_password_imageView];
                    [password_password_imageView release];

                    
                    
                    
                }
                else if(indexPath.row==1)
                {
                    cell.lbl.text=@"Confirm Password";
                    cell.txt.placeholder=@"Confirm Password";
                    cell.txt.secureTextEntry=YES;
                    cell.txt.delegate=self;
                    cell.txt.text = [textFieldDictionary objectForKey:@"5001"];
                    
                    //cell.imageView.image = [UIImage imageNamed:@"password_confirm_password_icon.png"];
                    
                    UIImageView*password_confirm_password_imageView = [[UIImageView alloc] init];
                    password_confirm_password_imageView.frame = CGRectMake(0, 0,40,40);
                    password_confirm_password_imageView.image = [UIImage imageNamed:@"password_confirm_password_icon.png"];
                    [cell.contentView addSubview:password_confirm_password_imageView];
                    [password_confirm_password_imageView release];
                    
                    
                }
                else if(indexPath.row==2)
                {
                    NSString *passwordEmailId = [self GetPassWordHint];
                    if(passwordEmailId != nil && isPasswordEmailFirstTime == YES)
                    {
                        [textFieldDictionary setObject:passwordEmailId forKey:@"5002"];
                        cell.txt.text = passwordEmailId;
                        cell.txt.delegate=self;
                        isPasswordEmailFirstTime = FALSE;
                    }
                    else
                    {
                        cell.txt.text = [textFieldDictionary objectForKey:@"5002"];
                        cell.txt.delegate=self;
                    }
                    cell.lbl.text=@"Email ID";
                    cell.txt.placeholder=@"you@example.com";
                   
                    //cell.imageView.image = [UIImage imageNamed:@"password_email_btn_icon.png"];
                    
                    
                    
                    
                    UIImageView*password_email_imageView = [[UIImageView alloc] init];
                    password_email_imageView.frame = CGRectMake(0, 0,40,40);
                    password_email_imageView.image = [UIImage imageNamed:@"password_email_btn_icon.png"];
                    [cell.contentView addSubview:password_email_imageView];
                    [password_email_imageView release];

                    
                    cell.txt.keyboardType = UIKeyboardTypeEmailAddress;
                }
                
                // Configure the cell...
                
                return cell;
            }
            
        }
        
    }
   
    else if(PassWord == nil  && isSetPasswordClicked == 0)
    {
        if (indexPath.section==0)
        {
            
            //cell.textLabel.text=@"Password";
            
            lbl=[[UILabel alloc] initWithFrame:CGRectMake(40, 5, 200, 30)];
            
            if(appDelegate.isIPad)
            {
                lbl.frame = CGRectMake(40, 5, 200, 30);
            }
            
            lbl.text=@"Password";
            lbl.backgroundColor=[UIColor clearColor];
            lbl.textColor=[UIColor blackColor];
            [cell.contentView addSubview:lbl];
            lbl.font = [UIFont systemFontOfSize:15.0f];
            [lbl release];
            
            cell.accessoryView=SwtPassCode;
            //cell.imageView.image = [UIImage imageNamed:@"password_lock_icon.png"];
            
            UIImageView*password_lock_imageView = [[UIImageView alloc] init];
            password_lock_imageView.frame = CGRectMake(0, 0,40,40);
            password_lock_imageView.image = [UIImage imageNamed:@"password_lock_icon.png"];
            [cell.contentView addSubview:password_lock_imageView];
            [password_lock_imageView release];
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
    }
    //else if([PassWord isEqualToString:@"(null)"] && isSetPasswordClicked == 1)
    else if(PassWord == nil  && isSetPasswordClicked == 1)
    {
        
        CustomeCell_PassWord *cell;// = (CustomeCell_PassWord *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        //if (cell == nil)
        if (1)
        {
            cell = [[[CustomeCell_PassWord alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }

        
        if(indexPath.section==0)
        {
            
            //cell.textLabel.frame = CGRectMake(40, 12, 125, 21);
            //cell.imageView.image = [UIImage imageNamed:@"password_lock_icon.png"];
            
            UIImageView*password_lock_imageView = [[UIImageView alloc] init];
            password_lock_imageView.frame = CGRectMake(0, 0,40,40);
            password_lock_imageView.image = [UIImage imageNamed:@"password_lock_icon.png"];
            [cell.contentView addSubview:password_lock_imageView];
            [password_lock_imageView release];
            
            lbl=[[UILabel alloc] initWithFrame:CGRectMake(40, 5, 200, 30)];
            
            if(appDelegate.isIPad)
            {
                lbl.frame = CGRectMake(40, 5, 200, 30);
            }
            
            lbl.text=@"Password";
            lbl.backgroundColor=[UIColor clearColor];
            lbl.textColor=[UIColor blackColor];
            [cell.contentView addSubview:lbl];
            lbl.font = [UIFont systemFontOfSize:15.0f];
            [lbl release];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryView=SwtPassCode;
            return cell;
        }
        else if(indexPath.section ==1)
        {
                        
            cell.textLabel.textColor=[UIColor blackColor];
            //cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:17.0];
            cell.textLabel.font=[UIFont systemFontOfSize:15.0];
            //cell.backgroundColor=[UIColor clearColor];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.txt.delegate=self;
            cell.txt.keyboardType = UIKeyboardTypeDefault;
            cell.txt.tag=5000+indexPath.row;
            cell.txt.autocorrectionType = UITextAutocorrectionTypeNo;
            cell.txt.autocapitalizationType = UITextAutocapitalizationTypeNone;
            
            if (indexPath.row==0)
            {
                cell.lbl.text=@"Password";
                cell.txt.placeholder=@"Password";
                cell.txt.secureTextEntry=YES;
                cell.txt.text = [textFieldDictionary objectForKey:@"5000"];
                
                //cell.imageView.image = [UIImage imageNamed:@"password_password_icon.png"];
                
                
                UIImageView*password_password_imageView = [[UIImageView alloc] init];
                password_password_imageView.frame = CGRectMake(0, 0,40,40);
                password_password_imageView.image = [UIImage imageNamed:@"password_password_icon.png"];
                [cell.contentView addSubview:password_password_imageView];
                [password_password_imageView release];
                
                
            }
            else if(indexPath.row==1)
            {
                cell.lbl.text=@"Confirm Password";
                
                cell.txt.placeholder=@"Confirm Password";
                cell.txt.secureTextEntry=YES;
                cell.txt.text = [textFieldDictionary objectForKey:@"5001"];
                
                //cell.imageView.image = [UIImage imageNamed:@"password_confirm_password_icon.png"];
                
                
                UIImageView*password_confirm_imageView = [[UIImageView alloc] init];
                password_confirm_imageView.frame = CGRectMake(0, 0,40,40);
                password_confirm_imageView.image = [UIImage imageNamed:@"password_confirm_password_icon.png"];
                [cell.contentView addSubview:password_confirm_imageView];
                [password_confirm_imageView release];
                
                
            }
            else if(indexPath.row==2)
            {
                cell.lbl.text=@"Email ID";
                cell.txt.placeholder=@"you@example.com";
                cell.txt.text = [textFieldDictionary objectForKey:@"5002"];
                cell.txt.keyboardType = UIKeyboardTypeEmailAddress;
                
                //cell.imageView.image = [UIImage imageNamed:@"password_email_btn_icon.png"];
                
                
                UIImageView*password_email_imageView = [[UIImageView alloc] init];
                password_email_imageView.frame = CGRectMake(0, 0,40,40);
                password_email_imageView.image = [UIImage imageNamed:@"password_email_btn_icon.png"];
                [cell.contentView addSubview:password_email_imageView];
                [password_email_imageView release];
                
                
            }
            
            // Configure the cell...
            
            return cell;
            
        }
        
    }
    
    
    
    
    
    /*
     if (indexPath.section==0)
     {
     cell.textLabel.text=@"Password Lock";
     cell.accessoryView=SwtPassCode;
     }
     
     else
     {
     lbl=[[UILabel alloc] initWithFrame:CGRectMake(85, 5, 200, 30)];
     lbl.text=@"Change Password";
     lbl.backgroundColor=[UIColor clearColor];
     lbl.textColor=[UIColor blackColor];
     [cell.contentView addSubview:lbl];
     [lbl release];
     }
     */
    // Configure the cell...
    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(isPasswordChangeClicked == 0 && isSetPasswordClicked == 0)
    {
        if (indexPath.section==1)
        {
            UIAlertView *Notes;
            UITextField *txt;
            Notes=[[UIAlertView alloc] initWithTitle:@"Password" message:@"Enter Your Password:\n            " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            Notes.alertViewStyle = UIAlertViewStylePlainTextInput;
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                //txt=[[UITextField alloc] initWithFrame:CGRectMake(15, 80, 250, 27)];
            }
            else
            {
                //txt=[[UITextField alloc] initWithFrame:CGRectMake(15, 70, 250, 27)];
            }
            
           
            txt = [Notes textFieldAtIndex:0];
            txt.delegate = self;
            
            [txt setBorderStyle:UITextBorderStyleNone];
            [txt becomeFirstResponder];
            
            //[Notes addSubview:txt];
            
            txt.secureTextEntry=YES;
            txt.tag=2000;
            //[txt release];
        
            
            [Notes show];
            [Notes release];
        }
    }
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    [self.view endEditing:TRUE];
    toolBarKeyboard.hidden = TRUE;
    
    
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
    }

    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            toolBarKeyboard.frame = CGRectMake(0, 785,768, 44);
           // self.view.frame = CGRectMake(0, 0,768,1024);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            toolBarKeyboard.frame = CGRectMake(0,326,1024, 44);
          //  self.view.frame = CGRectMake(0, 0,1024,768);
        }

         [tbl_PassOn_Off reloadData];
        
    }
    
  
    
    
    
   
    
    
    
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    textField.delegate = self;
    PassWord = [self getPassWord];
    if(!appDelegate.isIPad)
    {
        nslog(@" origint = %f",textField.frame.origin.y);
        
        
        if(appDelegate.isIphone5)
        {
            tbl_PassOn_Off.frame = CGRectMake(0,tbl_PassOn_Off.frame.origin.y ,tbl_PassOn_Off.frame.size.width,240.0f);
            
        }
        else
        {
        
            tbl_PassOn_Off.frame = CGRectMake(0,tbl_PassOn_Off.frame.origin.y , tbl_PassOn_Off.frame.size.width,155.0f);
        }
        
        
        
        //[tbl_PassOn_Off scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];
    }
    
    
    UITextField *txt=(UITextField *)[self.view viewWithTag:5002];
    if (txt==textField)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Property Log Book" message:@"Please choose a valid Email ID as this will be used to send forgotten password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    nslog(@"textFieldShouldBeginEditing tag = %d",textField.tag);
    
    
    if(!appDelegate.isIPad)
    {
        if(isPasswordChangeClicked == 1)
        {
            [tbl_PassOn_Off setContentOffset:CGPointMake(0,120) animated:YES];
        }
        
        
        if(TagForNext > 5000)
        {
            if(isSetPasswordClicked == 1)
            {
                [tbl_PassOn_Off setContentOffset:CGPointMake(0,50) animated:YES];
            }
        }
        
    }
    
    
    //[textField becomeFirstResponder];
    toolBarKeyboard.hidden=NO;
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    nslog(@"\n toolBarKeyboard = %@",toolBarKeyboard);
    
    if(appDelegate.isIPad)
    {
        
        
        
        
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                toolBarKeyboard.frame = CGRectMake(0, 670,768, 44);
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                
            {
                toolBarKeyboard.frame = CGRectMake(0,326,1024, 44);
                
            }

        
        
       
    }
    
         nslog(@"\n toolBarKeyboard = %@",toolBarKeyboard);

    
    TagForNext=textField.tag;
    toolBarKeyboard.hidden = FALSE;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    if(appDelegate.isIPad)
    {
        if(!isDonePressed)
        {
            //return NO;
        }
        
    }
    
    nslog(@"\n textField = %d",textField.tag);
    nslog(@"\n textField = %@",textField.text);
    [textFieldDictionary setObject:textField.text forKey:[NSString stringWithFormat:@"%d",textField.tag]];
    nslog(@"\n textFieldDictionary = %@",textFieldDictionary);
    
    if(textField.tag == 5000)
    {
        UITextField *txt_Field1=(UITextField *)[self.view viewWithTag:5000];
        txt_Field1.text = [textFieldDictionary objectForKey:@"5000"];
        if(appDelegate.isIPad)
        {
            isDonePressed = NO;
        }
        
        
    }
    else if(textField.tag == 5001)
    {
        
        UITextField *txt_Field1=(UITextField *)[self.view viewWithTag:5001];
        txt_Field1.text = [textFieldDictionary objectForKey:@"5001"];
        if(appDelegate.isIPad)
        {
            isDonePressed = NO;
        }
        //[tbl_PassOn_Off reloadData];
    }
    else if(textField.tag == 5002)
    {
        
        UITextField *txt_Field1=(UITextField *)[self.view viewWithTag:5002];
        txt_Field1.text = [textFieldDictionary objectForKey:@"5002"];
        if(appDelegate.isIPad)
        {
            isDonePressed = NO;
        }
        //[tbl_PassOn_Off reloadData];
    }
    
    //tableview height in textfieldend editing etc…
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    
    
    return  YES;
}

#pragma mark - ToolBarDone

-(IBAction)DoneButtonClick:(id)sender
{
    
    isDonePressed = YES;
    NSInteger nextTag = TagForNext + 1;
    // Try to find next responder
    
    nslog(@"\n nextTag = %d",nextTag);
    
    if(nextTag == 5003)
    {
        UITextField *txt_Field1=(UITextField *)[self.view viewWithTag:5002];
        [textFieldDictionary setObject:txt_Field1.text forKey:@"5002"];
        nslog(@"\n txt_Field1.text = %@",txt_Field1.text);
        if([txt_Field1 isFirstResponder])
        {
            [txt_Field1 resignFirstResponder];
        }
        [self.view endEditing:TRUE];
        
    }
    
    
    if(TagForNext == 5000)
    {
        if(isSetPasswordClicked == 1)
        {
            [tbl_PassOn_Off setContentOffset:CGPointMake(0,50) animated:YES];
        }
        else if(isPasswordChangeClicked == 1)
        {
            [tbl_PassOn_Off setContentOffset:CGPointMake(0,120) animated:YES];
        }
        
    }
    
    
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder)
    {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
        isDonePressed = NO;
    }
    else
    {
        UITextField *txt_View=(UITextField *)[self.view viewWithTag:5000];
        if([txt_View isFirstResponder])
        {
            [txt_View resignFirstResponder];
        }
        
        
        UITextField *txt_Field=(UITextField *)[self.view viewWithTag:5001];
        if([txt_Field isFirstResponder])
        {
            [txt_Field resignFirstResponder];
        }
        
        UITextField *txt_Field1=(UITextField *)[self.view viewWithTag:5002];
        if([txt_Field1 isFirstResponder])
        {
            [txt_Field1 resignFirstResponder];
        }
        
        
        if(appDelegate.isIPad)
        {
            tbl_PassOn_Off.frame = CGRectMake(0,tbl_PassOn_Off.frame.origin.y , tbl_PassOn_Off.frame.size.width,911.0f);
        }
        else
        {
            
            
            
            if(appDelegate.isIphone5)
            {
                //tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,455.0f);
                
                
                if(appDelegate.isIOS7)
                {
                    tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height+25.0f);
                }
                else
                {
                    tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height);
                }
                
                
                
            }
            else
            {
                
                if(appDelegate.isIOS7)
                {
                    tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height+25.0f);
                }
                else
                {
                    tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height);
                }
                
                
            }

            
           
        }
        
        
        toolBarKeyboard.hidden=YES;
    }
}

#pragma mark - SavePassWord

-(void)DoneToolBarKey
{
    
    
    UITextField *txt=(UITextField *)[self.view viewWithTag:5000];
    UITextField *txt1=(UITextField *)[self.view viewWithTag:5001];
    UITextField *txt2=(UITextField *)[self.view viewWithTag:5002];
    
    if(txt2.text != nil)
    {
        [textFieldDictionary setObject:txt2.text forKey:[NSString stringWithFormat:@"%d",5002]];
    }
    
    
    nslog(@"\n textFieldDictionary = %@",textFieldDictionary);
    
    NSString *emailString = txt2.text; // storing the entered email in a string.
	
	NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    
    //[self SetPassCodeStatus:0];
    
    if (txt.text.length==0)
    {
        UIAlertView *Alert=[[UIAlertView alloc] initWithTitle:@"Password" message:@"Enter password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [Alert show];
        [Alert release];
    }
    else if(txt1.text.length>0)
    {
        if ([txt.text isEqualToString:txt1.text])
        {
            
            if (txt2.text.length==0)
            {
                UIAlertView *Alert=[[UIAlertView alloc] initWithTitle:@"Property Log Book" message:@"Enter your Email ID." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [Alert show];
                [Alert release];
            }
            else
            {
                if (([emailTest evaluateWithObject:emailString] != YES) || ([emailString isEqualToString:@""]))
                {
                    NSString *str =[NSString stringWithFormat:@"Please enter email address in you@example.com format."];
                    UIAlertView *Alert=[[UIAlertView alloc] initWithTitle:@"Invalid email format" message:str delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [Alert show];
                    [Alert release];
                }
                else
                {
                    [self SetPassWordHint:txt2.text];
                    [self SetPassWord:txt1.text];
                    [self SetPassCodeStatus:1];
                    
                    [textFieldDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",5000]];
                    [textFieldDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",5001]];
                    [textFieldDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",5002]];
                    
                    isSetPasswordClicked = 0;
                    isPasswordChangeClicked = 0;
                    PassWord = [self getPassWord];
                    
                    
                    
                    
                    //tableview height in textfieldend editing etc…
                    
                    if(!appDelegate.isIPad)
                    {
                        
                        if(appDelegate.isIphone5)
                        {
                            //tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,455.0f);
                            
                            
                            if(appDelegate.isIOS7)
                            {
                                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height+25.0f);
                            }
                            else
                            {
                                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height);
                            }
                            
                            
                            
                        }
                        else
                        {
                            
                            if(appDelegate.isIOS7)
                            {
                                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height+25.0f);
                            }
                            else
                            {
                                tbl_PassOn_Off.frame = CGRectMake(tbl_PassOn_Off.frame.origin.x,tbl_PassOn_Off.frame.origin.y,tbl_PassOn_Off.frame.size.width,self.view.frame.size.height);
                            }
                            
                            
                        }
                        
                    }
                    
                    
                    
                    
                    self.navigationItem.rightBarButtonItem = nil;
                    [tbl_PassOn_Off reloadData];
                    [self.navigationController popViewControllerAnimated:YES];
                    
                    
                }
            }
        }
        else
        {
            UIAlertView *Alert=[[UIAlertView alloc] initWithTitle:@"Wrong Password" message:@"Password entered do not match" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [Alert show];
            [Alert release];
        }
        
    }
    else if(txt1.text.length==0)
    {
        UIAlertView *Alert=[[UIAlertView alloc] initWithTitle:@"Password" message:@"Enter confirm password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [Alert show];
        [Alert release];
    }
}

-(void)dealloc
{
    
    nslog(@"\n dealloc in passwordOnOff..(this should be corrected)");
    //[super dealloc];
}

@end
