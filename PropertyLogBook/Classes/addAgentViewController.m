//
//  addAgentViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "addAgentViewController.h"
#import "agentCustomCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UIPlaceHolderTextView.h"
#define NUMBERS	@"0123456789."

@implementation addAgentViewController



- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning  in addAgentViewController\n");
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        [tblView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
    }
    
    
    if(appDelegate.isIOS7)
    {
        tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25, tblView.frame.size.width, tblView.frame.size.height+25);
        
    }

   
    
    
    
    textFieldIndex = -1;
	isClicked = 0;
    
    if (appDelegate.isAgent >=0)
    {
        self.navigationItem.title = @"EDIT AGENT";
        
    }
    else
    {
        self.navigationItem.title = @"ADD AGENT";
    }
  
    
    
    
    if(appDelegate.isIphone5)
    {
        keytoolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,244, self.view.frame.size.width, 44)];
    }
    else
    {
        keytoolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 156, self.view.frame.size.width, 44)];
    }
    
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(keytoolBar_cancel_clicked)];
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flexiItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(keytoolBar_done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(keytoolBar_done_clicked)];
    */
     
    [keytoolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexiItem,doneButton, nil]];
    //    toolBar.items = array;
    keytoolBar.barStyle = UIBarStyleBlack;
    [self.view addSubview:keytoolBar];
    
    [cancelBtn release];
    [flexiItem release];
    [doneButton release];
    
    keytoolBar.hidden = TRUE;
    
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    //[button setTitle:@"  Back" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    button.bounds = CGRectMake(0, 0,49.0,29.0);
    [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    
    
    [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
    
    /*
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(done_clicked)];
    */
    
    UIButton * save_button = [UIButton buttonWithType:UIButtonTypeCustom];
    [save_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    save_button.bounds = CGRectMake(0, 0,51.0,30.0);
    [save_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_save_button.png"] forState:UIControlStateNormal];
    [save_button addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:save_button] autorelease];
    
    
    agentLabelArray = [[NSMutableArray alloc]initWithObjects:@"Property Agent",@"Contact Person",@"Agent Commission(%)",@"Work Phone", @"Mobile", @"Email",@"Address",nil];
    
    placeArray = [[NSMutableArray alloc]initWithObjects:@"Company",@"Name",@"Commision",@"Phone",@"Mobile",@"Email Address", nil];
    agentDict = [[NSMutableDictionary alloc]init];
    for (int i = 0;i < [agentLabelArray count];i++)
    {
        [agentDict setObject:@"" forKey:[NSString stringWithFormat:@"%d",i]];
    }
    [agentDict setObject:@"" forKey:@"9999"];
    if (appDelegate.isAgent >=0 ) 
    {
        for (int i = 0; i<6;i++)
        {
            [agentDict setObject:[[appDelegate.agentArray objectAtIndex:appDelegate.isAgent]valueForKey:[NSString stringWithFormat:@"%d",i]] forKey:[NSString stringWithFormat:@"%d",i]];
        }
         [agentDict setObject:[[appDelegate.agentArray objectAtIndex:appDelegate.isAgent]valueForKey:@"9999"] forKey:@"9999"];
        [agentDict setObject:[NSNumber numberWithInt:[[[appDelegate.agentArray objectAtIndex:appDelegate.isAgent]valueForKey:@"rowid"]intValue]] forKey:@"rowid"];
    }
    agentDataArray = [[NSMutableArray alloc]init];
	
    tblView.backgroundColor = [UIColor clearColor];
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0, 1024, 1024)];

    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
	
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];
    
    //[self.view addSubview:appDelegate.bottomView];
    
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    if(appDelegate.isIPad)
    {
    [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];

    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(appDelegate.isIPad)
    {
        return  YES;
    }
    return  NO;
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{

    if(appDelegate.isIPad)
    {
        
        [appDelegate manageViewControllerHeight];
        [self.view endEditing:YES];
        keytoolBar.hidden = YES;
        
        
        
    }

    
    [tblView reloadData];
}

#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}


#pragma mark -

-(void)cancel_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)done_clicked
{
    nslog(@"\n agentDict = %@",agentDict);
    
    if (textFieldIndex >= 0 && textFieldIndex <9998)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldIndex];
        [agentDict setObject:textField.text forKey:[NSString stringWithFormat:@"%d",(textField.tag-2000)]];
    }
    else if (textFieldIndex == 9999)
    {
        UITextView *textView = (UITextView *)[self.view viewWithTag:textFieldIndex];
        [agentDict setObject:textView.text forKey:@"9999"];
    }
    if ([agentDataArray count]>0)
    {
        [agentDataArray removeAllObjects];
    }
    
    
    [agentDataArray addObject:agentDict];
    if ([[agentDict objectForKey:@"0"] length]>0)
    {
        if (appDelegate.isAgent < 0 )
        {
            [appDelegate AddAgentInfo:agentDataArray];
        }
        else
        {
            [appDelegate UpdateAgentInfo:agentDataArray rowid:[[[agentDataArray objectAtIndex:0]valueForKey:@"rowid"]intValue]];
            
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Property Agent is required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
}


-(void)keytoolBar_cancel_clicked
{
    keytoolBar.hidden = TRUE;
    UITextField *text = (UITextField *)[self.view viewWithTag:isClicked];
    [text resignFirstResponder];
    if (isClicked == 6)
    {
        UITextView *txtView = (UITextView *)[self.view viewWithTag:9999];
        [txtView resignFirstResponder];
        [self.view endEditing:TRUE];
    }
}
-(void)keytoolBar_done_clicked
{
   

    if (isClicked == 2005)
    {
		UITextView *txtView = (UITextView *)[self.view viewWithTag:9999];
        [txtView becomeFirstResponder];
        return;
    }
	if(isClicked == 6)
	{
		UITextView *txtView = (UITextView *)[self.view viewWithTag:9999];
		[txtView resignFirstResponder];
		
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                
                tblView.contentSize = CGSizeMake(1024,500);
            }
        }
        [tblView scrollsToTop];
		keytoolBar.hidden = TRUE;
        [self.view endEditing:TRUE];
	}
    else
    {
        
        

        UITextField *tempText = (UITextField *)[self.view viewWithTag:isClicked];
        [agentDict setObject:tempText.text forKey:[NSString stringWithFormat:@"%d",(isClicked-2000)]];
        
        UITextField *txtEmail = (UITextField *)[self.view viewWithTag:isClicked+1];
		
        [txtEmail becomeFirstResponder];

    }

}

#pragma mark text field methods



- (void)textFieldDidEndEditing:(UITextField *)textField 
{
	 nslog(@"\n textFieldDidEndEditing");
    
 
    

}	

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

    if(appDelegate.isIPad)
    {
        
        
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                keytoolBar.frame = CGRectMake(0,672,768,44);
            }
            else  if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                keytoolBar.frame = CGRectMake(0,328,1024,44);
            }

            
      
        
        
    }
    textField.delegate = self;
    

    
    keytoolBar.hidden = FALSE;
    isClicked = textField.tag;
    textFieldIndex = textField.tag;


    if (textField.tag > 2002)
    {
		NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:textField.tag%2000 inSection:0];
		[tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
		return YES;
    }
		
	return YES;

    
   
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
       keytoolBar.hidden = FALSE;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    nslog(@"textFieldShouldReturn");
	if (textField.tag < 2005)
	{

		UITextField *text = (UITextField *)[self.view viewWithTag:textField.tag+1];
		[text becomeFirstResponder];
	}
	else if(textField.tag == 2005)
	{
		keytoolBar.hidden = FALSE;
		isClicked = 6;
		
		UITextView *txtView = (UITextView *)[self.view viewWithTag:9999];
		//tblView.frame = CGRectMake(0, -(textField.tag%2000)*17, tblView.frame.size.width, tblView.frame.size.height);
		NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:0 inSection:1];
		[tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
		
		[txtView becomeFirstResponder];
		[textField resignFirstResponder];
		

	}
    else
    {
		//tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
		[tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:TRUE];
		[textField resignFirstResponder];
    }
    return YES;
}


-(void)textViewDidBeginEditing:(UITextView *)textView
{
     keytoolBar.hidden = FALSE;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [agentDict setObject:textField.text forKey:[NSString stringWithFormat:@"%d",(textField.tag-2000)]];
    if(appDelegate.isIPad)
    {
        keytoolBar.hidden = TRUE;
        [textField resignFirstResponder];
    }
    
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{// return NO to not change text
	NSCharacterSet *cs;
    NSString *filtered;
	if(textField.tag == 2002)
	{
		
		cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
		filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
		//nslog(@"%@ str",string);
		if([string isEqualToString:@"."]){
			if ([textField.text rangeOfString:@"."].location != NSNotFound) {
				// "." IS in myString
				//nslog(@"Avail");
				return NO;
				
			} else {
				// "." ISN'T in myString
				//nslog(@"Not Avail");
				
				return YES;
			}
        }
		else {
			return [string isEqualToString:filtered];
		}
    }
    else if (textField.tag == 2003 || textField.tag == 2004)
    {
        cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
		filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
		//nslog(@"%@ str",string);
		if([string isEqualToString:@"+"])
        {
			if ([textField.text rangeOfString:@"+"].location != NSNotFound) {
				// "." IS in myString
				//nslog(@"Avail");
				return NO;
				
			} else {
				// "." ISN'T in myString
				//nslog(@"Not Avail");
				
				return YES;
			}
        }
        else if ([string isEqualToString:@"#"])
        {
            if ([textField.text rangeOfString:@"#"].location != NSNotFound) {
				// "." IS in myString
				//nslog(@"Avail");
				return NO;
				
			} else {
				// "." ISN'T in myString
				//nslog(@"Not Avail");
				
				return YES;
			}

        }
        else if ( [string isEqualToString:@"*"])
        {
            if ([textField.text rangeOfString:@"*"].location != NSNotFound) {
				// "." IS in myString
				//nslog(@"Avail");
				return NO;
				
			} else {
				// "." ISN'T in myString
				//nslog(@"Not Avail");
				
				return YES;
			}

        }
		else {
			return [string isEqualToString:filtered];
		}
    }
	return TRUE;
}




- (void)textViewDidChange:(UITextView *)textView
{
//	//nslog(@"\n [textView.text length] = %d",[textView.text length]);
	if([textView.text length]==1)
	{
	//	//nslog(@"\n comes in codition...");
		textView.text = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	}
	
}



- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    
    isClicked = 6;
    
    textFieldIndex = textView.tag;
     
    keytoolBar.hidden = FALSE;
    if(appDelegate.isIPad)
    {
        
        
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                keytoolBar.frame = CGRectMake(0,672,768,44);
            }
            else  if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                tblView.contentSize = CGSizeMake(1024,900);
                tblView.contentOffset = CGPointMake(0,(textView.frame.origin.y+textView.frame.size.height));
                keytoolBar.frame = CGRectMake(0,328,1024,44);
            }

            
        
        
        
    }
    else
    {
        
        
        /*
        NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:6 inSection:0];
        [tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
        */
        tblView.contentOffset = CGPointMake(0,240);
        
        
        
        
    }

    
   

    keytoolBar.hidden = FALSE;
	//textView.text = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	return TRUE;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [agentDict setObject:textView.text forKey:@"9999"];
    keytoolBar.hidden = TRUE;
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
   // return 2;
     return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0)
    {
        return [agentLabelArray count];
    }
    /*
    else if (section == 1)
    {
        return 1;
    }
     */
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        static NSString *CellIdentifier = @"Cell";

        agentCustomCell *cell;// = (agentCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        //if (cell == nil) 
        if (1) 
        {
            cell = [[[agentCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        
        
        if(appDelegate.isIOS7)
        {
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.agentTextField.delegate = self;
    cell.agentTextField.tag = 2000+indexPath.row;
	cell.AddressTextView.hidden = TRUE;
	cell.AddressTextView.delegate = self;
    if (indexPath.row <6)
    {
        [cell.agentTextField setPlaceholder:[placeArray objectAtIndex:indexPath.row]];    
    }
    
    
    cell.agentLabel.text = [agentLabelArray objectAtIndex:indexPath.row];
    cell.agentTextField.text = [agentDict objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]];
    

        // Configure the cell...
    
        
    
    if (indexPath.row == 0)
    {
        //cell.imageView.image = [UIImage imageNamed:@"property_agent_icon.png"];
        
        
        UIImageView*property_agent_imageView = [[UIImageView alloc] init];
        property_agent_imageView.frame = CGRectMake(0, 0,40,40);
        property_agent_imageView.image = [UIImage imageNamed:@"property_agent_icon.png"];
        [cell.contentView addSubview:property_agent_imageView];
        [property_agent_imageView release];

        
        
    }

    else  if (indexPath.row == 1)
    {
       // cell.imageView.image = [UIImage imageNamed:@"add_agent_contact_person_icon.png"];
        
        UIImageView*add_agent_contact_person_imageView = [[UIImageView alloc] init];
        add_agent_contact_person_imageView.frame = CGRectMake(0, 0,40,40);
        add_agent_contact_person_imageView.image = [UIImage imageNamed:@"add_agent_contact_person_icon.png"];
        [cell.contentView addSubview:add_agent_contact_person_imageView];
        [add_agent_contact_person_imageView release];

        
    }

        
   else if (indexPath.row == 2)
    {
       
        //cell.imageView.image = [UIImage imageNamed:@"add_agent_commission_icon.png"];
        
        UIImageView*add_agent_commission_imageView = [[UIImageView alloc] init];
        add_agent_commission_imageView.frame = CGRectMake(0, 0,40,40);
        add_agent_commission_imageView.image = [UIImage imageNamed:@"add_agent_commission_icon.png"];
        [cell.contentView addSubview:add_agent_commission_imageView];
        [add_agent_commission_imageView release];
        
        
        cell.agentTextField.keyboardType = UIKeyboardTypeDecimalPad;
        if(appDelegate.isIPad)
        {
            cell.agentTextField.keyboardType = UIKeyboardTypePhonePad;
        }
        
        
    }
    
    else if (indexPath.row == 3 || indexPath.row == 4)
    {
        
        if(indexPath.row ==3)
        {
            
            //cell.imageView.image = [UIImage imageNamed:@"add_agent_work_phone_icon.png"];
            
            
            UIImageView*add_agent_work_phone_imageView = [[UIImageView alloc] init];
            add_agent_work_phone_imageView.frame = CGRectMake(0, 0,40,40);
            add_agent_work_phone_imageView.image = [UIImage imageNamed:@"add_agent_work_phone_icon.png"];
            [cell.contentView addSubview:add_agent_work_phone_imageView];
            [add_agent_work_phone_imageView release];

            
            
        }
        else if(indexPath.row == 4)
        {
            //cell.imageView.image = [UIImage imageNamed:@"add_agent_mobile_icon.png"];
            
            UIImageView*add_agent_mobile_imageView = [[UIImageView alloc] init];
            add_agent_mobile_imageView.frame = CGRectMake(0, 0,40,40);
            add_agent_mobile_imageView.image = [UIImage imageNamed:@"add_agent_mobile_icon.png"];
            [cell.contentView addSubview:add_agent_mobile_imageView];
            [add_agent_mobile_imageView release];
            
        }
        
        cell.agentTextField.keyboardType = UIKeyboardTypePhonePad;
    }
	
	else if(indexPath.row == 5)
	{
		cell.agentTextField.hidden = FALSE;
        
        UIImageView*password_email_imageView = [[UIImageView alloc] init];
        password_email_imageView.frame = CGRectMake(0, 0,40,40);
        password_email_imageView.image = [UIImage imageNamed:@"password_email_btn_icon.png"];
        [cell.contentView addSubview:password_email_imageView];
        [password_email_imageView release];

        //cell.imageView.image = [UIImage imageNamed:@"password_email_btn_icon.png"];
		
        
        cell.agentTextField.keyboardType = UIKeyboardTypeEmailAddress;
        
       // cell.agentTextField.backgroundColor = [UIColor grayColor];
       // cell.agentTextField.frame = CGRectMake(165,10,130, 30);
	}
        
    else if(indexPath.row == 6)
	{
        cell.agentLabel.hidden = TRUE;
		cell.agentTextField.hidden = TRUE;
        
        UIPlaceHolderTextView *AddressTextView;
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                AddressTextView = [[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(10, 7,600,100)];
            }
            else
            {
                AddressTextView = [[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(10, 7,900,100)];
            }
            
        }
        else
        {
            AddressTextView = [[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(10, 7,280, 100)];
        }
        AddressTextView.placeholder = @"Address";
        
        [AddressTextView setFont:[UIFont systemFontOfSize:15.0]];
        [[AddressTextView layer] setCornerRadius:2.0];
        AddressTextView.tag = 9999;
        AddressTextView.delegate = self;
        AddressTextView.backgroundColor = [UIColor clearColor];
        AddressTextView.text = [agentDict objectForKey:@"9999"];
        
        [cell.contentView addSubview:AddressTextView];
        
        
        
	}
        
    else
    {
		cell.agentTextField.hidden = FALSE;
        cell.agentTextField.keyboardType = UIKeyboardTypeAlphabet;
    }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 1)/*Sun:0004:Remove this after testing...*/
    {
        static NSString *CellIdentifier1 = @"Cell1";
        UITextView *AddressTextView;
        UITableViewCell *cell1;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        //if (cell1== nil) 
        if (1) 
        {
            cell1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    AddressTextView = [[UITextView alloc]initWithFrame:CGRectMake(10, 7,600,100)];
                }
                else
                {
                    AddressTextView = [[UITextView alloc]initWithFrame:CGRectMake(10, 7,900,100)];
                }
                
            }
            else
            {
                AddressTextView = [[UITextView alloc]initWithFrame:CGRectMake(10, 7,280, 100)];     
            }    
            
            
            [AddressTextView setFont:[UIFont systemFontOfSize:15.0]];
            [[AddressTextView layer] setCornerRadius:2.0];
            AddressTextView.tag = 9999;
            AddressTextView.delegate = self;
            AddressTextView.backgroundColor = [UIColor clearColor];
//            [[AddressTextView layer] setBorderColor:[UIColor lightGrayColor].CGColor];
//            [[AddressTextView layer] setBorderWidth:1.0];
            [cell1.contentView addSubview:AddressTextView];
        }
        else
        {
            AddressTextView = (UITextView *)[cell1.contentView viewWithTag:9999];
        }
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        AddressTextView.text = [agentDict objectForKey:@"9999"];
        
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell1;

    }
    return nil;
}



#pragma mark - Table view delegate

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{

       return @"";
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if(indexPath.row==6)
        {
            return 120;
        }
		return 44;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
