//
//  pdfViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 9/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <CoreText/CoreText.h>
#import <CoreText/CTRun.h>

#define kBorderInset            20.0
#define kBorderWidth            1.0
#define kMarginInset            10.0

//Line drawing
#define kLineWidth              1.0

@class PropertyLogBookAppDelegate;
@interface pdfViewController : UIViewController<MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate,UIPrintInteractionControllerDelegate> {
    
    IBOutlet UIWebView *pdfView;
    PropertyLogBookAppDelegate *appDelegate;
    
    NSString *fromString, *toString;
    
    int arrayIndex;
    NSDateFormatter *df;
    
    NSMutableArray *incomeTypeSummaryArray, *expenseTypeSummaryArray,*milageTypeSummaryArray;
    NSMutableArray *tempIncomeArray, *tempExpenseArray,*tempMilageArray;
    NSMutableDictionary *incomeSummeryDictionary,*expenseSummeryDictionary,*milageSummeryDictionary;
    
    NSMutableDictionary*income_types_dictionary;
    
    
    
    float totalIncomeAmount, totalExpenseAmount,totalMilage;
    int incomeY, expenseY, tempY;
    NSNumberFormatter *numberFormatter;
    NSString *rootPath;
    NSString *saveFileName;
    NSString *plistPath;
    NSString *saveFile_Name;
    NSString *newFilePath;
    CGGlyph glyph;
    CFIndex cfIndex;
    int len;
    
    NSString*localeString;
    UIImage*currencySymbolImage;
    UISegmentedControl *segmentedControl;
    UIBarButtonItem *segmentBarItem;
    NSMutableArray *array_notes;
    
    
    
    int final_position_after_bloack;
    
    
    NSMutableDictionary*agent_information;
    CFAttributedStringRef attStr;
    CGContextRef pdf;
    
     NSOperationQueue *operationQueue;
    
     CGSize pageSize;
    CGRect pageRect;
    
    CGContextRef    currentContext;
    
}
@property (nonatomic, strong)NSString *fromString, *toString;
@property (nonatomic, readwrite)int arrayIndex;
@property (nonatomic,strong)UIImage*currencySymbolImage;
-(void)showEmail;
-(void)showPrint;

-(CGSize)calculate_size:(NSString*)string_to_calculate font_size:(float)font_size;

@end
