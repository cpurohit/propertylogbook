//
//  ExcelGenerationViewController.h
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 13/04/13.
//
//

#import <UIKit/UIKit.h>

#import "PropertyLogBookAppDelegate.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface ExcelGenerationViewController : UIViewController<MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate,UIPrintInteractionControllerDelegate>
{
    PropertyLogBookAppDelegate *appDelegate;
    
    NSString *fromString, *toString;
    int arrayIndex;
    NSDateFormatter *df;
    NSMutableDictionary*agent_information;
    UISegmentedControl *segmentedControl;
    UIBarButtonItem *segmentBarItem;
    
    NSString *rootPath;
    NSString *saveFileName;
    NSString *plistPath;
    NSString *saveFile_Name;
    NSString *newFilePath;
    NSNumberFormatter *numberFormatter;
    NSMutableArray *incomeTypeSummaryArray, *expenseTypeSummaryArray,*milageTypeSummaryArray;
    NSMutableDictionary *incomeSummeryDictionary,*expenseSummeryDictionary;
    
    NSString * temp_string;
    
    IBOutlet UIWebView *excelView;

    
}

@property (nonatomic, strong)NSString *fromString, *toString;
@property (nonatomic, readwrite)int arrayIndex;



@end
