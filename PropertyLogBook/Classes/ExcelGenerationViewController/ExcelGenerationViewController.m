//
//  ExcelGenerationViewController.m
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 13/04/13.
//
//

#import "ExcelGenerationViewController.h"

@interface ExcelGenerationViewController ()

@end


@implementation ExcelGenerationViewController

@synthesize fromString, toString, arrayIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDelegate=(PropertyLogBookAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    /*
    UIImage *image = [UIImage imageNamed:@"propertyLogBook.png"];
    */
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    button.bounds = CGRectMake(0, 0,49.0,29.0);
    [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    incomeTypeSummaryArray = [[NSMutableArray alloc]init];
    expenseTypeSummaryArray = [[NSMutableArray alloc]init];
    milageTypeSummaryArray= [[NSMutableArray alloc]init];
    
    
    
    
    
    
    if([appDelegate.propertyDetailArray count])
    {
        [appDelegate selectPropertyDetail]; 
    }
   
    if([appDelegate.agentArray count]==0)
    {
       [appDelegate selectAgentType];
    }
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
    
        [appDelegate selectCurrencyIndex];
    }
    
    /*
     [appDelegate selectIncomeExpenseByProperty:[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"]];
    */
    
    [appDelegate selectIncomeExpenseByPropertyForRange:[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"] startDate:[appDelegate fetchDateInCommonFormate:[fromString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]] endDate:[appDelegate fetchDateInCommonFormate:[toString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]];
    
    
    NSDate*fromDate = [appDelegate.regionDateFormatter dateFromString:[fromString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    
    
    NSDate*toDate = [appDelegate.regionDateFormatter dateFromString:[toString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    
    
    nslog(@"\n fromDate in pdf= %@",fromDate);
    nslog(@"\n toDate in pdf = %@",toDate);
    
    
  
    
    
    numberFormatter = [[NSNumberFormatter alloc] init] ;
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [numberFormatter setCurrencyCode:appDelegate.curCode];
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [numberFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [numberFormatter setLocale:locale];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [numberFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [numberFormatter setLocale:locale];
            //[locale release];
            
        }
        
        else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [numberFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [numberFormatter setLocale:locale];
            //[locale release];
            
        }
        
    }
    
    nslog(@"\n appDelegate.propertyDetailArray  = %@",[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]);
    
    
    
    agent_information = [appDelegate getAgentInforById:[[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] valueForKey:@"agent_pk"] intValue]];
    
    
    
    nslog(@"\n appDelegate.incomeExpenseSummery = %@",appDelegate.incomeExpenseSummery);
    
    for (int i =0; i<[appDelegate.incomeExpenseSummery count];i++)
    {
        int from;
        from = [fromDate compare:[[appDelegate.incomeExpenseSummery objectAtIndex:i]valueForKey:@"date"]];
        
        int to ;
        
        if ([toDate isEqualToDate:[[appDelegate.incomeExpenseSummery objectAtIndex:i]valueForKey:@"date"]])
        {
            to = 1;
        }
        else
        {
            to = [toDate compare:[[appDelegate.incomeExpenseSummery objectAtIndex:i]valueForKey:@"date"]];
        }
        if (from <= 0  && to > 0)
        {
            if ([[[appDelegate.incomeExpenseSummery objectAtIndex:i]valueForKey:@"incomeExpense"]intValue] == 1)
            {
                
                [incomeTypeSummaryArray addObject:[appDelegate.incomeExpenseSummery objectAtIndex:i]];
            }
            else if([[[appDelegate.incomeExpenseSummery objectAtIndex:i]valueForKey:@"incomeExpense"]intValue] == 0)
            {
                
                [expenseTypeSummaryArray addObject:[appDelegate.incomeExpenseSummery objectAtIndex:i]];
            }
            else/*
                 SUN:004
                 MILAGE....
                 */
            {
                [milageTypeSummaryArray addObject:[appDelegate.incomeExpenseSummery objectAtIndex:i]];
                
            }

            
        }
    }
    
    
    nslog(@"\n incomeTypeSummaryArray = %@",incomeTypeSummaryArray);
    
    
    nslog(@"\n agent_information  = %@",agent_information);
    
    
    self.navigationItem.title = [[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] valueForKey:@"0"];
    
    segmentedControl = [[UISegmentedControl alloc] initWithItems:
                        [NSArray arrayWithObjects:@"Print",@"Email",nil]];
	[segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
	segmentedControl.frame = CGRectMake(0, 0, 90, 30);
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
	segmentedControl.momentary = YES;
	
	segmentBarItem = [[UIBarButtonItem alloc] initWithCustomView:segmentedControl];
	//[segmentedControl release];
	
	self.navigationItem.rightBarButtonItem = segmentBarItem;
	//[segmentBarItem release];
    
    
    nslog(@"currency code === %@",appDelegate.curCode);
    
    /*
	NSString *error;
    */
    
    rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    saveFileName=[NSString stringWithFormat:@"%@%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"],@".plist"];
	saveFileName  = [saveFileName stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    
    plistPath = [rootPath stringByAppendingPathComponent:saveFileName];
    saveFile_Name=[NSString stringWithFormat:@"%@%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"],@".csv"];
    
    
    saveFile_Name  = [saveFile_Name stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    
    newFilePath =	[rootPath stringByAppendingPathComponent:saveFile_Name];
	
   
    //[newFilePath retain];
    
    
    NSError *file_error;
    
    
    NSString *dollar;
    
    /*
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
    */
    
    
    
    
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [currencyFormatter setCurrencyCode:appDelegate.curCode];
        /**
        dollar = [NSString stringWithFormat:@"%@",[currencyFormatter currencySymbol]] ;
         */
    }
    else
    {
        [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        /**
        dollar = [NSString stringWithFormat:@"%@",[currencyFormatter currencySymbol]] ;
        */
    }
    
    
    
    
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    dollar = [currencyFormatter currencyCode];
    
    nslog(@"\n dollar = %@",dollar);
   // [currencyFormatter release];
    
    
    
    NSString *property_report_title = @"PROPERTY REPORT";
    
    NSString *property_name         =[NSString stringWithFormat:@"\"%@\"", [[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"0"]];
    
    NSString *property_address      = [NSString stringWithFormat:@"\"%@\"", [[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"address"]];;
    
    NSString *property_type =  [NSString stringWithFormat:@"\"%@\"", [[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"propertytype"]];
    
    
    
    
    NSString *from_and_to_date   =   [NSString stringWithFormat:@"%@,\"%@\",%@,\"%@\" ",@"From",fromString,@"To",toString];
    
    nslog(@"\n property_name = %@",property_name);
    
    
    /*
     NSString *property_report_title = @"developer,link\niPhone Developer Tips,http://iPhoneDevelopTips.com,\"24,04,2013\"";
     */
    
    NSString*property_final_string = [NSString stringWithFormat:@"%@\n\n%@\n%@\n%@\n%@\n",
                                      property_report_title,
                                      property_name,
                                      property_address,
                                      property_type,
                                      from_and_to_date];
    
    
    
    /*Starts: Property Financials */
    
    
    
    NSString *property_Finance_title = @"PROPERTY FINANCIALS";
    
    
    
    
    /*
    NSString *property_purchase_price         =[NSString stringWithFormat:@"Purchase Price,%@ \"%@\" ",dollar,[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"8"]];
    
     
     
    */

    //removed quote from all amount values as in db we are not storing commas etc..
    
    float purchase_price = [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"8"]floatValue];
    
    /*
    NSString *property_purchase_price         =[NSString stringWithFormat:@"Purchase Price,%@ %.2f ",dollar,[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"8"]];
*/

    NSString *property_purchase_price         =[NSString stringWithFormat:@"Purchase Price,%@ %.2f ",dollar,purchase_price];

    float market_value = [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"9"]floatValue];
    
    /*
    NSString *property_market_value      = [NSString stringWithFormat:@"Market Value,%@ %@",dollar,[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"9"]];;
    */

    NSString *property_market_value      = [NSString stringWithFormat:@"Market Value,%@ %.2f",dollar,market_value];;

    
    NSString *property_equity =  [NSString stringWithFormat:@"Equity,%@ %@",dollar,[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"zEquity"]];
    
    
    NSString*proprty_finance_final_string = [NSString stringWithFormat:
                                             @"%@\n\n%@\n%@\n%@",
                                             property_Finance_title,
                                             property_purchase_price,
                                             property_market_value,
                                             property_equity
                                             ];
    
    /*Ends: Property Financials */
    
    
    //
    
    NSString*loan_information_final_string=@"";
    
    if([[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"fullypaid"] intValue]==0)
    {
        
        NSString *loan_information_title = @"LOAN INFORMATION";
        
        NSString *mortage_bank =[NSString stringWithFormat:@"Mortgage Bank,\"%@\" ",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"11"]];
        
        NSString *loan_term_in_years   = [NSString stringWithFormat:@"Loan Term (years),\"%@\" (%@) ",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"14"],[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"repayment_term"]];
        
        
        float float_loan_borrowed = [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"12"]floatValue];
        
        /*
        NSString *loan_borrowed      = [NSString stringWithFormat:@"Loan borrowed,%@ %@",dollar,[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"12"]];
        */

        NSString *loan_borrowed      = [NSString stringWithFormat:@"Loan borrowed,%@ %.2f",dollar,float_loan_borrowed];

        
        float float_loan_outstanding = [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"13"]floatValue];
        
        /*
        NSString *loan_outstanding      = [NSString stringWithFormat:@"Loan outstanding,%@ %@",dollar,[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"13"]];
        */
        
        NSString *loan_outstanding      = [NSString stringWithFormat:@"Loan outstanding,%@ %.2f",dollar,float_loan_outstanding];

        
        float float_repayment_amount = [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"15"]floatValue];
        
        /*
        NSString *repayment_amount      = [NSString stringWithFormat:@"Repayment amount,%@ %@",dollar,[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"15"]];
        */

        NSString *repayment_amount      = [NSString stringWithFormat:@"Repayment amount,%@ %.2f",dollar,float_repayment_amount];

        
        
        NSString *repayment_frequency      = [NSString stringWithFormat:@"Repayment frequency, %@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"repayment"]];
        
        
        loan_information_final_string = [NSString stringWithFormat:@"\n\n%@\n\n%@\n%@\n%@\n%@\n%@\n%@",
                                         
                                         loan_information_title,
                                         mortage_bank,
                                         loan_term_in_years,
                                         loan_borrowed,
                                         loan_outstanding,
                                         repayment_amount,
                                         repayment_frequency
                                         ];
        
    }
    
    
    /*Starts: Lease Information*/
    
    
    NSString*lease_information_final_string = @"";
    
    if([[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"lease"] intValue]==1)
    {
        
        
        NSString *lease_information_title = @"LEASE INFORMATION";
        
        float float_rental_income = [[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"rental_income"]floatValue];
        
        /*
        NSString *rental_income =[NSString stringWithFormat:@"Rental Income,%@ %@ ",dollar,[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"rental_income"]];
        */
        
        NSString *rental_income =[NSString stringWithFormat:@"Rental Income,%@ %.2f ",dollar,float_rental_income];

        
        
        NSString *tenant_information   = [NSString stringWithFormat:@"Tenant Information,\"%@\"",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"tenant_information"]];
        
        NSString *lease_start_date      = [NSString stringWithFormat:@"Lease Start Date,\"%@\"",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"leasestart"]];
        
        
        NSString *lease_end_date      = [NSString stringWithFormat:@"Lease End Date,\"%@\"",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"leaseend"]];
        
        
        
        lease_information_final_string = [NSString stringWithFormat:@"\n\n%@\n\n%@\n%@\n%@\n%@",
                                          
                                          lease_information_title,
                                          rental_income,
                                          tenant_information,
                                          lease_start_date,
                                          lease_end_date
                                          
                                          ];
        
        
        
        
    }
    /*Ends: Lease Information*/
    
    NSString*agent_information_final_string = @"";
    
    if([[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex] objectForKey:@"agentManage"] intValue]==1)
    {
        
        
        
        
        
        NSString *agent_information_title = [NSString stringWithFormat:@"PROPERTY MANAGED BY,%@",[agent_information objectForKey:@"propertyAgent"]];
        
        NSString *agent_commission =[NSString stringWithFormat:@"Agent Commission,\"%@\" %@",[agent_information objectForKey:@"AgentCommission"],@"%"];
        
        NSString *contact_person   = [NSString stringWithFormat:@"Contact Person,\"%@\"",[agent_information objectForKey:@"ContactPerson"]];
        
        NSString *address      = [NSString stringWithFormat:@"Address,\"%@\"",[agent_information objectForKey:@"address"]];
        
        
        NSString *phone      = [NSString stringWithFormat:@"Phone,\"%@\"",[agent_information objectForKey:@"WorkPhone"]];
        
        NSString *mobile      = [NSString stringWithFormat:@"Mobile,\"%@\"",[agent_information objectForKey:@"Mobile"]];
        
        NSString *email      = [NSString stringWithFormat:@"Email,\"%@\"",[agent_information objectForKey:@"Email"]];
        
        
        
        agent_information_final_string = [NSString stringWithFormat:@"\n\n%@\n\n%@\n%@\n%@\n%@\n%@\n%@",
                                          agent_information_title,
                                          agent_commission,
                                          contact_person,
                                          address,
                                          phone,
                                          mobile,
                                          email
                                          ];
        
    }
    
    
    
    
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
	[incomeTypeSummaryArray sortUsingDescriptors:[NSArray arrayWithObject:dateDescriptor]];
    [expenseTypeSummaryArray sortUsingDescriptors:[NSArray arrayWithObject:dateDescriptor]];
   // [dateDescriptor release];

    /*Starts :Income Transactions*/
    
    NSString*income_transaction_title = @"INCOME TRANSACTIONS\n";
    
    NSString*income_transaction_title_headers = @"";
    
    NSString*income_summary_string = @"";
    
    float total_income_amount = 0;
    
    if ([incomeTypeSummaryArray count]>0)
    {
        
        income_transaction_title_headers = @"Date,Income Type,Income Type,Note";
        
        
        for (int i =0;i<[incomeTypeSummaryArray count];i++)
        {
            
            
            
            
            
            NSString*date = [NSString stringWithFormat:@"%@",[appDelegate.regionDateFormatter stringFromDate:[[incomeTypeSummaryArray objectAtIndex:i]valueForKey:@"date"]]];;
            
            date = [date stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            nslog(@"\n date = %@",date);
            
            NSString*income_type = [NSString stringWithFormat:@"\"%@\"",[[incomeTypeSummaryArray objectAtIndex:i]valueForKey:@"type"]];
            
            
            NSString*amount = [NSString stringWithFormat:@"%@ %@",dollar,[[incomeTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]];
            
            total_income_amount +=[[[incomeTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]floatValue];
            
            
            NSString *notes = [[incomeTypeSummaryArray objectAtIndex:i]valueForKey:@"notes"];
            
            income_summary_string = [income_summary_string stringByAppendingFormat:@"\" %@\",%@,%@,%@\n",date,income_type,amount,notes];
            
        }
    }
    else
    {
        income_summary_string = @"No Income Transactions Found";
    }
    
    if([incomeTypeSummaryArray count]>0)
    {
       
         income_summary_string = [income_summary_string stringByAppendingFormat:@"\" %@\",%@,%@ %.2f,%@\n",@"",@"Total Income",dollar,total_income_amount,@""];
    }
    
    
    /*Ends:Income Transactions*/
    
    
    
    
    /*Starts :expense Transactions*/
    
    float total_expense_amount = 0;
    
    NSString*expense_transaction_title = @"EXPENSE TRANSACTIONS\n";
    
    NSString*expense_transaction_title_headers = @"";
    
    NSString*expense_summary_string = @"";
    
    if ([expenseTypeSummaryArray count]>0)
    {
        
        expense_transaction_title_headers = @"Date,Expense Type,Expense Type,Note";
        
        
        for (int i =0;i<[expenseTypeSummaryArray count];i++)
        {
            
            
            
            
            
            NSString*date = [NSString stringWithFormat:@"\" %@\"",[appDelegate.regionDateFormatter stringFromDate:[[expenseTypeSummaryArray objectAtIndex:i]valueForKey:@"date"]]];;
            NSString*income_type = [NSString stringWithFormat:@"\"%@\"",[[expenseTypeSummaryArray objectAtIndex:i]valueForKey:@"type"]];
            NSString*amount = [NSString stringWithFormat:@"%@ %@",dollar,[[expenseTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]];
            
            total_expense_amount +=[[[expenseTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]floatValue];
            
            NSString *notes = [[expenseTypeSummaryArray objectAtIndex:i]valueForKey:@"notes"];
            
            expense_summary_string = [expense_summary_string stringByAppendingFormat:@"%@,%@,%@,%@\n",date,income_type,amount,notes];
            
        }
    }
    else
    {
        expense_summary_string = @"No Expense Transactions Found";
    }
    
    
    if([expenseTypeSummaryArray count]>0)
    {
        
        expense_summary_string = [expense_summary_string stringByAppendingFormat:@"\" %@\",%@,%@ %.2f,%@\n",@"",@"Total Expense",dollar,total_expense_amount,@""];
    }
    
    
    /*Ends: expense Transactions*/

    
    
    
    /*Starts :Mileage Transactions*/
    
    NSString*milage_transaction_title_headers = @"";
    
    NSString*milage_summary_string = @"";
     NSString*milage_transaction_title = @"";
    
    float total_milage_amount = 0;

    if(appDelegate.is_milege_on)
    {
        milage_transaction_title = @"Mileage TRANSACTIONS\n";
        
        if ([milageTypeSummaryArray count]>0)
        {
            
            milage_transaction_title_headers = @"Date,Type,Total units,Note";
            
            
            for (int i =0;i<[milageTypeSummaryArray count];i++)
            {
                
                
                
                
                
                NSString*date = [NSString stringWithFormat:@"\" %@\"",[appDelegate.regionDateFormatter stringFromDate:[[milageTypeSummaryArray objectAtIndex:i]valueForKey:@"date"]]];;
                
                
                
                NSString*mileage_type = [NSString stringWithFormat:@"\"%@\"",[[milageTypeSummaryArray objectAtIndex:i]valueForKey:@"type"]];
                
                
                NSString*amount = [NSString stringWithFormat:@"%@ %@",[[milageTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"],[appDelegate.prefs objectForKey:@"milage_unit"]];
                
                total_milage_amount +=[[[milageTypeSummaryArray objectAtIndex:i] objectForKey:@"amount"]floatValue];
                
                NSString *notes = [[milageTypeSummaryArray objectAtIndex:i]valueForKey:@"notes"];
                
                milage_summary_string = [milage_summary_string stringByAppendingFormat:@"%@,%@,%@,%@\n",date,mileage_type,amount,notes];
                
            }
        }
        else
        {
            milage_summary_string = @"No Mileage Transactions Found";
        }
        
        
        if([milageTypeSummaryArray count]>0)
        {
            
            milage_summary_string = [milage_summary_string stringByAppendingFormat:@"\" %@\",%@,%.2f %@,%@\n",@"",@"Total Mileage",total_milage_amount,[appDelegate.prefs objectForKey:@"milage_unit"],@""];
        }
        

    }
    
    
    /*Ends: Mileage Transactions*/

    
    
    
    
    
    
    NSString*csv_final_string = [NSString stringWithFormat:@"%@\n\n%@%@%@%@\n\n%@\n%@\n\n%@\n\n%@\n%@\n\n%@\n\n%@\n%@\n\n%@",
                                 property_final_string,//0
                                 proprty_finance_final_string,//1
                                 loan_information_final_string,//2
                                 lease_information_final_string,//3
                                 agent_information_final_string,//4
                                 income_transaction_title,//5
                                 income_transaction_title_headers,//6
                                 income_summary_string,//7
                                 expense_transaction_title,//8
                                 expense_transaction_title_headers,//9
                                 expense_summary_string,//10
                                milage_transaction_title,//11
                                 milage_transaction_title_headers,//12
                                 milage_summary_string//13
                                 
                                 
                                 ];
    
    
    [csv_final_string writeToFile:newFilePath atomically:YES encoding:NSUTF8StringEncoding error:&file_error];
    
    
    
    NSURL *filePath=[[NSURL alloc] initFileURLWithPath:newFilePath];
	[excelView loadRequest:[NSURLRequest requestWithURL:filePath]];
    
    CGSize fitsSize = [excelView sizeThatFits:CGSizeMake(CGRectGetWidth(self.view.bounds), CGFLOAT_MAX)];
    nslog(@"\n fitsSize = %@",NSStringFromCGSize(fitsSize));
    
   // [filePath release];
    
    
    
}

-(void)cancel_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)segmentAction:(id)sender
{
	
	if([sender selectedSegmentIndex] == 1)
    {
        
		[self showEmail];
		nslog(@"Segment 1 preesed");
	}else{
        [self showPrint];
		nslog(@"Segment 2 preesed");
	}
}


-(void)showPrint
{
    rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    saveFile_Name=[NSString stringWithFormat:@"%@%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"],@".csv"];
    
    saveFile_Name  = [saveFile_Name stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    
    
    newFilePath =	[rootPath stringByAppendingPathComponent:saveFile_Name];
    NSString *path = newFilePath;
    NSData *dataFromPath = [NSData dataWithContentsOfFile:path];
    
    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    
    //if(printController && [UIPrintInteractionController canPrintData:dataFromPath])
    if(printController)
    {
        
        printController.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [path lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        printController.printInfo = printInfo;
        printController.showsPageRange = YES;
        printController.printingItem = dataFromPath;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                nslog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
            }
        };
        
        
        if(appDelegate.isIPad)
        {
            
            //[printController presentAnimated:YES completionHandler:completionHandler];
            
            /*
             [printController presentFromRect:segmentedControl.bounds inView:self.view animated:TRUE completionHandler:completionHandler];
             
             */
            
            [printController presentFromBarButtonItem:segmentBarItem animated:TRUE completionHandler:completionHandler];
            
            
        }
        else
        {
            [printController presentAnimated:YES completionHandler:completionHandler];
        }
        
        
    }
}

-(void)showEmail
{
    if( [MFMailComposeViewController canSendMail])
    {
        rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        saveFile_Name=[NSString stringWithFormat:@"%@%@",[[appDelegate.propertyDetailArray objectAtIndex:arrayIndex]valueForKey:@"0"],@".csv"];
        saveFile_Name  = [saveFile_Name stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
        
        
        
        newFilePath =	[rootPath stringByAppendingPathComponent:saveFile_Name];
        
        MFMailComposeViewController *sendEmail=[[MFMailComposeViewController alloc] init];
        [sendEmail setSubject:@"Property Report"];
        sendEmail.mailComposeDelegate = self;
        NSData *data = [NSData dataWithContentsOfFile:newFilePath];
        
        [sendEmail addAttachmentData:data mimeType:@"Application/csv" fileName:saveFile_Name];
        
        [sendEmail.navigationBar setBarStyle:UIBarStyleBlackOpaque];
        
        [self presentModalViewController:sendEmail animated:YES];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please setup your mail account." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
		[alert show];
		//[alert release];
    }
    
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
