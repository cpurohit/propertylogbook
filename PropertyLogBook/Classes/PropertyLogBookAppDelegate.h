//
//  PropertyLogBookAppDelegate.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Crittercism.h"

@interface PropertyLogBookAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate,UIAlertViewDelegate,UIActionSheetDelegate>
{
    //Sample change from home
    
    UIWindow *window;
    UITabBarController *tabBarController;
    NSMutableArray *incomeTypeDBArray, *expenseTypeArray, *reminderTypeArray, *propertyTypeArray, *propertyAddArray;
    int reminderRowid;
    BOOL fromProperty;
    NSString *propertyTypeStr,*currencyTypeStr;
    NSMutableArray *propertyListArray;
    NSMutableArray *currencyArray;
    NSMutableArray *agentArray;
    int isProperty;
    BOOL innerUpdate;
    int badgeCount;
    int isAgent;
    BOOL isFromPropertyAgent;
    NSString *agentTypeStr;
    NSString *agent_pk;
    
    NSMutableArray *propertyDetailArray, *reminderArrayByProperty;
    
    NSString *incomeProperty, *incomeString;
    BOOL isIncome;
    int incomeExpenseIndex;
    NSMutableArray *incomeExpenseSummery;
    BOOL isIncomeExpenseUpdate;
    
    
    BOOL isPropertyImageUpdate;
    NSMutableArray *incomeExpenseArray;
    int isFromIncomeExpenseView;
    
    NSString *reminderTypeStr, *reminderPropertyStr;
    NSMutableArray *reminderArray;
    UIImage *propertyImage;
    
    int numberScreen;
    int equityIndex;
    int currencyIndex;
    NSString *curCode;
    int reminderRowID;
    
    
    BOOL isFirst;
    
    
    /*Starts:Chirag*/
    int decimalScale;
    /*Starts:Chirag*/
    
    BOOL isIPad;
    
    NSMutableArray*currencyWithDollarSymbol;
    
    
    NSDateFormatter *regionDateFormatter;
    NSUserDefaults*prefs;
    
    BOOL isTakingBackupFromiCloud;
    
    /*
    UIImage*all_selected;
    UIImage*all;
    UIImage*barchart_selected;
    UIImage*barchart;
    UIImage*both_selected;
    UIImage*both;
    UIImage*dash_ipad_expense;
    UIImage*dash_ipad_expense_selected;
    UIImage*dash_ipad_income_selected;
    UIImage*dash_ipad_income;
    UIImage*dash_expense;
    UIImage*dash_expense_selected;
    UIImage*dash_income_selected;
    UIImage*dash_income;
    UIImage*expense_selected;
    UIImage*expense;
    UIImage*income_selected;
    UIImage*income;
    UIImage*mtd_selected;
    UIImage*mtd;
    UIImage*pie_chart;
    UIImage*piechart_selected;
    UIImage*table_selected;
    UIImage*table;
    UIImage*ytd_selected;
    UIImage*ytd;
    UIImage*expense_ipad_selected;
    UIImage*expense_ipad;
    UIImage*income_ipad_selected;
    UIImage*income_ipad;
    UIImage*both_ipad_selected;
    UIImage*both_ipad;
    */
    
    UIImage*divider;
    int currentOrientation;
    
    /*
     Starts:
     SUN:0004
     Manages receipts
     
     */
    
    BOOL is_receipt_image_choosen;
    BOOL is_receipt_image_updated;
    
    NSString* receipt_image_name;
    NSString* receipt_small_image_name;
    UIImage*receipt_image;
    UIImage*temp_receipt_image;//this manages last choosen receipt image upto save/update.
    
    BOOL income_expense_is_recursive;
    
    
    /*
     Ends:
     SUN:0004
     Manages receipts
     
     */
    
    BOOL isIphone5;
    
    
    UIButton *home_bottom_button;
    UIButton *add_income_expense_bottom_button;
    UIButton *settings_bottom_button;
    
    UIView*bottomView;
    
    
    UIImageView*imgView_bottom_tabBar;
    /*Bottomview buttos for ipad*/

    UIButton *dashboarad_bottom_button;
    UIButton *income_expsne_bottom_button;
    UIButton *calculator_bottom_button;//currently equity
   
    
    ///UIButton *report_bottom_button;
    
    
    UIButton *reminder_bottom_button;
    UIButton *ipad_settings_bottom_button;
    

    
    
    /*Bottomview buttos for ipad*/

    UINavigationController*temp_navigation_controller;
    BOOL isFromButtonClick;
    
    
    /*Milage*/
    
    BOOL is_milege_on;
    BOOL isMilege;

    /*Reminder*/
    
    int current_selected_reminder_type;//0 = not complted,1 = completed;
    
    int date_filter_type;//0= MTD,1 = YTD,3 = Date Range;
 
    IBOutlet UITabBar *temp_tabBar;
    
    BOOL is_tabBar_added;
    
    UINavigationController *receiptViewNavigationController;
    
    
    BOOL isIOS7;
    
}

/*
 Starts:
 SUN:0004
 Manages receipts
 
 */


@property (nonatomic, retain)UIImage*temp_receipt_image;
@property(nonatomic,readwrite) BOOL is_receipt_image_choosen;
@property(nonatomic,readwrite) BOOL is_receipt_image_updated;


@property(nonatomic,retain)  NSString* receipt_image_name;
@property(nonatomic,retain)  NSString* receipt_small_image_name;
@property (nonatomic, retain)UIImage*receipt_image;
@property(nonatomic,readwrite) BOOL income_expense_is_recursive;



/*
 Ends:
 SUN:0004
 Manages receipts
 
 */


//

@property(nonatomic,retain) UINavigationController *receiptViewNavigationController;

@property(nonatomic,readwrite) BOOL isIphone5;

-(NSString *)getDBPath;
-(void)copyDatabseIfNeeded;



// currency method

-(NSString *)selectCurrencyIndex;
-(void)updateCurrency:(NSString *)currencyCode;

// agent type methods
-(NSMutableArray *)selectAgentType;
-(void)AddAgentInfo:(NSMutableArray *)agentType;
-(void)UpdateAgentInfo:(NSMutableArray *)agentType rowid:(int)rowid;

-(void)DeleteAgentInfo:(int)rowid;

-(NSMutableDictionary*)getAgentInforById:(int)rowid;




// income expense detail methods
-(NSMutableArray *)selectIncomeExpense;
-(NSMutableArray *)selectIncomeExpenseByProperty:(NSString *)property;
-(NSMutableArray *)selectIncomeExpenseByPropertyForRange:(NSString *)property startDate:(NSString *)startDate endDate:(NSString *)endDate;
-(NSMutableArray *)selectIncomeExpenseByMDTOrYDT:(NSString *)MTDOrYTD;
-(NSMutableArray *)selectIncomeExpenseForDashBoardBoard:(NSString *)startDate endDate:(NSString *)endDate;



-(void)AddIncomeExpense:(NSString *)property type:(NSString *)type date:(NSString *)date amount:(NSString *)amount notes:(NSString *)notes incomeExpense:(int)incomeExpense
            isrecursive:(NSString*)isrecursive
              frequency:(NSString*)frequency
                enddate:(NSString*)enddate
               parentid:(int)parentid
is_receipt_image_choosen:(int)is_receipt_image_choosen
     receipt_image_name:(NSString*)receipt_image_name
receipt_small_image_name:(NSString*)receipt_small_image_name;



-(void)UpdateIncomeExpense:(NSString *)property type:(NSString *)type date:(NSString *)date amount:(NSString *)amount notes:(NSString *)notes isrecursive:(NSString *)isrecursive enddate:(NSString *)enddate parentid:(int)parentid rowid:(int)rowid  is_receipt_image_choosen:(int)is_receipt_image_choosen_local
        receipt_image_name:(NSString*)receipt_image_name_local
  receipt_small_image_name:(NSString*)receipt_small_image_name_local;





-(void)UpdateIncomeExpenseByProperty:(NSString *)newProperty oldProperty:(NSString *)oldProperty;

//-(void)DeleteIncomeExpense:(int)rowid;
-(void)DeleteIncomeExpense:(int)rowid parentId:(int)parentId deleteType:(int)deleteType dateString:(NSString*)dateString;
-(void)DeleteIncomeExpense:(int)rowid;
-(void)DeleteincomeExpenseByProperty:(NSString *)property;


/// reminder view mehods

//-(NSMutableArray *)selectReminder:(NSString *)dbPath;
-(NSMutableArray *)selectAllReminders;
-(NSMutableArray *)selectReminder:(int)reminder_on_off;
-(void)AddReminder:(NSString *)type property:(NSString *)property date:(NSString *)date repeat:(NSString *)repeat notes:(NSString *)notes reminder:(int)reminder time:(NSString *)time;
-(void)UpdateReminder:(NSString *)type property:(NSString *)property date:(NSString *)date repeat:(NSString *)repeat notes:(NSString *)notes reminder:(int)reminder time:(NSString *)time rowid:(int)rowid;
-(void)UpdateReminderNextFireDate:(NSString *)nextFireDate  rowid:(int)rowid;
-(void)DeleteReminder:(int)rowid;

-(void)deleteReminderByProperty:(NSString *)property;

-(NSMutableArray *)selectReminderByProperty:(NSString *)property;




/// property details method

-(NSMutableArray *)selectPropertyDetail;
-(void)addPropertyDetail:(NSMutableArray *)propertyArrayDetail;
-(void)updatePropertyDetail:(NSMutableArray *)propertyArrayDetail rowid:(int)rowid;
-(void)deletePropertyDetail:(int)rowid;

-(void)updatePropertyEquity:(NSString *)purchase maketValue:(NSString *)marketValue Loan:(NSString *)Loan equity:(NSString *)equity date:(NSString *)date rowid:(int)rowid;

// income type methods
-(NSMutableArray *)selectIncomeType;
-(void)AddIncomeType:(NSString *)incomeType;
-(void)updateIncomeType:(NSString *)incomeType rowid:(int)rowid;

-(void)deleteIncomeType:(int)rowid;

//expense type method

-(NSMutableArray *)selectExpenseType;
-(void)AddExpenseType:(NSString *)ExpenseType;
-(void)updateExpenseType:(NSString *)ExpenseType rowid:(int)rowid;
-(void)deleteExpenseType:(int)rowid;

-(NSMutableArray *)selectNewExpenseType;
-(void)AddNewExpenseType:(NSString *)ExpenseType;
-(void)updateNewExpenseType:(NSString *)ExpenseType rowid:(int)rowid;
-(void)deleteNewExpenseType:(int)rowid;


// reminder type methods

-(NSMutableArray *)selectReminderType;
-(void)AddReminderType:(NSString *)ReminderType;
-(void)updateReminderType:(NSString *)ReminderType rowid:(int)rowid;
-(void)deleteReminderType:(int)rowid;


//property type method

-(NSMutableArray *)selectPropertyType;
-(void)addPropertyType:(NSString *)PropertyType;
-(void)updatePropertyType:(NSString *)PropertyType rowid:(int)rowid;
-(void)deletePropertyType:(int)rowid;


//add property method

-(NSMutableArray *)selectProperty;
-(void)addProperty:(NSMutableArray *)proArray;
-(void)updateProperty:(int)rowid proArray:(NSMutableArray *)proArray;
-(void)deleteProperty:(int)rowid;


//number screen method

-(int)selectNumberOfScreen;
-(void)updateNumberOfScreen:(int)numberOfScreen;


//converting Date According to region Format
-(NSString*)fetchDateInCommonFormate:(NSString*)stringToConvert;

// restore factory method

-(void)restoreReminderType;
-(void)restoreIncomeType;
-(void)restoreExpenseType;
-(void)restorePropertyType;
-(void)restoreProperty;
-(void)restoreAgent;
-(void)restoreIncomeExpense;
-(void)restoreReminder;
-(void)insertPropertyType;
-(void)insertIncomeType;
-(void)insertExpenseType;
-(void)insertReminderType;


//DB Creation Method
-(void)createEditableCopyOfDatabaseIfNeeded;

//Max Row Id method
-(int)getMaxRowidFromTable:(NSString*)tableName;

//Deleting recursive Entries Method
-(void)deleteRecursiveRecord:(NSString*)parentId condition:(NSString*)condition;

-(void)updateParentFrequency:(NSString*)recurrenceTime pk:(int)pk;

//To manage bottom in ios_7
-(void)manageBottomBar_ios7_Hack;

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;


@property (nonatomic, retain)NSMutableArray *incomeTypeDBArray, *expenseTypeArray, *reminderTypeArray, *propertyTypeArray, *propertyAddArray, *propertyListArray, *currencyArray, *agentArray, *propertyDetailArray, *incomeExpenseArray, *reminderArray, *reminderArrayByProperty, *incomeExpenseSummery;
@property (nonatomic, readwrite)int reminderRowid,isFromIncomeExpenseView, equityIndex, badgeCount ;
@property (nonatomic, readwrite)BOOL fromProperty, innerUpdate, isFromPropertyAgent, isIncome, isIncomeExpenseUpdate,isPropertyImageUpdate;
@property (nonatomic, retain)NSString *propertyTypeStr, *currencyTypeStr, *agentTypeStr, *incomeProperty, *incomeString,*reminderTypeStr, *reminderPropertyStr,*agent_pk;

@property (nonatomic, retain)NSString *curCode;
@property(nonatomic, readwrite)int isProperty, isAgent, incomeExpenseIndex, numberScreen, reminderRowID;

@property(nonatomic, retain)UIImage *propertyImage;
-(BOOL)validateEmail:(NSString*)email;


@property(nonatomic, readwrite)int decimalScale;
@property(nonatomic, readwrite)BOOL isIPad;

@property (nonatomic, retain)NSMutableArray *currencyWithDollarSymbol;
@property(nonatomic, readwrite)BOOL isTakingBackupFromiCloud;
/*Starts:Chirag:New DB methods...*/

-(NSString *)selectNewCurrencyIndex;

/*Ends:Chirag:New DB methods...*/

@property (nonatomic, retain)NSDateFormatter* regionDateFormatter;


@property (nonatomic, readwrite)BOOL is_tabBar_added;

/*segment control images


@property (nonatomic, retain)UIImage*all_selected;
@property (nonatomic, retain)UIImage*all;
@property (nonatomic, retain)UIImage*barchart_selected;
@property (nonatomic, retain)UIImage*barchart;

@property (nonatomic, retain)UIImage*dash_ipad_expense;
@property (nonatomic, retain)UIImage*dash_ipad_expense_selected;


@property (nonatomic, retain)UIImage*dash_ipad_income_selected;
@property (nonatomic, retain)UIImage*dash_ipad_income;

@property (nonatomic, retain)UIImage*dash_expense_selected;
@property (nonatomic, retain)UIImage*dash_expense;
@property (nonatomic, retain)UIImage*dash_income_selected;
@property (nonatomic, retain)UIImage*dash_income;
*/

/*Income Expense

@property (nonatomic, retain)UIImage*expense_ipad_selected;
@property (nonatomic, retain)UIImage*expense_ipad;

@property (nonatomic, retain)UIImage*income_ipad_selected;
@property (nonatomic, retain)UIImage*income_ipad;

@property (nonatomic, retain)UIImage*both_ipad_selected;
@property (nonatomic, retain)UIImage*both_ipad;

@property (nonatomic, retain)UIImage*expense_selected;
@property (nonatomic, retain)UIImage*expense;

@property (nonatomic, retain)UIImage*income_selected;
@property (nonatomic, retain)UIImage*income;

@property (nonatomic, retain)UIImage*both_selected;
@property (nonatomic, retain)UIImage*both;
*/

/*Income expense



@property (nonatomic, retain)UIImage*mtd_selected;
@property (nonatomic, retain)UIImage*mtd;
@property (nonatomic, retain)UIImage*pie_chart;
@property (nonatomic, retain)UIImage*piechart_selected;
@property (nonatomic, retain)UIImage*table_selected;
@property (nonatomic, retain)UIImage*table;
@property (nonatomic, retain)UIImage*ytd_selected;
@property (nonatomic, retain)UIImage*ytd;
*/

@property (nonatomic, retain)UIImage*divider;




@property(nonatomic, readwrite)int currentOrientation;


-(void)update_default_date_filter:(NSMutableDictionary *)dictionary;
-(NSMutableDictionary *)select_default_date_filter;


/*
 Start:
 SUN:0004
 Manages image storing,retrieval,delettion etc..
 */

-(int)storeImageLocally:(NSString *)fileName image:(UIImage *)image;
-(BOOL)checkFileExistance:(NSString *)FileName;
-(UIImage *)getImage:(NSString *)FileName;
-(void)removeImageFromDirectory:(NSString *)FileName;

/*
 Ends:
 SUN:0004
 Manages image storing,retrieval,delettion etc..
 */

@property (nonatomic,retain)UIView* bottomView;
@property (nonatomic,retain) UIButton *home_bottom_button;
@property (nonatomic,retain) UIButton *add_income_expense_bottom_button;
@property (nonatomic,retain) UIButton *settings_bottom_button;
@property(nonatomic,readwrite)BOOL isFromButtonClick;
-(void)show_income_expense_button_clicked:(UINavigationController*)navigation_controller;

-(void)manageViewControllerHeight;

/*ipad bottom buttons*/

@property (nonatomic,retain) UIButton *dashboarad_bottom_button;
@property (nonatomic,retain) UIButton *income_expsne_bottom_button;
@property (nonatomic,retain) UIButton *calculator_bottom_button;
@property (nonatomic,retain) UIButton *reminder_bottom_button;
@property (nonatomic,retain) UIButton *ipad_settings_bottom_button;


/*ipad bottom buttons*/


@property (nonatomic,retain)UIImageView* imgView_bottom_tabBar;

/**/
@property(nonatomic,readwrite) BOOL isFirst;

@property(nonatomic,readwrite) BOOL is_milege_on;
@property(nonatomic,readwrite) BOOL isMilege;


@property(nonatomic,retain)NSUserDefaults* prefs;


@property(nonatomic,readwrite) int current_selected_reminder_type;

@property(nonatomic,readwrite) int date_filter_type;

-(NSString*)convert_common_date_to_region_date:(NSString*)commonDate;
- (NSString *)generateRandomImageName;
-(void)correctPassword;

@property(nonatomic,readwrite) BOOL isIOS7;

@end
