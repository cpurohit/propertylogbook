//
//  AddReminderViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddReminderViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "PropertyLogBookAppDelegate.h"
#import "AddReminderTypeViewController.h"
#import "propertyViewController.h"
 #import "UIPlaceHolderTextView.h"

@implementation AddReminderViewController

@synthesize reminderViewIndex;
#pragma mark -
#pragma mark View lifecycle

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
    

    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 156, 320, 44)];
    [self.view addSubview:toolBar];
    toolBar.hidden = YES;
    
    datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 200, 320,216 )];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    /*CP:Date- 03-03-14*/
    datePicker.backgroundColor = [UIColor whiteColor];
    
    
    /*
    [datePicker addTarget:self action:@selector(datePicker_change) forControlEvents:UIControlEventValueChanged];
    */
   
    datePicker.hidden = YES;
    
    
    pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 200, 320, 216)];
    
    //For ios 7
    /*CP:Date- 03-03-14*/
    pickerView.backgroundColor = [UIColor whiteColor];
    
    pickerView.dataSource = self;
    pickerView.delegate = self;
    pickerView.hidden = YES;
    [pickerView setShowsSelectionIndicator:YES];
    [self.view addSubview:pickerView];
    
    
    nslog(@"\n pickerView.frame.origin.y = %f",pickerView.frame.origin.y);
    nslog(@"\n datePicker.frame.origin.y = %f",datePicker.frame.origin.y);
    nslog(@"\n toolBar.frame.origin.y = %f",toolBar.frame.origin.y);
    
    
    if(appDelegate.isIphone5)
    {
        pickerView.frame = CGRectMake( pickerView.frame.origin.x, 244, pickerView.frame.size.width, pickerView.frame.size.height);
        
        datePicker.frame = CGRectMake( datePicker.frame.origin.x, 244, datePicker.frame.size.width, datePicker.frame.size.height);
        toolBar.frame = CGRectMake( toolBar.frame.origin.x, 200, toolBar.frame.size.width, toolBar.frame.size.height);
    }
        
    
    
    // [appDelegate selectReminder:[appDelegate getDBPath]];

    if (reminderViewIndex < 0)
    {
        self.navigationItem.title = @"ADD REMINDER";
    }
    else
    {
        self.navigationItem.title = @"EDIT REMINDER";   
    }
	
    
    /*
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save_clicked)] autorelease];
     */
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        UIButton * save_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [save_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        save_button.bounds = CGRectMake(0, 0,51.0,30.0);
        [save_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_save_button.png"] forState:UIControlStateNormal];
        [save_button addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:save_button] autorelease];
        
        UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator1.width = -12;
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
        
    }
    else
    {
        UIButton * save_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [save_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        save_button.bounds = CGRectMake(0, 0,51.0,30.0);
        [save_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_save_button.png"] forState:UIControlStateNormal];
        [save_button addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:save_button] autorelease];
        
        
        
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }

    
    
    
	reminderMainArray = [[NSMutableArray alloc]initWithObjects:@"Reminder Type",@"Property",nil];
    
	reminderDetailArray = [[NSMutableArray alloc]initWithObjects:@"Type",@"Property",@"Date"@"Time",@"Selection",@"Notes",nil];
     pickerArray = [[NSMutableArray alloc]initWithObjects:@"No Repeat",@"Weekly",@"Monthly",@"Quarterly",@"Yearly", nil];
    
    dict = [[NSMutableDictionary alloc]init];
    
    
    if([appDelegate.reminderTypeArray count]==0)
    {
        [appDelegate selectReminderType];
    }
    if([appDelegate.propertyDetailArray count]==0)
    {
        [appDelegate selectPropertyDetail];
    }
    
    
    
    nslog(@"\n array in viewDidLoad = %@",appDelegate.reminderArray);
    
    if (reminderViewIndex < 0)
    {
        nslog(@"\n comes in  add...");
        if ([appDelegate.reminderTypeArray count]>0)
        {
            appDelegate.reminderTypeStr = [[appDelegate.reminderTypeArray objectAtIndex:0]valueForKey:@"reminderType"];
        }
        else 
        {
            appDelegate.reminderTypeStr = @"Reminder Type";
        }

        if ([appDelegate.propertyDetailArray count]>0)
        {
            appDelegate.incomeProperty = [[appDelegate.propertyDetailArray objectAtIndex:0]valueForKey:@"0"];
        }
        else 
        {
            appDelegate.incomeProperty = @"Property";
        }
    
        [dict setObject:appDelegate.reminderTypeStr forKey:@"reminderType"];
        [dict setObject:appDelegate.incomeProperty forKey:@"property"];
        [dict setObject:@"" forKey:@"100"];
        [dict setObject:@"" forKey:@"102"];
        [dict setObject:@"  No Repeat" forKey:@"101"];
        
        [dict setObject:@"" forKey:@"notes"];

        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"hh:mm a"];
        
        [section_time_button setTitle:[NSString stringWithFormat:@"  %@",[df stringFromDate:[NSDate date]]] forState:UIControlStateNormal];
        [dict setObject:section_time_button.titleLabel.text forKey:[NSString stringWithFormat:@"%d",section_time_button.tag]];
        
        
        
        
        
       
        
        [df setDateFormat:@"yyyy-MM-dd"];
        
        
        [section_date_button setTitle:[NSString stringWithFormat:@"  %@",[appDelegate.regionDateFormatter stringFromDate:[NSDate date]]] forState:UIControlStateNormal];
        
        [dict setObject:section_date_button.titleLabel.text forKey:[NSString stringWithFormat:@"%d",section_date_button.tag]];
        [df release];
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    
    else
    {
        
        nslog(@"\n comes for edit...");
        
        appDelegate.reminderTypeStr = [[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"reminderType"];
        appDelegate.incomeProperty = [[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"property"];
        [dict setObject:[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"reminderType"] forKey:@"reminderType"];
         [dict setObject:[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"property"] forKey:@"property"];
        
        [dict setObject:[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"date"] forKey:@"100"];
        [dict setObject:[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"time"] forKey:@"102"];
        [dict setObject:[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"repeat"] forKey:@"101"];
        [dict setObject:[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"notes"] forKey:@"notes"];
    }
	tblView.backgroundColor = [UIColor clearColor];
	
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];

    
    if(appDelegate.isIphone5)
    {
        tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
    }
    else
    {
        /*CP : 03-03-14*/
        tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
    }
    

    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0, 1024,1024)];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [self.view addSubview:datePicker];
    
    [imgView release];
    
    
    
	
}
-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
     
}
- (void)viewWillAppear:(BOOL)animated 
{
    [super viewWillAppear:animated];
    
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    //[self.view addSubview:appDelegate.bottomView];
    
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    if(appDelegate.isIPad)
    {

    
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
    
    [tblView reloadData];
    
    
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    
    
    if(appDelegate.isIPad)
    {
        return  YES;
    }
    return FALSE;
}


-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
   
    pickerView.hidden = YES;
    datePicker.hidden = YES;
    toolBar.hidden = YES;
     
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
        [tblView reloadData];
    }

}

#pragma mark -

-(void)cancel_clicked
{
	[self.navigationController popViewControllerAnimated:YES];
}


-(void)save_clicked
{
    
    
    
    UIButton *dateBtn = (UIButton *)[self.view viewWithTag:100];
    UIButton *timeBtn = (UIButton *)[self.view viewWithTag:102];
    
    
    
    nslog(@"\n date === %@ \n",[dict objectForKey:[NSString stringWithFormat:@"%d",100]]);
    nslog(@"\n time === %@ \n ",[dict objectForKey:[NSString stringWithFormat:@"%d",102]]);
    UIButton *repeatBtn = (UIButton *)[self.view viewWithTag:101];
    
    if ((dateBtn.titleLabel.text).length <=0)
    {
        dateBtn.titleLabel.text = @"";
    }
    if ([timeBtn.titleLabel.text length] <=0)
    {
        timeBtn.titleLabel.text = @"";
    }
    if ((repeatBtn.titleLabel.text).length <=0)
    {
        repeatBtn.titleLabel.text = @"";
    }
    
    UITextView *textView = (UITextView *)[self.view viewWithTag:2005];
    if ((textView.text).length <=0)
    {
        textView.text = @"";
    }
    
    
    /*
    [dict setObject:dateBtn.titleLabel.text forKey:@"100"];
    [dict setObject:timeBtn.titleLabel.text forKey:@"102"];
    */
    [dict setObject:textView.text forKey:@"notes"];
    [dict setObject:repeatBtn.titleLabel.text forKey:@"101"];
    [dict setObject:appDelegate.reminderTypeStr forKey:@"reminderType"];
    [dict setObject:appDelegate.incomeProperty forKey:@"property"];
    
   
    if (![appDelegate.incomeProperty isEqualToString:@"Property"] && [[dict objectForKey:[NSString stringWithFormat:@"%d",100]] length]>0 && [[dict objectForKey:[NSString stringWithFormat:@"%d",102]] length]>0)
    {
     if (reminderViewIndex < 0)
    {
        
        /*
         Sun:004
         Now reminder is used for task completed(instead of reminder on/off) or not..so initially it will be 0;
         */
        int reminder = 0;
        
        nslog(@"\n goes for adding reminder...");
        NSString *dateString = [appDelegate fetchDateInCommonFormate:[dict objectForKey:[NSString stringWithFormat:@"%d",100]]];
        
        [appDelegate AddReminder:appDelegate.reminderTypeStr property:appDelegate.incomeProperty date:dateString repeat:repeatBtn.titleLabel.text notes:textView.text reminder:reminder time:[dict objectForKey:[NSString stringWithFormat:@"%d",102]]];
        
        [self scheduleAlarm];
        
    }
    else
    {
        nslog(@"appdelegate.reminderarry === %@",appDelegate.reminderArray);
        
        
                nslog(@"\n goes for edit reminder...");
        
        int reminder = [[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"reminder"]intValue];
        NSString *dateString = [appDelegate fetchDateInCommonFormate:[dict objectForKey:[NSString stringWithFormat:@"%d",100]]];
        
        [appDelegate UpdateReminder:appDelegate.reminderTypeStr property:appDelegate.incomeProperty date:dateString repeat:repeatBtn.titleLabel.text notes:textView.text reminder:reminder time:[dict objectForKey:[NSString stringWithFormat:@"%d",102]] rowid:[[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"rowid"]intValue]];
        
              
        if(reminder == 0)
        {
           [self cancelAlarm];
        }
    }
    
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        if ([[dict objectForKey:[NSString stringWithFormat:@"%d",100]] length] == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter valid Date" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            [alert release];
        }
        else if ([[dict objectForKey:[NSString stringWithFormat:@"%d",102]] length]==0)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter valid Time" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            [alert release];
        }
        else
        {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Property field is mandatory" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        }

    }
    
}

#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

#pragma mark - btn_clicked

-(IBAction)btn_clicked:(id)sender
{
    [self done_clicked];
    
    
    if(appDelegate.isIphone5)
    {
        toolBar.frame =  CGRectMake(0, 200, 320, 44);
    }
    
    UIButton *btn = (UIButton *)sender;
    ButtonTag = btn.tag;
    
    nslog(@"\n ButtonTag = %d",btn.tag);
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    NSDate* currentDate = [NSDate date];
    [datePicker  setDate:currentDate];

    [datePicker removeTarget:self action:@selector(timePicker_change) forControlEvents:UIControlEventValueChanged];

    [datePicker addTarget:self action:@selector(datePicker_change) forControlEvents:UIControlEventValueChanged];
    
    toolBar.hidden = NO;
    datePicker.hidden = NO;
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(done_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done_clicked)];
    */
    
    
    
    
    
    
    UIBarButtonItem *flexiSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexiSpace,doneButton, nil]];
    //    toolBar.items = array;
    toolBar.barStyle = UIBarStyleBlack;

    [cancelBtn release];
    [doneButton release];
    [flexiSpace release];
    
    if(appDelegate.isIPad)
    {
        
        
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            datePicker.frame =  CGRectMake(0,715, 768,216 );
            /*if(appDelegate.isIOS7)
            {
                toolBar.frame = CGRectMake(0, 651,768, 44);
            }
            else*/
            {
                toolBar.frame = CGRectMake(0, 671,768, 44);
            }
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            datePicker.frame =  CGRectMake(0,513,1024,116 );
           /* if(appDelegate.isIOS7)
            {
                toolBar.frame = CGRectMake(0,449,1024, 44);
            }
            else*/
            {
                toolBar.frame = CGRectMake(0,469,1024, 44);
            }
        }
        

    }
    
    
    [self.view addSubview:toolBar];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
	//df.dateStyle = NSDateFormatterMediumStyle;
    [df setDateFormat:@"yyyy-MM-dd"];
    if ([[dict objectForKey:@"100"] length]>0)
    {
       // datePicker.date = [df dateFromString:[dict objectForKey:@"100"]];
         datePicker.date = [appDelegate.regionDateFormatter dateFromString:[dict objectForKey:@"100"]];
    }
    
    /*
    [btn setTitle:[NSString stringWithFormat:@"  %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
    */
    [btn setTitle:[NSString stringWithFormat:@"  %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
    
    
    [dict setObject:btn.titleLabel.text forKey:[NSString stringWithFormat:@"%d",btn.tag]];

    [df release];

}

-(void)datePicker_change
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];

    [df setDateFormat:@"yyyy-MM-dd"];

    UIButton *localBtn = (UIButton *)[self.view viewWithTag:ButtonTag];
    [localBtn setTitle:[NSString stringWithFormat:@"  %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];

    [dict setObject:localBtn.titleLabel.text forKey:[NSString stringWithFormat:@"%d",ButtonTag]];
    [df release];
    
}


#pragma mark -
#pragma mark time methods

-(IBAction)time_clicked:(id)sender
{
    [self done_clicked];
    
    UIButton *btn = (UIButton *)sender;
    ButtonTag = btn.tag;
    datePicker.datePickerMode = UIDatePickerModeTime;
    
   
    
    
    
    
    NSDate* currentDate = [NSDate date];
    [datePicker  setDate:currentDate];
    
    [datePicker removeTarget:self action:@selector(datePicker_change) forControlEvents:UIControlEventValueChanged];
    [datePicker addTarget:self action:@selector(timePicker_change) forControlEvents:UIControlEventValueChanged];
    
    toolBar.hidden = NO;
    datePicker.hidden = NO;
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(done_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    
    
    
    
    
    UIBarButtonItem *flexiSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexiSpace,doneButton, nil]];
    //    toolBar.items = array;
    toolBar.barStyle = UIBarStyleBlack;
    
    
    [cancelBtn release];
    [doneButton release];
    [flexiSpace release];
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            datePicker.frame =  CGRectMake(0,715, 768,216 );
            
            /*if(appDelegate.isIOS7)
            {
                toolBar.frame = CGRectMake(0, 651,768, 44);
            }
            else*/
            {
                toolBar.frame = CGRectMake(0, 671,768, 44);
            }

        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            datePicker.frame =  CGRectMake(0,513,1024,116 );
            
            /*if(appDelegate.isIOS7)
            {
                toolBar.frame = CGRectMake(0,449,1024, 44);
            }
            else*/
            {
                toolBar.frame = CGRectMake(0,469,1024, 44);
            }
        }
        
    }
   
    
   
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"hh:mm a"];
    if ([[dict objectForKey:@"102"] length]>0)
    {
        datePicker.date = [df dateFromString:[dict objectForKey:@"102"]];
        //datePicker.date = [appDelegate.regionDateFormatter dateFromString:[dict objectForKey:@"102"]];
    }
    
    [btn setTitle:[NSString stringWithFormat:@"  %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
    
    
    
    
    [dict setObject:btn.titleLabel.text forKey:[NSString stringWithFormat:@"%d",btn.tag]];
    
    [df release];
    
    
    if(!appDelegate.isIPad)
    {
        tblView.frame = CGRectMake(0, -50, tblView.frame.size.width, tblView.frame.size.height);        
    }
    
}


-(void)timePicker_change
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"hh:mm a"];
    UIButton *btn = (UIButton *)[self.view viewWithTag:ButtonTag];
    [btn setTitle:[NSString stringWithFormat:@"  %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
    [dict setObject:btn.titleLabel.text forKey:[NSString stringWithFormat:@"%d",ButtonTag]];
    [df release];
    
}

#pragma mark -
#pragma mark repeat method


-(void)repeatBtn_clicked:(id)sender
{
    UIButton *repeatBtn = (UIButton *)sender;
    nslog(@"repeat button=====%@",repeatBtn.titleLabel.text);
    [self done_clicked];
    
    
    pickerView.hidden = NO;
    toolBar.hidden = NO;

    
    NSString *str = [repeatBtn.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    nslog(@"string ---- %@",str);
//    NSObject * abc = repeatBtn.titleLabel.text;
    [pickerView selectRow: [pickerArray indexOfObject:str] inComponent:0 animated:YES];
    //    [pickerView showsSelectionIndicator];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(done_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done_clicked)];
    */
    
    
    UIBarButtonItem *flexiSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexiSpace,doneButton, nil]];
    //    toolBar.items = array;
    toolBar.barStyle = UIBarStyleBlack;
   
    [cancelBtn release];
    [doneButton release];
    [flexiSpace release];
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            pickerView.frame =  CGRectMake(0,715, 768,216 );
            /*if(appDelegate.isIOS7)
            {
                toolBar.frame = CGRectMake(0, 651,768, 44);
            }
            else*/
            {
                toolBar.frame = CGRectMake(0, 671,768, 44);
            }
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            pickerView.frame =  CGRectMake(0,513,1024,116 );
           /* if(appDelegate.isIOS7)
            {
                toolBar.frame = CGRectMake(0,449,1024, 44);
            }
            else*/
            {
                toolBar.frame = CGRectMake(0,469,1024, 44);
            }
        }
        
    }
   
    if(!appDelegate.isIPad)
    {
       tblView.frame = CGRectMake(0, -100, tblView.frame.size.width, tblView.frame.size.height);     
    }
    
    if(appDelegate.isIphone5)
    {
        toolBar.frame = CGRectMake( toolBar.frame.origin.x, 200, toolBar.frame.size.width, toolBar.frame.size.height);
    }
    
    
}

#pragma mark -
#pragma mark reminder method

- (IBAction) scheduleAlarm
{
    
    
    nslog(@"in schedule method and dict = %@",dict);
    
    /**
    UIApplication *app = [UIApplication sharedApplication];
	NSArray *alarmArray = [app scheduledLocalNotifications];
    nslog(@"localnotification array === %@",alarmArray);
   */
    // NSMutableArray *temp=[appDelegate selectExerciseInfo:[appDelegate getDBPath]];
    
    
    
    

 
    NSString *reminderType =[dict valueForKey:@"reminderType"];
    NSString * property =[dict valueForKey:@"property"];
    NSString * date =[dict valueForKey:@"100"];
    NSString * time = [dict valueForKey:@"102"];
    NSString * repeat = [dict valueForKey:@"101"];
    NSString * notes = [dict valueForKey:@"notes"];

    if (reminderViewIndex >=0)
    {
    nslog(@"\n rowID = %d", [[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"rowid"] intValue]);
     
     appDelegate.reminderRowID = [[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"rowid"] intValue];
    }

    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
	
    // Get the current date
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
	NSDateFormatter *timeFormat = [[NSDateFormatter alloc]init];
	[dateFormat setDateFormat:@"yyyy-MM-dd"];
	[timeFormat setDateFormat:@"hh:mm a"];
    //NSDate *pickerDate = [dateFormat dateFromString:date];
    nslog(@"date is==== %@",date);
    NSDate *pickerDate = [appDelegate.regionDateFormatter dateFromString:date];
	
    nslog(@"picker date ==== %@",pickerDate);
    nslog(@"date is ====== %@",[NSDate date]);
	NSDate *pickerTime = [timeFormat dateFromString:time];
    // Break the date up into components
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
												   fromDate:pickerDate];
	
	
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
												   fromDate:pickerTime];
    // Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
	// Notification will fire in one minute
    [dateComps setMinute:[timeComponents minute]];
	[dateComps setSecond:[timeComponents second]];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    [dateComps release];
	
	UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
    {
        [dateFormat release];
        [timeFormat release];
        return;
    }
        
	nslog(@"item date = %@",itemDate);
    localNotif.fireDate = itemDate;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
	
	// Notification details
    
    nslog(@"notes === %@",notes);
    
    localNotif.alertBody = [NSString stringWithFormat:@"Reminder Type: %@ \n Property: %@ \n Notes: %@",reminderType,property,notes];
	// Set the action button
    localNotif.alertAction = @"View";
	
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = appDelegate.badgeCount+1;
    appDelegate.badgeCount = localNotif.applicationIconBadgeNumber;
    
    nslog(@"Application BadgeCount ==== %d",localNotif.applicationIconBadgeNumber);
	if ([repeat isEqualToString:@"  Weekly"])
    {
        localNotif.repeatInterval=NSWeekCalendarUnit;
    }
    else if ([repeat isEqualToString:@"  Quarterly"])
    {
        localNotif.repeatInterval=NSQuarterCalendarUnit;
    }
    else if ([repeat isEqualToString:@"  Monthly"])
    {
        localNotif.repeatInterval=NSMonthCalendarUnit;
    }
    else if ([repeat isEqualToString:@"  Yearly"])
    {
        localNotif.repeatInterval = NSYearCalendarUnit;
    }
    
	// Specify custom data for the notification
    NSDictionary *infoDict;
    
    infoDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:appDelegate.reminderRowID] forKey:@"Key"];
    
    
    localNotif.userInfo = infoDict;
	
	// Schedule the notification


    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    [localNotif release];
	
    
    
    [dateFormat release];
    [timeFormat release];
   
  
}

-(void) cancelAlarm 
{
    
    nslog(@"\n  cancelAlarm");
    int temp = [[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"rowid"] intValue];
    
    nslog(@"\n appDelegate.reminderArray = %@",appDelegate.reminderArray );
    nslog(@"\n temp = %d",temp);
    
    
	

    
    
    /*
     Chirag changing logic... 

    for (int i=0; i<[alarmArray count]; i++) 
    {
		UILocalNotification* oneAlarm = [alarmArray objectAtIndex:i];
        nslog(@"\n oneAlarm = %@",oneAlarm);
        
		int FinalTemp = [[oneAlarm.userInfo objectForKey:@"Key"]intValue];
      //  nslog(@"final temp unser info ===%d",FinalTemp);
		if (FinalTemp==temp) 
        {
			nslog(@"Cancelling alarm: %@", oneAlarm.alertBody);
			[app cancelLocalNotification:oneAlarm];
		}
	}
       
    */
    /*Starts:Deleting notification*/
    
    //nslog(@"\n\n\nBefore deleting notification == %@",[[UIApplication sharedApplication] scheduledLocalNotifications]);
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) 
    {
        
        
        for(int i=0;i<[[[UIApplication sharedApplication] scheduledLocalNotifications] count];i++)
        {
            
            UILocalNotification *notif1;// = [[[UILocalNotification alloc] init] autorelease];
            notif1    = [[[UIApplication sharedApplication] scheduledLocalNotifications]objectAtIndex:i];
            nslog(@"\n notif1 = %d ",[[notif1.userInfo objectForKey:@"Key"]intValue]);
            if([[notif1.userInfo objectForKey:@"Key"]intValue]==temp)
            {
                
                nslog(@"comes for removing..");
                [[UIApplication sharedApplication] cancelLocalNotification:notif1];
            }
        }
    }    
    
    //nslog(@"\n\n\n After deleting notification == %@",[[UIApplication sharedApplication] scheduledLocalNotifications]);
    
    
    
    [self scheduleAlarm];
}



#pragma mark - 
#pragma mark pickerview data

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{	
	return 1;
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [pickerArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	return [pickerArray objectAtIndex:row];
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    UIButton *btn = (UIButton *)[self.view viewWithTag:101];
    [btn setTitle:[NSString stringWithFormat:@"  %@",[pickerArray objectAtIndex:row]] forState:UIControlStateNormal];
    [dict setObject:btn.titleLabel.text forKey:[NSString stringWithFormat:@"%d",btn.tag]];
    //    [repayment retain];
}

- (void)selectRow:(NSInteger)row inComponent:(NSInteger)component animated:(BOOL)animated
{
    
}





-(void)done_clicked
{
    UITextView *textView = (UITextView *)[self.view viewWithTag:2005];
    [textView resignFirstResponder];
    
    if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
    {
        tblView.contentSize = CGSizeMake(1024,600);
    }

    
    tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
    
    pickerView.hidden = YES;
    datePicker.hidden = YES;
    toolBar.hidden = YES;
    
    
}







#pragma mark -
#pragma mark Table view data source

/*Ask Smit..
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if(section==1)
    {
        return @"Note: To use this feature, IOS5 users can set notification under Settings -> Notifications -> Property Log Book";
    }
    return @"";
}
 */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    // Return the number of sections.
    return 3;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return 0;
    }
    else if(section==1)
    {
        if(appDelegate.isIPad)
        {
            return 90;
        }
        else
        {
            return 55;
        }
        
    }
    else if(section==2)
    {
        return 10;
    }
    
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section==0)
    {
        return 30.0f;
    }
    
    return 25.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section==1)
    {
        
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
            {
                reminder_section_imageView.frame = CGRectMake(0, 0,768, 90);
                section_date_button.frame = CGRectMake(0,0,382, 90);
                section_time_button.frame = CGRectMake(384,0,382, 90);
                reminder_section_imageView.image = [UIImage imageNamed:@"ipad_port_reminder_date_cell.png"];
            }
            else
            {
                
                reminder_section_imageView.frame = CGRectMake(0, 0,1024, 90);
                section_date_button.frame = CGRectMake(0, 0,510, 90);
                section_time_button.frame = CGRectMake(514,0,510, 90);
                reminder_section_imageView.image = [UIImage imageNamed:@"ipad_land_reminder_date_cell.png"];
            }
            
        }
        
        [section_date_button setTitle:[dict objectForKey:[NSString stringWithFormat:@"%d",100]] forState:UIControlStateNormal];
        
        [section_time_button setTitle:[dict objectForKey:[NSString stringWithFormat:@"%d",102]] forState:UIControlStateNormal];
        
        
        return section_header_view;
    }
     return nil;
    
    
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return @"";
    
    /*
    if (section == 1)
    {
        return @"Notes";
    }
    return @"";
     */
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2)
    {
        if(indexPath.row==1)
        {
            if(appDelegate.isIphone5)
            {
                return 184;
            }
            else
            {
                return 100;
            }

        }
        
    }
	return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0)
    {
        return [reminderMainArray count];
    }
    else if (section == 1)
    {
        return 0;
    }
    else if(section==2)
    {
        return 2;
    }
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UILabel *label, *detailLabel;
    UIPlaceHolderTextView *textView;
   
    
    UIButton *btn;
    if (indexPath.section == 0)
    {
        UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        if (1) 
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            label = [[UILabel alloc]initWithFrame:CGRectMake(40, 7, 140, 30)];
            label.tag = 400;
            
            label.numberOfLines = 0;
            label.lineBreakMode = UILineBreakModeTailTruncation;
            [label setFont:[UIFont systemFontOfSize:15.0]];
            [label setBackgroundColor:[UIColor clearColor]];
            
            [cell.contentView addSubview:label];
            
            [label release];
            
            detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(145, 7, 122, 30)];
            if(appDelegate.isIPad)
            {
               
                if(appDelegate.isIOS7)
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                    {
                        detailLabel.frame = CGRectMake(130, 7, 590, 30);
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                    {
                        detailLabel.frame = CGRectMake(130, 7, 820, 30);
                    }
                }
                else
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                    {
                        detailLabel.frame = CGRectMake(130, 7, 520, 30);
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                    {
                        detailLabel.frame = CGRectMake(130, 7, 750, 30);
                    }
                }
                
            }
            else
            {
               detailLabel.frame = CGRectMake(145, 7, 122, 30);
            }
            
            
            detailLabel.tag = 500;
            
            detailLabel.textAlignment = UITextAlignmentRight;
            detailLabel.numberOfLines = 0;
            detailLabel.lineBreakMode = UILineBreakModeTailTruncation;
            [detailLabel setFont:[UIFont systemFontOfSize:15.0]];
            [detailLabel setBackgroundColor:[UIColor clearColor]];
            
            [cell.contentView addSubview:detailLabel];
            
            [detailLabel release];
            
            
            textView = [[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(150, 7, 140, 70)];
            textView.tag = 2000+indexPath.row;
            textView.delegate = self;
            [[textView layer] setCornerRadius:5.0];
            [[textView layer] setBorderWidth:2.0];
            [[textView layer] setBorderColor:[UIColor grayColor].CGColor];
            [textView setFont:[UIFont systemFontOfSize:15.0]];
            textView.backgroundColor = [UIColor clearColor];
            textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            if(appDelegate.isIPad)
            {
              
                if(appDelegate.isIOS7)
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                    {
                        btn.frame = CGRectMake(580, 7, 140, 30);
                        
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                    {
                        btn.frame = CGRectMake(810, 7, 140, 30);
                        
                    }
                }
                else
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                    {
                        btn.frame = CGRectMake(510, 7, 140, 30);
                        
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                    {
                        btn.frame = CGRectMake(740, 7, 140, 30);
                        
                    }
                }
            }
            else
            {
               btn.frame = CGRectMake(150, 7, 140, 30);
                
            }
            
            btn.backgroundColor  = [UIColor clearColor];
            
            //[btn setBackgroundImage:[UIImage imageNamed:@"dropDownBlue.png"] forState:UIControlStateNormal];
            
            
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
             btn.titleLabel.font = [UIFont systemFontOfSize:13];
            //[btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            
            
        }

    // Configure the cell...
    
        label.text = [reminderMainArray objectAtIndex:indexPath.row];
        if (indexPath.row == 0)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
            //cell.imageView.image = [UIImage imageNamed:@"reminder_type_icon.png"];
            
            UIImageView*reminderType_imageView = [[UIImageView alloc] init];
            reminderType_imageView.frame = CGRectMake(0, 0,40,40);
            reminderType_imageView.image = [UIImage imageNamed:@"reminder_type_icon.png"];
            [cell.contentView addSubview:reminderType_imageView];
            [reminderType_imageView release];
            
            
            
            
            
            detailLabel.text = appDelegate.reminderTypeStr;
        }
        else if (indexPath.row == 1)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            //cell.imageView.image = [UIImage imageNamed:@"property_icon.png"];
            
            UIImageView*property_imageView = [[UIImageView alloc] init];
            property_imageView.frame = CGRectMake(0, 0,40,40);
            property_imageView.image = [UIImage imageNamed:@"property_icon.png"];
            [cell.contentView addSubview:property_imageView];
            [property_imageView release];
            
            
            detailLabel.text = appDelegate.incomeProperty;
        }
        
   
        
       /*
	else if (indexPath.row == 2)
    {
        detailLabel.hidden = TRUE;
        
        [cell.contentView addSubview:btn];
        //btn.tag = 100;
        btn.tag = 100;
         [btn addTarget:self action:@selector(btn_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitle:[dict objectForKey:[NSString stringWithFormat:@"%d",btn.tag]] forState:UIControlStateNormal];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (indexPath.row == 3)
    {
        detailLabel.hidden = TRUE;
        [cell.contentView addSubview:btn];
        btn.tag = 102;
       [btn addTarget:self action:@selector(time_clicked:) forControlEvents:UIControlEventTouchUpInside];
       [btn setTitle:[dict objectForKey:[NSString stringWithFormat:@"%d",btn.tag]] forState:UIControlStateNormal];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
        
        
    else if (indexPath.row ==4)
    {
        detailLabel.hidden = TRUE;
        [cell.contentView addSubview:btn];
        btn.tag = 101;
        [btn addTarget:self action:@selector(repeatBtn_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitle:[dict objectForKey:[NSString stringWithFormat:@"%d",btn.tag]] forState:UIControlStateNormal];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
        
        */
        
    return cell;
    }
    else if (indexPath.section == 2)
    {
        static NSString *CellIdentifier1 = @"Cell1";
        UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        if (1) 
        {
            cell1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier1] autorelease];
            
            label = [[UILabel alloc]initWithFrame:CGRectMake(40, 7, 140, 30)];
            label.tag = 400;
            
            label.numberOfLines = 0;
            label.lineBreakMode = UILineBreakModeTailTruncation;
            [label setFont:[UIFont systemFontOfSize:15.0]];
            [label setBackgroundColor:[UIColor clearColor]];
            
            [cell1.contentView addSubview:label];
            
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            
            if(appDelegate.isIPad)
            {
                
                if(appDelegate.isIOS7)
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                    {
                        btn.frame = CGRectMake(580, 7, 140, 30);
                        
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                    {
                        btn.frame = CGRectMake(810, 7, 140, 30);
                    }
                }
                else
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                    {
                        btn.frame = CGRectMake(510, 7, 140, 30);
                        
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                    {
                        btn.frame = CGRectMake(740, 7, 140, 30);
                    }
                }
            }
            else
            {
                btn.frame = CGRectMake(110, 7, 157, 30);
                
            }
            
            btn.backgroundColor  = [UIColor clearColor];
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            
            
            
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:13];
            
            
            detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(130, 7, 140, 30)];
            
            if (indexPath.row ==0)
            {
                label.text = @"Repeat";
                detailLabel.hidden = TRUE;
                
                
                //cell1.imageView.image = [UIImage imageNamed:@"property_icon.png"];//need to change this icon.
                
                
                UIImageView*property_imageView = [[UIImageView alloc] init];
                property_imageView.frame = CGRectMake(0, 0,40,40);
                property_imageView.image = [UIImage imageNamed:@"property_icon.png"];
                [cell1.contentView addSubview:property_imageView];
                [property_imageView release];

                
                
                [cell1.contentView addSubview:btn];
                btn.tag = 101;
                [btn addTarget:self action:@selector(repeatBtn_clicked:) forControlEvents:UIControlEventTouchUpInside];
                [btn setTitle:[dict objectForKey:[NSString stringWithFormat:@"%d",btn.tag]] forState:UIControlStateNormal];
                [cell1.contentView addSubview:btn];
                cell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            else
            {
                
                
                if(appDelegate.isIphone5)
                {
                    textView = [[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(5, 7, 290, 170)];
                }
                else
                {
                    textView = [[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(5, 7, 290, 70)];
                }
                
                
                
                textView.placeholder = @"Notes";
                textView.tag = 2005;
                textView.delegate = self;
                
                [textView setFont:[UIFont systemFontOfSize:15.0]];
                textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
                textView.backgroundColor = [UIColor clearColor];
                [cell1.contentView addSubview:textView];
                [textView setText:[dict objectForKey:@"notes"]];
                [textView release];
            }
            
            [detailLabel release];
            [label release];
                      
        }
        
        
        
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell1;

    }
    return nil;
}

#pragma mark -
#pragma mark textview methods



-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    
    if(!appDelegate.isIPad)
    {
      // [tblView setFrame:CGRectMake(0, -180, tblView.frame.size.width, tblView.frame.size.height)];
        
        if(appDelegate.isIphone5)
        {
             tblView.contentOffset = CGPointMake(0,130);
        }
        else
        {
            tblView.contentOffset = CGPointMake(0,220);
        }
        
       
        
    }
    datePicker.hidden = YES;
    pickerView.hidden = YES;
    toolBar.hidden = NO;
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
           
                toolBar.frame = CGRectMake(0,672,768,44);
           
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            
            
                toolBar.frame = CGRectMake(0,328,1024,44);
                
           
            tblView.contentSize = CGSizeMake(1024,900);
            nslog(@"\n tblView.contentOffset = %f",tblView.contentOffset.y);
            tblView.contentOffset = CGPointMake(0,150);
        }
    }
    else
    {
        //
        
        
        if(appDelegate.isIphone5)
        {
            toolBar.frame =  CGRectMake(0, 244, 320, 44);
        }
        else
        {
            toolBar.frame =  CGRectMake(0, 156, 320, 44);
        }

        
       
    }
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(done_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done_clicked)];
    */
    
    
    UIBarButtonItem *flexiSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexiSpace,doneButton, nil]];
    toolBar.barStyle = UIBarStyleBlack;
    
    [cancelBtn release];
    [doneButton release];
    [flexiSpace release];
    
    
    
    return YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    
    
    
    if(appDelegate.isIPad)
    {
        toolBar.hidden = TRUE;
    }
    
    [dict setObject:textView.text forKey:@"notes"];
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    appDelegate.isFromIncomeExpenseView = -1;

    
    if(indexPath.section==0)
    {
        if (indexPath.row == 0)
        {
            AddReminderTypeViewController *addReminder = [[AddReminderTypeViewController alloc]initWithNibName:@"AddReminderTypeViewController" bundle:nil];
            [self.navigationController pushViewController:addReminder animated:YES];
            [addReminder release];
        }
        else if (indexPath.row == 1)
        {
            propertyViewController *propertyView = [[propertyViewController alloc]initWithNibName:@"propertyViewController" bundle:nil];
            [self.navigationController pushViewController:propertyView animated:YES];
            [propertyView release];
        }
        
    }
    
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload 
{
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc 
{
    
    if(!appDelegate.isIOS7)
    {
        [super dealloc];
    }
    
    
    
    [reminderMainArray release];
    [reminderDetailArray  release];
    [pickerArray release];
    [dict release];
    [datePicker release];
    [toolBar release];
    [pickerView release];
    
      
}


@end

