//
//  AddReminderTypeViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddReminderTypeViewController.h"


@implementation AddReminderTypeViewController


- (void)dealloc
{
    [super dealloc];
    [keytoolBar release];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
         [tblView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
        
    }
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        
    }

    

    
    
    
    
    textFieldIndex = -1;
    self.navigationItem.title = @"REMINDER TYPE";
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel_clicked)];

    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }


    
   
    [appDelegate selectReminderType];
    
    if(appDelegate.isIphone5)
    {
        keytoolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 244, self.view.frame.size.width, 44)];
    }
    else
    {
        keytoolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 156, self.view.frame.size.width, 44)];
    }

    
    
       

    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            keytoolBar.frame = CGRectMake(0, 671,768, 44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            keytoolBar.frame = CGRectMake(0,326,1024, 44);
        }
        
    }
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(keytoolBar_cancel_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flexiItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
   
    
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(keytoolBar_done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(keytoolBar_done_clicked)];
    */
    
    
    [keytoolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexiItem,doneButton, nil]];
    //    toolBar.items = array;
    keytoolBar.barStyle = UIBarStyleBlack;
    [self.view addSubview:keytoolBar];
    keytoolBar.hidden = TRUE;
    
    [cancelBtn release];
    [flexiItem release];
    [doneButton release];  
    
    
    
    /*
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_clicked)] autorelease];
    */
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
        UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator1.width = -12;
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
        
    }
    else
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
    }

    
    if (appDelegate.isFromIncomeExpenseView >0)
    {
        
        
        tblView.editing = TRUE;
    }
    else
    {
        tblView.editing = FALSE;
    }
    

    
	tblView.backgroundColor = [UIColor clearColor];
	
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];

    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];	
}

-(void)keytoolBar_cancel_clicked
{
    [tblView setContentOffset:CGPointMake(0, 0) animated:NO];
    keytoolBar.hidden = TRUE;
    UITextField *text = (UITextField *)[self.view viewWithTag:isClicked];
    nslog(@"\n textFieldTempText = %@",textFieldTempText);
    text.text =  textFieldTempText;
    [text resignFirstResponder];
    [self textFieldShouldReturn:text];
    
}
-(void)keytoolBar_done_clicked
{
    
    keytoolBar.hidden = TRUE;
    UITextField *text = (UITextField *)[self.view viewWithTag:isClicked];
    textFieldTempText = text.text;
    [text resignFirstResponder];
	[self textFieldShouldReturn:text];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    
    //[self.view addSubview:appDelegate.bottomView];
    
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    
    isAddClicked = NO;
	if([appDelegate.reminderTypeArray count]==0)
	{
        imageView.hidden = FALSE;
		lblNoData.hidden = FALSE;
        lblPlus.hidden = FALSE;
         
        
        
        if(appDelegate.isIPad)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                lblPlus.center = CGPointMake(363,455); 
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(494,655.0f/2.0f);   
                lblPlus.center = CGPointMake(473,327); 
            }

            
        }


        
		[tblView setHidden:TRUE];
		
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];

		
	}
	else 
    {
        imageView.hidden = TRUE;
		[tblView setHidden:FALSE];
		[lblNoData setHidden:TRUE];
        [lblPlus setHidden:TRUE];
		[tblView reloadData];
		
	}
    
      
    if(appDelegate.isIPad)
    {

    
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
    
    tempRowArray = [[NSMutableArray alloc]initWithArray:appDelegate.reminderTypeArray];
	
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    if(appDelegate.isIPad)
    {
        if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
        {
            imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
            lblNoData.center = CGPointMake(384,911.0f/2.0f);   
            lblPlus.center = CGPointMake(363,455); 
        }
        else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
        {
            imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
            lblNoData.center = CGPointMake(494,655.0f/2.0f);   
            lblPlus.center = CGPointMake(473,327); 
        }
        
        
    }
    
    
    if(appDelegate.isIPad)
    {
        
        if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
        {
            keytoolBar.frame = CGRectMake(0, 671,768, 44);
        }
        else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
        {
            keytoolBar.frame = CGRectMake(0,326,1024, 44);
            
        }
        
    }
     
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    /*
    if(textFieldIndex != 5000)
    {
        keytoolBar.hidden = TRUE;
        [self.view endEditing:YES];
    }

    [tblView reloadData];
     */
    
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
    }

    
    
}

#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

#pragma mark -

-(void)cancel_clicked
{
    if (textFieldIndex > 0)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldIndex];
        [textField resignFirstResponder];
    }
    
    for (int i=0;i<[appDelegate.reminderTypeArray count];i++)
    {
        [appDelegate updateReminderType:[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"reminderType"] rowid:[[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        
    }
    [appDelegate selectReminderType];    
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)add_clicked
{
    
    isAddClicked = TRUE;
    if(textFieldIndex != 5000 && textFieldIndex>0)//alert view tag..!
    {
        
        UITextField*textField = (UITextField*)[self.view viewWithTag:textFieldIndex];
        int index = textField.tag%1000;
        nslog(@"\n text while ad = %@",textField.text);
        
        
        [[appDelegate.reminderTypeArray objectAtIndex:index] setValue:textField.text forKey:@"reminderType"];
        
        for(int i=0;i<[appDelegate.reminderTypeArray count];i++)
        {
            [appDelegate updateReminderType:[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"reminderType"] rowid:[[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
            
        }

        
        
    }
    
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Enter Reminder Type" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];

    
    myAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    reminderText = [myAlertView textFieldAtIndex:0];
    
    //reminderText= [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    
    [reminderText setKeyboardAppearance:UIKeyboardAppearanceDefault];
    reminderText.delegate = self;
    reminderText.tag = 5000;
	[reminderText becomeFirstResponder];
    [reminderText setBackgroundColor:[UIColor clearColor]];
	[myAlertView addSubview:reminderText];
	[myAlertView show];
	[myAlertView release];
}

- (void) alertView:(UIAlertView *) actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex 
{
    isAddClicked = FALSE;
    
    

    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    
    

    
    keytoolBar.hidden = TRUE;
	if (buttonIndex == 1)
	{      
        if ([reminderText.text length]>0)
        {
            
            for (int i=0;i<[appDelegate.reminderTypeArray count];i++)
            {
                [appDelegate updateReminderType:[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"reminderType"] rowid:[[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
                
            }
            [appDelegate selectReminderType];
            
        [appDelegate AddReminderType:reminderText.text];

        NSMutableDictionary *tdict = [[NSMutableDictionary alloc] init];
        [tdict setObject:reminderText.text forKey:@"reminderType"];
        
		[appDelegate.reminderTypeArray addObject:tdict];
		[appDelegate selectReminderType];
        
      //  [tblView setEditing:YES];
            imageView.hidden = TRUE;
		[lblNoData setHidden:TRUE];
            lblPlus.hidden = TRUE;

		[tblView setHidden:FALSE];
		
		[tblView reloadData];
            [tdict release];   
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please enter valid reminder type" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            [alertView release];
        }
	}
	else 
    {
        if ([appDelegate.reminderTypeArray count] == 0)
        {
		[lblNoData setHidden:FALSE];
            [lblPlus setHidden:FALSE];
                        imageView.hidden = FALSE;
		[tblView setHidden:TRUE];
       // [self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
			UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
            [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            //  [imgView setContentMode:UIViewContentModeScaleToFill];
            [self.view addSubview:imgView];
            [self.view sendSubviewToBack:imgView];
            
            [imgView release];


        }
        

        
        if(!appDelegate.isIPad)
        {
            
            if(appDelegate.isIphone5)
            {
                //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
                
                
                if(appDelegate.isIOS7)
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
                }
                else
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
                }
                
                
                
            }
            else
            {
                
                if(appDelegate.isIOS7)
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
                }
                else
                {
                    tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
                }
                
                
            }
            
        }
        
        
        
        [tblView reloadData];
	}

}
#pragma mark text field

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    /*
    NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:textField.tag%1000 inSection:0];
	[tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
    */
    nslog(@"\n textField.tag = %d",textField.tag);
    
    keytoolBar.hidden = FALSE;
    isClicked = textField.tag;
    textFieldTempText = textField.text;
    [textFieldTempText retain];

    
    if (textField.tag > 1002)
    {
        
        if(!appDelegate.isIPad)
        {
            /*
            tblView.frame = CGRectMake(0, -((textField.tag%1000)*25), tblView.frame.size.width, tblView.frame.size.height);
             */
        }
    }
    
   
    
    
   
    //tblView.contentSize = CGSizeMake(tblView.frame.size.width,tblView.frame.size.height-(260));
    nslog(@"\n text  = %@",textField.text);
    
    textFieldIndex = textField.tag;
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    if(!appDelegate.isIPad)
    {
        nslog(@" origint = %f",textField.frame.origin.y);
        
        
        if(appDelegate.isIphone5)
        {
            tblView.frame = CGRectMake(0,tblView.frame.origin.y ,tblView.frame.size.width,240.0f);
            
        }
        else
        {
        
        tblView.frame = CGRectMake(0,tblView.frame.origin.y ,tblView.frame.size.width,155.0f);
        }
        
        
        [tblView setContentOffset:CGPointMake(0,textField.frame.origin.y+(44*(isClicked%1000)-44)) animated:YES];
       // [tblView scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];
        
    }
    
    if(!isAddClicked)
    {
        [self scrollViewToTextField:textField];
    }

    
    keytoolBar.hidden = FALSE;

}

- (void)scrollViewToTextField:(id)textField
{
    // Set the current _scrollOffset, so we can return the user after editing
    
	//_scrollOffsetY = self.tableView.contentOffset.y;
    
    // Get a pointer to the text field's cell
    UITableViewCell *theTextFieldCell = (UITableViewCell *)[textField superview];
    
    // Get the text fields location
    CGPoint point = [theTextFieldCell convertPoint:theTextFieldCell.frame.origin toView:tblView];
    
    // Scroll to cell
    [tblView setContentOffset:CGPointMake(0, point.y - 12) animated: YES];
    
    // Add some padding at the bottom to 'trick' the scrollView.
    //[tblView setContentInset:UIEdgeInsetsMake(0, 0, point.y - 60, 0)];
}



-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    if(appDelegate.isIPad)
    {
        if(isAddClicked)
        {
            return NO;
        }
    }

    
    nslog(@"\n textField.tag = %d",textField.tag);
    
    int index = textField.tag%1000;
   
    
    
    nslog(@"reminder = %d",index);
    nslog(@"\n %@",[appDelegate.reminderTypeArray objectAtIndex:index]);
    nslog(@"textField = %@",textField.text);
    
    
    if(textField.tag != 5000)//alert view tag..!
    {
        [[appDelegate.reminderTypeArray objectAtIndex:index] setValue:textField.text forKey:@"reminderType"];

        
        
        
        for(int i=0;i<[appDelegate.reminderTypeArray count];i++)
        {
            [appDelegate updateReminderType:[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"reminderType"] rowid:[[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
            
        }
 
    }
    

    
    if(appDelegate.isIPad)
    {
        keytoolBar.hidden = TRUE;
        [textField resignFirstResponder];
    }

   
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if(isAddClicked== TRUE)
    {
        return FALSE;
    }
    
    if (textField.tag > 999)
    {
        
        
        
        
        nslog(@"\n textField.tag = %d",textField.tag);
        nslog(@"\n textField.text = %@",textField.text);
        nslog(@"\n textField.tag = %d",textField.tag%1000);
        nslog(@"\n textFieldIndex = %d",textFieldIndex%1000);
        
        
        nslog(@"\n appDelegate.reminderTypeArray = %@",appDelegate.reminderTypeArray);
        
        nslog(@"\n appDelegate.reminderTypeArray at index = %@",[appDelegate.reminderTypeArray objectAtIndex:(textField.tag%1000)]);
        nslog(@"\n text here = %@",textField.text);
        
        
        NSString *text = @"";        
        for(int i=0;i<[appDelegate.reminderTypeArray count];i++)
        {
            textField = (UITextField*)[self.view viewWithTag:1000+i];
             nslog(@"\n text after.. = %@",textField.text);
            nslog(@"\n appDelegate.reminderTypeArray in loop = %@",[appDelegate.reminderTypeArray objectAtIndex:i]);
            if(textFieldIndex%1000 == i)
            {
                //text = textField.text;
                text = textFieldTempText;
                nslog(@"\n updated text = %@",textFieldTempText);
            }
            else
            {
                text =  [[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"reminderType"];
            }
            //nslog(@"\n text = %@",text);
            
            nslog(@"\n text just before updating... = %@",text);
            [appDelegate updateReminderType:text rowid:[[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
            
            //[appDelegate selectReminderType:[appDelegate getDBPath]];
            
        }
        [appDelegate selectReminderType];
        
        nslog(@"\n appDelegate.reminderTypeArray did end editing after updating = %@",appDelegate.reminderTypeArray);
        
        /*        
         [appDelegate updateReminderType:textField.text rowid:[[[appDelegate.reminderTypeArray objectAtIndex:(textField.tag%1000)]valueForKey:@"rowid"]intValue]];
         [appDelegate selectReminderType:[appDelegate getDBPath]];
         */
        
        //[tblView reloadData];
        if (textFieldIndex == textField.tag)
        {
            textFieldIndex = -1;
        }
        
    }

    keytoolBar.hidden = YES;
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Table view data source
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (appDelegate.isFromIncomeExpenseView >=0)
    {
    return @"Note: 1st entry is default entry";
    }
    return @"";
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
            
    if ([appDelegate.reminderTypeArray count]>0)
    {
        nslog(@"\n count = %d",[appDelegate.reminderTypeArray count]);
        return [appDelegate.reminderTypeArray count];
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
   
    
    static NSString *CellIdentifier = @"Cell";
    UITextField *reminderTypeTextField;
    
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (1) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        
        if(appDelegate.isIOS7)
        {
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        }
        
        
        [cell.textLabel setFont:[UIFont systemFontOfSize:15.0]];
        reminderTypeTextField = [[UITextField alloc]initWithFrame:CGRectMake(20, 12, 200, 31)];
        
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                reminderTypeTextField.frame = CGRectMake(20, 9, 500, 31);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                reminderTypeTextField.frame = CGRectMake(20, 9, 830, 31);
            }
            
            
        }
        else
        {
            reminderTypeTextField.frame = CGRectMake(20, 12, 200, 31);
        }

        
        
        
        reminderTypeTextField.delegate = self;
        reminderTypeTextField.borderStyle = UITextBorderStyleNone;
        reminderTypeTextField.tag = 1000+indexPath.row;
        if (appDelegate.isFromIncomeExpenseView >0)
        {
            [reminderTypeTextField setEnabled:TRUE];
        }
        else
        {
            [reminderTypeTextField setEnabled:FALSE];
        }
       
       // [reminderTypeTextField release];
    }
    else
    {
        reminderTypeTextField = (UITextField *)[cell.contentView viewWithTag:1000+indexPath.row];
    }
     [cell.contentView addSubview:reminderTypeTextField];
    
    reminderTypeTextField.hidden = TRUE;
    
    // Configure the cell...
    [cell.textLabel setFont:[UIFont systemFontOfSize:15.0]];
    
    reminderTypeTextField.font =[UIFont systemFontOfSize:15.0];
    
    
    [reminderTypeTextField setText:[[appDelegate.reminderTypeArray objectAtIndex:indexPath.row]valueForKey:@"reminderType"]];

    
    
    if ([appDelegate.reminderTypeArray count]>0)
    {
        cell.textLabel.hidden = TRUE;
        reminderTypeTextField.hidden = FALSE;
        [reminderTypeTextField setText:[[appDelegate.reminderTypeArray objectAtIndex:indexPath.row]valueForKey:@"reminderType"]];
        
        
        if (appDelegate.isFromIncomeExpenseView < 0 )
        {
            if ([reminderTypeTextField.text isEqualToString:appDelegate.reminderTypeStr])
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                oldIndex = indexPath;
                [oldIndex retain];
            }
        }

    }
    else
    {
        cell.textLabel.hidden = FALSE;
        [cell.textLabel setTextAlignment:UITextAlignmentCenter];
        cell.textLabel.text = @"Create New Reminder Type";
        cell.textLabel.textColor = [UIColor grayColor];
    }
     
    // Configure the cell...
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [reminderTypeTextField release];
    return cell;
     
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    for (int i=0;i<[appDelegate.reminderTypeArray count];i++)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:i+1000];
        [textField resignFirstResponder];
    }
    
    for (int i=0;i<[appDelegate.reminderTypeArray count];i++)
    {
        [appDelegate updateReminderType:[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"reminderType"] rowid:[[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        
    }
    [appDelegate selectReminderType]; 
    
    keytoolBar.hidden = TRUE;
    textFieldIndex  = -1;
    [appDelegate deleteReminderType:[[[appDelegate.reminderTypeArray objectAtIndex:indexPath.row]valueForKey:@"rowid"]intValue]];
   

    
    [appDelegate selectReminderType];

    nslog(@"\n remmider type = %@",appDelegate.reminderTypeArray);
    
    if([appDelegate.reminderTypeArray count]==0)
	{
		lblNoData.hidden = FALSE;
        lblPlus.hidden = FALSE;
		imageView.hidden = FALSE;
        
        
        
        
        
        if(appDelegate.isIPad)
        {
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    imageView.center = CGPointMake(384,(911.0f/2.0f)-70);    
                    lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                    lblPlus.center = CGPointMake(363,455); 
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    imageView.center = CGPointMake(494,(655.0f/2.0f)-70);    
                    lblNoData.center = CGPointMake(494,655.0f/2.0f);   
                    lblPlus.center = CGPointMake(473,327); 
                }
                
                
            }
 
        }
        
		[tblView setHidden:TRUE];
		//[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0,1024,1024)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];

		
	}
	else 
    {
        
        
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        else
        {
            tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
        }

        
        
        
        imageView.hidden = TRUE;
		[tblView setHidden:FALSE];
		[lblNoData setHidden:TRUE];
        lblPlus.hidden = TRUE;

		[tblView reloadData];
		
	}
   // [tblView reloadData];
}

-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew 
{
    
    if (textFieldIndex >0)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldIndex];
        [textField resignFirstResponder];
    }
    
   
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    
    
    
    [appDelegate selectReminderType];
    
    NSMutableArray *rowIdArray = [[NSMutableArray alloc] initWithCapacity:100];
    nslog(@"\n before moving = %@",appDelegate.reminderTypeArray);
    for (int i=0;i<[appDelegate.reminderTypeArray count];i++)
    {
        [rowIdArray addObject:[[appDelegate.reminderTypeArray objectAtIndex:i] valueForKey:@"rowid"]];
    }
    nslog(@"\n rowIdArray  = %@",rowIdArray);

    
	NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:appDelegate.reminderTypeArray];
	id tempObject = [tempArray objectAtIndex:indexOriginal];
	[tempArray removeObjectAtIndex:indexOriginal];
	[tempArray insertObject:tempObject atIndex:indexNew];
	appDelegate.reminderTypeArray = tempArray;
    
    
    //nslog(@"\n after moving = %@",appDelegate.reminderTypeArray);
	
    for (int i=0;i<[appDelegate.reminderTypeArray count];i++)
    {
        [[appDelegate.reminderTypeArray objectAtIndex:i] setObject:[rowIdArray objectAtIndex:i] forKey:@"rowid"];
    }
    
    nslog(@"\n after moving  and setting index..= %@",appDelegate.reminderTypeArray);


    
    
    for (int i=0;i<[appDelegate.reminderTypeArray count];i++)
    {
        [appDelegate updateReminderType:[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"reminderType"] rowid:[[[appDelegate.reminderTypeArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        
    }
    [appDelegate selectReminderType];
    
    //[tempRowArray release];
    
     keytoolBar.hidden = YES;
    [tempArray release];
   
    [rowIdArray release];
    
    [tblView reloadData];
    textFieldIndex = -1;
     
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [self moveFromOriginal:fromIndexPath.row toNew:toIndexPath.row];

}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([appDelegate.reminderTypeArray count]>0)
    {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // Return NO if you do not want the item to be re-orderable.
    
    nslog(@"\n indexpath = %d",indexPath.row);
        return YES;
    if ([appDelegate.reminderTypeArray count]>0)
    {
        return YES;
    }
    else
        return NO;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    nslog(@"\n didSelectRowAtIndexPath");
    
    
    if (tblView.editing)
    {
        UITextField *txtField = (UITextField *)[self.view viewWithTag:1000+indexPath.row];
        [txtField becomeFirstResponder];
    }
    else
    {
        if (appDelegate.isFromIncomeExpenseView < 0 )
        {
            if ([appDelegate.reminderTypeArray count]>0)
            {
                UITableViewCell *oldCell = [tblView cellForRowAtIndexPath:oldIndex];
                
                oldCell.accessoryType = UITableViewCellAccessoryNone;
            }
            
            UITableViewCell *newCell = [tblView cellForRowAtIndexPath:indexPath];
            newCell.accessoryType = UITableViewCellAccessoryCheckmark;
            
            appDelegate.reminderTypeStr = [NSString stringWithFormat:@"%@",[[appDelegate.reminderTypeArray objectAtIndex:indexPath.row]valueForKey:@"reminderType"]];
            
            
            /**
            [appDelegate.reminderTypeStr retain];//Dont know if not retain app crashes..
            */
            
            oldIndex = indexPath;
            [self.navigationController popViewControllerAnimated:YES];
        }

    }
     

}

@end
