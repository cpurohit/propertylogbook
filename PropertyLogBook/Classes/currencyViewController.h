
//  currencyViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"


@interface currencyViewController : UIViewController<UIAlertViewDelegate> {
	
	IBOutlet UITableView *tblView;
    NSMutableArray *currencyArray;
    PropertyLogBookAppDelegate *appDelegate;
    NSIndexPath *oldIndex;
    NSIndexPath *tempIndex;
    NSMutableDictionary *currencyDictionary;

}
@property (nonatomic, retain)NSMutableArray *currencyArray;

@end
