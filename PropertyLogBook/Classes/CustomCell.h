//
//  CustomCell.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

@interface CustomCell : UITableViewCell<UITextFieldDelegate> 
{
    UILabel *propertyLabel;
    UITextField *PropertyTextField;
    UISwitch *agentSwitch;
    UILabel *detailLabel;
    PropertyLogBookAppDelegate *appDelegate;

}
@property (nonatomic, retain)UILabel *propertyLabel;
@property (nonatomic, retain)UITextField *PropertyTextField;
@property (nonatomic, retain)UISwitch *agentSwitch;
@property (nonatomic, retain)UILabel *detailLabel;

@end
