//
//  ReminderViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ReminderViewController.h"
#import "AddReminderViewController.h"

@implementation ReminderViewController


#pragma mark -
#pragma mark View lifecycle

-(void)viewDidUnload
{
    //[appDelegate.bottomView removeFromSuperview];
    [super viewDidUnload];
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
    groupBtnArray = [[NSMutableArray alloc]init];
    propertyDictionary = [[NSMutableDictionary alloc]init];
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    is_any_reminder_overDue = FALSE;
    
    /*
     In ios - 7 this is causing issue...
     gregorian = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
     */
    
    gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
    
    date_formatter = [[NSDateFormatter alloc] init];
    [date_formatter setDateFormat:@"yyyy-MM-dd hh:mm a"];

    test_components = [[NSDateComponents alloc] init];
    components = [[NSDateComponents alloc] init];
    
    
    if(appDelegate.isIPad)
    {
        
        
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
        {
            
            
            btn_reminder_type_pending.frame = CGRectMake(0,0,384,44);
            
            
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_off_ipad_port.png"] forState:UIControlStateNormal];
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_on_ipad_port.png"] forState:UIControlStateSelected];
            
            
            btn_reminder_type_completed.frame = CGRectMake(384,0,384,44);
            
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_off_ipad_port.png"] forState:UIControlStateNormal];
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_on_ipad_port.png"] forState:UIControlStateSelected];
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
        {
            
            
             btn_reminder_type_pending.frame = CGRectMake(0,0,512,44);
            
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_off_ipad_land.png"] forState:UIControlStateNormal];
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_on_ipad_land.png"] forState:UIControlStateSelected];
            
            btn_reminder_type_completed.frame = CGRectMake(512,0,512,44);
            
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_off_ipad_land.png"] forState:UIControlStateNormal];
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_on_ipad_land.png"] forState:UIControlStateSelected];
            
        }
        
        
    }
    else
    {
        [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_off.png"] forState:UIControlStateNormal];
        [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_on.png"] forState:UIControlStateSelected];
        
        
        
        [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_off.png"] forState:UIControlStateNormal];
        [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_on.png"] forState:UIControlStateSelected];
        
       
    }
    
    [btn_reminder_type_pending setSelected:TRUE];
    
	self.navigationItem.title = @"REMINDERS";
	//self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_click)];
	
    
    
    
    
   
    
    
    /*
     Start:
     Sun:0004
     Setting custom image in navigation bar..
     */
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0)
    {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
        /*
         [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor],UITextAttributeTextColor,[UIColor blackColor],UITextAttributeTextShadowColor,[NSValue valueWithUIOffset:UIOffsetMake(1, 0)], UITextAttributeTextShadowOffset ,[UIFont fontWithName:@"System" size:25.0],UITextAttributeFont  , nil]];
         */
        
    }
    
    /*
     Ends:
     Sun:0004
     Setting custom image in navigation bar..
     */

    
    
    if(!appDelegate.isIPad)
    {
        
        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
            UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator.width = -12;
            
            [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
            
            
        }
        else
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
        }

    }
   
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    /*
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_click)] autorelease];
	*/
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_click) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
        UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator1.width = -12;
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
        
    }
    else
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_click) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
    }

    
    
    
    tblView.backgroundColor = [UIColor clearColor];
	
    
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    
    if(!appDelegate.isIPad)
    {
        if(appDelegate.isIphone5)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,367.0f);
        }

    }
    
        

    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,  1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
	
}


-(void)cancel_clicked
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)add_click
{
    
	addReminder = [[AddReminderViewController alloc]initWithNibName:@"AddReminderViewController" bundle:nil];
    addReminder.reminderViewIndex = -1;
	[self.navigationController pushViewController:addReminder animated:YES];
    [addReminder release];

}

- (void)viewWillAppear:(BOOL)animated 
{
    [super viewWillAppear:animated];
    [super setEditing:FALSE];
    [tblView setEditing:FALSE];
    
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    view_reminder_lagend_bg.hidden = TRUE;
    
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

     //[self.view addSubview:appDelegate.bottomView];
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    
    [propertyDictionary removeAllObjects];
    
    if([appDelegate.propertyDetailArray count]==0)
    {
       [appDelegate selectPropertyDetail];
    }
    
    
    btn_reminder_type_pending.selected = TRUE;
    btn_reminder_type_completed.selected = FALSE;
    
    appDelegate.current_selected_reminder_type = 0;
    [appDelegate selectReminder:appDelegate.current_selected_reminder_type];
    [self load_reminder_table];
    
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
        {
            
            
            btn_reminder_type_pending.frame = CGRectMake(0,0,384,44);
            btn_reminder_type_completed.frame = CGRectMake(384,0,384,44);
            
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_off_ipad_port.png"] forState:UIControlStateNormal];
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_on_ipad_port.png"] forState:UIControlStateSelected];
            
            
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_off_ipad_port.png"] forState:UIControlStateNormal];
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_on_ipad_port.png"] forState:UIControlStateSelected];

            
        }
        else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
        {
            
            
            btn_reminder_type_pending.frame = CGRectMake(0,0,512,44);
             btn_reminder_type_completed.frame = CGRectMake(512,0,512,44);
            
            
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_off_ipad_land.png"] forState:UIControlStateNormal];
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_on_ipad_land.png"] forState:UIControlStateSelected];
            
            
            
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_off_ipad_land.png"] forState:UIControlStateNormal];
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_on_ipad_land.png"] forState:UIControlStateSelected];
            
             
        }

        
        
    }

    
    /***/
    UIApplication *app = [UIApplication sharedApplication];
	NSArray *alarmArray = [app scheduledLocalNotifications];
    nslog(@"\n alarm array in view will appear=== %@ \n",alarmArray);
   
    if(appDelegate.isIPad)
    {
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
    // [tblView reloadData];
}

#pragma mark -
#pragma mark load_reminder_table : displays data in tableview
-(void)load_reminder_table
{
    view_reminder_lagend_bg.hidden = TRUE;
    
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
        {
            tblView.frame = CGRectMake(0,44,768,887);
        }
        else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
        {
            tblView.frame = CGRectMake(0,44,1024,630);
        }
    }
    else
    {
        if(appDelegate.isIphone5)
        {
            tblView.frame = CGRectMake(0,44,320,399);
            view_reminder_lagend_bg.frame = CGRectMake(view_reminder_lagend_bg.frame.origin.x, 417,view_reminder_lagend_bg.frame.size.width, view_reminder_lagend_bg.frame.size.height);
        }
        else
        {
            view_reminder_lagend_bg.frame = CGRectMake(view_reminder_lagend_bg.frame.origin.x,328,view_reminder_lagend_bg.frame.size.width, view_reminder_lagend_bg.frame.size.height);
            tblView.frame = CGRectMake(0,44,320,311);
        }
    }

    
    [propertyDictionary removeAllObjects];
    
    for (int i = 0;i <[appDelegate.propertyDetailArray count];i++)
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (int  j =0;j<[appDelegate.reminderArray count];j++)
        {
            if ([[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"0"] isEqualToString:[[appDelegate.reminderArray objectAtIndex:j]valueForKey:@"property"]])
            {
                [[appDelegate.reminderArray objectAtIndex:j] setObject:[[appDelegate.propertyDetailArray objectAtIndex:i]objectForKey:@"image"] forKey:@"image"];
                [array addObject:[appDelegate.reminderArray objectAtIndex:j]];
            }
        }
        
        
        if ([array count]>0)
        {
            [propertyDictionary setObject:array forKey:[NSString stringWithFormat:@"%@",[[appDelegate.propertyDetailArray objectAtIndex:i]valueForKey:@"0"]]];
        }
        
         nslog(@"\n array in load_reminder_table === %@ \n",array);
        [array release];
    }
    
    /*
    nslog(@"\n property info dictionary === %@ \n",propertyDictionary);
    nslog(@"\n reminder array ==== %@ \n",appDelegate.reminderArray);
    */
    
    
    
    if([appDelegate.reminderArray count]==0)
	{
        reminderImage.hidden = FALSE;
		lblNoData.hidden = FALSE;
        [lblPlus setHidden:FALSE];
		
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                
                reminderImage.center = CGPointMake(384,(911.0f/2.0f)-70);
                lblNoData.center = CGPointMake(384,911.0f/2.0f);
                lblPlus.center = CGPointMake(263,465);
                
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                reminderImage.center = CGPointMake(494,(655.0f/2.0f)-70);
                lblNoData.center = CGPointMake(494,655.0f/2.0f);
                lblPlus.center = CGPointMake(372,338);
            }
            
            
        }
        
        [tblView setHidden:TRUE];
        
		//[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0,1024,1024)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];
        
		
	}
	else
    {
		
        [tblView setHidden:FALSE];
        reminderImage.hidden = TRUE;
		lblNoData.hidden = TRUE;
        [lblPlus setHidden:TRUE];
		[tblView reloadData];
		
	}

    
}

#pragma mark -
#pragma mark viewWillDisappear

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
    //[appDelegate.bottomView removeFromSuperview];
    
    appDelegate.badgeCount = 0;
    appDelegate.current_selected_reminder_type = 0;
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
    
}

#pragma mark -
#pragma mark viewDidAppear

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /**
    UIButton *button = (UIButton *)[self.view viewWithTag:5000];
    nslog(@"button tag === %d",button.tag);
     */
    
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    if(appDelegate.isIPad)
    {
        
        
        
        
        [appDelegate manageViewControllerHeight];
        
        
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
        {
            
            
            btn_reminder_type_pending.frame = CGRectMake(0,0,384,44);
            
            
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_off_ipad_port.png"] forState:UIControlStateNormal];
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_on_ipad_port.png"] forState:UIControlStateSelected];
            
            
            btn_reminder_type_completed.frame = CGRectMake(384,0,384,44);
            
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_off_ipad_port.png"] forState:UIControlStateNormal];
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_on_ipad_port.png"] forState:UIControlStateSelected];
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
        {
            
            
            btn_reminder_type_pending.frame = CGRectMake(0,0,512,44);
            
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_off_ipad_land.png"] forState:UIControlStateNormal];
            [btn_reminder_type_pending setImage:[UIImage imageNamed:@"reminder_pending_on_ipad_land.png"] forState:UIControlStateSelected];
            
            btn_reminder_type_completed.frame = CGRectMake(512,0,512,44);
            
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_off_ipad_land.png"] forState:UIControlStateNormal];
            [btn_reminder_type_completed setImage:[UIImage imageNamed:@"reminder_completed_on_ipad_land.png"] forState:UIControlStateSelected];
            
        }
        

        
        
        [tblView reloadData];
        
    }

    
    
    
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    if(appDelegate.isIPad)
    {
        
        if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
        {
            
            reminderImage.center = CGPointMake(384,(911.0f/2.0f)-70);    
            lblNoData.center = CGPointMake(384,911.0f/2.0f);   
            lblPlus.center = CGPointMake(263,465); 
            
            
        }
        else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
        {
            reminderImage.center = CGPointMake(494,(655.0f/2.0f)-70);    
            lblNoData.center = CGPointMake(494,655.0f/2.0f);   
            lblPlus.center = CGPointMake(372,338); 
        }
        
        
    }

}

#pragma mark - swtch_chaged

-(void)swtch_chaged:(id)sender
{
   
    //UISwitch *swtch = (UISwitch *)sender;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tblView];
    NSIndexPath *indexPath = [tblView indexPathForRowAtPoint:buttonPosition];
    
    NSString*switchTag = [NSString stringWithFormat:@"%d99%d",indexPath.section+1,indexPath.row+1];
    int tag = [switchTag intValue];

    
    
    temp_button_for_reminder_switch = (UIButton*)sender;
    temp_button_for_reminder_switch.selected = !temp_button_for_reminder_switch.selected;
    
        
    
    
    nslog(@"\n swtch tag = %d ",temp_button_for_reminder_switch.tag);
    
    
    
    NSArray* sectionNameArray = [propertyDictionary allKeys];
    
    NSString*sectionTag = [NSString stringWithFormat:@"%d",tag];
    NSArray*sectionTagArray = [sectionTag componentsSeparatedByString:@"99"];
    
    nslog(@"\n sectionTagArray  = %@",sectionTagArray);
    
     selectedSection = [[sectionTagArray objectAtIndex:0] intValue]-1;
     selectedRow = [[sectionTagArray objectAtIndex:1] intValue]-1;
    
    
    nslog(@"\n selectedRow = %d ",selectedRow);
    
    tempArray_reminder_checklist = [propertyDictionary objectForKey:[sectionNameArray objectAtIndex:selectedSection]];
    
    
    nslog(@"\n tempArray_reminder_checklist  = %@",[tempArray_reminder_checklist objectAtIndex:selectedRow]);
    
    replaceDictionary = [tempArray_reminder_checklist objectAtIndex:selectedRow];
    
    if (temp_button_for_reminder_switch.selected)
    {
        
        
       /*
        SUN:004
        NEED TO CANCEL LOCAL NOTIFICATION FOR REMINDERS WHICH ARE COMPLETED.
        */
        reminder =1;//This indicates reminder is completed
        
        if([[replaceDictionary objectForKey:@"reminder"] intValue]==0 && (![[[replaceDictionary objectForKey:@"repeat"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"No Repeat"])
           )
        {
           
            
            UIAlertView*alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"This is a recurring reminder. Do you want to keep this reminder for the next recurring event?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [alertView show];
            [alertView release];
            return;
            
            
        }
        else
        {
            [replaceDictionary setObject:@"1" forKey:@"reminder"];
            nslog(@"\n Switch is on");
           
        }
        
        
        
    }
    else
    {
        
        [replaceDictionary setObject:@"0" forKey:@"reminder"];
        nslog(@"\n Switch is off");        
        reminder = 0;//this indicates reminder is not completed
    }
    
    [tempArray_reminder_checklist  replaceObjectAtIndex:selectedRow withObject:replaceDictionary];  
    nslog(@"\n tempArray_reminder_checklist  = %@",[tempArray_reminder_checklist objectAtIndex:selectedRow]);

    
    
   // int index = swtch.tag%2000;
    
    
    reminderType = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"reminderType"];
    property = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"property"];
    date = [[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"date"];
    date = [appDelegate fetchDateInCommonFormate:date];
    time = [[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"time"];

    repeat = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"repeat"];
    notes = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"notes"];
    appDelegate.reminderRowID = [[[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"rowid"] intValue];
    
    [appDelegate UpdateReminder:reminderType property:property date:date repeat:repeat notes:notes reminder:reminder time:time rowid:[[[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"rowid"]intValue]];
    
    
    
    [self cancelAlarm];
    if (reminder == 0)
    {
        [self scheduleAlarm];
    }
    nslog(@"reminder value ==== %d",reminder);
    

    [appDelegate selectReminder:appDelegate.current_selected_reminder_type];
    [self load_reminder_table];
   
}

#pragma mark -
#pragma mark reminder method

- (IBAction) scheduleAlarm
{
     
    // NSMutableArray *temp=[appDelegate selectExerciseInfo:[appDelegate getDBPath]];
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
	
    // Get the current date
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
	NSDateFormatter *timeFormat = [[NSDateFormatter alloc]init];
	[dateFormat setDateFormat:@"yyyy-MM-dd"];
 //   [dateFormat setDateFormat:appDelegate.regionDateFormatter];
	[timeFormat setDateFormat:@"hh:mm a"];
    
    //NSDate *pickerDate = [dateFormat dateFromString:date];
    nslog(@"date is ==== %@",date);
    //NSDate *pickerDate = [appDelegate.regionDateFormatter dateFromString:date];
    NSDate *pickerDate = [dateFormat dateFromString:date];
    nslog(@"picker date ===== %@",pickerDate);
    
    nslog(@"date is ====== %@",[NSDate date]);
	NSDate *pickerTime = [timeFormat dateFromString:time];
    // Break the date up into components
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
												   fromDate:pickerDate];
	
	
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
												   fromDate:pickerTime];
    // Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
	// Notification will fire in one minute
    [dateComps setMinute:[timeComponents minute]];
	[dateComps setSecond:[timeComponents second]];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    [dateComps release];
	
	UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
    {
        [dateFormat release];
        [timeFormat release];

        return;
    }
        
	nslog(@"item date = %@",itemDate);
    localNotif.fireDate = itemDate;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
	
	// Notification details
    localNotif.alertBody = [NSString stringWithFormat:@"Reminder Type: %@ \n Property: %@ \n Notes: %@",reminderType,property,notes];
	// Set the action button
    localNotif.alertAction = @"View";
	
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = appDelegate.badgeCount+1;

	if ([repeat isEqualToString:@"  Weekly"])
    {
        localNotif.repeatInterval=NSWeekCalendarUnit;
    }
    else if ([repeat isEqualToString:@"  Quarterly"])
    {
        localNotif.repeatInterval=NSQuarterCalendarUnit;
    }
    else if ([repeat isEqualToString:@"  Bi Weekly"])
    {
        localNotif.repeatInterval=NSWeekCalendarUnit * 2;
    }
    else if ([repeat isEqualToString:@"  Monthly"])
    {
        localNotif.repeatInterval=NSMonthCalendarUnit;
    }
    else if ([repeat isEqualToString:@"  Yearly"])
    {
        localNotif.repeatInterval = NSYearCalendarUnit;
    }

	// Specify custom data for the notification
    NSDictionary *infoDict;
   
        infoDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:appDelegate.reminderRowID] forKey:@"Key"];
    
    
    localNotif.userInfo = infoDict;
	
	// Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    [localNotif release];
    [dateFormat release];
    [timeFormat release];
	
}

-(void) cancelAlarm 
{
    
	UIApplication *app = [UIApplication sharedApplication];
	NSArray *alarmArray = [app scheduledLocalNotifications];
    int temp = appDelegate.reminderRowID;
    //    NSString *temp = [NSString stringWithFormat:@"%d",[[[appDelegate.reminderArray objectAtIndex:reminderViewIndex]valueForKey:@"rowid"]intValue]]];
    
	//nslog(@"Trying to cancel alarm %@ date Type:%@", alarm.alertBody, temp);
	for (int i=0; i<[alarmArray count]; i++) 
    {
		UILocalNotification* oneAlarm = [alarmArray objectAtIndex:i];
		int FinalTemp = [[oneAlarm.userInfo objectForKey:@"Key"]intValue];
        nslog(@"\n final temp unser info ===%d \n ",FinalTemp);
        nslog(@"\n appDelegate.reminderRowID ===%d \n",appDelegate.reminderRowID);
		if (FinalTemp==temp)
        {
			nslog(@"Cancelling alarm: %@", oneAlarm.alertBody);
			[app cancelLocalNotification:oneAlarm];
		}
	}
    
}


#pragma mark -
#pragma mark group button method

-(void)showHideSectionByBgImage:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    nslog(@"\n btn tag = %d",btn.tag);
    
    int tag = btn.tag;
    int count = 0;
    for (int i =0; i<[groupBtnArray count];i++)
    {
        if (tag  == [[groupBtnArray objectAtIndex:i]intValue])
        {
            
            [groupBtnArray removeObjectAtIndex:i];
            [btn setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateNormal];
            count = 1;
        }
    }
    
    if (count == 0)
    {
        [groupBtnArray addObject:[NSNumber numberWithInt:tag]];
        [btn setImage:[UIImage imageNamed:@"arrow_side"] forState:UIControlStateNormal];
    }
    else
    {
        [btn setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateNormal];
        
    }
    
    
    
    [tblView reloadData];
}


#pragma mark -
#pragma mark group button method

-(void)groupBtn_clicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    nslog(@"\n btn tag = %d",btn.tag);

    int tag = btn.tag;
    int count = 0;
    for (int i =0; i<[groupBtnArray count];i++)
    {
        if (tag  == [[groupBtnArray objectAtIndex:i]intValue])
        {
            
            [groupBtnArray removeObjectAtIndex:i];
             [btn setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateNormal];
            count = 1;
        }
    }
    
    if (count == 0)
    {
        [groupBtnArray addObject:[NSNumber numberWithInt:tag]];
        [btn setImage:[UIImage imageNamed:@"arrow_side"] forState:UIControlStateNormal];
    }
    else
    {
        [btn setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateNormal];

    }
    
    
    
    [tblView reloadData];
}

#pragma mark -
#pragma mark Table view data source



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320,44)] autorelease];
    
    NSArray* sectionNameArray = [propertyDictionary allKeys];
    
    
    nslog(@"\n image in header. =  %@",[[[propertyDictionary objectForKey:[sectionNameArray objectAtIndex:section]] objectAtIndex:0] objectForKey:@"image"]);
    
    UIImageView*cell_section_imageView = [[[UIImageView alloc] init] autorelease];
    
    
    if(appDelegate.isIPad)
    {
        
        
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            
            
        {
            cell_section_imageView.frame = CGRectMake(0, 0,768,44);
            //cell_section_imageView.image = [UIImage imageNamed:@"section_header_bg_port.png"];
            cell_section_imageView.image = [UIImage imageNamed:@"section_header_bg_land.png"];
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            cell_section_imageView.frame = CGRectMake(0, 0,1024,44);
            cell_section_imageView.image = [UIImage imageNamed:@"section_header_bg_land.png"];
        }
        
        
        
    }
    else
    {
        cell_section_imageView.frame = CGRectMake(0, 0,320,44);
        cell_section_imageView.image = [UIImage imageNamed:@"section_header_bg.png"];

    }
    
    
    [view addSubview:cell_section_imageView];
    
    
    UIImageView*cell_property_imageView = [[[UIImageView alloc] init] autorelease];
    cell_property_imageView.frame = CGRectMake(5,12,23,23);
    cell_property_imageView.image = [[[propertyDictionary objectForKey:[sectionNameArray objectAtIndex:section]] objectAtIndex:0] objectForKey:@"image"];
    [view addSubview:cell_property_imageView];
    
    
    

    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            btn.frame = CGRectMake(726,10,27,26);;
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            btn.frame = CGRectMake(982,10,27,26);
        }
        
        
    }
    else
    {
        [btn setFrame:CGRectMake(278,10,27,26)];
    }
    
    
    
    
    btn.tag = 5000+section;
    [btn setImage:[UIImage imageNamed:@"arrow_down.png"] forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(groupBtn_clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIButton*bg_button = [UIButton buttonWithType:UIButtonTypeCustom];
    bg_button.tag = section+5000;
    [bg_button addTarget:self action:@selector(showHideSectionByBgImage:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            bg_button.frame = CGRectMake(10,0,768,44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            bg_button.frame = CGRectMake(10,0,1024,44);
        }
        
    }
    else
    {
        bg_button.frame = CGRectMake(10,0,320,44);
    }

    
    [view addSubview:bg_button];

    

    if ([groupBtnArray count]>0)
    {
        for (int i =0; i<[groupBtnArray count];i++)
        {
            if ([[groupBtnArray objectAtIndex:i]intValue] == 5000+section)
            {
                [btn setImage:[UIImage imageNamed:@"arrow_open.png"] forState:UIControlStateNormal];
            }

        }
    }
   
    else
    {
        [btn setImage:[UIImage imageNamed:@"arrow_down.png"] forState:UIControlStateNormal];
    }
    UILabel *lable;
    
    /*Sun:0004
     
     ipad Pending..
     */
    lable = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, 240, 30)];
    if(appDelegate.isIPad)
    {
        
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
           lable.frame = CGRectMake(40, 10, 650, 30);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            lable.frame = CGRectMake(40, 10, 900, 30);
        }
        
    }
    else
    {
       lable.frame = CGRectMake(40, 10, 240, 30);
    }
    
    
    [lable setFont:[UIFont boldSystemFontOfSize:15.0]];
    lable.text =  [sectionNameArray objectAtIndex:section];
    [lable setBackgroundColor:[UIColor clearColor]];
        [view addSubview:btn];
    [view addSubview:lable];
    //[btn release];
    [lable release];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

   
    numberSection = [appDelegate.propertyDetailArray count];
    return [[propertyDictionary allKeys] count];
    

}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if(section == ([[propertyDictionary allKeys] count]-1))
    {
        //return @"Note: To use this feature, IOS5 users can set notification under Settings -> Notifications -> Property Log Book";
    }
    return @"";
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;  
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    
    
      
    nslog(@"\n test = %@",[propertyDictionary allKeys]); 
    NSArray *sectionNameArray  =[propertyDictionary allKeys]; 
    NSString *sectionNameString = [sectionNameArray objectAtIndex:section];

    for (int i =0; i<[groupBtnArray count];i++)
    {
        if (section == ([[groupBtnArray objectAtIndex:i] intValue])%5000)
        {
            return 0;
        }
    }
    return  [[propertyDictionary objectForKey:sectionNameString] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 
{
    NSArray* sectionNameArray = [propertyDictionary allKeys];
 
    return [sectionNameArray objectAtIndex:section];
    
    /*
    NSMutableArray *tempArray = [propertyDictionary objectForKey:[sectionNameArray objectAtIndex:section]];
    nslog(@"\n propertyDictionary  = %@",propertyDictionary);
*/
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    numberSection = [appDelegate.propertyDetailArray count];
    static NSString *CellIdentifier = @"Cell";
    UILabel *label, *detailLabel, *notelabel,*date_time_label;
    UISwitch *swtch;
    
    UIButton*btn_checklist;
    
    
    UIImageView *img;
    
    UIImageView*imgView_reminder_bell;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
  
        cell.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"settings_cell_bg.png"]];
        /*
            Sun:0004
            ipad 
         */
        
        label = [[UILabel alloc]initWithFrame:CGRectMake(40, 0, 180, 30)];
        label.backgroundColor = [UIColor clearColor];
        label.tag = 400;
        label.numberOfLines = 0;
        label.lineBreakMode = UILineBreakModeTailTruncation;
        [label setFont:[UIFont systemFontOfSize:15.0]];
        
        
        [cell.contentView addSubview:label];
        
        
        
        detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(150, 0, 50, 30)];
        
         
       
        detailLabel.tag = 500;
        //lbl.text = @"Select Mesurement:";
        detailLabel.numberOfLines = 0;
        [detailLabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];
        detailLabel.lineBreakMode = UILineBreakModeTailTruncation;
        [detailLabel setFont:[UIFont systemFontOfSize:15.0]];
        [detailLabel setBackgroundColor:[UIColor clearColor]];
        
        [cell.contentView addSubview:detailLabel];
        
        
        
        notelabel = [[UILabel alloc]initWithFrame:CGRectMake(33, 25,80, 30)];
        
 
        
        notelabel.tag = 600;
        //lbl.text = @"Select Mesurement:";
        [notelabel setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];
        notelabel.numberOfLines = 0;
        notelabel.lineBreakMode = UILineBreakModeTailTruncation;
        [notelabel setFont:[UIFont systemFontOfSize:13.0]];
        [notelabel setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:notelabel];
        
        

        date_time_label= [[UILabel alloc]initWithFrame:CGRectMake(110,25,120, 30)];
        

        
        date_time_label.tag= 700;
        
        
        date_time_label.numberOfLines = 0;
        date_time_label.lineBreakMode = UILineBreakModeTailTruncation;
        [date_time_label setFont:[UIFont systemFontOfSize:11.0]];
        [date_time_label setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:date_time_label];
        
        
        

        
        /*
         Sun:0004
         ipad pending..
         */
        
        //btn_checklist = [UIButton buttonWithType:UIButtonTypeCustom];
        
        
        
        /*
         Starts:Sun:004
         Reminder Bell Image
         */
        
        imgView_reminder_bell = [[UIImageView alloc] init];
        imgView_reminder_bell.image = [UIImage imageNamed:@"reminder_type_icon.png"];
        
          
        imgView_reminder_bell.tag = 900;
        [cell addSubview:imgView_reminder_bell];
        
        /*
         Ends:Sun:004
         */
        
        btn_checklist = [[UIButton alloc] initWithFrame:CGRectMake(10, 5, 40, 20)];
        [btn_checklist setImage:[UIImage imageNamed:@"checkmark_off.png"] forState:UIControlStateNormal];
        [btn_checklist setImage:[UIImage imageNamed:@"checkmark_on.png"] forState:UIControlStateSelected];
        btn_checklist.tag = 800;
        [cell addSubview:btn_checklist];
        
		
    }
	
    else
    {
        

        
        label = (UILabel*)[cell viewWithTag:400];
        notelabel = (UILabel*)[cell viewWithTag:600];
        date_time_label = (UILabel*)[cell viewWithTag:700];
        detailLabel =(UILabel*)[cell viewWithTag:500];
        btn_checklist= (UIButton*)[cell viewWithTag:800];
        
        
        imgView_reminder_bell= (UIImageView*)[cell viewWithTag:900];
        
        
        
        [label retain];
        [notelabel retain];
        [date_time_label retain];
        [imgView_reminder_bell retain];
        [detailLabel retain];
        
        
        
        
    }

   
    
    
    //
    
    if(appDelegate.isIPad)
    {
        
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            label.frame = CGRectMake(50,0,320, 30);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            label.frame = CGRectMake(50, 0,620, 30);
        }
        
    }
    else
    {
        label.frame = CGRectMake(40, 0, 180, 30);
    }

    //
    
    if(appDelegate.isIPad)
    {
        detailLabel.frame = CGRectMake(350, 0,150, 30);
    }
    else
    {
        detailLabel.frame = CGRectMake(150, 0, 50, 30);
    }
    
    //
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            notelabel.frame = CGRectMake(43,22,70,30);
        }
        else  if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            notelabel.frame = CGRectMake(43,22,70,30);
        }
    }
    
    //
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            date_time_label.frame = CGRectMake(125,22,160,30);
        }
        else  if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            date_time_label.frame = CGRectMake(125,22,160,30);
        }
    }
    
    //
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            
            
            btn_checklist.frame = CGRectMake(10,15,25,25);
            
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            btn_checklist.frame = CGRectMake(10,15, 25, 25);
            
            
        }
        
    }
    else
    {
        btn_checklist.frame = CGRectMake(10,15, 25, 25);
        
        
    }

    //
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            
            imgView_reminder_bell.frame = CGRectMake(697, 9, 44,44);
            
            
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            
            imgView_reminder_bell.frame = CGRectMake(930, 9, 44,44);
            
        }
        
    }
    else
    {
        
        imgView_reminder_bell.frame = CGRectMake(258, 9, 44,44);
        
    }

    
    NSString*switchTag = [NSString stringWithFormat:@"%d99%d",indexPath.section+1,indexPath.row+1];
    //btn_checklist.tag =[switchTag intValue];
    
    [btn_checklist addTarget:self action:@selector(swtch_chaged:) forControlEvents:UIControlEventTouchUpInside];

    
    
    NSArray* sectionNameArray = [propertyDictionary allKeys];
    NSMutableArray *tempArray = [propertyDictionary objectForKey:[sectionNameArray objectAtIndex:indexPath.section]];
    NSMutableDictionary*tempDictionaryForArray = [tempArray objectAtIndex:indexPath.row];
    
    
    
    
    
    nslog(@"\n propertyDictionary in cell for row= %@ \n",propertyDictionary);
    nslog(@"\n tempArray in cell for row = %@ \n",tempArray);
    nslog(@"\n tempDictionaryForArray in cell for row = %@ \n",tempDictionaryForArray);
    
    
    NSDate*current_date = [NSDate date];
    NSDate*reminder_date = [date_formatter dateFromString:[[tempArray objectAtIndex:indexPath.row] valueForKey:@"reminderDueDateTime"]];

    
    NSDate*next_fire_date = [date_formatter dateFromString:[[tempArray objectAtIndex:indexPath.row] valueForKey:@"next_fire_date"]];

    
    
    /*
     Changing region may cause this issue.
     */
    
    NSTimeInterval temp_timeInterval = [current_date timeIntervalSinceDate:next_fire_date];
    nslog(@"\n temp_timeInterval  = %f \n",temp_timeInterval);
    
    
    
    
    
    
    if(reminder_date!=nil)
    {
        if(temp_timeInterval>0 && [[[tempArray objectAtIndex:indexPath.row] valueForKey:@"reminder"] intValue]==0)
        {
                nslog(@"\n color this cell to orange...\n");
                is_any_reminder_overDue = TRUE;
                view_reminder_lagend_bg.hidden = FALSE;
                if(appDelegate.isIPad)
                {
                    
                    if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
                    {
                        if(appDelegate.isIOS7)
                        {
                            view_reminder_lagend_bg.frame = CGRectMake(0,870,768, view_reminder_lagend_bg.frame.size.height);
                            tblView.frame = CGRectMake(0,44,768,820);
                        }
                        else
                        {
                            view_reminder_lagend_bg.frame = CGRectMake(0,890,768, view_reminder_lagend_bg.frame.size.height);
                            tblView.frame = CGRectMake(0,44,768,840);
                        }
                        
                        
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
                    {
                        if(appDelegate.isIOS7)
                        {
                            view_reminder_lagend_bg.frame = CGRectMake(0,610,1024,view_reminder_lagend_bg.frame.size.height);
                            tblView.frame = CGRectMake(0,44,1024,560);
                        }
                        else
                        {
                            view_reminder_lagend_bg.frame = CGRectMake(0,630,1024,view_reminder_lagend_bg.frame.size.height);
                            tblView.frame = CGRectMake(0,44,1024,580);
                        }
                        
                    }
                }
                else
                {
                    if(appDelegate.isIphone5)
                    {
                        tblView.frame = CGRectMake(0,44,320,369);
                        
                        view_reminder_lagend_bg.frame = CGRectMake(view_reminder_lagend_bg.frame.origin.x,417,view_reminder_lagend_bg.frame.size.width, view_reminder_lagend_bg.frame.size.height);
                    }
                    else
                    {
                        tblView.frame = CGRectMake(0,44,320,281);
                        
                        view_reminder_lagend_bg.frame = CGRectMake(view_reminder_lagend_bg.frame.origin.x, 328,view_reminder_lagend_bg.frame.size.width, view_reminder_lagend_bg.frame.size.height);
                        
                    }
                }
            
            
            
            
            
                if([[[[tempArray objectAtIndex:indexPath.row] valueForKey:@"repeat"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"No Repeat"])
                {
                    imgView_reminder_bell.image = [UIImage imageNamed:@"due_reminder_type_icon.png"];
                }
                else
                {
                    imgView_reminder_bell.image = [UIImage imageNamed:@"due_reminder_type_icon_recurring.png"];
                }
            
                
        }
        else
        {
            
            if([[[[tempArray objectAtIndex:indexPath.row] valueForKey:@"repeat"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"No Repeat"])
            {
                 imgView_reminder_bell.image = [UIImage imageNamed:@"reminder_type_icon.png"];
            }
            else
            {
                imgView_reminder_bell.image = [UIImage imageNamed:@"reminder_type_icon_recurring.png"];
            }
            
           
        }

    }
    
    
        
    
     nslog(@"\n propertyDictionary in reminderview  = %@",propertyDictionary);
 

    
   label.text = [[[tempArray objectAtIndex:indexPath.row] valueForKey:@"reminderType"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    /*
    label.text = [[tempArray objectAtIndex:indexPath.row] valueForKey:@"reminderType"];
    */
    
    
    notelabel.text = [NSString stringWithFormat:@"%@",[[tempArray objectAtIndex:indexPath.row] valueForKey:@"repeat"]];
    
   // notelabel.backgroundColor = [UIColor redColor];
    
    
    nslog(@"\n next fired date for reminder  = %@",[appDelegate.regionDateFormatter stringFromDate:next_fire_date]);
    
    date_time_label.text = [NSString stringWithFormat:@"%@ %@",[appDelegate.regionDateFormatter stringFromDate:next_fire_date],[[tempArray objectAtIndex:indexPath.row] valueForKey:@"time"]];
    
    
    if([[[tempArray objectAtIndex:indexPath.row] valueForKey:@"reminder"] intValue] == 1)
    {
        

      //  [[[[propertyDictionary objectForKey:[sectionNameArray objectAtIndex:indexPath.section]]objectAtIndex:indexPath.row] valueForKey:@"reminder"] ]
      
        nslog(@"\n value changed...TO YES");
        
        //[swtch setOn:YES];
        
        btn_checklist.selected = TRUE;
        [btn_checklist setSelected:TRUE];
    }
    else
    {
        
        nslog(@"\n value changed...TO NO");
        //[swtch setOn:NO];
        btn_checklist.selected = FALSE;
        [btn_checklist setSelected:FALSE];
    }
    [btn_checklist setImage:[UIImage imageNamed:@"checkmark_off.png"] forState:UIControlStateNormal];
    [btn_checklist setImage:[UIImage imageNamed:@"checkmark_on.png"] forState:UIControlStateSelected];

    
    
    
    
    [label release];
    [imgView_reminder_bell release];
    [date_time_label release];
    [notelabel release];
    [detailLabel release];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}






- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
   // nslog(@"\n appDelegate.reminderArray = %@ ",appDelegate.reminderArray);
    
    
    NSArray* sectionNameArray = [propertyDictionary allKeys];
    NSMutableArray *tempArray = [propertyDictionary objectForKey:[sectionNameArray objectAtIndex:indexPath.section]];
    nslog(@"\n tempArray in commitEditingStyle  = %@",tempArray);
    nslog(@"\n dic in commitEditingStyle  = %@",[tempArray objectAtIndex:indexPath.row]);
   
    
    
  [appDelegate DeleteReminder:[[[tempArray objectAtIndex:indexPath.row] objectForKey:@"rowid" ] intValue ] ];
    
    [tempArray removeObjectAtIndex:indexPath.row]; 
    
    if([tempArray count]==0)
    {
        [propertyDictionary removeObjectForKey:[sectionNameArray objectAtIndex:indexPath.section]];
    }
    else
    {
        [propertyDictionary setObject:tempArray forKey:[sectionNameArray objectAtIndex:indexPath.section]]; 
    }
    
 
    
    
    [self cancelAlarm];
   
   
    //[appDelegate.reminderArray removeObjectAtIndex:indexPath.row];
   
    
    view_reminder_lagend_bg.hidden = TRUE;
    
    if([propertyDictionary count]==0)
	{
        reminderImage.hidden = FALSE;
		lblNoData.hidden = FALSE;
        [lblPlus setHidden:FALSE];
        
        
        
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                
                reminderImage.center = CGPointMake(384,(911.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(384,911.0f/2.0f);   
                lblPlus.center = CGPointMake(263,465); 
                
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                reminderImage.center = CGPointMake(494,(655.0f/2.0f)-70);    
                lblNoData.center = CGPointMake(494,655.0f/2.0f);   
                lblPlus.center = CGPointMake(372,338); 
            }
            
            
        }

        
        [tblView setHidden:TRUE];
		//[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
		
        
        
        UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0, 1024,1024)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];

		
	}
	else 
    {
		[tblView setHidden:FALSE];
        reminderImage.hidden = TRUE;
		lblNoData.hidden = TRUE;
        [lblPlus setHidden:TRUE];
		[tblView reloadData];
		
	}
     
	[tblView reloadData];
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([appDelegate.reminderArray count]>0)
    {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}




#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    NSArray* sectionNameArray = [propertyDictionary allKeys];
    NSMutableArray *tempArray = [propertyDictionary objectForKey:[sectionNameArray objectAtIndex:indexPath.section]];
    nslog(@"\n propertyDictionary  = %@",[tempArray objectAtIndex:indexPath.row]);
    appDelegate.reminderArray = tempArray;
    
    //   nslog(@"\n tempArray  = %@",[tempArray objectAtIndex:indexPath]);
    
    /*
    label.text = [[tempArray objectAtIndex:indexPath.row] valueForKey:@"reminderType"];
    notelabel.text = [NSString stringWithFormat:@"%@",[[tempArray objectAtIndex:indexPath.row] valueForKey:@"notes"]];
    
    if([[[tempArray objectAtIndex:indexPath.row] valueForKey:@"reminder"] intValue] == 1)
    {
        [swtch setOn:YES];
    }
    else
    {
        [swtch setOn:NO];
    }
    
    */
    
    nslog(@"\n appDelegate.reminderArray  = %@",appDelegate.reminderArray );
    
    
    if ([appDelegate.reminderArray count]>0)
    {
        addReminder = [[AddReminderViewController alloc]initWithNibName:@"AddReminderViewController" bundle:nil];
        
        //addReminder.reminderViewIndex = indexPath.row;
        addReminder.reminderViewIndex = indexPath.row ;
       
          nslog(@"\n appDelegate did select = %@", [appDelegate.reminderArray objectAtIndex:indexPath.row]);
        [self.navigationController pushViewController:addReminder animated:YES];
        [addReminder release];
    }
    
}

#pragma mark -
#pragma mark btn_reminder_type_clicked


-(IBAction)btn_reminder_type_clicked:(id)sender
{
    
    btn_reminder_type_pending.selected = FALSE;
    btn_reminder_type_completed.selected = FALSE;
    
    UIButton*temp_button = (UIButton*)sender;
    temp_button.selected = TRUE;
    if(temp_button.tag==8001)
    {
        appDelegate.current_selected_reminder_type = 0;
    }
    else if(temp_button.tag==8002)
    {
        appDelegate.current_selected_reminder_type = 1;
    }
    [appDelegate selectReminder:appDelegate.current_selected_reminder_type];
    [self load_reminder_table];

    
}

#pragma mark -
#pragma mark clickedButtonAtIndex


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex==0)
    {
        
        [replaceDictionary setObject:@"1" forKey:@"reminder"];
        [tempArray_reminder_checklist  replaceObjectAtIndex:selectedRow withObject:replaceDictionary];
        nslog(@"\n tempArray  = %@",[tempArray_reminder_checklist objectAtIndex:selectedRow]);
        
        
        
        
        
        
        reminderType = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"reminderType"];
        property = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"property"];
        date = [[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"date"];
        date = [appDelegate fetchDateInCommonFormate:date];
        time = [[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"time"];
        
        repeat = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"repeat"];
        notes = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"notes"];
        appDelegate.reminderRowID = [[[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"rowid"] intValue];
        
        [appDelegate UpdateReminder:reminderType property:property date:date repeat:repeat notes:notes reminder:reminder time:time rowid:[[[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"rowid"]intValue]];
        [self cancelAlarm];
        if (reminder == 1)
        {
            [self scheduleAlarm];
        }
        nslog(@"reminder value ==== %d",reminder);
        
        
        [appDelegate selectReminder:appDelegate.current_selected_reminder_type];
        [self load_reminder_table];

        
        
    }
    else
    {
        
       
       
        
        
        
        
        
        
        reminderType = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"reminderType"];
        
        property = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"property"];
        date = [[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"date"];
        time = [[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"time"];
        
        date = [appDelegate fetchDateInCommonFormate:date];
        repeat = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"repeat"];
        
        notes = [[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"notes"];
        appDelegate.reminderRowID = [[[tempArray_reminder_checklist objectAtIndex:selectedRow]valueForKey:@"rowid"] intValue];
        
        
        NSDate*next_fire_date = [date_formatter dateFromString:[[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"next_fire_date"]];
        
        
        
        /*
         Changing region may cause this issue.
         */
        NSDate*current_date = [NSDate date];
       
        
        NSTimeInterval temp_timeInterval = [current_date timeIntervalSinceDate:next_fire_date];
        nslog(@"\n temp_timeInterval  = %f \n",temp_timeInterval);

        
        
        
        
        while(temp_timeInterval>0)
        {
            
            
            
            if([[[[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"repeat"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"Weekly"])
            {
                nslog(@"\n weekly set new fire date... \n");
                
                next_fire_date = [next_fire_date dateByAddingTimeInterval:(86400*7)];
                [appDelegate UpdateReminderNextFireDate:[date_formatter stringFromDate:next_fire_date] rowid:[[[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"rowid"] intValue]];
                
                
                
            }
            
            else if([[[[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"repeat"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"Monthly"])
            {
                nslog(@"\n Monthly set new fire date... \n");
                
                
                
                NSDateComponents *offsetComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:next_fire_date];
                NSInteger theDay = [offsetComponents day];
                NSInteger theMonth = [offsetComponents month];
                NSInteger theYear = [offsetComponents year];
                NSInteger theHour = [offsetComponents hour];
                NSInteger theMinute = [offsetComponents minute];
                
                
                
                NSInteger tempDay = theDay;
                theMonth = theMonth+1;
                [offsetComponents setMonth:theMonth];
                
                
                
                [test_components setDay:1];
                [test_components setMonth:theMonth];
                [test_components setYear:theYear];
                NSDate *test_Date = [gregorian dateFromComponents:test_components];
                
                
                
                NSRange days = [gregorian rangeOfUnit:NSDayCalendarUnit
                                               inUnit:NSMonthCalendarUnit
                                              forDate:test_Date];
                
                nslog(@"\n days range = %d",days.length);
                if(days.length<tempDay)
                {
                    tempDay = days.length;
                }
                
                [components setDay:tempDay];
                [components setMonth:theMonth];
                [components setYear:theYear];
                [components setHour:theHour];
                [components setMinute:theMinute];
                
                
                next_fire_date = [gregorian dateFromComponents:components];
                
                
                
                
                [appDelegate UpdateReminderNextFireDate:[date_formatter stringFromDate:next_fire_date] rowid:[[[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"rowid"] intValue]];
                
                
                
            }
            
            else if([[[[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"repeat"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"Quarterly"])
            {
                nslog(@"\n Quarterly set new fire date... \n");
                
                
                
                
                
                NSDateComponents *offsetComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit| NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:next_fire_date];
                NSInteger theDay = [offsetComponents day];
                NSInteger theMonth = [offsetComponents month];
                NSInteger theYear = [offsetComponents year];
                NSInteger theHour = [offsetComponents hour];
                NSInteger theMinute = [offsetComponents minute];
                
                
                
                NSInteger tempDay = theDay;
                theMonth = theMonth+3;
                [offsetComponents setMonth:theMonth];
                
                
                
                [test_components setDay:1];
                [test_components setMonth:theMonth];
                [test_components setYear:theYear];
                NSDate *test_Date = [gregorian dateFromComponents:test_components];
                
                
                NSRange days = [gregorian rangeOfUnit:NSDayCalendarUnit
                                               inUnit:NSMonthCalendarUnit
                                              forDate:test_Date];
                
                nslog(@"\n days range = %d",days.length);
                if(days.length<tempDay)
                {
                    tempDay = days.length;
                }
                
                [components setDay:tempDay];
                [components setMonth:theMonth];
                [components setYear:theYear];
                [components setHour:theHour];
                [components setMinute:theMinute];
                
                next_fire_date = [gregorian dateFromComponents:components];
                
                [appDelegate UpdateReminderNextFireDate:[date_formatter stringFromDate:next_fire_date] rowid:[[[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"rowid"] intValue]];
                
                
                
            }
            
            else if([[[[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"repeat"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"Yearly"])
            {
                nslog(@"\n Yearly set new fire date... \n");
                
                
                
                
                NSDateComponents *offsetComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:next_fire_date];
                NSInteger theDay = [offsetComponents day];
                NSInteger theMonth = [offsetComponents month];
                NSInteger theYear = [offsetComponents year];
                NSInteger theHour = [offsetComponents hour];
                NSInteger theMinute = [offsetComponents minute];
                
                theYear++;
                
                
                [components setDay:theDay];
                [components setMonth:theMonth];
                [components setYear:theYear];
                [components setHour:theHour];
                [components setMinute:theMinute];
                
                
                next_fire_date = [gregorian dateFromComponents:components];
                
                
                
                
                [appDelegate UpdateReminderNextFireDate:[date_formatter stringFromDate:next_fire_date] rowid:[[[tempArray_reminder_checklist objectAtIndex:selectedRow] valueForKey:@"rowid"] intValue]];
                
                
                
            }
            
            
                        
            /*
             Sun:004
             This is to make recursive like functionality.So next Fire date will automatically greater than current date.For example if reminder is weekly and user opens after 1 month,then it should display next fire date....(table will reload 4 times...)
             */
            
            
           temp_timeInterval = [current_date timeIntervalSinceDate:next_fire_date];

            
            

        }
        
        
        [appDelegate selectReminder:appDelegate.current_selected_reminder_type];
        [self load_reminder_table];
        temp_button_for_reminder_switch.selected = NO;
    }
    
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}




- (void)dealloc
{
    NSLog(@"\n dealloc in ReminderViewController");
    
    
    
    
    
    [test_components release];
    
    /*Sun:004Causes crash in ios 7 */
    
    
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    nslog(@"\n currSysVer = %f \n",[currSysVer floatValue]);
    if ([currSysVer floatValue]<7.0f)
    {
        [components release];
    }
    
    [gregorian release];
    
    
   
   // [gregorian release];//PUt auto release instead of release..
    [date_formatter release];
    [groupBtnArray release];
    [propertyDictionary release];
    [tblView release];
    
    [super dealloc];
    
}


@end

