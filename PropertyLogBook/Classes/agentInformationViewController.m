//
//  agentInformationViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "agentInformationViewController.h"
#import "addAgentViewController.h"


@implementation agentInformationViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];

    
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        [tblView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
    }

   
    if(appDelegate.isIOS7)
    {
        tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25, tblView.frame.size.width, tblView.frame.size.height+25);
    }
    
           
    
    

    
    
    
    self.navigationItem.title = @"AGENT";
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel_clicked)];
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }

    
    
    
    if (!appDelegate.isFromPropertyAgent)
    {
        
        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
        {
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
            UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator1.width = -12;
            
            [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
            
        }
        else
        {
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
        }

        
        
        tblView.editing = TRUE;
        tblView.allowsSelectionDuringEditing = TRUE;
    }
    
	
    tblView.backgroundColor = [UIColor clearColor];
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,  1024,1024)];
    
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
	
}

-(void)cancel_clicked
{
    
    for (int i=0;i<[appDelegate.agentArray count];i++)
    {
        //-(void)UpdateAgentInfo:(NSMutableArray *)agentType rowid:(int)rowid
        
        NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:[appDelegate.agentArray objectAtIndex:i], nil];
        
        [appDelegate UpdateAgentInfo:array rowid:[[[appDelegate.agentArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        [array release];
		
    }
	[appDelegate selectAgentType];
    
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)add_clicked
{
    
    for (int i=0;i<[appDelegate.agentArray count];i++)
    {
        //-(void)UpdateAgentInfo:(NSMutableArray *)agentType rowid:(int)rowid
        
        NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:[appDelegate.agentArray objectAtIndex:i], nil];
        
        [appDelegate UpdateAgentInfo:array rowid:[[[appDelegate.agentArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        [array release];
		
    }
	[appDelegate selectAgentType];
    
    
    addAgentViewController *addAgent = [[addAgentViewController alloc]initWithNibName:@"addAgentViewController" bundle:nil];
    appDelegate.isAgent = -1;
    [self.navigationController pushViewController:addAgent animated:YES];
    [addAgent release];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    //[self.view addSubview:appDelegate.bottomView];
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    
    [appDelegate selectAgentType];
    
    
    nslog(@"\n agentArray = %@",appDelegate.agentArray);
    
	if([appDelegate.agentArray count]==0)
	{
        
        
        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
        {
            
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
            UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator1.width = -12;
            
            [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
            
        }
        else
        {
            UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
            [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            add_button.bounds = CGRectMake(0, 0,34.0,30.0);
            [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
            [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
            
        }

        
        
		lblNoData.hidden = FALSE;
        imageView.hidden = FALSE;
        lblPlus.hidden = FALSE;
        
        nslog(@"\n self.view.frame.size.height = %f",self.view.frame.size.height);
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                imageView.center = CGPointMake(384,(911.0f/2.0f)-70);
                lblNoData.center = CGPointMake(384,911.0f/2.0f);
                lblPlus.center = CGPointMake(284,467);
            }
            else
            {
                imageView.center = CGPointMake(494,(655.0f/2.0f)-70);
                lblNoData.center = CGPointMake(494,655.0f/2.0f);
                lblPlus.center = CGPointMake(393,340);
            }
            
            
        }
        
        
		[tblView setHidden:TRUE];
		//[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0, 1024,1024)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];
        
		
	}
	else {
        imageView.hidden = TRUE;
		[tblView setHidden:FALSE];
		lblNoData.hidden = TRUE;
        lblPlus.hidden = TRUE;
		[tblView reloadData];
		
	}
    
    if(appDelegate.isIPad)
    {
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
    
    tempRowArray = [[NSMutableArray alloc]initWithArray:appDelegate.agentArray];
	[tblView reloadData];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
    if([appDelegate.agentArray count]==0)
	{
		lblNoData.hidden = FALSE;
        lblPlus.hidden = FALSE;
        imageView.hidden = FALSE;
		
        if(appDelegate.isIPad)
        {
            if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
            {
                imageView.center = CGPointMake(384,(911.0f/2.0f)-70);
                lblNoData.center = CGPointMake(384,911.0f/2.0f);
                lblPlus.center = CGPointMake(284,467);
            }
            else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
            {
                imageView.center = CGPointMake(494,(655.0f/2.0f)-70);
                lblNoData.center = CGPointMake(494,655.0f/2.0f);
                lblPlus.center = CGPointMake(393,340);
            }
            
        }
        
    }
    
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //[tblView reloadData];
    
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
    }

    
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}


#pragma mark - Table view data source
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (!appDelegate.isFromPropertyAgent)
    {
        return @"Note: 1st entry is default entry";
    }
    return @"";
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([appDelegate.agentArray count]>0)
    {
        return [appDelegate.agentArray count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    if(appDelegate.isIOS7)
    {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    // Configure the cell...
    [cell.textLabel setFont:[UIFont systemFontOfSize:17.0]];
    if ([appDelegate.agentArray count]>0)
    {
        cell.textLabel.textColor= [UIColor blackColor];
        cell.textLabel.textAlignment = UITextAlignmentLeft;
        
        cell.textLabel.text = [[appDelegate.agentArray objectAtIndex:indexPath.row] valueForKey:@"0"];
        if (!appDelegate.isFromPropertyAgent)
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
        if (appDelegate.isFromPropertyAgent)
        {
            if ([cell.textLabel.text isEqualToString:appDelegate.agentTypeStr])
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                oldIndex = indexPath;
                [oldIndex retain];
            }
        }
    }
    else
    {
        cell.textLabel.text = @"Create New Property Agent";
        cell.textLabel.textColor = [UIColor grayColor];
        [cell.textLabel setTextAlignment:UITextAlignmentCenter];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([appDelegate.agentArray count]>0)
    {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    for (int i=0;i<[appDelegate.agentArray count];i++)
    {
        //-(void)UpdateAgentInfo:(NSMutableArray *)agentType rowid:(int)rowid
        
        NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:[appDelegate.agentArray objectAtIndex:i], nil];
        
        [appDelegate UpdateAgentInfo:array rowid:[[[appDelegate.agentArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
        [array release];
		
    }
	[appDelegate selectAgentType];
    
    
    
    // Delete the row from the data source
    
    
    
    [appDelegate DeleteAgentInfo:[[[appDelegate.agentArray objectAtIndex:indexPath.row]valueForKey:@"rowid"]intValue]];
    
    [appDelegate.agentArray removeObjectAtIndex:indexPath.row];
    [appDelegate selectAgentType];
	if([appDelegate.agentArray count]==0)
	{
		lblNoData.hidden = FALSE;
        lblPlus.hidden = FALSE;
        imageView.hidden = FALSE;
		
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                imageView.center = CGPointMake(384,(911.0f/2.0f)-70);
                lblNoData.center = CGPointMake(384,911.0f/2.0f);
                lblPlus.center = CGPointMake(284,467);
            }
            else
            {
                imageView.center = CGPointMake(494,(655.0f/2.0f)-70);
                lblNoData.center = CGPointMake(494,655.0f/2.0f);
                lblPlus.center = CGPointMake(393,340);
            }
            
        }
        
        
        
        [tblView setHidden:TRUE];
		//[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0,1024,1024)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];
        
		
	}
	else
    {
        
        imageView.hidden = TRUE;
		[tblView setHidden:FALSE];
		lblNoData.hidden = TRUE;
        lblPlus.hidden = TRUE;
		[tblView reloadData];
		
	}
}



// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [self moveFromOriginal:fromIndexPath.row toNew:toIndexPath.row];
    
}


-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew {
    
    
    NSMutableArray *rowIdArray = [[NSMutableArray alloc] initWithCapacity:10];
    nslog(@"\n before moving = %@",appDelegate.agentArray);
    for (int i=0;i<[appDelegate.agentArray count];i++)
    {
        [rowIdArray addObject:[[appDelegate.agentArray objectAtIndex:i] valueForKey:@"rowid"]];
    }
    nslog(@"\n rowIdArray  = %@",rowIdArray);
    
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:appDelegate.agentArray];
	id tempObject = [tempArray objectAtIndex:indexOriginal];
	[tempArray removeObjectAtIndex:indexOriginal];
	[tempArray insertObject:tempObject atIndex:indexNew];
	appDelegate.agentArray = tempArray;
	
    /*
     for (int i=0;i<[appDelegate.agentArray count];i++)
     {
     //-(void)UpdateAgentInfo:(NSMutableArray *)agentType rowid:(int)rowid
     
     NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:[appDelegate.agentArray objectAtIndex:i], nil];
     
     [appDelegate UpdateAgentInfo:array rowid:[[[tempRowArray objectAtIndex:i]valueForKey:@"rowid"]intValue]];
     [array release];
     
     }
     [appDelegate selectAgentType:[appDelegate getDBPath]];
     
     */
    
    //[tempRowArray release];
    
    nslog(@"\n after moving = %@",appDelegate.agentArray);
	
    for (int i=0;i<[appDelegate.agentArray count];i++)
    {
        [[appDelegate.agentArray objectAtIndex:i] setObject:[rowIdArray objectAtIndex:i] forKey:@"rowid"];
    }
    
    nslog(@"\n after moving  and setting index..= %@",appDelegate.agentArray);
    
    
    [tempArray release];
    [rowIdArray release];
	[tblView reloadData];
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (appDelegate.isFromPropertyAgent)
    {
        if ([appDelegate.agentArray count]>0)
        {
            UITableViewCell *oldCell = [tblView cellForRowAtIndexPath:oldIndex];
            oldCell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        UITableViewCell *newCell = [tblView cellForRowAtIndexPath:indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        appDelegate.agentTypeStr = newCell.textLabel.text;
        appDelegate.agent_pk = [[appDelegate.agentArray objectAtIndex:indexPath.row] objectForKey:@"rowid"];
        oldIndex = indexPath;
        
        /*Memory
         [appDelegate.agentTypeStr retain];
         */
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
    
    else if (!appDelegate.isFromPropertyAgent)
    {
        addAgentViewController *addAgent = [[addAgentViewController alloc]initWithNibName:@"addAgentViewController" bundle:nil];
        appDelegate.isAgent = indexPath.row;
        [self.navigationController pushViewController:addAgent animated:YES];
        [addAgent release];
    }
    
    
    
}

@end
