//
//  RumexCustomTabBar.m
//  
//
//  Created by Oliver Farago on 19/06/2010.
//  Copyright 2010 Rumex IT All rights reserved.
//

#import "CustomTabBar.h"

@implementation CustomTabBar

@synthesize btn1, btn2, btn3, btn4;
@synthesize dashboarad_bottom_button,income_expsne_bottom_button,calculator_bottom_button,reminder_bottom_button,ipad_settings_bottom_button;
- (void)viewDidAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    
   

   if(!appDelegate.is_tabBar_added)
   {
       [self hideTabBar];
       [self addCustomElements];
       appDelegate.is_tabBar_added = TRUE;
   }
    
	
}

- (void)hideTabBar
{
	for(UIView *view in self.view.subviews)
	{
		if([view isKindOfClass:[UITabBar class]])
		{
			view.hidden = YES;
			break;
		}
	}
}

- (void)hideNewTabBar 
{

}

- (void)showNewTabBar 
{

}

-(void)addCustomElements
{
    

    
    
    
    if(appDelegate.isIPad)
    {
        
        
        appDelegate.dashboarad_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [appDelegate.dashboarad_bottom_button setImage:[UIImage imageNamed:@"ipad_dashboard.png"] forState:UIControlStateNormal];
        [appDelegate.dashboarad_bottom_button setImage:[UIImage imageNamed:@"ipad_dashboard_selected.png"] forState:UIControlStateSelected];
        [appDelegate.dashboarad_bottom_button setTag:0];//only settings view...
        [appDelegate.dashboarad_bottom_button setSelected:TRUE];
        [appDelegate.dashboarad_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        appDelegate.income_expsne_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        
        [appDelegate.income_expsne_bottom_button setTag:1];//only settings view...
        [appDelegate.income_expsne_bottom_button setSelected:FALSE];
        [appDelegate.income_expsne_bottom_button setImage:[UIImage imageNamed:@"ipad_income_expense.png"] forState:UIControlStateNormal];
        [appDelegate.income_expsne_bottom_button setImage:[UIImage imageNamed:@"ipad_income_expense_selected.png"] forState:UIControlStateSelected];
        [appDelegate.income_expsne_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        appDelegate.calculator_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [appDelegate.calculator_bottom_button setTag:2];//only settings view...
        [appDelegate.calculator_bottom_button setSelected:FALSE];
        
        
        [appDelegate.calculator_bottom_button setImage:[UIImage imageNamed:@"ipad_calculator.png"] forState:UIControlStateNormal];
        [appDelegate.calculator_bottom_button setImage:[UIImage imageNamed:@"ipad_calculator_selected.png"] forState:UIControlStateSelected];
        
        [appDelegate.calculator_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        
        appDelegate.reminder_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [appDelegate.reminder_bottom_button setTag:3];//only settings view...
        [appDelegate.reminder_bottom_button setSelected:FALSE];
        
        
        [appDelegate.reminder_bottom_button setImage:[UIImage imageNamed:@"ipad_reminder.png"] forState:UIControlStateNormal];
        [appDelegate.reminder_bottom_button setImage:[UIImage imageNamed:@"ipad_reminder_selected.png"] forState:UIControlStateSelected];
        
        [appDelegate.reminder_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];

        
        appDelegate.ipad_settings_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [appDelegate.ipad_settings_bottom_button setImage:[UIImage imageNamed:@"ipad_settings.png"] forState:UIControlStateNormal];
        [appDelegate.ipad_settings_bottom_button setImage:[UIImage imageNamed:@"ipad_settings_selected.png"] forState:UIControlStateSelected];
        [appDelegate.ipad_settings_bottom_button setTag:4];
        
        [appDelegate.ipad_settings_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        
        //self.view.backgroundColor = [UIColor redColor];
        
        [self.view addSubview:appDelegate.imgView_bottom_tabBar]; 
        
        [self.view addSubview:appDelegate.dashboarad_bottom_button];
        [self.view addSubview:appDelegate.income_expsne_bottom_button];
        [self.view addSubview:appDelegate.calculator_bottom_button];
        [self.view addSubview:appDelegate.reminder_bottom_button];
        [self.view addSubview:appDelegate.ipad_settings_bottom_button];
       
        
        [appDelegate manageViewControllerHeight];
        
        
        if (!appDelegate.isFirst)
        {
            
            
            
            
            [appDelegate.dashboarad_bottom_button setSelected:FALSE];
            [appDelegate.income_expsne_bottom_button setSelected:FALSE];
            [appDelegate.calculator_bottom_button setSelected:FALSE];
            [appDelegate.reminder_bottom_button setSelected:FALSE];
            [appDelegate.ipad_settings_bottom_button setSelected:TRUE];

        }
        
        
    }
    else
    {
        
        
        appDelegate.home_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [appDelegate.home_bottom_button setImage:[UIImage imageNamed:@"home_btn_off.png"] forState:UIControlStateNormal];
        [appDelegate.home_bottom_button setImage:[UIImage imageNamed:@"home_btn_on.png"] forState:UIControlStateSelected];
        
        [appDelegate.home_bottom_button setTag:0];
        [appDelegate.home_bottom_button setSelected:TRUE];
        [appDelegate.home_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
        //[self.window addSubview:home_bottom_button];
        
        
        
        
        
        
        /*
         appDelegate.add_income_expense_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
         [appDelegate.add_income_expense_bottom_button setBackgroundImage:[UIImage imageNamed:@"add_btn_off.png"] forState:UIControlStateNormal];
         [appDelegate.add_income_expense_bottom_button setBackgroundImage:[UIImage imageNamed:@"add_btn_on.png"] forState:UIControlStateSelected];
         [appDelegate.add_income_expense_bottom_button setFrame:CGRectMake(107,431,107,49)];
         [appDelegate.add_income_expense_bottom_button setTag:1];
        */
         
        
        
        /*
         [appDelegate.add_income_expense_bottom_button addTarget:appDelegate action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
         */
        
        //[self.bottomView addSubview:add_income_expense_bottom_button];
        
        
        appDelegate.settings_bottom_button=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [appDelegate.settings_bottom_button setImage:[UIImage imageNamed:@"settings_btn_off.png"] forState:UIControlStateNormal];
        [appDelegate.settings_bottom_button setImage:[UIImage imageNamed:@"settings_btn_on.png"] forState:UIControlStateSelected];
        [appDelegate.settings_bottom_button setTag:2];
        [appDelegate.settings_bottom_button addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
       
       
        
        if(appDelegate.isIphone5)
        {
            [appDelegate.home_bottom_button setFrame:CGRectMake(0,519,107,49)];
            [appDelegate.settings_bottom_button setFrame:CGRectMake(214,519,106,49)];
            
        }
        else
        {
            [appDelegate.home_bottom_button setFrame:CGRectMake(0,431,107,49)];
            [appDelegate.settings_bottom_button setFrame:CGRectMake(214,431,106,49)];
        }
        
        [self.view addSubview:appDelegate.add_income_expense_bottom_button];
        
        
        [self.view addSubview:appDelegate.home_bottom_button];
        [self.view addSubview:appDelegate.settings_bottom_button];
        
        [self.view addSubview:appDelegate.imgView_bottom_tabBar];
    }
    /*
     
     // Initialise our two images
     UIImage *btnImage = [UIImage imageNamed:@"NavBar_01.png"];
     UIImage *btnImageSelected = [UIImage imageNamed:@"NavBar_01_s.png"];
     
     self.btn1 = [UIButton buttonWithType:UIButtonTypeCustom]; //Setup the button
     btn1.frame = CGRectMake(0, 430, 80, 50); // Set the frame (size and position) of the button)
     [btn1 setBackgroundImage:btnImage forState:UIControlStateNormal]; // Set the image for the normal state of the button
     [btn1 setBackgroundImage:btnImageSelected forState:UIControlStateSelected]; // Set the image for the selected state of the button
     [btn1 setTag:0]; // Assign the button a "tag" so when our "click" event is called we know which button was pressed.
     [btn1 setSelected:true]; // Set this button as selected (we will select the others to false as we only want Tab 1 to be selected initially
     
     // Now we repeat the process for the other buttons
     btnImage = [UIImage imageNamed:@"NavBar_02.png"];
     btnImageSelected = [UIImage imageNamed:@"NavBar_02_s.png"];
     self.btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
     btn2.frame = CGRectMake(80, 430, 80, 50);
     [btn2 setBackgroundImage:btnImage forState:UIControlStateNormal];
     [btn2 setBackgroundImage:btnImageSelected forState:UIControlStateSelected];
     [btn2 setTag:1];
     
     btnImage = [UIImage imageNamed:@"NavBar_03.png"];
     btnImageSelected = [UIImage imageNamed:@"NavBar_03_s.png"];
     self.btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
     btn3.frame = CGRectMake(160, 430, 80, 50);
     [btn3 setBackgroundImage:btnImage forState:UIControlStateNormal];
     [btn3 setBackgroundImage:btnImageSelected forState:UIControlStateSelected];
     [btn3 setTag:2];
     
     btnImage = [UIImage imageNamed:@"NavBar_04.png"];
     btnImageSelected = [UIImage imageNamed:@"NavBar_04_s.png"];
     self.btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
     btn4.frame = CGRectMake(240, 430, 80, 50);
     [btn4 setBackgroundImage:btnImage forState:UIControlStateNormal];
     [btn4 setBackgroundImage:btnImageSelected forState:UIControlStateSelected];
     [btn4 setTag:3];
     
     // Add my new buttons to the view
     [self.view addSubview:btn1];
     [self.view addSubview:btn2];
     [self.view addSubview:btn3];
     [self.view addSubview:btn4];
     
     // Setup event handlers so that the buttonClicked method will respond to the touch up inside event.
     [btn1 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
     [btn2 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
     [btn3 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
     [btn4 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
     
     */
    
	
}



#pragma mark - buttonTabBarPressed
#pragma mark  handling tabbarview click events..
-(void)buttonTabBarPressed:(id)sender
{
    
    
    
    if(appDelegate.isTakingBackupFromiCloud)
    {
        return;
    }
 
    
    
    
    /*CP : need to manage this, as in ios 7 app was crashing on set selected index...
    
    NSMutableArray *allControllersArray = [[appDelegate.tabBarController viewControllers] mutableCopy];
    appDelegate.tabBarController.viewControllers = allControllersArray;

    
    */
    
    
    UIButton*temp_button = (UIButton*)sender;
    
    
    
    NSArray *arr = [appDelegate.tabBarController viewControllers];
    for(UINavigationController *view1 in arr)
    {
        nslog(@"\n view1 retian count = %d",[view1 retainCount]);
        //nslog(@"\n class %@",NSStringFromClass([view1 class]));
        [view1 popToRootViewControllerAnimated:NO];
    }
    
    
    if(appDelegate.isIPad)
    {
        
        
        [appDelegate.dashboarad_bottom_button setSelected:FALSE];
        [appDelegate.income_expsne_bottom_button setSelected:FALSE];
        [appDelegate.calculator_bottom_button setSelected:FALSE];
        [appDelegate.reminder_bottom_button setSelected:FALSE];
        [appDelegate.ipad_settings_bottom_button setSelected:FALSE];
        
        
        
    }
    else
    {
        [appDelegate.home_bottom_button setSelected:FALSE];
        [appDelegate.add_income_expense_bottom_button setSelected:FALSE];
        [appDelegate.settings_bottom_button setSelected:FALSE];
        
    }
    
    
    nslog(@"\n temp_button.tag = %d",temp_button.tag);
    
    
    [self selectTab:temp_button.tag];
    [sender setSelected:YES];
}


/*
- (void)buttonClicked:(id)sender
{
	
    if(appDelegate.isIPad)
    {
        
        
        [dashboarad_bottom_button setSelected:FALSE];
        [income_expsne_bottom_button setSelected:FALSE];
        [calculator_bottom_button setSelected:FALSE];
        [reminder_bottom_button setSelected:FALSE];
        [ipad_settings_bottom_button setSelected:FALSE];
        
        
        
    }

    [sender setSelected:YES];
    int tagNum = [sender tag];
	[self selectTab:tagNum];
}
*/

- (void)selectTab:(int)tabID
{
    
    @try
    {
        nslog(@"\n  self.selectedIndex = %lu", (unsigned long)self.selectedIndex);
        nslog(@"\n  tabID = %d", tabID);
        /*CP: Crashing here in ios:7*/
        self.selectedIndex = (NSUInteger)tabID;
    }
    @catch (NSException *exception)
    {
        nslog(@"\n exception = %@",exception);
    }
    @finally
    {
        
    }
    
    
    
	
}





- (void)dealloc
{

    [super dealloc];
}

@end
