
#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"
@interface CustomTabBar : UITabBarController<UITabBarControllerDelegate>
{
	UIButton *btn1;
	UIButton *btn2;
	UIButton *btn3;
	UIButton *btn4;
    
    
    PropertyLogBookAppDelegate*appDelegate;
}

@property (nonatomic, retain) UIButton *btn1;
@property (nonatomic, retain) UIButton *btn2;
@property (nonatomic, retain) UIButton *btn3;
@property (nonatomic, retain) UIButton *btn4;


/*ipad bottom buttons*/

@property (nonatomic,retain) UIButton *dashboarad_bottom_button;
@property (nonatomic,retain) UIButton *income_expsne_bottom_button;
@property (nonatomic,retain) UIButton *calculator_bottom_button;
@property (nonatomic,retain) UIButton *reminder_bottom_button;
@property (nonatomic,retain) UIButton *ipad_settings_bottom_button;


/*ipad bottom buttons*/


-(void) hideTabBar;
-(void) addCustomElements;
-(void) selectTab:(int)tabID;

/*
-(void) hideNewTabBar;
-(void) ShowNewTabBar;
*/

@end
