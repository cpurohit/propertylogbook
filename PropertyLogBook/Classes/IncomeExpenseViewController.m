//
//  IncomeExpenseViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "IncomeExpenseViewController.h"
#import "AddIncomeExpenseViewController.h"
#import "HomeViewController.h"
#import "SettingsViewController.h"


@implementation IncomeExpenseViewController



#pragma mark -
#pragma mark View lifecycle


- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}


- (void)viewDidLoad 
{
    nslog(@"\n viewDidLoad in incomeExpenseViewcontroller...");

    
     /* CP :  */
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }
        
    }

    
    
    
//	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_clicked)];
    
    
    appDelegate.prefs = [NSUserDefaults standardUserDefaults];
    /*
    incomeExpenseFilterSegmentControl.segmentedControlStyle = UISegmentedControlStyleBezeled;
    
    segment.segmentedControlStyle = UISegmentedControlStyleBezeled;
    */
   
    
    
    
    
/*
    [[UISegmentedControl appearance] setDividerImage:appDelegate.divider forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
     
     [[UISegmentedControl appearance] setDividerImage:appDelegate.divider forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
     
     
     
     [[UISegmentedControl appearance] setDividerImage:appDelegate.divider forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
  */
    
    
    /*
     Start:
     Sun:0004
     Setting custom image in navigation bar..
     */
    
    
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0)
    {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
        /*
         [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor],UITextAttributeTextColor,[UIColor blackColor],UITextAttributeTextShadowColor,[NSValue valueWithUIOffset:UIOffsetMake(1, 0)], UITextAttributeTextShadowOffset ,[UIFont fontWithName:@"System" size:25.0],UITextAttributeFont  , nil]];
         */
        
    }
    
    /*
     Ends:
     Sun:0004
     Setting custom image in navigation bar..
     */

    
    numberFormatter = [[NSNumberFormatter alloc] init] ;
    
    cancelButton.enabled = FALSE;
    tempSearch = [[NSMutableArray alloc]init];
    
    
    
    
    
    if(!appDelegate.isIPad)
    {
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
            
            UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
            UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            negativeSeperator.width = -12;
            
            [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
            
            
        }
        else
        {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
            button.bounds = CGRectMake(0, 0,49.0,29.0);
            [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
            self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            
            
        }
        
    }

    selectedIndexpahRow = 0;

    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    //self.navigationItem.title = @"INCOME/EXPENSE";
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        add_button.bounds = CGRectMake(0, 0,34.0,30.0);
        [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
        [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
        
    }

    
    /*
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_clicked)] autorelease];
	
     */
     
	tblView.backgroundColor = [UIColor clearColor];
	[tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0, 1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    [imgView release];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];

    
    
    
       

    
    incomeArray = [[NSMutableArray alloc]init];
    expenseArray = [[NSMutableArray alloc]init];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],UITextAttributeTextColor,[NSValue valueWithUIOffset:UIOffsetMake(0, 1)],UITextAttributeTextShadowOffset,nil] forState:UIControlStateNormal];
    
    [super viewDidLoad];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    isViewWillAppearAfterDeleting = FALSE;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardDidHideNotification 
                                                  object:nil]; 
	[[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardDidShowNotification
												  object:nil];
    
    
    
    
    
    
    nslog(@"\n alltarget = %@ \n",[appDelegate.add_income_expense_bottom_button allTargets]);
    
    //[appDelegate.bottomView removeFromSuperview];
    
    NSSet*target_set = [appDelegate.add_income_expense_bottom_button allTargets];
    
    nslog(@"\n allObjects = %@ \n",[target_set allObjects]);
    
    if(![[[target_set allObjects] objectAtIndex:0] isKindOfClass:[HomeViewController class]])
    {
        
        [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
        [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                            action:NULL
                                                  forControlEvents:UIControlEventAllEvents];
    }
    
   
    
}


- (void)viewWillAppear:(BOOL)animated 
{
    
    nslog(@"\n viewWillAppear in IncomeExpenseViewController.");
    
    [super viewWillAppear:animated];
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    //[self.view addSubview:appDelegate.bottomView];
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    

    
    
    selectedSpan = 0;//by default mtd
    
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
    
        [appDelegate selectCurrencyIndex];
    }
    [tblView setEditing:FALSE];
    [super setEditing:FALSE];
    
    
    nslog(@"\n incomeExpenseFilterSegmentControl = %d",incomeExpenseFilterSegmentControl.selectedSegmentIndex);

   /*
        Sun:0004
        tableView condition for iphone-5
    */
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }

    
    
    
    
    
    
    if(!isViewWillAppearAfterDeleting)
    {
        
        @try
        {
            
            
            
            
            [mtdButton setImage:[UIImage imageNamed:@"mtd_selected.png"] forState:UIControlStateNormal];
            [mtdButton setImage:[UIImage imageNamed:@"mtd_selected.png"] forState:UIControlStateHighlighted];
            
            
            [ytdButton setImage:[UIImage imageNamed:@"ytd.png"] forState:UIControlStateNormal];
            [ytdButton setImage:[UIImage imageNamed:@"ytd.png"] forState:UIControlStateHighlighted];
            
            
            [allButton setImage:[UIImage imageNamed:@"all.png"] forState:UIControlStateNormal];
            [allButton setImage:[UIImage imageNamed:@"all.png"] forState:UIControlStateHighlighted];
            
        }
        @catch (NSException *exception)
        {
            
            nslog(@"\n exception = %@",[exception description]);
            
        }
        @finally
        {
            
        }
        
        
        
        
        
        if(appDelegate.isIPad)
        {
            [incomeButton setImage:[UIImage imageNamed:@"income_ipad.png"] forState:UIControlStateNormal];
            [incomeButton setImage:[UIImage imageNamed:@"income_ipad.png"] forState:UIControlStateHighlighted];
            
            
            [expenseButton setImage:[UIImage imageNamed:@"expense_ipad.png"] forState:UIControlStateNormal];
            [expenseButton setImage:[UIImage imageNamed:@"expense_ipad.png"] forState:UIControlStateHighlighted];
            
            
            [bothButton setImage:[UIImage imageNamed:@"both_ipad_selected.png"] forState:UIControlStateNormal];
            [bothButton setImage:[UIImage imageNamed:@"both_ipad_selected.png"] forState:UIControlStateHighlighted];
            
        }
        else
        {
            
            
            [incomeButton setImage:[UIImage imageNamed:@"income.png"] forState:UIControlStateNormal];
            [incomeButton setImage:[UIImage imageNamed:@"income.png"] forState:UIControlStateHighlighted];
            
            
            [expenseButton setImage:[UIImage imageNamed:@"expense.png"] forState:UIControlStateNormal];
            [expenseButton setImage:[UIImage imageNamed:@"expense.png"] forState:UIControlStateHighlighted];
            
            
            [bothButton setImage:[UIImage imageNamed:@"both_selected.png"] forState:UIControlStateNormal];
            [bothButton setImage:[UIImage imageNamed:@"both_selected.png"] forState:UIControlStateHighlighted];
        }
        
        
        
        
        
        incomeExpenseFilterSegmentControl.selectedSegmentIndex = 0;
        switch (appDelegate.date_filter_type)
        {
            case 0:
            {
                //MTD
                
                /*
                    No need to do any coding,as this is defualt behaviour.
                 */
                
                incomeExpenseFilterSegmentControl.selectedSegmentIndex = 0;
                
                break;
            }
            case 1:
            {
                //YTD
                
                [mtdButton setImage:[UIImage imageNamed:@"mtd.png"] forState:UIControlStateNormal];
                [mtdButton setImage:[UIImage imageNamed:@"mtd.png"] forState:UIControlStateHighlighted];
                
                
                [ytdButton setImage:[UIImage imageNamed:@"ytd_selected.png"] forState:UIControlStateNormal];
                [ytdButton setImage:[UIImage imageNamed:@"ytd_selected.png"] forState:UIControlStateHighlighted];
                
                
                [allButton setImage:[UIImage imageNamed:@"all.png"] forState:UIControlStateNormal];
                [allButton setImage:[UIImage imageNamed:@"all.png"] forState:UIControlStateHighlighted];

                
                incomeExpenseFilterSegmentControl.selectedSegmentIndex = 1;
                break;
            }
            case 2:
            {
                //Date Range
                /*
                 Need to ask from client...
                 */
                incomeExpenseFilterSegmentControl.selectedSegmentIndex = 0;
                
                break;
            }
                
            default:
                break;
        }

        
        segment.selectedSegmentIndex = 2;
        
        incomeExpenseFilterSegmentControl.frame = CGRectMake(5, 0,189,27);
        
        if(appDelegate.isIPad)
        {
            incomeExpenseFilterSegmentControl.center = CGPointMake(384,22);
        }
        
        
       // self.navigationItem.titleView = incomeExpenseFilterSegmentControl;
        self.navigationItem.titleView = incomeExpenseTransctionSortRangeView;
        
       
    }
    
    if(incomeExpenseFilterSegmentControl.selectedSegmentIndex == 0)
    {
        [appDelegate selectIncomeExpenseByMDTOrYDT:@"MTD"];
    }
    else if(incomeExpenseFilterSegmentControl.selectedSegmentIndex == 1)
    {
        [appDelegate selectIncomeExpenseByMDTOrYDT:@"YTD"];
    }
    else if(incomeExpenseFilterSegmentControl.selectedSegmentIndex == 2)
    {
        [appDelegate selectIncomeExpense];
    }
    
    
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
	[appDelegate.incomeExpenseArray sortUsingDescriptors:[NSArray arrayWithObject:dateDescriptor]];
    
    nslog(@"\n income expense array(crashing here after op..)===%@",appDelegate.incomeExpenseArray);
     nslog(@"\n count ===%d",[appDelegate.incomeExpenseArray count]);
    [dateDescriptor release];
    
    //When we come from another view..
    [searchBar resignFirstResponder];
    searchBar.text = NO;
    searching = NO;
    [self.view endEditing:YES];

    
    /*do not delete
    if(searching == YES)
    {
        [tempSearch removeAllObjects];
        [self searchCat];
    }
     */
    
    [incomeArray removeAllObjects];
    [expenseArray removeAllObjects];
    
    for (int i = 0; i<[appDelegate.incomeExpenseArray count];i++)
    {
        if ([[[appDelegate.incomeExpenseArray objectAtIndex:i]valueForKey:@"incomeExpense"]intValue]==1)
        {
            [incomeArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
        }
        else/*
                SUN:004- EXPENSE ARRAY HAVE EXPENSE + MILEAGE
             */
        {
            [expenseArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
        }
    }
    
    //nslog(@"income expense array == %@",appDelegate.incomeExpenseArray);
    if([appDelegate.incomeExpenseArray count]==0)
	{
        incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = FALSE;
		lblNoData.hidden = FALSE;
		[tblView setHidden:TRUE];
		UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];
        
		
	}
	else 
    {
		[tblView setHidden:FALSE];
        incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = TRUE;
		lblNoData.hidden = TRUE;
		[tblView reloadData];
		
	}
    
    if(appDelegate.isIPad)
    {
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:1.0];
    }
    
    
    
    
    
   
    
    
    
    
        
    [self.view bringSubviewToFront:searchBar];
    
    
    
    //These are requred as user can come back after switching app,(when there is password)
    
    [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];

    
        
    
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

-(void)keyboardDidShow:(NSNotification*) notification
{
    
    nslog(@"\n keyboardDidShow = %f",tblView.frame.size.height);
     nslog(@"\n keyboardDidShow = %f",tblView.frame.size.width);

    nslog(@"\n keyboardDidShow x = %f",tblView.frame.origin.x);
    nslog(@"\n keyboardDidShow y = %f",tblView.frame.origin.y);
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,196.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,110.0f);
        }
        
        
    }
    

}

-(void)keyboardDidHide:(NSNotification*) notification
{
    nslog(@"\n keyboardDidHide");
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width, 364.0f);
        }
        else
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width, 276.0f);
        }
            
        
        
    }
    
    
}




-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if(appDelegate.isIPad)
    {
        
        if(toInterfaceOrientation == 1 ||toInterfaceOrientation == 2 )
        {
            self.view.frame  = CGRectMake(0, 0,768,1024);
            
            segment.frame = CGRectMake(0, 0,768,44);
            
            incomeButton.frame = CGRectMake(0,0,256,44);
            expenseButton.frame = CGRectMake(256,0,256,44);
            bothButton.frame = CGRectMake(512,0,256,44);

            
            
            
            barImageIpad.frame = CGRectMake(0,0,768,55);
           
            searchBar.frame = CGRectMake(0,44,768,44);
            cancelButton.frame = CGRectMake(697,50,66,30);
            cancelButton.hidden = TRUE;
            incomeExpenseTypeVIew.frame = CGRectMake(0,0,768,44);
            
            
            incomeImage.frame = CGRectMake(255,347, 60,60);
            lblNoData.frame = CGRectMake(224,415, 320,54);
            plusLabel.frame = CGRectMake(278,442, 13, 21);
            expenseImage.frame = CGRectMake(479,347, 60,60);

            

           
            
            
            //incomeExpenseTypeVIew.center = CGPointMake(384,28);
            
            
            
        }
        else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
        {
            self.view.frame  = CGRectMake(0, 0,1024,768);
           
            segment.frame = CGRectMake(0, 0,1024,44);
           
            barImageIpad.frame = CGRectMake(0,0,1024,55);
            
            cancelButton.hidden = TRUE;

            
            searchBar.frame = CGRectMake(0,44,1024,44);
            incomeExpenseTypeVIew.frame = CGRectMake(0,0,1024,44);
            cancelButton.frame = CGRectMake(965,50,66,30);

            
            incomeButton.frame = CGRectMake(128,0,256,44);
            expenseButton.frame = CGRectMake(384,0,256,44);
            bothButton.frame = CGRectMake(640,0,256,44);
            
            
            
            incomeImage.center = CGPointMake(self.view.frame.size.width/2-85,(self.view.frame.size.height/2)-60);
            
            lblNoData.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
            plusLabel.center = CGPointMake(410,393);
            expenseImage.center = CGPointMake(self.view.frame.size.width/2+85,(self.view.frame.size.height/2)-60);
            lblNoData.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
            incomeExpenseFilterSegmentControl.center = CGPointMake(512,22);
            
            
            
            
        }
    }
    
    [self.view bringSubviewToFront:blackBar];
    [self.view bringSubviewToFront:searchBar];
    

    [self.view bringSubviewToFront:cancelButton];
     
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
   
    
    
    
    if(appDelegate.isIPad)
    {

               
        //[tblView reloadData];
        
        UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
        [imgView setFrame:CGRectMake(0, 0, 1024,1024)];
        //  [imgView setContentMode:UIViewContentModeScaleToFill];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
        
        [imgView release];
        
        [tblView reloadData];
        
        [appDelegate manageViewControllerHeight];
        
    }
    
    
    
    
}


#pragma mark -

//- (void)setEditing:(BOOL)editing animated:(BOOL)animated{
//    [super setEditing:editing animated:animated];
//    [tblView setEditing:editing animated:animated];
//    if (editing)
//    {
//        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_clicked)];
//    }
//    else
//    {
//                self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel_clicked)];
//    }
//}


-(void)add_clicked
{
    
    UIActionSheet *actionSheet;
    appDelegate.isIncomeExpenseUpdate = FALSE;
    appDelegate.prefs = [NSUserDefaults standardUserDefaults];
    
    
    /*
    appDelegate.is_milege_on = [appDelegate.prefs boolForKey:@"is_milage_set"];
    */
    
    appDelegate.is_milege_on = [[appDelegate.prefs objectForKey:@"is_milage_set"] boolValue];
    
    nslog(@"\n appDelegate.is_milege_on = %d \n ",appDelegate.is_milege_on);
    
    
    
    
    if(appDelegate.is_milege_on)
    {
        
        actionSheet = [[UIActionSheet alloc]initWithTitle:@"Select Income/Expense/Mileage" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Expense" otherButtonTitles:@"Income",@"Mileage", nil];
        
        
    }
    else
    {
        actionSheet = [[UIActionSheet alloc]initWithTitle:@"Select Income/Expense" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Expense" otherButtonTitles:@"Income", nil];
    }
    

    
/*
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Select Income/Expense" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Expense" otherButtonTitles:@"Income", nil];
  */  
        actionSheet.tag = 999;
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        [actionSheet release];
	}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == 999)
    {
        
        
        if(buttonIndex==2 && !appDelegate.is_milege_on)
        {
            return;
        }

        
        NSString *nibName = @"";
        
        nibName = @"AddIncomeExpenseViewController";
        
        
        /*
            SUN:0004
            Need to reset this variable...
            
         */
        
        appDelegate.is_receipt_image_choosen = FALSE;
        appDelegate.is_receipt_image_updated=FALSE;
        
        appDelegate.isMilege = FALSE;
        if (buttonIndex == 1)
        {
            appDelegate.isIncome = TRUE;
            addIncome = [[AddIncomeExpenseViewController alloc]initWithNibName:nibName bundle:nil];
            [self.navigationController pushViewController:addIncome animated:YES];
            [addIncome release];
            
            
        }
        else if (buttonIndex == 0)
        {
            appDelegate.isIncome = FALSE;
            addIncome = [[AddIncomeExpenseViewController alloc]initWithNibName:nibName bundle:nil];
            [self.navigationController pushViewController:addIncome animated:YES];
            [addIncome release];
        }
        else if(buttonIndex==2)
        {
            appDelegate.isIncome = FALSE;
            appDelegate.isMilege = TRUE;
            addIncome = [[AddIncomeExpenseViewController alloc]initWithNibName:nibName bundle:nil];
            [self.navigationController pushViewController:addIncome animated:YES];
            [addIncome release];
        }

    }
    else if(actionSheet.tag == 998)
    {
        nslog(@"\n buttonIndex = %d",buttonIndex);
        
        if(buttonIndex!=3)
        {
            int deleteType = buttonIndex;
            if (!searching)
            {
                               
                 
                if (segment.selectedSegmentIndex == 2)
                {
                    nslog(@"\n incomeExpenseArray remove = %@",[appDelegate.incomeExpenseArray objectAtIndex:selectedIndexpahRow]);
                    
                    
                    [appDelegate DeleteIncomeExpense:[[[appDelegate.incomeExpenseArray objectAtIndex:selectedIndexpahRow]valueForKey:@"rowid"]intValue] 
                    parentId:[[[appDelegate.incomeExpenseArray objectAtIndex:selectedIndexpahRow]valueForKey:@"parentid"]intValue]
                    deleteType:deleteType 
                    dateString:[NSString stringWithFormat:@"%@",[[appDelegate.incomeExpenseArray objectAtIndex:selectedIndexpahRow]valueForKey:@"date"]]
                     ];
                    [appDelegate.incomeExpenseArray removeObjectAtIndex:selectedIndexpahRow];
                    
                }
                else if (segment.selectedSegmentIndex == 1)
                {
                    
                    nslog(@"\n expenseArray = %@",[expenseArray objectAtIndex:selectedIndexpahRow]);
                    
                    
                    [appDelegate DeleteIncomeExpense:[[[expenseArray objectAtIndex:selectedIndexpahRow]valueForKey:@"rowid"]intValue] 
                                          parentId:[[[appDelegate.incomeExpenseArray objectAtIndex:selectedIndexpahRow]valueForKey:@"parentid"]intValue]
                                          deleteType:deleteType 
                                          dateString:[NSString stringWithFormat:@"%@",[[appDelegate.incomeExpenseArray objectAtIndex:selectedIndexpahRow]valueForKey:@"date"]]

                     
                     
                     
                     ];
                    [expenseArray removeObjectAtIndex:selectedIndexpahRow];
                    
                }
                else if (segment.selectedSegmentIndex == 0)
                {
                     nslog(@"\n incomeArray = %@",[incomeArray objectAtIndex:selectedIndexpahRow]);
                    
                    [appDelegate DeleteIncomeExpense:[[[incomeArray objectAtIndex:selectedIndexpahRow]valueForKey:@"rowid"]intValue] 
                                          parentId:[[[appDelegate.incomeExpenseArray objectAtIndex:selectedIndexpahRow]valueForKey:@"parentid"]intValue]
                                          deleteType:deleteType dateString:[NSString stringWithFormat:@"%@",[[appDelegate.incomeExpenseArray objectAtIndex:selectedIndexpahRow]valueForKey:@"date"]]
                     
                     ];
                    [incomeArray removeObjectAtIndex:selectedIndexpahRow];
                }
            }
            else
            {
                
                nslog(@"\n tempSearch ...%@",tempSearch);
                
                [appDelegate DeleteIncomeExpense:[[[tempSearch objectAtIndex:selectedIndexpahRow]valueForKey:@"rowid"]intValue] 
                                      parentId:[[[tempSearch objectAtIndex:selectedIndexpahRow]valueForKey:@"parentid"]intValue]
                                      deleteType:deleteType 
                                      dateString:[NSString stringWithFormat:@"%@",[[tempSearch objectAtIndex:selectedIndexpahRow]valueForKey:@"date"]]
                 ]; 
               
                [tempSearch removeObjectAtIndex:selectedIndexpahRow];
            }
            if([appDelegate.incomeExpenseArray count]==0)
            {
                incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = FALSE;
                
                lblNoData.hidden = FALSE;
                [tblView setHidden:TRUE];
                [self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
                
            }
            else 
            {
                [tblView setHidden:FALSE];
                incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = TRUE;
                lblNoData.hidden = TRUE;
                [tblView reloadData];
                
            }
            isViewWillAppearAfterDeleting = TRUE;
            [self viewWillAppear:YES];
        }
        

    }
}


#pragma mark -  cancel_clicked
#pragma mark Manages popViewControllerAnimated and stack in navigationController array

-(void)cancel_clicked
{
	
    [appDelegate.home_bottom_button setSelected:TRUE];
    [appDelegate.add_income_expense_bottom_button setSelected:NO];
    [appDelegate.settings_bottom_button setSelected:NO];

    NSMutableArray*viewControllerArray = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]] ;
    NSMutableArray*temp_viewControllerArray = [[[NSMutableArray alloc] init] autorelease];
    
    nslog(@"\n viewControllerArray before= %@",viewControllerArray);
    
    
    
    
    for(int i=0;i<[viewControllerArray count]-1;i++)
    {
        //[viewControllerArray removeObjectAtIndex:i];
        
        if( ([[viewControllerArray objectAtIndex:i] isKindOfClass:[HomeViewController class]] || [[viewControllerArray objectAtIndex:i] isKindOfClass:[self class]] ||  [[viewControllerArray objectAtIndex:i] isKindOfClass:[SettingsViewController class]]  )  )
        {
           // [viewControllerArray removeObjectAtIndex:i];
            [temp_viewControllerArray addObject:[viewControllerArray objectAtIndex:i]];
        }
        
    }
    
    nslog(@"\n viewControllerArray = %@",temp_viewControllerArray);
    self.navigationController.viewControllers =  [NSArray arrayWithArray:temp_viewControllerArray];
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
}




/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/

#pragma mark -
#pragma mark incomeExpenseFilterSegmentControl_Changed method

-(IBAction)incomeExpenseFilterSegmentControl_Changed_Through_Click:(id)sender
{
    UIButton*tempButton = (UIButton*)sender;
    int tag = (tempButton.tag - 6000);
    incomeExpenseFilterSegmentControl.selectedSegmentIndex = tag;
    if(tag == 0)
    {
        
        
        [mtdButton setImage:[UIImage imageNamed:@"mtd_selected.png"] forState:UIControlStateNormal];
        [mtdButton setImage:[UIImage imageNamed:@"mtd_selected.png"] forState:UIControlStateHighlighted];

        
        [ytdButton setImage:[UIImage imageNamed:@"ytd.png"] forState:UIControlStateNormal];
        [ytdButton setImage:[UIImage imageNamed:@"ytd.png"] forState:UIControlStateHighlighted];

        
        [allButton setImage:[UIImage imageNamed:@"all.png"] forState:UIControlStateNormal];
        [allButton setImage:[UIImage imageNamed:@"all.png"] forState:UIControlStateHighlighted];

        
        
        
        
        
        [appDelegate selectIncomeExpenseByMDTOrYDT:@"MTD"];
        
        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        [appDelegate.incomeExpenseArray sortUsingDescriptors:[NSArray arrayWithObject:dateDescriptor]];
        
        
        nslog(@"income expense array in incomeExpenseFilterSegmentControl_Changed ===%@",appDelegate.incomeExpenseArray);
        [incomeArray removeAllObjects];
        [expenseArray removeAllObjects];
        
        
        [dateDescriptor release];
        for (int i = 0; i<[appDelegate.incomeExpenseArray count];i++)
        {
            if ([[[appDelegate.incomeExpenseArray objectAtIndex:i]valueForKey:@"incomeExpense"]intValue]==1)
            {
                [incomeArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
            else
            {
                [expenseArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
        }
        
        //nslog(@"income expense array == %@",appDelegate.incomeExpenseArray);
        if([appDelegate.incomeExpenseArray count]==0)
        {
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = FALSE;
            lblNoData.hidden = FALSE;
            [tblView setHidden:TRUE];
            //[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
            [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            //  [imgView setContentMode:UIViewContentModeScaleToFill];
            [self.view addSubview:imgView];
            [self.view sendSubviewToBack:imgView];
            
            [imgView release];
            
            
        }
        else
        {
            
            
            
            if (searching)
            {
                [self searchCat];
            }
            
            [tblView setHidden:FALSE];
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = TRUE;
            lblNoData.hidden = TRUE;
            [tblView reloadData];
            
        }
        
        
        
        
    }
    else if(tag == 1)
    {
        
        [mtdButton setImage:[UIImage imageNamed:@"mtd.png"] forState:UIControlStateNormal];
        [mtdButton setImage:[UIImage imageNamed:@"mtd.png"] forState:UIControlStateHighlighted];
        
        
        [ytdButton setImage:[UIImage imageNamed:@"ytd_selected.png"] forState:UIControlStateNormal];
        [ytdButton setImage:[UIImage imageNamed:@"ytd_selected.png"] forState:UIControlStateHighlighted];
        
        
        [allButton setImage:[UIImage imageNamed:@"all.png"] forState:UIControlStateNormal];
        [allButton setImage:[UIImage imageNamed:@"all.png"] forState:UIControlStateHighlighted];
        
        
        
        [appDelegate selectIncomeExpenseByMDTOrYDT:@"YTD"];
        
        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        [appDelegate.incomeExpenseArray sortUsingDescriptors:[NSArray arrayWithObject:dateDescriptor]];
        
        
        nslog(@"income expense array = 1===%@",appDelegate.incomeExpenseArray);
        [incomeArray removeAllObjects];
        [expenseArray removeAllObjects];
        
        
        [dateDescriptor release];
        for (int i = 0; i<[appDelegate.incomeExpenseArray count];i++)
        {
            if ([[[appDelegate.incomeExpenseArray objectAtIndex:i]valueForKey:@"incomeExpense"]intValue]==1)
            {
                [incomeArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
            else
            {
                [expenseArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
        }
        
        //nslog(@"income expense array == %@",appDelegate.incomeExpenseArray);
        if([appDelegate.incomeExpenseArray count]==0)
        {
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = FALSE;
            lblNoData.hidden = FALSE;
            [tblView setHidden:TRUE];
            //[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
            [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            //  [imgView setContentMode:UIViewContentModeScaleToFill];
            [self.view addSubview:imgView];
            [self.view sendSubviewToBack:imgView];
            
            [imgView release];
            
            
        }
        else
        {
            
            if (searching)
            {
                [self searchCat];
            }

            
            [tblView setHidden:FALSE];
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = TRUE;
            lblNoData.hidden = TRUE;
            [tblView reloadData];
            
        }
        
        
        
    }
    else if(tag == 2)
    {
        
        [mtdButton setImage:[UIImage imageNamed:@"mtd.png"] forState:UIControlStateNormal];
        [mtdButton setImage:[UIImage imageNamed:@"mtd.png"] forState:UIControlStateHighlighted];
        
        
        [ytdButton setImage:[UIImage imageNamed:@"ytd.png"] forState:UIControlStateNormal];
        [ytdButton setImage:[UIImage imageNamed:@"ytd.png"] forState:UIControlStateHighlighted];
        
        
        [allButton setImage:[UIImage imageNamed:@"all_selected.png"] forState:UIControlStateNormal];
        [allButton setImage:[UIImage imageNamed:@"all_selected.png"] forState:UIControlStateHighlighted];
        
        [appDelegate selectIncomeExpense];
        
        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        [appDelegate.incomeExpenseArray sortUsingDescriptors:[NSArray arrayWithObject:dateDescriptor]];
        
        
        nslog(@"income expense array in tempSegementedControl 2===%@",appDelegate.incomeExpenseArray);
        [incomeArray removeAllObjects];
        [expenseArray removeAllObjects];
        
        
        [dateDescriptor release];
        for (int i = 0; i<[appDelegate.incomeExpenseArray count];i++)
        {
            if ([[[appDelegate.incomeExpenseArray objectAtIndex:i]valueForKey:@"incomeExpense"]intValue]==1)
            {
                [incomeArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
            else
            {
                [expenseArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
        }
        
        //nslog(@"income expense array == %@",appDelegate.incomeExpenseArray);
        if([appDelegate.incomeExpenseArray count]==0)
        {
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = FALSE;
            lblNoData.hidden = FALSE;
            [tblView setHidden:TRUE];
            //[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
            [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            //  [imgView setContentMode:UIViewContentModeScaleToFill];
            [self.view addSubview:imgView];
            [self.view sendSubviewToBack:imgView];
            
            [imgView release];
            
            
        }
        else
        {
            if (searching)
            {
                [self searchCat];
            }

            
            
            [tblView setHidden:FALSE];
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = TRUE;
            lblNoData.hidden = TRUE;
            [tblView reloadData];
            
        }
        
        
    }
}

-(IBAction)incomeExpenseFilterSegmentControl_Changed:(id)sender
{
    UISegmentedControl *tempSegementedControl = (UISegmentedControl*)sender;
    if(tempSegementedControl.selectedSegmentIndex == 0)
    {
        
        
        [incomeExpenseFilterSegmentControl setImage:[UIImage imageNamed:@"mtd_selected.png"] forSegmentAtIndex:0];
        [incomeExpenseFilterSegmentControl setImage:[UIImage imageNamed:@"ytd.png"] forSegmentAtIndex:1];
        [incomeExpenseFilterSegmentControl setImage:[UIImage imageNamed:@"all.png"] forSegmentAtIndex:2];

        
        
        [appDelegate selectIncomeExpenseByMDTOrYDT:@"MTD"];  
        
        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        [appDelegate.incomeExpenseArray sortUsingDescriptors:[NSArray arrayWithObject:dateDescriptor]];
        
        
        nslog(@"income expense array in incomeExpenseFilterSegmentControl_Changed ===%@",appDelegate.incomeExpenseArray);
        [incomeArray removeAllObjects];
        [expenseArray removeAllObjects];        
        
        
        [dateDescriptor release];
        for (int i = 0; i<[appDelegate.incomeExpenseArray count];i++)
        {
            if ([[[appDelegate.incomeExpenseArray objectAtIndex:i]valueForKey:@"incomeExpense"]intValue]==1)
            {
                [incomeArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
            else
            {
                [expenseArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
        }
        
        //nslog(@"income expense array == %@",appDelegate.incomeExpenseArray);
        if([appDelegate.incomeExpenseArray count]==0)
        {
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = FALSE;
            lblNoData.hidden = FALSE;
            [tblView setHidden:TRUE];
            //[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
            [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            //  [imgView setContentMode:UIViewContentModeScaleToFill];
            [self.view addSubview:imgView];
            [self.view sendSubviewToBack:imgView];
            
            [imgView release];
            
            
        }
        else 
        {
            [tblView setHidden:FALSE];
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = TRUE;
            lblNoData.hidden = TRUE;
            [tblView reloadData];
            
        }

        
        
        
    }
    else if(tempSegementedControl.selectedSegmentIndex == 1)
    {
        
        [incomeExpenseFilterSegmentControl setImage:[UIImage imageNamed:@"mtd.png"] forSegmentAtIndex:0];
        [incomeExpenseFilterSegmentControl setImage:[UIImage imageNamed:@"mtd.png"] forSegmentAtIndex:1];
        [incomeExpenseFilterSegmentControl setImage:[UIImage imageNamed:@"all.png"] forSegmentAtIndex:2];
        
        [appDelegate selectIncomeExpenseByMDTOrYDT:@"YTD"];     
        
        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        [appDelegate.incomeExpenseArray sortUsingDescriptors:[NSArray arrayWithObject:dateDescriptor]];
        
        
        nslog(@"income expense array = 1===%@",appDelegate.incomeExpenseArray);
        [incomeArray removeAllObjects];
        [expenseArray removeAllObjects];        
        
        
        [dateDescriptor release];
        for (int i = 0; i<[appDelegate.incomeExpenseArray count];i++)
        {
            if ([[[appDelegate.incomeExpenseArray objectAtIndex:i]valueForKey:@"incomeExpense"]intValue]==1)
            {
                [incomeArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
            else
            {
                [expenseArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
        }
        
        //nslog(@"income expense array == %@",appDelegate.incomeExpenseArray);
        if([appDelegate.incomeExpenseArray count]==0)
        {
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = FALSE;
            lblNoData.hidden = FALSE;
            [tblView setHidden:TRUE];
            //[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
            [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            //  [imgView setContentMode:UIViewContentModeScaleToFill];
            [self.view addSubview:imgView];
            [self.view sendSubviewToBack:imgView];
            
            [imgView release];
            
            
        }
        else
        {
            [tblView setHidden:FALSE];
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = TRUE;
            lblNoData.hidden = TRUE;
            [tblView reloadData];
            
        }
        

        
    }
    else if(tempSegementedControl.selectedSegmentIndex == 2)
    {
        [incomeExpenseFilterSegmentControl setImage:[UIImage imageNamed:@"mtd.png"] forSegmentAtIndex:0];
        [incomeExpenseFilterSegmentControl setImage:[UIImage imageNamed:@"ytd.png"] forSegmentAtIndex:1];
        [incomeExpenseFilterSegmentControl setImage:[UIImage imageNamed:@"all_selected.png"] forSegmentAtIndex:2];
        
        [appDelegate selectIncomeExpense];     

        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        [appDelegate.incomeExpenseArray sortUsingDescriptors:[NSArray arrayWithObject:dateDescriptor]];
        
        
        nslog(@"income expense array in tempSegementedControl 2===%@",appDelegate.incomeExpenseArray);
        [incomeArray removeAllObjects];
        [expenseArray removeAllObjects];        
        
        
        [dateDescriptor release];
        for (int i = 0; i<[appDelegate.incomeExpenseArray count];i++)
        {
            if ([[[appDelegate.incomeExpenseArray objectAtIndex:i]valueForKey:@"incomeExpense"]intValue]==1)
            {
                [incomeArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
            else
            {
                [expenseArray addObject:[appDelegate.incomeExpenseArray objectAtIndex:i]];
            }
        }
        
        //nslog(@"income expense array == %@",appDelegate.incomeExpenseArray);
        if([appDelegate.incomeExpenseArray count]==0)
        {
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = FALSE;
            lblNoData.hidden = FALSE;
            [tblView setHidden:TRUE];
            //[self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
            [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            //  [imgView setContentMode:UIViewContentModeScaleToFill];
            [self.view addSubview:imgView];
            [self.view sendSubviewToBack:imgView];
            
            [imgView release];
            
            
        }
        else 
        {
            [tblView setHidden:FALSE];
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = TRUE;
            lblNoData.hidden = TRUE;
            [tblView reloadData];
            
        }
        

    }

}

#pragma mark - incomeExpenseTypeSegmentControl_Changed_Through_Click
#pragma mark manages income data/expense data


-(IBAction)incomeExpenseTypeSegmentControl_Changed_Through_Click:(id)sender
{
    UIButton *tempButton = (UIButton*)sender;
    segment.selectedSegmentIndex = (tempButton.tag - 7000);
    

    
    if (segment.selectedSegmentIndex == 2)
	{
        if(appDelegate.isIPad)
        {
            [incomeButton setImage:[UIImage imageNamed:@"income_ipad.png"] forState:UIControlStateNormal];
            [incomeButton setImage:[UIImage imageNamed:@"income_ipad.png"] forState:UIControlStateHighlighted];
            
            
            [expenseButton setImage:[UIImage imageNamed:@"expense_ipad.png"] forState:UIControlStateNormal];
            [expenseButton setImage:[UIImage imageNamed:@"expense_ipad.png"] forState:UIControlStateHighlighted];
            
            
            [bothButton setImage:[UIImage imageNamed:@"both_ipad_selected.png"] forState:UIControlStateNormal];
            [bothButton setImage:[UIImage imageNamed:@"both_ipad_selected.png"] forState:UIControlStateHighlighted];
            
        }
        else
        {
            
            [incomeButton setImage:[UIImage imageNamed:@"income.png"] forState:UIControlStateNormal];
            [incomeButton setImage:[UIImage imageNamed:@"income.png"] forState:UIControlStateHighlighted];
            
            
            [expenseButton setImage:[UIImage imageNamed:@"expense.png"] forState:UIControlStateNormal];
            [expenseButton setImage:[UIImage imageNamed:@"expense.png"] forState:UIControlStateHighlighted];
            
            
            [bothButton setImage:[UIImage imageNamed:@"both_selected.png"] forState:UIControlStateNormal];
            [bothButton setImage:[UIImage imageNamed:@"both_selected.png"] forState:UIControlStateHighlighted];
            
        }
        
        

        
        

	}
	else if (segment.selectedSegmentIndex == 1)
	{
		
        if(appDelegate.isIPad)
        {
            
            [incomeButton setImage:[UIImage imageNamed:@"income_ipad.png"] forState:UIControlStateNormal];
            [incomeButton setImage:[UIImage imageNamed:@"income_ipad.png"] forState:UIControlStateHighlighted];
            
            
            [expenseButton setImage:[UIImage imageNamed:@"expense_ipad_selected.png"] forState:UIControlStateNormal];
            [expenseButton setImage:[UIImage imageNamed:@"expense_ipad_selected.png"] forState:UIControlStateHighlighted];
            
            
            [bothButton setImage:[UIImage imageNamed:@"both_ipad.png"] forState:UIControlStateNormal];
            [bothButton setImage:[UIImage imageNamed:@"both_ipad.png"] forState:UIControlStateHighlighted];
            
        }
        else
        {
            [incomeButton setImage:[UIImage imageNamed:@"income.png"] forState:UIControlStateNormal];
            [incomeButton setImage:[UIImage imageNamed:@"income.png"] forState:UIControlStateHighlighted];
            
            
            [expenseButton setImage:[UIImage imageNamed:@"expense_selected.png"] forState:UIControlStateNormal];
            [expenseButton setImage:[UIImage imageNamed:@"expense_selected.png"] forState:UIControlStateHighlighted];
            
            
            [bothButton setImage:[UIImage imageNamed:@"both.png"] forState:UIControlStateNormal];
            [bothButton setImage:[UIImage imageNamed:@"both.png"] forState:UIControlStateHighlighted];
            
        }
        
        
	}
	else
    {
        if(appDelegate.isIPad)
        {
            [incomeButton setImage:[UIImage imageNamed:@"income_ipad_selected.png"] forState:UIControlStateNormal];
            [incomeButton setImage:[UIImage imageNamed:@"income_ipad_selected.png"] forState:UIControlStateHighlighted];
            
            
            [expenseButton setImage:[UIImage imageNamed:@"expense_ipad.png"] forState:UIControlStateNormal];
            [expenseButton setImage:[UIImage imageNamed:@"expense_ipad.png"] forState:UIControlStateHighlighted];
            
            
            [bothButton setImage:[UIImage imageNamed:@"both_ipad.png"] forState:UIControlStateNormal];
            [bothButton setImage:[UIImage imageNamed:@"both_ipad.png"] forState:UIControlStateHighlighted];
        }
        else
        {
            [incomeButton setImage:[UIImage imageNamed:@"income_selected.png"] forState:UIControlStateNormal];
            [incomeButton setImage:[UIImage imageNamed:@"income_selected.png"] forState:UIControlStateHighlighted];
            
            
            [expenseButton setImage:[UIImage imageNamed:@"expense.png"] forState:UIControlStateNormal];
            [expenseButton setImage:[UIImage imageNamed:@"expense.png"] forState:UIControlStateHighlighted];
            
            
            [bothButton setImage:[UIImage imageNamed:@"both.png"] forState:UIControlStateNormal];
            [bothButton setImage:[UIImage imageNamed:@"both.png"] forState:UIControlStateHighlighted];
        }
        
        
		
	}
    
    
    if (searching)
    {
        [self searchCat];
    }
    [tblView reloadData];

    
    
}


-(IBAction)segment_changed
{
    if (segment.selectedSegmentIndex == 2)
	{
		barImage.image = [UIImage imageNamed:@"blackBar-1"];
	}
	else if (segment.selectedSegmentIndex == 1)
	{
		barImage.image = [UIImage imageNamed:@"redBar-1"];
	}
	else 
    {
		barImage.image = [UIImage imageNamed:@"greenBar-1"];
	}

    
    if (searching)
    {
        [self searchCat];
    }
    [tblView reloadData];
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
	
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    // Return the number of rows in the section.
    if (searching)
    {
        nslog(@"\n tempSearch  = %@",tempSearch);
        return [tempSearch count];
    }
    else
    {
        if ([appDelegate.incomeExpenseArray count]>0)
        {
            if (segment.selectedSegmentIndex == 2)
            {
                return [appDelegate.incomeExpenseArray count];
            }
            else if (segment.selectedSegmentIndex == 0)
            {
                
                return [incomeArray count];
            }
            else if (segment.selectedSegmentIndex == 1)
            {
                return [expenseArray count];    
            }
        }
    }
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    //[appDelegate selectCurrencyIndex];
    
    
   
    static NSString *CellIdentifier = @"Cell";
    UIImageView *cellImage;
    UIImageView*cell_receipt_image;
    
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    
 nslog(@"\n locale = %@",numberFormatter.locale.localeIdentifier);
    nslog(@"\n appDelegate.curCode = %@",appDelegate.curCode);
    
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [numberFormatter setCurrencyCode:appDelegate.curCode];
       
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            
            [numberFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
           
            [numberFormatter setLocale:locale];
            [locale release];
            
            
        } 
        else if([[numberFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [numberFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [numberFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"EUR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_PT"])
        {
            [numberFormatter setCurrencyCode:@"EUR"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"pt_PT"];
            [numberFormatter setLocale:locale];
            [locale release];

        }
         

    }
    


    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.tag = indexPath.row;
    if (cell == nil)
    //if (1)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        cellImage = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, 47,50)];
        cellImage.tag = 5000;
        [cell.contentView addSubview:cellImage];
        
        
        /*SUN:0004
         
         Pending ipad
         */
        cell_receipt_image = [[UIImageView alloc]init];
        cell_receipt_image.tag = 700;
        
        
               
        
        
        cell_receipt_image.image = [UIImage imageNamed:@"receipt_icon.png"];
        
      
        
        [cell.contentView addSubview:cell_receipt_image];
        
        
        label = [[UILabel alloc]initWithFrame:CGRectMake(50, 0,140, 30)];
        
            
        
        //label.tag = CELL_LABEL_START_TAG+indexPath.row;
        label.tag = CELL_LABEL_START_TAG;
        label.numberOfLines = 1;
        label.lineBreakMode = UILineBreakModeTailTruncation;
        [label setFont:[UIFont systemFontOfSize:13.0]];
        [label setBackgroundColor:[UIColor clearColor]];
        
        [cell.contentView addSubview:label];
        
        
   
        dateLable = [[UILabel alloc]initWithFrame:CGRectMake(50, 20, 140, 40)];
        dateLable.tag = 450;
        
        dateLable.numberOfLines = 0;
        dateLable.lineBreakMode = UILineBreakModeWordWrap;
        [dateLable setTextColor:[UIColor colorWithRed:81.0/255.0 green:106.0/255.0 blue:150.0/255.0 alpha:1.0]];
        [dateLable setFont:[UIFont systemFontOfSize:13.0]];
        [dateLable setBackgroundColor:[UIColor clearColor]];
        
        [cell.contentView addSubview:dateLable];
        
        

       /*SUN:0004
        iPad Pending...
        */
        detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(190, 0, 85, 30)];
        
               
        
        detailLabel.tag = 500;
       
        detailLabel.numberOfLines = 0;
        detailLabel.textAlignment = UITextAlignmentRight;
        detailLabel.lineBreakMode = UILineBreakModeTailTruncation;
        [detailLabel setFont:[UIFont systemFontOfSize:13.0]];
        [detailLabel setBackgroundColor:[UIColor clearColor]];
        
        [cell.contentView addSubview:detailLabel];
        
       amountLabel = [[UILabel alloc]initWithFrame:CGRectMake(190,25, 85, 30)];
        
        
              
        
        
        amountLabel.tag = 600;
        amountLabel.textAlignment = UITextAlignmentRight;
        amountLabel.numberOfLines = 0;
        amountLabel.lineBreakMode = UILineBreakModeTailTruncation;
        [amountLabel setFont:[UIFont systemFontOfSize:13.0]];
        [amountLabel setBackgroundColor:[UIColor clearColor]];
        
        [cell.contentView addSubview:amountLabel];
        
        

    }
    else
    {
        //label = (UILabel *)[cell.contentView viewWithTag:CELL_LABEL_START_TAG+indexPath.row];
        label = (UILabel *)[cell.contentView viewWithTag:CELL_LABEL_START_TAG];
        
        nslog(@"\n CELL_LABEL_START_TAG+indexPath.row = %d \n",CELL_LABEL_START_TAG+indexPath.row);
        nslog(@"\n label.tag in cell!=nil = %d \n",label.tag);
        
        dateLable = (UILabel *)[cell.contentView viewWithTag:450];
        detailLabel = (UILabel *)[cell.contentView viewWithTag:500];
        amountLabel = (UILabel *)[cell.contentView viewWithTag:600];
        cellImage = (UIImageView *)[cell.contentView viewWithTag:5000];
        cell_receipt_image= (UIImageView *)[cell.contentView viewWithTag:700];
        
        
        [dateLable retain];
        [label retain];
        [detailLabel retain];
        [amountLabel retain];
        [cellImage retain];
        [cell_receipt_image retain];
        
        
        
    }
    
    
    

    if(appDelegate.isIPad)
    {
        
        if(appDelegate.isIOS7)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
            {
                cell_receipt_image.frame = CGRectMake(744,0, 24,24);
            }
            else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
                
            {
                cell_receipt_image.frame = CGRectMake(1001,0, 24,24);
            }

        }
        else
        {
            
            if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
            {
                cell_receipt_image.frame = CGRectMake(654,0, 24,24);
            }
            else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
                
            {
                cell_receipt_image.frame = CGRectMake(911,0, 24,24);
            }

        }
        
        
        
    }
    else
    {
        cell_receipt_image.frame = CGRectMake(276,0, 24,24);
        if(appDelegate.isIOS7)
        {
            cell_receipt_image.frame = CGRectMake(296,-2, 24,24);
        }
        
        
    }
    if(indexPath.row==0)
    {
        cell_receipt_image.image = [UIImage imageNamed:@"receipt_icon_curve.png"];
    }
    
    //
    
    if(appDelegate.isIPad)
    {
        
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 ||[UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            label.frame = CGRectMake(50, 0, 170, 30);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 ||[UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            label.frame = CGRectMake(50, 0, 220, 30);
            
        }
        
    }
    else
    {
        label.frame = CGRectMake(50, 0,140, 30);
    }

    //
    
    //detailLabel.backgroundColor = [UIColor redColor];
    
    
    if(appDelegate.isIPad)
    {
        if(appDelegate.isIOS7)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 ||[UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                detailLabel.frame = CGRectMake(400, 0, 330, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 ||[UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                detailLabel.frame = CGRectMake(480, 0, 490, 30);
            }
        }
        else
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 ||[UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                detailLabel.frame = CGRectMake(400, 0, 250, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 ||[UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                detailLabel.frame = CGRectMake(480, 0, 420, 30);
            }
        }
        
    }
    else
    {
        detailLabel.frame = CGRectMake(190, 0, 85, 30);
    }

    //
    if(appDelegate.isIPad)
    {
        
        if(appDelegate.isIOS7)
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 ||[UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                amountLabel.frame = CGRectMake(200, 25,530, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 ||[UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                amountLabel.frame = CGRectMake(300, 25,670, 30);
            }
        }
        else
        {
            if([UIApplication sharedApplication].statusBarOrientation == 1 ||[UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                amountLabel.frame = CGRectMake(200, 25,450, 30);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 ||[UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                amountLabel.frame = CGRectMake(300, 25,600, 30);
            }
        }
        
    }
    else
    {
        amountLabel.frame = CGRectMake(170,25, 105, 30);
    }
    
    //amountLabel.backgroundColor = [UIColor redColor];
    
    
    
    
    
    // Configure the cell...
    if (searching)
    {
        label.text = [[tempSearch objectAtIndex:indexPath.row]valueForKey:@"type"];
        detailLabel.text = [[tempSearch objectAtIndex:indexPath.row]valueForKey:@"property"];
        if([[[tempSearch objectAtIndex:indexPath.row] objectForKey:@"is_receipt_image_choosen"] intValue]==0)
        {
            cell_receipt_image.hidden = TRUE;
        }
        else
        {
            cell_receipt_image.hidden = FALSE;
        }
        
        
        
        
        /*
         SUNSHINE:South African currency causing issue... :-(
         ID:0004
         */

        
        if([[[tempSearch objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue] == 2)
        {
            amountLabel.text = [[tempSearch objectAtIndex:indexPath.row]valueForKey:@"amount"];
            
        }
        else
        {
            amountLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[[tempSearch objectAtIndex:indexPath.row]valueForKey:@"amount"]floatValue]]];
            
        }
        
        
        
               
       
        
        
        if ([[[tempSearch objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue] == 0)//Expense...
        {
            
            
            if([[[tempSearch objectAtIndex:indexPath.row]valueForKey:@"isrecursive"]intValue] == 1)
            {
                
                
                
                if(indexPath.row==0 && [tempSearch count]==1)//First row and only one item(both side curve)
                {
                    cellImage.image = [UIImage imageNamed:@"recurring_expence_top_bottom_curve.png"];
                }
                else if([tempSearch count]>1 && indexPath.row == ([tempSearch count]-1))/*last row.*/
                {
                    cellImage.image = [UIImage imageNamed:@"recurring_expence_bottom_curve.png"];
                }
                else if([tempSearch count]>1 && indexPath.row==0)
                {
                    cellImage.image = [UIImage imageNamed:@"recurring_expense_top_curve.png"];
                }
                else
                {
                    cellImage.image = [UIImage imageNamed:@"recurring_expence.png"];
                }

                
                
                
            }
            else
            {
                
                if(indexPath.row==0 && [tempSearch count]==1)//First row and only one item(both side curve)
                {
                     cellImage.image = [UIImage imageNamed:@"new-cell-expense_top_bottom_curve.png"];
                }
                else if([tempSearch count]>1 && indexPath.row == ([tempSearch count]-1))/*last row.*/
                {
                     cellImage.image = [UIImage imageNamed:@"new-cell-expense_bottom_curve.png"];
                }
                else if([tempSearch count]>1 && indexPath.row==0)
                {
                    cellImage.image = [UIImage imageNamed:@"new-cell-expense_top_curve.png"];
                }
                else
                {
                    cellImage.image = [UIImage imageNamed:@"new-cell-expense.png"];
                }
                
                
            }
            amountLabel.textColor = [UIColor redColor];
        }
        else if ([[[tempSearch objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue] == 1)//income
        {
            
            if([[[tempSearch objectAtIndex:indexPath.row]valueForKey:@"isrecursive"]intValue] == 1)
            {
               // cellImage.image = [UIImage imageNamed:@"recurring_income.png"];
                
                
                if(indexPath.row==0 && [tempSearch count]==1)//First row and only one item(both side curve)
                {
                    cellImage.image = [UIImage imageNamed:@"recurring_income_top_bottom_curve.png"];
                }
                else if([tempSearch count]>1 && indexPath.row == ([tempSearch count]-1))/*last row.*/
                {
                    cellImage.image = [UIImage imageNamed:@"recurring_income_bottom_curve.png"];
                }
                else if([tempSearch count]>1 && indexPath.row==0)
                {
                    cellImage.image = [UIImage imageNamed:@"recurring_income_top_curve.png"];
                }
                else
                {
                    cellImage.image = [UIImage imageNamed:@"recurring_income.png"];
                }

                
                
                
                
            }
            else
            {
               
                
                
                if(indexPath.row==0 && [tempSearch count]==1)//First row and only one item(both side curve)
                {
                    cellImage.image = [UIImage imageNamed:@"income-revised-30_top_bottom_curve.png"];
                }
                else if([tempSearch count]>1 && indexPath.row == ([tempSearch count]-1))/*last row.*/
                {
                    cellImage.image = [UIImage imageNamed:@"income-revised-30_bottom_curve.png"];
                }
                else if([tempSearch count]>1 && indexPath.row==0)
                {
                    cellImage.image = [UIImage imageNamed:@"income-revised-30_top_curve.png"];
                }
                else
                {
                    cellImage.image = [UIImage imageNamed:@"income-revised-30.png"];
                }

                
                
                
            }

            
            
            [amountLabel setTextColor:[UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0]];
        }
        else//Milege
        {
            
            
            if(indexPath.row==0 && [tempSearch count]==1)//First row and only one item(both side curve)
            {
                cellImage.image = [UIImage imageNamed:@"new-cell-milage_top_bottom_curve.png"];
            }
            else if([tempSearch count]>1 && indexPath.row == ([tempSearch count]-1))/*last row.*/
            {
                cellImage.image = [UIImage imageNamed:@"new-cell-milage_bottom_curve.png"];
            }
            else if([tempSearch count]>1 && indexPath.row==0)
            {
                cellImage.image = [UIImage imageNamed:@"new-cell-milage_top_curve.png"];
            }
            else
            {
                cellImage.image = [UIImage imageNamed:@"new-cell-milage.png"];
            }
            
            amountLabel.textColor = [UIColor redColor];
            
        }

        
        
        dateLable.text = [appDelegate.regionDateFormatter stringFromDate:[[tempSearch objectAtIndex:indexPath.row]valueForKey:@"date"]];

    }

    else
    {
        if ([appDelegate.incomeExpenseArray count]>0)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            if (segment.selectedSegmentIndex == 2)
            {
                
                
                
                if([[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row] objectForKey:@"is_receipt_image_choosen"] intValue]==0)
                {
                    cell_receipt_image.hidden = TRUE;
                }
                else
                {
                    cell_receipt_image.hidden = FALSE;
                }

                
                nslog(@"\n type = %@",[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"type"]);
                
                nslog(@"\n label.tag = %d",label.tag);
                nslog(@"\n indexPath.row = %d",indexPath.row);
                
                
                
                label.text = [[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"type"];
                
                
                detailLabel.text = [[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"property"];
                
                
                
                
                /*
                    SUN:004
                 */
                if ([[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue] == 2)
                {
                    
                    /*
                    amountLabel.text = [[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"amount"];
                    */
                    
                    amountLabel.text = [NSString stringWithFormat:@"%@ %@",[appDelegate.prefs objectForKey:@"milage_unit"],[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"amount"]];
                    
                    
                }
                else
                {
                    
                  //amountLabel.text =  [NSString stringWithFormat:@"%@%@",[numberFormatter currencySymbol],[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"amount"]];
                  
                    
                    amountLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"amount"]floatValue]]];

                }
                
                
                               
                

                
                
                
                
                if ([[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue] == 0)//Expense...
                {
                    
                    
                    if([[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"isrecursive"]intValue] == 1)
                    {
                        
                        
                        
                        if(indexPath.row==0 && [appDelegate.incomeExpenseArray count]==1)//First row and only one item(both side curve)
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expence_top_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row == ([appDelegate.incomeExpenseArray count]-1))/*last row.*/
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expence_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row==0)
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expense_top_curve.png"];
                        }
                        else
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expence.png"];
                        }
                        
                        
                        
                        
                    }
                    else
                    {
                        
                        if(indexPath.row==0 && [appDelegate.incomeExpenseArray count]==1)//First row and only one item(both side curve)
                        {
                            cellImage.image = [UIImage imageNamed:@"new-cell-expense_top_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row == ([appDelegate.incomeExpenseArray count]-1))/*last row.*/
                        {
                            cellImage.image = [UIImage imageNamed:@"new-cell-expense_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row==0)
                        {
                            cellImage.image = [UIImage imageNamed:@"new-cell-expense_top_curve.png"];
                        }
                        else
                        {
                            cellImage.image = [UIImage imageNamed:@"new-cell-expense.png"];
                        }
                        
                        
                    }
                    amountLabel.textColor = [UIColor redColor];
                }
                else if ([[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue] == 1) //income
                {
                    
                    if([[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"isrecursive"]intValue] == 1)
                    {
                        // cellImage.image = [UIImage imageNamed:@"recurring_income.png"];
                        
                        
                        if(indexPath.row==0 && [appDelegate.incomeExpenseArray count]==1)//First row and only one item(both side curve)
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_income_top_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row == ([appDelegate.incomeExpenseArray count]-1))/*last row.*/
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_income_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row==0)
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_income_top_curve.png"];
                        }
                        else
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_income.png"];
                        }
                        
                        
                        
                        
                        
                    }
                    else
                    {
                        
                        
                        
                        if(indexPath.row==0 && [appDelegate.incomeExpenseArray count]==1)//First row and only one item(both side curve)
                        {
                            cellImage.image = [UIImage imageNamed:@"income-revised-30_top_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row == ([appDelegate.incomeExpenseArray count]-1))/*last row.*/
                        {
                            cellImage.image = [UIImage imageNamed:@"income-revised-30_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row==0)
                        {
                            cellImage.image = [UIImage imageNamed:@"income-revised-30_top_curve.png"];
                        }
                        else
                        {
                            cellImage.image = [UIImage imageNamed:@"income-revised-30.png"];
                        }
                        
                        
                        
                        
                    }
                    
                    
                    
                    [amountLabel setTextColor:[UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0]];
                }
                else //MILEGE
                {
                    
                    /*SUN:99.99% THERE WILL BE NO RECURRING
                    
                    if([[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"isrecursive"]intValue] == 1)
                    {
                        
                        
                        
                        if(indexPath.row==0 && [appDelegate.incomeExpenseArray count]==1)//First row and only one item(both side curve)
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expence_top_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row == ([appDelegate.incomeExpenseArray count]-1))
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expence_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row==0)
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expense_top_curve.png"];
                        }
                        else
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expence.png"];
                        }
                        
                        
                        
                        
                    }
                    
                    else*/
                    {
                        
                        if(indexPath.row==0 && [appDelegate.incomeExpenseArray count]==1)//First row and only one item(both side curve)
                        {
                            cellImage.image = [UIImage imageNamed:@"new-cell-milage_top_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row == ([appDelegate.incomeExpenseArray count]-1))
                        {
                            cellImage.image = [UIImage imageNamed:@"new-cell-milage_bottom_curve.png"];
                        }
                        else if([appDelegate.incomeExpenseArray count]>1 && indexPath.row==0)
                        {
                            cellImage.image = [UIImage imageNamed:@"new-cell-milage_top_curve.png"];
                        }
                        else
                        {
                            cellImage.image = [UIImage imageNamed:@"new-cell-milage.png"];
                        }
                        
                        
                    }
                    amountLabel.textColor = [UIColor redColor];

                }
                
                
                
                
                dateLable.text = [appDelegate.regionDateFormatter stringFromDate:[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"date"]];

            // more method
            
            }
            else if(segment.selectedSegmentIndex == 0)
            {
                
                
                if([[[incomeArray objectAtIndex:indexPath.row] objectForKey:@"is_receipt_image_choosen"] intValue]==0)
                {
                    cell_receipt_image.hidden = TRUE;
                }
                else
                {
                    cell_receipt_image.hidden = FALSE;
                }
                

                
                label.text = [[incomeArray objectAtIndex:indexPath.row]valueForKey:@"type"];
                detailLabel.text = [[incomeArray objectAtIndex:indexPath.row]valueForKey:@"property"];
                
                
                 amountLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[[incomeArray objectAtIndex:indexPath.row]valueForKey:@"amount"]floatValue]]];
                
                
                                
                
                amountLabel.textColor = [UIColor colorWithRed:14.0/255.0 green:184.0/255.0 blue:29.0/255.0 alpha:1.0];
              
                
                if([[[incomeArray objectAtIndex:indexPath.row]valueForKey:@"isrecursive"]intValue] == 1)
                {
                    // cellImage.image = [UIImage imageNamed:@"recurring_income.png"];
                    
                    
                    if(indexPath.row==0 && [incomeArray count]==1)//First row and only one item(both side curve)
                    {
                        cellImage.image = [UIImage imageNamed:@"recurring_income_top_bottom_curve.png"];
                    }
                    else if([incomeArray count]>1 && indexPath.row == ([incomeArray count]-1))/*last row.*/
                    {
                        cellImage.image = [UIImage imageNamed:@"recurring_income_bottom_curve.png"];
                    }
                    else if([incomeArray count]>1 && indexPath.row==0)
                    {
                        cellImage.image = [UIImage imageNamed:@"recurring_income_top_curve.png"];
                    }
                    else
                    {
                        cellImage.image = [UIImage imageNamed:@"recurring_income.png"];
                    }
                    
                    
                    
                    
                    
                }
                else
                {
                    
                    
                    
                    if(indexPath.row==0 && [incomeArray count]==1)//First row and only one item(both side curve)
                    {
                        cellImage.image = [UIImage imageNamed:@"income-revised-30_top_bottom_curve.png"];
                    }
                    else if([incomeArray count]>1 && indexPath.row == ([incomeArray count]-1))
                    {
                        cellImage.image = [UIImage imageNamed:@"income-revised-30_bottom_curve.png"];
                    }
                    else if([incomeArray count]>1 && indexPath.row==0)
                    {
                        cellImage.image = [UIImage imageNamed:@"income-revised-30_top_curve.png"];
                    }
                    else
                    {
                        cellImage.image = [UIImage imageNamed:@"income-revised-30.png"];
                    }
                    
                    
                    
                    
                }
                    

                
                dateLable.text = [appDelegate.regionDateFormatter stringFromDate:[[incomeArray objectAtIndex:indexPath.row]valueForKey:@"date"]];

                
                
            }
            else if (segment.selectedSegmentIndex == 1)/*
                                                            SUN:004:EXPENSE + MILEGE
                                                        */
            {
                
                if([[[expenseArray objectAtIndex:indexPath.row] objectForKey:@"is_receipt_image_choosen"] intValue]==0)
                {
                    cell_receipt_image.hidden = TRUE;
                }
                else
                {
                    cell_receipt_image.hidden = FALSE;
                }

                
                label.text = [[expenseArray objectAtIndex:indexPath.row]valueForKey:@"type"];
                detailLabel.text = [[expenseArray objectAtIndex:indexPath.row]valueForKey:@"property"];
                
                
                
                if ([[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue]==2)/*Sun:004
                                                                                                           If milege no         formattting.
                                                                                                           */
                {
                    /*
                    amountLabel.text =[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"amount"];
                    */
                   amountLabel.text = [NSString stringWithFormat:@"%@ %@",[appDelegate.prefs objectForKey:@"milage_unit"],[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"amount"]];
                    
                    
                    
                }
                
                else
                {
                    amountLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"amount"]floatValue]]];
                }
                
                                
                
                amountLabel.textColor = [UIColor redColor];
                
                
                
                if([[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"isrecursive"]intValue] == 1)
                {
                    cellImage.image = [UIImage imageNamed:@"recurring_expence.png"];
                }
                else
                {
                    cellImage.image = [UIImage imageNamed:@"new-cell-expense.png"];    
                }
                


                    
                    
                    if([[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"isrecursive"]intValue] == 1)
                    {
                        
                        
                        
                        if(indexPath.row==0 && [expenseArray count]==1)//First row and only one item(both side curve)
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expence_top_bottom_curve.png"];
                        }
                        else if([expenseArray count]>1 && indexPath.row == ([expenseArray count]-1))/*last row.*/
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expence_bottom_curve.png"];
                        }
                        else if([expenseArray count]>1 && indexPath.row==0)
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expense_top_curve.png"];
                        }
                        else
                        {
                            cellImage.image = [UIImage imageNamed:@"recurring_expence.png"];
                        }
                        
                        
                        
                        
                    }
                    else
                    {
                        
                        /*Sun:004
                         We need to put condition in each cell for mileage..as it may come anywhere..
                         */
                        
                        if(indexPath.row==0 && [expenseArray count]==1)//First row and only one item(both side curve)
                        {
                            
                            if([[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue]==2)
                            {
                                cellImage.image = [UIImage imageNamed:@"new-cell-milage_top_bottom_curve.png"];
                            }
                            else
                            {
                                cellImage.image = [UIImage imageNamed:@"new-cell-expense_top_bottom_curve.png"];
                            }
                            
                            
                        }
                        else if([expenseArray count]>1 && indexPath.row == ([expenseArray count]-1))/*last row.*/
                        {
                            if([[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue]==2)
                            {
                                cellImage.image = [UIImage imageNamed:@"new-cell-milage_bottom_curve.png"];
                            }
                            else
                            {
                                cellImage.image = [UIImage imageNamed:@"new-cell-expense_bottom_curve.png"];
                            }
                            
                            
                        }
                        else if([expenseArray count]>1 && indexPath.row==0)
                        {
                            if([[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue]==2)
                            {
                                cellImage.image = [UIImage imageNamed:@"new-cell-milage_top_curve.png"];
                            }
                            else
                            {
                                cellImage.image = [UIImage imageNamed:@"new-cell-expense_top_curve.png"];
                            }
                            
                        }
                        else
                        {
                            if([[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue]==2)
                            {
                                cellImage.image = [UIImage imageNamed:@"new-cell-milage.png"];
                            }
                            else
                            {
                                cellImage.image = [UIImage imageNamed:@"new-cell-expense.png"];
                            }
                        
                        }
                        
                        
                    }
                
                dateLable.text = [appDelegate.regionDateFormatter stringFromDate:[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"date"]];
                
                
                
            }
        }
    }
   
    [label release];
    [dateLable release];
    [detailLabel release];
    [cellImage release];
    [cell_receipt_image release];
    [amountLabel release];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    selectedIndexpahRow = indexPath.row;
    int isReccuring;
    
     if (!searching)
     {
         if (segment.selectedSegmentIndex == 2)
         {
             
             nslog(@"\n incomeExpenseArray = %@",[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]);   
             isReccuring = [[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]objectForKey:@"isrecursive"] intValue];
             
         }
         else if (segment.selectedSegmentIndex == 1)
         {
             
             nslog(@"\n expenseArray = %@",[expenseArray objectAtIndex:indexPath.row]); 
             isReccuring = [[[expenseArray objectAtIndex:indexPath.row]objectForKey:@"isrecursive"] intValue];

                          
         }
         else if (segment.selectedSegmentIndex == 0)
         {
             nslog(@"\n incomeArray = %@",[incomeArray objectAtIndex:indexPath.row]);
             isReccuring = [[[incomeArray objectAtIndex:indexPath.row]objectForKey:@"isrecursive"] intValue];

            
         }

     }
     else if(searching)
     {
        isReccuring =  [[[tempSearch objectAtIndex:indexPath.row]objectForKey:@"isrecursive"] intValue];  
        
     }
    
    nslog(@"\n isReccuring = %d",isReccuring);
    
    
    if(isReccuring == 0)
    {
        
        if (!searching)
        {
            if (segment.selectedSegmentIndex == 2)
            {
                
                nslog(@"\n incomeExpenseArray = %@",[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]);   
                [appDelegate DeleteIncomeExpense:[[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"rowid"]intValue]];
                [appDelegate.incomeExpenseArray removeObjectAtIndex:indexPath.row];
                
            }
            else if (segment.selectedSegmentIndex == 1)
            {
                
                nslog(@"\n expenseArray = %@",[expenseArray objectAtIndex:indexPath.row]);   
                [appDelegate DeleteIncomeExpense:[[[expenseArray objectAtIndex:indexPath.row]valueForKey:@"rowid"]intValue]];
                [expenseArray removeObjectAtIndex:indexPath.row];
                
            }
            else if (segment.selectedSegmentIndex == 0)
            {
                [appDelegate DeleteIncomeExpense:[[[incomeArray objectAtIndex:indexPath.row]valueForKey:@"rowid"]intValue]];
                [incomeArray removeObjectAtIndex:indexPath.row];
            }
        }
        else
        {
            
            nslog(@"\n tempSearch ...%@",tempSearch);
            
            [appDelegate DeleteIncomeExpense:[[[tempSearch objectAtIndex:indexPath.row]valueForKey:@"rowid"]intValue]]; 
            
            [tempSearch removeObjectAtIndex:indexPath.row];
        }
        
        
        if([appDelegate.incomeExpenseArray count]==0)
        {
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = FALSE;
            
            lblNoData.hidden = FALSE;
            [tblView setHidden:TRUE];
            [self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            
        }
        else 
        {
            [tblView setHidden:FALSE];
            incomeImage.hidden = expenseImage.hidden = plusLabel.hidden = TRUE;
            lblNoData.hidden = TRUE;
            [tblView reloadData];
            
        }
        isViewWillAppearAfterDeleting = TRUE;
        
        if(appDelegate.isIPad)
        {
            
            if(appDelegate.isIOS7)
            {
                // need to check..
            }
            else
            {
                 [self viewWillAppear:YES];
            }
            
        }
        else
        {
             [self viewWillAppear:YES];
        }
       
        
    }
    else
    {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"This entry is recurring entry.\n What do you want to do?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Delete this entry",@" Delete this & future entries",@"Delete entire series", nil];
        actionSheet.tag = 998;
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        [actionSheet release];
    }
    
   
    
    
    
    

	
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    if ([appDelegate.incomeExpenseArray count]>0)
    {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}
 

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
   // [self doneSearching_Clicked];

    //if([[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row] objectForKey:@"parentid"] intValue] == 0)
    appDelegate.isMilege = FALSE;
    if(1)
    {
        appDelegate.isIncomeExpenseUpdate = TRUE;
        
        if (!searching)
        {
            
            appDelegate.incomeExpenseIndex = indexPath.row;
            nslog(@"\n row = %d",indexPath.row);
            nslog(@"\n appDelegate.incomeExpenseArray = %@",appDelegate.incomeExpenseArray);
            nslog(@"\n segment.selectedSegmentIndex = %d",segment.selectedSegmentIndex);
            if(segment.selectedSegmentIndex != 2)
            {
                [appDelegate.incomeExpenseArray removeAllObjects];
            }
            
            if(segment.selectedSegmentIndex == 0)
            {
                appDelegate.isIncome = TRUE;
                [appDelegate.incomeExpenseArray addObjectsFromArray:incomeArray];
                
            }
            else  if(segment.selectedSegmentIndex == 1)
            {
                
                appDelegate.isIncome = FALSE;
                [appDelegate.incomeExpenseArray addObjectsFromArray:expenseArray];
                
                
            }
            nslog(@"\n appDelegate.incomeExpenseArray = %@",appDelegate.incomeExpenseArray);
            
            if ([[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue]==1)
            {
                appDelegate.isIncome = TRUE;
            }
            else if ([[[appDelegate.incomeExpenseArray objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue]==0)
            {
                appDelegate.isIncome = FALSE;
            }
            else
            {
                appDelegate.isIncome = FALSE;
                appDelegate.isMilege = TRUE;
            }
        }
        else
        {
            int i;
            for (i =0;i<[appDelegate.incomeExpenseArray count];i++)
            {
                if ([[[tempSearch objectAtIndex:indexPath.row]valueForKey:@"rowid"]intValue] == [[[appDelegate.incomeExpenseArray objectAtIndex:i]valueForKey:@"rowid"]intValue])
                {
                    break;
                }
            }
            appDelegate.incomeExpenseIndex = i;
            if ([[[tempSearch objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue]==1)
            {
                appDelegate.isIncome = TRUE;
            }
            else if ([[[tempSearch objectAtIndex:indexPath.row]valueForKey:@"incomeExpense"]intValue]==0)
            {
                appDelegate.isIncome = FALSE;
            }
            else
            {
                appDelegate.isIncome = FALSE;
                appDelegate.isMilege = TRUE;
            }
            
        }
        
        //Receipt image was recising every update...(in ipad mini)
        appDelegate.is_receipt_image_updated=FALSE;
        appDelegate.is_receipt_image_choosen = FALSE;
        
        nslog(@"\n appDelegate.incomeExpenseArray finally = %@",appDelegate.incomeExpenseArray);
        addIncome = [[AddIncomeExpenseViewController alloc]initWithNibName:@"AddIncomeExpenseViewController" bundle:nil];
        [self.navigationController pushViewController:addIncome animated:YES];
        [addIncome release];

    }
    
    
    
}
#pragma mark UISearchBarDelegate



-(IBAction)cancelSearch_clicked
{
    [self doneSearching_Clicked];
    //cancelButton.userInteractionEnabled = FALSE;
    cancelButton.enabled = FALSE;
    
    if(appDelegate.isIPad)
    {
        tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width, 911.0f);    
    }
    else 
    {
        
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }

        
        
    }
    
    
    searching = NO;

}
-(IBAction)searchSegment_changed
{
    [self searchCat];
    [tblView reloadData];
}
- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar 
{
    
    cancelButton.enabled = TRUE;
    searchSegment.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
    [self.navigationController.navigationBar addSubview:searchSegment];
    letUserSelectRow = NO;

}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
	
	//Remove all objects first.
	[tempSearch removeAllObjects];
	
	if([searchText length] > 0) 
    {
		searching = YES;
		[self searchCat];
	}
    else if([searchText length] == 0)
    {
        [self doneSearching_Clicked];
        [searchBar retain];
        //cancelButton.userInteractionEnabled = FALSE;
        cancelButton.enabled = FALSE;
        searching = NO;

    }
	
	[tblView reloadData];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar 
{
	
    if(searching)
    {
        [self.view endEditing:TRUE];
        return;
    }
    
	searching = YES;

    [tempSearch removeAllObjects];
	[self searchCat];
	

	[searchBar resignFirstResponder];
    [self.view endEditing:TRUE];
	tblView.userInteractionEnabled = TRUE;
	tblView.scrollEnabled = YES;
	
	[tblView reloadData];
	
	
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self doneSearching_Clicked];
    //cancelButton.userInteractionEnabled = FALSE;
    cancelButton.enabled = FALSE;
    
    if(appDelegate.isIPad)
    {
        tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width, 911.0f);
    }
    else
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }

        
    }
    
    
    searching = NO;

}

- (void) searchCat 
{
	[tempSearch removeAllObjects];
   
	NSString *searchText = searchBar.text;
    
    nslog(@"\n searchText = %@",searchText);
    
    if (segment.selectedSegmentIndex == 2)
    {
        for(int i=0;i<[appDelegate.incomeExpenseArray count];i++)
        {
            NSDictionary *temp=[appDelegate.incomeExpenseArray objectAtIndex:i];
            
            nslog(@"\n temp = %@",temp);
            
            
            NSString *str   = [temp valueForKey:@"type"];
            NSString *str1  = [temp valueForKey:@"property"];
            NSString *str2  = [temp valueForKey:@"amount"];
            NSString *str3  = [appDelegate.regionDateFormatter stringFromDate:[temp valueForKey:@"date"]];
            NSString *str4  = [temp valueForKey:@"notes"];

            
            
            BOOL isFoundValue = FALSE;
            if ([str rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
            {
                isFoundValue = TRUE;
            } 
            else if ([str1 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
            {
                isFoundValue = TRUE;
            } 
            else if ([str2 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
            {
                isFoundValue = TRUE;
            } 
            else if ([str3 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
            {
                isFoundValue = TRUE;
            } 
            else if ([str4 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                isFoundValue = TRUE;
            }
            
            
            /*
            NSRange titleResultsRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange titleResultsRange1 = [str1 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange titleResultsRange2 = [str2 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange titleResultsRange3 = [str3 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (titleResultsRange.length > 0 || titleResultsRange1.length > 0 || titleResultsRange2.length > 0 || titleResultsRange3.length > 0)
            
            */
            if(isFoundValue)
            {
                [tempSearch addObject:temp];
            }
        }
    }
    else if (segment.selectedSegmentIndex == 1)
    {
        for(int i=0;i<[expenseArray count];i++)
        {
            NSDictionary *temp=[expenseArray objectAtIndex:i];
            NSString *str   =  [temp valueForKey:@"type"];
            NSString *str1  = [temp valueForKey:@"property"];
            NSString *str2  = [temp valueForKey:@"amount"];
            NSString *str3  = [appDelegate.regionDateFormatter stringFromDate:[temp valueForKey:@"date"]];
            NSString *str4  = [temp valueForKey:@"notes"];
            
            
            BOOL isFoundValue = FALSE;
            if ([str rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
            {
                isFoundValue = TRUE;
            } 
            else if ([str1 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
            {
                isFoundValue = TRUE;
            } 
            else if ([str2 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
            {
                isFoundValue = TRUE;
            } 
            else if ([str3 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
            {
                isFoundValue = TRUE;
            } 
            else if ([str4 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                isFoundValue = TRUE;
            }
            
            /*
             NSRange titleResultsRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
             NSRange titleResultsRange1 = [str1 rangeOfString:searchText options:NSCaseInsensitiveSearch];
             NSRange titleResultsRange2 = [str2 rangeOfString:searchText options:NSCaseInsensitiveSearch];
             NSRange titleResultsRange3 = [str3 rangeOfString:searchText options:NSCaseInsensitiveSearch];
             if (titleResultsRange.length > 0 || titleResultsRange1.length > 0 || titleResultsRange2.length > 0 || titleResultsRange3.length > 0)
             
             */
            if(isFoundValue)
            {
                [tempSearch addObject:temp];
            }
        }
    }
    else if (segment.selectedSegmentIndex == 0)
    {
            for(int i=0;i<[incomeArray count];i++)
            {
                NSDictionary *temp=[incomeArray objectAtIndex:i];
                NSString *str   =  [temp valueForKey:@"type"];
                NSString *str1  = [temp valueForKey:@"property"];
                NSString *str2  = [temp valueForKey:@"amount"];
                NSString *str3  = [appDelegate.regionDateFormatter stringFromDate:[temp valueForKey:@"date"]];
                NSString *str4  = [temp valueForKey:@"notes"];
                
                
                BOOL isFoundValue = FALSE;
                if ([str rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
                {
                    isFoundValue = TRUE;
                } 
                else if ([str1 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
                {
                    isFoundValue = TRUE;
                } 
                else if ([str2 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
                {
                    isFoundValue = TRUE;
                } 
                else if ([str3 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) 
                {
                    isFoundValue = TRUE;
                } 
                else if ([str4 rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound)
                {
                    isFoundValue = TRUE;
                }
                
                if(isFoundValue)
                {
                    [tempSearch addObject:temp];
                }
            }
        
    }

    
    else
    {
        if (segment.selectedSegmentIndex == 2)
        {
            for(int i=0;i<[appDelegate.incomeExpenseArray count];i++)
            {
                NSDictionary *temp=[appDelegate.incomeExpenseArray objectAtIndex:i];
                NSString *str=[temp valueForKey:@"property"];
                NSRange titleResultsRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (titleResultsRange.length > 0)
                {
                    [tempSearch addObject:temp];
                }
            }
        }
        else if (segment.selectedSegmentIndex == 1)
        {
            for(int i=0;i<[expenseArray count];i++)
            {
                NSDictionary *temp=[expenseArray objectAtIndex:i];
                NSString *str=[temp valueForKey:@"property"];
                NSRange titleResultsRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (titleResultsRange.length > 0)
                {
                    [tempSearch addObject:temp];
                }
            }
        }
        else if (segment.selectedSegmentIndex == 0)
        {
            for(int i=0;i<[incomeArray count];i++)
            {
                NSDictionary *temp=[incomeArray objectAtIndex:i];
                NSString *str=[temp valueForKey:@"property"];
                NSRange titleResultsRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (titleResultsRange.length > 0)
                {
                    [tempSearch addObject:temp];
                }
            }
    
    
}
    }
    
    
    
}

- (void)doneSearching_Clicked 
{
	
    [searchSegment removeFromSuperview];
	if([searchBar.text length]>0)
    {
        searchBar.text = @"";
    }
    
	[searchBar resignFirstResponder];
	letUserSelectRow = YES;
	searching = NO;
	
    
    /*
    UIButton * add_button = [UIButton buttonWithType:UIButtonTypeCustom];
    [add_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    add_button.bounds = CGRectMake(0, 0,34.0,30.0);
    [add_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_add_button.png"] forState:UIControlStateNormal];
    [add_button addTarget:self action:@selector(add_clicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:add_button] autorelease];
*/
    
    /*
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add_clicked)];
	
    */
    
   
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    
   
    
    tblView.scrollEnabled = YES;
	[tblView setEditing:FALSE animated:TRUE];
	[tblView reloadData];
}




#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class])); 
    [super didReceiveMemoryWarning];
    
   
}

- (void)viewDidUnload 
{
    
}


- (void)dealloc 
{
    nslog(@"\n dealloc in IncomeExpense...");

    
    [incomeArray release];
    [expenseArray release];
    [tempSearch release];
    [tblView release];
    [segment release];
    [incomeExpenseFilterSegmentControl release];
    [searchBar release];
    
    
    [super dealloc];
    
}


@end

