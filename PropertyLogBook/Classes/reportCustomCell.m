//
//  reportCustomCell.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 9/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "reportCustomCell.h"


@implementation reportCustomCell
@synthesize pdfButton, propertyLbl, cellImage,detailLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (self) 
    {
        // Initialization code
               
        
        propertyLbl = [[UILabel alloc]init];
        //lbl.text = @"Select Mesurement:";
        propertyLbl.numberOfLines = 0;
        
        propertyLbl.lineBreakMode = UILineBreakModeTailTruncation;
        propertyLbl.textAlignment = UITextAlignmentLeft;
        //[propertyLbl setFont:[UIFont boldSystemFontOfSize:15.0]];
        [propertyLbl setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:propertyLbl];
        
        [propertyLbl release];
        
        
        
 
        



        pdfButton = [[UIButton buttonWithType:UIButtonTypeCustom]retain];
		[pdfButton setTitle:@"pdf" forState:UIControlStateNormal];
        [pdfButton setImage:[UIImage imageNamed:@"PDF-icon3.png"] forState:UIControlStateNormal];
		[pdfButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [pdfButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        //        [agentDateButton.titleLabel setContentMode:UIViewContentModeLeft];
        //        agentDateButton.titleLabel.textAlignment = UITextAlignmentLeft;
		[self.contentView addSubview:pdfButton];
               
    }
    return self;
}

-(void)layoutSubviews
{
    

   
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            propertyLbl.frame = CGRectMake(10, 0, 590, 30);
            pdfButton.frame = CGRectMake(630,7,30, 30);

        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            propertyLbl.frame = CGRectMake(10, 0, 830, 30);
            pdfButton.frame = CGRectMake(850,7,30, 30);

        }
        

    }
    else
    {
        //cellImage.frame = CGRectMake(10, 10, 30, 30);
        propertyLbl.frame = CGRectMake(10, 0, 240, 30);
        //detailLabel.frame = CGRectMake(10, 20, 300, 30);
        pdfButton.frame = CGRectMake(260,7,30, 30);
     
    }
    [super layoutSubviews];
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
