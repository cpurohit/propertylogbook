//
//  AddPropertyViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddPropertyViewController.h"
#import "CustomCell.h"
#import "CustomCell1.h"
#import "CustomCell2.h"
#import "propertyTypeViewController.h"
#import "currencyViewController.h"
#import "propertyTypeSelectViewController.h"

@implementation AddPropertyViewController

//- (id)initWithStyle:(UITableViewStyle)style
//{
//    self = [super initWithStyle:style];
//    
//    if (self) 
//    {
//        // Custom initialization
//    }
//    return self;
//}

- (void)dealloc
{
    [super dealloc];
    
    [propertyInfoArray release];
    [agentInfoArray release];
    [financialArray release];
    [AddPropertyArray release];
    [datePicker release];
    [financialInfoDictionary release];
    [agenInfoDictionary release];
    [propertyInfoDictionary release];
    [pickerArray release];
    [tblView release];
    
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    //nslog(@"\n this is workspace...");
    
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    /*Chirag:Starts:*/
    agenInfoDictionary = [[NSMutableDictionary alloc] init];
    financialInfoDictionary = [[NSMutableDictionary alloc] init];
    propertyInfoDictionary = [[NSMutableDictionary alloc]init];
    
    /*Chirag:Ends:*/    
    
    self.navigationItem.title = @"ADD PROPERTY";
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel_clicked)] autorelease];
    
    /*
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save_clicked)];
     */
    
    
    
    UIButton * save_button = [UIButton buttonWithType:UIButtonTypeCustom];
    [save_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    save_button.bounds = CGRectMake(0, 0,51.0,30.0);
    [save_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_save_button.png"] forState:UIControlStateNormal];
    [save_button addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:save_button] autorelease];
    
    propertyInfoArray = [[NSMutableArray alloc]initWithObjects:@"Name",@"Address",@"City",@"State",@"Country",@"Property Type",@"Currency",@"Managed By Agent", nil];
    

    agentInfoArray = [[NSMutableArray alloc]initWithObjects:@"Agency Name",@"Contact Person",@"Address",@"City",@"State",@"Country",@"Work Phone",@"Mobile",@"Email",@"Agent Comission %",@"On Lease",@"Lease Start Date",@"Lease End Date", nil];
    
    financialArray = [[NSMutableArray alloc]initWithObjects:@"Purchase Price",@"Estimated Price",@"Fully Paid",@"Mortgage Bank",@"Repayment Amount",@"Repayment Freq.", nil];
    
    AddPropertyArray = [[NSMutableArray alloc]init];
    
    
    
	
    if([appDelegate.selectPropertyType count]==0)
    {
         [appDelegate selectPropertyType];
    }
   
    
    
    //agentSwitch = (UISwitch *)[tblView viewWithTag:50];
    
    CustomCell *custom = [[CustomCell alloc]init];
    agentSwitch = custom.agentSwitch;
//    agentSwitch = (UISwitch *)[custom viewWithTag:50];
    
    /**
    CustomCell1 *custom1 = [[CustomCell1 alloc]init];
   */
    
    CustomCell2 *custom2 = [[CustomCell2 alloc]init];
    fullyPaid = custom2.financialSwitch;
    
    if (leaseSwitch.on)
    {
        isLeaseOn = TRUE;
    }
    else
    {
        isLeaseOn = FALSE;
    }
    [agenInfoDictionary setObject:@"" forKey:@"leasestart"];
    [agenInfoDictionary setObject:@"" forKey:@"leaseend"];
    
    if (fullyPaid.on)
    {
        isFullyPaid = TRUE;
    }
    else
    {
        isFullyPaid = FALSE;
    }
    
    [financialInfoDictionary setObject:[NSNumber numberWithBool:isFullyPaid] forKey:@"fullypaid"];
    
    
    if (agentSwitch.on)
    {
        isAgent = TRUE;
    }
    else
    {
        isAgent = FALSE;
    }
    
    pickerArray = [[NSMutableArray alloc]initWithObjects:@"Weekly",@"Bi Weekly",@"Monthly",@"Yearly", nil];
    
    if ([appDelegate.propertyTypeArray count]>0)
    {
    appDelegate.propertyTypeStr = [[appDelegate.propertyTypeArray objectAtIndex:0]valueForKey:@"propertyType"];
    }
    else
    {
        appDelegate.propertyTypeStr = @"Property Type";
    }
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if (appDelegate.isProperty >=0)
    {
        
        for (int i = 0;i <15;i++)
        {
            [propertyInfoDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",i]];
        }
        for (int i=0;i<15;i++)
        {
            [agenInfoDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",i]];
        }
        for (int i=0;i<15;i++)
        {
            [financialInfoDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",i]];
        }
        [financialInfoDictionary setObject:@"Weekly" forKey:@"repayment"];

        
        
        
        // property info dictinary;
        [propertyInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"23"] forKey:@"0"];
        [propertyInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"16"] forKey:@"1"];
        [propertyInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"18"] forKey:@"2"];        
        [propertyInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"21"] forKey:@"3"];
        [propertyInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"19"] forKey:@"4"];
        appDelegate.currencyTypeStr = [[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"20"];
       
        appDelegate.propertyTypeStr = [[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"22"];
        isAgent = [[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"17"]boolValue];
        
        //  agent info dictionary
        
        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"7"] forKey:@"0"];
        
        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"8"] forKey:@"1"];
        
        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"0"] forKey:@"2"];
        
        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"1"] forKey:@"3"];
        
        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"11"] forKey:@"4"];
        
        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"3"] forKey:@"5"];
        
        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"10"] forKey:@"6"];
        
        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"6"] forKey:@"7"];
        
        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"4"] forKey:@"8"];
        
        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"2"] forKey:@"9"];
        
        isLeaseOn = [[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"5"]boolValue];
        
        [agenInfoDictionary setObject:[NSNumber numberWithBool:isLeaseOn] forKey:@"leaseswitch"];

        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"14"] forKey:@"leasestart"];
        [agenInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"13"] forKey:@"leaseend"];

        //nslog(@"agentinfo======%@",agenInfoDictionary);
        //financial info array
        
        [financialInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"24"] forKey:@"0"];

        [financialInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"11"] forKey:@"1"];
        
        [financialInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"15"] forKey:@"3"];
        
        [financialInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"25"] forKey:@"4"];
        
        isFullyPaid = [[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"12"]boolValue];

        [financialInfoDictionary setObject:[NSNumber numberWithBool:isFullyPaid] forKey:@"fullypaid"];
        
        [financialInfoDictionary setObject:[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"26"] forKey:@"repayment"];
        
    }
    else
    {
        for (int i = 0;i <5;i++)
        {
            [propertyInfoDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",i]];
        }
        for (int i=0;i<10;i++)
        {
            [agenInfoDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",i]];
        }
        for (int i=0;i<8;i++)
        {
            [financialInfoDictionary setObject:@"" forKey:[NSString stringWithFormat:@"%d",i]];
        }
    }
	tblView.backgroundColor = [UIColor clearColor];
	
    
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];

    datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 200, 320,216 )];
    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0, 1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
	
}



-(void)save_clicked
{
    
    
    //nslog(@"proprtyinfo dictionary ===== %@",propertyInfoDictionary);
    //nslog(@"agentinfodict ===== %@",agenInfoDictionary);
    //nslog(@"financialinfodict ======= %@",financialInfoDictionary);
    
    
    
    [AddPropertyArray removeAllObjects];
    
    ///// property info
    
    for (int i = 0; i <5; i++)
    {
        [AddPropertyArray addObject:[propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",i]]];
    }

    
    [AddPropertyArray addObject:appDelegate.propertyTypeStr];
    [AddPropertyArray addObject:appDelegate.currencyTypeStr];
    
    [AddPropertyArray addObject:[NSNumber numberWithBool:isAgent]];

    /// agent info
    
    for (int i =0; i<10; i++)
    {
        [AddPropertyArray addObject:[agenInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",i]]];
    }
    
    [AddPropertyArray addObject:[NSNumber numberWithBool:isLeaseOn]];
    [AddPropertyArray addObject:[agenInfoDictionary objectForKey:@"leasestart"]];    
    [AddPropertyArray addObject:[agenInfoDictionary objectForKey:@"leaseend"]];
    
       
    //// finanicial info
    
    for (int i =0;i<2;i++)
    {
        [AddPropertyArray addObject:[financialInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",i]]];
    }
  
    [AddPropertyArray addObject:[financialInfoDictionary objectForKey:@"fullypaid"]];
    for (int i =3;i<5;i++)
    {
        [AddPropertyArray addObject:[financialInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",i]]];
    }
    
    
    if ([[financialInfoDictionary objectForKey:@"repayment"] length]>0)
    {
    [AddPropertyArray addObject:[financialInfoDictionary objectForKey:@"repayment"]];    
    }
    else
    {
    [AddPropertyArray addObject:@""];
    }

    
    //nslog(@"add priperty array ==== %@",AddPropertyArray);
    
    if (appDelegate.isProperty < 0)
    {
        [appDelegate addProperty:AddPropertyArray];
        
    }
    else
    {
        
        [appDelegate updateProperty:[[[appDelegate.propertyListArray objectAtIndex:appDelegate.isProperty]valueForKey:@"27"]intValue] proArray:AddPropertyArray];
        
        
    }
     
    [self.navigationController popViewControllerAnimated:YES];

    
}
 
 

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    
    //[self.view addSubview:appDelegate.bottomView];
    
    
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    if(appDelegate.isIPad)
    {

    
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
    [tblView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)cancel_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
    
     [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark - 
#pragma mark pickerview data

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{	
	return 1;
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [pickerArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	return [pickerArray objectAtIndex:row];
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    UIButton *btn = (UIButton *)[self.view viewWithTag:102];
    [btn setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
    repayment = btn.titleLabel.text;
    [financialInfoDictionary setObject:repayment forKey:@"repayment"];
    [repayment retain];
}
#pragma mark -
#pragma mark date button methods

-(IBAction)btn_clicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:btn.tag-90 inSection:1];
	[tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    isStart = btn.tag;
    [datePicker addTarget:self action:@selector(date_changed:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:datePicker];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 156, 320, 44)];
    
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    
    /*
    UIBarButtonItem *doneButton = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done_clicked)] autorelease];
    */
    
    
    
    [toolBar setItems:[NSArray arrayWithObjects:doneButton, nil]];
    [doneButton release];
    
    toolBar.barStyle = UIBarStyleBlack;
    [self.view addSubview:toolBar];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
	df.dateStyle = NSDateFormatterMediumStyle;
   
    
    /*
    [btn setTitle:[NSString stringWithFormat:@"  %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
     */
    [btn setTitle:[NSString stringWithFormat:@"  %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
    
    if (btn.tag == 100)
    {
        //leaseStart = btn.titleLabel.text;
        leaseStart = [appDelegate fetchDateInCommonFormate:btn.titleLabel.text];
        [agenInfoDictionary setObject:leaseStart forKey:@"leasestart"];
        [leaseStart retain];
    }
    else if (btn.tag == 101)
    {
        //leaseEnd = btn.titleLabel.text;
        
        leaseEnd = [appDelegate fetchDateInCommonFormate:btn.titleLabel.text];
        [agenInfoDictionary setObject:leaseEnd forKey:@"leaseend"];

        [leaseEnd retain];
    }
    [df release];
}

-(void)done_clicked
{
    
    if(!appDelegate.isIPad)
    {
        tblView.frame = CGRectMake(0, 0, 320, tblView.frame.size.height);
    }
    
    
    
    [datePicker removeFromSuperview];
    [toolBar removeFromSuperview];
    [pickerView removeFromSuperview];
    
}

-(void)date_changed:(id)sender
{
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
	df.dateStyle = NSDateFormatterMediumStyle;
    
    if (isStart == 100)
    {
        UIButton *btn = (UIButton *)[self.view viewWithTag:100];
    
        /*
        [btn setTitle:[NSString stringWithFormat:@"  %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
         */
        
        [btn setTitle:[NSString stringWithFormat:@"  %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
        
        
        leaseStart = btn.titleLabel.text;
        [agenInfoDictionary setObject:leaseStart forKey:@"leasestart"];

        [leaseStart retain];
    }
    else if (isStart == 101)
    {
        UIButton *btn = (UIButton *)[self.view viewWithTag:101];
        
        /*
        [btn setTitle:[NSString stringWithFormat:@"  %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
        */
        [btn setTitle:[NSString stringWithFormat:@"  %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
        
        
        leaseEnd = btn.titleLabel.text;
        [agenInfoDictionary setObject:leaseEnd forKey:@"leaseend"];

        [leaseEnd retain];
    }
    [df release];
}


-(void)agentSwitch_changed:(id)sender
{
    agentSwitch = (UISwitch *)sender;
    if (agentSwitch.on)
    {
        isAgent = TRUE;
    }
    else
    {
        isAgent = FALSE;
    }
    [tblView reloadData];
}

-(void)leaseSwitch_chaged:(id)sender
{
    leaseSwitch = (UISwitch *)sender;
//    leaseSwitch = (UISwitch *)[tblView viewWithTag:51];
    if (leaseSwitch.on)
    {
        isLeaseOn = TRUE;
    }
    else
    {
        isLeaseOn = FALSE;
    }
    [agenInfoDictionary setObject:[NSNumber numberWithBool:isLeaseOn] forKey:@"leaseswitch"];
    [tblView reloadData];
}


-(void)fullyPaidSwitch_changed:(id)sender
{
    fullyPaid = (UISwitch *)sender;
    
    if (fullyPaid.on)
    {
        isFullyPaid = TRUE;
    }
    else
    {
        isFullyPaid = FALSE;
    }
    [financialInfoDictionary setObject:[NSNumber numberWithBool:isFullyPaid] forKey:@"fullypaid"];
    [tblView reloadData];
}
-(void)repaymentButton_clicked
{
    tblView.frame = CGRectMake(0, -200, 320, tblView.frame.size.height);
 
    pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 200, 320, 216)];
    pickerView.dataSource = self;
    pickerView.delegate = self;
    [pickerView setShowsSelectionIndicator:YES];
//    [pickerView showsSelectionIndicator];
    [self.view addSubview:pickerView];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 156, 320, 44)];
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    
    /*
    UIBarButtonItem *doneButton = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done_clicked)] autorelease];
    */
    
    
    
    [toolBar setItems:[NSArray arrayWithObjects:doneButton, nil]];
    [doneButton release];
    toolBar.barStyle = UIBarStyleBlack;
    [self.view addSubview:toolBar];

}
#pragma mark -
#pragma mark text field methds

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if (textField.tag < 1999)
    {
    NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:textField.tag%1000 inSection:0];
	[tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag > 1999 && textField.tag < 2999)
    {
        NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:textField.tag%2000 inSection:1];
        [tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else
    {
        NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:textField.tag%3000 inSection:2];
        [tblView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    toolBar.hidden = FALSE;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    //nslog(@"\n textFieldShouldEndEditing");
    
    if (textField.tag >=1000 && textField.tag < 1999)
    {
        [propertyInfoDictionary setObject:textField.text forKey:[NSString stringWithFormat:@"%d",(textField.tag-1000)]];
    }
    
    if(textField.tag >=2000 && textField.tag < 2999)
    {
        [agenInfoDictionary setObject:textField.text forKey:[NSString stringWithFormat:@"%d",(textField.tag -2000)]];
    }
    
    if(textField.tag >=3000)
    {
        [financialInfoDictionary setObject:textField.text forKey:[NSString stringWithFormat:@"%d",(textField.tag -3000)]];
        
    }
    
    if(appDelegate.isIPad)
    {
        toolBar.hidden = TRUE;
        [textField resignFirstResponder];
    }

    
       return YES;
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //nslog(@"\n textFieldShouldReturn");  
    
    if (textField.tag >999 && textField.tag < 1004)
    {
        UITextField *txt = (UITextField *)[self.view viewWithTag:textField.tag+1];
        [txt becomeFirstResponder];
    }
    else if (textField.tag >1999 && textField.tag <2008)
    {
        UITextField *txt = (UITextField *)[self.view viewWithTag:textField.tag+1];
        [txt becomeFirstResponder];
    }
    else if (textField.tag == 3000)
    {
        UITextField *txt = (UITextField *)[self.view viewWithTag:textField.tag+1];
        [txt becomeFirstResponder];
    }
    else if (textField.tag == 3004)
    {
        UITextField *txt = (UITextField *)[self.view viewWithTag:textField.tag+1];
        [txt becomeFirstResponder];
    }
     
     
    [textField resignFirstResponder];
    return YES;
}
 
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
	return 44;
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	
	NSString *sectionHeader = nil;
	
	if(section == 0) 
    {
		sectionHeader = @"Property Information";
	}
	else if(section == 1) 
    {
        if (agentSwitch.on)
        {
            sectionHeader = @"Agent Information";
        }
        else
        {
		sectionHeader = nil;
        }
	}
    else
    {
        sectionHeader = @"Financials";
    }
    
	
	
	return sectionHeader;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    if (section == 0)
    {
        return [propertyInfoArray count];
    }
    else if (section == 1)
    {
        if (agentSwitch.on)
        {
            if (isLeaseOn)
            {
            return [agentInfoArray count];
            }
            else
            {
                return [agentInfoArray count]-2;
            }
        }
        else
        {
        return 0;
        }
    }
    else if (section == 2)
    {
        if (!isFullyPaid)
        {
           return [financialArray count];     
        }
        
        else
        {
            return [financialArray count]-3;
        }
            
        
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        static NSString *CellIdentifier = @"Property";

        CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

        if (cell == nil) 
        {
            cell = [[[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

        }
        cell.agentSwitch.tag = 50;
        [cell.agentSwitch addTarget:self action:@selector(agentSwitch_changed:) forControlEvents:UIControlEventValueChanged];
        cell.agentSwitch.hidden = TRUE;
        cell.detailLabel.hidden = TRUE;
        //cell.detailLabel.tag = 400+indexPath.row;

        cell.propertyLabel.text = [propertyInfoArray objectAtIndex:indexPath.row];
        cell.PropertyTextField.tag = 1000+indexPath.row;
        cell.PropertyTextField.delegate = self;


        cell.PropertyTextField.hidden = FALSE;

        cell.PropertyTextField.text= [propertyInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]];



        if (indexPath.row == 7)
        {
            cell.PropertyTextField.hidden = TRUE;
            cell.agentSwitch.hidden = FALSE;
        }
        else if (indexPath.row == 5)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.PropertyTextField.hidden = TRUE;
            cell.detailLabel.hidden = FALSE;
            if ([appDelegate.propertyTypeArray count]>0)
            {
                cell.detailLabel.textAlignment = UITextAlignmentCenter;
                cell.detailLabel.text = appDelegate.propertyTypeStr;
            }
            else 
            {
                cell.detailLabel.text = @"Property Type";
            }
        }
        else if (indexPath.row == 6)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.PropertyTextField.hidden = TRUE;
            cell.detailLabel.hidden = FALSE;
            cell.detailLabel.text = appDelegate.currencyTypeStr;
            cell.detailLabel.textAlignment = UITextAlignmentCenter;

        }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;    
    return cell;
    }
    else if (indexPath.section == 1)
    {
        
        //nslog(@"\n cellForRowAtIndexPath = %d ",indexPath.row);
        
        static NSString *CellIdentifier1 = @"Agent";
        CustomCell1 *cell1;// = (CustomCell1 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        if(1) 
        
        {
            cell1 = [[[CustomCell1 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
            cell1.selectionStyle = UITableViewCellSelectionStyleNone;

        }

        
      
//        cell1.leaseSwitch.tag = 51;
//        [cell1.leaseSwitch addTarget:self action:@selector(leaseSwitch_chaged:) forControlEvents:UIControlEventValueChanged];
//        cell1.leaseSwitch.hidden = TRUE;

        cell1.agentTextField.delegate = self;

        cell1.agentTextField.tag= 2000+indexPath.row;
        cell1.agentTextField.hidden = FALSE;
        cell1.agentDateButton.hidden = TRUE;
        
        
        
        cell1.agentTextField.text= [agenInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]];
    //    cell1.leaseSwitch.on = [[agenInfoDictionary valueForKey:@"leaseswitch"]boolValue];
        
        
        if (isLeaseOn)
        {
            cell1.agentLabel.text = [agentInfoArray objectAtIndex:indexPath.row];
            
            if (indexPath.row == 9)
            {
                CGSize sizeToMakeLabel = [cell1.agentLabel.text sizeWithFont: [UIFont fontWithName:@"HelveticaNeue" size:17] constrainedToSize:CGSizeMake(130,1000) lineBreakMode:UILineBreakModeWordWrap];
                [cell1.agentLabel setFrame:CGRectMake(20,7,130, sizeToMakeLabel.height)];

            }
        
        if (indexPath.row == 10)
        {
            cell1.agentTextField.hidden = TRUE;
         //   cell1.leaseSwitch.hidden = FALSE;
        }
        else if (indexPath.row == 11)
        {
            cell1.agentDateButton.tag = 100;
            [cell1.agentDateButton setTitle:[agenInfoDictionary objectForKey:@"leasestart"] forState:UIControlStateNormal];
            cell1.agentTextField.hidden = TRUE;
          //  cell1.leaseSwitch.hidden = TRUE;
            cell1.agentDateButton.hidden = FALSE;
        }
       else if(indexPath.row == 12)
       {
           cell1.agentDateButton.tag = 101;
           [cell1.agentDateButton setTitle:[agenInfoDictionary objectForKey:@"leaseend"] forState:UIControlStateNormal];

           cell1.agentTextField.hidden = TRUE;
          // cell1.leaseSwitch.hidden = TRUE;
           cell1.agentDateButton.hidden = FALSE;
       }
        
        }
        else if (!isLeaseOn)
        {
            cell1.agentDateButton.hidden = TRUE;
           // cell1.leaseSwitch.hidden = TRUE;
            
            
            if (indexPath.row <10)
            {
                cell1.agentLabel.text = [agentInfoArray objectAtIndex:indexPath.row];
                if (indexPath.row == 9)
                {
                    CGSize sizeToMakeLabel = [cell1.agentLabel.text sizeWithFont: [UIFont fontWithName:@"HelveticaNeue" size:17] constrainedToSize:CGSizeMake(130,1000) lineBreakMode:UILineBreakModeWordWrap];
                    [cell1.agentLabel setFrame:CGRectMake(20,7,130, sizeToMakeLabel.height)];
                    
                }
            }
            else if (indexPath.row == 10)
            {
                cell1.agentLabel.text = [agentInfoArray objectAtIndex:indexPath.row];
                cell1.agentTextField.hidden = TRUE;
             //   cell1.leaseSwitch.hidden = FALSE;
            }
            
        }
        
        [cell1.agentDateButton addTarget:self action:@selector(btn_clicked:) forControlEvents:UIControlEventTouchUpInside];
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell1;
    }
    else if (indexPath.section == 2)
    {
        static NSString *CellIdentifier2 = @"Financials";
        
        CustomCell2 *cell2;// = (CustomCell2 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        //if (cell2 == nil) 
        if (1) 
        {
            cell2 = [[[CustomCell2 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            cell2.selectionStyle = UITableViewCellSelectionStyleNone;

        }
             
        cell2.financialTextField.delegate = self;
        cell2.financialSwitch.tag = 52;
        [cell2.financialSwitch addTarget:self action:@selector(fullyPaidSwitch_changed:) forControlEvents:UIControlEventTouchUpInside];
        cell2.financialTextField.tag = 3000+indexPath.row;
        cell2.financialSwitch.hidden = TRUE;
        cell2.repaymentButton.hidden = TRUE;
        cell2.repaymentButton.tag = 102;
        cell2.financialTextField.hidden = FALSE;
        [cell2.repaymentButton addTarget:self action:@selector(repaymentButton_clicked) forControlEvents:UIControlEventTouchUpInside];
        
        cell2.financialTextField.text= [financialInfoDictionary objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]];
        cell2.financialSwitch.on = [[financialInfoDictionary valueForKey:@"fullypaid"]boolValue];
        
        if (indexPath.row == 2)
        {
            cell2.financialSwitch.hidden = FALSE;
            cell2.financialTextField.hidden = TRUE;
        }
        if (!isFullyPaid)
        {
        if (indexPath.row == 5)
        {
            cell2.repaymentButton.hidden = FALSE;
            [cell2.repaymentButton setTitle:[financialInfoDictionary objectForKey:@"repayment"] forState:UIControlStateNormal];
            cell2.financialTextField.hidden = TRUE;
            cell2.financialSwitch.hidden = TRUE;
        }
        }
        // Configure the cell...
        
        cell2.financialLabel.text = [financialArray objectAtIndex:indexPath.row];
        CGSize sizeToMakeLabel = [cell2.financialLabel.text sizeWithFont: [UIFont fontWithName:@"HelveticaNeue" size:17] constrainedToSize:CGSizeMake(130,1000) lineBreakMode:UILineBreakModeWordWrap];
        [cell2.financialLabel setFrame:CGRectMake(20,7,130, sizeToMakeLabel.height)];
cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell2;
    }
    return nil;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 5)
        {
         
            propertyTypeViewController *propertyTypeView = [[propertyTypeViewController alloc]initWithNibName:@"propertyTypeViewController" bundle:nil];
            appDelegate.fromProperty = TRUE;
            [self.navigationController pushViewController:propertyTypeView animated:YES];
            [propertyTypeView release];
        }
        else if (indexPath.row == 6)
        {
            currencyViewController *currecyView = [[currencyViewController alloc]initWithNibName:@"currencyViewController" bundle:nil];
            [self.navigationController pushViewController:currecyView animated:YES];
            [currecyView release];
        }
    }
}

@end
