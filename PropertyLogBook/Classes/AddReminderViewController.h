//
//  AddReminderViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"


@interface AddReminderViewController : UIViewController<UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource> 
{
	
	IBOutlet UITableView *tblView;
	NSMutableArray *reminderMainArray;
	NSMutableArray *reminderDetailArray;
    UIDatePicker *datePicker;
    UIToolbar *toolBar;
    int ButtonTag;
    PropertyLogBookAppDelegate *appDelegate;
    NSMutableDictionary *dict;
    UIPickerView *pickerView;
    NSMutableArray *pickerArray;
    
    int reminderViewIndex;
    
    
    IBOutlet UIView*section_header_view;
    
    
    IBOutlet UIButton* section_date_button;
    IBOutlet UIButton* section_time_button;
    
    IBOutlet UIImageView*reminder_section_imageView;
    
    

}

-(void)done_clicked;
- (IBAction) scheduleAlarm;
-(IBAction)btn_clicked:(id)sender;
-(IBAction)time_clicked:(id)sender;
-(void) cancelAlarm;
@property (nonatomic, readwrite)int reminderViewIndex;

@end
