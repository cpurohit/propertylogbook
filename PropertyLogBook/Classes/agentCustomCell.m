//
//  agentCustomCell.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "agentCustomCell.h"
#import <QuartzCore/QuartzCore.h>


@implementation agentCustomCell
@synthesize agentLabel, agentTextField;
@synthesize AddressTextView;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (self) 
    {
        // Initialization code
        agentLabel = [[UILabel alloc]init];
        agentLabel.numberOfLines = 0;
        [agentLabel setTextColor:[UIColor blackColor]];
        agentLabel.lineBreakMode = UILineBreakModeWordWrap;

        [agentLabel setFont:[UIFont systemFontOfSize:15.0]];
        
        [agentLabel setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:agentLabel];
        
        [agentLabel release];
        agentTextField = [[UITextField alloc]init];
        agentTextField.borderStyle = UITextBorderStyleNone;
        [agentTextField setTextAlignment:UITextAlignmentRight];
        [agentTextField setFont:[UIFont systemFontOfSize:15.0]];
        [agentTextField setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
        [self.contentView addSubview:agentTextField];
		
		AddressTextView = [[UITextView alloc]init];
		[AddressTextView setFont:[UIFont systemFontOfSize:15.0]];
		[[AddressTextView layer] setCornerRadius:2.0];
		[[AddressTextView layer] setBorderColor:[UIColor lightGrayColor].CGColor];
		[[AddressTextView layer] setBorderWidth:1.0];
        [AddressTextView setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
        
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                agentTextField.frame = CGRectMake(170,10,500, 30);
            }
            
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                agentTextField.frame = CGRectMake(170, 10,750,30);
            }
            
            
            if(appDelegate.isIOS7)
            {
                agentTextField.frame = CGRectMake(agentTextField.frame.origin.x+90,agentTextField.frame.origin.y,agentTextField.frame.size.width, agentTextField.frame.size.height);
            }
            
            
            
            
        }
        else
        {
            agentTextField.frame = CGRectMake(165,10,130, 30);
        }
        
        
        
        agentLabel.frame = CGRectMake(40,7, 165, 30);
        //AddressTextView.frame = CGRectMake(150, 7, 140, 100);
        
        
    }
    return self;
}


-(void)layoutSubviews
{
	[super layoutSubviews];

   

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
