//
//  IncomeExpenseViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

enum
{
    CELL_LABEL_START_TAG = 4000,
    CELL_DATE_LABEL_START_TAG = 4500,
    CELL_IMAGE_START_TAG = 5000,
    
    
};

@class AddIncomeExpenseViewController;
@interface IncomeExpenseViewController : UIViewController <UIActionSheetDelegate,UISearchBarDelegate,UIAlertViewDelegate,UITabBarDelegate,UITableViewDataSource>{
	
	IBOutlet UITableView *tblView;
    IBOutlet UIImageView *barImage;

	AddIncomeExpenseViewController *addIncome;
    NSMutableArray *incomeArray, *expenseArray;
    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UILabel *lblNoData;
    IBOutlet UISegmentedControl *segment;
    IBOutlet UISearchBar *searchBar;
    IBOutlet UISegmentedControl *searchSegment;

    IBOutlet UIButton *cancelButton;
    BOOL searching;
    
    IBOutlet UIImageView *incomeImage, *expenseImage;
    IBOutlet UILabel *plusLabel;

    BOOL letUserSelectRow;
    NSMutableArray *tempSearch;
    
    UILabel *label, *detailLabel, *amountLabel, *dateLable;
    IBOutlet UIImageView *barImageIpad;
    IBOutlet UIImageView *blackBar;
    
    NSMutableArray*temporaryIncomeArray;
    NSMutableArray*temporaryExpenseArray; 
    
    IBOutlet UISegmentedControl *incomeExpenseFilterSegmentControl;
    BOOL isViewWillAppearAfterDeleting;
    
    
    int selectedIndexpahRow;
    
    IBOutlet UIView*incomeExpenseTransctionSortRangeView;
    
    IBOutlet UIButton*mtdButton;
    IBOutlet UIButton*ytdButton;
    IBOutlet UIButton*allButton;
    int selectedSpan;//0 mtd,1 ytd 2 all
    
    
    IBOutlet UIView*incomeExpenseTypeVIew;
    IBOutlet UIButton*incomeButton;
    IBOutlet UIButton*expenseButton;
    IBOutlet UIButton*bothButton;
    
    NSNumberFormatter *numberFormatter;
    
    
}
-(IBAction)cancelSearch_clicked;
- (void) searchCat;
- (void) doneSearching_Clicked;
-(IBAction)segment_changed;

-(IBAction)searchSegment_changed;
-(IBAction)incomeExpenseFilterSegmentControl_Changed:(id)sender;

-(IBAction)incomeExpenseFilterSegmentControl_Changed_Through_Click:(id)sender;
-(IBAction)incomeExpenseTypeSegmentControl_Changed_Through_Click:(id)sender;

@end
