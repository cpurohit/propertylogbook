//
//  DashBoardViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@class PropertyLogBookAppDelegate;
@interface DashBoardViewController : UIViewController<UITableViewDelegate> 
{
     
    IBOutlet UITableView *tblView;
    PropertyLogBookAppDelegate *appDelegate;
    NSString *rootPath;

    
    
    UIDatePicker *datePicker;
    IBOutlet UIToolbar *toolBar;
    IBOutlet UIButton *fromButton, *toButton;
    int buttontag;
    
    float netIncomeAmount;
    float netExpenseAmount;
    float netMileageAmount;
    
    
    
    NSMutableArray *dashBoardArray;
    
    NSMutableDictionary *dictionary;
    NSDateFormatter *df;
    
    IBOutlet UILabel *lblFrom;
    IBOutlet UILabel *lblTo; 
    IBOutlet UIImageView*imgViewBar;
    
//    IBOutlet UIButton *fromBtn, *toBtn;
    
    BOOL is_done_clicked;
    
    IBOutlet UILabel*noDataLabel;
    IBOutlet UIImageView*no_data_dashboard_listing_imageView;
    
    IBOutlet UIView*welcome_screen_view;
    IBOutlet UIImageView*welcome_image_view;
    IBOutlet UIButton*close_welcome_screen_button;
    
    BOOL is_view_will_appear_called;
    
    IBOutlet UIBarButtonItem *cancelBtn;
    
}
-(IBAction)tool_bar_cancel_pressed:(id)sender;

-(IBAction)dateBtn_clicked:(id)sender;

-(IBAction)done_clicked;

-(IBAction)datePicker_changed:(id)sender;

@end
