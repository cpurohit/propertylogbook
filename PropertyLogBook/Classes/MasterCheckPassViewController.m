//
//  MasterCheckPassViewController.m
//  MyReferences
//
//  Created by Neel  Shah on 02/01/12.
//  Copyright (c) 2012 Sunshine Infotech. All rights reserved.
//

#import "MasterCheckPassViewController.h"
#import "PropertyLogBookAppDelegate.h"


@implementation MasterCheckPassViewController
@synthesize responseDataDictionary;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class])); 
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)viewWillAppear:(BOOL)animated
{
    appDelegate=(PropertyLogBookAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.view setUserInteractionEnabled:TRUE];
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            ActivityView.center = CGPointMake(384,512);
            tblView.frame = CGRectMake(234, 592, 299, 80);
            masterPasswordLockButton.frame = CGRectMake(462, 365, 50, 50);
            masterPasswordBG.image = [UIImage imageNamed:@"master_password_ipad_port.png"];
            masterPasswordBG.frame = CGRectMake(0, 0, 768,1024);
            
            messageLabel.frame = CGRectMake(144, 795, 488, 41);
            forgotPasswordButton.frame = CGRectMake(310, 745, 150, 33);
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            ActivityView.center = CGPointMake(512,384);
            tblView.frame = CGRectMake(376, 370, 299, 80);
            tblView.center = CGPointMake(512,384);
            
            
            
            masterPasswordLockButton.frame = CGRectMake(586,222, 50, 50);
            
            masterPasswordBG.image = [UIImage imageNamed:@"master_password_ipad_landscape.png"];
            masterPasswordBG.frame = CGRectMake(0, 0, 1024,768);
            
            
            
            
            messageLabel.frame = CGRectMake(289, 649, 488, 41);
            messageLabel.center = CGPointMake(512,670);
            
            forgotPasswordButton.frame = CGRectMake(458, 551, 150, 33);
            forgotPasswordButton.center = CGPointMake(512,570);
            
            
        }
        
    }

    
    if(appDelegate.isIphone5)
    {

        
        masterPasswordBG.frame = CGRectMake(0,0,320,548);
        lock_imageView.frame = CGRectMake(lock_imageView.frame.origin.x,lock_imageView.frame.origin.y+10,lock_imageView.frame.size.width, lock_imageView.frame.size.height);
        masterPasswordBG.image = [UIImage imageNamed:@"masterpasswordBG_iphone5.png"];
        
    }
    
    
    [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    [super viewWillAppear:animated];
}
- (void)viewDidLoad
{
   
    tblView.backgroundColor = [UIColor clearColor];
    
    
    
    appDelegate=(PropertyLogBookAppDelegate *)[[UIApplication sharedApplication] delegate];

    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    
    txtPass=[[UITextField  alloc] init];
    if(appDelegate.isIphone5)
    {
        tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y+88,tblView.frame.size.width,tblView.frame.size.height);
        forgot_password_button.frame = CGRectMake(forgot_password_button.frame.origin.x,forgot_password_button.frame.origin.y+88,forgot_password_button.frame.size.width,forgot_password_button.frame.size.height);
        messageLabel.frame= CGRectMake(messageLabel.frame.origin.x,messageLabel.frame.origin.y+88,messageLabel.frame.size.width,messageLabel.frame.size.height);
    }

    
    
    //Create done button
    
    if(appDelegate.isIPad)
    {
        [tblView setBackgroundView:nil];
        [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    }

    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0, 60, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(DoneToolBarKey) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[[UIBarButtonItem alloc] initWithCustomView:btnDone1] autorelease];
    
    
    StringPassword=@"";
    // Do any additional setup after loading the view from its nib.
    
    /*Activity Indicator*/
    
    messageLabel.textColor = [UIColor colorWithRed:112.0f/255.0f green:112.0f/255.0f blue:112.0f/255.0f alpha:1.0];
    messageLabel.text = @"To skip this screen, \n  TURN OFF Password in Settings.";
   
    ActivityView = [[UIView alloc]initWithFrame:CGRectMake(160,240,20,20)];
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            ActivityView.center = CGPointMake(384,512);
            tblView.frame = CGRectMake(234, 592, 299, 80);
            masterPasswordLockButton.frame = CGRectMake(462, 365, 50, 50);
            masterPasswordBG.image = [UIImage imageNamed:@"master_password_ipad_port.png"];
            masterPasswordBG.frame = CGRectMake(0, 0, 776,1004);
            messageLabel.frame = CGRectMake(144, 795, 488, 41);
            forgotPasswordButton.frame = CGRectMake(310, 745, 150, 33);
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            ActivityView.center = CGPointMake(512,384);
            tblView.frame = CGRectMake(376, 370, 299, 80);
            tblView.center = CGPointMake(512,384);
            
            
            
            masterPasswordLockButton.frame = CGRectMake(586,222, 50, 50);
            
            masterPasswordBG.image = [UIImage imageNamed:@"master_password_ipad_landscape.png"];
            masterPasswordBG.frame = CGRectMake(0, 0, 1024,748);
            
            
            
            
            messageLabel.frame = CGRectMake(289, 649, 488, 41);
            messageLabel.center = CGPointMake(512,670);
            
            forgotPasswordButton.frame = CGRectMake(458, 551, 150, 33);
            forgotPasswordButton.center = CGPointMake(512,570);

            
        }

    }
    else 
    {
        ActivityView.center = CGPointMake(160,240);    
    }
    
    
    [ActivityView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.0]];
    ActivityView.hidden=NO;
    
    
    
    mainActivityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0,0,20,20)];
    [mainActivityIndicator setHidesWhenStopped:YES];
    [ActivityView addSubview:mainActivityIndicator];
    mainActivityIndicator.activityIndicatorViewStyle =UIActivityIndicatorViewStyleWhiteLarge;
    [mainActivityIndicator startAnimating];
    
    UILabel *lblLoading =[[UILabel alloc]initWithFrame:CGRectMake(35,10,80,20)];
    [lblLoading setText:@""];
    [lblLoading setTextAlignment:UITextAlignmentLeft];
    [lblLoading setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    lblLoading.font=[UIFont boldSystemFontOfSize:14];
    [lblLoading setTextColor:[UIColor whiteColor]];
    [lblLoading setBackgroundColor:[UIColor clearColor]];
   
    [lblLoading release];
    
    ActivityView.hidden = YES;
    [self.view addSubview:ActivityView];
    [self.view bringSubviewToFront:ActivityView];
    /*Activity Indicator*/

     [super viewDidLoad];
}
-(void)viewDidAppear:(BOOL)animated
{
    /*have to write here..because of ios 6. if we start app in landscape mode.(with password)*/
    [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    [super viewDidAppear:animated];
}
-(NSString *)GetPassWord
{
    prefs=[NSUserDefaults standardUserDefaults];
    NSString *Password=[prefs objectForKey:@"PassWordValue"];
    return Password;
}

-(NSString *)GetPassWordHint
{
    prefs=[NSUserDefaults standardUserDefaults];
    NSString *Password=[prefs objectForKey:@"PassWordValueHint"];
    return Password;
}

-(IBAction)DoneToolBarKey
{
    UITextField *txt=(UITextField *)[self.view viewWithTag:1000];
    
    if ([txt.text isEqualToString:[self GetPassWord]]) 
    {
        /*
        [self removeFromParentViewController];
        appDelegate.window.rootViewController=appDelegate.tabBarController;
       */
       
        
        
        [txt resignFirstResponder];
        [appDelegate.tabBarController.view setUserInteractionEnabled: TRUE];
        
        NSString *reqSysVer = @"5.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        nslog(@"\n currSysVer = %@",currSysVer);
        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        {
            [self.view endEditing:TRUE];
            
            [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(setViewContrller) userInfo:nil repeats:NO];
            
            //appDelegate.window.rootViewController=appDelegate.tabBarController;
        }
        else
        {
            [self.view removeFromSuperview];
            [appDelegate correctPassword];
           
        }

       
        
        
        
    }
    else
    {
        [txtPass resignFirstResponder];
        messageLabel.textColor = [UIColor colorWithRed:212.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0f];
        messageLabel.text = @"You have entered incorrect password.\n Please try again.";
        txtPass.text = @"";
        /*
        UIAlertView *Wrongpass=[[UIAlertView alloc] initWithTitle:@"Wrong Password" message:[NSString stringWithFormat:@"You have entered incorrect password. Please try again."] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [Wrongpass show];
        [Wrongpass release];
         */
    }
    
}

-(IBAction)ForgotPassword
{
    messageLabel.textColor = [UIColor colorWithRed:112.0f/255.0f green:112.0f/255.0f blue:112.0f/255.0f alpha:1.0];
    messageLabel.text=@"Please wait...";
    ActivityView.hidden = FALSE;
    [self.view bringSubviewToFront:ActivityView];
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(mailPassword:) userInfo:nil repeats:NO];
    
    [txtPass resignFirstResponder];
    
}
-(void)mailPassword:(id)sender
{
    requestObjects = [NSArray arrayWithObjects:[self GetPassWordHint],[self GetPassWord],nil];
    requestkeys = [NSArray arrayWithObjects:@"email",@"password",nil];
	requestJSONDict = [NSDictionary dictionaryWithObjects:requestObjects forKeys:requestkeys];
    jsonRequest = [requestJSONDict JSONRepresentation];
    //jsonRequest = [finalJSONDictionary JSONRepresentation];
	
    requestString = [NSString stringWithFormat:@"data=%@",[requestJSONDict JSONRepresentation], nil];  // Convert array to JSON formated string
    
    
    nslog(@"\n requestString == %@",requestString);
    
    requestData = [NSData dataWithBytes: [requestString UTF8String] length: [requestString length]]; // Convert string to data to send to web service
    
    //nslog(@"\n requestData == %@",requestData);
	
    /*
     urlString = [NSString stringWithFormat:@"%@",@"http://localhost:8888/forgotpassword/forgotpassword.php"];
     urlString = [NSString stringWithFormat:@"%@",@"http://www.sunshineinfotech.com/smitnebhwani/forgotpassword/forgotpassword.php"];
     
     */
    
    urlString = [NSString stringWithFormat:@"%@",@"http://www.iapplab.com/forgotp/forgotpassword.php"];
    
    request = [[[NSMutableURLRequest alloc] init] autorelease];   
	[request setURL:[NSURL URLWithString:urlString]];                  // set URL for the request
	[request setHTTPMethod:@"POST"];                                  // set method the request
    [request setHTTPBody:requestData];        
	
	
	returnData = [ NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil ]; 
	NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
	nslog(@"\n returnString == %@",returnString);
	
	json = [[SBJSON new] autorelease];
	
	responseDataDictionary = [json objectWithString:returnString error:&error];
    [responseDataDictionary retain];
    nslog(@"\n responseDataDictionary = %@ ",responseDataDictionary);
    ActivityView.hidden = TRUE;
    //messageLabel.text=[NSString stringWithFormat:@"Your password has been sent on %@",[self GetPassWordHint]];
    
    if([[responseDataDictionary objectForKey:@"message"] isEqualToString:@"SUCCESS"])
    {
        messageLabel.textColor = [UIColor colorWithRed:8.0/255.0f green:148.0/255.0f blue:1.0/255.0f alpha:1.0f];
        messageLabel.text=@"Your password has been sent to your email address. \n It may take upto 5 minutes."; 
    }
    else
    {
        messageLabel.textColor = [UIColor colorWithRed:212.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0f];
        messageLabel.text=@"Message sending failed. \n Please try again later.";  
    }
    
    
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    
    if(!(appDelegate.isIPad))
    {
        
        if(appDelegate.isIphone5)
        {
            [self.view setFrame:CGRectMake(0, 20,320,548)];
        }
        else
        {
            [self.view setFrame:CGRectMake(0, 20,320, 460)];
        }
        
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelegate:self];
        
        
        if(appDelegate.isIphone5)
        {
            [self.view setFrame:CGRectMake(0,  -50,320,548)];
        }
        else
        {
            [self.view setFrame:CGRectMake(0,  -50,320, 460)];
        }

        
       

        [UIView commitAnimations];

    }
    
    
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{   
    if(!appDelegate.isIPad )
    {
        
        if(appDelegate.isIphone5)
        {
            [self.view setFrame:CGRectMake(0,  -50,320,548)];
        }
        else
        {
            [self.view setFrame:CGRectMake(0,  -50,320, 460)];
        }

        
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelegate:self];
         
        if(appDelegate.isIphone5)
        {
            [self.view setFrame:CGRectMake(0,  20,320,548)];
        }
        else
        {
            [self.view setFrame:CGRectMake(0,  20,320, 460)];
        }

        
        
        
        [UIView commitAnimations];
    }
    
    /*
    StringPassword=textField.text;
    UITextField *txt=(UITextField *)[self.view viewWithTag:1000];
    if ([txt.text isEqualToString:[self GetPassWord]])
    {

    
        NSString *reqSysVer = @"5.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        nslog(@"\n currSysVer = %@",currSysVer);
        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        {
            appDelegate.window.rootViewController=appDelegate.tabBarController;
        }
        else
        {
        
        
            [self.view removeFromSuperview];
            [appDelegate correctPassword];
        
        }
    }
     */
    
    
        

    

    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //keytoolBar.hidden = FALSE;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    UITextField *txt=(UITextField *)[self.view viewWithTag:1000];
    
    if ([txt.text isEqualToString:[self GetPassWord]]) 
    {
        //appDelegate.window.rootViewController=appDelegate.tabBarController;
        
        [appDelegate.tabBarController.view setUserInteractionEnabled: TRUE];
        
        
       
        
        
        
        NSString *reqSysVer = @"5.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        nslog(@"\n currSysVer = %@",currSysVer);
        
        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        {
            [self.view endEditing:TRUE];
            //[appDelegate.window setRootViewController:self.tabBarController];
            [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(setViewContrller) userInfo:nil repeats:NO];
            
            
        }
        else
        {
            
            
            [self.view removeFromSuperview];
            [appDelegate correctPassword];
            
        }
         
        
       
        
    }
    else
    {
        [txtPass resignFirstResponder];
        messageLabel.textColor = [UIColor colorWithRed:212.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0f];
        messageLabel.text = @"You have entered incorrect password.\n Please try again.";
        txtPass.text = @"";
    }

    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - setViewContrller
-(void)setViewContrller
{
    appDelegate.window.rootViewController=appDelegate.tabBarController;
}
    


#pragma mark - TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.textLabel.textColor=[UIColor blackColor];
    
    cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:17.0];
    
    cell.textLabel.text=@"Password";
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    txtPass.textAlignment=UITextAlignmentRight;
    txtPass.frame=CGRectMake(122, 10, 148, 30);
    txtPass.tag=1000;

    txtPass.textColor=[UIColor blackColor];
    txtPass.placeholder=@"Password";
    txtPass.delegate=self;
    txtPass.secureTextEntry=YES;
    txtPass.backgroundColor=[UIColor clearColor];
    txtPass.returnKeyType = UIReturnKeyGo;
    [cell.contentView addSubview:txtPass];
    
    
    
    // Configure the cell...
    
    return cell;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    appDelegate=(PropertyLogBookAppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.isIPad)
    {
        return TRUE;
    }
    return FALSE;
    
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            ActivityView.center = CGPointMake(384,512);
            tblView.frame = CGRectMake(234, 592, 299, 80);
            masterPasswordLockButton.frame = CGRectMake(462, 365, 50, 50);
            masterPasswordBG.image = [UIImage imageNamed:@"master_password_ipad_port.png"];
            masterPasswordBG.frame = CGRectMake(0, 20, 776,1004);
            messageLabel.frame = CGRectMake(144, 795, 488, 41);
            forgotPasswordButton.frame = CGRectMake(310, 745, 150, 33);
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            ActivityView.center = CGPointMake(512,384);
            tblView.frame = CGRectMake(376, 370, 299, 80);
            tblView.center = CGPointMake(512,384);
            
            
            
            masterPasswordLockButton.frame = CGRectMake(586,222, 50, 50);
            
            masterPasswordBG.image = [UIImage imageNamed:@"master_password_ipad_landscape.png"];
            masterPasswordBG.frame = CGRectMake(0, 0, 1024,748);
            
            
            
            
            messageLabel.frame = CGRectMake(289, 649, 488, 41);
            messageLabel.center = CGPointMake(512,670);
            
            forgotPasswordButton.frame = CGRectMake(458, 551, 150, 33);
            forgotPasswordButton.center = CGPointMake(512,570);
            
        }
        
    }
}
- (void)dealloc
{
    [super dealloc];
    [ActivityView release];
    [txtPass release];
    //[mainActivityIndicator release];
    
}


@end
