//
//  ReportViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 9/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ReportViewController.h"
#import "PropertyLogBookAppDelegate.h"
#import "reportCustomCell.h"
#import "pdfViewController.h"
@implementation ReportViewController
@synthesize noDataLabel,noDataImageView;


- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class])); 
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    appDelegate =(PropertyLogBookAppDelegate *) [[UIApplication sharedApplication]delegate];
    
    self.navigationItem.title = @"REPORTS";
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
   // [button setTitle:@"  Back" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    button.bounds = CGRectMake(0, 0,49.0,29.0);
    [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
    if(!appDelegate.isIPad)
    {
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
    }

    
    
   
    datePicker.hidden = TRUE;
    toolBar.hidden = TRUE;
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *yearComponent = [gregorian components:NSYearCalendarUnit fromDate:[NSDate date]];
    NSDateComponents *monthComponent = [gregorian components:NSMonthCalendarUnit fromDate:[NSDate date]];
    [yearComponent setMonth:[monthComponent month]];
    [yearComponent setDay:1];
    
    
    NSDate *firstDateOfyear = [gregorian dateFromComponents:yearComponent];
    
    
    NSDate *current = [NSDate date];
    //NSString *currentDate = [df stringFromDate:current];
    
    
    tblView.backgroundColor = [UIColor clearColor];

    
   // NSDate*commonDateFromRegionDate = [regionDateFormatter stringFromDate:current];
    
    
    /*
        Starts:  SUN:0004
        Date:05-03-2013
        For date filter conditions...
     
     */
    
    NSMutableDictionary*data_dictionary = [[[NSMutableDictionary alloc] initWithDictionary:[appDelegate select_default_date_filter]] autorelease];
    
    nslog(@"\n data_dictionary = %@",data_dictionary);

    
    
    if([[data_dictionary objectForKey:@"1000"] length]==0)
    {
        [fromButton setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:firstDateOfyear]] forState:UIControlStateNormal];
    }
    else
    {
        [fromButton setTitle:[NSString stringWithFormat:@" %@",[data_dictionary objectForKey:@"1000"]] forState:UIControlStateNormal];
    }
    
    if([[data_dictionary objectForKey:@"1001"] length]==0)
    {
        [toButton setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:current]] forState:UIControlStateNormal];
    }
    else
    {
        [toButton setTitle:[NSString stringWithFormat:@" %@",[data_dictionary objectForKey:@"1001"]] forState:UIControlStateNormal];
        
    }
    
    
    /*
     Ends: SUN:0004

     Date:05-03-2013
     For date filter conditions...
     
     */

    
    
    
    toButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    fromButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];

    tblView.backgroundColor = [UIColor clearColor];
    
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
    
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0, 1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    [imgView release];
    [gregorian release];

   


}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)cancel_clicked
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [appDelegate selectPropertyDetail];
    noDataLabel.textAlignment =  UITextAlignmentCenter;
    [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation ];
    [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:1.0];
    [tblView reloadData];
    
    if(appDelegate.isIPad)
    {
        
		[self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    	[self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
	}
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
   if(appDelegate.isIPad)
   {
       return YES;
   }
    return NO;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    if(appDelegate.isIPad)
    {
        datePicker.hidden = YES;
        toolBar.hidden = YES;

        
        if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
        {
            imgViewBar.frame = CGRectMake(0, 0,1024, imgViewBar.frame.size.height);
            lblFrom.frame = CGRectMake(170,11,lblFrom.frame.size.width, lblFrom.frame.size.height);
            fromButton.frame = CGRectMake(230,fromButton.frame.origin.y,112, fromButton.frame.size.height);
            
            lblTo.frame = CGRectMake(422,lblTo.frame.origin.y,lblTo.frame.size.width,lblTo.frame.size.height);
            toButton.frame = CGRectMake(460,toButton.frame.origin.y,112, toButton.frame.size.height);
            
            
            noDataImageView.center = CGPointMake(384,(self.view.frame.size.height/2)-50);
            noDataLabel.center = CGPointMake(384,self.view.frame.size.height/2);

            
                       
            
        }
        else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
        {
            imgViewBar.frame = CGRectMake(0, 0,1024, imgViewBar.frame.size.height);
            lblFrom.frame = CGRectMake(286,11,lblFrom.frame.size.width, lblFrom.frame.size.height);
            fromButton.frame = CGRectMake(346,fromButton.frame.origin.y,fromButton.frame.size.width, fromButton.frame.size.height);
            
            lblTo.frame = CGRectMake(560,lblTo.frame.origin.y,lblTo.frame.size.width,lblTo.frame.size.height);
            toButton.frame = CGRectMake(600,toButton.frame.origin.y,toButton.frame.size.width, toButton.frame.size.height);
            
            
            noDataImageView.center = CGPointMake(520,250);
            noDataLabel.center = CGPointMake(520,300);
           

            
        }

    }
     
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //[tblView reloadData];
    
    if([appDelegate.propertyDetailArray count]==0)
    {
        noDataLabel.hidden = FALSE;
        noDataImageView.hidden = FALSE;
        
        if(appDelegate.isIPad)
        {
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                noDataImageView.center = CGPointMake(384,(self.view.frame.size.height/2)-50);
                noDataLabel.center = CGPointMake(384,self.view.frame.size.height/2);
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                noDataImageView.center = CGPointMake(520,(self.view.frame.size.height/2)-50);
                noDataLabel.center = CGPointMake(520,self.view.frame.size.height/2);
                
            }
            
        }
        
    }
    else
    {
        noDataLabel.hidden = YES;
        noDataImageView.hidden = YES;
    }

    
}

-(IBAction)dateBtn_clicked:(id)sender
{
    
    /* original
    NSDate *fromDate = [df dateFromString:[fromButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    
    NSDate *toDate = [df dateFromString:[toButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
 
    */
    NSDate*fromDate = [appDelegate.regionDateFormatter dateFromString:[fromButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    
    
     NSDate*toDate = [appDelegate.regionDateFormatter dateFromString:[toButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    
   

    
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            datePicker.frame =  CGRectMake(0,695, 768,216 );
            toolBar.frame = CGRectMake(0, 651,768, 44);

        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            datePicker.frame =  CGRectMake(0,493,1024,116);
            toolBar.frame = CGRectMake(0,449,1024, 44);
        }
        
    }
    
    datePicker.hidden = toolBar.hidden = FALSE;
    datePicker.timeZone = [NSTimeZone defaultTimeZone];
    UIButton *btn = (UIButton *)sender;
    buttontag = btn.tag;
    if (btn.tag == 20)
    {
        datePicker.date = fromDate;
    }
    else if (btn.tag == 21)
    {
        datePicker.date = toDate;
    }
    [btn setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
}

-(IBAction)datePicker_changed:(id)sender
{
    UIButton *btn = (UIButton *)[self.view viewWithTag:buttontag];
    
    //nslog(@"\n datePicker.date = %@",datePicker.date);
    
  
    
    
    [btn setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
    
    
    
    
   

    
}
-(IBAction)done_clicked
{
    if (buttontag == 21)
    {
        if ([fromButton.titleLabel.text length]>0)
        {
            
           
            
            /*
            NSDate *tempDate = [df dateFromString:[[regionDateFormatter stringFromDate:commonDateFromRegionDate] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
            
            NSDate *tempDate = [df dateFromString:[fromButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
             */
            
            nslog(@"\n fromButton.titleLabel.text = %@",fromButton.titleLabel.text);
            
            NSDate*commonDateFromRegionDate = [appDelegate.regionDateFormatter dateFromString:[fromButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
            
            nslog(@"\n commonDateFromRegionDate = %@",commonDateFromRegionDate);
            
            
            
            NSDate *tempDate = [df dateFromString:[df stringFromDate:commonDateFromRegionDate]];
            
            
            nslog(@"\n tempDate = %@",tempDate);

            
          
            
            nslog(@"date comp: %d", [tempDate compare:datePicker.date]);
            
            if (![tempDate isEqualToDate:datePicker.date])
            {
                int comp = [tempDate compare:datePicker.date];
                if (comp > 0)
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Date should be greater than from date" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    [alert show];
                    [alert release];
                    return;
                }
            }
        }
    }
    else if (buttontag == 20)
    {
        if ([toButton.titleLabel.text length]>0)
        {
            
            
            
            
            NSDate*commonDateFromRegionDate = [appDelegate.regionDateFormatter dateFromString:[toButton.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
            
            nslog(@"\n commonDateFromRegionDate = %@",commonDateFromRegionDate);
            
            nslog(@"\n med date = %@",[df dateFromString:[df stringFromDate:commonDateFromRegionDate]]);
            NSDate *tempDate = [df dateFromString:[df stringFromDate:commonDateFromRegionDate]];
            nslog(@"\n tempDate = %@",tempDate);
            
          
            
            nslog(@"date comp: %d", [tempDate compare:datePicker.date]);
            
            if (![tempDate isEqualToDate:datePicker.date])
            {
                int comp = [tempDate compare:datePicker.date];
                if (comp < 0)
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Date should be less than to date" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    [alert show];
                    [alert release];
                    return;
                }
            }
        }
    }
    
    datePicker.hidden = toolBar.hidden = TRUE;
}


#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(appDelegate.isIPad)
    {
        if (section == 0 && [appDelegate.propertyDetailArray count]>0)
        {
            return @"                                             Detailed Transaction Report";
        }
    }
    else 
    {
        if (section == 0 && [appDelegate.propertyDetailArray count]>0)
        {
            return @"       Detailed Transaction Report";
        }

    }
    return @"";
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [appDelegate.propertyDetailArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    static NSString *CellIdentifier = @"Cell";
    
    reportCustomCell *cell;// = (reportCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (cell == nil) 
    if (1) 
    {
        cell = [[[reportCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
    }
    
    [cell.pdfButton addTarget:self action:@selector(pdf_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.propertyLbl setFont:[UIFont systemFontOfSize:15.0]];
    cell.propertyLbl.text = [[appDelegate.propertyDetailArray objectAtIndex:indexPath.row] valueForKey:@"0"];
    cell.pdfButton.tag = 2000+indexPath.row;
    // Configure the cell...

    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark -
#pragma mark pdf method

-(IBAction)pdf_clicked:(id)sender
{
    
    nslog(@"\n toButton.titleLabel.text = %@",toButton.titleLabel.text);
    
    UIButton *btn = (UIButton*)sender;
    pdfViewController *pdfView = [[pdfViewController alloc]initWithNibName:@"pdfViewController" bundle:nil];
    pdfView.fromString = fromButton.titleLabel.text;
    pdfView.toString = toButton.titleLabel.text;
    pdfView.arrayIndex = btn.tag%2000;
    [self.navigationController pushViewController:pdfView animated:YES];
    [pdfView release];
    
}

@end
