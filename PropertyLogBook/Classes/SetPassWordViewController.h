//
//  SetPassWordViewController.h
//  MyReferences
//
//  Created by Neel  Shah on 09/12/11.
//  Copyright (c) 2011 Sunshine Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PropertyLogBookAppDelegate;
@interface SetPassWordViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField *txt_PassWord;
    IBOutlet UILabel *lbl_Showtext;
    
    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UIButton *btn_Done;
    
    NSString *StringPass1,*StringPass2;
    
    IBOutlet UILabel *lbl_Hint;
    IBOutlet UITextField *txt_Hint;
    
    IBOutlet UIToolbar *toolBarKeyboard;
    BOOL Bool_SetTextField;
    
    IBOutlet UITableView *tbl_Setpass;
    
    int TagForNext;
    NSUserDefaults* prefs;
}

@end
