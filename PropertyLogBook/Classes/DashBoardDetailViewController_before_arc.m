//
//  DashBoardDetailViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 9/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DashBoardDetailViewController.h"
#import "PropertyLogBookAppDelegate.h"
#import "DashBoardCustomCell.h"
#import <QuartzCore/QuartzCore.h>
#import "pdfViewController.h"
#import "ExcelGenerationViewController.h"

@implementation DashBoardDetailViewController
@synthesize arrayindex, dashboardArray, periodString, propertyImage,incomeBarAxis,expenseBarAxis;
@synthesize from_date,to_date;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        // Custom initialization
    }
    return self;
}


- (void)dealloc
{
    MemoryLog(@"\n dealloc in = %@ \n",NSStringFromClass([self class]));
    
    [to_date release];
    [from_date release];
    
    [expenseBarAxis release];
    [incomeBarAxis release];
    
    [periodString release];
    
    [propertyImage release];
    
    [customTickLocations release];
    [dashboardArray release];
    
    [bgImageView release];
    [barImageView release];
    [lblLine release];
    
    [lblExpenseSummary release];
    [lblIncomeSummary release];
    
    [lblBarExpenseSummary release];
    [lblBarIncomeSummary release];
    
    [incomeExpenseFilterSegmentControl release];
    [segementControlView release];
    [barBackground release];
    
    
    [piechart_shadow_imageView release];
    [piechart_hover_imageView release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}




#pragma mark - View lifecycle




- (void)viewDidLoad
{

    [super viewDidLoad];
        appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
     [self setLayoutFrames];

}


#pragma mark - cancel_clicked
#pragma mark back button clicked..

-(void)cancel_clicked
{
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark segment Title Button Chnaged


-(IBAction)bar_type_button_pressed:(id)sender
{
    UIButton*tmpButton = (UIButton*)sender;
    if(tmpButton.tag == 6003)
    {
        selectedBarTypeSegment = 0;
        
        
        [imgPieChartButton setImage:[UIImage imageNamed:@"pie_selected.png"] forState:UIControlStateNormal];
        [imgPieChartButton setImage:[UIImage imageNamed:@"pie_selected.png"] forState:UIControlStateHighlighted];
        
        [imgBarChartButton setImage:[UIImage imageNamed:@"barchart.png"] forState:UIControlStateNormal];
        [imgBarChartButton setImage:[UIImage imageNamed:@"barchart.png"] forState:UIControlStateHighlighted];

        
        [imgTableViewButton setImage:[UIImage imageNamed:@"table.png"] forState:UIControlStateNormal];
        [imgTableViewButton setImage:[UIImage imageNamed:@"table.png"] forState:UIControlStateHighlighted];

        
    }
    else if(tmpButton.tag == 6004)
    {
        selectedBarTypeSegment = 1;
        
        [imgPieChartButton setImage:[UIImage imageNamed:@"pie.png"] forState:UIControlStateNormal];
        [imgPieChartButton setImage:[UIImage imageNamed:@"pie.png"] forState:UIControlStateHighlighted];
        
        [imgBarChartButton setImage:[UIImage imageNamed:@"barchart_selected.png"] forState:UIControlStateNormal];
        [imgBarChartButton setImage:[UIImage imageNamed:@"barchart_selected.png"] forState:UIControlStateHighlighted];
        
        
        [imgTableViewButton setImage:[UIImage imageNamed:@"table.png"] forState:UIControlStateNormal];
        [imgTableViewButton setImage:[UIImage imageNamed:@"table.png"] forState:UIControlStateHighlighted];
        
        
    }
    else if(tmpButton.tag == 6005)
    {
        selectedBarTypeSegment = 2;
        
        [imgPieChartButton setImage:[UIImage imageNamed:@"pie.png"] forState:UIControlStateNormal];
        [imgPieChartButton setImage:[UIImage imageNamed:@"pie.png"] forState:UIControlStateHighlighted];
        
        [imgBarChartButton setImage:[UIImage imageNamed:@"barchart.png"] forState:UIControlStateNormal];
        [imgBarChartButton setImage:[UIImage imageNamed:@"barchart.png"] forState:UIControlStateHighlighted];
        
        
        [imgTableViewButton setImage:[UIImage imageNamed:@"table_selected.png"] forState:UIControlStateNormal];
        [imgTableViewButton setImage:[UIImage imageNamed:@"table_selected.png"] forState:UIControlStateHighlighted];
    }
    
    /*SUN:0004
     
     */
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation ==1 || [UIApplication sharedApplication].statusBarOrientation ==2)
        {
            pieChartScrollView.frame = CGRectMake(0,118,768,830);
        }
        else if([UIApplication sharedApplication].statusBarOrientation ==3 || [UIApplication sharedApplication].statusBarOrientation ==4)
        {
            pieChartScrollView.frame = CGRectMake(0,118,1024,600);
        }
    }
    
    
    
    
    [self generateIncomeExpenseReport];
    
    
}








#pragma mark -
#pragma mark willRotateToInterfaceOrientation 
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
    /*
    if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2)
    {
        expenseGraphView.frame = CGRectMake(80,30,600,640);
        expenseGraphView.center = CGPointMake(384,512);
    }
    
    else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4)
    {
        expenseGraphView.frame = CGRectMake(180,30,600,520);
        expenseGraphView.center = CGPointMake(512,132);
    }
     */


}
#pragma mark -
#pragma mark didRotateFromInterfaceOrientation
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
       
    




    
    
    
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
        
        
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
        {
            piechart_shadow_imageView.frame = CGRectMake(197,110,372,360);
            piechart_hover_imageView.frame = CGRectMake(197,110,372,360);
            [self.view sendSubviewToBack:piechart_shadow_imageView];
            
            [self.view bringSubviewToFront:expense_piechart_hover_imageView];
            expense_piechart_shadow_imageView.frame = CGRectMake(197,110,372,360);
            expense_piechart_hover_imageView.frame = CGRectMake(197,110,372,360);
            
            pie_chart_label_bg_imageView.frame =CGRectMake(0,730,768,72);
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4 )
        {
            piechart_shadow_imageView.frame = CGRectMake(297,42,367,364);
            piechart_hover_imageView.frame = CGRectMake(297,42,367,364);
            [self.view sendSubviewToBack:piechart_shadow_imageView];
            [self.view bringSubviewToFront:expense_piechart_hover_imageView];
            expense_piechart_shadow_imageView.frame = CGRectMake(297,42,367,364);
            expense_piechart_hover_imageView.frame = CGRectMake(297,42,367,364);
            
            
             pie_chart_label_bg_imageView.frame =CGRectMake(0,474,1024,72);
            
        }

        
        
        
        
        
        
        
    }

    
    [tblView reloadData];
    incomeBarView.backgroundColor = [UIColor clearColor];
    expenseBarView.backgroundColor = [UIColor clearColor];
    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
    {
        
        
      //  self.view.frame = CGRectMake(0, 0,768,1024);
        
        
      
        segementControlView.frame = CGRectMake(0,1,768,44);
        segementControlView.backgroundColor = [UIColor clearColor];
       
        
        imgIncomeButton.frame =CGRectMake(0, 0,384,44);
        imgExpenseButton.frame=CGRectMake(384,0,384,44);
        
        
        
        
        
        
        barImageView.frame = CGRectMake(0, 0,768,44);
        bgImageView.frame = CGRectMake(0, 0,768,1024);
        
        segment.frame = CGRectMake(0, 0,768,44);
        lblLine.frame = CGRectMake(0, 100,768,1);
        propertyName.frame = CGRectMake(73, 55,propertyName.frame.size.width,propertyName.frame.size.height);
        period.frame = CGRectMake(73, 75,period.frame.size.width,period.frame.size.height);
        pieChartScrollView.frame = CGRectMake(0,118,768,830);
        
        
        //--------------------
        
        noExpensePie.frame = CGRectMake(250,300, 280, 21);
        noExpensePie.center = CGPointMake(384,325);
        
        
        
        
        //--------------------
        
        noIncomePie.frame = CGRectMake(250,300, 280, 21);
        noIncomePie.center = CGPointMake(384,325);

        
        /*Starts: pie chart
        
        pieChart1.paddingLeft = 20.0;
        pieChart1.paddingTop = 00.0;
        pieChart1.paddingRight = 20.0;
        pieChart1.paddingBottom = 120.0;
        
        
        lblExpenseSummary.frame =  CGRectMake(340,23,150,21);
        imgExpenseSummary.frame= CGRectMake(305,20,30,30);
        expenseGraphView.frame = CGRectMake(80,30,600,640);
        
        
        lblIncomeSummary.frame= CGRectMake(340,23,150,21);
        imgIncomeSummary.frame= CGRectMake(305,20,30,30);

        
        incomeGraphView.frame = CGRectMake(80,30,600,540);
        
        pieChart.paddingLeft = 20.0;
        pieChart.paddingTop = 0.0;
        pieChart.paddingRight = 20.0;
        pieChart.paddingBottom = 120.0;
         
        
        /*Ends:  pie chart*/
        
        /*Starts : tblview */
        
        tableViewChart.frame = CGRectMake(0, 0,768,900);
        tblView.frame = CGRectMake(0, 0,768,900);
        
        /*Ends : tblview */
        
        
        propertyImage.frame= CGRectMake(16,55,40,40);
        
        
        
        
        /*Starts:bar chart*/
        
        
        
        noIncomeBar.frame= CGRectMake(250,300, 280, 21);
        noIncomeBar.center = CGPointMake(384,325);
        
        noExpenseBar.frame= CGRectMake(250,300, 280, 21);
        noExpenseBar.center = CGPointMake(384,325);
        

        expensePercentageImg.frame = CGRectMake(0,280,34,137);
        incomePercentageImg.frame = CGRectMake(0,280,34,137);

        
        

        
        
        
        imgBarIncomeSummary.frame= CGRectMake(305,20,30,30);
        imgBarExpenseSummary.frame= CGRectMake(305,20,30,30);
        
        
        
        lblBarIncomeSummary.frame= CGRectMake(340,23,150,21);
        
        
        incomeBarAxis.frame = CGRectMake(32,261,700,150);
        incomeBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];
        
        
        
        
        
        
        incomeBarView.frame = CGRectMake(10,260,700,420);
        incomeBarView.backgroundColor = [UIColor clearColor];
        
        
        incomeBarScrollView.backgroundColor = [UIColor clearColor];
        incomeBarScrollView.frame = CGRectMake(44.0f,320.0f,700,420);

        
        
        incomePercentageImg.frame = CGRectMake(0,280,34,137);
        
        
        
        

        
        
        
        

        
        [self generateBarChart];
        
        
        //-------------
        

        expenseBarAxis.frame = CGRectMake(32,261,700,150);
        expenseBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];

        
        expenseBarView.frame = CGRectMake(10,260,700,420);
        expenseBarView.backgroundColor = [UIColor clearColor];
        
       
        expenseBarScrollView.backgroundColor = [UIColor clearColor];
        expenseBarScrollView.frame = CGRectMake(44.0f,320.0f,700,420);
        
        
        
        
        
        lblBarExpenseSummary.frame = CGRectMake(340,23,150,21);

        
        
        

        [self generateExpenseBarChart];
        /*Ends Bar chart*/
        
        
        
        
        
        
       

        
        
        
        
        
        
        infoScrollView.delegate = self;
        yCalForColor = -30;
        
        
        infoScrollView1.delegate = self;
        yCalForColor1 = -30;
        
        infoScrollView1.userInteractionEnabled = TRUE;
        infoScrollView1.scrollEnabled = TRUE;
        
        
       
        
        
        
        infoScrollView1.frame = CGRectMake(0,730,768,66);
        infoScrollView1.backgroundColor  = [UIColor clearColor];
        
        
        //incomeGraphView.frame = CGRectMake(80,30,600,640);
        
        infoScrollView.frame = CGRectMake(0,730,768,66);
        
       
        
        
        // Pie chart
        
        pieChartView.frame = CGRectMake(0, 0,768,1024);
        
        tableViewChart.frame = CGRectMake(0, 0,768,900);
        tblView.frame = CGRectMake(0, 0,768,900);
        
        
        
        
         
    }
    
    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
    {
        
        segementControlView.frame = CGRectMake(0,0,1024,44);
        barBackground.frame = CGRectMake(0,-1,1024,45);
        imgIncomeButton.frame =CGRectMake(128, 0,384,44);
        imgExpenseButton.frame=CGRectMake(512,0,384,44);

        
        //segementControlView.center = CGPointMake(512,22);
        
        
        //self.view.frame = CGRectMake(0, 0,1024,768);
        
        
        bgImageView.frame = CGRectMake(0, 0,1024,768);
        barImageView.frame = CGRectMake(0, 0,1024,44);
        segment.frame = CGRectMake(0, 0,1024,44);
        lblLine.frame = CGRectMake(0, 100,1024,1);
        propertyName.frame = CGRectMake(73, 55,propertyName.frame.size.width,propertyName.frame.size.height);
        period.frame = CGRectMake(73, 75,period.frame.size.width,period.frame.size.height);
        propertyImage.frame= CGRectMake(16,55,40,40);
        pieChartScrollView.frame = CGRectMake(0,118,1024,600);
        
        
        
        noIncomePie.frame = CGRectMake(250,300, 280, 21);
        noIncomePie.center = CGPointMake(512,225);
        
        noExpensePie.frame = CGRectMake(250,300, 280, 21);
        noExpensePie.center = CGPointMake(512,225);
        
        
        /*Starts: pie chart
        
        pieChart1.paddingLeft = 10.0;
        pieChart1.paddingTop = 0.0;
        pieChart1.paddingRight = 20.0;
        pieChart1.paddingBottom = 10.0;
        
        lblExpenseSummary.frame= CGRectMake(445,23,150,21);
        imgExpenseSummary.frame= CGRectMake(410,20,30,30);

        noExpensePie.frame = CGRectMake(250,300, 280, 21);
        noExpensePie.center = CGPointMake(512,384);
        
        
       
        
        
        expenseGraphView.frame = CGRectMake(180,30,600,400);
        expenseGraphView.backgroundColor = [UIColor clearColor];
        
        
        
        infoScrollView1.frame = CGRectMake(290,450,700,80);
        yCalForColor1 = -30;
        
        pieChart.paddingLeft = 10.0;
        pieChart.paddingTop = 0.0;
        pieChart.paddingRight = 20.0;
        pieChart.paddingBottom = 10.0;
        
        incomeGraphView.frame = CGRectMake(180,30,600,400);
        */
        
        lblIncomeSummary.frame= CGRectMake(455,23,150,21);
        imgIncomeSummary.frame= CGRectMake(410,20,30,30);
        
        
        infoScrollView1.frame = CGRectMake(0,474,1024,66);
        infoScrollView.frame = CGRectMake(0,474,1024,66);
        
        
        yCalForColor = -30;
        
        
        
        
        

        
        
        
        /*Ends:  pie chart*/
        
        
        /*Starts : bar chart*/
        
        lblBarIncomeSummary.frame= CGRectMake(455,23,150,21);
        imgBarIncomeSummary.frame= CGRectMake(410,20,30,30);
        
        
        lblBarExpenseSummary.frame= CGRectMake(455,23,150,21);
        imgBarExpenseSummary.frame= CGRectMake(410,20,30,30);
        
        
        incomePercentageImg.frame = CGRectMake(0,220,34,137);
        
        
        incomeBarView.frame = CGRectMake(10,200,700,420);
        incomeBarScrollView.frame= CGRectMake(44,210,700,420);

        
        incomeBarAxis.frame = CGRectMake(32,201,700,150);
        incomeBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];

        
        noIncomeBar.frame = CGRectMake(250,300, 280, 21);
        noIncomeBar.center = CGPointMake(512,225);

        
        [self generateBarChart];
        
        
        noExpenseBar.frame = CGRectMake(250,300, 280, 21);
        noExpenseBar.center = CGPointMake(512,225);

        
        expenseBarAxis.frame = CGRectMake(32,201,700,150);
        expenseBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];

        
        expenseBarView.frame = CGRectMake(10,200,700,420);
        expenseBarScrollView.frame= CGRectMake(44,210,700,420);

        expensePercentageImg.frame = CGRectMake(0,220,34,137);

        
        
        [self generateExpenseBarChart];
        
        /*Ends : bar chart*/
        
       
        /*Starts : tblview chart*/
        
        
        

        
        
               
        
        
        tableViewChart.frame = CGRectMake(0, 0,1024,768);
        tblView.frame = CGRectMake(0, 0,1024,768);
        
        /*Ends : tblview chart*/
        
        
       
        
        
        
       
        
        
        
    }
    
    

    
   
    yCalForColor1 = -30;
    yCalForColor = -30;
    [self generateExpensePieChart];
    [self generateIncomePieChart];
   
    
    

    
}



-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [appDelegate.add_income_expense_bottom_button removeTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventAllEvents];
    
}

#pragma mark -
#pragma mark viewWillAppear 
-(void)viewWillAppear:(BOOL)animated
{
    
     appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
        [appDelegate selectCurrencyIndex];
    }
    
    
    
    
    /*Memory*/
    //[appDelegate.curCode retain];
    
     pieChartView.backgroundColor = [UIColor clearColor];
    
    

    [self.view bringSubviewToFront:dashboard_proeprtydetail_bg];
    [self.view bringSubviewToFront:propertyImage];
    [self.view bringSubviewToFront:propertyName];
    [self.view bringSubviewToFront:period];
    
    
    segment.frame = CGRectMake(0,7,264,30);
    
    [segment setImage:[UIImage imageNamed:@"piechart_selected.png"] forSegmentAtIndex:0];    
    [segment setImage:[UIImage imageNamed:@"barchart.png"] forSegmentAtIndex:1];
    [segment setImage:[UIImage imageNamed:@"tableview.png"] forSegmentAtIndex:2];    
    
    
    
   
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    //[self.view addSubview:appDelegate.bottomView];
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    if(appDelegate.isIPad)
    {
        //[self setLayoutFrames];
        
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
        
        //[appDelegate manageViewControllerHeight];
        
    }


    
    
    [tblView reloadData];
}

-(void)setLayoutFrames
{
    
    
    
    
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    //pieChartScrollView.backgroundColor = [UIColor redColor];
    
    if(appDelegate.isIPad)
    {
        propertyImage.frame= CGRectMake(16,48,30,30);
        piechart_shadow_imageView.hidden = TRUE;
        piechart_hover_imageView.hidden = TRUE;
        
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2 )
        {
            piechart_shadow_imageView.frame = CGRectMake(197,110,372,360);
            piechart_hover_imageView.frame = CGRectMake(197,110,372,360);
            
            
            [self.view sendSubviewToBack:piechart_shadow_imageView];
            [self.view bringSubviewToFront:expense_piechart_hover_imageView];
            
            expense_piechart_shadow_imageView.frame = CGRectMake(197,110,372,360);
            expense_piechart_hover_imageView.frame = CGRectMake(197,110,372,360);
            
        }
        else
        {
            piechart_shadow_imageView.frame = CGRectMake(297,42,367,364);
            piechart_hover_imageView.frame = CGRectMake(297,42,367,364);
            [self.view sendSubviewToBack:piechart_shadow_imageView];
            [self.view bringSubviewToFront:expense_piechart_hover_imageView];
            expense_piechart_shadow_imageView.frame = CGRectMake(297,42,367,364);
            expense_piechart_hover_imageView.frame = CGRectMake(297,42,367,364);
            
        }
        
    }
    
    else
    {
        if(appDelegate.isIphone5)
        {
            propertyImage.frame= CGRectMake(16,38,30,30);
            
            piechart_shadow_imageView.frame = CGRectMake(50,33,240,240);
            piechart_hover_imageView.frame = CGRectMake(44,30,242,242);
            [self.view sendSubviewToBack:piechart_shadow_imageView];
            
            
            [self.view bringSubviewToFront:expense_piechart_hover_imageView];
            
            expense_piechart_shadow_imageView.frame = CGRectMake(50,33,240,240);
            expense_piechart_hover_imageView.frame = CGRectMake(44,30,242,242);
            
        }
        else
        {
            propertyImage.frame= CGRectMake(16,48,30,30);
            
            piechart_shadow_imageView.frame = CGRectMake(83,33,160,160);
            piechart_hover_imageView.frame = CGRectMake(80,30,160,160);
            
            
            expense_piechart_shadow_imageView.frame =CGRectMake(83,33,160,160);
            expense_piechart_hover_imageView.frame = CGRectMake(80,30,160,160);
            
            
        }
        
    }
    
    
    expense_piechart_shadow_imageView.hidden = TRUE;
    expense_piechart_hover_imageView.hidden = TRUE;
    
    
    
    piechart_hover_imageView.hidden = TRUE;
    piechart_shadow_imageView.hidden = TRUE;
    
    
    
    noIncomeBar.hidden = noExpenseBar.hidden = TRUE;
    noIncomePie.hidden = noExpensePie.hidden = TRUE;
    
    
    
    
    
    
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            incomeExpenseFilterSegmentControl.frame = CGRectMake(0, 0,768,44);
        }
        
    }
    
    if(appDelegate.isIPad)
    {
        [imgIncomeButton setImage:[UIImage imageNamed:@"dash_ipad_income.png"] forState:UIControlStateNormal];
        [imgIncomeButton setImage:[UIImage imageNamed:@"dash_ipad_income.png"] forState:UIControlStateHighlighted];
        
        
        
        [imgExpenseButton setImage:[UIImage imageNamed:@"dash_ipad_expense_selected.png"] forState:UIControlStateNormal];
        [imgExpenseButton setImage:[UIImage imageNamed:@"dash_ipad_expense_selected.png"] forState:UIControlStateHighlighted];
    }
    else
    {
        [imgIncomeButton setImage:[UIImage imageNamed:@"income_dash.png"] forState:UIControlStateNormal];
        [imgIncomeButton setImage:[UIImage imageNamed:@"income_dash.png"] forState:UIControlStateHighlighted];
        
        [imgExpenseButton setImage:[UIImage imageNamed:@"expense_dash_selected.png"] forState:UIControlStateNormal];
        [imgExpenseButton setImage:[UIImage imageNamed:@"expense_dash_selected.png"] forState:UIControlStateHighlighted];
    }
    
    
    
    selectedBarTypeSegment = 0;
    isIncomeOrExpenseSelected = 1;
    
    
    customTickLocations = [[NSMutableArray alloc]init];
    barPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor colorWithComponentRed:255.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0] horizontalBars:NO];
    
    barPlotIncome = [CPTBarPlot tubularBarPlotWithColor:[CPTColor colorWithComponentRed:0.0f/255.0 green:255.0/255.0 blue:0.0/255.0 alpha:1.0] horizontalBars:NO];
    
    
    
    barChart1 = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    barChart = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    
    
    [incomeExpenseFilterSegmentControl setImage:[UIImage imageNamed:@"expense_dash.png"] forSegmentAtIndex:0];
    [incomeExpenseFilterSegmentControl setImage:[UIImage imageNamed:@"income_dash_selected.png"] forSegmentAtIndex:1];
    
    
    
    
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            self.view.frame = CGRectMake(0, 0,768,1024);
            
            
            barBackground.frame = CGRectMake(0,-1,768,45);
            
            
            imgIncomeButton.frame =CGRectMake(0, 0,384,44);
            imgExpenseButton.frame=CGRectMake(384,0,384,44);
            
            
            
            
            
            barImageView.frame = CGRectMake(0, 0,768,44);
            bgImageView.frame = CGRectMake(0, 0,768,1024);
            
            segment.frame = CGRectMake(0, 0,768,44);
            lblLine.frame = CGRectMake(0, 100,768,1);
            propertyName.frame = CGRectMake(73, 55,propertyName.frame.size.width,propertyName.frame.size.height);
            period.frame = CGRectMake(73, 75,period.frame.size.width,period.frame.size.height);
            pieChartScrollView.frame = CGRectMake(0,118,768,830);
            
            
            
            propertyImage.frame= CGRectMake(16,55,40,40);
            
            
            
            noIncomePie.frame = CGRectMake(250,150, 280, 21);
            noExpensePie.frame = CGRectMake(250,150, 280, 21);
            
            
            noIncomeBar.frame= CGRectMake(250,150, 280, 21);
            noExpenseBar.frame= CGRectMake(250,150, 280, 21);
            
            
            
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            self.view.frame = CGRectMake(0, 0,1024,768);
            
            barBackground.frame = CGRectMake(0,-1,1024,45);
            
            
            imgIncomeButton.frame =CGRectMake(128, 0,384,44);
            imgExpenseButton.frame=CGRectMake(512,0,384,44);
            
            
            
            
            bgImageView.frame = CGRectMake(0, 0,1024,768);
            barImageView.frame = CGRectMake(0, 0,1024,44);
            
            segment.frame = CGRectMake(0, 0,1024,44);
            lblLine.frame = CGRectMake(0, 100,1024,1);
            
            propertyName.frame = CGRectMake(73, 55,propertyName.frame.size.width,propertyName.frame.size.height);
            period.frame = CGRectMake(73, 75,period.frame.size.width,period.frame.size.height);
            
            pieChartScrollView.frame = CGRectMake(0,118,1024,600);
            propertyImage.frame= CGRectMake(16,55,40,40);
            
            
            
        }
        
    }
    
    
    
    tblView.backgroundColor = [UIColor clearColor];
    propertyImage.image = [[appDelegate.propertyDetailArray objectAtIndex:arrayindex]valueForKey:@"image"];
    incomeDataTypeArray = [[NSMutableArray alloc]init];
    expenseDataTypeArray = [[NSMutableArray alloc]init];
    totalIncomeAmount = 0.0;
    incomeDataforChart = [[NSMutableArray alloc]init];
    expenseDataForChart = [[NSMutableArray alloc]init];
    
    
    
    //pieChart = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
	
    
    
    
    
    if(appDelegate.isIPad)
    {
        
        
        //incomeGraphView.backgroundColor = [UIColor magentaColor];
        expenseGraphView.backgroundColor = [UIColor clearColor];
        
        /*
        incomeExpenseFilterSegmentControl.segmentedControlStyle = UISegmentedControlStyleBezeled;
        segment.segmentedControlStyle = UISegmentedControlStyleBezeled;
        */
        
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            lblIncomeSummary.frame= CGRectMake(340,23,150,21);
            imgIncomeSummary.frame= CGRectMake(305,20,30,30);
            
            
            lblExpenseSummary.frame =  CGRectMake(340,23,150,21);
            imgExpenseSummary.frame= CGRectMake(305,20,30,30);
            
            imgBarIncomeSummary.frame= CGRectMake(305,20,30,30);
            imgBarExpenseSummary.frame= CGRectMake(305,20,30,30);
            
            
            
            lblBarIncomeSummary.frame= CGRectMake(340,23,150,21);
            
            
            incomeBarAxis.frame = CGRectMake(32,261,700,150);
            incomeBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];
            incomePercentageImg.frame = CGRectMake(0,280,34,137);
            
            
            expenseBarAxis.frame = CGRectMake(32,261,700,150);
            expenseBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];
            
            incomeGraphView.frame = CGRectMake(80,30,600,540);
            
            
            
            
            
            expenseBarScrollView.frame = CGRectMake(expenseBarScrollView.frame.origin.x,expenseBarScrollView.frame.origin.y+50,expenseBarScrollView.frame.size.width,expenseBarScrollView.frame.size.height);
            
            
            
            
            
            expenseBarView.frame = CGRectMake(expenseBarView.frame.origin.x,expenseBarView.frame.origin.y+50,expenseBarView.frame.size.width,expenseBarView.frame.size.height);
            
            
            expensePercentageImg.frame = CGRectMake(0,280,34,137);
            incomePercentageImg.frame = CGRectMake(0,280,34,137);
            
            
            expenseBarAxis.frame = CGRectMake(expenseBarAxis.frame.origin.x,261,expenseBarAxis.frame.size.width,expenseBarAxis.frame.size.height);
            
            
            lblBarExpenseSummary.frame = CGRectMake(340,23,150,21);
            
            
            
            
            
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            
            
            lblIncomeSummary.frame= CGRectMake(455,23,150,21);
            imgIncomeSummary.frame= CGRectMake(420,20,30,30);
            
            
            /*Starts:Bar Chart*/
            
            
            lblBarIncomeSummary.frame= CGRectMake(455,23,150,21);
            lblBarExpenseSummary.frame= CGRectMake(455,23,150,21);
            
            imgBarIncomeSummary.frame= CGRectMake(420,23,30,30);
            incomePercentageImg.frame = CGRectMake(0,220,34,137);
            
            
            
            incomeBarAxis.frame = CGRectMake(210,291,700,150);
            incomeBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];
            
            
            
            
            imgBarExpenseSummary.frame= CGRectMake(420,23,30,30);
            
            
            expenseBarAxis.frame = CGRectMake(210,291,700,150);
            expenseBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];
            expensePercentageImg.frame = CGRectMake(175,308,expensePercentageImg.frame.size.width,expensePercentageImg.frame.size.height);
            /*Ends:Bar Chart*/
            
            
            
            
            incomeGraphView.frame = CGRectMake(300,30,600,240);
            
            noExpenseBar.frame = CGRectMake(noExpenseBar.frame.origin.x,noExpenseBar.frame.origin.y+50,noExpenseBar.frame.size.width,noExpenseBar.frame.size.height);
            
            
            
            expenseBarScrollView.frame = CGRectMake(expenseBarScrollView.frame.origin.x,expenseBarScrollView.frame.origin.y+50,expenseBarScrollView.frame.size.width,expenseBarScrollView.frame.size.height);
            nslog(@"\n  expensePercentageImg.frame.origin.y = %f",expensePercentageImg.frame.origin.y);
            
            
            
            
        }
        
    }
    else
    {
        
        expenseBarAxis.frame = CGRectMake(32, 32,276,150);
        
        
        
        if(appDelegate.isIphone5)
        {
            noExpenseBar.frame = CGRectMake(20,150,280,21);
            noIncomePie.frame = CGRectMake(20,150,280,21);
            
            noIncomeBar.frame = CGRectMake(20,150,280,21);
            noExpensePie.frame = CGRectMake(20,150,280,21);
        }
        else
        {
            
            
            noExpenseBar.frame = CGRectMake(20,124,280,21);
            noIncomePie.frame = CGRectMake(20,124,280,21);
            
            noIncomeBar.frame = CGRectMake(20,124,280,21);
            noExpensePie.frame = CGRectMake(20,124,280,21);
            
            
            
        }
        
        
        
        
        
        
        expensePercentageImg.frame = CGRectMake(0,52,34,137);
        expenseBarScrollView.frame = CGRectMake(44,41,276,220);
        expenseBarView.frame = CGRectMake(10,41,310,220);
        
        imgIncomeSummary.frame = CGRectMake(74,7,30,30);
        lblIncomeSummary.frame = CGRectMake(114,11,150,21);
        
        
        lblExpenseSummary.frame = CGRectMake(114,11,150,21);
        imgExpenseSummary.frame = CGRectMake(74,7,30,30);
        
        
        
        lblBarExpenseSummary.frame = CGRectMake(114,11,150,21);
        imgBarExpenseSummary.frame = CGRectMake(74,7,30,30);
        
        lblBarIncomeSummary.frame = CGRectMake(114,11,150,21);
        imgBarIncomeSummary.frame = CGRectMake(74,7,30,30);
        
        
    }
    
    
    
    
    
    /*
     Start:genrating pie chart...
     */
    
    // Add pie chart
    
    
    /*
     piePlot = [[CPTPieChart alloc] init];
     piePlot.dataSource = self;
     */
    
    [self generateIncomePieChart];
    
    
    pie_chart_label_bg_imageView = [[UIImageView alloc] init];
    pie_chart_label_bg_imageView.image = [UIImage imageNamed:@"dashboard_income_expense_list_bg.png"];
    
    [pieChartView addSubview:pie_chart_label_bg_imageView];
    pieChartView.backgroundColor = [UIColor redColor];
    
    if(!appDelegate.isIPad)
    {
        if(appDelegate.isIphone5)
        {
            /*Sun : 0004 */
            
            pie_chart_label_bg_imageView.frame =CGRectMake(0,290,320,72);
            
        }
        else
        {
            pie_chart_label_bg_imageView.frame = CGRectMake(0,200,320,72);
        }
        
    }
    else
    {
        if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
        {
            pie_chart_label_bg_imageView.frame =CGRectMake(0,730,768,72);
        }
        else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
        {
            pie_chart_label_bg_imageView.frame =CGRectMake(0,474,1024,72);
        }
    }
    
    
    
    
    
    if(appDelegate.isIPad)
    {
        
        
        
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            infoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,730,768,66)];
            infoScrollView.delegate = self;
            yCalForColor = -30;
            
            infoScrollView1 = [[UIScrollView alloc] initWithFrame:CGRectMake(0,730,768,66)];
            infoScrollView1.delegate = self;
            yCalForColor1 = -30;
            
            infoScrollView1.userInteractionEnabled = TRUE;
            infoScrollView1.scrollEnabled = TRUE;
            
            expenseGraphView.frame = CGRectMake(80,30,600,640);
            incomeGraphView.frame = CGRectMake(80,30,600,640);
            
            lblExpenseSummary.frame= CGRectMake(340,23,150,21);
            imgExpenseSummary.frame= CGRectMake(305,20,30,30);
            
            
            infoScrollView1.frame = CGRectMake(0,730,768,66);
            infoScrollView.frame = CGRectMake(0,730,768,66);
            
            
            lblIncomeSummary.frame= CGRectMake(340,23,150,21);
            imgIncomeSummary.frame= CGRectMake(305,20,30,30);
            
            
            
            
            
            
            //infoScrollView.backgroundColor = [UIColor greenColor];
            
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            infoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,474,1024,66)];
            infoScrollView.delegate = self;
            yCalForColor = -30;
            
            infoScrollView1 = [[UIScrollView alloc] initWithFrame:CGRectMake(0,474,1024,66)];
            infoScrollView1.delegate = self;
            yCalForColor1 = -30;
            
            
            
            
        }
        
        
    }
    else
    {
        
        
        
        if(appDelegate.isIphone5)
        {
            infoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,290,320,70)];
        }
        else
        {
            infoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,200,320,70)];
        }
        
        
        infoScrollView.delegate = self;
        yCalForColor = -30;
        
        /*Sun:0004
         infoScrollView1: for expenses label of pie chart.
         
         */
        
        if(appDelegate.isIphone5)
        {
            infoScrollView1 = [[UIScrollView alloc] initWithFrame:CGRectMake(0,290,320,70)];
        }
        else
        {
            infoScrollView1 = [[UIScrollView alloc] initWithFrame:CGRectMake(0,200,320,70)];
        }
        
        
        infoScrollView1.delegate = self;
        yCalForColor1 = -30;
    }
    
    
    //pieChart1 == expense pie chart..
    
    
    
    [self generateExpensePieChart];
    
    incomeSummeryArray = [[NSMutableArray alloc]init];
    expenseSummeryArray = [[NSMutableArray alloc]init];
    
    incomeSummeryDictionary = [[NSMutableDictionary alloc]init];
    expenseSummeryDictionary = [[NSMutableDictionary alloc]init];
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    
    
    self.navigationItem.titleView = chartTypeView;
    
    
    
    
    UIButton * button_back = [UIButton buttonWithType:UIButtonTypeCustom];
    // [button setTitle:@"  Back" forState:UIControlStateNormal];
    [button_back.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    button_back.bounds = CGRectMake(0, 0,49.0,29.0);
    [button_back setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [button_back addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    if(!appDelegate.isIPad)
    {
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button_back] autorelease];
    }
    
    
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0)
    {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
        /*
         [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor],UITextAttributeTextColor,[UIColor blackColor],UITextAttributeTextShadowColor,[NSValue valueWithUIOffset:UIOffsetMake(1, 0)], UITextAttributeTextShadowOffset ,[UIFont fontWithName:@"System" size:25.0],UITextAttributeFont  , nil]];
         */
        
    }
    
    
    
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
    button.bounds = CGRectMake(0, 0,49.0,29.0);
    [button setBackgroundImage:[UIImage imageNamed:@"export_icon.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(choose_report_type_button_pressed:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
    
    
    
    [incomeExpenseFilterSegmentControl setSelectedSegmentIndex:1];
    //incomeExpenseFilterSegmentControl.frame = CGRectMake(0, 0,180,30);
    
    propertyName.text = [[dashboardArray objectAtIndex:0]valueForKey:@"property"];
    period.text = [NSString stringWithFormat:@"Period: %@",periodString];
    
    nslog(@"dashboard detail arrya ==== %@",dashboardArray);
    
    for (int i = 0;i<[dashboardArray count];i++)
    {
        if ([[[dashboardArray objectAtIndex:i] valueForKey:@"incomeExpense"] intValue]==1)
        {
            [incomeSummeryArray addObject:[dashboardArray objectAtIndex:i]];
        }
        else  if ([[[dashboardArray objectAtIndex:i] valueForKey:@"incomeExpense"] intValue]==0)
        {
            [expenseSummeryArray addObject:[dashboardArray objectAtIndex:i]];
        }
    }
    
    tempIncomeArray = [[NSMutableArray alloc]init];
    
    for (int i =0;i<[incomeSummeryArray count];i++)
    {
        float amount = 0.0;
        
        NSString *str  = [[incomeSummeryArray objectAtIndex:i]valueForKey:@"type"];
        
        NSArray *arr = [incomeSummeryDictionary allKeys];
        int count =0 ;
        for (int i =0; i<[arr count];i++)
        {
            if ([str isEqualToString:[arr objectAtIndex:i]])
            {
                count ++;
            }
        }
        if (count == 0)
        {
            for (int j =0;j<[incomeSummeryArray count];j++)
            {
                if ([str isEqualToString:[[incomeSummeryArray objectAtIndex:j]valueForKey:@"type"]])
                {
                    amount = amount + [[[incomeSummeryArray objectAtIndex:j]valueForKey:@"amount"]floatValue];
                }
            }
            totalIncomeAmount = totalIncomeAmount + amount;
            [incomeSummeryDictionary setObject:[NSNumber numberWithFloat:amount] forKey:str];
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[NSString stringWithFormat:@"%.2f",amount] forKey:str];
            [incomeDataTypeArray addObject:str];
            [tempIncomeArray addObject:dict];
            [dict release];
        }
    }
    
    for ( int i =0;i<[tempIncomeArray count];i++)
    {
        float result = 100*[[[tempIncomeArray objectAtIndex:i]valueForKey:[NSString stringWithFormat:@"%@",[incomeDataTypeArray objectAtIndex:i]]]floatValue];
        
        result = result/totalIncomeAmount;
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        [dict setObject:[NSString stringWithFormat:@"%.2f",result] forKey:[NSString stringWithFormat:@"%@",[incomeDataTypeArray objectAtIndex:i]]];
        
        [incomeDataforChart addObject:dict];
        
        [dict release];
    }
    
    
    
    tempExpenseArray = [[NSMutableArray alloc]init];
    
    for (int i =0;i<[expenseSummeryArray count];i++)
    {
        float amount = 0.0;
        
        NSString *str  = [[expenseSummeryArray objectAtIndex:i]valueForKey:@"type"];
        
        NSArray *arr = [expenseSummeryDictionary allKeys];
        int count =0 ;
        for (int i =0; i<[arr count];i++)
        {
            if ([str isEqualToString:[arr objectAtIndex:i]])
            {
                count ++;
            }
        }
        if (count == 0)
        {
            for (int j =0;j<[expenseSummeryArray count];j++)
            {
                if ([str isEqualToString:[[expenseSummeryArray objectAtIndex:j]valueForKey:@"type"]])
                {
                    amount = amount + [[[expenseSummeryArray objectAtIndex:j]valueForKey:@"amount"]floatValue];
                }
            }
            totalExpenseAmount = totalExpenseAmount + amount;
            [expenseSummeryDictionary setObject:[NSNumber numberWithFloat:amount] forKey:str];
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[NSString stringWithFormat:@"%.2f",amount] forKey:str];
            [expenseDataTypeArray addObject:str];
            [tempExpenseArray addObject:dict];
            [dict release];
        }
    }
    
    for ( int i =0;i<[tempExpenseArray count];i++)
    {
        float result = 100*[[[tempExpenseArray objectAtIndex:i]valueForKey:[NSString stringWithFormat:@"%@",[expenseDataTypeArray objectAtIndex:i]]]floatValue];
        
        result = result/totalExpenseAmount;
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[NSString stringWithFormat:@"%.2f",result] forKey:[NSString stringWithFormat:@"%@",[expenseDataTypeArray objectAtIndex:i]]];
        
        [expenseDataForChart addObject:dict];
        
        [dict release];
    }
    
    nslog(@"expense data for chart aray === %@",expenseDataForChart);
    nslog(@"income data for chart array === %@",incomeDataforChart);
    
    
    
    
    [self generateBarChart];
    [self generateExpenseBarChart];
    
    
    
    /*
     Sun : 0004
     pieChartView where......
     */
    //pieChartView.backgroundColor = [UIColor redColor];
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            pieChartView.frame = CGRectMake(0, 0,768,1024);
        }
        else
        {
            pieChartView.frame = CGRectMake(0, 0,1024,768);
        }
        
    }
    else
    {
        if(appDelegate.isIphone5)
        {
            pieChartView.frame = CGRectMake(0, 0,320,348);
        }
        else
        {
            pieChartView.frame = CGRectMake(0, 0,320,300);
        }
        
        
    }
    
    
    
    
    
    
    incomeBarAxis.hidden = FALSE;
    expenseBarAxis.hidden = FALSE;
    
    
    /*Sun:0004:shadow and hover image for pie-chart */
    
    piechart_shadow_imageView.hidden = TRUE;
    piechart_hover_imageView.hidden = TRUE;
    
    expense_piechart_shadow_imageView.hidden = TRUE;
    expense_piechart_hover_imageView.hidden = TRUE;
    
    
    if ([incomeDataforChart count]==0)
    {
        incomePercentageImg.hidden = TRUE;
        
        incomeBarView.hidden = true;
        incomeGraphView.hidden = TRUE;
        noIncomeBar.hidden = FALSE;
        incomeBarAxis.hidden = TRUE;
        
        
        noIncomePie.hidden = FALSE;
        pie_chart_label_bg_imageView.hidden = TRUE;
        
        
        
        /*Sun:0004
         
         This
         piechart_shadow_imageView.hidden = TRUE;
         piechart_hover_imageView.hidden = TRUE;
         */
        
        
    }
    if ([expenseDataForChart count]==0)
    {
        expensePercentageImg.hidden = TRUE;
        expenseBarView.hidden = TRUE;
        expenseGraphView.hidden = TRUE;
        
        noExpenseBar.hidden = FALSE;
        noExpensePie.hidden = FALSE;
        
        expenseBarAxis.hidden = TRUE;
        
        pie_chart_label_bg_imageView.hidden = TRUE;
        
        
        expense_piechart_shadow_imageView.hidden = TRUE;
        expense_piechart_hover_imageView.hidden = TRUE;
        
    }
    else
    {
        //  expense_piechart_shadow_imageView.hidden = FALSE;
        //  expense_piechart_hover_imageView.hidden = FALSE;
    }
    
    
    isIncome =FALSE;
    /*Starts:Showing Pie Chart at initial*/
    //pieChartView.backgroundColor = [UIColor greenColor];
    
    /*Hiding Bar Chart Element*/
    expensePercentageImg.hidden = TRUE;
    expenseBarView.hidden = TRUE;
    noExpenseBar.hidden = TRUE;
    
    incomePercentageImg.hidden = TRUE;
    incomeBarView.hidden = TRUE;
    noIncomeBar.hidden = TRUE;
    
    /*Hiding Bar Chart Element*/
    
    /*Starts:Hiding all Income Elemetns*/
    
    
    incomeGraphView.hidden = TRUE;
    noIncomePie.hidden = TRUE;
    lblIncomeSummary.hidden = TRUE;
    imgIncomeSummary.hidden = TRUE;
    infoScrollView.hidden = YES;
    
    /*Ends:Hiding all Expense Elemetns*/
    
    
    /*Starts:Showing all Income Elemetns*/
    infoScrollView1.hidden = NO;
    lblExpenseSummary.hidden = NO;
    imgExpenseSummary.hidden = NO;
    if ([expenseDataForChart count]==0)
    {
        expenseGraphView.hidden = TRUE;
        noExpensePie.hidden = FALSE;
        
        pie_chart_label_bg_imageView.hidden = TRUE;
        expense_piechart_shadow_imageView.hidden = TRUE;
        expense_piechart_hover_imageView.hidden = TRUE;
        
    }
    else
    {
        expenseGraphView.hidden = FALSE;
        
        noExpensePie.hidden = TRUE;
        
        pie_chart_label_bg_imageView.hidden = FALSE;
        expense_piechart_shadow_imageView.hidden = FALSE;
        expense_piechart_hover_imageView.hidden = FALSE;
        
    }
    /*Ends:Showing all expense Elemetns*/
    
    [pieChartScrollView addSubview:pieChartView];
    
    
    //[pieChartScrollView setContentSize:CGSizeMake(pieChartView.frame.size.width, pieChartView.frame.size.height)];
    
    /*Ends:Showing Pie Chart at initial*/
    
    // nslog(@"\n tableview frame = %@",NSStringFromCGRect(tblView.frame));
    
    //tblView.backgroundColor = [UIColor redColor];
    
    if(!appDelegate.isIPad)
    {
        if(appDelegate.isIphone5)
        {
            pieChartScrollView.frame = CGRectMake(pieChartScrollView.frame.origin.x,pieChartScrollView.frame.origin.y,pieChartScrollView.frame.size.width, pieChartScrollView.frame.size.height+88);
            
            
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-30,tblView.frame.size.width, tblView.frame.size.height+88);
            
        }
        else
            
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-30,tblView.frame.size.width, tblView.frame.size.height);
        }
        
    }
    else
    {
        
    }
    
    
    
    
    
    
    
    /*Starts: Code is not managable so..writing orientation code here...*/
    
    if(appDelegate.isIPad)
    {
        
        
        [tblView reloadData];
        incomeBarView.backgroundColor = [UIColor clearColor];
        expenseBarView.backgroundColor = [UIColor clearColor];
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            
            
            self.view.frame = CGRectMake(0, 0,768,1024);
            
            
            
            segementControlView.frame = CGRectMake(0,1,768,44);
            segementControlView.backgroundColor = [UIColor clearColor];
            
            
            imgIncomeButton.frame =CGRectMake(0, 0,384,44);
            imgExpenseButton.frame=CGRectMake(384,0,384,44);
            
            
            
            
            
            
            barImageView.frame = CGRectMake(0, 0,768,44);
            bgImageView.frame = CGRectMake(0, 0,768,1024);
            
            segment.frame = CGRectMake(0, 0,768,44);
            lblLine.frame = CGRectMake(0, 100,768,1);
            propertyName.frame = CGRectMake(73, 55,propertyName.frame.size.width,propertyName.frame.size.height);
            period.frame = CGRectMake(73, 75,period.frame.size.width,period.frame.size.height);
            pieChartScrollView.frame = CGRectMake(0,118,768,830);
            
            /*Starts: pie chart*/
            
            pieChart1.paddingLeft = 20.0;
            pieChart1.paddingTop = 0.0;
            pieChart1.paddingRight = 20.0;
            pieChart1.paddingBottom = 120.0;
            
            
            
            
            
            
            lblExpenseSummary.frame =  CGRectMake(340,23,150,21);
            imgExpenseSummary.frame= CGRectMake(305,20,30,30);
            expenseGraphView.frame = CGRectMake(80,30,600,640);
            
            noExpensePie.frame = CGRectMake(250,300, 280, 21);
            noExpensePie.center = CGPointMake(384,325);
            
            
            //--------------------
            
            noIncomePie.frame = CGRectMake(250,150, 280, 21);
            noIncomePie.center = CGPointMake(384,325);
            
            lblIncomeSummary.frame= CGRectMake(340,23,150,21);
            imgIncomeSummary.frame= CGRectMake(305,20,30,30);
            
            
            incomeGraphView.frame = CGRectMake(80,30,600,540);
            
            pieChart.paddingLeft = 20.0;
            pieChart.paddingTop = 0.0;
            pieChart.paddingRight = 20.0;
            pieChart.paddingBottom = 120.0;
            
            
            /*Ends:  pie chart*/
            
            /*Starts : tblview */
            //tableViewChart.backgroundColor = [UIColor redColor];
            
            tableViewChart.frame = CGRectMake(0,0,768,900);
            tblView.frame = CGRectMake(0, 0,768,900);
            
            /*Ends : tblview */
            
            
            propertyImage.frame= CGRectMake(16,55,40,40);
            
            
            
            
            /*Starts:bar chart*/
            
            
            
            noIncomeBar.frame= CGRectMake(250,300, 280, 21);
            noIncomeBar.center = CGPointMake(384,325);
            
            noExpenseBar.frame= CGRectMake(250,300, 280, 21);
            noExpenseBar.center = CGPointMake(384,325);
            
            
            expensePercentageImg.frame = CGRectMake(0,280,34,137);
            incomePercentageImg.frame = CGRectMake(0,280,34,137);
            
            
            
            
            
            
            
            imgBarIncomeSummary.frame= CGRectMake(305,20,30,30);
            imgBarExpenseSummary.frame= CGRectMake(305,20,30,30);
            
            
            
            lblBarIncomeSummary.frame= CGRectMake(340,23,150,21);
            
            
            incomeBarAxis.frame = CGRectMake(32,261,700,150);
            incomeBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];
            
            
            
            
            
            
            incomeBarView.frame = CGRectMake(10,260,700,420);
            incomeBarView.backgroundColor = [UIColor clearColor];
            
            
            incomeBarScrollView.backgroundColor = [UIColor clearColor];
            incomeBarScrollView.frame = CGRectMake(44.0f,320.0f,700,420);
            
            
            
            incomePercentageImg.frame = CGRectMake(0,280,34,137);
            
            
            
            
            
            
            
            
            
            
            
            [self generateBarChart];
            
            
            //-------------
            
            
            expenseBarAxis.frame = CGRectMake(32,261,700,150);
            expenseBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];
            
            
            expenseBarView.frame = CGRectMake(10,260,700,420);
            expenseBarView.backgroundColor = [UIColor clearColor];
            
            
            expenseBarScrollView.backgroundColor = [UIColor clearColor];
            expenseBarScrollView.frame = CGRectMake(44.0f,320.0f,700,420);
            
            
            
            
            
            lblBarExpenseSummary.frame = CGRectMake(340,23,150,21);
            
            
            
            
            
            [self generateExpenseBarChart];
            /*Ends Bar chart*/
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            infoScrollView.delegate = self;
            yCalForColor = -30;
            
            infoScrollView.userInteractionEnabled = TRUE;
            infoScrollView.scrollEnabled = TRUE;
            
            
            infoScrollView1.delegate = self;
            yCalForColor1 = -30;
            
            infoScrollView1.userInteractionEnabled = TRUE;
            infoScrollView1.scrollEnabled = TRUE;
            
            
            
            
            
            
            //infoScrollView1.frame = CGRectMake(230,530,530,230);
            infoScrollView1.backgroundColor  = [UIColor clearColor];
            
            
            incomeGraphView.frame = CGRectMake(80,30,600,640);
            
            // infoScrollView.frame = CGRectMake(230,530,530,230);
            
            
            
            
            // Pie chart
            
            pieChartView.frame = CGRectMake(0, 0,768,1024);
            tableViewChart.frame = CGRectMake(0,0,768,900);
            tblView.frame = CGRectMake(0, 0,768,900);
            
            
            
            
            
        }
        
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            self.view.frame = CGRectMake(0, 0,1024,768);
            segementControlView.frame = CGRectMake(0,1,1024,44);
            barBackground.frame = CGRectMake(0,-1,1024,45);
            imgIncomeButton.frame =CGRectMake(128, 0,384,44);
            imgExpenseButton.frame=CGRectMake(512,0,384,44);
            
            
            //segementControlView.center = CGPointMake(512,22);
            
            
            
            bgImageView.frame = CGRectMake(0, 0,1024,768);
            barImageView.frame = CGRectMake(0, 0,1024,44);
            segment.frame = CGRectMake(0, 0,1024,44);
            lblLine.frame = CGRectMake(0, 100,1024,1);
            propertyName.frame = CGRectMake(73, 55,propertyName.frame.size.width,propertyName.frame.size.height);
            period.frame = CGRectMake(73, 75,period.frame.size.width,period.frame.size.height);
            propertyImage.frame= CGRectMake(16,55,40,40);
            pieChartScrollView.frame = CGRectMake(0,118,1024,600);
            
            
            /*Starts: pie chart*/
            
            pieChart1.paddingLeft = 10.0;
            pieChart1.paddingTop = 0.0;
            pieChart1.paddingRight = 20.0;
            pieChart1.paddingBottom = 10.0;
            
            
            
            
            
            
            lblExpenseSummary.frame= CGRectMake(445,23,150,21);
            imgExpenseSummary.frame= CGRectMake(410,20,30,30);
            
            noExpensePie.frame = CGRectMake(250,300, 280, 21);
            noExpensePie.center = CGPointMake(512,225);
            
            
            noIncomePie.frame = CGRectMake(250,300, 280, 21);
            noIncomePie.center = CGPointMake(512,225);
            
            
            expenseGraphView.frame = CGRectMake(180,30,600,400);
            expenseGraphView.backgroundColor = [UIColor clearColor];
            
            
            
            yCalForColor1 = -30;
            
            pieChart.paddingLeft = 10.0;
            pieChart.paddingTop = 00.0;
            pieChart.paddingRight = 20.0;
            pieChart.paddingBottom = 10.0;
            
            incomeGraphView.frame = CGRectMake(180,30,600,400);
            
            lblIncomeSummary.frame= CGRectMake(455,23,150,21);
            imgIncomeSummary.frame= CGRectMake(410,20,30,30);
            
            infoScrollView1.frame = CGRectMake(0,474,1024,66);
            infoScrollView.frame = CGRectMake(0,474,1024,66);
            yCalForColor = -30;
            
            
            
            
            
            
            
            
            
            /*Ends:  pie chart*/
            
            
            /*Starts : bar chart*/
            
            lblBarIncomeSummary.frame= CGRectMake(455,23,150,21);
            imgBarIncomeSummary.frame= CGRectMake(410,20,30,30);
            
            
            lblBarExpenseSummary.frame= CGRectMake(455,23,150,21);
            imgBarExpenseSummary.frame= CGRectMake(410,20,30,30);
            
            
            incomePercentageImg.frame = CGRectMake(0,220,34,137);
            
            
            incomeBarView.frame = CGRectMake(10,200,700,420);
            incomeBarScrollView.frame= CGRectMake(44,210,700,420);
            
            
            incomeBarAxis.frame = CGRectMake(32,201,700,150);
            incomeBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];
            
            
            noIncomeBar.frame = CGRectMake(250,300, 280, 21);
            noIncomeBar.center = CGPointMake(512,225);
            
            
            [self generateBarChart];
            
            
            noExpenseBar.frame = CGRectMake(250,300, 280, 21);
            noExpenseBar.center = CGPointMake(512,225);
            
            
            expenseBarAxis.frame = CGRectMake(32,201,700,150);
            expenseBarAxis.image = [UIImage imageNamed:@"chart_bar_axis_ipad.png"];
            
            
            expenseBarView.frame = CGRectMake(10,200,700,420);
            expenseBarScrollView.frame= CGRectMake(44,210,700,420);
            
            expensePercentageImg.frame = CGRectMake(0,220,34,137);
            
            
            
            [self generateExpenseBarChart];
            
            /*Ends : bar chart*/
            
            
            /*Starts : tblview chart*/
            
            
            
            
            
            
            
            
            
            tableViewChart.frame = CGRectMake(0,0,1024,768);
            tblView.frame = CGRectMake(0, 0,1024,768);
            
            /*Ends : tblview chart*/
            
            
            
            
            
            
            
            
            
            
        }
    }
    
    
    nslog(@"\n pieChartScrollView = %f",pieChartScrollView.contentSize.height);
    
    
    
    
    /*Ends: Code is not managable so..writing orientation code here...*/
    

    
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(appDelegate.isIPad)
    {
        return YES;
    }
    return NO;
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}


#pragma mark -
#pragma mark generateIncomePieChart

-(void)generateIncomePieChart
{
    [[infoScrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    pieChart = [[[CPTXYGraph alloc] initWithFrame:CGRectZero] autorelease];
    hostingView = (CPTGraphHostingView *)incomeGraphView;
    hostingView.backgroundColor = [UIColor clearColor];
    
    
   
    if(!appDelegate.isIPad)
    {
        if(appDelegate.isIphone5)
        {
            incomeGraphView.frame = CGRectMake(15,10,300,300);
            hostingView.frame= CGRectMake(15,10,300,300);

        }
        else
        {
            incomeGraphView.frame = CGRectMake(50,5,220,220);
        }

    }
    else if(appDelegate.isIPad)
    {
        
        
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                
                
                incomeGraphView.frame = CGRectMake(80,30,600,640);
                hostingView.frame = CGRectMake(80,30,600,640);
                
                
                
                pieChart.paddingLeft = 20.0;
                pieChart.paddingTop = 0.0;
                pieChart.paddingRight = 20.0;
                pieChart.paddingBottom = 120.0;
                
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                
                
                incomeGraphView.frame = CGRectMake(180,30,600,400);
                hostingView.frame = CGRectMake(180,30,600,400);
                

                
                pieChart.paddingLeft = 10.0;
                pieChart.paddingTop = 0.0;
                pieChart.paddingRight = 20.0;
                pieChart.paddingBottom = 10.0;
               
            }
             
      

    }
        
    
    
    hostingView.hostedGraph = pieChart;
   
    
    
    piePlotIncome = [[CPTPieChart alloc] init];
    //piePlotIncome.backgroundColor = [UIColor greenColor].CGColor;
    piePlotIncome.dataSource = self;
    
    if(appDelegate.isIPad)
    {
        piePlotIncome.pieRadius = 180.0;
    }
    else
    {
        
        
        if(appDelegate.isIphone5)
        {
            piePlotIncome.pieRadius = 120.0;
        }
        else
        {
            piePlotIncome.pieRadius = 80.0;
        }
        
        
        
    }
    
    
    piePlotIncome.identifier = @"Pie Chart 1";
	piePlotIncome.startAngle = M_PI_4;
	piePlotIncome.sliceDirection = CPTPieDirectionClockwise;
    
        
	pieChart.axisSet = nil;

    
    
    [pieChart addPlot:piePlotIncome];
    [piePlotIncome release];
    
    
    /*
     Ends : genrating pie chart...
     */
    
    
}


#pragma mark -
#pragma mark generateExpensePieChart 
-(void)generateExpensePieChart
{
    pieChart1 = [[[CPTXYGraph alloc] initWithFrame:CGRectZero] autorelease];
    nslog(@"\n generateExpensePieChart");
    [[infoScrollView1 subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
   
   
    
    hostingView = (CPTGraphHostingView *)expenseGraphView;
    hostingView.hostedGraph = nil;
    expenseGraphView.backgroundColor = [UIColor clearColor];
    if(appDelegate.isIPad)
    {
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            lblExpenseSummary.frame= CGRectMake(340,23,150,21);
            imgExpenseSummary.frame= CGRectMake(305,20,30,30);
            
            
            
            expenseGraphView.frame = CGRectMake(80,30,600,640);
            hostingView.frame = CGRectMake(80,30,600,640);
            
            
            pieChart1.paddingLeft = 20.0;
            pieChart1.paddingTop = 0.0;
            pieChart1.paddingRight = 20.0;
            pieChart1.paddingBottom = 120.0;
             
            
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            lblExpenseSummary.frame= CGRectMake(445,23,150,21);
            imgExpenseSummary.frame= CGRectMake(410,20,30,30);
            
            expenseGraphView.frame = CGRectMake(180,30,600,400);
            hostingView.frame = CGRectMake(180,30,600,400);
            
            
            
            pieChart1.paddingLeft = 10.0;
            pieChart1.paddingTop = 0.0;
            pieChart1.paddingRight = 20.0;
            pieChart1.paddingBottom = 10.0;
             
        }
        
    }
    else
    {
               
        
        
        if(appDelegate.isIphone5)
        {
            expenseGraphView.frame = CGRectMake(15,10,300,300);
            hostingView.frame= CGRectMake(15,10,300,300);
        }
        else
        {
            expenseGraphView.frame = CGRectMake(50,5,220,220);
        }
        
      
        
    }
    

    
  
   
    
    
   
    hostingView.hostedGraph = pieChart1;
	pieChart1.axisSet = nil;
	
    
	
    
    // Add pie chart
    
    
    piePlot = [[CPTPieChart alloc] init];
  
    
    
    
    piePlot.dataSource = self;
    if(appDelegate.isIPad)
    {
        piePlot.pieRadius = 180.0;
        
        
    }
    else
    {
        if(appDelegate.isIphone5)
        {
            piePlot.pieRadius = 120.0;
        }
        else
        {
            piePlot.pieRadius = 80.0;
        }
        
        
    }

    
    piePlot.identifier = @"Pie Chart 2";
	piePlot.startAngle = M_PI_4;
	piePlot.sliceDirection = CPTPieDirectionClockwise;
    //piePlot.backgroundColor = [UIColor blueColor].CGColor;
    [pieChart1 addPlot:piePlot];
    [piePlot release];
    
    
   
    
    
}

#pragma mark - dataLabelForPlot
#pragma mark pie chat custom method.This prints label of income expense values..


-(CPTLayer *)dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)index 
{
    
    if ([plot.identifier isEqual:@"Pie Chart 1"])
    {
        
       
        
        int xCalForColor = index;

       
        if(xCalForColor%2==0  && xCalForColor==0 )
        {
           yCalForColor+=30;
        }
        else if(xCalForColor%2==0)
        {
           yCalForColor+=22;
        }
        
                
        
        
        nslog(@"\n xCalForColor = %d",xCalForColor);
        nslog(@"\n yCalForColor = %d",yCalForColor);  
            if([incomeDataforChart count]>3)
            {

               // infoScrollView.backgroundColor = [UIColor greenColor];
                
                if(appDelegate.isIPad)
                {
                   
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                    {
                        infoScrollView.contentSize = CGSizeMake(768,yCalForColor+30);
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
                    {
                        infoScrollView.contentSize = CGSizeMake(1024,yCalForColor+30);
                    }
                    
                    
                    
                    
                }
                else
                {
                    infoScrollView.contentSize = CGSizeMake(320,yCalForColor+22);
                }
                
                
                
            }
            else
            {
                if(appDelegate.isIPad)
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                    {
                         infoScrollView.contentSize = CGSizeMake(768,yCalForColor);
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
                    {
                         infoScrollView.contentSize = CGSizeMake(1024,yCalForColor);
                    }
                   
                }
                else
                {
                    infoScrollView.contentSize = CGSizeMake(320,yCalForColor);
                }
                
                
            }
        
        infoScrollView.scrollEnabled = TRUE;
        infoScrollView.delegate = self;
        infoScrollView.userInteractionEnabled = TRUE;
        nslog(@"\n infoScrollView = %f",infoScrollView.contentSize.height);
        
        infoScrollView.backgroundColor = [UIColor clearColor];
        

            //infoScrollView.backgroundColor = [UIColor redColor];
            
            UILabel*label1 = [[UILabel alloc] init];

        
            UIImageView*color_label_bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dashboard_income_expense_list_color_button.png"]];
        
            if(appDelegate.isIPad)
            {
                //label1.frame = CGRectMake(0,yCalForColor, 15,15);//Pending

                
                if([UIApplication sharedApplication].statusBarOrientation==1 ||[UIApplication sharedApplication].statusBarOrientation==2 )
                {
                    if(xCalForColor%2==0)
                    {
                        label1.frame = CGRectMake(12,yCalForColor +4,40,14);
                    }
                    else
                    {
                        label1.frame = CGRectMake(393,yCalForColor+4,40,14);
                    }
                    

                }
                else if([UIApplication sharedApplication].statusBarOrientation==3 ||[UIApplication sharedApplication].statusBarOrientation==4 )
                {
                    if(xCalForColor%2==0)
                    {
                        label1.frame = CGRectMake(12,yCalForColor +4,40,14);
                    }
                    else
                    {
                        label1.frame = CGRectMake(524,yCalForColor+4,40,14);
                    }

                }
                
                                
                
                
            }
            else
            {
                
                
                if(xCalForColor%2==0)
                {
                    label1.frame = CGRectMake(12,yCalForColor +7,15,7);
                }
                else
                {
                    label1.frame = CGRectMake(169,yCalForColor +7,15,7);
                }

                
                //color_label_bg.frame =CGRectMake(0,yCalForColor,15,20);
                
            }
            

            
        //nslog(@"color index ==== %@",[CPTPieChart customIndex:index]);

        NSString* colorString =  [CPTPieChart customIndex:index];
        NSArray*colorStringArray = [colorString componentsSeparatedByString:@","];
        label1.backgroundColor = [UIColor colorWithRed:[[colorStringArray objectAtIndex:0]floatValue] green:[[colorStringArray objectAtIndex:1]floatValue] blue:[[colorStringArray objectAtIndex:2]floatValue] alpha:1.0];
        
        
        
        
        
        
        [infoScrollView addSubview:label1];
        
        
        //[infoScrollView addSubview:color_label_bg];
        
        [color_label_bg release];
        
        [label1 release];


        UILabel*label2 = [[UILabel alloc] init];

        if(appDelegate.isIPad)
        {
            label2.frame = CGRectMake(20,yCalForColor,180, 20);
            
            if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
            {
                
                if(xCalForColor%2==0)
                {
                    label2.frame = CGRectMake(58,yCalForColor,240, 20);
                }
                else
                {
                    label2.frame = CGRectMake(439,yCalForColor,240, 20);
                }

            }
            else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
            {
                if(xCalForColor%2==0)
                {
                    label2.frame = CGRectMake(58,yCalForColor,367, 20);
                }
                else
                {
                    label2.frame = CGRectMake(570,yCalForColor,367, 20);
                }

            }
            
        }
        else
        {
            
            
            if(xCalForColor%2==0)
            {
                label2.frame = CGRectMake(33,yCalForColor,70, 20);
            }
            else
            {
                label2.frame = CGRectMake(190,yCalForColor,70, 20);
            }

            
           
        }
            


        nslog(@"\n [incomeDataTypeArray objectAtIndex:index] = %@",[incomeDataTypeArray objectAtIndex:index]);    
        label2.text = [incomeDataTypeArray objectAtIndex:index];
        [label2 setBackgroundColor:[UIColor clearColor]];

        if(appDelegate.isIPad)
        {
            [label2 setFont:[UIFont systemFontOfSize:15.0]];
        }
        else
        {
            [label2 setFont:[UIFont systemFontOfSize:10.0]];
        }
            

        label2.textColor = [UIColor blackColor];
            
        [infoScrollView addSubview:label2];
        [label2 release];

        
        
        
        
        

            
        UILabel *label3 = [[UILabel alloc]init];
        [label3 setFont:[UIFont systemFontOfSize:10.0]];
        if(appDelegate.isIPad)
        {
            
            [label3 setFont:[UIFont systemFontOfSize:15.0]];
            if(appDelegate.isIPad)
            {
                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                {
                    if(xCalForColor%2==0)
                    {
                        label3.frame = CGRectMake(301,yCalForColor,80, 20);
                    }
                    else
                    {
                        label3.frame = CGRectMake(685,yCalForColor,80, 20);
                    }

                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
                {
                    if(xCalForColor%2==0)
                    {
                        label3.frame = CGRectMake(428,yCalForColor,80, 20);
                    }
                    else
                    {
                        label3.frame = CGRectMake(941,yCalForColor,80, 20);
                    }

                }
                
                                

            }
            
                        
        }
        else
        {
           
            
            if(xCalForColor%2==0)
            {
                label3.frame = CGRectMake(110,yCalForColor,47, 20);
            }
            else
            {
                label3.frame = CGRectMake(267,yCalForColor,47, 20);
            }

            
            //label3.frame = CGRectMake(195,yCalForColor,60, 20);
        }
        
        label3.text = [NSString stringWithFormat:@"%@ %@", [[incomeDataforChart objectAtIndex:index]valueForKey:[NSString stringWithFormat:@"%@",[incomeDataTypeArray objectAtIndex:index]]],@"%"];

        [label3 setBackgroundColor:[UIColor clearColor]];
        
        label3.textColor = [UIColor blackColor];
        
        
        /*
        infoScrollView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dashboard_income_expense_list_bg.png"]];
        */
        
        
        
        [infoScrollView addSubview:label3];
        [label3 release];
        
        
        CPTLineStyle *borderLineStyle = [CPTLineStyle lineStyle];
        
        borderLineStyle.lineWidth = 1000.0f;
        borderLineStyle.lineColor = [CPTColor whiteColor];
        
        plot.plotArea.borderLineStyle = nil;
        
        [pieChartView addSubview:infoScrollView];
        [pieChartView bringSubviewToFront:infoScrollView];
        [infoScrollView scrollIndicatorInsets];
        [infoScrollView scrollsToTop];
            

    }
    
    else if ([plot.identifier isEqual:@"Pie Chart 2"])
    {
        int xCalForColor1 = index;
        
        
        if(xCalForColor1%2==0 && xCalForColor1==0 )
        {
            yCalForColor1+=30;
        }
        else if(xCalForColor1%2==0)
        {
            yCalForColor1+=22;
        }
        
        infoScrollView1.backgroundColor = [UIColor clearColor];
        
        
        nslog(@"\n xCalForColor1 = %d",xCalForColor1);
        nslog(@"\n yCalForColor1 = %d",yCalForColor1);
        
        
        nslog(@"\n count = %d",[expenseDataForChart count]);
        
        if([expenseDataForChart count]==0)
        {
            pie_chart_label_bg_imageView.hidden = TRUE;
        }
        else
        {
            pie_chart_label_bg_imageView.hidden = FALSE;
        }

        
        
        if([expenseDataForChart count]>3)
        {
            if(appDelegate.isIPad)
            {
                
                
                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                {
                    infoScrollView1.contentSize = CGSizeMake(768,yCalForColor1+30);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
                {
                    infoScrollView1.contentSize = CGSizeMake(1024,yCalForColor1+30);
                }

                
            }
            else
            {
                infoScrollView1.contentSize = CGSizeMake(320,yCalForColor1+22);
               
                
            }
        }
        else
        {
            if(appDelegate.isIPad)
            {
                
                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                {
                    infoScrollView1.contentSize = CGSizeMake(768,yCalForColor1);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
                {
                    infoScrollView1.contentSize = CGSizeMake(1024,yCalForColor1);
                }

                
                
            }
            else
            {
                infoScrollView1.contentSize = CGSizeMake(300,yCalForColor1);
            }
        }
        
       nslog(@"\n infoScrollView1 = %f",infoScrollView1.contentSize.height);
        
        
        UIImageView*color_label_bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dashboard_income_expense_list_color_button.png"]];
        
        UILabel*label1 = [[UILabel alloc] init];
        
        /*
            Sun:0004
            ipad missing.
         */
        
        if(appDelegate.isIPad)
        {
            //label1.frame = CGRectMake(0,yCalForColor1,15,20);
            
            
            if([UIApplication sharedApplication].statusBarOrientation==1 ||[UIApplication sharedApplication].statusBarOrientation==2 )
            {
                if(xCalForColor1%2==0)
                {
                    label1.frame = CGRectMake(12,yCalForColor1 +4,40,14);
                }
                else
                {
                    label1.frame = CGRectMake(393,yCalForColor1+4,40,14);
                }
                
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation==3 ||[UIApplication sharedApplication].statusBarOrientation==4 )
            {
                if(xCalForColor1%2==0)
                {
                    label1.frame = CGRectMake(12,yCalForColor1 +4,40,14);
                }
                else
                {
                    label1.frame = CGRectMake(524,yCalForColor1+4,40,14);
                }
                
            }

            
            
        }
        else
        {
            
            if(xCalForColor1%2==0)
            {
                label1.frame = CGRectMake(12,yCalForColor1 +7,15,7);
            }
            else
            {
                label1.frame = CGRectMake(169,yCalForColor1 +7,15,7);
            }
            
            
            
            //color_label_bg.frame =CGRectMake(0,yCalForColor1,15,20);
            
        }
        nslog(@"color index ==== %@",[CPTPieChart customIndex:index]);
        NSString* colorString =  [CPTPieChart customIndex: index];
        NSArray*colorStringArray = [colorString componentsSeparatedByString:@","];
        label1.backgroundColor = [UIColor colorWithRed:[[colorStringArray objectAtIndex:0]floatValue] green:[[colorStringArray objectAtIndex:1]floatValue] blue:[[colorStringArray objectAtIndex:2]floatValue] alpha:1.0];
        label1.font = [UIFont systemFontOfSize:12.0f];
        
        
        /*
        infoScrollView1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dashboard_income_expense_list_bg.png"]];
        */
        
        [infoScrollView1 addSubview:label1];

        
        
        
        [label1 release];
        [color_label_bg release];
        
        
        UILabel*label2 = [[UILabel alloc] init];
        
        
        if(appDelegate.isIPad)
        {
           // label2.frame = CGRectMake(20,yCalForColor1,180, 20);
            
            if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
            {
                
                if(xCalForColor1%2==0)
                {
                    label2.frame = CGRectMake(58,yCalForColor1,240, 20);
                }
                else
                {
                    label2.frame = CGRectMake(439,yCalForColor1,240, 20);
                }
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
            {
                if(xCalForColor1%2==0)
                {
                    label2.frame = CGRectMake(58,yCalForColor1,367, 20);
                }
                else
                {
                    label2.frame = CGRectMake(570,yCalForColor1,367, 20);
                }
                
            }

            
            
            
        }
        else
        {
           
            
            if(xCalForColor1%2==0)
            {
                label2.frame = CGRectMake(33,yCalForColor1,70, 20);
            }
            else
            {
                label2.frame = CGRectMake(190,yCalForColor1,70, 20);
            }
        }

        
        label2.text = [expenseDataTypeArray objectAtIndex:index];
        
        [label2 setBackgroundColor:[UIColor clearColor]];

        if(appDelegate.isIPad)
        {
            [label2 setFont:[UIFont systemFontOfSize:15.0]];
        }
        else
        {
            [label2 setFont:[UIFont systemFontOfSize:10.0]];
        }
        label2.textColor = [UIColor blackColor];
        [infoScrollView1 addSubview:label2];
        
        [label2 release];
        
        
        UILabel *label3 = [[UILabel alloc]init];
        [label3 setFont:[UIFont systemFontOfSize:10.0]];
        
        if(appDelegate.isIPad)
        {
            label3.frame = CGRectMake(210,yCalForColor1,60, 20);
             [label3 setFont:[UIFont systemFontOfSize:15.0]];
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
            {
                if(xCalForColor1%2==0)
                {
                    label3.frame = CGRectMake(301,yCalForColor1,80, 20);
                }
                else
                {
                    label3.frame = CGRectMake(685,yCalForColor1,80, 20);
                }
                
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
            {
                if(xCalForColor1%2==0)
                {
                    label3.frame = CGRectMake(428,yCalForColor1,80, 20);
                }
                else
                {
                    label3.frame = CGRectMake(941,yCalForColor1,80, 20);
                }
                
            }
            
            
        }
        else
        {
           
            
            if(xCalForColor1%2==0)
            {
                label3.frame = CGRectMake(110,yCalForColor1,47, 20);
            }
            else
            {
                label3.frame = CGRectMake(267,yCalForColor1,47, 20);
            }
            
        }

         nslog(@"\n infoScrollView1 content = %f",infoScrollView1.contentSize.height);
        
        
        
        label3.text = [NSString stringWithFormat:@"%@ %@", [[expenseDataForChart objectAtIndex:index]valueForKey:[NSString stringWithFormat:@"%@",[expenseDataTypeArray objectAtIndex:index]]],@"%"];
        
        
        //label3.text = [label3.text stringByAppendingFormat:@"%@",@"%"];
        
        
        [label3 setBackgroundColor:[UIColor clearColor]];
        
        label3.textColor = [UIColor blackColor];
        [infoScrollView1 addSubview:label3];
        [label3 release];
        
        //pieChartView.backgroundColor = [UIColor clearColor];
        
        [pieChartView addSubview:infoScrollView1];
        
        nslog(@"\n color = %@",[CPTPieChart customIndex: index]);

    }
    
    
    [self.view bringSubviewToFront:pieChartView];
    [pieChartView bringSubviewToFront:infoScrollView];
    
    //[self.view addSubview:infoScrollView];
    
    
    /*
    
    CGPoint bottomOffset = CGPointMake(0, infoScrollView.contentSize.height - infoScrollView.bounds.size.height);
    [infoScrollView setContentOffset:bottomOffset animated:YES];
    
    
    UITapGestureRecognizer *messagesTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messagesBucketTap:)];
    [messagesTap setDelegate:self];
    [messagesTap setNumberOfTapsRequired:1];
   
    [infoScrollView addGestureRecognizer:messagesTap];
    [messagesTap release];
    */
    return nil;
}

#pragma mark - messagesBucketTap

-(void)messagesBucketTap:(id)sender
{
    nslog(@"\n tapped..");
}


#pragma mark -
#pragma mark incomeExpenseFilterSegmentControl_Changed method


-(IBAction)income_expense_segment_change_through_click:(id)sender
{
    
    UIButton*tmpButton = (UIButton*)sender;
    
    isIncomeOrExpenseSelected = (tmpButton.tag-6000);
    
    nslog(@"\n isIncomeOrExpenseSelected = %d",isIncomeOrExpenseSelected);
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation ==1 || [UIApplication sharedApplication].statusBarOrientation ==2)
        {
            pieChartScrollView.frame = CGRectMake(0,118,768,830);
        }
        else if([UIApplication sharedApplication].statusBarOrientation ==3 || [UIApplication sharedApplication].statusBarOrientation ==4)
        {
            pieChartScrollView.frame = CGRectMake(0,118,1024,600);
        }

    }
    
    
    
    
    [self generateIncomeExpenseReport];

    
    
    
    
}

#pragma mark - generateIncomeExpenseReport
#pragma mark called when income or expense clicked.

-(void)generateIncomeExpenseReport
{
    
    if(isIncomeOrExpenseSelected == 0)
    {
        isIncome =TRUE;
        
        if(appDelegate.isIPad)
        {
            [imgIncomeButton setImage:[UIImage imageNamed:@"dash_ipad_income_selected.png"] forState:UIControlStateNormal];
            [imgIncomeButton setImage:[UIImage imageNamed:@"dash_ipad_income_selected.png"] forState:UIControlStateHighlighted];
            
            
            [imgExpenseButton setImage:[UIImage imageNamed:@"dash_ipad_expense.png"] forState:UIControlStateNormal];
            [imgExpenseButton setImage:[UIImage imageNamed:@"dash_ipad_expense.png"] forState:UIControlStateHighlighted];
        }
        else
        {
          
            [imgIncomeButton setImage:[UIImage imageNamed:@"income_dash_selected.png"] forState:UIControlStateNormal];
            [imgIncomeButton setImage:[UIImage imageNamed:@"income_dash_selected.png"] forState:UIControlStateHighlighted];
            
            [imgExpenseButton setImage:[UIImage imageNamed:@"expense_dash.png"] forState:UIControlStateNormal];
            [imgExpenseButton setImage:[UIImage imageNamed:@"expense_dash.png"] forState:UIControlStateHighlighted];
        }
        
       
        
        
        if (selectedBarTypeSegment == 0)//Pie Chart
        {
            [tableViewChart removeFromSuperview];
            [barChartView removeFromSuperview];
         
            
            
            /*Starts:Hiding all Expense Elemetns*/
            
            
            expenseGraphView.hidden = TRUE;
            noExpensePie.hidden = TRUE;
            lblExpenseSummary.hidden = TRUE;
            imgExpenseSummary.hidden = TRUE;
            /*Ends:Hiding all Expense Elemetns*/
            
            
            /*Starts:Showing all Income Elemetns*/
            lblIncomeSummary.hidden = NO;
            imgIncomeSummary.hidden = NO;
            
            
            /*shadow and hover image for pie-chart*/
            
            piechart_shadow_imageView.hidden = TRUE;
            piechart_hover_imageView.hidden = TRUE;
            
            expense_piechart_shadow_imageView.hidden = TRUE;
            expense_piechart_hover_imageView.hidden = TRUE;

            
            

            
            if ([incomeDataforChart count]==0)
            {
                incomeGraphView.hidden = TRUE;
                noIncomePie.hidden = FALSE;
                
                pie_chart_label_bg_imageView.hidden = TRUE;
                piechart_shadow_imageView.hidden = TRUE;
                piechart_hover_imageView.hidden = TRUE;
                
            }
            else
            {
                incomeGraphView.hidden = FALSE;
                noIncomePie.hidden = TRUE;
                pie_chart_label_bg_imageView.hidden = FALSE;
                
                piechart_shadow_imageView.hidden = FALSE;
                piechart_hover_imageView.hidden = FALSE;
                
                
            }
            /*Ends:Showing all Income Elemetns*/
            
            infoScrollView.hidden = NO;
            infoScrollView1.hidden = YES;
            
            
            
            
            
            [pieChartScrollView addSubview:pieChartView];
           
            
            if(appDelegate.isIPad)
            {
                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                {
                   // pieChartView.frame = CGRectMake(0,0,768,1024);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
                {
                   // pieChartView.frame = CGRectMake(0, 0,1024,768);
                }
                
            }
            else
            {
                
                if(appDelegate.isIphone5)
                {
                    pieChartView.frame = CGRectMake(0, 0,320,348);
                }
                else
                {
                    pieChartView.frame = CGRectMake(0, 0,320,300);
                }

                
            }
             
            /*Sun:0004
             
             */
            
            [pieChartScrollView setContentSize:CGSizeMake(pieChartView.frame.size.width, 0)];
            
        }
        
        else if (selectedBarTypeSegment == 1)//Bar Chart
        {
            [tableViewChart removeFromSuperview];
            [pieChartView removeFromSuperview];
            
            incomeBarAxis.hidden = FALSE;
            
            /*Hiding Expense Bar Elements*/
            expensePercentageImg.hidden = TRUE;
            expenseBarView.hidden = TRUE;
            noExpenseBar.hidden = TRUE;
            expenseBarAxis.hidden = TRUE;
            expenseBarScrollView.hidden = TRUE;
            
            lblBarExpenseSummary.hidden = TRUE;
            imgBarExpenseSummary.hidden = TRUE;
            
            
            lblBarIncomeSummary.hidden = FALSE;
            imgBarIncomeSummary.hidden = FALSE;
            
            
            /*shadow and hover image for pie-chart*/
            
            piechart_shadow_imageView.hidden = TRUE;
            piechart_hover_imageView.hidden = TRUE;
            
            expense_piechart_shadow_imageView.hidden = TRUE;
            expense_piechart_hover_imageView.hidden = TRUE;
            
            
            if ([incomeDataforChart count]==0)
            {
                incomePercentageImg.hidden = TRUE;
                incomeBarView.hidden = true;
                
                
                
                noIncomeBar.hidden = FALSE;
                incomeBarAxis.hidden = TRUE;
                
                
                
                
            }
            else
            {
                incomePercentageImg.hidden = FALSE;
                incomeBarView.hidden = FALSE;
                noIncomeBar.hidden = TRUE;
                incomeBarAxis.hidden = FALSE;
                incomeBarScrollView.hidden = FALSE;
                
                
                
            }
            
            
            
            [pieChartScrollView addSubview:barChartView];
            
            //[pieChartScrollView setContentSize:CGSizeMake(barChartView.frame.size.width, 270.0f)];
            
        }
        
        
        else if (selectedBarTypeSegment == 2)//table
        {
            [barChartView removeFromSuperview];
            [pieChartView removeFromSuperview];
           
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation ==1 || [UIApplication sharedApplication].statusBarOrientation ==2)
                {
                    pieChartScrollView.frame = CGRectMake(0,40,768,830);
                }
                else if([UIApplication sharedApplication].statusBarOrientation ==3 || [UIApplication sharedApplication].statusBarOrientation ==4)
                {
                    pieChartScrollView.frame = CGRectMake(0,40,1024,600);
                }
                
                
            }
            
            

            
            
            [tblView reloadData];
            if(appDelegate.isIPad)
            {
                
                
                
                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    tableViewChart.frame = CGRectMake(0, 0,768,900);
                    tblView.frame = CGRectMake(0, 0,768,900);
                   
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    tableViewChart.frame = CGRectMake(0, 0,1024,768);
                    tblView.frame = CGRectMake(0, 0,1024,768);
                   
                }
                
                
            }
            else
            {
               // [pieChartScrollView setContentSize:CGSizeMake(320, 300)];
            }
            [tblView setContentOffset:CGPointZero animated:YES];
            [pieChartScrollView addSubview:tableViewChart];
            pieChartScrollView.backgroundColor = [UIColor clearColor];
            

        }
        
        
    }
    else if(isIncomeOrExpenseSelected == 1)
    {
        isIncome =FALSE;
        
        
        if(appDelegate.isIPad)
        {
            [imgIncomeButton setImage:[UIImage imageNamed:@"dash_ipad_income.png"] forState:UIControlStateNormal];
            [imgIncomeButton setImage:[UIImage imageNamed:@"dash_ipad_income.png"] forState:UIControlStateHighlighted];
            
            [imgExpenseButton setImage:[UIImage imageNamed:@"dash_ipad_expense_selected.png"] forState:UIControlStateNormal];
            [imgExpenseButton setImage:[UIImage imageNamed:@"dash_ipad_expense_selected.png"] forState:UIControlStateHighlighted];
        }
        else
        {
            [imgIncomeButton setImage:[UIImage imageNamed:@"income_dash.png"] forState:UIControlStateNormal];
            [imgIncomeButton setImage:[UIImage imageNamed:@"income_dash.png"] forState:UIControlStateHighlighted];
            
            [imgExpenseButton setImage:[UIImage imageNamed:@"expense_dash_selected.png"] forState:UIControlStateNormal];
            [imgExpenseButton setImage:[UIImage imageNamed:@"expense_dash_selected.png"] forState:UIControlStateHighlighted];
        }
        
        
        if (selectedBarTypeSegment == 0)
        {
            [tableViewChart removeFromSuperview];
            [barChartView removeFromSuperview];
            
            
            
            /*Starts:Hiding all Income Elemetns*/
            
            
            incomeGraphView.hidden = TRUE;
            noIncomePie.hidden = TRUE;
            lblIncomeSummary.hidden = TRUE;
            imgIncomeSummary.hidden = TRUE;
            /*Ends:Hiding all Expense Elemetns*/
            
            
            /*Starts:Showing all Income Elemetns*/
            lblExpenseSummary.hidden = NO;
            imgExpenseSummary.hidden = NO;
            
            
            /*shadow and hover image for pie-chart*/
            
            piechart_shadow_imageView.hidden = TRUE;
            piechart_hover_imageView.hidden = TRUE;
            
            expense_piechart_shadow_imageView.hidden = TRUE;
            expense_piechart_hover_imageView.hidden = TRUE;
            
            if ([expenseDataForChart count]==0)
            {
                expenseGraphView.hidden = TRUE;
                noExpensePie.hidden = FALSE;
                
                expense_piechart_shadow_imageView.hidden = TRUE;
                expense_piechart_hover_imageView.hidden = TRUE;
                pie_chart_label_bg_imageView.hidden = TRUE;
                
                
            }
            else
            {
                expenseGraphView.hidden = FALSE;
                noExpensePie.hidden = TRUE;
                
                
                expense_piechart_shadow_imageView.hidden = FALSE;
                expense_piechart_hover_imageView.hidden = FALSE;
                pie_chart_label_bg_imageView.hidden = FALSE;
                
                
                
                
            }
            /*Ends:Showing all expense Elemetns*/
            infoScrollView.hidden = YES;
            infoScrollView1.hidden = NO;
            
            
            
            nslog(@"\n NSStringFromCGRect(pieChartView.frame) = %@",NSStringFromCGRect(pieChartView.frame));
            
            [pieChartScrollView addSubview:pieChartView];
            
            
            
            
            
            if(appDelegate.isIPad)
            {
                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                {
                   // pieChartView.frame = CGRectMake(0, 0,768,1024);
                }
                else
                {
                   // pieChartView.frame = CGRectMake(0, 0,1024,768);
                }
                
            }
            else
            {
                if(appDelegate.isIphone5)
                {
                    pieChartView.frame = CGRectMake(0, 0,320,348);
                }
                else
                {
                    pieChartView.frame = CGRectMake(0, 0,320,300);
                }

            }
            
           // [pieChartScrollView setContentSize:CGSizeMake(pieChartView.frame.size.width, pieChartView.frame.size.height)];
            
            /*Sun:0004
             
             */
            
            [pieChartScrollView setContentSize:CGSizeMake(pieChartView.frame.size.width, 0)];
            
            
        }
        
        
        else if (selectedBarTypeSegment == 1)//Bar chart
        {
            [tableViewChart removeFromSuperview];
            [pieChartView removeFromSuperview];
            
            
            /*Hiding Expense Bar Elements*/
            incomePercentageImg.hidden = TRUE;
            incomeBarView.hidden = TRUE;
            noIncomeBar.hidden = TRUE;
            incomeBarAxis.hidden = TRUE;
            lblBarIncomeSummary.hidden = TRUE;
            imgBarIncomeSummary.hidden = TRUE;
            incomeBarScrollView.hidden = TRUE;
            
            lblBarExpenseSummary.hidden = FALSE;
            imgBarExpenseSummary.hidden = FALSE;
            if ([expenseDataForChart count]==0)
            {
                expensePercentageImg.hidden = TRUE;
                expenseBarView.hidden = TRUE;
                noExpenseBar.hidden = FALSE;
                expenseBarAxis.hidden = TRUE;
                
                pie_chart_label_bg_imageView.hidden = TRUE;
            }
            else
            {
                expensePercentageImg.hidden = FALSE;
                expenseBarView.hidden = FALSE;
                noExpenseBar.hidden = TRUE;
                expenseBarAxis.hidden = FALSE;
                expenseBarScrollView.hidden = FALSE;
                
                pie_chart_label_bg_imageView.hidden = FALSE;
            }
            
            
            
            [pieChartScrollView addSubview:barChartView];
            
            /*
             [pieChartScrollView setContentSize:CGSizeMake(barChartView.frame.size.width, barChartView.frame.size.height+20.0f)];
             */
            //[pieChartScrollView setContentSize:CGSizeMake(barChartView.frame.size.width, 270.0f)];
            
        }
        
        
        else if (selectedBarTypeSegment == 2)//table
        {
            [barChartView removeFromSuperview];
            [pieChartView removeFromSuperview];
            
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation ==1 || [UIApplication sharedApplication].statusBarOrientation ==2)
                {
                    pieChartScrollView.frame = CGRectMake(0,40,768,830);
                }
                else if([UIApplication sharedApplication].statusBarOrientation ==3 || [UIApplication sharedApplication].statusBarOrientation ==4)
                {
                    pieChartScrollView.frame = CGRectMake(0,40,1024,600);
                }
            }
            
            

            
            
            [tblView reloadData];
            if(appDelegate.isIPad)//This is pending...
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    tableViewChart.frame = CGRectMake(0, 0,768,900);
                    tblView.frame = CGRectMake(0, 0,768,900);
                   // [pieChartScrollView setContentSize:CGSizeMake(700,900)];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    tableViewChart.frame = CGRectMake(0, 0,1024,768);
                    tblView.frame = CGRectMake(0, 0,1024,768);
                  //  [pieChartScrollView setContentSize:CGSizeMake(700,768)];
                }
                
                
            }
            else
            {
               [pieChartScrollView setContentSize:CGSizeMake(320, 300)];
            }
            [pieChartScrollView addSubview:tableViewChart];
            
            
            //  [pieChartScrollView setContentSize:CGSizeMake(tableViewChart.frame.size.width, tableViewChart.frame.size.height)];
            
            
        }
        
        
    }
    
}



-(IBAction)incomeExpenseFilterSegmentControl_Changed:(id)sender
{
    UISegmentedControl *tempSegementedControl = (UISegmentedControl*)sender;
    if(tempSegementedControl.selectedSegmentIndex == 0)
    {
        isIncome =TRUE;
        
        if (segment.selectedSegmentIndex == 0)//Pie Chart
        {
            [tableViewChart removeFromSuperview];
            [barChartView removeFromSuperview];


            /*Starts:Hiding all Expense Elemetns*/
            
            
            expenseGraphView.hidden = TRUE;
            noExpensePie.hidden = TRUE;
            lblExpenseSummary.hidden = TRUE;
            imgExpenseSummary.hidden = TRUE;
            /*Ends:Hiding all Expense Elemetns*/
            
            
            /*Starts:Showing all Income Elemetns*/
            lblIncomeSummary.hidden = NO;
            imgIncomeSummary.hidden = NO;
            if ([incomeDataforChart count]==0)
            {
                incomeGraphView.hidden = TRUE;
                noIncomePie.hidden = FALSE;
                
                pie_chart_label_bg_imageView.hidden = TRUE;
                piechart_shadow_imageView.hidden = TRUE;
                piechart_hover_imageView.hidden = TRUE;
                
            }
            else
            {
                incomeGraphView.hidden = FALSE;
                noIncomePie.hidden = TRUE;
                
                pie_chart_label_bg_imageView.hidden = FALSE;
                piechart_shadow_imageView.hidden = FALSE;
                piechart_hover_imageView.hidden = FALSE;
                
            }
            /*Ends:Showing all Income Elemetns*/
            
            infoScrollView.hidden = NO; 
            infoScrollView1.hidden = YES;

            [pieChartScrollView addSubview:pieChartView];
            
            
            if(appDelegate.isIPad)
            {
                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                {
                   // pieChartView.frame = CGRectMake(0, 0,768,1024);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
                {
                   // pieChartView.frame = CGRectMake(0, 0,1024,768);
                }
                
            }
            else 
            {
                if(appDelegate.isIphone5)
                {
                    pieChartView.frame = CGRectMake(0, 0,320,348);
                }
                else
                {
                    pieChartView.frame = CGRectMake(0, 0,320,300);
                }

            }
            
            
            
            //[pieChartScrollView setContentSize:CGSizeMake(pieChartView.frame.size.width, pieChartView.frame.size.height)];
            
            
        }
        
        else if (segment.selectedSegmentIndex == 1)//Bar Chart
        {
            [tableViewChart removeFromSuperview];
            [pieChartView removeFromSuperview];
            
            incomeBarAxis.hidden = FALSE;

            /*Hiding Expense Bar Elements*/
            expensePercentageImg.hidden = TRUE;
            expenseBarView.hidden = TRUE;
            noExpenseBar.hidden = TRUE;
            expenseBarAxis.hidden = TRUE;
            expenseBarScrollView.hidden = TRUE;
            
            lblBarExpenseSummary.hidden = TRUE;
            imgBarExpenseSummary.hidden = TRUE;
            

            lblBarIncomeSummary.hidden = FALSE;
            imgBarIncomeSummary.hidden = FALSE;
            
            if ([incomeDataforChart count]==0)
            {
                incomePercentageImg.hidden = TRUE;
                incomeBarView.hidden = true;
                noIncomeBar.hidden = FALSE;
                incomeBarAxis.hidden = TRUE;
            }
            else
            {
                incomePercentageImg.hidden = FALSE;
                incomeBarView.hidden = FALSE;
                noIncomeBar.hidden = TRUE;
                incomeBarAxis.hidden = FALSE;
                incomeBarScrollView.hidden = FALSE;
            }

            
            
            [pieChartScrollView addSubview:barChartView];
            /*
            [pieChartScrollView setContentSize:CGSizeMake(barChartView.frame.size.width, barChartView.frame.size.height+20.0f)];
             */

            
            
            //[pieChartScrollView setContentSize:CGSizeMake(barChartView.frame.size.width, 270.0f)];

        }

        
        else if (segment.selectedSegmentIndex == 2)
        {
            [barChartView removeFromSuperview];
            [pieChartView removeFromSuperview];
            
            [tblView reloadData];
            if(appDelegate.isIPad)//Pending..
            {
                
                
                
                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    tableViewChart.frame = CGRectMake(0, 0,768,900);
                    tblView.frame = CGRectMake(0, 0,768,900);
                    [pieChartScrollView setContentSize:CGSizeMake(700,900)];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    tableViewChart.frame = CGRectMake(0, 0,1024,768);
                    tblView.frame = CGRectMake(0, 0,1024,768);
                    [pieChartScrollView setContentSize:CGSizeMake(700,768)];
                }
                
                
            }
            else
            {
               // [pieChartScrollView setContentSize:CGSizeMake(320, 300)];
            }
            [pieChartScrollView addSubview:tableViewChart];
            
            
            //  [pieChartScrollView setContentSize:CGSizeMake(tableViewChart.frame.size.width, tableViewChart.frame.size.height)];
        }

        
    }
    else if(tempSegementedControl.selectedSegmentIndex == 1)
    {
        isIncome =FALSE;
        
        
        if (segment.selectedSegmentIndex == 0)
        {
            [tableViewChart removeFromSuperview];
            [barChartView removeFromSuperview];
            
                       
            
            /*Starts:Hiding all Income Elemetns*/
            
            
            incomeGraphView.hidden = TRUE;
            noIncomePie.hidden = TRUE;
            lblIncomeSummary.hidden = TRUE;
            imgIncomeSummary.hidden = TRUE;
            /*Ends:Hiding all Expense Elemetns*/
            
            
            /*Starts:Showing all Income Elemetns*/
            lblExpenseSummary.hidden = NO;
            imgExpenseSummary.hidden = NO;
            if ([expenseDataForChart count]==0)
            {
                expenseGraphView.hidden = TRUE;
                noExpensePie.hidden = FALSE;
                
                
                expense_piechart_shadow_imageView.hidden = TRUE;
                expense_piechart_hover_imageView.hidden = TRUE;
                
                pie_chart_label_bg_imageView.hidden = TRUE;
                
            }
            else
            {
                expenseGraphView.hidden = FALSE;
                noExpensePie.hidden = TRUE;
                
                expense_piechart_shadow_imageView.hidden = FALSE;
                expense_piechart_hover_imageView.hidden = FALSE;
                
                pie_chart_label_bg_imageView.hidden = FALSE;
                
            }
            /*Ends:Showing all expense Elemetns*/
            infoScrollView.hidden = YES; 
            infoScrollView1.hidden = NO; 
            [pieChartScrollView addSubview:pieChartView];
            
            if(appDelegate.isIPad)
            {
                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                {
                    pieChartView.frame = CGRectMake(0, 0,768,1024);
                }
                else 
                {
                    pieChartView.frame = CGRectMake(0, 0,1024,768);
                }
                
            }
            else 
            {
                if(appDelegate.isIphone5)
                {
                    pieChartView.frame = CGRectMake(0, 0,320,348);
                }
                else
                {
                    pieChartView.frame = CGRectMake(0, 0,320,300);
                }

            }
            
            [pieChartScrollView setContentSize:CGSizeMake(pieChartView.frame.size.width, pieChartView.frame.size.height)];
            
            
            
        }

        
        else if (segment.selectedSegmentIndex == 1)//Bar chart
        {
            [tableViewChart removeFromSuperview];
            [pieChartView removeFromSuperview];
            
            
            /*Hiding Expense Bar Elements*/
            incomePercentageImg.hidden = TRUE;
            incomeBarView.hidden = TRUE;
            noIncomeBar.hidden = TRUE;
            incomeBarAxis.hidden = TRUE;
            lblBarIncomeSummary.hidden = TRUE;
            imgBarIncomeSummary.hidden = TRUE;
            incomeBarScrollView.hidden = TRUE;
            
            lblBarExpenseSummary.hidden = FALSE;
            imgBarExpenseSummary.hidden = FALSE;
            if ([expenseDataForChart count]==0)
            {
                expensePercentageImg.hidden = TRUE;
                expenseBarView.hidden = TRUE;
                noExpenseBar.hidden = FALSE;
                expenseBarAxis.hidden = TRUE;
            }
            else
            {
                expensePercentageImg.hidden = FALSE;
                expenseBarView.hidden = FALSE;
                noExpenseBar.hidden = TRUE;
                expenseBarAxis.hidden = FALSE;
                expenseBarScrollView.hidden = FALSE;
            }

            
            
            [pieChartScrollView addSubview:barChartView];
            
            /*
            [pieChartScrollView setContentSize:CGSizeMake(barChartView.frame.size.width, barChartView.frame.size.height+20.0f)];
            */
            [pieChartScrollView setContentSize:CGSizeMake(barChartView.frame.size.width, 270.0f)];
            
        }
 
                    
        else if (segment.selectedSegmentIndex == 2)
        {
            [barChartView removeFromSuperview];
            [pieChartView removeFromSuperview];
            
            [tblView reloadData];
            if(appDelegate.isIPad)//This is pending...
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    tableViewChart.frame = CGRectMake(0, 0,768,900);
                    tblView.frame = CGRectMake(0, 0,768,900);
                    [pieChartScrollView setContentSize:CGSizeMake(700,900)];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    tableViewChart.frame = CGRectMake(0, 0,1024,768);
                    tblView.frame = CGRectMake(0, 0,1024,768);
                    [pieChartScrollView setContentSize:CGSizeMake(700,768)];
                }
                
                
            }
            else
            {
                [pieChartScrollView setContentSize:CGSizeMake(320, 300)];    
            }
            [pieChartScrollView addSubview:tableViewChart];
            
            
            //  [pieChartScrollView setContentSize:CGSizeMake(tableViewChart.frame.size.width, tableViewChart.frame.size.height)];
            
            
        }

        
    }

} 


#pragma mark -
#pragma mark segment change

-(void)segment_change_through_click
{
    if (segment.selectedSegmentIndex == 0)
    {
        
        [tableViewChart removeFromSuperview];
        [barChartView removeFromSuperview];
        
        if(isIncome)
        {
            /*Starts:Hiding all Expense Elemetns*/
            
            
            expenseGraphView.hidden = TRUE;
            noExpensePie.hidden = TRUE;
            lblExpenseSummary.hidden = TRUE;
            imgExpenseSummary.hidden = TRUE;
            /*Ends:Hiding all Expense Elemetns*/
            
            
            /*Starts:Showing all Income Elemetns*/
            lblIncomeSummary.hidden = NO;
            imgIncomeSummary.hidden = NO;
            if ([incomeDataforChart count]==0)
            {
                incomeGraphView.hidden = TRUE;
                noIncomePie.hidden = FALSE;
                
                pie_chart_label_bg_imageView.hidden = TRUE;
                piechart_shadow_imageView.hidden = TRUE;
                piechart_hover_imageView.hidden = TRUE;
                
            }
            else
            {
                incomeGraphView.hidden = FALSE;
                noIncomePie.hidden = TRUE;
                
                pie_chart_label_bg_imageView.hidden = FALSE;
                piechart_shadow_imageView.hidden = FALSE;
                piechart_hover_imageView.hidden = FALSE;
                
            }
            /*Ends:Showing all Income Elemetns*/
            
            infoScrollView.hidden = NO; 
            infoScrollView1.hidden = YES;
            
            
            
        }
        else
        {
            /*Starts:Hiding all Income Elemetns*/
            
            
            incomeGraphView.hidden = TRUE;
            noIncomePie.hidden = TRUE;
            lblIncomeSummary.hidden = TRUE;
            imgIncomeSummary.hidden = TRUE;
            /*Ends:Hiding all Expense Elemetns*/
            
            
            /*Starts:Showing all Income Elemetns*/
            lblExpenseSummary.hidden = NO;
            imgExpenseSummary.hidden = NO;
            if ([expenseDataForChart count]==0)
            {
                expenseGraphView.hidden = TRUE;
                noExpensePie.hidden = FALSE;
                
                pie_chart_label_bg_imageView.hidden = TRUE;
                expense_piechart_shadow_imageView.hidden = TRUE;
                expense_piechart_hover_imageView.hidden = TRUE;
                
            }
            else
            {
                expenseGraphView.hidden = FALSE;
                noExpensePie.hidden = TRUE;
                
                pie_chart_label_bg_imageView.hidden = TRUE;
                expense_piechart_shadow_imageView.hidden = FALSE;
                expense_piechart_hover_imageView.hidden = FALSE;
                
            }
            /*Ends:Showing all Income Elemetns*/
            infoScrollView.hidden = YES; 
            infoScrollView1.hidden = NO; 
            
        }
        
        
        [pieChartScrollView addSubview:pieChartView];
        

        /*
        if(!appDelegate.isIPad)
        {
            if(appDelegate.isIphone5)
            {
                pieChartView.frame = CGRectMake(0, 0,320,348);
            }
            else
            {
                pieChartView.frame = CGRectMake(0, 0,320,300);
            }

        }
        */
        
        
        
        
        
        [pieChartScrollView setContentSize:CGSizeMake(pieChartView.frame.size.width, pieChartView.frame.size.height)];
        
        
        
    }
    
    else if (segment.selectedSegmentIndex == 1)
    {
        [tableViewChart removeFromSuperview];
        [pieChartView removeFromSuperview];
        if(isIncome)
        {
            
            incomeBarAxis.hidden = FALSE;
            
            /*Hiding Expense Bar Elements*/
            expensePercentageImg.hidden = TRUE;
            expenseBarView.hidden = TRUE;
            noExpenseBar.hidden = TRUE;
            expenseBarAxis.hidden = TRUE;
            expenseBarScrollView.hidden = TRUE;
            
            lblBarExpenseSummary.hidden = TRUE;
            imgBarExpenseSummary.hidden = TRUE;
            
            
            lblBarIncomeSummary.hidden = FALSE;
            imgBarIncomeSummary.hidden = FALSE;
            
            if ([incomeDataforChart count]==0)
            {
                incomePercentageImg.hidden = TRUE;
                incomeBarView.hidden = true;
                noIncomeBar.hidden = FALSE;
                incomeBarAxis.hidden = TRUE;
            }
            else
            {
                incomePercentageImg.hidden = FALSE;
                incomeBarView.hidden = FALSE;
                noIncomeBar.hidden = TRUE;
                incomeBarAxis.hidden = FALSE;
                incomeBarScrollView.hidden = FALSE;
            }
            
            
        }
        else
        {
            
            /*Hiding Expense Bar Elements*/
            incomePercentageImg.hidden = TRUE;
            incomeBarView.hidden = TRUE;
            noIncomeBar.hidden = TRUE;
            incomeBarAxis.hidden = TRUE;
            lblBarIncomeSummary.hidden = TRUE;
            imgBarIncomeSummary.hidden = TRUE;
            incomeBarScrollView.hidden = TRUE;
            
            lblBarExpenseSummary.hidden = FALSE;
            imgBarExpenseSummary.hidden = FALSE;
            if ([expenseDataForChart count]==0)
            {
                expensePercentageImg.hidden = TRUE;
                expenseBarView.hidden = TRUE;
                noExpenseBar.hidden = FALSE;
                expenseBarAxis.hidden = TRUE;
            }
            else
            {
                expensePercentageImg.hidden = FALSE;
                expenseBarView.hidden = FALSE;
                noExpenseBar.hidden = TRUE;
                expenseBarAxis.hidden = FALSE;
                expenseBarScrollView.hidden = FALSE;
            }
            
        }
        
        
        [pieChartScrollView addSubview:barChartView];
        /*
         [pieChartScrollView setContentSize:CGSizeMake(barChartView.frame.size.width, barChartView.frame.size.height+20.0f)];
         */
        
        [pieChartScrollView setContentSize:CGSizeMake(barChartView.frame.size.width, 270.0f)];
        
    }
    
    
    else if (segment.selectedSegmentIndex == 2)
    {
        [barChartView removeFromSuperview];
        [pieChartView removeFromSuperview];
        
        [tblView reloadData];
        if(appDelegate.isIPad)
        {
            
            
            
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                tableViewChart.frame = CGRectMake(0, 0,768,900);
                tblView.frame = CGRectMake(0, 0,768,900);
                [pieChartScrollView setContentSize:CGSizeMake(700,900)];
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                tableViewChart.frame = CGRectMake(0, 0,1024,768);
                tblView.frame = CGRectMake(0, 0,1024,768);
                [pieChartScrollView setContentSize:CGSizeMake(700,768)];
            }
            
            
        }
        else
        {
            [pieChartScrollView setContentSize:CGSizeMake(320, 300)];    
        }
        
        [pieChartScrollView addSubview:tableViewChart];
        
        
        //  [pieChartScrollView setContentSize:CGSizeMake(tableViewChart.frame.size.width, tableViewChart.frame.size.height)];
        
        
    }  
}

-(IBAction)segment_change
{
    if (segment.selectedSegmentIndex == 0)
    {
        
        [segment setImage:[UIImage imageNamed:@"piechart_selected.png"] forSegmentAtIndex:0];    
        [segment setImage:[UIImage imageNamed:@"barchart.png"] forSegmentAtIndex:1];
        [segment setImage:[UIImage imageNamed:@"tableview.png"] forSegmentAtIndex:2];    

        
        [tableViewChart removeFromSuperview];
        [barChartView removeFromSuperview];
        
        if(isIncome)
        {
            /*Starts:Hiding all Expense Elemetns*/
            
            
            expenseGraphView.hidden = TRUE;
            noExpensePie.hidden = TRUE;
            lblExpenseSummary.hidden = TRUE;
            imgExpenseSummary.hidden = TRUE;
            /*Ends:Hiding all Expense Elemetns*/
            
            
            /*Starts:Showing all Income Elemetns*/
            lblIncomeSummary.hidden = NO;
            imgIncomeSummary.hidden = NO;
            if ([incomeDataforChart count]==0)
            {
                incomeGraphView.hidden = TRUE;
                noIncomePie.hidden = FALSE;
                
                pie_chart_label_bg_imageView.hidden = TRUE;
                piechart_shadow_imageView.hidden = TRUE;
                piechart_hover_imageView.hidden = TRUE;
                
            }
            else
            {
                incomeGraphView.hidden = FALSE;
                noIncomePie.hidden = TRUE;
                
                pie_chart_label_bg_imageView.hidden = FALSE;
                piechart_shadow_imageView.hidden = FALSE;
                piechart_hover_imageView.hidden = FALSE;
                
            }
            /*Ends:Showing all Income Elemetns*/
            
            infoScrollView.hidden = NO; 
            infoScrollView1.hidden = YES;
            
           

        }
        else
        {
            /*Starts:Hiding all Income Elemetns*/
            
            
            incomeGraphView.hidden = TRUE;
            noIncomePie.hidden = TRUE;
            lblIncomeSummary.hidden = TRUE;
            imgIncomeSummary.hidden = TRUE;
            /*Ends:Hiding all Expense Elemetns*/
            
            
            /*Starts:Showing all Income Elemetns*/
            lblExpenseSummary.hidden = NO;
            imgExpenseSummary.hidden = NO;
            if ([expenseDataForChart count]==0)
            {
                expenseGraphView.hidden = TRUE;
                noExpensePie.hidden = FALSE;
                pie_chart_label_bg_imageView.hidden = TRUE;
                
                expense_piechart_shadow_imageView.hidden = TRUE;
                expense_piechart_hover_imageView.hidden = TRUE;
                
            }
            else
            {
                expenseGraphView.hidden = FALSE;
                noExpensePie.hidden = TRUE;
                pie_chart_label_bg_imageView.hidden = FALSE;
                
                expense_piechart_shadow_imageView.hidden = FALSE;
                expense_piechart_hover_imageView.hidden = FALSE;
                
            }
            /*Ends:Showing all Income Elemetns*/
            infoScrollView.hidden = YES; 
            infoScrollView1.hidden = NO; 

        }

        
        [pieChartScrollView addSubview:pieChartView];
        //pieChartView.frame = CGRectMake(0, 0,320,300);
        [pieChartScrollView setContentSize:CGSizeMake(pieChartView.frame.size.width, pieChartView.frame.size.height)];

        
        
    }
    
    else if (segment.selectedSegmentIndex == 1)
    {
        
        [segment setImage:[UIImage imageNamed:@"piechart.png"] forSegmentAtIndex:0];    
        [segment setImage:[UIImage imageNamed:@"barchart_selected.png"] forSegmentAtIndex:1];
        [segment setImage:[UIImage imageNamed:@"tableview.png"] forSegmentAtIndex:2];    

        
        [tableViewChart removeFromSuperview];
        [pieChartView removeFromSuperview];
        if(isIncome)
        {
            
            incomeBarAxis.hidden = FALSE;
            
            /*Hiding Expense Bar Elements*/
            expensePercentageImg.hidden = TRUE;
            expenseBarView.hidden = TRUE;
            noExpenseBar.hidden = TRUE;
            expenseBarAxis.hidden = TRUE;
            expenseBarScrollView.hidden = TRUE;
            
            lblBarExpenseSummary.hidden = TRUE;
            imgBarExpenseSummary.hidden = TRUE;
            
            
            lblBarIncomeSummary.hidden = FALSE;
            imgBarIncomeSummary.hidden = FALSE;
            
            if ([incomeDataforChart count]==0)
            {
                incomePercentageImg.hidden = TRUE;
                incomeBarView.hidden = true;
                noIncomeBar.hidden = FALSE;
                incomeBarAxis.hidden = TRUE;
            }
            else
            {
                incomePercentageImg.hidden = FALSE;
                incomeBarView.hidden = FALSE;
                noIncomeBar.hidden = TRUE;
                incomeBarAxis.hidden = FALSE;
                incomeBarScrollView.hidden = FALSE;
            }


        }
        else
        {
            
            /*Hiding Expense Bar Elements*/
            incomePercentageImg.hidden = TRUE;
            incomeBarView.hidden = TRUE;
            noIncomeBar.hidden = TRUE;
            incomeBarAxis.hidden = TRUE;
            lblBarIncomeSummary.hidden = TRUE;
            imgBarIncomeSummary.hidden = TRUE;
            incomeBarScrollView.hidden = TRUE;
            
            lblBarExpenseSummary.hidden = FALSE;
            imgBarExpenseSummary.hidden = FALSE;
            if ([expenseDataForChart count]==0)
            {
                expensePercentageImg.hidden = TRUE;
                expenseBarView.hidden = TRUE;
                noExpenseBar.hidden = FALSE;
                expenseBarAxis.hidden = TRUE;
            }
            else
            {
                expensePercentageImg.hidden = FALSE;
                expenseBarView.hidden = FALSE;
                noExpenseBar.hidden = TRUE;
                expenseBarAxis.hidden = FALSE;
                expenseBarScrollView.hidden = FALSE;
            }

        }
        
        
        [pieChartScrollView addSubview:barChartView];
        /*
         [pieChartScrollView setContentSize:CGSizeMake(barChartView.frame.size.width, barChartView.frame.size.height+20.0f)];
         */
        
        [pieChartScrollView setContentSize:CGSizeMake(barChartView.frame.size.width, 270.0f)];
        
    }

    
    else if (segment.selectedSegmentIndex == 2)
    {
        
        [segment setImage:[UIImage imageNamed:@"piechart.png"] forSegmentAtIndex:0];    
        [segment setImage:[UIImage imageNamed:@"barchart.png"] forSegmentAtIndex:1];
        [segment setImage:[UIImage imageNamed:@"tableview_selected.png"] forSegmentAtIndex:2];    

        
        [barChartView removeFromSuperview];
        [pieChartView removeFromSuperview];
 
        [tblView reloadData];
        if(appDelegate.isIPad)
        {
            
            
            
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                tableViewChart.frame = CGRectMake(0, 0,768,900);
                tblView.frame = CGRectMake(0, 0,768,900);
                [pieChartScrollView setContentSize:CGSizeMake(700,900)];
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                tableViewChart.frame = CGRectMake(0, 0,1024,768);
                tblView.frame = CGRectMake(0, 0,1024,768);
                [pieChartScrollView setContentSize:CGSizeMake(700,768)];
            }
            
            
        }
        else
        {
            [pieChartScrollView setContentSize:CGSizeMake(320, 300)];    
        }
        
        [pieChartScrollView addSubview:tableViewChart];
        
       
        


    }
    
}

#pragma mark -
#pragma mark generateExpenseBarChart 
-(void)generateExpenseBarChart
{
    
	
    if(appDelegate.isIPad)
    {
        //expenseBarView.backgroundColor = [UIColor redColor];
        //expenseBarScrollView.backgroundColor = [UIColor magentaColor];


       // expenseBarView.backgroundColor = [UIColor clearColor];
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            expenseBarView.frame = CGRectMake(10,260,700,420);
            expenseBarScrollView.frame= CGRectMake(44,270,700,420);

        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )//Pending..
        {
            expenseBarView.frame = CGRectMake(10,200,700,420);
            expenseBarScrollView.frame= CGRectMake(44,210,700,420);
            
        }
        
    }
    
    
    //expenseBarScrollView.backgroundColor = [UIColor clearColor];
    
	hostingView = (CPTGraphHostingView *)expenseBarView;
    hostingView.hostedGraph = barChart1;
    barChart1.plotAreaFrame.masksToBorder = NO;
	
    barChart1.paddingLeft = 0.0;
	barChart1.paddingTop = 20.0;
	barChart1.paddingRight = 0.0;
	
    if(appDelegate.isIPad)
    {
       barChart1.paddingBottom = 280.0;     
    }
    else 
    {
        barChart1.paddingBottom = 80.0;
    }
    
    
	
    
    float xRange = (float)[expenseDataForChart count];
	// Add plot space for horizontal bar charts
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)barChart1.defaultPlotSpace;
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(100.0f)];
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(xRange)];
    
	
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *)barChart1.axisSet;
    
    CPTXYAxis *x = axisSet.xAxis;
    x.axisLineStyle = nil;
    x.majorTickLineStyle = nil;
    x.minorTickLineStyle = nil;
    x.majorIntervalLength = CPTDecimalFromString(@"5");
    x.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
	//x.title = @"X Axis";
    x.titleLocation = CPTDecimalFromFloat(7.5f);
	x.titleOffset = 55.0f;
    
	// Define some custom labels for the data elements
	x.labelRotation = M_PI/4;
	x.labelingPolicy = CPTAxisLabelingPolicyNone;
	
    [customTickLocations removeAllObjects];
    for (int i=0;i<[expenseDataTypeArray count];i++)
    {
        [customTickLocations addObject:[NSNumber numberWithInteger:i*1]];
    }
	NSUInteger labelLocation = 0;
	NSMutableArray *customLabels = [NSMutableArray arrayWithCapacity:[expenseDataTypeArray count]];
	for (NSNumber *tickLocation in customTickLocations) 
    {
		NSString*labelText = [expenseDataTypeArray objectAtIndex:labelLocation++];
        nslog(@"labelText = %@",labelText);
        if([labelText length]>9)
        {
            
            if(!appDelegate.isIPad)
            {
                labelText = [ NSString stringWithFormat:@"%@...",[labelText substringToIndex:9]];
            }
             
        }
        
        nslog(@"labelText after = %@",labelText);
        /*
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText: [expenseDataTypeArray objectAtIndex:labelLocation++] textStyle:x.labelTextStyle];
        */ 
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText: labelText textStyle:x.labelTextStyle];

        NSString*value = [NSString stringWithFormat:@"%f",[tickLocation floatValue]+0.2];
        newLabel.tickLocation = CPTDecimalFromString(value);
        newLabel.offset = x.labelOffset + x.majorTickLength+5;
        newLabel.alignment = CPTAlignmentBottom;
        newLabel.rotation = M_PI/2;

		[customLabels addObject:newLabel];
		[newLabel release];
	}
	x.axisLabels =  [NSSet setWithArray:customLabels];
    
	CPTXYAxis *y = axisSet.yAxis;
    y.axisLineStyle = nil;
    y.majorTickLineStyle = nil;
    y.minorTickLineStyle = nil;
    y.majorIntervalLength = CPTDecimalFromString(@"20");
    y.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
	//y.title = @"Y Axis";
	y.titleOffset = 45.0f;
    y.titleLocation = CPTDecimalFromFloat(150.0f);
    
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    
   
    
    /*
    customTickLocations = [NSArray arrayWithObjects:[NSDecimalNumber numberWithInt:0], [NSDecimalNumber numberWithInt:20], [NSDecimalNumber numberWithInt:40], [NSDecimalNumber numberWithInt:60], [NSDecimalNumber numberWithInt:80],[NSDecimalNumber numberWithInt:100],nil];
    */
    /*OldCode:for xAxisLabels(Dont remove..)
    NSArray *xAxisLabels = [NSArray arrayWithObjects:@"0%", @"20%", @"40%", @"60%",@"80%",@"100%", nil];
    labelLocation = 0;
    customLabels = [NSMutableArray arrayWithCapacity:[xAxisLabels count]];
    for (NSNumber *tickLocation in customTickLocations) 
    {
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText: [xAxisLabels objectAtIndex:labelLocation++] textStyle:y.labelTextStyle];
        newLabel.tickLocation = [tickLocation decimalValue];
        newLabel.offset = y.labelOffset + y.majorTickLength;
        //newLabel.rotation = M_PI/4;
        [customLabels addObject:newLabel];
        [newLabel release];
    }
    
    
    y.axisLabels =  [NSSet setWithArray:customLabels];
     */
    // First bar plot
    /*
    CPTBarPlot *barPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor colorWithComponentRed:77.0/255.0 green:77.0/255.0 blue:77.0/255.0 alpha:1.0] horizontalBars:NO];
     */
    
    

   
    /*Chirag:Another Method.
    CPTBarPlot *barPlot = [[CPTBarPlot alloc] init];
    barPlot.fill = [CPTFill fillWithColor:[CPTColor greenColor]];
     */
    
    
    barPlot.baseValue = CPTDecimalFromString(@"0");
    barPlot.dataSource = self;
    barPlot.barOffset = CPTDecimalFromFloat(0.20f);
    barPlot.identifier = @"Bar Plot 2";
    barPlot.barWidth =CPTDecimalFromString(@"0.40");
    
    [barChart1 addPlot:barPlot toPlotSpace:plotSpace];
    
    
    //expenseBarScrollView.contentSize = CGSizeMake(120+[expenseDataForChart count]*80,220);
    
    /*
    incomeBarView.frame = CGRectMake(0, 0,[incomeDataforChart count]*53,220);
    incomeBarScrollView.contentSize = CGSizeMake([incomeDataforChart count]*60,220);
    */
    
    if(appDelegate.isIPad)
    {
        expenseBarView.frame = CGRectMake(0, 0,[expenseDataForChart count]*80,420);
        expenseBarScrollView.contentSize = CGSizeMake([expenseDataForChart count]*80,420);
    
    }
    else
    {
        expenseBarView.frame = CGRectMake(0, 0,[expenseDataForChart count]*55,220);
        expenseBarScrollView.contentSize = CGSizeMake([expenseDataForChart count]*55,220);
    }
    
    
    [expenseBarScrollView addSubview:expenseBarView];

}



#pragma mark -
#pragma mark generateBarChart 

-(void)generateBarChart
{
    
    

    
    
/**/
    
	
    if(appDelegate.isIPad)
    {
        
        //incomeBarView.backgroundColor = [UIColor redColor];
        //incomeBarScrollView.backgroundColor = [UIColor magentaColor];
     
        
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
            
            incomeBarView.frame = CGRectMake(10,260,700,420);
            incomeBarScrollView.frame= CGRectMake(44,270,700,420);
            
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )//Pending..
        {
            incomeBarView.frame = CGRectMake(10,200,700,420);
            incomeBarScrollView.frame= CGRectMake(44,210,700,420);
            
        }

        
        
        
        
    }
    
    
   // incomeBarScrollView.backgroundColor = [UIColor clearColor];
    
	hostingView = (CPTGraphHostingView *)incomeBarView;
    hostingView.hostedGraph = barChart;
    barChart.plotAreaFrame.masksToBorder = NO;
	
    barChart.paddingLeft = 0.0;
	barChart.paddingTop = 20.0;
	barChart.paddingRight = 0.0;
    
    if(appDelegate.isIPad)
    {
         barChart.paddingBottom = 280.0;   
    }
    else 
    {
        barChart.paddingBottom = 80.0;
    }
    
	
	float xRange = (float)[incomeDataforChart count];
	// Add plot space for horizontal bar charts
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)barChart.defaultPlotSpace;
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(100.0f)];
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(xRange)];
    
	
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *)barChart.axisSet;
    
    CPTXYAxis *x = axisSet.xAxis;
    x.axisLineStyle = nil;
    x.majorTickLineStyle = nil;
    x.minorTickLineStyle = nil;
    x.majorIntervalLength = CPTDecimalFromString(@"5");
    x.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
	//x.title = @"X Axis";
    x.titleLocation = CPTDecimalFromFloat(7.5f);
	x.titleOffset = 55.0f;
    
	// Define some custom labels for the data elements
	x.labelRotation = M_PI/4;
	x.labelingPolicy = CPTAxisLabelingPolicyNone;
	
   // NSMutableArray *customTickLocations = [[NSMutableArray alloc]init];
   
    [customTickLocations removeAllObjects];
    for (int i=0;i<[incomeDataTypeArray count];i++)
    {
        [customTickLocations addObject:[NSNumber numberWithInteger:i*1]];
    }
	NSUInteger labelLocation = 0;
	NSMutableArray *customLabels = [NSMutableArray arrayWithCapacity:[incomeDataTypeArray count]];
	for (NSNumber *tickLocation in customTickLocations) 
    {
		NSString*labelText = [incomeDataTypeArray objectAtIndex:labelLocation++];
        nslog(@"labelText = %@",labelText);
        if([labelText length]>9)
        {
            
            if(!appDelegate.isIPad)
            {
                labelText = [ NSString stringWithFormat:@"%@...",[labelText substringToIndex:9]];
            }
            
        }
        
        nslog(@"labelText after = %@",labelText);

 CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText: labelText textStyle:x.labelTextStyle];
        
        NSString*value = [NSString stringWithFormat:@"%f",[tickLocation floatValue]+0.2];
        newLabel.tickLocation = CPTDecimalFromString(value);
        newLabel.offset = x.labelOffset + x.majorTickLength+5;
        newLabel.alignment = CPTAlignmentBottom;
        newLabel.rotation = M_PI/2;
        
		[customLabels addObject:newLabel];
		[newLabel release];
	}
	x.axisLabels =  [NSSet setWithArray:customLabels];
    
	CPTXYAxis *y = axisSet.yAxis;
    y.axisLineStyle = nil;
    y.majorTickLineStyle = nil;
    y.minorTickLineStyle = nil;
    y.majorIntervalLength = CPTDecimalFromString(@"20");
    y.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
	//y.title = @"Y Axis";
	y.titleOffset = 45.0f;
    y.titleLocation = CPTDecimalFromFloat(150.0f);
    
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    

    
     
    

     
    
   
    barPlotIncome.baseValue = CPTDecimalFromString(@"0");
    barPlotIncome.dataSource = self;
    barPlotIncome.barOffset = CPTDecimalFromFloat(0.20f);
    barPlotIncome.identifier = @"Bar Plot 1";
    barPlotIncome.barWidth =CPTDecimalFromString(@"0.40");
    
    [barChart addPlot:barPlotIncome toPlotSpace:plotSpace];
    
    
    //expenseBarScrollView.contentSize = CGSizeMake(120+[expenseDataForChart count]*80,220);
    
    
    if(appDelegate.isIPad)
    {
        incomeBarView.frame = CGRectMake(0, 0,[incomeDataforChart count]*80,420);
        incomeBarScrollView.contentSize = CGSizeMake([incomeDataforChart count]*80,420);
        
    }
    else
    {
        incomeBarView.frame = CGRectMake(0, 0,[incomeDataforChart count]*55,220);
        incomeBarScrollView.contentSize = CGSizeMake([incomeDataforChart count]*55,220);
    }
    //incomeBarView.backgroundColor = [UIColor redColor];
    
    [incomeBarScrollView addSubview:incomeBarView];

    
}

#pragma mark -
#pragma mark Plot Data Source Methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
//   if (segment.selectedSegmentIndex == 0)
//   {
        if ([plot.identifier isEqual:@"Pie Chart 1"])
        {
                return [incomeDataforChart count];
        }
        else if ([plot.identifier isEqual:@"Pie Chart 2"])
        {
            return [expenseDataForChart count];
        }
//   }
//    else if (segment.selectedSegmentIndex == 1)
//    {
        else if ([plot.identifier isEqual:@"Bar Plot 1"])
        {
            return [incomeDataforChart count];
        }
        else if ([plot.identifier isEqual:@"Bar Plot 2"])
        {
            return [expenseDataForChart count];
        }
   // }
    
    return 0;
    
}


-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index 
{

//    if (segment.selectedSegmentIndex == 0)
//    {
        if ([plot.identifier isEqual:@"Pie Chart 1"])
        {
	if ( index >= [incomeDataforChart count] ) return nil;
	
	if ( fieldEnum == CPTPieChartFieldSliceWidth ) {
		return [[incomeDataforChart objectAtIndex:index]valueForKey:[NSString stringWithFormat:@"%@",[incomeDataTypeArray objectAtIndex:index]]];
	}
	else {
		return [NSNumber numberWithInt:index];
	}
    }
        else if ([plot.identifier isEqual:@"Pie Chart 2"])
        {
            if ( index >= [expenseDataForChart count] ) return nil;
            
            if ( fieldEnum == CPTPieChartFieldSliceWidth ) {
                return [[expenseDataForChart objectAtIndex:index]valueForKey:[NSString stringWithFormat:@"%@",[expenseDataTypeArray objectAtIndex:index]]];
            }
            else {
                return [NSNumber numberWithInt:index];
            }
        }
    
  //  }
//    else if (segment.selectedSegmentIndex == 1)
//    {

        NSDecimalNumber *num = nil;
         if ( [plot isKindOfClass:[CPTBarPlot class]] ) {
            if ([plot.identifier isEqual:@"Bar Plot 1"])
            {
            switch ( fieldEnum ) {
                case CPTBarPlotFieldBarLocation:
                    //num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInteger:[arr count]];
                    num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInteger:index];
                    break;
                case CPTBarPlotFieldBarTip:
                    num = [[incomeDataforChart objectAtIndex:index]valueForKey:[NSString stringWithFormat:@"%@",[incomeDataTypeArray objectAtIndex:index]]];
                }
                return num;

        }
            else if ([plot.identifier isEqual:@"Bar Plot 2"])
            {
                switch ( fieldEnum ) {
                    case CPTBarPlotFieldBarLocation:
                        //num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInteger:[arr count]];
                        num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInteger:index];
                        break;
                    case CPTBarPlotFieldBarTip:
                        num = [[expenseDataForChart objectAtIndex:index]valueForKey:[NSString stringWithFormat:@"%@",[expenseDataTypeArray objectAtIndex:index]]];
                }
                return num;

            }
        
        }  
  //  }
    return nil;
}

#pragma mark -
#pragma mark tableview methods



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    /*Old Code..
    return 2;
     */
    return 1;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)] autorelease];
    
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(65, 0, 30, 30)];
    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(105, 3, tableView.bounds.size.width - 10, 20)] autorelease];
    
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
        {
            img.frame= CGRectMake(305,20,30,30);
            label.frame= CGRectMake(340,23,150,21);
        }
        else
        {
            img.frame= CGRectMake(410,20,30,30);
            label.frame= CGRectMake(455,23,150,21);
            
        }
        
    }
    else
    {
        
        label.frame = CGRectMake(114,11,140,21);
        img.frame = CGRectMake(74,7,30,30);
    }
    
    
    
    
    [label setFont:[UIFont boldSystemFontOfSize:13.0]];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:76.0/255.0f green:86.0/255.0f blue:108.0/255.0f alpha:1.0f];
    
    if (isIncome)
    {
        img.image = [UIImage imageNamed:@"income-revised-30.png"];
        label.text = @"INCOME SUMMARY";
    }
    else
    {
        img.image = [UIImage imageNamed:@"new-cell-expense.png"];
        label.text = @"EXPENSE SUMMARY";
    }

    
    
    label.backgroundColor = [UIColor clearColor];
    
    /*
        Sun:0004
        Commented this due to change in layout-Phase 3
     
    
    
    
    [headerView addSubview:img];
    [headerView addSubview:label];
*/
    
    
    [img release];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(appDelegate.isIPad)
    {
        return 80;
    }
     return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
   
    if(isIncome)
    {
        return [incomeDataforChart count]+2;
    }
    else
    {
        return [expenseDataForChart count]+2;
    }
    /*
    if (section == 0)
    {
        return [incomeDataforChart count]+2;
    }
    else
    {
        return [expenseDataForChart count]+2;
    }
   */
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [numberFormatter setCurrencyCode:appDelegate.curCode];
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [numberFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        } 
        else if([[numberFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [numberFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [numberFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        
    }

    static NSString *CellIdentifier = @"Cell";
    
    DashBoardCustomCell *cell;// = (DashBoardCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (cell == nil) 
    if (1) 
    {
        cell = [[[DashBoardCustomCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
    cell.label.hidden = FALSE;
    cell.percentageLabel.hidden = FALSE;
    cell.amountLabel.hidden = FALSE;
    
    cell.headingLabel1.hidden = TRUE;
    cell.headingLabel2.hidden = TRUE;
    cell.headingLabel3.hidden = TRUE;
    
    /*Old Code..
    if (indexPath.section == 0)
    {
    
        cell.backgroundColor = [UIColor whiteColor];
        if (indexPath.row == 0)
        {
            cell.backgroundColor =  [UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0];
            cell.headingLabel1.textColor = [UIColor whiteColor];
            cell.headingLabel2.textColor = [UIColor whiteColor];
            cell.headingLabel3.textColor = [UIColor whiteColor];
            cell.headingLabel1.hidden = FALSE;
            cell.headingLabel2.hidden = FALSE;
            cell.headingLabel3.hidden = FALSE;
            cell.label.hidden = TRUE;
            cell.percentageLabel.hidden = TRUE;
            cell.amountLabel.hidden = TRUE;
            cell.headingLabel1.text = @"Income Type";
            cell.headingLabel2.text = @"%";
            cell.headingLabel3.text = @"Amount";
            
            
        }
        else if (indexPath.row == [incomeDataforChart count]+1)
        {
            cell.headingLabel1.hidden = FALSE;
            cell.headingLabel2.hidden = FALSE;
            cell.headingLabel3.hidden = FALSE;
            cell.label.hidden = TRUE;
            cell.percentageLabel.hidden = TRUE;
            cell.amountLabel.hidden = TRUE;
            cell.headingLabel1.text = @"Total Income";
            cell.headingLabel2.text = @"100.00";
            
            if([[numberFormatter stringFromNumber:[NSNumber numberWithDouble:totalExpenseAmount]] length]>=11)
            {
                [cell.headingLabel3 setFont:[UIFont boldSystemFontOfSize:12.0]];
            }

            
            cell.headingLabel3.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:totalIncomeAmount]];
        }
    else
    {
        cell.label.text = [incomeDataTypeArray objectAtIndex:indexPath.row-1];
        cell.label.lineBreakMode = UILineBreakModeWordWrap;
        
        //  nslog(@"income data at cell ==== %@",[[incomeDataforChart objectAtIndex:indexPath.row-1]valueForKey:[incomeDataTypeArray objectAtIndex:indexPath.row-1]]);
        cell.percentageLabel.text = [NSString stringWithFormat:@"%@",[[incomeDataforChart objectAtIndex:indexPath.row-1]valueForKey:[incomeDataTypeArray objectAtIndex:indexPath.row-1]]];
        //cell.amountLabel.text = @"Amount";
        cell.amountLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[[tempIncomeArray objectAtIndex:indexPath.row-1]valueForKey:[incomeDataTypeArray objectAtIndex:indexPath.row-1]]floatValue]]];
    }
    }
    else if (indexPath.section == 1)
    {
        cell.backgroundColor = [UIColor whiteColor];
        if (indexPath.row == 0)
        {
            cell.backgroundColor = [UIColor redColor];
            cell.headingLabel1.textColor = [UIColor whiteColor];
                        cell.headingLabel2.textColor = [UIColor whiteColor];
                        cell.headingLabel3.textColor = [UIColor whiteColor];
            cell.headingLabel1.hidden = FALSE;
            cell.headingLabel2.hidden = FALSE;
            cell.headingLabel3.hidden = FALSE;
            cell.label.hidden = TRUE;
            cell.percentageLabel.hidden = TRUE;
            cell.amountLabel.hidden = TRUE;
            cell.headingLabel1.text = @"Expense Type";
            cell.headingLabel2.text = @"%";
            cell.headingLabel3.text = @"Amount";
            
        }
        else if (indexPath.row == [expenseDataForChart count]+1)
        {

            cell.headingLabel1.hidden = FALSE;
            cell.headingLabel2.hidden = FALSE;
            cell.headingLabel3.hidden = FALSE;
            cell.label.hidden = TRUE;
            cell.percentageLabel.hidden = TRUE;
            cell.amountLabel.hidden = TRUE;
            cell.headingLabel1.text = @"Total Expense";
            cell.headingLabel2.text = @"100.00";
            if([[numberFormatter stringFromNumber:[NSNumber numberWithDouble:totalExpenseAmount]] length]>=11)
            {
               [cell.headingLabel3 setFont:[UIFont boldSystemFontOfSize:12.0]];
            }
            cell.headingLabel3.text = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:totalExpenseAmount]];
            nslog(@"\n number foramt...%@",[numberFormatter stringFromNumber:[NSNumber numberWithDouble:totalExpenseAmount]]);
            nslog(@"\n totalExpenseAmount = %lf",totalExpenseAmount);
            
        }
        else
        {
            cell.label.text = [expenseDataTypeArray objectAtIndex:indexPath.row-1];
            //        nslog(@"income data at cell ==== %@",[[incomeDataforChart objectAtIndex:indexPath.row-1]valueForKey:[incomeDataTypeArray objectAtIndex:indexPath.row-1]]);
            cell.percentageLabel.text = [NSString stringWithFormat:@"%@",[[expenseDataForChart objectAtIndex:indexPath.row-1]valueForKey:[expenseDataTypeArray objectAtIndex:indexPath.row-1]]];
            //cell.amountLabel.text = @"Amount";
            cell.amountLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[[tempExpenseArray objectAtIndex:indexPath.row-1]valueForKey:[expenseDataTypeArray objectAtIndex:indexPath.row-1]]doubleValue]]];
        }

    }
    */ 
    
    if (isIncome)
    {
        
        cell.backgroundColor = [UIColor whiteColor];
        if (indexPath.row == 0)
        {
            //cell.backgroundColor =  [UIColor colorWithRed:0.0/255.0 green:116.0/255.0 blue:0.0/255.0 alpha:1.0];
            
            
            
           
            
                        
            
            cell.headingLabel1.textColor = [UIColor whiteColor];
            cell.headingLabel2.textColor = [UIColor whiteColor];
            cell.headingLabel3.textColor = [UIColor whiteColor];
            cell.headingLabel1.hidden = FALSE;
            cell.headingLabel2.hidden = FALSE;
            cell.headingLabel3.hidden = FALSE;
            cell.label.hidden = TRUE;
            cell.percentageLabel.hidden = TRUE;
            cell.amountLabel.hidden = TRUE;
            cell.headingLabel1.text = @"Income Type";
            cell.headingLabel2.text = @"%";
            cell.headingLabel3.text = @"Amount";
            
            
            UIImageView*bg_imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dashboard_detail_table_cell_green_bg.png"] ];
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
                {
                    bg_imageView.frame = CGRectMake(-1,-1, 682,46);
                    bg_imageView.layer.cornerRadius = 3.0f;
                }
                else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
                {
                   
                        bg_imageView.frame = CGRectMake(-1, -1, 937,46);
                }
                
            }
            else
            {
                bg_imageView.frame = CGRectMake(0, 0, 301,44);
                 bg_imageView.layer.cornerRadius = 2.0f;
            }
            
            
            
            
           
            bg_imageView.layer.masksToBounds = YES;
            [cell.contentView addSubview:bg_imageView];
            [cell.contentView sendSubviewToBack:bg_imageView];
            [bg_imageView release];

            
            
            
        }
        else if (indexPath.row == [incomeDataforChart count]+1)
        {
            cell.headingLabel1.hidden = FALSE;
            cell.headingLabel2.hidden = FALSE;
            cell.headingLabel3.hidden = FALSE;
            cell.label.hidden = TRUE;
            cell.percentageLabel.hidden = TRUE;
            cell.amountLabel.hidden = TRUE;
            cell.headingLabel1.text = @"Total Income";
            cell.headingLabel2.text = @"100.00";
            
            if([[numberFormatter stringFromNumber:[NSNumber numberWithDouble:totalExpenseAmount]] length]>=11)
            {
                [cell.headingLabel3 setFont:[UIFont boldSystemFontOfSize:12.0]];
            }
            
            
            cell.headingLabel3.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:totalIncomeAmount]];
        }
        else
        {
            cell.label.text = [incomeDataTypeArray objectAtIndex:indexPath.row-1];
            cell.label.lineBreakMode = UILineBreakModeWordWrap;
            
            //  nslog(@"income data at cell ==== %@",[[incomeDataforChart objectAtIndex:indexPath.row-1]valueForKey:[incomeDataTypeArray objectAtIndex:indexPath.row-1]]);
            cell.percentageLabel.text = [NSString stringWithFormat:@"%@",[[incomeDataforChart objectAtIndex:indexPath.row-1]valueForKey:[incomeDataTypeArray objectAtIndex:indexPath.row-1]]];
            //cell.amountLabel.text = @"Amount";
            cell.amountLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[[tempIncomeArray objectAtIndex:indexPath.row-1]valueForKey:[incomeDataTypeArray objectAtIndex:indexPath.row-1]]floatValue]]];
        }
    }
    else
    {
        cell.backgroundColor = [UIColor whiteColor];
        if (indexPath.row == 0)
        {
            //cell.backgroundColor = [UIColor redColor];
            
            
            
            
            cell.headingLabel1.textColor = [UIColor whiteColor];
            cell.headingLabel2.textColor = [UIColor whiteColor];
            cell.headingLabel3.textColor = [UIColor whiteColor];
            cell.headingLabel1.hidden = FALSE;
            cell.headingLabel2.hidden = FALSE;
            cell.headingLabel3.hidden = FALSE;
            cell.label.hidden = TRUE;
            cell.percentageLabel.hidden = TRUE;
            cell.amountLabel.hidden = TRUE;
            cell.headingLabel1.text = @"Expense Type";
            cell.headingLabel2.text = @"%";
            cell.headingLabel3.text = @"Amount";
            
            
            UIImageView*bg_imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dashboard_detail_table_cell_red_bg.png"] ];
            
            
            
            
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation==1 || [UIApplication sharedApplication].statusBarOrientation==2)
                {
                    bg_imageView.frame = CGRectMake(-1,-1, 682,46);
                    bg_imageView.layer.cornerRadius = 3.0f;
                }
                else if([UIApplication sharedApplication].statusBarOrientation==3 || [UIApplication sharedApplication].statusBarOrientation==4)
                {
                    
                    bg_imageView.frame = CGRectMake(-1, -1, 937,46);
                }
                
            }
            else
            {
                bg_imageView.frame = CGRectMake(0, 0, 301,44);
                bg_imageView.layer.cornerRadius = 2.0f;
            }

            
            
            
            bg_imageView.layer.masksToBounds = YES;
            [cell.contentView addSubview:bg_imageView];
            [cell.contentView sendSubviewToBack:bg_imageView];
            [bg_imageView release];

            
            
        }
        else if (indexPath.row == [expenseDataForChart count]+1)
        {
            
            cell.headingLabel1.hidden = FALSE;
            cell.headingLabel2.hidden = FALSE;
            cell.headingLabel3.hidden = FALSE;
            cell.label.hidden = TRUE;
            cell.percentageLabel.hidden = TRUE;
            cell.amountLabel.hidden = TRUE;
            cell.headingLabel1.text = @"Total Expense";
            cell.headingLabel2.text = @"100.00";
            if([[numberFormatter stringFromNumber:[NSNumber numberWithDouble:totalExpenseAmount]] length]>=11)
            {
                [cell.headingLabel3 setFont:[UIFont boldSystemFontOfSize:12.0]];
            }
            cell.headingLabel3.text = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:totalExpenseAmount]];
            nslog(@"\n number foramt...%@",[numberFormatter stringFromNumber:[NSNumber numberWithDouble:totalExpenseAmount]]);
            nslog(@"\n totalExpenseAmount = %lf",totalExpenseAmount);
            
        }
        else
        {
            cell.label.text = [expenseDataTypeArray objectAtIndex:indexPath.row-1];
            //        nslog(@"income data at cell ==== %@",[[incomeDataforChart objectAtIndex:indexPath.row-1]valueForKey:[incomeDataTypeArray objectAtIndex:indexPath.row-1]]);
            cell.percentageLabel.text = [NSString stringWithFormat:@"%@",[[expenseDataForChart objectAtIndex:indexPath.row-1]valueForKey:[expenseDataTypeArray objectAtIndex:indexPath.row-1]]];
            //cell.amountLabel.text = @"Amount";
            cell.amountLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[[tempExpenseArray objectAtIndex:indexPath.row-1]valueForKey:[expenseDataTypeArray objectAtIndex:indexPath.row-1]]doubleValue]]];
        }
        
    }
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


#pragma pragma mark - choose_report_type_button_pressed
#pragma pragma mark whether user wants generate excel or pdf
-(IBAction)choose_report_type_button_pressed:(id)sender
{
    //
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Export" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"PDF",@"Excel",nil];
    actionSheet.tag = 2000;
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
    [actionSheet release];
}

#pragma pragma mark - clickedButtonAtIndex
#pragma pragma mark actionsheet delegate methods..

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    nslog(@"\n buttonIndex = %d",buttonIndex);
    
    switch (buttonIndex)
    {
        case 0:
        {
            //pdf
            

            

            pdfViewController *pdfView = [[pdfViewController alloc]initWithNibName:@"pdfViewController" bundle:nil];
            pdfView.fromString = from_date;
            pdfView.toString = to_date;
           
            pdfView.arrayIndex =arrayindex;
            [self.navigationController pushViewController:pdfView animated:YES];
            [pdfView release];
            
            
            
            
            break;
        }
        case 1:
        {
            //need to implement excel here..
            
            
            /* */
            ExcelGenerationViewController*viewController = [[ExcelGenerationViewController alloc] initWithNibName:@"ExcelGenerationViewController" bundle:nil];
            viewController.fromString = from_date;
            viewController.toString = to_date;
            viewController.arrayIndex =arrayindex;
            [self.navigationController pushViewController:viewController animated:YES];
            [viewController release];

            
            
            break;
        }
            
        default:
        {
             break;
        }
           
    }
    
    
}


@end
