//
//  AddIncomeTypeViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PropertyLogBookAppDelegate;
@interface AddIncomeTypeViewController : UIViewController<UITextFieldDelegate> {
    
    IBOutlet UITableView *tblView;
    
    NSMutableArray *incomeTypeArray;
    UITextField *incomeText;
    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UILabel *lblNoData,*lblPlus;
    NSIndexPath *oldIndex;
    int textFieldIndex;
    IBOutlet UIImageView *imageView;
    
    NSMutableArray *tempRowArray;
    
     UIToolbar *keytoolBar;
    int isClicked;
    NSString *textFieldTempText;
    
    BOOL isAddClicked;

}
-(void)cancel_clicked;
-(void)add_clicked;
-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew;
@end
