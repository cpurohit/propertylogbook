//
//  propertyTypeViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

@interface propertyTypeViewController : UIViewController<UITextFieldDelegate> {
    IBOutlet UITableView *tblView;
    UITextField *newType;
    NSMutableArray *propertyTypeArray;
    IBOutlet UILabel *defaultLabel;
    PropertyLogBookAppDelegate *appDelegate;
    NSIndexPath *oldIndex;
    IBOutlet UILabel *lblNoData,*lblPlus;
    int textFieldIndex;
    IBOutlet UIImageView *imageView;
    
    NSMutableArray *tempRowArray;
     UIToolbar *keytoolBar;
    int isClicked;
    NSString *textFieldTempText;
    
    BOOL isAddClicked;
    
}
-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew;
@end
