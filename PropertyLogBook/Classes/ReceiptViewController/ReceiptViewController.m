

#import "ReceiptViewController.h"
#import "UIImage+Resize.h"

@implementation ReceiptViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        
        
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class])); 
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    // Do any additional setup after loading the view from its nib.
    
    /**/
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(dismissControllerIfNeeded:)
                                                 name: @"dismissControllerIfNeeded"
                                               object: nil];
     
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    if(appDelegate.isIPad)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    }
    
    
    
    appDelegate.is_receipt_image_updated = FALSE;
    
    /*
     Start:
     Sun:0004
     Setting custom image in navigation bar..
     */
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor],UITextAttributeTextColor,
                                                                     [UIColor whiteColor],UITextAttributeTextShadowColor,
                                                                     [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
                                                                     UITextAttributeTextShadowOffset ,
                                                                     [UIFont boldSystemFontOfSize:19.0f],UITextAttributeFont , nil]];
    
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0)
    {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
    }
    
    /*
     Ends:
     Sun:0004
     Setting custom image in navigation bar..
     */

    
    UIButton * done_button = [UIButton buttonWithType:UIButtonTypeCustom];
    done_button.bounds = CGRectMake(0, 0, 50.0, 30.0);
    [done_button setBackgroundImage:[UIImage imageNamed:@"receipt_done_button.png"] forState:UIControlStateNormal];
    [done_button addTarget:self action:@selector(done_button_pressed:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[[UIBarButtonItem alloc] initWithCustomView:done_button] autorelease];
    
    
    
    UIButton * choose_photo = [UIButton buttonWithType:UIButtonTypeCustom];
    choose_photo.frame = CGRectMake(0, 0, 50.0, 30.0);
    [choose_photo setBackgroundImage:[UIImage imageNamed:@"receipt_photo_button.png"] forState:UIControlStateNormal];
    [choose_photo addTarget:self action:@selector(choose_photo_button_pressed:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[[UIBarButtonItem alloc] initWithCustomView:choose_photo] autorelease];
    
    
   // float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    self.navigationItem.title = NSLocalizedString(@"RECEIPT", @"RECEIPT");
    
    
    imgviewForNoData.hidden = TRUE;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    if(appDelegate.is_receipt_image_choosen==TRUE)
    {
        
        
        nslog(@"\n appDelegate.receipt_image_name = %@",appDelegate.receipt_image_name);
        
        /*
         Sun:0004
         Date:10-05-2013
         If user chooses image,and without saving if he comes back to this view,he should see,last choosen image and not what he has saved...
         */
        
        if(appDelegate.temp_receipt_image==nil)
        {
            receipt_imageView.image = [appDelegate getImage:appDelegate.receipt_image_name];
            nslog(@"\n receipt_imageView.image = %@",receipt_imageView.image);
            
            if(receipt_imageView.image == nil)
            {
                receipt_imageView.image = appDelegate.receipt_image;
            }
            
        }
        else
        {
            receipt_imageView.image = appDelegate.temp_receipt_image;
        }
        
        
        
    }
    else
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
        actionSheet_temp = actionSheet;
        actionSheet.delegate = self;
        actionSheet.title = @"Select From";
        [actionSheet addButtonWithTitle:@"Photo Library"];
        [actionSheet addButtonWithTitle:@"Camera"];
        [actionSheet addButtonWithTitle:@"Cancel"];
        actionSheet.destructiveButtonIndex = 2;
        [actionSheet showInView:self.view];
       
        //[actionSheet showFromTabBar:appDelegate.tabBarController.tabBar];//working in ios 6.1.3
        // [actionSheet showFromTabBar:self.tabBarController.tabBar];
        //[actionSheet showInView:self.tabBarController.view];
        //[actionSheet showFromTabBar:self.tabBarController.tabBar];
        //[actionSheet showFromRect:CGRectMake(0,1024,20, 20) inView:self.view animated:TRUE];
        
        [actionSheet release];
         [actionSheet_temp retain];
        
        if(appDelegate.isIPad)
        {
            imgForReceipt = [UIImage imageNamed:@"img_attachment@2x.png"];
        }
        else
        {
            imgForReceipt = [UIImage imageNamed:@"img_attachment.png"];
        }
    }
    
    [super viewDidAppear:animated];
}



-(void)dismissControllerIfNeeded:(id)sender
{
    if(!is_dismiss_properly)
    {
        
        
        [actionSheet_temp dismissWithClickedButtonIndex:nil animated:NO];
        
        if(appDelegate.is_receipt_image_choosen==TRUE)
        {
            //Process image...
            
            
            
            if(appDelegate.temp_receipt_image==nil)
            {
                receipt_imageView.image = [appDelegate getImage:appDelegate.receipt_image_name];
                nslog(@"\n receipt_imageView.image = %@",receipt_imageView.image);
                
                if(receipt_imageView.image == nil)
                {
                    receipt_imageView.image = appDelegate.receipt_image;
                }
                
            }
            else
            {
                receipt_imageView.image = appDelegate.temp_receipt_image;
            }

            
            
            
        }
        [self.navigationController dismissModalViewControllerAnimated:YES];
    }

}



-(void)viewWillAppear:(BOOL)animated
{
    
    
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    
    nslog(@"\n radnom number = %@",[appDelegate generateRandomImageName]);
    
    receipt_imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    
    

    [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
    [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];

    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    ////[self.view addSubview:appDelegate.bottomView];
    
    /*
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    */
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    if(!appDelegate.isIPad)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:FALSE];
    }

}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}


#pragma mark - done_button_pressed
#pragma mark manages save.


-(void)done_button_pressed:(id)sender
{
    
    nslog(@"\n Done_clicked");
    
    is_dismiss_properly = TRUE;
    
    if(appDelegate.is_receipt_image_choosen==TRUE)
    {
        //Process image...
        
        
        appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
        appDelegate.receipt_image = receipt_imageView.image;
        
        
    }
    
    if(appDelegate.isIPad)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    }

    
    
    [self.navigationController dismissModalViewControllerAnimated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - choose_photo_button_pressed
#pragma mark manages choosing photo or deleting existing photo.

-(void)choose_photo_button_pressed:(id)sender
{
    if(receipt_imageView.image)
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
        actionSheet_temp = actionSheet;
        actionSheet.delegate = self;
        actionSheet.tag = 10001;
        actionSheet.title = @"Select From";
        [actionSheet addButtonWithTitle:@"Photo Library"];
        [actionSheet addButtonWithTitle:@"Camera"];
        [actionSheet addButtonWithTitle:@"Cancel"];
        [actionSheet addButtonWithTitle:@"Delete"];
        actionSheet.destructiveButtonIndex = 3;
        [actionSheet showInView:self.view];
        [actionSheet release];
        [actionSheet_temp retain];
    }
    else
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
        
        actionSheet_temp = actionSheet;
        actionSheet.delegate = self;
        actionSheet.tag = 10001;
        actionSheet.title = @"Select From";
        [actionSheet addButtonWithTitle:@"Photo Library"];
        [actionSheet addButtonWithTitle:@"Camera"];
        [actionSheet addButtonWithTitle:@"Cancel"];
        actionSheet.destructiveButtonIndex = 2;
        [actionSheet showInView:self.view];
        [actionSheet release];
        [actionSheet_temp retain];
        
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    imgviewForNoData.hidden = FALSE;
    
    if(appDelegate.isIPad)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    }
    
}
-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    

    picker = [[UIImagePickerController alloc]init];
	picker.delegate = self;
    
    
    
	if(buttonIndex == 0)
	{
        //appDelegate.bool_receiptImageSelected = TRUE;
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        
        if (!appDelegate.isIPad)
        {
            [self presentModalViewController:picker animated:YES];
            
        }
        else
        {
            popoverController = [[UIPopoverController alloc]initWithContentViewController:picker];
            [popoverController presentPopoverFromRect:CGRectMake(650,-70,100,100) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
        
        
        
		
	}
    
	else if(buttonIndex == 1)
	{
        
		if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
		{
			[picker setAllowsEditing:NO];
			picker.sourceType = UIImagePickerControllerSourceTypeCamera;
			picker.delegate = self;
			[self presentModalViewController:picker animated:YES];
		}
		else
		{
			UIAlertView *showalert = [[UIAlertView alloc]initWithTitle:nil message:@"No camera device found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[showalert show];
			[showalert release];
            
		}
	}
    else if(buttonIndex == 2)
    {
        
        if(receipt_imageView.image==nil)
        {
            if(appDelegate.isIPad)
            {
                imgForReceipt = [UIImage imageNamed:@"img_attachment@2x.png"];
            }
            else
            {
                imgForReceipt = [UIImage imageNamed:@"img_attachment.png"];
            }
            nslog(@"cancel clicked");
            receipt_imageView.image = nil;
            appDelegate.is_receipt_image_choosen= FALSE;
            imgviewForNoData.hidden = FALSE;
        }
        
    }
    else
    {
        
        
        if(appDelegate.isIPad)
        {
            imgForReceipt = [UIImage imageNamed:@"img_attachment@2x.png"];
        }
        else
        {
            imgForReceipt = [UIImage imageNamed:@"img_attachment.png"];
        }
        
        
        
        receipt_imageView.image = nil;
        appDelegate.is_receipt_image_choosen= FALSE;
        
       
        
        
        imgviewForNoData.hidden = FALSE;
        
    }

    if(appDelegate.isIPad)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    }

    [picker release];

}

-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker1
{
    // bool_nocamera = TRUE;
    // imgForReceipt = [UIImage imageNamed:@"img_receiptIcon.png"];
    if(receipt_imageView.image)
    {
        imgviewForNoData.hidden = TRUE;
    }
    else
    {
        imgviewForNoData.hidden = FALSE;
    }
	[picker1 dismissModalViewControllerAnimated:YES];
	
    if(appDelegate.isIPad)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    }
}

/*
- (UIImage *)normalizedImage:(UIImage*)targetImage
{
    if (targetImage.imageOrientation == UIImageOrientationUp)
    {
        return targetImage;
    }
    
    
    UIGraphicsBeginImageContextWithOptions(targetImage.size, NO, targetImage.scale);
    [self drawInRect:(CGRect){0, 0, targetImage.size}];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return normalizedImage;
}
*/
-(void) imagePickerController:(UIImagePickerController *)picker_local didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    if(appDelegate.isIPad)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    }
    
    
	if (picker_local.sourceType == UIImagePickerControllerSourceTypePhotoLibrary)
    {
        imgForReceipt = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
        imgviewForNoData.hidden = TRUE;
        
    }
	else if (picker_local.sourceType == UIImagePickerControllerSourceTypeCamera)
	{
        
        
        UIImage *theImage = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
        if (picker.cameraDevice == UIImagePickerControllerCameraDeviceFront)
        {
            imgForReceipt = theImage; // new
            
            /*
            imgForReceipt =[self correctImageOrientation: [[info valueForKey:@"UIImagePickerControllerOriginalImage"] CGImage]];
            */
            
            /*
            imgForReceipt =[self correctImageOrientation: [[info valueForKey:@"UIImagePickerControllerOriginalImage"] CGImage]];
             
            */
            imgviewForNoData.hidden = TRUE;
            
        }
        else
        {
            imgForReceipt = theImage; //new
            /*
            imgForReceipt =[self correctImageOrientation: [[info valueForKey:@"UIImagePickerControllerOriginalImage"] CGImage]];
             */
            imgviewForNoData.hidden = TRUE;
            
        }
        
        
	}

    
    UIImage *fixedOrientationImage = [UIImage imageWithCGImage:imgForReceipt.CGImage
                                                         scale:imgForReceipt.scale
                                                   orientation:imgForReceipt.imageOrientation];
    
    
    
    
    
    float actualHeight = fixedOrientationImage.size.height;
    float actualWidth = fixedOrientationImage.size.width;
    
    
    nslog(@"\n actualWidth before = %f \n",actualWidth);
    nslog(@"\n actualHeight before = %f \n",actualHeight);
    
    float imgRatio = actualWidth/actualHeight;
    float maxRatio;
    
    float approx_height;
    float approx_width;
    
    
    
    
    if(appDelegate.isIPad)
    {
        //Pending..
        
        approx_width= 320;
        
       
       
        approx_height = 524.0f;
        maxRatio = 320/524.0f;
       

        
        if(imgRatio!=maxRatio)
        {
            if(imgRatio < maxRatio)
            {
                imgRatio = approx_height / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = approx_height;
            }
            else
            {
                imgRatio = approx_width / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = approx_width;
            }
        }
        
        nslog(@"\n actualWidth = %f \n",actualWidth);
        nslog(@"\n actualHeight = %f \n",actualHeight);
        
        CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        [fixedOrientationImage drawInRect:rect];
        fixedOrientationImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        
        
        fixedOrientationImage = [UIImage imageWithData:UIImageJPEGRepresentation(fixedOrientationImage,0.7)];

        
        
        
    }
    else
    {
        approx_width= 320;
        
        if(appDelegate.isIphone5)
        {
            approx_height = 524.0f;
            maxRatio = 320/524.0f;
        }
        else
        {
            approx_height = 436;
            maxRatio = 320/760.0;
        }
        
        
        
        
        if(imgRatio!=maxRatio)
        {
            if(imgRatio < maxRatio)
            {
                imgRatio = approx_height / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = approx_height;
            }
            else
            {
                imgRatio = approx_width / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = approx_width;
            }
        }
        
        nslog(@"\n actualWidth = %f \n",actualWidth);
        nslog(@"\n actualHeight = %f \n",actualHeight);
        
        CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        [fixedOrientationImage drawInRect:rect];
        fixedOrientationImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        
        
        fixedOrientationImage = [UIImage imageWithData:UIImageJPEGRepresentation(fixedOrientationImage,0.7)];
        
        
    }
    
    
   
    
    
    
    
    imgForReceipt = fixedOrientationImage;
    
    appDelegate.is_receipt_image_choosen= TRUE;
    appDelegate.is_receipt_image_updated= TRUE;
    
    //[imgForReceipt retain];
    
    
    
    receipt_imageView.image = imgForReceipt;
    appDelegate.temp_receipt_image = imgForReceipt;
    //Precaution for deleting images...
    
    
    nslog(@"\n appDelegate.receipt_image_name deletion = %@",appDelegate.receipt_image_name);
    
    

    
    
    
    
    //Precaution for deleting images...

    
    
    
//    [appDelegate removeImageFromDirectory:appDelegate.receipt_image_name];
//    [appDelegate removeImageFromDirectory:appDelegate.receipt_small_image_name];

    
    /**/
    
    if([[appDelegate.receipt_image_name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0 || appDelegate.income_expense_is_recursive == TRUE)
    {
        //NSDate*date = [NSDate date];
        
        //float time_interval = [date timeIntervalSinceNow];
        
        NSString*temp_random_number = [appDelegate generateRandomImageName];
        nslog(@"\n temp_random_number = %@",temp_random_number);
        
        
        appDelegate.receipt_image_name          = [NSString stringWithFormat:@"%@.png",temp_random_number];
        appDelegate.receipt_small_image_name    = [NSString stringWithFormat:@"%@_small.png",temp_random_number];
        nslog(@"\n appDelegate.receipt_image_name = %@",appDelegate.receipt_image_name);
    }
    
    
    
    
    
    
    
    
    [popoverController dismissPopoverAnimated:TRUE];
	[picker_local dismissModalViewControllerAnimated:YES];
}


-(UIImage*)correctImageOrientation:(CGImageRef)image
{
    
    
    CGFloat width = CGImageGetHeight(image);//189;
	CGFloat height = CGImageGetWidth(image);//134;
	CGRect bounds = CGRectMake(0.0f, 0.0f, width, height);
	
	CGFloat boundHeight = bounds.size.height;
	bounds.size.height = bounds.size.width;
	bounds.size.width = boundHeight;
	
	CGAffineTransform transform = CGAffineTransformMakeTranslation(height, 0.0f);
	transform = CGAffineTransformRotate(transform, M_PI / 2.0f);
	
	UIGraphicsBeginImageContext(bounds.size);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextScaleCTM(context, - 1.0f, 1.0f);
	CGContextTranslateCTM(context, -height, 0.0f);
	CGContextConcatCTM(context, transform);
	
	CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, width, height), image);
	
	UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	
	UIGraphicsEndImageContext();
	
	return imageCopy;
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

-(void)dealloc
{
    
    [receipt_imageView release];
    [imgviewForNoData release];
    [super dealloc];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    /*
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            appDelegate.imghoverHome.frame = CGRectMake(224, 0, 107, 44);
            appDelegate.imghoverAddExpense.frame = CGRectMake(331, 0, 107, 44);
            appDelegate.imghoverSettings.frame = CGRectMake(438, 0, 107, 44);
            
            
            
        }
        else
        {
            appDelegate.imghoverHome.frame = CGRectMake(351, 0, 107, 44);
            appDelegate.imghoverAddExpense.frame = CGRectMake(458, 0, 107, 44);
            appDelegate.imghoverSettings.frame = CGRectMake(565, 0, 107, 44);
            
        }
    }
     */
    
    if(appDelegate.isIPad)
    {
        [appDelegate manageViewControllerHeight];
    }

    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
       
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    if(appDelegate.isIPad)
    {
        return YES;
        
    }
    return NO;
}




@end
