//
//  ReceiptViewController.h
//
//  Created by Chirag@Sunshine on 06/03/13.
//  
//

#import <UIKit/UIKit.h>

#import "PropertyLogBookAppDelegate.h"
@interface ReceiptViewController : UIViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate>
{
    PropertyLogBookAppDelegate *appDelegate;
    UIImage *imgForReceipt;
    IBOutlet UIImageView *receipt_imageView;
    
    UIImagePickerController *picker;
    IBOutlet UIImageView *imgviewForNoData;
    
    UIPopoverController *popoverController;
    
    BOOL is_image_choosen;
    
    
    BOOL is_dismiss_properly;
    
    UIActionSheet*actionSheet_temp;
    
    
}

-(UIImage*)correctImageOrientation:(CGImageRef)image;

@end
