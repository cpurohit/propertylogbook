//
//  customCell.h
//  productTracker
//
//  Created by Smit Sunshine on 12/08/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@class AppDelegate;
@interface customCell_plb : UITableViewCell 
{
    AppDelegate *obj_appdelegate;
    UILabel *lbl,*lbl_triptype,*lbl_currencyName;
    UITextField *txt_triptype;
    UIImageView *imgView,*imgviewBudget,*imgviewTriptype,*imgviewDestination,*imgviewAccessory;
    
    UILabel *lbl_TripName,*lbl_startEndDate,*lbl_totalBudget,*lbl_tripbudget;
    UILabel *lbl_destination;
    
    /*
	UILabel *lbl,*lbl_sound;
    UISwitch *swtch_alarm;
    UIButton *btn_dateandtime;
    UITextField *txt_alarmName;
    UIImageView *imgView;*/
//    UITextView *txtview;
//    UIButton *btn_dateTime,*btn_chapterNew;
    
   
	//UILabel *lbl_productAmount;
	
}
@property (nonatomic, retain) UILabel *lbl,*lbl_triptype,*lbl_TripName,*lbl_startEndDate,*lbl_totalBudget,*lbl_currencyName,*lbl_tripbudget,*lbl_destination;
@property (nonatomic,retain) UITextField *txt_triptype;
@property (nonatomic, retain)UIImageView *imgView,*imgviewBudget,*imgviewTriptype,*imgviewDestination,*imgviewAccessory;

/*
@property (nonatomic, retain) UILabel *lbl,*lbl_sound;
@property (nonatomic,retain)UISwitch *swtch_alarm;
@property(nonatomic,retain) UIButton *btn_dateandtime;
@property (nonatomic,retain) UITextField *txt_alarmName;
@property (nonatomic, retain)UIImageView *imgView;*/

//@property (nonatomic,retain) UITextView *txtview;

//@property (nonatomic,retain) UIButton *btn_dateTime,*btn_chapterNew;



//@property (nonatomic, retain)UILabel *lbl_productAmount;

@end
