//
//  CustomCell1.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"

@interface CustomCell1 : UITableViewCell<UITextFieldDelegate> {
    UILabel *agentLabel;
    UITextField *agentTextField;
    UISwitch *agentManageSwitch;
    UIButton *agentDateButton;
    UILabel *agentDetailLabel;
    PropertyLogBookAppDelegate *appDelegate;    
    
}
@property (nonatomic, retain)UILabel *agentLabel;
@property (nonatomic, retain)UITextField *agentTextField;
@property (nonatomic, retain)UIButton *agentDateButton;
@property (nonatomic, retain)UISwitch *agentManageSwitch;
@property (nonatomic, retain)UILabel *agentDetailLabel;

@end
