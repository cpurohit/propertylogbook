//
//  AddReminderTypeViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"


@interface AddReminderTypeViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource> 
{
    
    IBOutlet UITableView *tblView;
    UITextField *reminderText;

    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UILabel *lblNoData,*lblPlus;
    NSIndexPath *oldIndex;
    int textFieldIndex;
    UIButton *backButton;
    IBOutlet UIImageView *imageView;
    
    NSMutableArray *tempRowArray;
    
    UIToolbar *keytoolBar;
    int isClicked;
    NSString *textFieldTempText;
    BOOL isAddClicked;
    
    
    
}

-(void) moveFromOriginal:(NSInteger)indexOriginal toNew:(NSInteger)indexNew;

@end
