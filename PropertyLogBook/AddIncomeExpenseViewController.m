//
//  AddIncomeExpenseViewController.m
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddIncomeExpenseViewController.h"
#import "propertyViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "PropertyLogBookAppDelegate.h"
#import "AddIncomeTypeViewController.h"
#import "AddExpenseTypeViewController.h"
#define NUMBERS	@"0123456789."
#import "PropertyLogBookAppDelegate.h"
#import "ActionSheetPicker.h"
#import "UIPlaceHolderTextView.h"
#import "IncomeExpenseViewController.h"

#import "ReceiptViewController.h"
#import "UIImage+Resize.h"
#import "HomeViewController.h"

@implementation AddIncomeExpenseViewController
@synthesize actionSheetPicker = _actionSheetPicker,originalEndDateString;

#pragma mark -
#pragma mark View lifecycle

- (BOOL)prefersStatusBarHidden
{
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.isIPad)
    {
        
        return YES;
    }
    return NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /* CP :  */
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    
    /*ios 7 condition*/
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }
    else
    {
        if(appDelegate.isIOS7)
        {
            tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y-25.0f,tblView.frame.size.width,self.view.frame.size.height+25.0f);
        }

    }

    
    
    
    
    
    
     appDelegate.prefs = [NSUserDefaults standardUserDefaults];
    nslog(@"\n viewdidLoad in AddIncomeExpsneViewController.. ");
    nslog(@"\n milage = %@ ",[appDelegate.prefs objectForKey:@"milage_unit"]);
    
    nslog(@"\n appDelegate.receipt_image_name = %@",appDelegate.receipt_image_name);
    
    
    
    textFieldIndex = -1;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
	
    if([appDelegate.propertyDetailArray count]==0)
    {
        [appDelegate selectPropertyDetail];
    }
    
    
    placeHolderTextView = [[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(10, 7, 280, 70)];
    if (!appDelegate.isIncomeExpenseUpdate)
    {
        isRecurringRecord = FALSE;
        
        if(!appDelegate.isMilege)/*SUN:IF NOT MILEGE THEN CHECK FOR
                                      INCOME AND EXPENSE,AS INCOME AND EXPENSE ARE MANAGED BY SAME VARIABLE..*/
        {
            if (appDelegate.isIncome)
            {
                self.navigationItem.title = @"ADD INCOME";
            }
            else
            {
                self.navigationItem.title = @"ADD EXPENSE";
            }
        }
        else
        {
            self.navigationItem.title = @"ADD MILEAGE";
        }
    }
    else
    {
        
        if(!appDelegate.isMilege)
        {
            if (appDelegate.isIncome)
            {
                self.navigationItem.title = @"EDIT INCOME";
            }
            else
            {
                self.navigationItem.title = @"EDIT EXPENSE";
            }
        }
        else
        {
            self.navigationItem.title = @"EDIT MILEAGE";
        }
        
        
    }
    dictionary = [[NSMutableDictionary alloc]init];
    
    if([appDelegate.propertyTypeArray count]==0)
    {
        [appDelegate selectPropertyType];
    }
    
    
    if (!appDelegate.isIncome)
    {
        if([appDelegate.expenseTypeArray count]==0)
        {
            [appDelegate selectExpenseType];
        }
        
        
    }
    else
    {
        if([appDelegate.incomeTypeDBArray count]==0)
        {
            [appDelegate selectIncomeType];
        }
        
        
    }
    
    if (!appDelegate.isIncomeExpenseUpdate)
    {
        
        if ([appDelegate.propertyDetailArray count]>0)
        {
            appDelegate.incomeProperty = [[appDelegate.propertyDetailArray objectAtIndex:0]valueForKey:@"0"];
        }
        else
        {
            appDelegate.incomeProperty = @"Property";
        }
        
        
        
        if(appDelegate.isMilege)
        {
            appDelegate.incomeString = @"Mileage";
        }
        else
        {
            if (appDelegate.isIncome)
            {
                if ([appDelegate.incomeTypeDBArray count]>0)
                {
                    appDelegate.incomeString = [[appDelegate.incomeTypeDBArray objectAtIndex:0]valueForKey:@"incomeType"];
                }
                else
                {
                    appDelegate.incomeString = @"Income type";
                }
                
            }
            else
            {
                if ([appDelegate.expenseTypeArray count]>0)
                {
                    appDelegate.incomeString = [[appDelegate.expenseTypeArray objectAtIndex:0]valueForKey:@"expenseType"];
                }
                else
                {
                    appDelegate.incomeString = @"Expense Type";
                }
            }
        }
        
        
        
        
        [dictionary setObject:@"0.00" forKey:@"amount"];
        
        [dictionary setObject:@""forKey:@"notes"];
        
        [dictionary setObject:@"0" forKey:@"isrecursive"];
        [dictionary setObject:@"" forKey:@"frequency"];
        [dictionary setObject:@"" forKey:@"enddate"];
        [dictionary setObject:@"" forKey:@"date"];
        [dictionary setObject:@"0" forKey:@"is_receipt_image_choosen"];

        
        
        NSDate *currentDate = [NSDate date];
        [dictionary setObject:[appDelegate.regionDateFormatter stringFromDate:currentDate] forKey:@"date"];
        
        /*
         Start:
         Date:06-03-2013
         SUN:0004
         Manages date and month label..
         */
        
        
        NSDateFormatter*formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd"];
        
        
        icome_expense_date_label.text = [formatter stringFromDate:[appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]]];
        
        
        [formatter setDateFormat:@"MMM"];
        icome_expense_month_label.text = [formatter stringFromDate:[appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]]];
        
        
        [formatter release];
        
        /*
         Start:
         Date:06-03-2013
         SUN:0004
         Manages date and month label..
         */
        
        
        
    }
    else
    {
        /*Starts:Old data to check whether data has been changed or not.*/
        
        oldDataDictionary = [[NSMutableDictionary alloc] init];
        [oldDataDictionary setObject:[appDelegate.regionDateFormatter stringFromDate:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"date"]] forKey:@"date"];
        
        
        
                
        
        [oldDataDictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"amount"] forKey:@"amount"];
        
        [oldDataDictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"notes"] forKey:@"notes"];
        
        [oldDataDictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"property"] forKey:@"property"];
        [oldDataDictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"type"] forKey:@"type"];
        
        
        
        /*Ends:Old data to check whether data has been changed or not.*/
        
        appDelegate.incomeProperty = [[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"property"];
        
        appDelegate.incomeString = [[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"type"];
        
        
        
        
        
        
        
        [dictionary setObject:[appDelegate.regionDateFormatter stringFromDate:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"date"]] forKey:@"date"];
        
        [dictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"amount"] forKey:@"amount"];
        
        [dictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"notes"] forKey:@"notes"];
        
        [dictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"isrecursive"] forKey:@"isrecursive"];
        
        if([[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"isrecursive"] intValue]==0)
        {
            appDelegate.income_expense_is_recursive = FALSE;
        }
        else
        {
            appDelegate.income_expense_is_recursive = TRUE;
        }
        
       
        
        
        [dictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"frequency"] forKey:@"frequency"];
        
        recurrenceTime = [[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"frequency"];
        
        [dictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"parentid"] forKey:@"parentid"];
        
        [dictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"rowid"] forKey:@"rowid"];
        
        
        [dictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"is_receipt_image_choosen"] forKey:@"is_receipt_image_choosen"];
        
        [dictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"receipt_image_name"] forKey:@"receipt_image_name"];
        
        [dictionary setObject:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"receipt_small_image_name"] forKey:@"receipt_small_image_name"];
        
        
        /*
         Start:
         Date:06-03-2013
         SUN:0004
         Manages date and month label..
         */
        
        
        NSDateFormatter*formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd"];
        
        
        icome_expense_date_label.text = [formatter stringFromDate:[appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]]];
        
        
        [formatter setDateFormat:@"MMM"];
        icome_expense_month_label.text = [formatter stringFromDate:[appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]]];
        
        
        
        
        
        [formatter release];
        
        /*
         Start:
         Date:06-03-2013
         SUN:0004
         Manages date and month label..
         */
        

        
        
        
        if([[dictionary objectForKey:@"is_receipt_image_choosen"] intValue]==1)
        {
            appDelegate.is_receipt_image_choosen = TRUE;
            appDelegate.receipt_image_name = [dictionary objectForKey:@"receipt_image_name"];
            appDelegate.receipt_small_image_name = [dictionary objectForKey:@"receipt_small_image_name"];
            appDelegate.receipt_image = [appDelegate getImage:appDelegate.receipt_small_image_name];
            
        }
        else
        {
            appDelegate.is_receipt_image_choosen = FALSE;
        }
        
        
        
        
        if([[dictionary objectForKey:@"isrecursive"] isEqualToString:@"1"])
        {
            isRecurring = YES;
            isRecurringRecord = YES;
            [dictionary setObject:[appDelegate.regionDateFormatter stringFromDate:[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"enddate"]] forKey:@"enddate"];
            
            originalEndDateString = [[NSString alloc] initWithString:[dictionary objectForKey:@"enddate"]];
            nslog(@"\n originalEndDateString = %@",originalEndDateString);
            
        }
        else if([[dictionary objectForKey:@"isrecursive"] isEqualToString:@"0"])
        {
            [dictionary setObject:@"" forKey:@"enddate"];
            isRecurring = NO;
            isRecurringRecord = NO;
        }
        else//updated db
        {
            isRecurring = NO;
            isRecurringRecord = NO;
        }
        
    }
    
    nslog(@"\n dictionary = %@",dictionary);
    nslog(@"\n old dictionary = %@",oldDataDictionary);
   
    if(appDelegate.isMilege)
    {
        
        
       
        
        /*
        [icome_expense_amount_text setText:[dictionary objectForKey:@"amount"]];
        */
        [icome_expense_amount_text setText: [NSString stringWithFormat:@"%@ %@",[appDelegate.prefs objectForKey:@"milage_unit"],[dictionary objectForKey:@"amount"]]];
    }
    else
    {
        
        [icome_expense_amount_text setText:[numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[dictionary objectForKey:@"amount"]doubleValue]]]];
    }
    
    
    
    /*
	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save_clicked)] autorelease];
     */
    
    
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * save_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [save_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        save_button.bounds = CGRectMake(0, 0,51.0,30.0);
        [save_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_save_button.png"] forState:UIControlStateNormal];
        [save_button addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn1 =[[[UIBarButtonItem alloc] initWithCustomView:save_button] autorelease];
        
        UIBarButtonItem *negativeSeperator1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator1.width = -12;
        
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSeperator1,barbtn1, nil]];
        
    }
    else
    {
        UIButton * save_button = [UIButton buttonWithType:UIButtonTypeCustom];
        [save_button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        save_button.bounds = CGRectMake(0, 0,51.0,30.0);
        [save_button setBackgroundImage:[UIImage imageNamed:@"navigationbar_save_button.png"] forState:UIControlStateNormal];
        [save_button addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:save_button] autorelease];
        
    }

	
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barbtn =[[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
        
        UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSeperator.width = -12;
        
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSeperator,barbtn, nil] animated:NO];
        
        
    }
    else
    {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        button.bounds = CGRectMake(0, 0,49.0,29.0);
        [button setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancel_clicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
    }

    
    
    
	
    recurringArray  = [[NSMutableArray alloc]initWithObjects:@"Recurring",@"Frequency",@"End Date",@"Notes",nil];
    
    if (appDelegate.isIncome)
    {
        /*Frequency is  in this version*/
        mainArray = [[NSMutableArray alloc]initWithObjects:@"Property",@"Income Type",nil];
        
        
        
    }
    else
    {
        
        mainArray = [[NSMutableArray alloc]initWithObjects:@"Property",@"Expense Type",nil];
        
        
        
    }
	detailArray = [[NSMutableArray alloc]initWithObjects:@"Select Property",@"Select Type",@"Select Date",@"Select Amount",@"Notes",nil];
    
    /*
     recurringDetailArray = [[NSMutableArray alloc]initWithObjects:@"Recurring",@"Frequency",@"End Date",nil];
     */
    
    
    
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //nslog(@"dict %@",dictionary);
	tblView.backgroundColor = [UIColor clearColor];
	
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];
    
	UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0,  1024,1024)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
    
    [imgView release];
    
    if(appDelegate.isIPad)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    }
    
    
    [df release];
    
    
    /*Starts:viewWillAppear*/
    
    appDelegate = (PropertyLogBookAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
        
        [appDelegate selectCurrencyIndex];
    }
    
    numberFormatter = [[NSNumberFormatter alloc]init];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    nslog(@"\n locale = %@",[numberFormatter locale].localeIdentifier) ;
    
    
    
    if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
    {
        [numberFormatter setCurrencyCode:appDelegate.curCode];
        
        
        if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
        {
            [numberFormatter setCurrencyCode:@"USD"];
            nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"CNY"])
        {
            [numberFormatter setCurrencyCode:@"CNY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"JPY"])
        {
            [numberFormatter setCurrencyCode:@"JPY"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        else if([[numberFormatter currencyCode] isEqualToString:@"NAD"])
        {
            [numberFormatter setCurrencyCode:@"NAD"];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"af_NA"];
            [numberFormatter setLocale:locale];
            [locale release];
            
        }
        
        
    }
    datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 200, 320,216 )];
    datePicker.backgroundColor = [UIColor whiteColor];
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,156, 320, 44)];
    
    datePicker.hidden = TRUE;
    toolBar.hidden = TRUE;
    
    
    [self.view addSubview:datePicker];
    [self.view addSubview:toolBar];
    
    pickerArray = [[NSMutableArray alloc]initWithObjects:@"Weekly",@"Every 2 weeks",@"Monthly",@"Quarterly",@"Yearly", nil];
    
    
    pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 200, 320, 216)];
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    
    
    //pickerView.tag = 1052;
    [pickerView setShowsSelectionIndicator:YES];
    pickerView.hidden = YES;
    [self.view addSubview:pickerView];
    
    if (appDelegate.isIncomeExpenseUpdate)
    {
        NSString*trimmedString = [[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"frequency"] stringByTrimmingCharactersInSet:
                                  [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        nslog(@"\n trimmedString = %@",trimmedString);
        if([trimmedString isEqualToString:@"Weekly"])
        {
            [pickerView selectRow:0 inComponent:0 animated:NO];
        }
        else if([trimmedString isEqualToString:@"Every 2 weeks"])
        {
            [pickerView selectRow:1 inComponent:0 animated:NO];
        }
        else if([trimmedString isEqualToString:@"Monthly"])
        {
            [pickerView selectRow:2 inComponent:0 animated:NO];
        }
        else if([trimmedString isEqualToString:@"Quarterly"])
        {
            [pickerView selectRow:3 inComponent:0 animated:NO];
        }
        //int pickerIndex = [pickerArray indexOfObjectIdenticalTo:trimmedString];
        //nslog(@"\n pickerIndex = %d",pickerIndex);
        
    }
    
    
    /*Ends:viewWillAppear*/
    
    /*
     Starts:
     Sun:0004
     Checking iphone-5 condition.
     
     */
    if(appDelegate.isIphone5)
    {
        datePicker.frame = CGRectMake(datePicker.frame.origin.x, datePicker.frame.origin.y+44,datePicker.frame.size.width, datePicker.frame.size.height);
        toolBar.frame= CGRectMake(toolBar.frame.origin.x,200,toolBar.frame.size.width, toolBar.frame.size.height);
        pickerView.frame= CGRectMake(pickerView.frame.origin.x, pickerView.frame.origin.y+44,pickerView.frame.size.width, pickerView.frame.size.height);
    }
    
    

    
    
    /*
     Ends :
     Sun:0004
     Checking iphone-5 condition.
     */
    

    
}
-(void)viewWillDisappear:(BOOL)animated
{
    
    //[appDelegate selectIncomeExpense];
    
    [super viewWillAppear:animated];
    toolBar.hidden = YES;
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    appDelegate = (PropertyLogBookAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /*Sun:0004
     Managing bottom toolbar
     */
    
    
    [appDelegate.add_income_expense_bottom_button removeTarget:nil
                                                        action:NULL
                                              forControlEvents:UIControlEventAllEvents];

    //[self.view addSubview:appDelegate.bottomView];
     
    /* no need to give option here...
    
    [appDelegate.add_income_expense_bottom_button addTarget:self action:@selector(show_income_expense_button_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    */
    
    
    
    /*Sun:0004
     Managing bottom toolbar
     */

    
    
    if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
        [appDelegate selectCurrencyIndex];
    }
    
    NSString *reqSysVer = @"5.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    nslog(@"\n currSysVer = %@",currSysVer);
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
    {
        isIOS5 = YES;
        nslog(@"\n c1");
    }
    else
    {
        isIOS5 = NO;
        nslog(@"\n c2");
    }
    
    nslog(@"\n tblView.frame = %f",tblView.frame.size.height);
    
    nslog(@"\n viewWillAppear in AddIncomeExpenseViewController.");
    
    
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }

    
    
    [tblView reloadData];
    
    //These are requred as user can come back after switching app,(when there is password)
    
    
    if(appDelegate.isIPad)
    {

        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0.1];
        [self didRotateFromInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    
    }
    [self.view endEditing:TRUE];
    
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations.
    if(appDelegate.isIPad)
    {
        return  YES;
    }
    return FALSE;
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if(appDelegate.isIPad)
    {
        
        [appDelegate manageViewControllerHeight];
        [tblView reloadData];
    }
    
}
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //
    
    if(appDelegate.isIPad)
    {
        toolBar.hidden = TRUE;
        pickerView.hidden = TRUE;
        datePicker.hidden = TRUE;
        
        if(toInterfaceOrientation == 1 || toInterfaceOrientation == 2 )
        {
            datePicker.frame =  CGRectMake(0,715, 768,216 );
            pickerView.frame =  CGRectMake(0,715, 768,216 );
            toolBar.frame = CGRectMake(0, 671,768, 44);
            
            
        }
        else if(toInterfaceOrientation == 3 || toInterfaceOrientation == 4 )
        {
            datePicker.frame =  CGRectMake(0,513,1024,116 );
            pickerView.frame =  CGRectMake(0,513,1024,116 );
            toolBar.frame = CGRectMake(0,469,1024, 44);
        }
        
        
        
        
    }
    
    
}


#pragma mark - show_income_expense_button_clicked
#pragma mark Manages navigation for AddEditIncomeExpense

-(void)show_income_expense_button_clicked
{
    [appDelegate show_income_expense_button_clicked:self.navigationController];
}

#pragma mark -
#pragma mark pickerview data

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [pickerArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	return [pickerArray objectAtIndex:row];
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    [frequencyButton setTitle:[pickerArray objectAtIndex:row] forState:UIControlStateNormal];
    
    
    if([[dictionary objectForKey:@"frequency"] isEqualToString:@"Every 2 weeks"])
    {
        frequencyButton.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    }
    else
    {
        frequencyButton.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    }
    
    
    
    
    [dictionary setObject:[pickerArray objectAtIndex:row] forKey:@"frequency"];
    recurrenceTime = [pickerArray objectAtIndex:row];
    
    //[propertyInfoDictionary setObject:btn.titleLabel.text forKey:@"repayment"];
    //    [repayment retain];
}



#pragma mark -
#pragma mark rotation Notifications

- (void) didRotate:(NSNotification *)notification
{
    
    nslog(@"\n keyBoardHide...");
    /*
     if(appDelegate.isIPad)
     {
     datePicker.hidden = YES;
     toolBar.hidden = YES;
     
     }
     */
    
}




#pragma mark - cancel_clicked
#pragma mark manages popViewControllerAnimated


-(void)cancel_clicked
{
    
    
    /*
     Sun:0004
     Managing viewcontroller stack.So when user presses back,he will be redirected to IncomeExpenseViewController and not form where he come.
     
    */
    
    appDelegate.receipt_image_name = @"";
    
    if(appDelegate.isFromButtonClick)
    {
        NSMutableArray*viewControllerArray = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]] ;
        nslog(@"\n viewControllerArray before= %@",viewControllerArray);
        
        /*Sun:004
         Managing receipt image name.
         */
        
        
        int is_homeViewController_exists = 0;
        int is_incomeExpenseViewController_exists = 0;
        
        for(int i=0;i<[viewControllerArray count]-1;i++)
        {
            //[viewControllerArray removeObjectAtIndex:i];
            
             if([[viewControllerArray objectAtIndex:i] isKindOfClass:[HomeViewController class]])
             {
                 is_homeViewController_exists = 1;
             }
            else if([[viewControllerArray objectAtIndex:i] isKindOfClass:[IncomeExpenseViewController class]])
            {
                 is_incomeExpenseViewController_exists = 1;
            }
        }
        
        
       
        
       
        HomeViewController*homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        if(is_homeViewController_exists!=1)
        {
            [viewControllerArray insertObject:homeViewController atIndex:[viewControllerArray count]-1];
        }
        [homeViewController release];
        
        IncomeExpenseViewController*incomeExpenseViewController = [[IncomeExpenseViewController alloc] initWithNibName:@"IncomeExpenseViewController" bundle:nil];
        if(is_incomeExpenseViewController_exists!=1)
        {
            [viewControllerArray insertObject:incomeExpenseViewController atIndex:[viewControllerArray count]-1];
        }
        self.navigationController.viewControllers =  [NSArray arrayWithArray:viewControllerArray];

        
        [incomeExpenseViewController release];
        
        appDelegate.isFromButtonClick = FALSE;
    }
    

    nslog(@"\n receipt_image_name on leaving = %@",appDelegate.receipt_image_name);

    if(appDelegate.temp_receipt_image != nil)
    {
        appDelegate.temp_receipt_image = nil;
    }
    
       
	[self.navigationController popViewControllerAnimated:YES];
}


-(void)save_clicked
{
    
    
    appDelegate.temp_receipt_image = nil;
    
    
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }

    
    
    
    UIButton *btn = (UIButton *)[self.view viewWithTag:100];
    
    toolBar.hidden = YES;
    [self.view endEditing:YES];
    
    BOOL recurringDateIssue = NO;
    BOOL recurringFrequencyIssue = NO;
    
    
    
    if(isRecurring == YES)
    {
        NSString *tmpEndDate   = [dictionary objectForKey:@"enddate"];
        NSString *tmpFrequency  = [dictionary objectForKey:@"frequency"];
        
        if(tmpFrequency==nil || [[tmpFrequency stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
        {
            
            recurringFrequencyIssue = YES;
        }
        
        else if(tmpEndDate==nil || [[tmpEndDate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
        {
            recurringDateIssue = YES;
        }
        
        
    }
    
   
   
    
     NSMutableString *amountStr = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@",[dictionary objectForKey:@"amount"]]];
    
    
   
    
    
    
    
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    
    
    
    [amountStr replaceOccurrencesOfString:numberFormatter.currencySymbol
                               withString:@""
                                  options:NSLiteralSearch
                                    range:NSMakeRange(0, [amountStr length])];
    
    [amountStr replaceOccurrencesOfString:numberFormatter.groupingSeparator
                               withString:@""
                                  options:NSLiteralSearch
                                    range:NSMakeRange(0, [amountStr length])];
    
    [amountStr replaceOccurrencesOfString:numberFormatter.decimalSeparator
                               withString:@"."
                                  options:NSLiteralSearch
                                    range:NSMakeRange(0, [amountStr length])];
    
    
    
    
    NSLog(@"\n textFieldStrValue = %@",amountStr);
    
    
    
    
    
    
    
    /*
    
    
    
    NSRange suffixRange = [amountStr rangeOfString:@","
                           
                                           options:(NSAnchoredSearch | NSCaseInsensitiveSearch | NSBackwardsSearch)];
    
    
    
    
    
    
    
    NSString *reqSysVer = @"7.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    nslog(@"\n currSysVer = %@",currSysVer);
    nslog(@"\n [numberFormatter locale].localeIdentifier = %@",[numberFormatter locale].localeIdentifier);
    
    
    if([[numberFormatter locale].localeIdentifier isEqualToString:@"en_FR"])
    {
        amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
    }
    
    
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
    {
        
        if([[numberFormatter locale].localeIdentifier isEqualToString:@"pt_PT"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_BR"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_AO"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_CV"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_GW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_ST"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_TL"] ||
           [[numberFormatter locale].localeIdentifier isEqualToString:@"bg_BG"]||
           [[numberFormatter locale].localeIdentifier isEqualToString:@"en_FR"]
           
           
           
           )
        {
            
            amountStr = [amountStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"€0123456789,."] invertedSet];
            amountStr = [[amountStr componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
            
        }
    }
    

    
    
    if(!appDelegate.isMilege)
    {
        
        
        
        NSNumberFormatter *numberFormatterTemp = [[NSNumberFormatter alloc] init] ;
        [numberFormatterTemp setNumberStyle: NSNumberFormatterCurrencyStyle];
        
        nslog(@"\n numberFormatter cur code.. = %@",[numberFormatterTemp currencyCode]);
        nslog(@"\n numberFormatter locale = %@",[numberFormatterTemp locale].localeIdentifier);
        
        
        
        
        
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        
        
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        
        
        NSMutableString *reversedStr;
        int len = [amountStr length];
        reversedStr = [NSMutableString stringWithCapacity:len];
        
        while (len > 0)
        {
            [reversedStr appendString:[NSString stringWithFormat:@"%C", [amountStr characterAtIndex:--len]]];
        }
        
        nslog(@"\n reversedStr = %@",reversedStr);
        
        NSRange d = [reversedStr rangeOfString:@","];//for example 3,50(normal 3.50)
       
        
        // NSRange r = [loanValue rangeOfString:@","];
        
        
        nslog(@"\n d = %d",d.location);
        
        
        if(appDelegate.isIOS7)
        {
            
            
            if ([amountStr rangeOfString:@" "].location != NSNotFound || d.location == 3)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
        }

        
        
        if ([amountStr rangeOfString:@" "].location != NSNotFound || d.location == 2)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        
        if ([amountStr rangeOfString:@"'"].location != NSNotFound)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"'" withString:@""];
        }
        
        if([[numberFormatter locale].localeIdentifier isEqualToString:@"en_FR"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        nslog(@"\n amound = %@",amountStr);
        
        
        amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([appDelegate.curCode isEqualToString:@"ZAR"])
        {
            if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            else
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@""];
            }
            
        }
        
        
        
        else if([[numberFormatter locale].localeIdentifier isEqualToString:@"en_BE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_BE"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_AW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_CW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_NL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_SX"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"id_ID"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_AO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_BR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_GW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_PT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_ST"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_AT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_DE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_LU"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_CY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_GR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"da_DK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"it_IT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_MD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_RO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_AR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_BO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_EC"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_GQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_PY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_UY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_VE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"seh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sg_CF"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rn_BI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mua_CM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ms_BN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mgh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mk_MK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"lu_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rw_RW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kl_GL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kea_CV"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"is_IS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ka_GE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"gl_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"fo_FO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"swc_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ca_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"bs_BA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"eu_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az-Cyrl_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sq_AL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MA"] ||
             [[numberFormatter locale].localeIdentifier isEqualToString:@"en_FR"]
                
                
                )
        {
            nslog(@"Str_budget -- %@",amountStr);
            
            NSString *stringToFilter =[NSString stringWithFormat:@"%@",amountStr];
            NSMutableString *targetString = [NSMutableString string];
            //set of characters which are required in the string......
            NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"."];
            nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
            
            for(int i = 0; i < [stringToFilter length]; i++)
            {
                unichar currentChar = [stringToFilter characterAtIndex:i];
                nslog(@"currentChar -- %C",currentChar);
                
                if(i == [stringToFilter length]-3)
                {
                    nslog(@"currentChar -- %C",currentChar);
                    [targetString appendFormat:@"%C", currentChar];
                }
                else
                {
                    if([okCharacterSet characterIsMember:currentChar])
                    {
                        [targetString appendFormat:@""];
                    }
                    else
                    {
                        [targetString appendFormat:@"%C", currentChar];
                    }
                }
            }
            nslog(@"targetString -- %@",targetString);
            amountStr = [NSString stringWithFormat:@"%@",targetString];
            nslog(@"Str_budget -- %@",amountStr);
        }
        
        else if([[numberFormatter locale].localeIdentifier isEqualToString:@"gsw_CH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rm_CH"])
        {
            nslog(@"Str_budget -- %@",amountStr);
            
                        NSString *stringToFilter =[NSString stringWithFormat:@"%@",amountStr];
            NSMutableString *targetString = [NSMutableString string];
            //set of characters which are required in the string......
            NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
            
            for(int i = 0; i < [stringToFilter length]; i++)
            {
                unichar currentChar = [stringToFilter characterAtIndex:i];
                nslog(@"currentChar -- %C",currentChar);
                
                if(i == [stringToFilter length]-3)
                {
                    nslog(@"currentChar -- %C",currentChar);
                    [targetString appendFormat:@"%C", currentChar];
                }
                else
                {
                    if([okCharacterSet characterIsMember:currentChar])
                    {
                        [targetString appendFormat:@"%C", currentChar];
                    }
                    else
                    {
                        [targetString appendFormat:@""];
                    }
                }
            }
            nslog(@"targetString -- %@",targetString);
            amountStr = [NSString stringWithFormat:@"%@",targetString];
            nslog(@"Str_budget -- %@",amountStr);
        }
        else if([[numberFormatter locale].localeIdentifier isEqualToString:@"ar_IQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_BH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_TD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DJ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_ER"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_JO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LB"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_OM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_PS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_QA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_AE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_001"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_YE"])
        {
            nslog(@"Str_budget -- %@",amountStr);
            NSString *stringToFilter =[NSString stringWithFormat:@"%@",amountStr];
            NSMutableString *targetString = [NSMutableString string];
            //set of characters which are required in the string......
            // NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@","];
            nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
            
            for(int i = 0; i < [stringToFilter length]; i++)
            {
                unichar currentChar = [stringToFilter characterAtIndex:i];
                nslog(@"currentChar -- %C",currentChar);
                
                if(i == [stringToFilter length]-3)
                {
                    nslog(@"currentChar -- %C",currentChar);
                    [targetString appendFormat:@"."];
                }
                else
                {
                    [targetString appendFormat:@"%C", currentChar];
                }
            }
            nslog(@"targetString -- %@",targetString);
            amountStr = [NSString stringWithFormat:@"%@",targetString];
            nslog(@"Str_budget -- %@",amountStr);
        }
        
        
        
        
        
        
        else
        {
            
            if ([appDelegate.curCode isEqualToString:@"Default Region Currency"])
            {
                
                
                if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
                {
                    amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
                }
                else
                {
                    amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@""];
                }
                
                
            }
            else
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@""];
            }
            
            
        }
        
        
        
        
        
        
        
        
    }
    
    
    */
    
    
    UITextView *textView = (UITextView *)[self.view viewWithTag:2004];
    
    float amountVal = [amountStr floatValue];
    if ((textView.text).length <=0)
    {
        textView.text = @"";
    }
    
    NSString*temp_date_string =  [dictionary objectForKey:@"date"];
    
    nslog(@"\n temp_date_string = %@",temp_date_string);
    
    if ([temp_date_string length] <= 0)
    {
        [btn setTitle:@"" forState:UIControlStateNormal];
    }
    
    if ((placeHolderTextView.text).length <=0)
    {
        placeHolderTextView.text = @"";
    }
    
    if (!appDelegate.isIncomeExpenseUpdate)
    {
        
        NSString* startDate = [[dictionary objectForKey:@"date"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString* endDate = [[dictionary objectForKey:@"enddate"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        //for backup...
        if(isRecurring)
        {
            [self countRecurrenceTime];
        }
        
        
        if ([appDelegate.incomeProperty isEqualToString:@"Property"])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Property is mandatory" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        else if ([appDelegate.incomeString isEqualToString:@"Income Type"] || [appDelegate.incomeString isEqualToString:@"Expense Type"])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Type is mandatory" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        else if ([[dictionary objectForKey:@"date"] length]<=0)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Date is mandatory" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        else if (amountVal<=0)
        {
            
            if(appDelegate.isMilege)
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Mileage should be greater than 0" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Amount should be greater than 0" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
            }
        }
        else if(recurringFrequencyIssue)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select frequency" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            /*
            recurringFrequencyIssue = YES;
             */
        }
        
        else if(isRecurring && ([endDate length]==0))
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select a future end date." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        
        else if([startDate isEqualToString:endDate] && isRecurring )
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select a future end date." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        
        else if(recurringDateIssue)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select a future end date." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            /*
            recurringDateIssue = YES;
             */
        }
        
        
        else
        {
            
            nslog(@"\n dictionaryu while saving new rec = %@",dictionary);
            recurrenceTime = [dictionary objectForKey:@"frequency"];
            NSString *dateString = [appDelegate fetchDateInCommonFormate:[dictionary objectForKey:@"date"]];
            
            
            NSString *recurringDateString = [appDelegate fetchDateInCommonFormate:[dictionary objectForKey:@"enddate"]];
            
            NSString*isRecurringValue = @"0";
            NSString*frequency= @" ";
            NSString *endDate= @" ";
            
            NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
            [dtFormatter setDateFormat:@"yyyy-MM-dd"];
           
            int is_receipt_image_choosen =1;
            if(appDelegate.is_receipt_image_choosen==FALSE)
            {
                
                appDelegate.receipt_small_image_name = @"";
                appDelegate.receipt_image_name = @"";
                is_receipt_image_choosen =0;
            }
            else
            {
                
                
                UIImage *fixedOrientationImage = [UIImage imageWithCGImage:appDelegate.receipt_image.CGImage
                                                                     scale:appDelegate.receipt_image.scale
                                                               orientation:appDelegate.receipt_image.imageOrientation];
                
                
                
                [appDelegate storeImageLocally:appDelegate.receipt_image_name image:fixedOrientationImage];

                
                [appDelegate storeImageLocally:appDelegate.receipt_small_image_name image:[appDelegate.receipt_image resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(200,200) interpolationQuality:kCGInterpolationHigh]];
            }
            
            
            
            if(isRecurring)
            {
                isRecurringValue = @"1";
                
                frequency = [recurrenceTime stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                endDate = recurringDateString;
                
                nslog(@"\n endDate = %@",endDate);
                
                
                [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                  isrecursive:isRecurringValue
                                    frequency:frequency
                                      enddate:endDate
                                     parentid:0
                     is_receipt_image_choosen:is_receipt_image_choosen
                           receipt_image_name:appDelegate.receipt_image_name
                     receipt_small_image_name:appDelegate.receipt_small_image_name
                 ];
                
                
                int parentId = [appDelegate getMaxRowidFromTable:@"IncomeExpenseTB"];
                nslog(@"\n parentID = %d",parentId);
                
                if([recurrenceTime isEqualToString:@"Weekly"])
                {
                    
                    
                    
                    
                    
                    for(int i=0;i<numberOfWeeks;i++)
                    {
                        
                        NSDate *dt = [dtFormatter dateFromString:dateString];
                        nslog(@"\n dt = %@",dt);
                        dt = [dt dateByAddingTimeInterval:(86400*7)];
                        
                        nslog(@"\n dt = %@",dt);
                        dateString = [dtFormatter stringFromDate:dt];
                        nslog(@"\n recursive dateString = %@",dateString);
                        
                        
                        [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                          isrecursive:isRecurringValue
                                            frequency:frequency
                                              enddate:endDate
                                             parentid:parentId
                             is_receipt_image_choosen:is_receipt_image_choosen
                                   receipt_image_name:appDelegate.receipt_image_name
                             receipt_small_image_name:appDelegate.receipt_small_image_name
                         ];
                        
                        
                        
                        
                    }
                    
                    
                }
                else if([recurrenceTime isEqualToString:@"Every 2 weeks"])
                {
                    
                    nslog(@"\n parentID = %d",parentId);
                    
                    for(int i=0;i<numberOfTwoWeeks;i++)
                    {
                        
                        NSDate *dt = [dtFormatter dateFromString:dateString];
                        nslog(@"\n dt = %@",dt);
                        dt = [dt dateByAddingTimeInterval:(86400*14)];
                        
                        nslog(@"\n dt = %@",dt);
                        dateString = [dtFormatter stringFromDate:dt];
                        nslog(@"\n recursive dateString = %@",dateString);
                        
                        
                        [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                          isrecursive:isRecurringValue
                                            frequency:frequency
                                              enddate:endDate
                                             parentid:parentId
                             is_receipt_image_choosen:is_receipt_image_choosen
                                   receipt_image_name:appDelegate.receipt_image_name
                             receipt_small_image_name:appDelegate.receipt_small_image_name
                         ];
                        
                        
                        
                        
                    }
                }
                else if([recurrenceTime isEqualToString:@"Monthly"])
                {
                    NSDate *dt = [dtFormatter dateFromString:dateString];
                    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                    
                    NSDateComponents *offsetComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:dt];
                    NSInteger theDay = [offsetComponents day];
                    NSInteger theMonth = [offsetComponents month];
                    NSInteger theYear = [offsetComponents year];
                    
                    /**
                    NSInteger tempDay = theDay;
                    */
                    NSInteger tempDay;
                    nslog(@"\n parentID in monthly = %d",parentId);
                    
                    nslog(@"\n numberOfMonths = %d",numberOfMonths);
                    for(int i=0;i<numberOfMonths;i++)
                    {
                        
                        tempDay = theDay;
                        
                        theMonth = theMonth+1;
                        [offsetComponents setMonth:theMonth];
                        if(theMonth >12)
                        {
                            //theYear++;
                        }
                        
                        
                        NSDateComponents *test_components = [[NSDateComponents alloc] init];
                        [test_components setDay:1];
                        [test_components setMonth:theMonth];
                        [test_components setYear:theYear];
                        NSDate *test_Date = [gregorian dateFromComponents:test_components];
                        [test_components release];
                        
                        
                        
                        NSRange days = [gregorian rangeOfUnit:NSDayCalendarUnit
                                                       inUnit:NSMonthCalendarUnit
                                                      forDate:test_Date];
                        
                        nslog(@"\n days range = %d",days.length);
                        
                        if(days.length<tempDay)
                        {
                            tempDay = days.length;
                        }
                        
                        
                        NSDateComponents *components = [[NSDateComponents alloc] init];
                        [components setDay:tempDay];
                        [components setMonth:theMonth];
                        [components setYear:theYear];
                        NSDate *thisDate = [gregorian dateFromComponents:components];
                        
                        
                        
                        
                        
                        
                        // nslog(@"\n thisDate = %@",thisDate);
                        
                        
                        
                        dateString = [dtFormatter stringFromDate:thisDate];
                        nslog(@"\n recursive dateString = %@",dateString);
                        
                        
                        [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                          isrecursive:isRecurringValue
                                            frequency:frequency
                                              enddate:endDate
                                             parentid:parentId
                             is_receipt_image_choosen:is_receipt_image_choosen
                                   receipt_image_name:appDelegate.receipt_image_name
                             receipt_small_image_name:appDelegate.receipt_small_image_name
                         
                         ];
                        
                        theMonth = [components month];
                        theYear = [components year];
                        
                        
                        
                        [components release];
                        
                    }
                    
                    //[offsetComponents release];
                    [gregorian release];
                    
                }
                else if([recurrenceTime isEqualToString:@"Quarterly"])
                {
                    
                    NSDate *dt = [dtFormatter dateFromString:dateString];
                    // start by retrieving day, weekday, month and year components for yourDate
                    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                    
                    NSDateComponents *offsetComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:dt];
                    NSInteger theDay = [offsetComponents day];
                    NSInteger theMonth = [offsetComponents month];
                    NSInteger theYear = [offsetComponents year];
                    
                    
                    
                    
                    nslog(@"\n parentID in quartely.. = %d",parentId);
                    nslog(@"\n numberOfQuarters = %d",numberOfQuarters);
                    
                    for(int i=0;i<numberOfQuarters;i++)
                    {
                        
                        theMonth = theMonth+3;
                        [offsetComponents setMonth:theMonth];
                        if(theMonth >12)
                        {
                            //theYear++;
                        }
                        
                        NSDateComponents *components = [[NSDateComponents alloc] init];
                        [components setDay:theDay];
                        [components setMonth:theMonth];
                        [components setYear:theYear];
                        NSDate *thisDate = [gregorian dateFromComponents:components];
                        
                        
                        
                        
                        //nslog(@"\n thisDate = %@",thisDate);
                        
                        
                        
                        dateString = [dtFormatter stringFromDate:thisDate];
                        nslog(@"\n recursive dateString = %@",dateString);
                        
                        
                        [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                          isrecursive:isRecurringValue
                                            frequency:frequency
                                              enddate:endDate
                                             parentid:parentId
                             is_receipt_image_choosen:is_receipt_image_choosen
                                   receipt_image_name:appDelegate.receipt_image_name
                             receipt_small_image_name:appDelegate.receipt_small_image_name
                         ];
                        
                        theMonth = [components month];
                        theYear = [components year];
                        [components release];
                        
                    }
                    
                    //[offsetComponents release];
                    [gregorian release];
                    
                    
                }
                else if([recurrenceTime isEqualToString:@"Yearly"])
                {
                    
                    NSDate *dt = [dtFormatter dateFromString:dateString];
                    // start by retrieving day, weekday, month and year components for yourDate
                    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                    
                    NSDateComponents *offsetComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:dt];
                    NSInteger theDay = [offsetComponents day];
                    NSInteger theMonth = [offsetComponents month];
                    NSInteger theYear = [offsetComponents year];
                    
                    
                    
                    
                    nslog(@"\n parentID = %d",parentId);
                    nslog(@"\n numberOfYears = %d",numberOfYears);
                    
                    for(int i=0;i<numberOfYears;i++)
                    {
                        
                        theYear = theYear+1;
                        [offsetComponents setYear:theYear];
                        if(theMonth >12)
                        {
                            //theYear++;
                        }
                        
                        NSDateComponents *components = [[NSDateComponents alloc] init];
                        [components setDay:theDay];
                        [components setMonth:theMonth];
                        [components setYear:theYear];
                        NSDate *thisDate = [gregorian dateFromComponents:components];
                        
                        
                        
                        
                        //nslog(@"\n thisDate = %@",thisDate);
                        
                        
                        
                        dateString = [dtFormatter stringFromDate:thisDate];
                        nslog(@"\n recursive dateString = %@",dateString);
                        
                        
                        [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                          isrecursive:isRecurringValue
                                            frequency:frequency
                                              enddate:endDate
                                             parentid:parentId
                             is_receipt_image_choosen:is_receipt_image_choosen
                                   receipt_image_name:appDelegate.receipt_image_name
                             receipt_small_image_name:appDelegate.receipt_small_image_name
                         ];
                        
                        theMonth = [components month];
                        theYear = [components year];
                        [components release];
                        
                    }
                    
                    //[offsetComponents release];
                    [gregorian release];
                    
                    
                }
                
                
                
            }
            else
            {
                
                if(appDelegate.isMilege)/*
                                            SUN:004
                                            IF MILEGE THEN INCOME - EXPENSE TYPE IS 2
                                         */
                {
                 
                    [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:2
                                      isrecursive:isRecurringValue
                                        frequency:frequency
                                          enddate:endDate
                                         parentid:0
                         is_receipt_image_choosen:is_receipt_image_choosen
                               receipt_image_name:appDelegate.receipt_image_name
                         receipt_small_image_name:appDelegate.receipt_small_image_name
                     ];
                    
                }
                else
                {
                    [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                      isrecursive:isRecurringValue
                                        frequency:frequency
                                          enddate:endDate
                                         parentid:0
                         is_receipt_image_choosen:is_receipt_image_choosen
                               receipt_image_name:appDelegate.receipt_image_name
                         receipt_small_image_name:appDelegate.receipt_small_image_name
                     ];
                }
                
                
                
            }
            
        
        
            [dtFormatter release];
            
            /*
             Sun:0004
             Managing viewcontroller stack.So when user presses back,he will be redirected to IncomeExpenseViewController and not form where he come.
             
             */
            if(appDelegate.isFromButtonClick)
            {
                NSMutableArray*viewControllerArray = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]] ;
                nslog(@"\n viewControllerArray before= %@",viewControllerArray);
                
                int is_homeViewController_exists = 0;
                int is_incomeExpenseViewController_exists = 0;
                
                for(int i=0;i<[viewControllerArray count]-1;i++)
                {
                    //[viewControllerArray removeObjectAtIndex:i];
                    
                    if([[viewControllerArray objectAtIndex:i] isKindOfClass:[HomeViewController class]])
                    {
                        is_homeViewController_exists = 1;
                    }
                    else if([[viewControllerArray objectAtIndex:i] isKindOfClass:[IncomeExpenseViewController class]])
                    {
                        is_incomeExpenseViewController_exists = 1;
                    }
                    
                    
                }
                
                
                
                
                
                HomeViewController*homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
                // [viewControllerArray addObject:homeViewController];
                
                
                if(is_homeViewController_exists!=1)
                {
                    [viewControllerArray insertObject:homeViewController atIndex:[viewControllerArray count]-1];
                }
                [homeViewController release];
                
                IncomeExpenseViewController*incomeExpenseViewController = [[IncomeExpenseViewController alloc] initWithNibName:@"IncomeExpenseViewController" bundle:nil];
                
                //[viewControllerArray addObject:incomeExpenseViewController];
                
                if(is_incomeExpenseViewController_exists!=1)
                {
                    [viewControllerArray insertObject:incomeExpenseViewController atIndex:[viewControllerArray count]-1];
                }
                [incomeExpenseViewController release];
                
                nslog(@"\n viewControllerArray = %@",viewControllerArray);
                self.navigationController.viewControllers =  [NSArray arrayWithArray:viewControllerArray];
                appDelegate.isFromButtonClick = FALSE;
            }
            /*
             Sun:0004
             Managing viewcontroller stack.So when user presses back,he will be redirected to IncomeExpenseViewController and not form where he come.
             
             */
            
            appDelegate.receipt_image_name = @"";
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }
    else//update
    {
        
        
         nslog(@"\n startdate = %@ ",dictionary );
        
        
        nslog(@"\n startdate = %@ ",[dictionary objectForKey:@"date"] );
        nslog(@"\n enddate = %@ ",[dictionary objectForKey:@"enddate"] );
        
        NSString* startDate = [[dictionary objectForKey:@"date"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString* endDate = [[dictionary objectForKey:@"enddate"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        NSString *tmpFrequency  = [dictionary objectForKey:@"frequency"];
        BOOL recurringFrequencyIssue = NO;
        if( (isRecurringRecord == FALSE && isRecurring == TRUE) && (tmpFrequency==nil || [[tmpFrequency stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0))
        {
            recurringFrequencyIssue = YES;
        }
        
        if ([appDelegate.incomeProperty isEqualToString:@"Property"])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Property is mandatory" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        else if ([appDelegate.incomeString isEqualToString:@"Income Type"] || [appDelegate.incomeString isEqualToString:@"Expense Type"])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Type is mandatory" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        
        else if ([[dictionary objectForKey:@"date"] length]<=0)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Date is mandatory" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        else if(recurringFrequencyIssue)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select frequency" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            /*
            recurringFrequencyIssue = YES;
             */
        }
        else if( (isRecurringRecord == FALSE && isRecurring == TRUE) && ([endDate length]==0))
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select a future end date." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        else if([startDate isEqualToString:endDate] && (isRecurringRecord == FALSE && isRecurring == TRUE))
        {
            
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select a future end date." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            
        }
        
        
        else if (amountVal<=0)
        {
            
            
            if(appDelegate.isMilege)
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Mileage should be greater than 0" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Amount should be greater than 0" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
            }
            
            
        }
        else//Start Updating
        {
            
            
            
            
            int is_receipt_image_choosen =1;
            if(appDelegate.is_receipt_image_choosen==FALSE)
            {
                
                appDelegate.receipt_small_image_name = @"";
                appDelegate.receipt_image_name = @"";
                is_receipt_image_choosen =0;
            }
            else if(appDelegate.is_receipt_image_updated==TRUE)/*Sun:004
                                                                if receipt is not update,it should not resize the same image...
                                                                */
            {
                
                
                
                
                
                UIImage *fixedOrientationImage = [UIImage imageWithCGImage:appDelegate.receipt_image.CGImage
                                                                     scale:appDelegate.receipt_image.scale
                                                               orientation:appDelegate.receipt_image.imageOrientation];
                
                
                
                [appDelegate storeImageLocally:appDelegate.receipt_image_name image:fixedOrientationImage];
                
                
               UIImage* temp_image_before_stroing = [appDelegate.receipt_image resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(appDelegate.receipt_image.size.width*0.25,appDelegate.receipt_image.size.height*0.25) interpolationQuality:kCGInterpolationNone];
                
                fixedOrientationImage = [UIImage imageWithCGImage:temp_image_before_stroing.CGImage
                                                            scale:temp_image_before_stroing.scale
                                                      orientation:temp_image_before_stroing.imageOrientation];
                
                [appDelegate storeImageLocally:appDelegate.receipt_small_image_name image:fixedOrientationImage];

                 
                
                
                
            }

            
            
            
            //NSString *dateString = [appDelegate fetchDateInCommonFormate:btn.titleLabel.text];
            NSString *dateString = [appDelegate fetchDateInCommonFormate:[dictionary objectForKey:@"date"]];
            
            NSDate* date1 = [appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]];
            NSDate* date2 = [appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"enddate"]];
            
            
            
            NSTimeInterval distanceBetweenDates = [date1 timeIntervalSinceDate:date2];
            double secondsInADay = 86400;
            NSInteger daysBetweenDates = distanceBetweenDates / secondsInADay;
            nslog(@"\n daysBetweenDates = %d",daysBetweenDates);
            
            NSDateFormatter *dtFormatter=[[NSDateFormatter alloc]init];
            [dtFormatter setDateFormat:@"yyyy-MM-dd"];
            
            
            NSCalendar* gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            
            
            NSString *recurringDateString = [appDelegate fetchDateInCommonFormate:[dictionary objectForKey:@"enddate"]];
            
            NSString*isRecurringValue = @"0";
            NSString*frequency= @" ";
            NSString *endDate= @" ";
            
            //if(![originalEndDateString isEqualToString:[dictionary objectForKey:@"enddate"]])
            //This condition is not useful as it may be possible child are deleted...
            {
                
                
                
                if(![[dictionary objectForKey:@"parentid"] isEqualToString:@"0"])
                {
                    //Child of parent
                    isRecurringValue = @"1";
                    endDate = recurringDateString;
                    /**
                    frequency =[dictionary objectForKey:@"frequency"];
                    */
                    
                }
                else if([[dictionary objectForKey:@"parentid"] isEqualToString:@"0"])
                {
                    //Main Entry
                    nslog(@"\n isRecurring = %d",isRecurring);
                    
                    if(isRecurring)
                    {
                        isRecurringValue = @"1";
                        endDate = recurringDateString;
                        /**
                        frequency =[dictionary objectForKey:@"frequency"];
                         */
                    }
                    
                    //checking if old record and new record are same..if not current entry is not recurring
                    
                    
                    
                    
                    //checking if old record and new record are same..if not current entry is not recurring
                    
                    if(isRecurring && isRecurringRecord==FALSE)
                    {
                        
                        
                        [appDelegate deleteRecursiveRecord:[dictionary objectForKey:@"rowid"] condition:@""];
                        frequency =[dictionary objectForKey:@"frequency"];
                        
                        endDate = recurringDateString;
                        
                        
                        
                        int parentId = [[dictionary objectForKey:@"rowid"] intValue];
                        
                        nslog(@"\n parentID = %d",parentId);
                        
                        [appDelegate updateParentFrequency:recurrenceTime pk:parentId];
                        
                        
                        if([recurrenceTime isEqualToString:@"Weekly"])
                        {
                            
                            numberOfWeeks = daysBetweenDates /7;
                            numberOfRemainingDays = daysBetweenDates %7;
                            numberOfWeeks *=-1;
                            nslog(@"\n numberOfWeeks = %d",numberOfWeeks);
                            nslog(@"\n numberOfRemainingDays = %d",numberOfRemainingDays);
                            
                            nslog(@"\n numberOfWeeks = %d",numberOfWeeks);
                            
                            for(int i=0;i<numberOfWeeks;i++)
                            {
                                
                                NSDate *dt = [dtFormatter dateFromString:dateString];
                                nslog(@"\n dt = %@",dt);
                                dt = [dt dateByAddingTimeInterval:(86400*7)];
                                
                                nslog(@"\n dt = %@",dt);
                                dateString = [dtFormatter stringFromDate:dt];
                                nslog(@"\n recursive dateString = %@",dateString);
                                
                                
                                [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                                  isrecursive:isRecurringValue
                                                    frequency:frequency
                                                      enddate:endDate
                                                     parentid:parentId
                                     is_receipt_image_choosen:is_receipt_image_choosen
                                           receipt_image_name:appDelegate.receipt_image_name
                                     receipt_small_image_name:appDelegate.receipt_small_image_name
                                 ];
                                
                                
                                
                                
                            }
                            
                            
                        }
                        else if([recurrenceTime isEqualToString:@"Every 2 weeks"])
                        {
                            numberOfTwoWeeks = daysBetweenDates /14;
                            numberOfRemainingDays = daysBetweenDates %14;
                            
                            numberOfTwoWeeks *=-1;
                            nslog(@"\n numberOfWeeks = %d",numberOfTwoWeeks);
                            nslog(@"\n numberOfRemainingDays = %d",numberOfRemainingDays);
                            
                            for(int i=0;i<numberOfTwoWeeks;i++)
                            {
                                
                                NSDate *dt = [dtFormatter dateFromString:dateString];
                                nslog(@"\n dt = %@",dt);
                                dt = [dt dateByAddingTimeInterval:(86400*14)];
                                
                                nslog(@"\n dt = %@",dt);
                                dateString = [dtFormatter stringFromDate:dt];
                                nslog(@"\n recursive dateString = %@",dateString);
                                
                                
                                [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                                  isrecursive:isRecurringValue
                                                    frequency:frequency
                                                      enddate:endDate
                                                     parentid:parentId
                                     is_receipt_image_choosen:is_receipt_image_choosen
                                           receipt_image_name:appDelegate.receipt_image_name
                                     receipt_small_image_name:appDelegate.receipt_small_image_name
                                 ];
                                
                                
                                
                                
                            }
                            
                            
                        }
                        else if([recurrenceTime isEqualToString:@"Monthly"])
                        {
                            
                            unsigned int uintFlags =  NSMonthCalendarUnit | NSDayCalendarUnit;
                            
                            
                            NSDateComponents* differenceComponents = [gregorian components:uintFlags fromDate:date1 toDate:date2 options:0];
                            
                            
                            nslog(@"\n differenceComponents.month  = %d",differenceComponents.month);
                            nslog(@"\n differenceComponents.day  = %d",differenceComponents.day);
                            
                            
                            numberOfMonths = differenceComponents.month;
                            
                            
                            
                            NSDate *dt = [dtFormatter dateFromString:dateString];
                            // start by retrieving day, weekday, month and year components for yourDate
                            
                            //NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                            
                            NSDateComponents *offsetComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:dt];
                            NSInteger theDay = [offsetComponents day];
                            NSInteger theMonth = [offsetComponents month];
                            NSInteger theYear = [offsetComponents year];
                            
                            
                            for(int i=0;i<numberOfMonths;i++)
                            {
                                
                                theMonth = theMonth+1;
                                [offsetComponents setMonth:theMonth];
                                if(theMonth >12)
                                {
                                    //theYear++;
                                }
                                
                                NSDateComponents *components = [[NSDateComponents alloc] init];
                                [components setDay:theDay];
                                [components setMonth:theMonth];
                                [components setYear:theYear];
                                NSDate *thisDate = [gregorian dateFromComponents:components];
                                
                                
                                
                                
                                nslog(@"\n thisDate = %@",thisDate);
                                
                                
                                
                                dateString = [dtFormatter stringFromDate:thisDate];
                                nslog(@"\n recursive dateString = %@",dateString);
                                
                                
                                [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                                  isrecursive:isRecurringValue
                                                    frequency:frequency
                                                      enddate:endDate
                                                     parentid:parentId
                                     is_receipt_image_choosen:is_receipt_image_choosen
                                           receipt_image_name:appDelegate.receipt_image_name
                                     receipt_small_image_name:appDelegate.receipt_small_image_name
                                 ];
                                
                                theMonth = [components month];
                                theYear = [components year];
                                [components release];
                                
                            }
                            
                            //[offsetComponents release];
                            //[gregorian release];
                            
                            
                            
                        }
                        else if([recurrenceTime isEqualToString:@"Quarterly"])
                        {
                            
                            unsigned int uintFlags = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit;
                            
                            
                            NSDateComponents* differenceComponents = [gregorian components:uintFlags fromDate:date1 toDate:date2 options:0];
                            
                            
                            NSDate *dt = [dtFormatter dateFromString:dateString];
                            // start by retrieving day, weekday, month and year components for yourDate
                            
                            //NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                            
                            NSDateComponents *offsetComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:dt];
                            NSInteger theDay = [offsetComponents day];
                            NSInteger theMonth = [offsetComponents month];
                            NSInteger theYear = [offsetComponents year];
                            
                            numberOfQuarters = (int)(differenceComponents.month/3);
                            numberOfQuarters+=(int)((differenceComponents.year*12)/3);
                            nslog(@"\n numberOfQuarters  = %d",numberOfQuarters);
                            
                            
                            for(int i=0;i<numberOfQuarters;i++)
                            {
                                
                                theMonth = theMonth+3;
                                [offsetComponents setMonth:theMonth];
                                if(theMonth >12)
                                {
                                    //theYear++;
                                }
                                
                                NSDateComponents *components = [[NSDateComponents alloc] init];
                                [components setDay:theDay];
                                [components setMonth:theMonth];
                                [components setYear:theYear];
                                NSDate *thisDate = [gregorian dateFromComponents:components];
                                dateString = [dtFormatter stringFromDate:thisDate];
                                nslog(@"\n recursive dateString = %@",dateString);
                                
                                
                                [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                                  isrecursive:isRecurringValue
                                                    frequency:frequency
                                                      enddate:endDate
                                                     parentid:parentId
                                     is_receipt_image_choosen:is_receipt_image_choosen
                                           receipt_image_name:appDelegate.receipt_image_name
                                     receipt_small_image_name:appDelegate.receipt_small_image_name
                                 ];
                                
                                theMonth = [components month];
                                theYear = [components year];
                                [components release];
                                
                            }
                            
                            //[offsetComponents release];
                            //[gregorian release];
                            
                            
                            
                            
                        }
                        else if([recurrenceTime isEqualToString:@"Yearly"])
                        {
                            
                            NSDate *dt = [dtFormatter dateFromString:dateString];
                            unsigned int uintFlags = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit;
                            
                            
                            NSDateComponents* differenceComponents = [gregorian components:uintFlags fromDate:date1 toDate:date2 options:0];
                            
                            
                            //NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                            
                            NSDateComponents *offsetComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:dt];
                            NSInteger theDay = [offsetComponents day];
                            NSInteger theMonth = [offsetComponents month];
                            NSInteger theYear = [offsetComponents year];
                            
                            numberOfYears = differenceComponents.year;
                            
                            nslog(@"\n differenceComponents.year  = %d",differenceComponents.year);
                            nslog(@"\n differenceComponents.month  = %d",differenceComponents.month);
                            nslog(@"\n differenceComponents.day  = %d",differenceComponents.day);
                            nslog(@"\n differenceComponents.week  = %d",differenceComponents.week);
                            
                            for(int i=0;i<numberOfYears;i++)
                            {
                                
                                theYear = theYear+1;
                                [offsetComponents setYear:theYear];
                                if(theMonth >12)
                                {
                                    //theYear++;
                                }
                                
                                NSDateComponents *components = [[NSDateComponents alloc] init];
                                [components setDay:theDay];
                                [components setMonth:theMonth];
                                [components setYear:theYear];
                                NSDate *thisDate = [gregorian dateFromComponents:components];
                                
                                
                                
                                
                                nslog(@"\n thisDate = %@",thisDate);
                                
                                
                                
                                dateString = [dtFormatter stringFromDate:thisDate];
                                nslog(@"\n recursive dateString = %@",dateString);
                                
                                
                                [appDelegate AddIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"] incomeExpense:appDelegate.isIncome
                                                  isrecursive:isRecurringValue
                                                    frequency:frequency
                                                      enddate:endDate
                                                     parentid:parentId
                                     is_receipt_image_choosen:is_receipt_image_choosen
                                           receipt_image_name:appDelegate.receipt_image_name
                                     receipt_small_image_name:appDelegate.receipt_small_image_name
                                 ];
                                
                                theMonth = [components month];
                                theYear = [components year];
                                [components release];
                                
                            }
                            
                            //[offsetComponents release];
                            //[gregorian release];
                            
                            
                            
                        }
                        
                        
                        
                    }
                    
                    
                }
            }
            [gregorian release];
            
            
            
            nslog(@"\n  data dic at saving = %@",dictionary);
            nslog(@"\n  property at saving = %@",appDelegate.incomeProperty);
            nslog(@"\n  incometype dic at saving = %@",appDelegate.incomeString);
            nslog(@"\n old data dic at saving = %@",oldDataDictionary);
            dateString = [dtFormatter stringFromDate:date1];
            
            if(isRecurring == FALSE || isRecurringRecord == FALSE)
            {
                [appDelegate UpdateIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"]
                                     isrecursive:isRecurringValue
                                         enddate:endDate
                                        parentid:[[dictionary objectForKey:@"parentid"]intValue]
                                           rowid:[[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"rowid"]intValue]
                        is_receipt_image_choosen:is_receipt_image_choosen
                              receipt_image_name:appDelegate.receipt_image_name
                        receipt_small_image_name:appDelegate.receipt_small_image_name
                 
                 
                 ];
                
                
                
                
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            else
            {
                if([[oldDataDictionary objectForKey:@"date"] isEqualToString:[dictionary objectForKey:@"date"]] &&
                   [[oldDataDictionary objectForKey:@"amount"] isEqualToString:[dictionary objectForKey:@"amount"]]&&
                   [[oldDataDictionary objectForKey:@"property"] isEqualToString:appDelegate.incomeProperty]&&
                   [[oldDataDictionary objectForKey:@"type"] isEqualToString:appDelegate.incomeString]
                   )
                {
                    nslog(@"\n no change..");
                    
                    nslog(@"\n endDate = %@",endDate);
                    
                    
                    [appDelegate UpdateIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:dateString amount:amountStr notes:[dictionary objectForKey:@"notes"]
                                         isrecursive:isRecurringValue
                                             enddate:endDate
                                            parentid:[[dictionary objectForKey:@"parentid"]intValue]
                                               rowid:[[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"rowid"]intValue]
                            is_receipt_image_choosen:is_receipt_image_choosen
                                  receipt_image_name:appDelegate.receipt_image_name
                            receipt_small_image_name:appDelegate.receipt_small_image_name

                     
                     ];
                    
                    
                    
                    appDelegate.receipt_image_name = @"";
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                    
                }
                else
                {
                    
                    isRecurringValue = @"0";
                    
                    [dictionary setObject:dateString forKey:@"dateString"];
                    [dictionary setObject:amountStr forKey:@"amountStr"];
                    [dictionary setObject:isRecurringValue forKey:@"isRecurringValue"];
                    [dictionary setObject:@"0" forKey:@"parentid"];
                    [dictionary setObject:endDate forKey:@"endDate"];
                    
                    UIAlertView*recurringAlertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please note that changes made to recurring entry will apply to current entry only. Do you wish to proceed?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                    recurringAlertView.tag = 999;
                    [recurringAlertView show];
                    [recurringAlertView release];
                    
                }
                
            }
            
            
            [dtFormatter release];
        }
        
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==999)
    {
        if(buttonIndex == 1)
        {
            isRecurring = FALSE;
            
            
            int is_receipt_image_choosen =1;
            if(appDelegate.is_receipt_image_choosen==FALSE)
            {
                
                appDelegate.receipt_small_image_name = @"";
                appDelegate.receipt_image_name = @"";
                is_receipt_image_choosen = 0;
                
            }

            
            
            [appDelegate UpdateIncomeExpense:appDelegate.incomeProperty type:appDelegate.incomeString date:[dictionary objectForKey:@"dateString"] amount:[dictionary objectForKey:@"amountStr"] notes:[dictionary objectForKey:@"notes"]
                                 isrecursive:[dictionary objectForKey:@"isRecurringValue"]
                                     enddate:[dictionary objectForKey:@"endDate"]
                                    parentid:[[dictionary objectForKey:@"parentid"]intValue]
                                       rowid:[[[appDelegate.incomeExpenseArray objectAtIndex:appDelegate.incomeExpenseIndex]valueForKey:@"rowid"]intValue]
                    is_receipt_image_choosen:is_receipt_image_choosen
                          receipt_image_name:appDelegate.receipt_image_name
                    receipt_small_image_name:appDelegate.receipt_small_image_name

             
             ];
            
            
            
            
            appDelegate.receipt_image_name = @"";
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(void)pickerview_done_clicked
{
    
    nslog(@"\n pickerView.selectedRowInComponent = %d",[pickerView selectedRowInComponent:0]);
    
  
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }

    
    
    [dictionary setObject:[pickerArray objectAtIndex:[pickerView selectedRowInComponent:0]] forKey:@"frequency"];
    recurrenceTime = [pickerArray objectAtIndex:[pickerView selectedRowInComponent:0]];
    
    
    if([[dictionary objectForKey:@"frequency"] isEqualToString:@"Every 2 weeks"])
    {
        frequencyButton.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    }
    else
    {
        frequencyButton.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    }
    
    
    
    [frequencyButton setTitle:[NSString stringWithFormat:@" %@",[dictionary objectForKey:@"frequency"]]
                     forState:UIControlStateNormal];
    
    
    
    pickerView.hidden = YES;
    datePicker.hidden = YES;
    toolBar.hidden = YES;
    
}

-(void)done_clicked
{
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
         [tblView setContentOffset:CGPointMake(0,0)];
    }
    
   
    //[tblView setContentOffset:CGPointMake(0,0)];
    
    
    
    
    /*
     UITextField *textField = (UITextField *)[self.view viewWithTag:1003];
     
     if([textField isFirstResponder])
     {
     [textField resignFirstResponder];
     }
     
     
     
     
     UIPlaceHolderTextView *textView= (UIPlaceHolderTextView *)[self.view viewWithTag:2004];
     if([textView isFirstResponder])
     {
     [textView resignFirstResponder];
     }
     */
    
    
    [self.view endEditing:YES];
    
    pickerView.hidden = YES;
    datePicker.hidden = YES;
    toolBar.hidden = YES;
    
    
}


#pragma mark -
#pragma mark dateBtn_clicked
#pragma mark called when date button clicked

-(IBAction)dateBtn_clicked:(id)sender
{
    [self done_clicked];
    UIButton *btn = (UIButton *)sender;
    
    if(appDelegate.isIphone5)
    {
         toolBar.frame= CGRectMake(toolBar.frame.origin.x,200,toolBar.frame.size.width, toolBar.frame.size.height);
    }
    
    if(btn.tag == 100)
    {
        isrecurringDate = NO;
        datePicker.minimumDate = nil;
        
        if ([[dictionary objectForKey:@"date"] length]>0)
        {
            datePicker.date = [appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]];
        }
        
        
    }
    else if(btn.tag == 101)
    {
        isrecurringDate = YES;
        
        if(!appDelegate.isIPad)
        {
            tblView.frame = CGRectMake(0,tblView.frame.origin.y , tblView.frame.size.width,180.0f);
            tblView.contentOffset = CGPointMake(0,180);
        }
        
        
        //UIButton*incomeStartDate = (UIButton*)[self.view viewWithTag:100];
        
        datePicker.minimumDate = [appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]];
        
        
        if ([[dictionary objectForKey:@"enddate"] length]>0)
        {
            datePicker.date = [appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"enddate"]];
        }
        
        
    }
    
    datePicker.hidden = NO;
    toolBar.hidden = NO;
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    [datePicker addTarget:self  action:@selector(datePicker_change)  forControlEvents:UIControlEventValueChanged];
    
    
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(done_clicked)];
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flextSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done_clicked)];
    */
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flextSpace,doneButton, nil]];
    toolBar.tag = 50;
    toolBar.barStyle = UIBarStyleBlack;
    
    [cancelBtn release];
    [flextSpace release];
    [doneButton release];
    
    
    if(appDelegate.isIPad)
    {
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd"];
        
        NSDate* date = nil;
        if ([[dictionary objectForKey:@"date"] length]>0)
        {
            //date = [df dateFromString:[dictionary objectForKey:@"date"]];
            /**
            date = [appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]];
             */
        }
        
        
        
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
			pickerView.frame =  CGRectMake(0,715, 768,216 );
            datePicker.frame =  CGRectMake(0,715, 768,216 );
            toolBar.frame = CGRectMake(0, 671,768, 44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            datePicker.frame =  CGRectMake(0,513,1024,116 );
            pickerView.frame =  CGRectMake(0,513,1024,116 );
            toolBar.frame = CGRectMake(0,469,1024, 44);
        }
        
        [df release];
        
    }
    
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    [df release];
    
    /*
    [btn setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
    */
    
    if(btn.tag == 100)
    {
       // [dictionary setObject:btn.titleLabel.text forKey:@"date"];
        [dictionary setObject:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forKey:@"date"];
    }
    else if(btn.tag == 101)
    {
        
        
        [btn setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
        
        if(recurringDateButton.titleLabel.text != nil)
        {
            [dictionary setObject:recurringDateButton.titleLabel.text forKey:@"enddate"];
        }
        
    }
    
    
    
}


#pragma mark -
#pragma mark dateWasSelected
#pragma mark dont know when called.. :-)

- (void)dateWasSelected:(NSDate *)selectedDate:(id)element
{
	//Date selection was made
	
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    [dictionary setObject:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forKey:@"date"];
    [df release];
    
}


#pragma mark -
#pragma mark datePicker_change
#pragma mark called when date changed..
-(void)datePicker_change
{
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    
	//df.dateStyle = NSDateFormatterMediumStyle;
    UIButton *btn;
    if(isrecurringDate)
    {
        btn = (UIButton *)[self.view viewWithTag:101];
        [btn setTitle:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
        
    }
    else
    {
        /**
        btn = (UIButton *)[self.view viewWithTag:100];
         */
    }
    
    
    
    
    /*
     [btn setTitle:[NSString stringWithFormat:@"  %@",[df stringFromDate:datePicker.date]] forState:UIControlStateNormal];
     
    
    
    */
    if(!isrecurringDate)
    {
        [dictionary setObject:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forKey:@"date"];
        
        
        NSDateFormatter*formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd"];
        
        
        icome_expense_date_label.text = [formatter stringFromDate:[appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]]];
        
        
        [formatter setDateFormat:@"MMM"];
        icome_expense_month_label.text = [formatter stringFromDate:[appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]]];
        
        
        [formatter release];
        
    }
    else
    {
        
        
        [dictionary setObject:[NSString stringWithFormat:@" %@",[appDelegate.regionDateFormatter stringFromDate:datePicker.date]] forKey:@"enddate"];
        
        
        NSDate* date1 = [appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]];
        NSDate* date2 = [appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"enddate"]];
        
        /*
         NSDate* date1 = [appDelegate.regionDateFormatter dateFromString:incomeStartDate.titleLabel.text];
         NSDate* date2 = [appDelegate.regionDateFormatter dateFromString:recurringEndDate.titleLabel.text];
         */
        
        
        
        NSTimeInterval distanceBetweenDates = [date1 timeIntervalSinceDate:date2];
        double secondsInADay = 86400;
        NSInteger daysBetweenDates = distanceBetweenDates / secondsInADay;
        nslog(@"\n daysBetweenDates = %d",daysBetweenDates);
        
        
        
        NSCalendar* gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        
        
        if([recurrenceTime isEqualToString:@"Weekly"])
        {
            numberOfWeeks = daysBetweenDates /7;
            numberOfRemainingDays = daysBetweenDates %7;
            numberOfWeeks *=-1;
            nslog(@"\n numberOfWeeks = %d",numberOfWeeks);
            nslog(@"\n numberOfRemainingDays = %d",numberOfRemainingDays);
            
        }
        else if([recurrenceTime isEqualToString:@"Every 2 weeks"])
        {
            numberOfTwoWeeks = daysBetweenDates /14;
            numberOfRemainingDays = daysBetweenDates %14;
            
            numberOfTwoWeeks *=-1;
            nslog(@"\n numberOfTwoWeeks = %d",numberOfTwoWeeks);
            nslog(@"\n numberOfRemainingDays = %d",numberOfRemainingDays);
            
            
            
        }
        else if([recurrenceTime isEqualToString:@"Monthly"])
        {
            
            unsigned int uintFlags =  NSMonthCalendarUnit | NSDayCalendarUnit;
            
            
            NSDateComponents* differenceComponents = [gregorian components:uintFlags fromDate:date1 toDate:date2 options:0];
            
            
            nslog(@"\n differenceComponents.month  = %d",differenceComponents.month);
            nslog(@"\n differenceComponents.day  = %d",differenceComponents.day);
            numberOfMonths = differenceComponents.month;
            
            
        }
        else if([recurrenceTime isEqualToString:@"Quarterly"])
        {
            unsigned int uintFlags = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit;
            
            
            NSDateComponents* differenceComponents = [gregorian components:uintFlags fromDate:date1 toDate:date2 options:0];
            
            numberOfQuarters = (int)(differenceComponents.month/3);
            numberOfQuarters+=(int)((differenceComponents.year*12)/3);
            nslog(@"\n numberOfQuarters  = %d",numberOfQuarters);
            
        }
        else if([recurrenceTime isEqualToString:@"Yearly"])
        {
            unsigned int uintFlags = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit;
            
            NSDateComponents* differenceComponents = [gregorian components:uintFlags fromDate:date1 toDate:date2 options:0];
            
            numberOfYears = differenceComponents.year;
            
            /*
            nslog(@"\n differenceComponents.year  = %d",differenceComponents.year);
            nslog(@"\n differenceComponents.month  = %d",differenceComponents.month);
            nslog(@"\n differenceComponents.day  = %d",differenceComponents.day);
            nslog(@"\n differenceComponents.week  = %d",differenceComponents.week);
             */
            
        }
        
        [gregorian release];
        
    }
    
    
    
    
    [df release];
    
}

#pragma mark - countRecurrenceTime

-(void)countRecurrenceTime
{
    
    NSDate* date1 = [appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"date"]];
    NSDate* date2 = [appDelegate.regionDateFormatter dateFromString:[dictionary objectForKey:@"enddate"]];
    
    /*
     NSDate* date1 = [appDelegate.regionDateFormatter dateFromString:incomeStartDate.titleLabel.text];
     NSDate* date2 = [appDelegate.regionDateFormatter dateFromString:recurringEndDate.titleLabel.text];
     */
    
    
    
    NSTimeInterval distanceBetweenDates = [date1 timeIntervalSinceDate:date2];
    double secondsInADay = 86400;
    NSInteger daysBetweenDates = distanceBetweenDates / secondsInADay;
    nslog(@"\n daysBetweenDates = %d",daysBetweenDates);
    
    
    
    NSCalendar* gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    
    
    if([recurrenceTime isEqualToString:@"Weekly"])
    {
        numberOfWeeks = daysBetweenDates /7;
        numberOfRemainingDays = daysBetweenDates %7;
        numberOfWeeks *=-1;
        nslog(@"\n numberOfWeeks = %d",numberOfWeeks);
        nslog(@"\n numberOfRemainingDays = %d",numberOfRemainingDays);
        
    }
    else if([recurrenceTime isEqualToString:@"Every 2 weeks"])
    {
        numberOfTwoWeeks = daysBetweenDates /14;
        numberOfRemainingDays = daysBetweenDates %14;
        
        numberOfTwoWeeks *=-1;
        nslog(@"\n numberOfTwoWeeks = %d",numberOfTwoWeeks);
        nslog(@"\n numberOfRemainingDays = %d",numberOfRemainingDays);
        
        
        
    }
    else if([recurrenceTime isEqualToString:@"Monthly"])
    {
        
        unsigned int uintFlags =  NSMonthCalendarUnit | NSDayCalendarUnit;
        
        
        NSDateComponents* differenceComponents = [gregorian components:uintFlags fromDate:date1 toDate:date2 options:0];
        
        
        nslog(@"\n differenceComponents.month  = %d",differenceComponents.month);
        nslog(@"\n differenceComponents.day  = %d",differenceComponents.day);
        numberOfMonths = differenceComponents.month;
        
        
    }
    else if([recurrenceTime isEqualToString:@"Quarterly"])
    {
        unsigned int uintFlags = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit;
        
        
        NSDateComponents* differenceComponents = [gregorian components:uintFlags fromDate:date1 toDate:date2 options:0];
        
        numberOfQuarters = (int)(differenceComponents.month/3);
        numberOfQuarters+=(int)((differenceComponents.year*12)/3);
        nslog(@"\n numberOfQuarters  = %d",numberOfQuarters);
        
    }
    else if([recurrenceTime isEqualToString:@"Yearly"])
    {
        unsigned int uintFlags = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit;
        
        NSDateComponents* differenceComponents = [gregorian components:uintFlags fromDate:date1 toDate:date2 options:0];
        
        numberOfYears = differenceComponents.year;
        
        nslog(@"\n differenceComponents.year  = %d",differenceComponents.year);
        nslog(@"\n differenceComponents.month  = %d",differenceComponents.month);
        nslog(@"\n differenceComponents.day  = %d",differenceComponents.day);
        nslog(@"\n differenceComponents.week  = %d",differenceComponents.week);
        
    }
    
    [gregorian release];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section==0)
    {
        return 30;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return 0;
    }
    else if(section==1)
    {
        
        if(appDelegate.isIPad)
        {
            return 90;
        }
        return 60;
    }
    else if(section==2)
    {
        return 20;
    }
    
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if(section==1)
    {
        
        if(appDelegate.isMilege)
        {
           // [icome_expense_amount_text setText:[dictionary objectForKey:@"amount"]];
            
            [icome_expense_amount_text setText: [NSString stringWithFormat:@"%@ %@",[dictionary objectForKey:@"amount"],[appDelegate.prefs objectForKey:@"milage_unit"]]];
            
            
        }
        else
        {
            [icome_expense_amount_text setText:[numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[dictionary objectForKey:@"amount"]doubleValue]]]];
            
        }
        
        income_expense_receipt_button.frame = CGRectMake(274,8,45,43);
        
        if(appDelegate.isIPad)
        {
            section_date_button.frame = CGRectMake(0, 0, 113,88);
            icome_expense_date_label.frame = CGRectMake(0,0,114, 48);
            
            icome_expense_month_label.frame = CGRectMake(0,51,114, 38);
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                icome_expense_amount_text.frame = CGRectMake(115,0,546, 90);

                
               
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                icome_expense_amount_text.frame = CGRectMake(115,0,800 ,90);
            }
            
            
        }

        
        
        if(appDelegate.is_receipt_image_choosen == TRUE)
        {
            
            
            
            if(appDelegate.isIPad)
            {
            
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    income_expense_receipt_button.frame = CGRectMake(684,17,60 ,60);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    income_expense_receipt_button.frame = CGRectMake(934,17,60 ,60);
                }

                
            }
            else
            {
               income_expense_receipt_button.frame = CGRectMake(274,8,45,43); 
            }
            
            
            if([[dictionary objectForKey:@"is_receipt_image_choosen"] intValue]==1 || appDelegate.is_receipt_image_choosen== TRUE)
            {
                
                
                
                nslog(@"\n appDelegate.receipt_image = %@ \n",NSStringFromCGSize(appDelegate.receipt_image.size) );
                
                
                
                if(appDelegate.receipt_image == nil)
                {
                    [income_expense_receipt_button setImage:[appDelegate getImage:[dictionary objectForKey:@"receipt_small_image_name"]] forState:UIControlStateNormal];
                }
                else if (appDelegate.receipt_image.size.width == 0)
                {
                    [income_expense_receipt_button setImage:[UIImage imageNamed:@"receipt_no_attachment.png"] forState:UIControlStateNormal];
                }
                else
                {
                    [income_expense_receipt_button setImage:appDelegate.receipt_image forState:UIControlStateNormal];
                }
                
            }
        }
        else
        {
         
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    income_expense_receipt_button.frame = CGRectMake(690,25,45 ,43);
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    income_expense_receipt_button.frame = CGRectMake(940,25,45 ,43);
                }
            }
            else
            {

            
                income_expense_receipt_button.frame = CGRectMake(281,10,28,36);
            
            }
            
            
            [income_expense_receipt_button setImage:[UIImage imageNamed:@"receipt_no_attachment.png"] forState:UIControlStateNormal];
        }
        
       
         
        
        
        if(appDelegate.isIPad)
        {
            
            
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                section_header_view.frame = CGRectMake(0, 0,768 ,90);
                income_expense_section_header_bg.frame = CGRectMake(0, 0,768 ,90);
               
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                section_header_view.frame = CGRectMake(0, 0,1024 ,90);
                income_expense_section_header_bg.frame = CGRectMake(0, 0,1024 ,90);
                
            }

        }
        
        
        if(appDelegate.isIncome)
        {
            
            if(appDelegate.isIPad)
            {

                
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    
                    
                    
                    income_expense_section_header_bg.image = [UIImage imageNamed:@"ipad_port_image_temp_cell_bg_green.png"];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    income_expense_section_header_bg.image = [UIImage imageNamed:@"ipad_land_image_temp_cell_bg_green.png"];
                                   
                }
                
            }
            else
            {
                    income_expense_section_header_bg.image = [UIImage imageNamed:@"image_temp_cell_bg_green.png"];
            }
            
            
        }
        else
        {
            
            if(appDelegate.isIPad)
            {
                if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                {
                    income_expense_section_header_bg.image = [UIImage imageNamed:@"ipad_port_image_temp_cell_bg_red.png"];
                }
                else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                {
                    income_expense_section_header_bg.image = [UIImage imageNamed:@"ipad_land_image_temp_cell_bg_red.png"];
                }
            }
            else
            {
                    income_expense_section_header_bg.image = [UIImage imageNamed:@"image_temp_cell_bg_red.png"];
            }
            
            
        }
        
        
        return section_header_view;
        
        
        
    }
    
    return nil;
    
}


#pragma mark -
#pragma mark Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 3)
    {
        //return @"Notes";
    }
    return @"";
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 1)
    {
        return 50;
    }
    
    
    
    else if (indexPath.section == 2)
    {
        
        if(appDelegate.isMilege)
        {
            return 84; 
        }
        else
        {
            if(!isRecurring)
            {
                if(indexPath.row==1 || indexPath.row==2)
                {
                    return 0;
                }
                
            }
            
            
            
            if(indexPath.row==3)
            {
                return 84; 
            }

        }
            
        
        
               
    }
    
	return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    //return 4;
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0)
    {
        
        
        return [mainArray count];
        
        
    }
    else if (section == 1)
    {
        return 0;
    }
    else if(section ==2)
    {
        
        if(appDelegate.isMilege)
        {
            return 1;
        }
        
        
        return [recurringArray count];
        
        /*
        if(isRecurring == YES)
        {
            return [recurringArray count];
        }
        else
        {
            return [recurringArray count]-2;
        }
         */
    }
    else if(section==3)
    {
        return 1;
    }
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    UIButton *dateButton;
    
    UILabel *label, *detailLabel;
    UITextField *amountTextField;
    
    NSNumberFormatter *numberFormatterTemp = [[[NSNumberFormatter alloc] init] autorelease] ;
    [numberFormatterTemp setNumberStyle: NSNumberFormatterCurrencyStyle];
    
    if (indexPath.section == 0)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (1)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
            
            
            
            
            label = [[UILabel alloc]initWithFrame:CGRectMake(40, 7, 110, 30)];
            label.tag = 400;
            
            label.numberOfLines = 0;
            label.lineBreakMode = UILineBreakModeWordWrap;
            [label setFont:[UIFont systemFontOfSize:13.0]];
            [label setBackgroundColor:[UIColor clearColor]];
            
            [cell.contentView addSubview:label];
            
            
            
            detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(130, 7, 140, 30)];
            //detailLabel.backgroundColor = [UIColor redColor];
            
            
            if(appDelegate.isIPad)
            {
                if(appDelegate.isIOS7)
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                    {
                        detailLabel.frame = CGRectMake(130, 7, 600, 30);
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                    {
                        detailLabel.frame = CGRectMake(130, 7, 850, 30);
                    }
                }
                else
                {
                    if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                    {
                        detailLabel.frame = CGRectMake(130, 7, 500, 30);
                    }
                    else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                    {
                        detailLabel.frame = CGRectMake(130, 7, 750, 30);
                    }
                }
            }
            else
            {
                detailLabel.frame = CGRectMake(130, 7, 140, 30);
            }
            
            
            
            
            detailLabel.tag = 500;
            detailLabel.textAlignment = UITextAlignmentRight;
            detailLabel.numberOfLines = 0;
            detailLabel.lineBreakMode = UILineBreakModeTailTruncation;
            [detailLabel setFont:[UIFont systemFontOfSize:13.0]];
            [detailLabel setBackgroundColor:[UIColor clearColor]];
            
            [cell.contentView addSubview:detailLabel];
            
            
            
        }
        
        
        detailLabel.hidden = FALSE;
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        label.text = [mainArray objectAtIndex:indexPath.row];
        
        if (indexPath.row == 0)
        {
            //cell.imageView.image = [UIImage imageNamed:@"property_icon.png"];
            
            UIImageView*property_imageView = [[UIImageView alloc] init];
            property_imageView.frame = CGRectMake(0, 0,40,40);
            property_imageView.image = [UIImage imageNamed:@"property_icon.png"];
            [cell.contentView addSubview:property_imageView];
            [property_imageView release];

            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            detailLabel.text = appDelegate.incomeProperty;
            
        }
        else if (indexPath.row == 1)
        {
            
            
            if(appDelegate.isMilege)
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            
            
            
            
           
            
            if (appDelegate.isIncome)
            {
                detailLabel.text = appDelegate.incomeString;
                
                //cell.imageView.image = [UIImage imageNamed:@"income_type_icon.png"];
                
                
                UIImageView*income_type_imageView = [[UIImageView alloc] init];
                income_type_imageView.frame = CGRectMake(0, 0,40,40);
                income_type_imageView.image = [UIImage imageNamed:@"income_type_icon.png"];
                [cell.contentView addSubview:income_type_imageView];
                [income_type_imageView release];

                
                
            }
            else
            {
                detailLabel.text = appDelegate.incomeString;
                
                UIImageView*exp_type_imageView = [[UIImageView alloc] init];
                exp_type_imageView.frame = CGRectMake(0, 0,40,40);
                exp_type_imageView.image = [UIImage imageNamed:@"exp_type_icon.png"];
                [cell.contentView addSubview:exp_type_imageView];
                [exp_type_imageView release];
                
                //cell.imageView.image = [UIImage imageNamed:@"exp_type_icon.png"];
            }
        }
        
        
        
        
        
        [label release];
        [detailLabel release];
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    
    else if(indexPath.section==1)
    {
        
        
        //nothing.. :-)
        
        
        
    }
    else if(indexPath.section ==2)//Recurring
    {
        
        
        
        
        
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (1)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            label = [[UILabel alloc]initWithFrame:CGRectMake(40, 7, 110, 30)];
            
            label.tag = 400;
            
            label.numberOfLines = 0;
            label.lineBreakMode = UILineBreakModeWordWrap;
            [label setFont:[UIFont systemFontOfSize:13.0]];
            [label setBackgroundColor:[UIColor clearColor]];
            
            [cell.contentView addSubview:label];
        }
        
        
        label.text = [recurringArray objectAtIndex:indexPath.row];
                
        
        if(appDelegate.isMilege)
        {
            label.hidden = TRUE;
            
            placeHolderTextView.frame = CGRectMake(2,3, 280, 70);
            placeHolderTextView.placeholder = @"Receipt number, Reference number, comments etc";
            placeHolderTextView.tag = 2004;
            placeHolderTextView.delegate = self;
            placeHolderTextView.editable = YES;
            [placeHolderTextView setFont:[UIFont systemFontOfSize:13.0]];
            placeHolderTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            placeHolderTextView.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:placeHolderTextView];
            
            if([[dictionary objectForKey:@"notes"] length]>0)
            {
                [placeHolderTextView setText:[dictionary objectForKey:@"notes"]];
            }
        }
        else/*
                SUN:004 IF INCOME OR EXPENSE.
             */
        {
            if(indexPath.row == 0)
            {
                UISwitch *recurringSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(194, 8, 94, 27)];
                recurringSwitch.onTintColor = [UIColor colorWithRed:10.0f/255.0f green:142.0f/255.0f blue:184.0f/255.0f alpha:1.0];
                if(isIOS5)
                {
                    if(appDelegate.isIPad)
                    {
                        
                        if(appDelegate.isIOS7)
                        {
                            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                            {
                                recurringSwitch.frame = CGRectMake(687, 8, 94, 27);
                            }
                            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                            {
                                recurringSwitch.frame = CGRectMake(932, 8, 94, 27);
                            }
                        }
                        else
                        {
                            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                            {
                                recurringSwitch.frame = CGRectMake(587, 8, 94, 27);
                            }
                            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                            {
                                recurringSwitch.frame = CGRectMake(802, 8, 94, 27);
                            }
                        }
                        
                        
                        
                    }
                    else
                    {
                        recurringSwitch.frame = CGRectMake(212, 8, 94, 27);
                        if(appDelegate.isIOS7)
                        {
                            recurringSwitch.frame = CGRectMake(254, 8, 94, 27);
                        }
                        
                    }
                    
                }
                else
                {
                    if(appDelegate.isIPad)
                    {
                        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2)
                        {
                            recurringSwitch.frame = CGRectMake(571, 8, 94, 27);
                        }
                        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                        {
                            recurringSwitch.frame = CGRectMake(786, 8, 94, 27);
                        }
                        
                    }
                }
                
                [recurringSwitch addTarget:self action:@selector(recurringSwitch_chaged:) forControlEvents:UIControlEventValueChanged];
                [cell.contentView addSubview:recurringSwitch];
                
                
                
                if(isRecurring)
                {
                    [recurringSwitch setOn:YES];
                }
                else
                {
                    [recurringSwitch setOn:NO animated:YES];
                    
                }
                
                
                if (appDelegate.isIncomeExpenseUpdate)
                {
                    if(isRecurring && isRecurringRecord)
                    {
                        recurringSwitch.enabled = FALSE;
                    }
                    
                }
                
                
                //cell.imageView.image = [UIImage imageNamed:@"add_edit_income_expense_recurring_icon.png"];
                
                
                UIImageView*add_edit_income_expense_recurring_imageView = [[UIImageView alloc] init];
                add_edit_income_expense_recurring_imageView.frame = CGRectMake(0, 0,40,40);
                add_edit_income_expense_recurring_imageView.image = [UIImage imageNamed:@"add_edit_income_expense_recurring_icon.png"];
                [cell.contentView addSubview:add_edit_income_expense_recurring_imageView];
                [add_edit_income_expense_recurring_imageView release];

                
                
                [recurringSwitch release];
                
                
            }
            else if(indexPath.row == 1)
            {
                frequencyButton = [UIButton buttonWithType:UIButtonTypeCustom];
                
                frequencyButton.frame = CGRectMake(150, 8, 120, 30);
                frequencyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                if(appDelegate.isIPad)
                {
                    
                    if(appDelegate.isIOS7)
                    {
                        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                        {
                            frequencyButton.frame = CGRectMake(590, 7, 140, 30);
                        }
                        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                        {
                            frequencyButton.frame = CGRectMake(840, 7, 140, 30);
                        }
                    }
                    else
                    {
                        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                        {
                            frequencyButton.frame = CGRectMake(490, 7, 140, 30);
                        }
                        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                        {
                            frequencyButton.frame = CGRectMake(740, 7, 140, 30);
                        }
                    }
                        
                    
                }
                
                
                
                
                // [frequencyButton setBackgroundImage:[UIImage imageNamed:@"dropDownBlue.png"] forState:UIControlStateNormal];
                
                recurrenceTime = [dictionary objectForKey:@"frequency"];
                [frequencyButton setTitle:[NSString stringWithFormat:@"%@",[dictionary objectForKey:@"frequency"]]
                                 forState:UIControlStateNormal];
                [frequencyButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [frequencyButton addTarget:self action:@selector(frequency_clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                if([[dictionary objectForKey:@"frequency"] isEqualToString:@"Every 2 weeks"])
                {
                    frequencyButton.titleLabel.font = [UIFont systemFontOfSize:13.0f];
                }
                else
                {
                    frequencyButton.titleLabel.font = [UIFont systemFontOfSize:13.0f];
                }
                
                
                if (appDelegate.isIncomeExpenseUpdate)
                {
                    if(isRecurring && isRecurringRecord)
                    {
                        frequencyButton.enabled = FALSE;
                    }
                    
                    
                    
                }
                
                
               // cell.imageView.image = [UIImage imageNamed:@"add_edit_income_expense_frequency_icon.png"];
                
                
                if(isRecurring)
                {
                    UIImageView*filter_end_date_imageView = [[UIImageView alloc] init];
                    filter_end_date_imageView.frame = CGRectMake(0, 0,40,40);
                    filter_end_date_imageView.image = [UIImage imageNamed:@"add_edit_income_expense_frequency_icon.png"];
                    [cell.contentView addSubview:filter_end_date_imageView];
                    [filter_end_date_imageView release];
                }

                
               
                
                
                
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell.contentView addSubview:frequencyButton];
                
                
            }
            else if(indexPath.row == 2)
            {
                
                
                
                recurringDateButton = [[UIButton buttonWithType:UIButtonTypeCustom]retain];
                recurringDateButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
                //[recurringDateButton setBackgroundImage:[UIImage imageNamed:@"dropDownBlue.png"] forState:UIControlStateNormal];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [recurringDateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                recurringDateButton.tag = 101;
                
                
                
                if(appDelegate.isIPad)
                {
                    
                    if(appDelegate.isIOS7)
                    {
                        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                        {
                            
                            recurringDateButton.frame = CGRectMake(620, 7, 140, 30);
                        }
                        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                        {
                            
                            recurringDateButton.frame = CGRectMake(870, 7, 140, 30);
                        }
                    }
                    else
                    {
                        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
                        {
                            
                            recurringDateButton.frame = CGRectMake(520, 7, 140, 30);
                        }
                        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
                        {
                            
                            recurringDateButton.frame = CGRectMake(770, 7, 140, 30);
                        }
                    }
                    
                    
                }
                else
                {
                    
                    recurringDateButton.frame = CGRectMake(150, 7, 120, 30);
                    recurringDateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                    
                    
                }
                [recurringDateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                
                
                
                
                [cell.contentView addSubview:recurringDateButton];
                
                [recurringDateButton setTitle:[NSString stringWithFormat:@" %@",[dictionary objectForKey:@"enddate"]]
                                     forState:UIControlStateNormal];
                [recurringDateButton addTarget:self action:@selector(dateBtn_clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                if (appDelegate.isIncomeExpenseUpdate)
                {
                    if(isRecurring && isRecurringRecord)
                    {
                        recurringDateButton.enabled = FALSE;
                    }
                    
                    
                }
                
                
                if(isRecurring)
                {
                    UIImageView*filter_end_date_imageView = [[UIImageView alloc] init];
                    filter_end_date_imageView.frame = CGRectMake(0, 0,40,40);
                    filter_end_date_imageView.image = [UIImage imageNamed:@"filter_end_date.png"];
                    [cell.contentView addSubview:filter_end_date_imageView];
                    [filter_end_date_imageView release];
                }
                
                
                
                //cell.imageView.image = [UIImage imageNamed:@"filter_end_date.png"];
                
            }
            else if(indexPath.row == 3)
            {
                label.hidden = TRUE;
                
                placeHolderTextView.frame = CGRectMake(2,3, 280, 70);
                placeHolderTextView.placeholder = @"Receipt number, Reference number, comments etc";
                placeHolderTextView.tag = 2004;
                placeHolderTextView.delegate = self;
                placeHolderTextView.editable = YES;
                [placeHolderTextView setFont:[UIFont systemFontOfSize:13.0]];
                placeHolderTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
                placeHolderTextView.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:placeHolderTextView];
                
                if([[dictionary objectForKey:@"notes"] length]>0)
                {
                    [placeHolderTextView setText:[dictionary objectForKey:@"notes"]];
                }
                
                
                
                
            }
            
            
            if(!isRecurring)
            {
                if(indexPath.row==1)
                {
                    frequencyButton.hidden = TRUE;
                }
                else if(indexPath.row ==2)
                {
                    recurringDateButton.hidden = TRUE;
                }
                
                
                if(indexPath.row==1||indexPath.row==2)
                {
                    label.hidden = TRUE;
                }
                
            }

        }
        
        
               

        
        
        
        [label release];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
    }
    else if (indexPath.section == 3)
    {
        static NSString *CellIdentifier1 = @"Cell1";
        UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        if (1)
        {
            cell1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier1] autorelease];
            
            
            placeHolderTextView.frame = CGRectMake(10, 7, 280, 70);
            placeHolderTextView.placeholder = @"Receipt number, Reference number, comments etc";
            placeHolderTextView.tag = 2004;
            placeHolderTextView.delegate = self;
            placeHolderTextView.editable = YES;
            [placeHolderTextView setFont:[UIFont systemFontOfSize:13.0]];
            placeHolderTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            placeHolderTextView.backgroundColor = [UIColor clearColor];
            [cell1.contentView addSubview:placeHolderTextView];
            
            
        }
        
        if([[dictionary objectForKey:@"notes"] length]>0)
        {
            [placeHolderTextView setText:[dictionary objectForKey:@"notes"]];
        }
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell1;
        
    }
    
    return nil;
}


#pragma mark -
#pragma mark frequency_clicked



-(void)frequency_clicked:(id)sender
{
    [self.view endEditing:YES];
    pickerView.hidden = NO;
    toolBar.hidden = NO;
    
    
    if(appDelegate.isIphone5)
    {
        toolBar.frame= CGRectMake(toolBar.frame.origin.x,200,toolBar.frame.size.width, toolBar.frame.size.height);
    }
    
    if(!appDelegate.isIPad)
    {
        tblView.contentOffset = CGPointMake(0,180);
        tblView.frame = CGRectMake(0,tblView.frame.origin.y , tblView.frame.size.width,180.0f);
    }
    
    
    
    
    
    if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
        {
			pickerView.frame =  CGRectMake(0,715, 768,216 );
            datePicker.frame =  CGRectMake(0,715, 768,216 );
            toolBar.frame = CGRectMake(0, 671,768, 44);
        }
        else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
        {
            datePicker.frame =  CGRectMake(0,513,1024,116 );
            pickerView.frame =  CGRectMake(0,513,1024,116 );
            toolBar.frame = CGRectMake(0,469,1024, 44);
        }
        
    }
    
    
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(pickerview_done_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flextSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(pickerview_done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerview_done_clicked)];
    */
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flextSpace,doneButton, nil]];
    toolBar.tag = 50;
    toolBar.barStyle = UIBarStyleBlack;
    
    [cancelBtn release];
    [flextSpace release];
    [doneButton release];
    
    
    
}

#pragma mark -
#pragma mark recurringSwitch_chaged

-(void)recurringSwitch_chaged:(id)sender
{
    UISwitch *tempSwitch = (UISwitch*)sender;
    if(tempSwitch.isOn)
    {
        nslog(@"\n switch is on");
        toolBar.hidden = YES;
        datePicker.hidden = YES;
        isRecurring = YES;
        [tblView reloadData];
    }
    else
    {
        nslog(@"\n switch is off");
        toolBar.hidden = YES;
        pickerView.hidden = YES;
        datePicker.hidden = YES;
        
        isRecurring = NO;
        [tblView reloadData];
    }
}




#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    appDelegate.isFromIncomeExpenseView = -1;
    
    pickerView.hidden = TRUE;
    datePicker.hidden = TRUE;
    toolBar.hidden = TRUE;
    [self.view endEditing:YES];
    
    if (indexPath.row == 0 && indexPath.section == 0)
    {
        propertyViewController *propertyView = [[propertyViewController alloc]initWithNibName:@"propertyViewController" bundle:nil];
        [self.navigationController pushViewController:propertyView animated:YES];
        [propertyView release];
    }
    else if (indexPath.row == 1 && indexPath.section == 0 && !appDelegate.isMilege)
    {
        if (appDelegate.isIncome)
        {
            AddIncomeTypeViewController *addIncome = [[AddIncomeTypeViewController alloc]initWithNibName:@"AddIncomeTypeViewController" bundle:nil];
            [self.navigationController pushViewController:addIncome animated:YES];
            [addIncome release];
        }
        else
        {
            
            AddExpenseTypeViewController *addExpense = [[AddExpenseTypeViewController alloc]initWithNibName:@"AddExpenseTypeViewController" bundle:nil];
            [self.navigationController pushViewController:addExpense animated:YES];
            [addExpense release];
        }
    }
    else if((indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) && indexPath.section ==2)
    {
        if(isRecurringRecord)
        {
            UIAlertView *recurringAlertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Recurring settings can not be changed. Any changes made will apply to current entry only." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [recurringAlertView show];
            [recurringAlertView release];
        }
        else if(indexPath.row == 1)
        {
            [self frequency_clicked:frequencyButton];
        }
        else if(indexPath.row == 2)
        {
            [self dateBtn_clicked:recurringDateButton];
        }
        
    }
    
}



-(IBAction)background_touch
{
    /*
     UITextView *textView = (UITextView *)[self.view viewWithTag:2004];
     [textView resignFirstResponder];
     */
    
    UITextField *textField = (UITextField *)[self.view viewWithTag:1003];
    [textField resignFirstResponder];
    
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }
        
    }

    
    
}
#pragma mark -
#pragma mark text View methods
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    nslog(@"\n textViewShouldBeginEditing");
    
    
    if(appDelegate.isIphone5)
    {
        toolBar.frame= CGRectMake(toolBar.frame.origin.x,244,toolBar.frame.size.width, toolBar.frame.size.height);
    }
    
    //[self done_clicked];
    textView.editable = YES;
    
    if(!appDelegate.isIPad)
    {
        
        if(appDelegate.isIphone5)
        {
            [tblView setContentOffset:CGPointMake(0,200)];
        }
        else
        {
            [tblView setContentOffset:CGPointMake(0,220)];
        }
        
        
    }
    else if(appDelegate.isIPad)
    {
        if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
        {
            [tblView setContentOffset:CGPointMake(0,200)];
        }
        
    }
    [textView setEditable:YES];
    [textView setUserInteractionEnabled:YES];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(done_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    
    UIBarButtonItem *flextSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done_clicked)];
    
    */
    
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flextSpace,doneButton, nil]];
    
    
    [cancelBtn release];
    [flextSpace release];
    [doneButton release];
    pickerView.hidden = YES;
    
    datePicker.hidden = YES;
    
    if(appDelegate.isIPad)
    {
        
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                
                toolBar.frame = CGRectMake(0, 671,768, 44);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                
                toolBar.frame = CGRectMake(0,328,1024, 44);
                
            }
            
        
        
    }
    //[self.view endEditing:NO];
    
    UIPlaceHolderTextView *tmpTextView = (UIPlaceHolderTextView*)[self.view viewWithTag:2004];
    
    
    if(!appDelegate.isIOS7)
    {
        [tmpTextView becomeFirstResponder];
    }
    
    
    [self scrollViewToTextView:textView];
    
    
    nslog(@"\n textview = %@",tmpTextView);
    nslog(@"\n textview FR= %d",[tmpTextView isFirstResponder]);
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    nslog(@"\n textViewShouldEndEditing");
    
    if(appDelegate.isIPad)
    {
        toolBar.hidden = TRUE;
        tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,911.0f);
    }
    else
    {
        
        if(appDelegate.isIphone5)
        {
            //tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,455.0f);
            
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
            
        }
        else
        {
            
            if(appDelegate.isIOS7)
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height+25.0f);
            }
            else
            {
                tblView.frame = CGRectMake(tblView.frame.origin.x,tblView.frame.origin.y,tblView.frame.size.width,self.view.frame.size.height);
            }
            
            
        }

    
    
    }
    [dictionary setObject:textView.text forKey:@"notes"];
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    nslog(@"\n textViewDidBeginEditing");
    
    toolBar.hidden = FALSE;
    toolBar.tag = 50;
    toolBar.barStyle = UIBarStyleBlack;
    
    if(!appDelegate.isIOS7)
    {
        [textView becomeFirstResponder];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    nslog(@"\n shouldChangeTextInRange");
    /*Chirag:old condition.
     if ([text isEqualToString:@"\n"])
     {
     [textView resignFirstResponder];
     toolBar.hidden = YES;
     tblView.frame = CGRectMake(0, 0, tblView.frame.size.width, tblView.frame.size.height);
     }
     */
    
    return YES;
}

#pragma mark -
#pragma mark text field methods



-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if(!appDelegate.isIPad)
    {
        //tblView.frame = CGRectMake(0, -30, tblView.frame.size.width, tblView.frame.size.height);
        [tblView setContentOffset:CGPointMake(0,80)];
        
    }
    pickerView.hidden = YES;
    datePicker.hidden = TRUE;
    
    
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(done_clicked)];
    
    if(appDelegate.isIOS7)
    {
        cancelBtn.tintColor = [UIColor whiteColor];
    }
    
    UIBarButtonItem *flextSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    
    UIButton *btnDone1=[UIButton buttonWithType:UIButtonTypeCustom];
    btnDone1.frame=CGRectMake(0, 0,51, 30);
    btnDone1.backgroundColor=[UIColor clearColor];
    [btnDone1 setImage:[UIImage imageNamed:@"nav_right_done_button.png"] forState:UIControlStateNormal];
    [btnDone1 addTarget:self action:@selector(done_clicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone1];
    
    /*
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done_clicked)];
    */
    
    
    
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flextSpace,doneButton, nil]];
    //    toolBar.items = array;
    [cancelBtn release];
    [flextSpace release];
    [doneButton release];
    
    
    if(appDelegate.isIphone5)
    {
        toolBar.frame= CGRectMake(toolBar.frame.origin.x,244,toolBar.frame.size.width, toolBar.frame.size.height);
    }

    
    toolBar.tag = 50;
    toolBar.barStyle = UIBarStyleBlack;
    toolBar.hidden = FALSE;
    
    
    
    if(appDelegate.isIPad)
    {
     
        
            if([UIApplication sharedApplication].statusBarOrientation == 1 || [UIApplication sharedApplication].statusBarOrientation == 2 )
            {
                toolBar.frame = CGRectMake(0, 671,768, 44);
            }
            else if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4 )
            {
                toolBar.frame = CGRectMake(0,328,1024,44);
            
        }
        
        
    }
    
    //[self scrollViewToTextField:textField];
    textFieldIndex = textField.tag;
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    toolBar.hidden = FALSE;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    if(appDelegate.isMilege)
    {
        
        
        NSString *clean_string = [[textField.text
                                   componentsSeparatedByCharactersInSet:
                                   [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                  componentsJoinedByString:@""];
        double centAmount = [clean_string doubleValue];
       
        
        [dictionary setObject:[NSString stringWithFormat:@"%.2f",centAmount / 100] forKey:@"amount"];
    }
    else
    {
        
        
        
        
       
        
        
        
        
        
        textField.text = [textField.text stringByReplacingOccurrencesOfString:@"%20" withString:@""];
        
        
        //NSString *amountStr = textField.text;
        
        NSString *textFieldStr = [NSString stringWithFormat:@"%@",textField.text];
        
        NSMutableString *amountStr = [NSMutableString stringWithString:textFieldStr];
        
        //NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        
        
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        
        
        
        
        [amountStr replaceOccurrencesOfString:numberFormatter.currencySymbol
                                           withString:@""
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [amountStr length])];
        
        [amountStr replaceOccurrencesOfString:numberFormatter.groupingSeparator
                                           withString:@""
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [amountStr length])];
        
        [amountStr replaceOccurrencesOfString:numberFormatter.decimalSeparator
                                           withString:@"."
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [amountStr length])];
        
        
        
        
        NSLog(@"\n textFieldStrValue = %@",amountStr);

        
        
        
        
        /*
        
        
        NSNumberFormatter *numberFormatterTemp = [[NSNumberFormatter alloc] init] ;
        [numberFormatterTemp setNumberStyle: NSNumberFormatterCurrencyStyle];
        
        NSString *amountStr = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet symbolCharacterSet]];
        amountStr = [amountStr stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
        
        amountStr = [amountStr stringByReplacingOccurrencesOfString:@"[^A-Z]" withString:@""];
        amountStr = [amountStr stringByReplacingOccurrencesOfString:@"[^a-z]" withString:@""];
        
        
        amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        
       
        
        
        

        
        
        NSString *reqSysVer = @"7.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        nslog(@"\n currSysVer = %@",currSysVer);
        nslog(@"\n [numberFormatter locale].localeIdentifier = %@",[numberFormatter locale].localeIdentifier);

        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
        {
            
            if([[numberFormatter locale].localeIdentifier isEqualToString:@"pt_PT"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_BR"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_AO"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_CV"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"pt_GW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_ST"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_TL"] ||
                [[numberFormatter locale].localeIdentifier isEqualToString:@"bg_BG"]
               
               
               )
            {
                
                amountStr = [amountStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"€0123456789,."] invertedSet];
                
                amountStr = [[amountStr componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
                
            }
        }

        
        
        
                
         NSLog (@"\n Result: %@ \n", amountStr);
        
        
        nslog(@"\n numberFormatter cur code.. = %@",[numberFormatterTemp currencyCode]);
        nslog(@"\n numberFormatter locale = %@",[numberFormatterTemp locale].localeIdentifier);
        

       
        
        
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        
        
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"])
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        
        
        NSMutableString *reversedStr;
        int len = [amountStr length];
        reversedStr = [NSMutableString stringWithCapacity:len];
        
        while (len > 0)
        {
            [reversedStr appendString:[NSString stringWithFormat:@"%C", [amountStr characterAtIndex:--len]]];
        }
        
        NSRange d = [reversedStr rangeOfString:@","];//for example 3,50(normal 3.50)
        
        
        nslog(@"\n d = %d",d.location);
        
        if(appDelegate.isIOS7)
        {
            
            
            if ([amountStr rangeOfString:@" "].location != NSNotFound || d.location == 3)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            
            
        }
        
        
        if ([amountStr rangeOfString:@" "].location != NSNotFound || d.location == 2)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        
        if ([amountStr rangeOfString:@"'"].location != NSNotFound)//if space in amount string.mostly it is problematic cur.(with , in place of . as decimal place.)
        {
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
            amountStr = [amountStr stringByReplacingOccurrencesOfString:@"'" withString:@""];
        }
        
        
        
        amountStr = [amountStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([appDelegate.curCode isEqualToString:@"ZAR"])
        {
            if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
            }
            else
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@""];
            }
            
        }
        
        
       
        
        else if([[numberFormatter locale].localeIdentifier isEqualToString:@"en_BE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_BE"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_AW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_CW"] || [[numberFormatter locale].localeIdentifier isEqualToString:@"nl_NL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"nl_SX"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"id_ID"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_AO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_BR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_GW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_PT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"pt_ST"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_AT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_DE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"de_LU"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_CY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"el_GR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"da_DK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"it_IT"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_MD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ro_RO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sl_SI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_AR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_BO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_CR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_EC"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_GQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_PY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_UY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"es_VE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"tr_TR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_ME"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sr-Latn_RS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"seh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sg_CF"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rn_BI"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mua_CM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ms_BN"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mgh_MZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"mk_MK"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"lu_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ln_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rw_RW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kl_GL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"kea_CV"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"is_IS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ka_GE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"gl_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"fo_FO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"hr_HR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"swc_CD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ca_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"bs_BA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"eu_ES"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"az-Cyrl_AZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DZ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"sq_AL"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MA"])
        {
            nslog(@"Str_budget -- %@",amountStr);
            
                       NSString *stringToFilter =[NSString stringWithFormat:@"%@",amountStr];
            NSMutableString *targetString = [NSMutableString string];
            //set of characters which are required in the string......
            NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"."];
            nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
            
            for(int i = 0; i < [stringToFilter length]; i++)
            {
                unichar currentChar = [stringToFilter characterAtIndex:i];
                nslog(@"currentChar -- %C",currentChar);
                
                if(i == [stringToFilter length]-3)
                {
                    nslog(@"currentChar -- %C",currentChar);
                    [targetString appendFormat:@"%C", currentChar];
                }
                else
                {
                    if([okCharacterSet characterIsMember:currentChar])
                    {
                        [targetString appendFormat:@""];
                    }
                    else
                    {
                        [targetString appendFormat:@"%C", currentChar];
                    }
                }
            }
            nslog(@"targetString -- %@",targetString);
            amountStr = [NSString stringWithFormat:@"%@",targetString];
            nslog(@"Str_budget -- %@",amountStr);
        }
        
        else if([[numberFormatter locale].localeIdentifier isEqualToString:@"gsw_CH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"rm_CH"])
        {
            nslog(@"Str_budget -- %@",amountStr);
            
                       NSString *stringToFilter =[NSString stringWithFormat:@"%@",amountStr];
            NSMutableString *targetString = [NSMutableString string];
            //set of characters which are required in the string......
            NSCharacterSet *okCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
            
            for(int i = 0; i < [stringToFilter length]; i++)
            {
                unichar currentChar = [stringToFilter characterAtIndex:i];
                nslog(@"currentChar -- %C",currentChar);
                
                if(i == [stringToFilter length]-3)
                {
                    nslog(@"currentChar -- %C",currentChar);
                    [targetString appendFormat:@"%C", currentChar];
                }
                else
                {
                    if([okCharacterSet characterIsMember:currentChar])
                    {
                        [targetString appendFormat:@"%C", currentChar];
                    }
                    else
                    {
                        [targetString appendFormat:@""];
                    }
                }
            }
            nslog(@"targetString -- %@",targetString);
            amountStr = [NSString stringWithFormat:@"%@",targetString];
            nslog(@"Str_budget -- %@",amountStr);
        }
        else if([[numberFormatter locale].localeIdentifier isEqualToString:@"ar_IQ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_BH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_TD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_DJ"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EG"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_ER"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_JO"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_KW"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LB"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_LY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_MR"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_OM"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_PS"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_QA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SA"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SD"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_SY"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_AE"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_EH"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_001"] ||[[numberFormatter locale].localeIdentifier isEqualToString:@"ar_YE"])
        {
            nslog(@"Str_budget -- %@",amountStr);
            NSString *stringToFilter =[NSString stringWithFormat:@"%@",amountStr];
            NSMutableString *targetString = [NSMutableString string];
            
            nslog(@"[stringToFilter length] -- %d",[stringToFilter length]);
            
            for(int i = 0; i < [stringToFilter length]; i++)
            {
                unichar currentChar = [stringToFilter characterAtIndex:i];
                nslog(@"currentChar -- %C",currentChar);
                
                if(i == [stringToFilter length]-3)
                {
                    nslog(@"currentChar -- %C",currentChar);
                    [targetString appendFormat:@"."];
                }
                else
                {
                    [targetString appendFormat:@"%C", currentChar];
                }
            }
            nslog(@"targetString -- %@",targetString);
            amountStr = [NSString stringWithFormat:@"%@",targetString];
            nslog(@"Str_budget -- %@",amountStr);
        }
        
        
        
        
        else
        {
            
            if ([appDelegate.curCode isEqualToString:@"Default Region Currency"])
            {
                
                
                if([[numberFormatter currencyCode] isEqualToString:@"ZAR"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"en_ZA"])//THis means user has South afric from device.
                {
                    amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
                }
                else if([[numberFormatter locale].localeIdentifier isEqualToString:@"bg_BG"])
                {
                    amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@"."];
                }
                else
                {
                    amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@""];
                }
                
                
            }
            else
            {
                amountStr = [amountStr stringByReplacingOccurrencesOfString:@"," withString:@""];
            }
            
            
        }
        
        
        */
        
        nslog(@"\n amount str = %@",amountStr);
        
        [dictionary setObject:amountStr forKey:@"amount"];
        
        if(appDelegate.isIPad)
        {
            toolBar.hidden = TRUE;
            [textField resignFirstResponder];
        }
        
       // [numberFormatterTemp release];

        
        
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    toolBar.hidden = YES;
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    nslog(@"\n string in shouldChangeCharactersInRange = %@",string);
    
    
    if(appDelegate.isMilege)
    {
        
        /**
        NSMutableCharacterSet *numberSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
        */
        NSMutableCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet] ;
        [numberSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
        NSCharacterSet *nonNumberSet = [numberSet invertedSet];

        
        BOOL result = NO;
        
        if([string length] == 0)
		{
            result = YES;
        }
        else
		{
            if([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0)
			{
                result = YES;
            }
        }
        
        if(result)
        {
            
            
            NSString *clean_string = [[textField.text
                                       componentsSeparatedByCharactersInSet:
                                       [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                      componentsJoinedByString:@""];
            
            
            
            nslog(@"\n clean_string = %@",clean_string);
            
            
            
            double centAmount = [clean_string doubleValue];
            nslog(@"\n  centAmount %f",centAmount);
            nslog(@"\n [string doubleValue] =  %f",[string doubleValue]);
            
            
            NSNumber *_amount_local;
            
            if (string.length > 0)
            {
                // Digit added
                centAmount = centAmount * 10 + [string doubleValue];
                _amount_local = [[NSNumber alloc]initWithDouble:(double)centAmount/100];
            }
            else
            {
                // Digit deleted
                
                centAmount = [[NSString stringWithFormat:@"%.2f",centAmount / 10] doubleValue];
                _amount_local = [[NSNumber alloc]initWithDouble:(double)centAmount/100];
            }
            
            
            
            nslog(@"\n  centAmount after = %f",centAmount);
            nslog(@"\n  _amount_local = %@",_amount_local);
            clean_string = [NSString stringWithFormat:@"%.2f",[_amount_local doubleValue]];
            nslog(@"\n  clean_string %@",clean_string);

            [_amount_local release];

          
            clean_string  = [clean_string stringByAppendingFormat:@" %@",[appDelegate.prefs objectForKey:@"milage_unit"]];
            
			[textField setText:clean_string];
        }
       
        //textField.text = [@"$" stringByAppendingString:textField.text];
        
        
        
    }
    else/*
            SUN:004- IF EITHER INCOME OR EXPENSE.
         */
    {
        if(appDelegate.isIPad)
        {
            if([string length]!=0)
            {
                NSMutableString *strippedString = [NSMutableString
                                                   stringWithCapacity:string.length];
                NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
                NSScanner *scanner = [NSScanner scannerWithString:string];
                
                
                while ([scanner isAtEnd] == NO)
                {
                    NSString *buffer;
                    if ([scanner scanCharactersFromSet:numbers intoString:&buffer])
                    {
                        [strippedString appendString:buffer];
                        
                    } else
                    {
                        [scanner setScanLocation:([scanner scanLocation] + 1)];
                    }
                }
                
                nslog(@"strippedString  = %@", strippedString); // "123123123"
                if([strippedString length]==0)
                {
                    return NO;
                }
                string = strippedString;
                
            }
            
        }
        
        
        
        
        
        
        /**
        NSDecimalNumber *someAmount = [NSDecimalNumber decimalNumberWithString:@"5.00"];
        */
        
        NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
        
        
        
        if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
        {
            [currencyFormatter setCurrencyCode:appDelegate.curCode];
            
            
            if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
            {
                [currencyFormatter setCurrencyCode:@"USD"];
                nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                [currencyFormatter setLocale:locale];
                [locale release];
                
            }
            else if([[currencyFormatter currencyCode] isEqualToString:@"CNY"])
            {
                [currencyFormatter setCurrencyCode:@"CNY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                [currencyFormatter setLocale:locale];
                [locale release];
                
            }
            else if([[currencyFormatter currencyCode] isEqualToString:@"JPY"])
            {
                [currencyFormatter setCurrencyCode:@"JPY"];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                [currencyFormatter setLocale:locale];
                [locale release];
                
            }
        }
        [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        
        
        int currencyScale = [currencyFormatter maximumFractionDigits];
        [currencyFormatter release];
        nslog(@"\n scale = %d",currencyScale);
        
        
        
        
        
        
        
        
        
        
        
        
        /*This is old code...*/
        
        if([appDelegate.curCode isEqualToString:@"Default Region Currency"] && [[numberFormatter locale].localeIdentifier isEqualToString:@"vi_VN"])
        {
            currencyScale = 0;
        }
        
        if(currencyScale == 0)
        {
            
            NSString *cleanCentString = [[textField.text
                                          componentsSeparatedByCharactersInSet:
                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                         componentsJoinedByString:@""];
            
            nslog(@"\n cleanCentString = %@",cleanCentString);
            // Parse final integer value
            nslog(@"\n textField.text = %@",textField.text);
            double centAmount = [cleanCentString doubleValue];
            // Check the user input
            nslog(@"\n centAmount = %f",centAmount);
            if (string.length > 0)
            {
                // Digit added
                centAmount = centAmount * 10 + [string doubleValue];
            }
            else
            {
                // Digit deleted
                centAmount = centAmount / 10;
            }
            // Update call amount value
            NSNumber *_amount;
            //_amount = [[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f];
            
            _amount = [[NSNumber alloc]initWithLongLong:(long long)centAmount];
            
            NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
            {
                [_currencyFormatter setCurrencyCode:appDelegate.curCode];
                
                if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
                {
                    [_currencyFormatter setCurrencyCode:@"USD"];
                    nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
                {
                    [_currencyFormatter setCurrencyCode:@"CNY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
                {
                    [_currencyFormatter setCurrencyCode:@"JPY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                
            }
            else
            {
                nslog(@"\n NSLocaleCurrencyCode = %@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]);
                [_currencyFormatter setCurrencyCode:[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
            }
            [_currencyFormatter setNegativeFormat:@"-¤#,##0.00"];
            
            nslog(@"\n _amount = %@",_amount);
            
            //textField.text = [_currencyFormatter stringFromNumber:_amount];
            textField.text = [_currencyFormatter stringFromNumber:_amount];
            nslog(@"\n textField.tex = %@",textField.text);
            [_currencyFormatter release];
            [_amount release];
            // Since we already wrote our changes to the textfield
            // we don't want to change the textfield again
        }
        else
        {
            
            if([[appDelegate.curCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
            {
                [appDelegate selectCurrencyIndex];
            }
            
            
            NSString *cleanCentString = [[textField.text
                                          componentsSeparatedByCharactersInSet:
                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                         componentsJoinedByString:@""];
            
            
            NSLog(@"\n textField.text = %@",textField.text);
            
            nslog(@"\n cleanCentString = %@",cleanCentString);
            // Parse final integer value
            nslog(@"\n textField.text = %@",textField.text);
            double centAmount = [cleanCentString doubleValue];
            // Check the user input
            nslog(@"\n centAmount = %f",centAmount);
            if (string.length > 0)
            {
                // Digit added
                centAmount = centAmount * 10 + [string doubleValue];
            }
            else
            {
                // Digit deleted
                centAmount = centAmount / 10;
            }
            // Update call amount value
            NSNumber *_amount;
            
            
            _amount = [[NSNumber alloc]initWithDouble:(double)centAmount / 100.0f];
            
            NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            if (![appDelegate.curCode isEqualToString:@"Default Region Currency"])
            {
                [_currencyFormatter setCurrencyCode:appDelegate.curCode];
                
                if([appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode] != NSNotFound)
                {
                    [_currencyFormatter setCurrencyCode:@"USD"];
                    nslog(@" index = %d",[appDelegate.currencyWithDollarSymbol indexOfObject:appDelegate.curCode]);
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"CNY"])
                {
                    [_currencyFormatter setCurrencyCode:@"CNY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"zh_CN"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
                else if([[_currencyFormatter currencyCode] isEqualToString:@"JPY"])
                {
                    [_currencyFormatter setCurrencyCode:@"JPY"];
                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_JM"];
                    [_currencyFormatter setLocale:locale];
                    [locale release];
                    
                }
            }
            else
            {
                nslog(@"\n NSLocaleCurrencyCode = %@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]);
                [_currencyFormatter setCurrencyCode:[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
            }
            [_currencyFormatter setNegativeFormat:@"-¤#,##0.00"];
            
            nslog(@"\n _amount = %@",_amount);
            
            //textField.text = [_currencyFormatter stringFromNumber:_amount];
            textField.text = [_currencyFormatter stringFromNumber:_amount];
            nslog(@"\n textField.tex = %@",textField.text);
            
            

            
            
            
            
            
           
            NSLog (@"\n Result: %@ \n", textField.text);
            
            
            [_currencyFormatter release];
            [_amount release];
            // Since we already wrote our changes to the textfield
            // we don't want to change the textfield again
            
        }
        

    }
    
    
    
    return NO;
}


#pragma mark -
#pragma mark income_expense_receipt_button_pressed


-(IBAction)income_expense_receipt_button_pressed
{
    nslog(@"\n income_expense_receipt_button_pressed \n");
    
    
    nslog(@"\n income_expense_receipt_button_pressed = %@ \n",[self.tabBarController viewControllers]);

    
    
    
   
    ReceiptViewController*viewController = [[ReceiptViewController alloc] initWithNibName:@"ReceiptViewController" bundle:nil];
    
    UINavigationController *receiptViewNavigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    receiptViewNavigationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
     [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    [self.navigationController presentModalViewController:receiptViewNavigationController animated:YES];
    //[self.navigationController pushViewController:viewController animated:YES];
    
    
    [receiptViewNavigationController release];
    
    [viewController release];
    
    
    
}

#pragma mark - scrollViewToTextField

- (void)scrollViewToTextField:(id)textField
{
    // Set the current _scrollOffset, so we can return the user after editing
    //   _scrollOffsetY = self.tableView.contentOffset.y;
    
    // Get a pointer to the text field's cell
    
    
    
    
    UITableViewCell *theTextFieldCell = (UITableViewCell *)[textField superview];
    
    // Get the text fields location
    CGPoint point = [theTextFieldCell convertPoint:theTextFieldCell.frame.origin toView:tblView];
    
    // Scroll to cell
    [tblView setContentOffset:CGPointMake(0, point.y - 12) animated: YES];
    
    /*
     // Add some padding at the bottom to 'trick' the scrollView.
     [tblView setContentInset:UIEdgeInsetsMake(0, 0, point.y - 60, 0)];
     */
}

#pragma mark - scrollViewToTextView


- (void)scrollViewToTextView:(id)textView
{
    // Set the current _scrollOffset, so we can return the user after editing
    //   _scrollOffsetY = self.tableView.contentOffset.y;
    
    // Get a pointer to the text field's cell
    UITableViewCell *theTextFieldCell = (UITableViewCell *)[textView superview];
    
    // Get the text fields location
    CGPoint point = [theTextFieldCell convertPoint:theTextFieldCell.frame.origin toView:tblView];
    
    
    
    // Scroll to cell
    
    if(appDelegate.isIPad)
    {
        [tblView setContentOffset:CGPointMake(0,point.y-120) animated: YES];
    }
    else
    {
        if(appDelegate.isIphone5)
        {
            [tblView setContentOffset:CGPointMake(0,point.y-80) animated: YES];
        }
        else
        {
            [tblView setContentOffset:CGPointMake(0,point.y-65) animated: YES];
        }

    }
    
        
    
    
    // Add some padding at the bottom to 'trick' the scrollView.
    // [tblView setContentInset:UIEdgeInsetsMake(0, 0, point.y - 60, 0)];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning
{
    
    MemoryLog(@"\n didReceiveMemoryWarning  in AddIncomeExpenseViewController \n");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload
{
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc
{
    
    nslog(@"\n dealloc in AAdIncomeExpense...");
    
    [section_header_view release];
    [tblView release];
    [datePicker release];
    [toolBar release];
    [pickerArray release];
    [pickerView release];
    [originalEndDateString release];

    
    if(!appDelegate.isIOS7)
    {
       
        [super dealloc];
    }
    
    
    
    
}


@end

