//
//  FAQViewController.h
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 11/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
@interface FAQViewController : UIViewController<UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UIButton *faqButton;
    IBOutlet UIButton *onlineTrainigButton;
    IBOutlet UIButton *supportButton;
    IBOutlet UIButton *feedBackButton;
    
    NSString*emailSubject;
    NSString*emailRecepient;
    
    IBOutlet UITableView*tblView;
    
}
-(IBAction)faqButton_pressed:(id)sender;
-(IBAction)onlineTrainigButton_pressed:(id)sender;
-(IBAction)supportButton_pressed:(id)sender;
-(IBAction)feedBackButton_pressed:(id)sender;
-(void)displayComposerSheetforsupport;
-(void)displayComposerSheet ;
-(void)launchMailAppOnDevice;
@end
