//
//  AddIncomeExpenseViewController.h
//  PropertyLogBook
//
//  Created by Smit Nebhwani on 7/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPlaceHolderTextView.h"


@class PropertyLogBookAppDelegate;
@class ActionSheetPicker;
@interface AddIncomeExpenseViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate,UIPopoverControllerDelegate,UIActionSheetDelegate, UIPickerViewDelegate, UIPickerViewDataSource,UIAlertViewDelegate> {
	
	IBOutlet UITableView *tblView;
	
	NSMutableArray *mainArray;
    
    /*
        SUN:0004
     
     
     */
    NSMutableArray*recurringArray;
    
    
	NSMutableArray *detailArray;
    PropertyLogBookAppDelegate *appDelegate;
    UIDatePicker *datePicker;
    
    UIToolbar *toolBar;
    
    int textFieldIndex;
    NSNumberFormatter *numberFormatter;

    NSMutableDictionary *dictionary;
    NSMutableDictionary *oldDataDictionary;
    
    BOOL isRecurring;
    BOOL isRecurringRecord;
    UIPickerView *pickerView;
    NSMutableArray *pickerArray;
    
    /*CP:Starts:Recurring
     Date:*/    
    
    NSMutableArray*recurringDetailArray;
    UIButton *frequencyButton;
    BOOL isrecurringDate;
    UIButton *recurringDateButton;
    NSString*recurrenceTime;
    NSInteger numberOfWeeks;
    NSInteger numberOfRemainingDays;
    NSInteger numberOfTwoWeeks;
    NSInteger numberOfMonths;
    NSInteger numberOfYears;
    NSInteger numberOfQuarters;
    NSString*originalEndDateString;
    
    /*CP:Starts:Recurring
     Date:*/
    UILabel*placeholderLabel;
    UIPlaceHolderTextView* placeHolderTextView;
    
    /*Checking if ios 5*/
    BOOL isIOS5;
    

    
    
    IBOutlet UIView*section_header_view;
    IBOutlet UILabel*icome_expense_date_label;
    IBOutlet UILabel*icome_expense_month_label;
    IBOutlet UITextField*icome_expense_amount_text;
    IBOutlet UIButton*income_expense_receipt_button;
    
    IBOutlet UIImageView*income_expense_section_header_bg;
    
    IBOutlet UIButton*section_date_button;
    
    
    
}
@property (nonatomic, retain) ActionSheetPicker *actionSheetPicker;
@property (nonatomic, retain) NSString*originalEndDateString;



@property (nonatomic, retain) UINib *cellNib;




-(IBAction)background_touch;



-(IBAction)income_expense_receipt_button_pressed;
-(IBAction)dateBtn_clicked:(id)sender;

- (void)scrollViewToTextView:(id)textView;
- (void)scrollViewToTextField:(id)textField;

@end
