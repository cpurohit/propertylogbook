//
//  FAQViewController.m
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 11/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FAQViewController.h"




@implementation FAQViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:@"FAQViewController" bundle:nibBundleOrNil];
    
    nslog(@"\n initWithNibName");
    
    if (self) 
    {
        // Custom initialization
    }
    return self;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;   
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0f;
}

/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView*view;
    if(section ==0)
    {
       view = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,40)];
       UIImageView*imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"FAQ.png"]];
       imageView.frame = CGRectMake(10,5,30,30);
       
        UILabel*label = [[UILabel alloc] initWithFrame:CGRectMake(45,5,300,30)]; 
        label.text = @"Frequently Asked Questions";
        label.textColor = [UIColor colorWithRed:50.0f/255.0f green:79.0f/255.0f blue:133.0f/255.0f alpha:1.0f];
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        label.backgroundColor = [UIColor clearColor];
        [view addSubview:label];
        [view addSubview:imageView];
    }
    else if(section ==1)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,40)];
        UIImageView*imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"VIDEO.png"]];
        imageView.frame = CGRectMake(10,5,30,30);
        
        UILabel*label = [[UILabel alloc] initWithFrame:CGRectMake(45,5,300,30)]; 
        label.text = @"Online Training Video";
        label.textColor = [UIColor colorWithRed:50.0f/255.0f green:79.0f/255.0f blue:133.0f/255.0f alpha:1.0f];
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        label.backgroundColor = [UIColor clearColor];
        [view addSubview:label];

        
        [view addSubview:imageView];
    }

    else if(section ==2)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,40)];
        UIImageView*imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SUPPORT.png"]];
        imageView.frame = CGRectMake(10,5,30,30);
        
        UILabel*label = [[UILabel alloc] initWithFrame:CGRectMake(45,5,100,30)]; 
        label.text = @"Support";
        label.textColor = [UIColor colorWithRed:50.0f/255.0f green:79.0f/255.0f blue:133.0f/255.0f alpha:1.0f];
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        label.backgroundColor = [UIColor clearColor];
        [view addSubview:label];

        
        [view addSubview:imageView];
    }


    else if(section ==3)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,40)];
        UIImageView*imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"FEEDBACK.png"]];
        imageView.frame = CGRectMake(10,5,30,30);
        
        UILabel*label = [[UILabel alloc] initWithFrame:CGRectMake(45,5,100,30)]; 
        label.text = @"Feedback";
        label.textColor = [UIColor colorWithRed:50.0f/255.0f green:79.0f/255.0f blue:133.0f/255.0f alpha:1.0f];
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
        label.backgroundColor = [UIColor clearColor];
        [view addSubview:label];
        
        [view addSubview:imageView];
    }

    return view;
}
*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *kCellID = @"cellID";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellID] autorelease];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}
    if(indexPath.row == 0)
    {
        cell.imageView.image =  [UIImage imageNamed:@"FAQ_SMLL.png"];
        cell.textLabel.text = @"FAQ’s";        
    }
    
    /*
    else if(indexPath.row == 1)
    {
        cell.imageView.image =  [UIImage imageNamed:@"VIDEO_SMLL.png"];
        cell.textLabel.text = @"Online Training Video";        
    }
     */

    else if(indexPath.row == 1)
    {
        cell.imageView.image =  [UIImage imageNamed:@"SUPPORT_SMLL.png"];
        cell.textLabel.text = @"Support";        
    }
    else if(indexPath.row == 2)
    {
        cell.imageView.image =  [UIImage imageNamed:@"FEEDBACK_SMLL.png"];
        cell.textLabel.text = @"Feedback";        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        NSURL *url = [NSURL URLWithString:@"http://www.iapplab.com/propertylogbook_faq.pdf"];
        
        if (![[UIApplication sharedApplication] openURL:url])
        {
            nslog(@"%@%@",@"Failed to open url:",[url description]);
        }

    }
    
    else if(indexPath.row==1)
    {
        Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
        if (mailClass != nil)
        {
            
            emailRecepient = @"support@iapplab.com";
            // We must always check whether the current device is configured for sending emails
            if ([mailClass canSendMail])
            {
                [self displayComposerSheetforsupport];
            }
            else
            {
                [self launchMailAppOnDevice];
            }
        }
        else
        {
            [self launchMailAppOnDevice];
        }

    }
    else if(indexPath.row==2)
    {
        emailRecepient = @"feedback@iapplab.com";
        Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
        if (mailClass != nil)
        {
            // We must always check whether the current device is configured for sending emails
            if ([mailClass canSendMail])
            {
                [self displayComposerSheet];
            }
            else
            {
                [self launchMailAppOnDevice];
            }
        }
        else
        {
            [self launchMailAppOnDevice];
        }
        

    }

}
-(IBAction)faqButton_pressed:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"http://www.iapplab.com/propertylogbook_faq.pdf"];
    
    if (![[UIApplication sharedApplication] openURL:url])
    {
        nslog(@"%@%@",@"Failed to open url:",[url description]);
    }
        

}
-(IBAction)onlineTrainigButton_pressed:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"http://www.iapplab.com/propertylogbook_video"];
    
    if (![[UIApplication sharedApplication] openURL:url])
    {
        nslog(@"%@%@",@"Failed to open url:",[url description]);
    }

}
-(IBAction)supportButton_pressed:(id)sender
{
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
        
        emailRecepient = @"support@iapplab.com";
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheetforsupport];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}

    /*
    NSURL *url = [NSURL URLWithString:@"http://www.stackoverflow.com"];
    
    if (![[UIApplication sharedApplication] openURL:url])
    {
        nslog(@"%@%@",@"Failed to open url:",[url description]);
    }
     */

}
-(IBAction)feedBackButton_pressed:(id)sender
{
    
    emailRecepient = @"feedback@iapplab.com";
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}
    

}

-(void)displayComposerSheetforsupport
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
    //	[[picker navigationBar] setTintColor:[UIColor blackColor]];
	//[[picker navigationBar] setTintColor:[UIColor colorWithRed:0.36 green:0.09 blue:0.39 alpha:1.00]];
    
	[picker setSubject:@"Support: Property Log Book"];
	
	
	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:emailRecepient]; 
    NSString *deviceModel = [UIDevice currentDevice].model;
    nslog(@"Device Model------%@",[UIDevice currentDevice].model);
    NSString *deviceOS = [UIDevice currentDevice].systemVersion;
    nslog(@"Device Current os-----%@",[UIDevice currentDevice].systemVersion);
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    nslog(@"App Version-----%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]);
    
    NSLocale *currentUsersLocale = [NSLocale currentLocale];
    NSString *region = [currentUsersLocale localeIdentifier];
    nslog(@"Current Locale: %@", [currentUsersLocale localeIdentifier]);
	
	[picker setToRecipients:toRecipients];
    
    NSString *emailBody = [NSString stringWithFormat:@"Device : %@ \n OS Version: %@ \n App Version: %@ \n Region Code: %@",deviceModel, deviceOS, version, region];
    
   	[picker setMessageBody:emailBody isHTML:NO];
    
	
	[self presentModalViewController:picker animated:YES];
    [picker release];

}

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
    //	[[picker navigationBar] setTintColor:[UIColor blackColor]];
	//[[picker navigationBar] setTintColor:[UIColor colorWithRed:0.36 green:0.09 blue:0.39 alpha:1.00]];
    
	[picker setSubject:@"Feedback: Property Log Book"];
	
	
	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:emailRecepient]; 
    
	
	[picker setToRecipients:toRecipients];
    
    NSString *emailBody = @"";
    
   	[picker setMessageBody:emailBody isHTML:YES];
    
	
	[self presentModalViewController:picker animated:YES];
    [picker release];
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
    [self dismissModalViewControllerAnimated:YES];
	
}

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	NSString *recipients =[NSString stringWithFormat:@"mailto:%@&subject=",emailRecepient] ;
	NSString *body = @"&body=";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    MemoryLog(@"\n didReceiveMemoryWarning in = %@ \n",NSStringFromClass([self class]));
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    
}

#pragma mark - View lifecycle
-(void)viewDidDisappear:(BOOL)animated
{
    nslog(@"disappear");
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    /*
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_shadow.png"]];
    [imgView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //  [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
    [self.view sendSubviewToBack:imgView];
      [imgView release];
    */
    
    
    [tblView setBackgroundView:nil];
    [tblView setBackgroundView:[[[UIView alloc] init] autorelease]];

    
    tblView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_shadow.png"]];
    
   

}



- (void)viewDidLoad
{
    [super viewDidLoad];
     self.navigationItem.title = @"HELP & SUPPORT";
    
    /* CP :  */
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }


    
    /*
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(btnDone)];
     
    */ 
    // Do any additional setup after loading the view from its nib.
}




-(void)btnDone
{
    [self dismissModalViewControllerAnimated:TRUE];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
