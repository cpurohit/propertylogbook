//
//  AboutViewController.h
//  PropertyLogBook
//
//  Created by Chirag@Sunshine on 11/1/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyLogBookAppDelegate.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface AboutViewController : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    IBOutlet UIWebView*webview;
    PropertyLogBookAppDelegate *appDelegate;
    IBOutlet UIImageView*iconImageView;
    IBOutlet UIButton*feedBackButton;
    IBOutlet UILabel*textLabel;
    
    IBOutlet UIButton*myReferenceLinkButton;
    IBOutlet UIButton*siteLinkButton;
    IBOutlet UIImageView*bgImageView;
    
  
    IBOutlet UIButton*my_reference_button;
    IBOutlet UIButton*travel_logbook_button;
    IBOutlet UIButton*iapplab_site_button;
    
    BOOL is_dismiss_properly;
    
}
-(IBAction)feedBackButton_pressed:(id)sender;
@property(nonatomic,retain)IBOutlet UIWebView*webview;
-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;
-(IBAction)iapplab_clicked:(id)sender;
-(IBAction)myreference_clicked:(id)sender;
-(IBAction)bonvoyage_clicked:(id)sender;

@end
