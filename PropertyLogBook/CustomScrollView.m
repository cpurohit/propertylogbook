//
//  CustomScrollView.m
//  ScrollTouch
//
//  Created by Davut Can Abacigil on 5/21/11.
//  Copyright 2011 Abacigil Information Technologies Ltd. All rights reserved.
//

#import "CustomScrollView.h"


@implementation CustomScrollView
- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event 
{
	
	if (!self.dragging) 
    {
		[self.nextResponder touchesEnded: touches withEvent:event]; 
	}		
	[super touchesEnded: touches withEvent: event];
}

@end
